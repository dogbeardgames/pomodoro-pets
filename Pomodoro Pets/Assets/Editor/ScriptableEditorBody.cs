﻿using UnityEngine;
using System.Collections;
using UnityEditor;


public static class ScriptableEditorBody
{
	[MenuItem("Assets/Create/Create Body Equipment")]
	public static void CreateDatabaseAsset()
	{

		BodyDatabase bodyDb =  ScriptableObject.CreateInstance<BodyDatabase >();

		ProjectWindowUtil.CreateAsset(bodyDb , "Assets/Scripts/Scriptable Objects/Equipment/Body/BodyDatabase.asset");

		AssetDatabase.SaveAssets();

		EditorUtility.FocusProjectWindow();

		Selection.activeObject = bodyDb ;
	}
}