﻿using UnityEngine;
using System.Collections;
using UnityEditor;


public static class ScriptableEditorHead
{
	[MenuItem("Assets/Create/Create Head Equipment")]
	public static void CreateDatabaseAsset()
	{
		
		HeadDatabase headDb =  ScriptableObject.CreateInstance<HeadDatabase>();

		ProjectWindowUtil.CreateAsset(headDb , "Assets/Scripts/Scriptable Objects/Equipment/Head/HeadDatabase.asset");

		AssetDatabase.SaveAssets();

		EditorUtility.FocusProjectWindow();

		Selection.activeObject = headDb ;
	}
}