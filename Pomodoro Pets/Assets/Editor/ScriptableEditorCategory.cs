﻿using UnityEngine;
using System.Collections;
using UnityEditor;


public  class ScriptableEditorCategory
{
    [MenuItem("Assets/Create/Create Category")]
    public static void CreateDatabaseAsset()
    {
        CategoryDatabase categdb =  ScriptableObject.CreateInstance<CategoryDatabase>();

		ProjectWindowUtil.CreateAsset(categdb, "Assets/Scripts/Scriptable Objects/Category/CategoryDatabase.asset");

		AssetDatabase.SaveAssets();

		EditorUtility.FocusProjectWindow();

		Selection.activeObject = categdb;
    }
}