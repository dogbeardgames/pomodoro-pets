﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CatAsset {

    [MenuItem("Assets/Create/Create Cat Asset")]
    public static void CreateDatabaseAsset()
    {
        CatDatabase catDb =  ScriptableObject.CreateInstance<CatDatabase>();

        ProjectWindowUtil.CreateAsset(catDb, "Assets/Scripts/Scriptable Objects/Pets/PetDatabase.asset");

        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = catDb;
    }
}
