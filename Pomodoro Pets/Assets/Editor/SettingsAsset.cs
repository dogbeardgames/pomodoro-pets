﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SettingsAsset{

    [MenuItem("Assets/Create/Create Settings Asset")]
    public static void CreateDatabaseAssets()
    {
        Settings settings = ScriptableObject.CreateInstance<Settings>();

        ProjectWindowUtil.CreateAsset(settings, "Assets/Scripts/Scriptable Objects/Settings/Settings.asset");
        AssetDatabase.SaveAssets();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = settings;
    }
}
