﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class WizardTagFinder : ScriptableWizard {

	public string searchTag = "Your tag here";

    [MenuItem("My Tools/Tag Finder")]
    static void FindTagDialog()
    {
		ScriptableWizard.DisplayWizard<WizardTagFinder> ("Select All Of Tag...", "Make Selection");
    }

	void OnWizardCreate()
	{
		GameObject[] gameObject = GameObject.FindGameObjectsWithTag (searchTag);
		Selection.objects = gameObject;
	}
}