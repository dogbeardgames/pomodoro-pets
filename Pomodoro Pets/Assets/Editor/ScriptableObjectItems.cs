﻿using System.Collections;
using UnityEngine;
using UnityEditor;

public class ScriptableObjectItems{

    [MenuItem("Assets/Create/Create Shop Items")]
    public static void CreateDatabaseAsset()
    {

        PetShopItems petShopDB =  ScriptableObject.CreateInstance<PetShopItems>();

        ProjectWindowUtil.CreateAsset(petShopDB , "Assets/Scripts/Scriptable Objects/PetItems/PetShopItems.asset");

        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = petShopDB ;
    }

    [MenuItem("Assets/Create/Create Food Shop Items")]
    public static void CreatePetShopFoodAsset()
    {

        PetShopFoodItems petFoodShopDB =  ScriptableObject.CreateInstance<PetShopFoodItems>();

        ProjectWindowUtil.CreateAsset(petFoodShopDB , "Assets/Scripts/Scriptable Objects/PetItems/PetFoodShopItems.asset");

        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = petFoodShopDB ;
    }

    [MenuItem("Assets/Create/Create Background Items")]
    public static void CreateBackgroundAsset()
    {

        Backgrounds backgroundDB =  ScriptableObject.CreateInstance<Backgrounds>();

        ProjectWindowUtil.CreateAsset(backgroundDB , "Assets/Scripts/Scriptable Objects/BG/BackgroundDB.asset");

        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = backgroundDB ;
    }

    [MenuItem("Assets/Create/Create Playthings Items")]
    public static void CreatePetPlaythingsAsset()
    {

        PetPlaythings playThings =  ScriptableObject.CreateInstance<PetPlaythings>();

        ProjectWindowUtil.CreateAsset(playThings , "Assets/Scripts/Scriptable Objects/PetItems/PetPlaythings.asset");

        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = playThings ;
    }

    [MenuItem("Assets/Create/Create Pet Home Location Asset")]
    public static void CreatePetHomeLocationAsset()
    {

        PetHomeLocation petHomeLocation =  ScriptableObject.CreateInstance<PetHomeLocation>();

        ProjectWindowUtil.CreateAsset(petHomeLocation , "Assets/Scripts/Scriptable Objects/PetHomeLocation/PetHomeLocation.asset");

        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = petHomeLocation ;
    }

    [MenuItem("Assets/Create/Create Pet Bath Items")]
    public static void CreatePetBathItems()
    {

        PetBathScriptableObject PetBathItem =  ScriptableObject.CreateInstance<PetBathScriptableObject>();

        ProjectWindowUtil.CreateAsset(PetBathItem , "Assets/Scripts/Scriptable Objects/PetBath/PetBathDatabase.asset");

        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = PetBathItem ;
    }

    [MenuItem("Assets/Create/Create Hammer Items")]
    public static void CreateHammerItems()
    {

        HammerData HammerItem =  ScriptableObject.CreateInstance<HammerData>();

        ProjectWindowUtil.CreateAsset(HammerItem , "Assets/Scripts/Scriptable Objects/Hammer/HammerDatabase.asset");

        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = HammerItem ;
    }

    [MenuItem("Assets/Create/Create Bundle Items")]
    public static void CreateBundleItemAsset()
    {

        BundleItems bundleItem =  ScriptableObject.CreateInstance<BundleItems>();

        ProjectWindowUtil.CreateAsset(bundleItem , "Assets/Scripts/Scriptable Objects/BundleItems/BundleItemDatabase.asset");

        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = bundleItem ;
    }
}
