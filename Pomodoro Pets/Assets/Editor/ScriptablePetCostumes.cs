﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ScriptablePetCostumes : MonoBehaviour {

    [MenuItem("Assets/Create/Create Pet Body Costume Dictionary")]
    public static void CreateCostumeDictionary()
    {
        PetCostumeDictionary petCostumeDb =  ScriptableObject.CreateInstance<PetCostumeDictionary>();

        ProjectWindowUtil.CreateAsset(petCostumeDb, "Assets/Scripts/Scriptable Objects/Costumes/PetCostumeDictionary.asset");

        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = petCostumeDb;
    }  

    [MenuItem("Assets/Create/Create Pet Category Icons")]
    public static void CreatePetCategoryIcon()
    {
        PetCategoryIcons petcategoryIconDB =  ScriptableObject.CreateInstance<PetCategoryIcons>();

        ProjectWindowUtil.CreateAsset(petcategoryIconDB, "Assets/Scripts/Scriptable Objects/PetCategoryIcons/PetCategoryIcon.asset");

        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = petcategoryIconDB;
    } 
}
