﻿using UnityEngine;
using System.Collections;
using UnityEditor;


public static class ScriptableEditorShoes
{
	[MenuItem("Assets/Create/Create Shoe Equipment")]
	public static void CreateDatabaseAsset()
	{

		ShoesDatabase shoesDb =  ScriptableObject.CreateInstance<ShoesDatabase>();

		ProjectWindowUtil.CreateAsset(shoesDb , "Assets/Scripts/Scriptable Objects/Equipment/Shoes/ShoeDatabase.asset");

		AssetDatabase.SaveAssets();

		EditorUtility.FocusProjectWindow();

		Selection.activeObject = shoesDb ;
	}
}