﻿using UnityEngine;
using System.Collections;
using UnityEditor;


public  class DogAsset
{
    [MenuItem("Assets/Create/Create Dog Asset")]
    public static void CreateDatabaseAsset()
    {
        DogDatabase dogDb =  ScriptableObject.CreateInstance<DogDatabase>();

        ProjectWindowUtil.CreateAsset(dogDb, "Assets/Scripts/Scriptable Objects/Pets/PetDatabase.asset");

		AssetDatabase.SaveAssets();

		EditorUtility.FocusProjectWindow();

        Selection.activeObject = dogDb;
    }
}