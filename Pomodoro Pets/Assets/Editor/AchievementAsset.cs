﻿
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEditor;

	public class AchievementAsset {

		[MenuItem("Assets/Create/Create Achievement Asset")]
		public static void CreateDatabaseAsset()
		{
		AchievementDatabase achDb =  ScriptableObject.CreateInstance<AchievementDatabase>();

		ProjectWindowUtil.CreateAsset(achDb, "Assets/Scripts/Scriptable Objects/Achievements/AchievementDatabase.asset");

			AssetDatabase.SaveAssets();

			EditorUtility.FocusProjectWindow();

		Selection.activeObject = achDb;
		}
	}
