﻿using UnityEngine;
using System.Collections;
using UnityEditor;


public  class ScriptableObjectTask
{
	[MenuItem("Assets/Create/Create Task")]
	public static void CreateTaskAsset()
	{
		TaskDatabase taskdb =  ScriptableObject.CreateInstance<TaskDatabase>();

		ProjectWindowUtil.CreateAsset(taskdb, "Assets/Scripts/Scriptable Objects/Task/TaskDatabase.asset");

		AssetDatabase.SaveAssets();

		EditorUtility.FocusProjectWindow();

		Selection.activeObject = taskdb;
	}
}