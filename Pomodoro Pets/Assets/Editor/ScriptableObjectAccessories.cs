﻿using UnityEngine;
using System.Collections;
using UnityEditor;


public static class ScriptableObjectAccessories
{
	[MenuItem("Assets/Create/Create Accessories Equipment")]
	public static void CreateDatabaseAsset()
	{

		AcceDatabase accessor =  ScriptableObject.CreateInstance<AcceDatabase>();

		ProjectWindowUtil.CreateAsset(accessor , "Assets/Scripts/Scriptable Objects/Equipment/Accessories/AccessoriesDatabase.asset");

		AssetDatabase.SaveAssets();

		EditorUtility.FocusProjectWindow();

		Selection.activeObject = accessor ;
	}
}