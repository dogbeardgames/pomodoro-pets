﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowPanelPiggyPopup : MonoBehaviour {

	[SerializeField]
	Button hammerNormal, hammerSilver,hammerGold;

	[SerializeField]

	GameObject normalPopup,silverPopup,goldPopup;

	public void InitializeButton()
	{
		hammerNormal.GetComponent<Button>().onClick.AddListener(() => ShowNormalHammer());	
		hammerSilver.GetComponent<Button>().onClick.AddListener(() => ShowNormalHammer());	
		hammerGold.GetComponent<Button>().onClick.AddListener(() => ShowNormalHammer());	
	}


	void ShowSilverHammer()
	{
		silverPopup.gameObject.SetActive (true);
	}

	void ShowGoldHammer()
	{
		goldPopup.gameObject.SetActive (true);
	}

	void ShowNormalHammer()
	{
		normalPopup.gameObject.SetActive (true);
	}
}
