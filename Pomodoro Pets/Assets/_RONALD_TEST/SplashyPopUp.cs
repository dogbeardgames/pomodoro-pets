﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SplashyPopUp : MonoBehaviour {

	// Use this for initialization


	void Start () {
		Splash ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnEnable()
	{
		Splash ();
	}


	void Splash()
	{
		transform.localScale = new Vector3 (0,0,0);
		transform.DOScale (1.2f, 0.2f).OnComplete(()=> NormalScale());
	}

	void NormalScale()
	{
		transform.DOScale (1f, 0.1f);
	}
}
