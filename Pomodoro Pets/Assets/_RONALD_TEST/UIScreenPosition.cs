﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScreenPosition : MonoBehaviour {

	// Use this for initialization

	void Start () {
		float x = (Screen.width * 0.5f) * -1f;
		//float x = Screen.width;
		transform.localPosition = new Vector2 (x, 0f);	
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
