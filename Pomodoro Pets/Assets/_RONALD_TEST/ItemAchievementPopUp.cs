﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class ItemAchievementPopUp : MonoBehaviour {


	public TextMeshProUGUI txtContent;
	public RectTransform rect;
//	[SerializeField]
//	TextMeshProUGUI txtTitle;
//	[SerializeField]
//	Image icon;

	[SerializeField]
	float height;
	Vector2 size = Vector2.zero;
	Vector2 originalSize;
	[SerializeField]
	bool animating = false;

	[SerializeField]
	bool shown;
	void Awake()
	{

	}

	void OnEnable()
	{
		//rect.sizeDelta = originalSize;
	}
	void OnDisable()
	{
		rect.sizeDelta = originalSize;
		animating = false;
		shown = false;
		transform.parent.GetComponent<Image> ().enabled = true;
	}

	void Start()
	{
		//txtTitle.alignment = TextAlignmentOptions.Center;
		//txtTitle.alignment = TextAlignmentOptions.Baseline;
		originalSize = new Vector2 (rect.sizeDelta.x, 0f);
	
	}

	public void StrecthVertical(float px)
	{
		px += 5f;
		height += px + 5f;
		//rect.sizeDelta = new Vector2 (rect.sizeDelta.x, rect.sizeDelta.y + px);
		//transform.localPosition = new Vector2 (transform.localPosition.x, transform.localPosition.y - (px/2));
		//print("stretch here");
	}

	public void AnimateReverse()
	{
		if (!animating && shown) {
			size = originalSize;
			animating = true;
			rect.DOSizeDelta(size,0.2f).OnComplete(() => Hide());

		}
	}

	public void Hide()
	{
		animating = false;
		shown = false;
		gameObject.SetActive(false);
		transform.parent.GetComponent<Image> ().enabled = true;
		transform.parent.GetChild (1).gameObject.SetActive (false);
	}

	public void Animate()
	{
		if (!animating && !shown) {
			transform.parent.GetChild (1).gameObject.SetActive (true);
			transform.parent.GetComponent<Image> ().enabled = false;
			size = new Vector2 (rect.sizeDelta.x, rect.sizeDelta.y + height);
			animating = true;
			rect.DOSizeDelta(size,0.2f).OnComplete(() => animating = false);
			shown = true;
		}
	
	}

	public void SetPopUp(string title, Sprite iconStateSprite)
	{
		//txtTitle.text = title;
		//icon.sprite = iconStateSprite;
		//print("WHAT THE WORLD!");
	}



}


#region FAIL
//public class ItemAchievementPopUp : MonoBehaviour {
//
//	RectTransform rectTrans;
//	[SerializeField]
//	float maxH = 590f;
//	[SerializeField]
//	Vector2 startPos;
//
//	Vector2 minSize;
//	void Awake()
//	{
//
//	}
//
//	void Start()
//	{
//		rectTrans = GetComponent<RectTransform> ();
//		minSize = new Vector2 (rectTrans.sizeDelta.x, 102.5f);
//		rectTrans.sizeDelta = minSize;
//		//Expand ();
//		Invoke("Show",2f);
//		//Show();
//	}
//	//go up
//	//expand down
//	void OnEnable()
//	{
//		//Expand ();
//	}
//
//	void OnDisable()
//	{
//		rectTrans.sizeDelta = minSize;
//		Invoke("Show",2f);
//	}
//
//
//	void Show()
//	{
//		transform.DOLocalMoveY (startPos.y, 0.5f).OnComplete(() => Expand());
//	}
//
//	void Expand()
//	{
//		StartCoroutine (IEExpand());
//
//	}
//
//	IEnumerator IEExpand()
//	{
//		float h = 0;
//		float y = startPos.y;
//		for(int i=0;i<100; i++){
//			h = Mathf.Lerp (h,maxH,0.2f);
//			y = Mathf.Lerp (y,14f,0.2f );
//			transform.localPosition = new Vector2 (transform.localPosition.x, y);
//			rectTrans.sizeDelta = new Vector2 (rectTrans.sizeDelta.x, h);
//			yield return new WaitForSeconds (0.02f);
//		}
//	}
//}

#endregion FAIL
