﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Framework;

public class ButtonMuteApp : MonoBehaviour {

	[SerializeField]
	bool mute=false;

	[SerializeField]
	Sprite sprtActive, sprtMute;

	Image img;

	void Awake()
	{
		img = GetComponent<Image>();
	}

	void Start () {
		CheckState();
	}
	
	void OnEnable()
	{
		CheckState();
	}
	

	public void Click()
	{
		if(mute){
			mute = false;
			SoundManager.Instance.UnmuteSFX ();
			SoundManager.Instance.UnmuteInsideBGM ();
			SoundManager.Instance.UnmuteOutsideBGM ();
			//SoundManager.Instance.
		}
		else{
			mute = true;
			SoundManager.Instance.MuteSFX ();
			SoundManager.Instance.MuteInsideBGM ();
			SoundManager.Instance.MuteOutsideBGM();
		}
		CheckState();
	}
	
	void CheckState()
	{
		if(mute)
		{
			img.sprite = sprtMute;
		}	
		else
		{
			img.sprite = sprtActive;
		}
	}

}
