﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataUILogger : MonoBehaviour {

	[SerializeField]
	Text txtLog;

	int logIndex, logIndexCntr;

	void Start () {
		InvokeRepeating ("Log",1f,0.2f);	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void LogNext()
	{
		logIndex++;
		if(logIndex > 1)
		{
			logIndex = 0;
		}

	}

	public void Show()
	{
		if (txtLog.gameObject.activeInHierarchy) {
			txtLog.gameObject.SetActive (false);
			GetComponent<Image> ().enabled = false;
		} else {
			txtLog.gameObject.SetActive (true);
			GetComponent<Image> ().enabled = true;
		}
	}

	void Log()
	{
		if (logIndex == 0) {
			txtLog.text = "DATA LOGGER\n\n" + "\nDC isnewInstalled" + DataController.Instance.playerData.isnewInstalled +
				"\nDC isPetHome: " + DataController.Instance.playerData.ispetHome +
				"\nDC isPaused: " + DataController.Instance.playerData.isPaused +
				"\nDC isTimerRunning: " + DataController.Instance.playerData.isTimerRunning +
				"\nDC isFromIndoor: " + DataController.Instance.playerData.isfromIndoor +
				"\nDC state: " + DataController.Instance.playerData.state +
				"\nDC closingState: " + DataController.Instance.playerData.closingState +
				"\nPomoUI isMenuActive: " + PomodoroUI.Instance.isMenuActive +
				"\nPomoUI isThereActiveDialog: " + PomodoroUI.Instance.thereIsActiveDialog;
		}
		else if(logIndex >= 1)
		{
			txtLog.text = "TASK DATA LOGGER\n\n"+TaskData ();
		}
		
	}

	string TaskData()
	{
		string _str = "";

		for(int i=0; i<TaskDataController.Instance.completedTaskData.taskItem.Count; i++)
		{
				_str += "\nTD TName" + TaskDataController.Instance.completedTaskData.taskItem[i].task + 
					"\nTD TCatIndex" + TaskDataController.Instance.completedTaskData.taskItem[i].categoryIndex +
					"\nTD TDateFin" + TaskDataController.Instance.completedTaskData.taskItem[i].dateFinished;
		}
	
		return _str;

		//task name, cat index, datafin
	}


}
