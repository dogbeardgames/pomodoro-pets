﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollviewContentValueStarter : MonoBehaviour {
	//use this if scrollrect gives headache

	[SerializeField]
	float scrollValue = 1f;

	void Start () {
		if(GetComponent<ScrollRect> ().horizontal)
		GetComponent<ScrollRect> ().horizontalNormalizedPosition = scrollValue;	
		else if(GetComponent<ScrollRect> ().vertical)
			GetComponent<ScrollRect> ().verticalNormalizedPosition = scrollValue;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
