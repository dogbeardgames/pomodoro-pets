﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CoroutineUtilities
{
	public delegate void OnCompleteEventHandler();

	public delegate void OnLoopEventHandler (int cntr);



	public static IEnumerator WaitForRealTime(float delay, OnCompleteEventHandler onComplete){
		
		while(true)
		{
			float pauseEndTime = Time.realtimeSinceStartup + delay;
			while (Time.realtimeSinceStartup < pauseEndTime)
			{
				//Debug.Log ("HOOLER");
				yield return 0;
			}

			if (onComplete != null) 
			{
				onComplete ();
			}
			break;
		}
	}

	//trying real time for loop? refer to StaticCoroutinec.cs

	#region FAIL REALTIME FOR LOOP ------------------------------------------------------------------------
//	public static void RealTime_ForLoop()
//	{
//		//StaticCoroutine.lo
//	}
//
//	static void Count(int cntr, int loops)
//	{
//		if (cntr < loops) {
//			Debug.Log(cntr);
//			cntr++;
//			Count (cntr, loops);
//		}
//
//
//	}
//	public static IEnumerator WaitForRealTime_ForLoop(float delay, int numOfLoop, OnLoopEventHandler onLoop ){
//		//recursion
//		int loopCntr=0;
//		while(true)
//		{
//			float pauseEndTime = Time.realtimeSinceStartup + delay;
//			while (Time.realtimeSinceStartup < pauseEndTime && loopCntr < numOfLoop)
//			{
//				//DELAY HAPPENED HERE
//				Debug.Log ("wow");
//				//yield return 0;
//			}
//			Count (loopCntr++, numOfLoop);
//			break;
//		}
//		
//
//
//		yield return 0;
//	}
	#endregion fail


}


