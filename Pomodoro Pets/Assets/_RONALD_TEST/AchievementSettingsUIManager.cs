﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementSettingsUIManager : MonoBehaviour {

	public static AchievementSettingsUIManager ins;

	public List<AchievementDescriptionPopup> lstItemDesc;
	[SerializeField]
	GameObject piggyBank;
	GameObject piggyBankRef;
	[SerializeField]
	Transform piggyBankPosition;

	public GameObject PiggyBank{
		get{ return piggyBankRef; }
		set{ piggyBankRef = value;}
	}

	void Start()
	{
		ins = this;
		piggyBank.GetComponent<SetPositionToUIPosition> ().SetTarget (piggyBankPosition, false);
		piggyBankRef =  (GameObject)Instantiate (piggyBank);


		//disable screen dragging in pethome screen

			DataController.Instance.playerData.enableDrag = false;


	}



	public void HidePopUps()
	{
		for(int i=0; i<lstItemDesc.Count; i++)
		{
			lstItemDesc [i].PopUp.Hide();
			lstItemDesc [i].PopUp.gameObject.SetActive (false);
		}
	}

	void OnEnable()
	{
		if (piggyBankRef != null) {
			piggyBankRef.SetActive (true);
		}

	}

	void OnDisable()
	{
		PomodoroUI.Instance.isMenuActive = false;
		if (piggyBankRef != null) {
			piggyBankRef.SetActive (false);
		}
		//enable dragging to pethome screen
		DataController.Instance.playerData.enableDrag = true;
	}
		
}
