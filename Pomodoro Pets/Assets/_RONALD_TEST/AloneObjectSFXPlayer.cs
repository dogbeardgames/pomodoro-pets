﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class AloneObjectSFXPlayer : MonoBehaviour, IPointerClickHandler {
	[SerializeField]
	bool playOnShow;
	[SerializeField]
	bool objectIsAToggle;


	// Use this for initialization
	[SerializeField]
	string sfxName;
	void Start () {
		if (playOnShow)
			PlaySFX ();	
	}

	void Enable()
	{
		if (playOnShow)
			PlaySFX ();
	}
	// Update is called once per frame
	void Update () {
		
	}

	public void PlaySFX()
	{
		if(sfxName == "")
			Framework.SoundManager.Instance.PlaySFX("seIconTapPomodoro");
		else
			Framework.SoundManager.Instance.PlaySFX(sfxName);
	}

	public void OnPointerClick(PointerEventData data)
	{
		if(objectIsAToggle)
		{
			PlaySFX ();
		}
	}
}
