﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.UI;
public class PanButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

	//area of improvement "spring effect" or "stop delay"
	[SerializeField]
	GameObject opposite;

	[SerializeField]
	int leftRight;
	[SerializeField]
	ScrollRect scrollRect;


	public float panSpeed = 1;
	//bool holdingDown;

	public ScrollRect scrollViewRect{
		set{ scrollRect = value;}
	}

	[SerializeField]
	bool moving = false;


	void Start () {
		
	}

	void OnDestroy()
	{
		
	}
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate()
	{
		if (moving) {
			
			if (scrollRect.vertical) {
				if (leftRight == 0 && scrollRect.verticalNormalizedPosition > 0)
				{
					scrollRect.verticalNormalizedPosition -= panSpeed * Time.deltaTime;
					if (scrollRect.verticalNormalizedPosition < 0)
					{
						scrollRect.verticalNormalizedPosition = 0f;
					}
				
				} else if (leftRight == 1 && scrollRect.verticalNormalizedPosition < 1) 
				{
					scrollRect.verticalNormalizedPosition += panSpeed * Time.deltaTime;
					if (scrollRect.verticalNormalizedPosition > 1)
					{
						scrollRect.verticalNormalizedPosition = 1;
					}
				
				}
			} 
			else 
			{
				if (leftRight == 0 && scrollRect.horizontalNormalizedPosition > 0) 
				{
					scrollRect.horizontalNormalizedPosition -= panSpeed * Time.deltaTime;
					if(scrollRect.horizontalNormalizedPosition < 0)
					{
						scrollRect.horizontalNormalizedPosition = 0;
						opposite.SetActive (true);
						gameObject.SetActive (false);
					}

				}
				if (leftRight == 1 && scrollRect.horizontalNormalizedPosition < 1)
				{
					scrollRect.horizontalNormalizedPosition += panSpeed * Time.deltaTime;
					if(scrollRect.horizontalNormalizedPosition > 1)
					{
						scrollRect.horizontalNormalizedPosition = 1;
						opposite.SetActive (true);
						gameObject.SetActive (false);
					}
				
				} else {
					//print (">>>+" + scrollRect.horizontalNormalizedPosition);
				}
			}

		} 
	}
		

	public void OnPointerDown(PointerEventData eData)
	{
		moving = true;
	}

	void UpdateState()
	{
		if (leftRight == 0 || leftRight == 1) {
			//this is left
			if (scrollRect.vertical) {
				if (scrollRect.verticalNormalizedPosition <= 0 ||  scrollRect.verticalNormalizedPosition >= 1) {
					opposite.SetActive (true);
					gameObject.SetActive (false);
				} else {
					opposite.SetActive (true);
					gameObject.SetActive (true);
				}
			
			} else {
				if (scrollRect.horizontalNormalizedPosition <= 0 ||  scrollRect.horizontalNormalizedPosition >= 1) {
					opposite.SetActive (true);
					gameObject.SetActive (false);
				} else {
					opposite.SetActive (true);
					gameObject.SetActive (true);
				}
			}
		

		}
	}

	void OnDisable()
	{
		moving = false;
	}
		
	public void OnPointerUp(PointerEventData eData)
	{
		//DOTween.Kill (panTarget);
		moving = false;
		UpdateState ();

	}

}
