﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework;


public class ToggleMute : MonoBehaviour {

	//13/06/2017 SFX disabled
	public void Mute()
	{
		//SoundManager.Instance.MuteSFX ();

		//06/26/2017
//		SoundManager.Instance.MuteInsideBGM ();
//		SoundManager.Instance.MuteOutsideBGM();

		//DataController.Instance.settings.isBGMOn = false;
		//SettingsController.ins.TurnOnBGM (false);
	}

	public void UnMute()
	{
		//SoundManager.Instance.UnmuteSFX ();

		//06/26/2017
//		SoundManager.Instance.UnmuteInsideBGM ();
//		SoundManager.Instance.UnmuteOutsideBGM ();



		//DataController.Instance.settings.isBGMOn = true;
		//SettingsController.ins.TurnOnBGM (true);
		if(DataController.Instance.settings.isBGMOn && 
			(!SoundManager.Instance.BGMOutside.mute ||
			!SoundManager.Instance.BGMInside.mute))
		{
			SoundManager.Instance.BGMVolume (0);
			//SoundManager.Instance.SFXVolume (0);
		}
	}


}
