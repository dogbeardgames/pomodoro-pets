﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class StaticCoroutine: MonoBehaviour
{
	public static StaticCoroutine ins;

	void Start()
	{
		//this has to be on the scene
		ins = this;
	}

	public static IEnumerator WaitForRealTime_ForLoop(float delay,int loopCntr, int numOfLoop, CoroutineUtilities.OnLoopEventHandler onLoop ){
		//recursion

		while(true)
		{
			float pauseEndTime = Time.realtimeSinceStartup + delay;
			while (Time.realtimeSinceStartup < pauseEndTime && loopCntr < numOfLoop)
			{
				//DELAY HAPPENED HERE
				yield return 0;
			}
			//Debug.Log (loopCntr);
			loopCntr++;
			if(loopCntr < numOfLoop)
			StartForLoop (delay, loopCntr, numOfLoop, onLoop);
			break;
		}
		yield return 0;
	}

	public static IEnumerator WaitForRealTime_ForLoopReverse(float delay,int loopCntr, int endCnt, CoroutineUtilities.OnLoopEventHandler onLoop ){
		//recursion

		while(true)
		{
			float pauseEndTime = Time.realtimeSinceStartup + delay;
			while (Time.realtimeSinceStartup < pauseEndTime && loopCntr > endCnt)
			{
				//DELAY HAPPENED HERE
				yield return 0;
			}
			//Debug.Log (loopCntr);
			loopCntr--;
			if(loopCntr >= endCnt)
				StartForLoopReverse(delay, loopCntr, endCnt, onLoop);
			break;
		}
		yield return 0;
	}

	public static void StartForLoop(float delay,int loopCntr, int numOfLoop, CoroutineUtilities.OnLoopEventHandler onLoop)
	{
		//print ("for loop Coroutine in realtime");
		ins.StartCoroutine (WaitForRealTime_ForLoop(delay,loopCntr, numOfLoop, onLoop));
		if (onLoop != null) {
			onLoop (loopCntr);
		}
	}

	public static void StartForLoopReverse(float delay,int startCnt, int endCnt, CoroutineUtilities.OnLoopEventHandler onLoop)
	{
		//print ("for loop Coroutine in realtime");
		if (endCnt > startCnt) {
			//Debug.Log ("ERROR: endCnt must be less than equal to startCnt");
		} else {
			ins.StartCoroutine (WaitForRealTime_ForLoopReverse(delay,startCnt, endCnt, onLoop));
			if (onLoop != null) {
				onLoop (startCnt);
			}
		
		}

	}


}