﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetScrollViewValue : MonoBehaviour {

	[SerializeField]
	ScrollRect scrollrect;

	// Use this for initialization
	void Start () {
		
	}
	
	public void Set(float value)
	{
		if (scrollrect.horizontal) {
			scrollrect.horizontalNormalizedPosition = value;
		} else if(scrollrect.vertical) {
			scrollrect.verticalNormalizedPosition = value;
		}
	}
}
