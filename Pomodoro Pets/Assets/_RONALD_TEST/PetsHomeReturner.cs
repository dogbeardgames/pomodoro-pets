﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework;

public class PetsHomeReturner : MonoBehaviour {

	public static PetsHomeReturner retuner;
	public static bool isPetHome = false;
	// Use this for initialization
	void Start () {
		 

		if (DataController.Instance.playerData.ispetHome) {
			//Invoke ("ShowPetHome",0.2f);
			ShowPetHome ();
			PetHomeController.ins.isOnPetHome = true;	
		} else {
			//SoundManager.Instance.FadeOutOutside ();
			if(DataController.Instance.settings.isBGMOn)
			SoundManager.Instance.FadeOutInside ();

		}

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ShowPetHome()
	{
		PomodoroUI.Instance.ShowPetHome ();
	}
}
