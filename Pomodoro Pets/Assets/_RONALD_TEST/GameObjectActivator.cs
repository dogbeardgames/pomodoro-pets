﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectActivator : MonoBehaviour {

	[SerializeField]
	GameObject[] obj;
	[SerializeField]
	string[] objName;

	[SerializeField]
	GameObject[] objsToEnable;


	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Enable(bool b)
	{
		for(int i=0; i<obj.Length; i++)
		{
			obj [i].SetActive (b);
		}
		for(int i=0; i<objName.Length; i++)
		{
			if(GameObject.Find (objName [i]) != null)
			GameObject.Find (objName [i]).SetActive (b);
		}

		for(int i=0; i<objsToEnable.Length; i++)
		{
			if (b)
				objsToEnable [i].SetActive (false);
			else
				objsToEnable [i].SetActive (true);
		}
	}


}

