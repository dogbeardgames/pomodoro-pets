﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenshotParticleSystemManager : MonoBehaviour {
	public static bool swingMode;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Play()
	{
		if (PetUIController.ACTION == PetUIController.PET_ACTION.SPONGE || PetUIController.ACTION == PetUIController.PET_ACTION.HOSE) {
			ParticleSystem[] lstParticle = FindObjectsOfType (typeof(ParticleSystem)) as ParticleSystem[];
			for (int i = 0; i < lstParticle.Length; i++) {
				lstParticle [i].Play ();
			}
		}

	}

	public void Pause()
	{
		if(PetUIController.ACTION == PetUIController.PET_ACTION.SPONGE || PetUIController.ACTION == PetUIController.PET_ACTION.HOSE)
		{
			ParticleSystem[] lstParticle = FindObjectsOfType (typeof(ParticleSystem)) as ParticleSystem[];
			for (int i = 0; i < lstParticle.Length; i++) {
				lstParticle [i].Pause ();
			}
		}

	}
}
