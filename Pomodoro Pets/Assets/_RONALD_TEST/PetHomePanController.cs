﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PetHomePanController : MonoBehaviour {

	[SerializeField]
	int maxItems = 10;
	[SerializeField]
	PetHomePanButton left, right;
	[SerializeField]
	CustomScrollView scroll;

	[SerializeField]
	float maxLeft=0f, maxRight = 1f;
	uint enableScrolling = 0;

	public float getMaxLeft
	{
		get{ return maxLeft; }	
	}
	public float getMaxRight
	{
		get{ return maxRight; }	
	}

	public PetHomePanButton LeftPanButton{
		get{ return left;}
	}
	void Start () {
		UpdatePanButtons ();

		scroll.OnContentMove += UpdatePanButtons;
	
	}

	void OnEnable()
	{
		UpdatePanButtons ();
	}


	public void UpdatePanButtons()
	{
		if (scroll.Content.localPosition.x < scroll.MaxLeft - 2f) {
			//print ("A");
			left.gameObject.SetActive (true);
		} else {
			//print ("B");
			left.gameObject.SetActive (false);
		}

		if (scroll.Content.localPosition.x > scroll.MaxRight + 2f) {
			//print ("C");
			right.gameObject.SetActive (true);
		} else {
			//print ("D");
			right.gameObject.SetActive (false);
		}
	}
}
