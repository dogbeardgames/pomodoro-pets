﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
public class ScreenshotSkeletonAnimationTarget : MonoBehaviour {


	[SerializeField]
	string _tempAnimState;
	SkeletonAnimation anim;

	void Start () {
		anim = GetComponent <SkeletonAnimation> ();
	}

	// Update is called once per frame
	void Update () {
		
	}

	public void Pause()
	{
		//_tempAnimState = anim.AnimationName;
		//anim.AnimationName = "";
		anim.timeScale = 0;
		ThoughtBubble.enable = false;
	}


	public void Play()
	{
		//anim.AnimationName = _tempAnimState;
		anim.timeScale = 1;
		ThoughtBubble.enable = true;
	}
}
