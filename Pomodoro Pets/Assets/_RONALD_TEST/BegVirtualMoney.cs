﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BegVirtualMoney : MonoBehaviour {
	

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Piggy()
	{
		DataController.Instance.playerData.pigyBank += 10;
		if (DataController.Instance.playerData.pigyBank > 100) 
		{
			DataController.Instance.playerData.pigyBank = 100;
		}

	}

	public void Gold()
	{
		DataController.Instance.playerData.gold += 10;
		DataController.Instance.playerData.goldacquired += 10;
	}

	public void Gem()
	{
		DataController.Instance.playerData.gem += 10;
		DataController.Instance.playerData.gemacquired += 10;
	}

	public void ThousandGold(){
		DataController.Instance.playerData.gold += 1000;
		//DataController.Instance.playerData.goldacquired += 1000;
	}




//	public void BegBeagle()
//	{
//		PetBase petbase = new PetBase();
//		petbase.petName = "Beagle";
//		petbase.petBreed = "Beagle";
//		petbase.petType = EnumCollection.Pet.Dog;
//		petbase.petCategory = EnumCollection.PetCategory.Small;
//
//		DataController.Instance.playerData.playerPetList.Add(petbase);
//	}
//
//	public void BegAkita()
//	{
//		PetBase petbase = new PetBase();
//		petbase.petName = "Akita";
//		petbase.petBreed = "Akita";
//		petbase.petType = EnumCollection.Pet.Dog;
//		petbase.petCategory = EnumCollection.PetCategory.Big;
//
//		DataController.Instance.playerData.playerPetList.Add(petbase);
//	}
//
//	public void BegHusky()
//	{
//		PetBase petbase = new PetBase();
//		petbase.petName = "Husky";
//		petbase.petBreed = "Husky";
//		petbase.petType = EnumCollection.Pet.Dog;
//		petbase.petCategory = EnumCollection.PetCategory.Big;
//
//		DataController.Instance.playerData.playerPetList.Add(petbase);
//	}
//
//	public void BegPug()
//	{
//		PetBase petbase = new PetBase();
//		petbase.petName = "Pug";
//		petbase.petBreed = "Pug";
//		petbase.petType = EnumCollection.Pet.Dog;
//		petbase.petCategory = EnumCollection.PetCategory.Small;
//
//		DataController.Instance.playerData.playerPetList.Add(petbase);
//	}
//
//	public void BegRottweiler()
//	{
//		PetBase petbase = new PetBase();
//		petbase.petName = "Rottweiler";
//		petbase.petBreed = "Rottweiler";
//		petbase.petType = EnumCollection.Pet.Dog;
//		petbase.petCategory = EnumCollection.PetCategory.Big;
//
//		DataController.Instance.playerData.playerPetList.Add(petbase);
//	}
//
//	public void BegYorkie()
//	{
//		PetBase petbase = new PetBase();
//		petbase.petName = "Yorkie";
//		petbase.petBreed = "Yorkie";
//		petbase.petType = EnumCollection.Pet.Dog;
//		petbase.petCategory = EnumCollection.PetCategory.Small;
//
//		DataController.Instance.playerData.playerPetList.Add(petbase);
//	}
//
//	public void BegChowChow()
//	{
//		PetBase petbase = new PetBase();
//		petbase.petName = "Chow Chow";
//		petbase.petBreed = "Chow_Chow";
//		petbase.petType = EnumCollection.Pet.Dog;
//		petbase.petCategory = EnumCollection.PetCategory.Big;
//
//		DataController.Instance.playerData.playerPetList.Add(petbase);
//	}
//
//	public void BegPoodle()
//	{
//		PetBase petbase = new PetBase();
//		petbase.petName = "Poodle";
//		petbase.petBreed = "Poodle";
//		petbase.petType = EnumCollection.Pet.Dog;
//		petbase.petCategory = EnumCollection.PetCategory.Small;
//
//		DataController.Instance.playerData.playerPetList.Add(petbase);
//	}
//
//	public void BegCorgi()
//	{
//		PetBase petbase = new PetBase();
//		petbase.petName = "Corgi";
//		petbase.petBreed = "Corgi";
//		petbase.petType = EnumCollection.Pet.Dog;
//		petbase.petCategory = EnumCollection.PetCategory.Small;
//
//		DataController.Instance.playerData.playerPetList.Add(petbase);
//	}
//
//	//Cat -------------------------------------------------------------------------------------------------
//
//	public void BegPersian()
//	{
//		PetBase petbase = new PetBase();
//		petbase.petName = "Persian";
//		petbase.petBreed = "Persian";
//		petbase.petType = EnumCollection.Pet.Cat;
//		petbase.petCategory = EnumCollection.PetCategory.Hairy;
//
//		DataController.Instance.playerData.playerPetList.Add(petbase);
//	}
//
//	public void BegSiamese()
//	{
//		PetBase petbase = new PetBase();
//		petbase.petName = "Siamese";
//		petbase.petBreed = "Siamese";
//		petbase.petType = EnumCollection.Pet.Cat;
//		petbase.petCategory = EnumCollection.PetCategory.ShortHair;
//
//		DataController.Instance.playerData.playerPetList.Add(petbase);
//	}
//
//	public void BegMaineCoon()
//	{
//		PetBase petbase = new PetBase();
//		petbase.petName = "Maine Coon";
//		petbase.petBreed = "Maine_Coon";
//		petbase.petType = EnumCollection.Pet.Cat;
//		petbase.petCategory = EnumCollection.PetCategory.ShortHair;
//
//		DataController.Instance.playerData.playerPetList.Add(petbase);
//	}
//
//	public void BegBengal()
//	{
//		PetBase petbase = new PetBase();
//		petbase.petName = "Bengal";
//		petbase.petBreed = "Bengal";
//		petbase.petType = EnumCollection.Pet.Cat;
//		petbase.petCategory = EnumCollection.PetCategory.ShortHair;
//
//		DataController.Instance.playerData.playerPetList.Add(petbase);
//	}
//
//
//	public void BegSphynx()
//	{
//		PetBase petbase = new PetBase();
//		petbase.petName = "Sphynx";
//		petbase.petBreed = "Sphynx";
//		petbase.petType = EnumCollection.Pet.Cat;
//		petbase.petCategory = EnumCollection.PetCategory.ShortHair;
//
//		DataController.Instance.playerData.playerPetList.Add(petbase);
//	}
//
//	public void BegHimalayan()
//	{
//		PetBase petbase = new PetBase();
//		petbase.petName = "Himalayan";
//		petbase.petBreed = "Himalayan";
//		petbase.petType = EnumCollection.Pet.Cat;
//		petbase.petCategory = EnumCollection.PetCategory.Hairy;
//
//		DataController.Instance.playerData.playerPetList.Add(petbase);
//	}
//
//	public void BegMunchkin()
//	{
//		PetBase petbase = new PetBase();
//		petbase.petName = "Munchkin";
//		petbase.petBreed = "Munchkin";
//		petbase.petType = EnumCollection.Pet.Cat;
//		petbase.petCategory = EnumCollection.PetCategory.Hairy;
//
//		DataController.Instance.playerData.playerPetList.Add(petbase);
//	}
//
//	public void BegRagdoll()
//	{
//		PetBase petbase = new PetBase();
//		petbase.petName = "Ragdoll";
//		petbase.petBreed = "Ragdoll";
//		petbase.petType = EnumCollection.Pet.Cat;
//		petbase.petCategory = EnumCollection.PetCategory.ShortHair;
//
//		DataController.Instance.playerData.playerPetList.Add(petbase);
//	}
//
//	public void BegBombay()
//	{
//		PetBase petbase = new PetBase();
//		petbase.petName = "Bombay";
//		petbase.petBreed = "Bombay";
//		petbase.petType = EnumCollection.Pet.Cat;
//		petbase.petCategory = EnumCollection.PetCategory.ShortHair;
//
//		DataController.Instance.playerData.playerPetList.Add(petbase);
//	}
//
//
//
//
//
//	public void BegDog()
//	{
//		BegCorgi ();
//		BegPoodle ();
//		BegChowChow();
//		BegYorkie();
//		BegRottweiler();
//		BegPug();
//		BegHusky();
//		BegAkita();
//		BegBeagle();
//	}
//
//	public void BegCat()
//	{
//		BegBombay ();
//		BegRagdoll();
//		BegMunchkin();
//		BegHimalayan();
//		BegSphynx();
//		BegBengal();
//		BegMaineCoon();
//		BegSiamese();
//		BegPersian();
//	}


}
