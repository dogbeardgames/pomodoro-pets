﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AudioResources : MonoBehaviour {

	public List<AudioClip> audClipBGM;
	public List<AudioClip> audClipSFX;


	public virtual AudioClip GetBGMClip(string clipName)
	{
		for(int i=0; i<audClipBGM.Count; i++)
		{
			if (audClipBGM [i].name == clipName) {
				return audClipBGM [i];
			}
		}

		return null;
	}

	public virtual AudioClip GetSFXClip(string clipName)
	{
		for(int i=0; i<audClipSFX.Count; i++)
		{
			if (audClipSFX [i].name == clipName) {
				return audClipSFX [i];
			}
		}

		return null;
	}
}
