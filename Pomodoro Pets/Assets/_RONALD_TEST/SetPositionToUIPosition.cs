﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetPositionToUIPosition : MonoBehaviour {

	[SerializeField]
	Transform target;

	[SerializeField]
	bool targetAnchored;
	RectTransform rect;

	public void SetTarget(Transform t, bool anchored)
	{
		targetAnchored = anchored;
		target = t;
	}

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(target != null)
		{
			if (targetAnchored) 
			{
				if (rect == null) 
				{
					rect = target.GetComponent<RectTransform> ();
				
				} 
				else 
				{
					transform.position = rect.anchoredPosition;	
				}

			} 
			else
			{
				transform.position = target.position;

			}

		}

	}
}
