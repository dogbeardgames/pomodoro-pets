﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ScreenshotBlocker : MonoBehaviour {

	[SerializeField]
	string[] blockerNameToDestroy;

	[SerializeField]
	GameObject[] friendlyBlocker;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void EnableFriendlyBlocker(bool b)
	{
		if(PetUIController.ACTION == PetUIController.PET_ACTION.SWING || PetUIController.ACTION == PetUIController.PET_ACTION.SPA)
		{
			for(int i=0; i<friendlyBlocker.Length; i++)
			{
				friendlyBlocker [i].gameObject.SetActive (b);
			}
		}
	

		//Invoke ("LateDestroy", 0.02f);
	}

	void LateDestroy()
	{
		if (PetUIController.ACTION == PetUIController.PET_ACTION.SWING || PetUIController.ACTION == PetUIController.PET_ACTION.SPA) {
			for(int i=0;i<blockerNameToDestroy.Length; i++)
			{
				GameObject obj = GameObject.Find (blockerNameToDestroy[i]);
				Destroy (obj);
			}
		}

	}

	public void AddToSwingEvent()
	{
		//this is still attached
		//Invoke ("OnSwing",0.02f);
	}

	void OnSwing()
	{
		
		try
		{
			Button btn = GameObject.Find ("Swing").GetComponent<Button> ();
			btn.onClick.AddListener (delegate {
				ScreenshotParticleSystemManager.swingMode = true;
				EnableFriendlyBlocker(true);
				Invoke("OnSwingEnd", 10f);
			});
		}
		catch(Exception ex)
		{
			
		}

	}
		
	void OnSwingEnd()
	{
		
		EnableFriendlyBlocker(false);
	}
}
