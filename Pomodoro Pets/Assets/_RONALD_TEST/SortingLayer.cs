﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SortingLayer : MonoBehaviour {

	[SerializeField]
	string layerName;
	[SerializeField]
	int order;
	Renderer r;
	void Start () {
		r = GetComponent<Renderer> ();
		r.sortingLayerName = layerName;
		r.sortingOrder = order;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
