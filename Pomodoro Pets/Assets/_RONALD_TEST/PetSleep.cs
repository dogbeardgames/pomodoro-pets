﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;

public class PetSleep : MonoBehaviour, IPointerClickHandler {

	public bool isSleeping;

	DateTime currentDate;
	DateTime oldDate;
    [SerializeField]
    float maxTime;

	void Start () 
    {
//        if(DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].sleepDate != 0)
//        {
//            isSleeping = true;
//        }
	}
	
    #region METHODS

	public void OnPointerClick(PointerEventData eData)
	{
		PetUIController.Instance.ManualSleep();		
		if (isSleeping) 
		{
			//woke up			
			if (GetTimeSpan ().TotalSeconds > maxTime) 
			{
				//print ("YOU HAVE PLENTY OF SLEEP AF");
                SleepReward();
                SleepDone();
			} 
			else 
			{
				//print ("WOKE UP TOO EARLY?");
//                if(DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].petType == EnumCollection.Pet.Cat)
//                {
//                    FindObjectOfType<PetCostume>().ReApplyCatEquipment();
//                }
                SleepReward();
                SleepDone();
			}
		} 
		else 
		{
			//sleep
			isSleeping = true;
			DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].sleepDate = DateTime.Now.ToBinary ();
		}
	}

	public TimeSpan GetTimeSpan()
	{
		currentDate = System.DateTime.Now;


        long tempLong = Convert.ToInt64 (DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].sleepDate);

		DateTime oldDate = DateTime.FromBinary (tempLong);

		TimeSpan difference = currentDate.Subtract (oldDate);
        return difference;

	}

    // done sleeping
    public void SleepDone()
    {
        DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].sleepDate = 0;
        isSleeping = false;
    }

	public bool IsStillSleeping()
	{
		if(GetTimeSpan().TotalSeconds < maxTime &&
			DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].sleepDate != 0)
		{
			//print ("pet should be sleeping on start");

			return true;
		}
		//print ("span " + GetTimeSpan().TotalSeconds);
		return false;
	}       

    public void SleepReward()
    {        
        PetBase pet = DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex];
        if (pet.energy > 0)
        {
            PetActionReward petActionReward = FindObjectOfType<PetActionReward>();
            PetInteractionController petInteractionController = FindObjectOfType<PetInteractionController>();

            float totalEnery = SleepDurationCompute();

            petActionReward.UserDefinedGoldReward(GoldCompute());
            petInteractionController.FillEnergy(totalEnery);
            pet.totalEnergyDeducted += totalEnery;

            if (pet.totalEnergyDeducted >= 1)
            {
                pet.totalEnergyDeducted -= 1;
                FindObjectOfType<SpinePetMeter>().CallHeartAnimation();
            }
        }

        //Update Pet Meter
		if(!ScreenshotPetModuleManager.screenshotMode)
        FindObjectOfType<RotatePawIcon>().UpdatePetMeter();
    }

    float SleepDurationCompute()
    {
        // reward 
        float totalEnergyIncrease = Mathf.Clamp((float)GetTimeSpan().Seconds, 0f, maxTime) / (float)maxTime;
        //Debug.Log("<color=red>time " + GetTimeSpan().Seconds + ", energy " + totalEnergyIncrease + "</color>");
        return totalEnergyIncrease;
    }

    uint GoldCompute()
    {
        //Debug.Log("<color=red>gold " + (uint)Mathf.FloorToInt((Mathf.Clamp((float)GetTimeSpan().Seconds, 0f, maxTime) / maxTime) * 20) + "</color>");
        return (uint)Mathf.FloorToInt((Mathf.Clamp((float)GetTimeSpan().Seconds, 0f, maxTime) / maxTime) * 20);
    }

    /// <summary>
    /// Disable bottom buttons when pet is sleeping
    /// </summary>
    public void LockDragAndButtons()
    {
        // enable drag of screen for inside/outside change
        BackgroundDrag.canDragBG = false;

        // find tag PetBottomButton
        GameObject[] toggleButtons = GameObject.FindGameObjectsWithTag("PetBottomButton");
        // hide thought bubble
        FindObjectOfType<ThoughtBubbleController>().DisableThoughtBubble();

        for (int i = 0; i < toggleButtons.Length; i++)
        {
            toggleButtons[i].GetComponent<Toggle>().interactable = false;
        }
    }
    /// <summary>
    /// Disable bottom buttons when pet is not sleeping
    /// </summary>
    public void UnlockDragAndButtons()
    {
        // enable drag of screen for inside/outside change
        BackgroundDrag.canDragBG = true;

        // find tag PetBottomButton
        GameObject[] toggleButtons = GameObject.FindGameObjectsWithTag("PetBottomButton");
        // show thought bubble
        FindObjectOfType<ThoughtBubbleController>().EnableThoughtBubble();
        // Stop pet SFX
        PetsAudio.Instance.StopPetSFX();

        for (int i = 0; i < toggleButtons.Length; i++)
        {
            toggleButtons[i].GetComponent<Toggle>().interactable = true;
        }
    }

    #endregion
}
