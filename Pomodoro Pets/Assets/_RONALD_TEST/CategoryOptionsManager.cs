﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class CategoryOptionsManager : MonoBehaviour {

	[SerializeField]
	int index;
	public GameObject categoryMaintenance;

	Button btnYes, btnNo;
	Transform confirmationPanel;
	[SerializeField]
	Button btnDelete, btnEdit;

	CategoryButtonOptions btnOptions;
	bool categoryButtonOptionsShow;

	public int Index{
		set{ index = value;}
	}

	void Start () {
		btnEdit.onClick.AddListener(Edit);
		btnDelete.onClick.AddListener(Delete);
		GetComponent<Button> ().onClick.AddListener (HideOptions);
		btnOptions = GetComponent<CategoryButtonOptions> ();
		//DataController.Instance.playerData.enableDrag = false;
	}

	// Update is called once per frame
	void Update () {

	}

	public void HideOptionsButton()
	{
		if (!categoryButtonOptionsShow) {
			btnOptions.Hide ();
			//print ("hiding");
		}
		categoryButtonOptionsShow = false;
	}

	void HideOptions()
	{
		categoryButtonOptionsShow = true;
		CategoryPanel.ins.HideOptions ();
		//btnOptions.Show ();
	}

	#region METHODS


	private void Edit()
	{
		GameObject obj = Instantiate(categoryMaintenance, GameObject.Find("Canvas").transform, false);
		CategoryPanel.ins.selectedIndex = index;
		//print ("SELECTED INDEX");
		//WRONG INDEX!
		obj.GetComponent<CategoryMaintenance>().SetUpEditCategoryUI(CategoryPanel.ins.selectedIndex);
		obj.GetComponent<CategoryMaintenance>().SetMaintenanceStateEvent(CategoryPanel.MaintenanceState.Edit);
	}

	private void Delete()
	{        
		GameObject window = Instantiate (Resources.Load<GameObject>("Prefab/StoreDialog"),GameObject.Find("Canvas").transform, false);
		ShowDialog dialog = window.GetComponent <ShowDialog>();
		dialog.SetCaption ("Delete category?");
		dialog.SetConfirmCaption ("Yes");
		dialog.SetCancelCaption ("No");
		dialog.OnYesEvent += delegate {
			ConfirmDelete();
			Destroy(window);
		};
			
	}

	private void ConfirmDelete()
	{
		//Debug.Log("Confirm delete");


		//update task category index once task category is deleted.
		List<TaskItem> filteredTask = DataController.Instance.taskList.items.Where (x => x.categoryIndex == index).ToList ();

		for (int i = 0; i < filteredTask.Count; i++) {

			//Debug.Log ("Task List for this category" + filteredTask[i].task);
			filteredTask [i].categoryIndex = 0;
		}

		//Delete selected category
		DataController.Instance.categoryList.items.RemoveAt(index);
		CategoryPanel.ins.InitializeButtons ();

		GameObject window = Instantiate (Resources.Load<GameObject>("Prefab/StoreDialog"),GameObject.Find("Canvas").transform, false);
		ShowDialog dialog = window.GetComponent <ShowDialog>();
		dialog.SetCaption ("Category has been deleted.");
		dialog.SetConfirmCaption ("OK");
		dialog.DisableNO ();
		dialog.OnYesEvent += delegate {
			Destroy(window);
			//CategoryPanel.ins.setActive();
		};

	}

	private void CancelDelete()
	{
		Destroy(GameObject.Find("PopUp"));
	}
	#endregion
}
