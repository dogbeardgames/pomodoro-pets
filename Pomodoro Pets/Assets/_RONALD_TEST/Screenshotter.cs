﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Screenshotter : MonoBehaviour {
	public static Screenshotter ins;

	[SerializeField]
	private Camera _camera;
	[SerializeField]
	Image outputImage;
	private Texture2D _screenShot;

	public Texture2D GetScreenshotOutPut()
	{
		return _screenShot;
	}


	// Use this for initialization
	void Start () {
		ins = this;	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Screenshot()
	{
		StartCoroutine (TakeScreenShot());
	}

	private IEnumerator TakeScreenShot()
	{
		yield return new WaitForEndOfFrame();

		RenderTexture rt = new RenderTexture(Screen.currentResolution.width, Screen.currentResolution.height, 24);
		_camera.targetTexture = rt;
		_screenShot= new Texture2D(Screen.currentResolution.width, Screen.currentResolution.height, TextureFormat.RGB24, false);
		_camera.Render();
		RenderTexture.active = rt;
		_screenShot.ReadPixels(new Rect(0, 0, Screen.currentResolution.width, Screen.currentResolution.height), 0, 0);
		_screenShot.Apply(); 
		_camera.targetTexture = null;
		RenderTexture.active = null;
		Destroy(rt);

		//string filename = ScreenShotName(Screen.currentResolution.width, Screen.currentResolution.height);

		//byte[] bytes = _screenShot.EncodeToPNG();
		//System.IO.File.WriteAllBytes(filename, bytes);

		//Debug.Log(string.Format("Took screenshot to: {0}", filename));

		Sprite tempSprite = Sprite.Create(_screenShot,new Rect(0,0,Screen.currentResolution.width, Screen.currentResolution.height),new Vector2(0,0));
		outputImage.raycastTarget = false;
		outputImage.gameObject.SetActive (true);
		outputImage.sprite = tempSprite;
		outputImage.preserveAspect = true;
	}

}
