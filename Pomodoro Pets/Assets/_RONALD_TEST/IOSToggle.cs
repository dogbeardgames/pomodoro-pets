﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class IOSToggle : MonoBehaviour, IPointerClickHandler {

	//add BGM and vibration toggler to data state

	bool interactible = true;
	[SerializeField]
	bool isOn = false;
	[SerializeField]
	Transform toggleObject;

	[SerializeField]
	Image offImage, onImage;


	public UnityEvent onOff;

	public UnityEvent onOn;

	bool moving = false;
	Vector2 off, on;

	RectTransform onRect;
	RectTransform offRect;

	Vector2 fillRectMaxSize = new Vector2(55f, 60f);
	Vector2 fillRectMinSize = new Vector2(0f, 60f);

	public bool Interactible{
		set{ 
			interactible = value; 
			InteractibleToggleAlpha ();
		}
		get{ 
			return interactible;
		}
	}

	public bool On{
		set{ isOn = value;}
		get{ return isOn;}
	}

	void InteractibleToggleAlpha()
	{
		Image img;
		Color a;
		if (interactible) {
			for(int i=0; i<transform.childCount; i++)
			{
				if(transform.GetChild(i).GetComponent<Image>() != null)
				{
					img = transform.GetChild (i).GetComponent<Image> ();
					a = img.color;
					a.a = 1f;
					img.color = a;
				}
			}

		}
		else {
			for(int i=0; i<transform.childCount; i++)
			{
				if(transform.GetChild(i).GetComponent<Image>() != null)
				{
					img = transform.GetChild (i).GetComponent<Image> ();
					a = img.color;
					a.a = 0.5f;
					img.color = a;
				}
			}

		}
	}

	void Awake()
	{
		

		off = toggleObject.localPosition;
		on = toggleObject.localPosition * -1f;
		onRect = offImage.gameObject.GetComponent<RectTransform> ();
		offRect = onImage.gameObject.GetComponent<RectTransform> ();
		moving = false;

	}

	void OnEnable()
	{
		CheckState ();
	}
	void Start()
	{
		CheckState ();	
		InteractibleToggleAlpha ();
	}


	void CheckState()
	{
		if (isOn) {
			toggleObject.DOLocalMoveX (on.x, 0.01f);//.OnComplete(() => SetState(onRect, false, fillRectMaxSize));
			onImage.gameObject.transform.SetSiblingIndex (1);
			offImage.gameObject.transform.SetSiblingIndex (0);
			SetState (offRect, true, fillRectMaxSize, 0.01f);
			if (onOn != null) {
				onOn.Invoke ();
			}
		}
		else {
			toggleObject.DOLocalMoveX (off.x, 0.01f);//.OnComplete(() => SetState(offRect, false, fillRectMaxSize));
			onImage.gameObject.transform.SetSiblingIndex (0);
			offImage.gameObject.transform.SetSiblingIndex (1);
			SetState (onRect, true, fillRectMaxSize, 0.01f);
			if (onOff != null) {
				onOff.Invoke ();
			}
		}
	}

	void Click()
	{
		if (!moving && interactible) {
			moving = true;

			if (isOn) {
				
				toggleObject.DOLocalMoveX (off.x, 0.5f);//.OnComplete(() => SetState(offGraphicState, true));
				isOn = false;
				onImage.gameObject.transform.SetSiblingIndex (0);
				offImage.gameObject.transform.SetSiblingIndex (1);
				SetState (onRect, true, fillRectMaxSize, 0.5f);

				if (onOff != null) {
					onOff.Invoke ();
				}


			} else {
				
				toggleObject.DOLocalMoveX (on.x, 0.5f);//.OnComplete(() => SetState(onGraphicState, true));
				isOn = true;
				onImage.gameObject.transform.SetSiblingIndex (1);
				offImage.gameObject.transform.SetSiblingIndex (0);
				SetState (offRect, true, fillRectMaxSize, 0.5f);


				if (onOn != null) {
					onOn.Invoke ();
				}
					

			}
		}

	}
		

	void SetState(RectTransform rect,bool animated, Vector2 size, float duration)
	{
		
		if (animated) {
			rect.sizeDelta = fillRectMinSize;
			rect.DOSizeDelta (size, duration).OnComplete (() => moving = false);
		} 

		//grow image---------------
		//moving = false;
	}



	public void OnPointerClick(PointerEventData eventData) // 3
	{
		//print ("CLICK");
		Click ();
	}




	public void SETON()
	{
		//print ("TEST ON");
	}

	public void SETOFF()
	{
		//print ("TEST OFF");
	}

}
