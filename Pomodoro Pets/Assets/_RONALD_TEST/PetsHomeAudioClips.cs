﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetsHomeAudioClips : AudioResources {
	public static PetsHomeAudioClips ins; 

	void Awake()
	{
		ins = this;
	}

	void Start()
	{
		
	}

	public override AudioClip GetBGMClip (string clipName)
	{
		return base.GetBGMClip (clipName);
	}

	public override AudioClip GetSFXClip (string clipName)
	{
		return base.GetSFXClip (clipName);
	}
}
