﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class ItemAchievement : MonoBehaviour {

	public bool isDone;
	[SerializeField]
	Image itemBGImg;
	Transform t ;

	void Awake()
	{


	}

	void Start () {
			
	}
		
	// Update is called once per frame
	void Update () {
		
	}

	void ChangeDescriptionUI()
	{
		//transform.GetChild (0).GetChild (0).transform.GetComponent<Image>().sprite = itemBGImg.sprite;
		transform.GetChild (1).transform.GetComponent<Image> ().color = itemBGImg.color;
	}

	void OnEnable()
	{
		t= transform.Find ("reward_group").transform;
		int n = t.childCount;
		List<AchievementItem> achievementItem = DataController.Instance.achievementDatabase.items.Where  
			(x => x.achievementId == this.gameObject.name).ToList ();

		if (achievementItem [0].isUnlocked == true) {
			//itemBGImg.sprite = bgDone;
			//statusIconImg.sprite = iconDone;
			itemBGImg.color = new Color32(150,150,150,255);
			for(int i=0; i<n; i++)
			{
				if (t.GetChild(i).gameObject.activeInHierarchy) {
					t.GetChild(i).GetComponent<Image> ().color = itemBGImg.color;
				}
			}

		
		} else {
			//itemBGImg.sprite = defaultBG;
			//statusIconImg.sprite = defaultIcon;
			itemBGImg.color = Color.white;
			for(int i=0; i<n; i++)
			{
				if (t.GetChild(i).gameObject.activeInHierarchy) {
					t.GetChild(i).GetComponent<Image> ().color = itemBGImg.color;
				}
			}
		}
		ChangeDescriptionUI ();
	}
}
