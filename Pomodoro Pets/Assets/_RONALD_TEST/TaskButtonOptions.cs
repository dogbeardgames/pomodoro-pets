﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskButtonOptions : MonoBehaviour {

	[SerializeField]
	GameObject[] button;
	[SerializeField]
	TaskItem taskItem;
	[SerializeField]
	TaskButton taskButton;

	// Use this for initialization
	bool shown, animating;
	int _len= 0;
	public void SetTaskItem(TaskItem item)
	{
		taskItem = item;
	}
	void Start () {
	}

	// Update is called once per frame
	void Update () {
	}

	public void Hide()
	{
		shown = false;
		for (int i = 0; i < button.Length; i++) {
			button [i].SetActive (false);
		}

	}

	public void SetShown(bool b)
	{
		shown = b;
	
	}

	IEnumerator IEShowOptions(bool b)
	{
		shown = b;
		if (b) {
			_len = button.Length;
			if (!taskItem.isDone) {
				_len -= 1;
				//print ("FANNY!");
			}
			if (DataController.Instance.playerData.isTimerRunning && !taskItem.isDone) {
				
				button [1].SetActive (false);
				button [0].SetActive (true);
				button [2].SetActive (true);
			} else {
				for (int i = 0; i < _len; i++) {
					yield return new WaitForSeconds (0.05f);
					//print("YOOOOHH");
					button [i].SetActive (b);
				}
			}



		} else {
			_len = button.Length;

			//disables edit
			if (!taskItem.isDone)
				_len -= 2;
			for (int i = button.Length-1; i >= 0; i--) {
				yield return new WaitForSeconds (0.05f);
				button [i].SetActive (b);
			}

		}

		animating = false;
	}

	public void Show()
	{
		if (!animating) 
		{
			//taskButton.SetUITask (taskItem);
			if (!shown) {
				StartCoroutine (IEShowOptions (true));
			} else {
				StartCoroutine (IEShowOptions(false));
			}
			Framework.SoundManager.Instance.PlaySFX("seIconTapPomodoro");

		}
	}
}
