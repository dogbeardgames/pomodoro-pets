﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpineMeshEnabler : MonoBehaviour {
	public static SpineMeshEnabler ins;

	public string spineName;
	[SerializeField]
	MeshRenderer spineMesh;
	[SerializeField]
	bool enable;

	[SerializeField]
	bool activeOnUpdate;
	void Awake()
	{
		ins = this;
	}

	void Start () {
		
	}

	void Update()
	{
		if(activeOnUpdate)
		{
			if (spineMesh != null)
				spineMesh.enabled = enable;
			else
				EnableMeshRenderer (enable);
		}
	}
	public void ActiveOnUpdate(bool b)
	{
		activeOnUpdate = b;
	}
	public void EnableMeshRenderer(bool b)
	{
		//print ("wasdadasdasdasdas");
		if (spineMesh == null) {
			//print ("a");
			if (GameObject.Find (spineName) != null) {
				spineMesh = GameObject.Find (spineName).GetComponent<MeshRenderer> ();//.enabled = b;	
				spineMesh.enabled = b;
				enable = b;
				//print ("b");
			}
		} else {
			spineMesh.enabled = b;
			enable = b;

			//print ("c");
		}


	}

	public void EnableMesh()
	{
		
		EnableMeshRenderer(true);
	}
}
