﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BirdGiftDialog : MonoBehaviour {

	private string m_caption;
	private string m_confirm, m_cancel;

	[SerializeField]
	Button btnDouble, btnNo, btnYes;
	[SerializeField]
	TextMeshProUGUI txtCaption, txtReward;
	// Use this for initialization
	[SerializeField]
	Sprite[] icon;
	[SerializeField]
	Image imgReward;
	void Start () {

		//btnYes = transform.Find("btnYes").GetComponent<Button>();

		btnDouble.onClick.AddListener(DoubleReward);
		btnNo.onClick.AddListener(No);
		btnYes.onClick.AddListener (Yes);
	}

	#region PUBLIC METHODS
	/// <summary>
	/// Sets the caption.
	/// </summary>
	/// 
	public void SetTextReward(string text, int icondIndex)
	{
		txtReward.text = text;
		imgReward.sprite = icon [icondIndex];
	}
	public void SetCaption(string caption)
	{
		txtCaption.text = caption;
	}
	/// <summary>
	/// Sets the confirm caption.
	/// </summary>
	public void SetConfirmCaption(string caption)
	{
		btnYes.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = caption;
	}

	public void SetCancelCaption(string caption)
	{
		btnNo.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = caption;
	}

	public void DisableNO()
	{
		btnYes.gameObject.SetActive(true);
		btnNo.gameObject.SetActive (false);
		btnDouble.gameObject.SetActive (false);
	}
	#endregion

	#region PRIVATE METHODS
	void DoubleReward()
	{
		if (OnDoubleEvent != null)
			OnDoubleEvent();        
	}       

	void Yes()
	{
		if (OnYesEvent != null)
			OnYesEvent();
	}

	void No()
	{
		if (OnNoEvent != null)
			OnNoEvent();
	}       
	#endregion

	#region Events
	public delegate void PopUp();
	public event PopUp OnDoubleEvent;
	public event PopUp OnNoEvent;
	public event PopUp OnYesEvent;
	#endregion
}
