﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SetTextColor : MonoBehaviour {

	[SerializeField]
	Color fontColor;

	void Start () {
		GetComponent<TextMeshProUGUI> ().color = fontColor;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
