﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CategoryButtonOptions : MonoBehaviour {


	[SerializeField]
	GameObject[] button;

	[SerializeField]
	CategoryButton CatButton;



	// Use this for initialization
	bool shown, animating;
	int _len= 0;
	public void SetTaskItem(TaskItem item)
	{
		
	}
	void Start () {
	}

	// Update is called once per frame
	void Update () {
	}

	public void Hide()
	{
		shown = false;
		for (int i = 0; i < button.Length; i++) {
			button [i].SetActive (false);
		}

	}

	public void SetShown(bool b)
	{
		shown = b;
	}

	IEnumerator IEShowOptions(bool b)
	{
		shown = b;
		_len = button.Length;
		if (b) {
			for (int i = 0; i < _len; i++) {
				yield return new WaitForSeconds (0.05f);
				button [i].SetActive (b);
				if(CatButton.locked && i==2)
				button [i].SetActive (false);
				
			}
		} else {
			for (int i = button.Length-1; i >= 0; i--) {
				yield return new WaitForSeconds (0.05f);
				button [i].SetActive (b);
			}
		}

		animating = false;
	}

	public void Show()
	{
		if (!animating) 
		{
			if (!shown) {
				StartCoroutine (IEShowOptions (true));
			} else {
				StartCoroutine (IEShowOptions(false));
			}
			Framework.SoundManager.Instance.PlaySFX("seIconTapPomodoro");
		}
	}
}
