﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanController : MonoBehaviour {

	[SerializeField]
	int maxItems = 10;
	[SerializeField]
	PanButton left, right;
	[SerializeField]
	ScrollRect scrollRect;

	[SerializeField]
	float maxLeft=0f, maxRight = 1f;
	uint enableScrolling = 0;

	public float getMaxLeft
	{
		get{ return maxLeft; }	
	}
	public float getMaxRight
	{
		get{ return maxRight; }	
	}
	void Start () {
		UpdatePanButtons ();
		scrollRect.onValueChanged.AddListener (delegate {
			UpdatePanButtons();
		});

		if (scrollRect.horizontal)
			enableScrolling = 1;
		else if (scrollRect.vertical)
			enableScrolling = 2;
		else if (scrollRect.horizontal && scrollRect.vertical)
			enableScrolling = 3;
	}

	void OnEnable()
	{
		UpdatePanButtons ();
	}
	// Update is called once per frame
	void Update () {
		
	}

	public void SetTargetScrollRect(ScrollRect sr)
	{
		scrollRect.onValueChanged.RemoveAllListeners ();
		scrollRect = sr;
		scrollRect.onValueChanged.AddListener (delegate {
			UpdatePanButtons();
		});
		left.scrollViewRect = scrollRect;
		right.scrollViewRect = scrollRect;
		UpdatePanButtons ();
	}

	public void SetMaxItems(int num)
	{
		maxItems = num;
	}

	public void UpdatePanButtons()
	{
		if (scrollRect.content.childCount > maxItems) {
			if (enableScrolling == 1) 
			{
				scrollRect.horizontal = true;	
			} 
			else if (enableScrolling == 2)
			{
				scrollRect.vertical = true;
			} 
			else if (enableScrolling == 3) 
			{
				scrollRect.vertical = true;
				scrollRect.horizontal = true;
			}

			if (scrollRect.vertical) {
				if (scrollRect.verticalNormalizedPosition <= maxLeft) {
					left.gameObject.SetActive (false);
					right.gameObject.SetActive (true);
				} else if (scrollRect.verticalNormalizedPosition >= maxRight) {
					right.gameObject.SetActive (false);
					left.gameObject.SetActive (true);
				} else if (scrollRect.verticalNormalizedPosition > maxLeft && scrollRect.verticalNormalizedPosition < maxRight) {
					right.gameObject.SetActive (true);
					left.gameObject.SetActive (true);
				}

			} else {
				if (scrollRect.horizontalNormalizedPosition <= maxLeft) {
					left.gameObject.SetActive (false);
					right.gameObject.SetActive (true);
				} else if (scrollRect.horizontalNormalizedPosition >= maxRight) {
					right.gameObject.SetActive (false);
					left.gameObject.SetActive (true);
				} else if (scrollRect.horizontalNormalizedPosition > maxLeft && scrollRect.horizontalNormalizedPosition < maxRight) {
					right.gameObject.SetActive (true);
					left.gameObject.SetActive (true);
				}
			}
		}
		else 
		{
			//print ("MAKE SOME NOISE!");
			left.gameObject.SetActive (false);
			right.gameObject.SetActive (false);
			scrollRect.verticalNormalizedPosition = 1;
			if (enableScrolling == 1) 
			{
				scrollRect.horizontal = false;	
			} 
			else if (enableScrolling == 2)
			{
				scrollRect.vertical = false;
			} 
			else if (enableScrolling == 3) 
			{
				scrollRect.vertical = false;
				scrollRect.horizontal = false;
			}
		}

		if (scrollRect.horizontal)
		{
			if (scrollRect.horizontalNormalizedPosition > maxRight) 
			{
				scrollRect.horizontalNormalizedPosition = maxRight;
			} 
			else if(scrollRect.horizontalNormalizedPosition < maxLeft) 
			{
				scrollRect.horizontalNormalizedPosition = maxLeft;
			}
		}
		else if(scrollRect.vertical)
		{
			if(scrollRect.verticalNormalizedPosition > maxRight)
			{
				scrollRect.verticalNormalizedPosition = maxRight;
			}
			else if(scrollRect.verticalNormalizedPosition < maxLeft)
			{
				scrollRect.verticalNormalizedPosition = maxLeft;
			}
		}
	

	}
}
