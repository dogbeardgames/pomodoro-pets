﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ScreenShotCustomScroll : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ScreenShot(Transform content)
	{
		DOTween.Kill ("inertia");
		content.transform.localPosition = Vector2.zero;
	}
}
