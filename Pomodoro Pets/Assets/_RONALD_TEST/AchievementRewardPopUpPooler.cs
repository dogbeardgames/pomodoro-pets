﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AchievementRewardPopUpPooler : MonoBehaviour {

	public static AchievementRewardPopUpPooler ins;

	public static bool serving;
	public delegate void DoSomething();

	List<DoSomething> lstAction = new List<DoSomething>();

	public void AddToQueue(DoSomething action)
	{

		lstAction.Add (action);
	}

	IEnumerator IEInvokeQueue()
	{
		for(int i=0; i<lstAction.Count; i++)
		{
			lstAction [i].Invoke ();
			serving = true;
			while(serving)
			{
				yield return new WaitForSeconds (0.01f);
			}

		}
		//print (lstAction.Count + " WALA NA" );
		yield return new WaitForSecondsRealtime(1f);
	}
	public void InvokeQueue()
	{

		StartCoroutine (IEInvokeQueue());
	}

	void Awake()
	{
		ins = this;
	}

	void Start () 
	{

	}
	

}
