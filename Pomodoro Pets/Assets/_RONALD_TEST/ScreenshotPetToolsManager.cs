﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenshotPetToolsManager : MonoBehaviour {

	[SerializeField]
	string[] toolName;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Interactible(bool b)
	{
		Collider2D[] box = FindObjectsOfType (typeof(Collider2D)) as Collider2D[];
		for(int i=0; i<box.Length; i++)
		{
			for(int j=0; j<toolName.Length; j++)
			{
				box [i].enabled = b;
			}
		}

		Rigidbody2D[] physics = FindObjectsOfType (typeof(Rigidbody2D)) as Rigidbody2D[];
		for(int i=0; i<physics.Length; i++)
		{
			for(int j=0; j<toolName.Length; j++)
			{
				if (b)
					physics [i].WakeUp ();
				else
					physics [i].Sleep ();
			}
		}
	}

}
