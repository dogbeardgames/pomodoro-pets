﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FollowToScreenPosition : MonoBehaviour {

	[SerializeField]
	Transform canvas;

	[SerializeField]
	Transform target;
	float tempY;
	void Start () {
		tempY = transform.localPosition.y;
		transform.SetParent (target);

		transform.localPosition = new Vector2 (0, transform.localPosition.y);
		Invoke ("Reparent",0.1f);
	}

	void Reparent()
	{
		transform.SetParent (canvas);
		transform.localPosition = new Vector2 (transform.localPosition.x, tempY);
	}
	// Update is called once per frame
	void Update () {
			
	}


}
