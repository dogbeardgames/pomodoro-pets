﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class BouncyIdleButton : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Shrink ();
	}
	
	// Update is called once per frame
	void Update () {
				
	}

	void Grow()
	{
		transform.DOScale (1f, 0.5f).OnComplete(()=> Shrink());
	}

	void Shrink()
	{
		transform.DOScale (0.8f, 0.5f).OnComplete(()=>Grow());
	}
}
