﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class FormatNumber{

	public static string GetFormat(long num)
	{
		// Ensure number has max 3 significant digits (no rounding up can happen)
		long i = (long)Math.Pow(10, (int)Math.Max(0, Math.Log10(num) - 2));
		num = num / i * i;

		if (num >= 1000000000)
			return (num / 1000000000D).ToString("0.##") + "B";
		if (num >= 1000000)
			return (num / 1000000D).ToString("0.##") + "M";
		if (num >= 1000)
			return (num / 1000D).ToString("0.##") + "K";

		return num.ToString("#,0");
	}
}
