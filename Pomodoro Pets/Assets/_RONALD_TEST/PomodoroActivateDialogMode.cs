﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PomodoroActivateDialogMode : MonoBehaviour {

	// Use this for initialization
	void Start () {
		On ();	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void On()
	{
		PomodoroUI.Instance.thereIsActiveDialog = true;
	}

	public void Off()
	{
		PomodoroUI.Instance.thereIsActiveDialog = false;
	}
}
