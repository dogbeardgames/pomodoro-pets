#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("sU/t4ZT/qL759pw7X2PL069LPYfd2tnc2Nvesv/l293Y2tjR2tnc2FYcm3MGOoznI5Gn3DBK1hGQF4Mg7tjn7uu99fvp6Rfs7djr6ekX2PXnddUbw6HA8iAWJl1R5jG29D4j1eXu4cJuoG4f5enp7e3o62rp6ei0j2fgXMgfI0TEyIeYXtfp2GRfqyeBjoGLiZyBh4bIqZ2cgIeagZyR2YqEjcibnImGjImajMicjZqFm8iJkthq6Z7Y5u7rvfXn6ekX7Ozr6un3eTP2r7gD7QW2kWzFA95Kv6S9BP7Y/O7rvezr++WpmJiEjci6h4ec7ejraunn6Nhq6eLqaunp6Ax5QeGEjcihhovG2c7YzO7rvezj+/WpmN5xpMWQXwVkczQbn3Manjqf2KcpmISNyLqHh5zIq6nY9v/l2N7Y3NqGjMiLh4aMgZyBh4abyIeOyJ2bjX12kuRMr2OzPP7f2yMs56Um/IE5921rbfNx1a/fGkFzqGbEPFl4+jBANJbK3SLNPTHnPoM8SszL+R9JRMfYaSvu4MPu6e3t7+rq2Gle8mlbQ0uZeq+7vSlHx6lbEBMLmCUOS6TIh47InICNyJyAjYbIiZiYhIGLibqNhIGJhouNyIeGyJyAgZvIi42an5/GiZiYhI3Gi4eFx4mYmISNi4nb3rLYitnj2OHu673s7vvqvbvZ+6Ewnnfb/I1Jn3whxerr6ejpS2rpxqhOH6+ll+C22Pfu6731y+zw2P7Vzo/IYtuCH+VqJzYDS8cRu4KzjJepQHAROSKOdMyD+ThLUwzzwiv37uu99ebs/uz8wziBr3ye4RYcg2VZ2LAEsuzaZIBbZ/U2jZsXj7aNVGrp6O7hwm6gbh+LjO3p2Gka2MLuyImGjMiLjZqcgY6Bi4mcgYeGyJiM3cv9o/2x9Vt8Hx50die4UimwuMirqdhq6crY5e7hwm6gbh/l6enpmomLnIGLjcibnImcjYWNhpybxticgIeagZyR2f7Y/O7rvezr++WpmMwKAzlfmDfnrQnPIhmFkAUPXf//4MPu6e3t7+rp/vaAnJyYm9LHx59o/MM4ga98nuEWHINlxqhOH6+ll+C22Grp+e7rvfXI7Grp4Nhq6ezYnIGOgYuJnI3IipHIiYaRyJiJmpzs7vvqvbvZ+9j57uu97OL74qmYmO8EldFrY7vIO9AsWVdyp+KDF8MUxMiLjZqcgY6Bi4mcjciYh4SBi5HY+e7rvezi++KpmJiEjcihhovG2THelylvvTFPcVHaqhMwPZl2lkm6kciJm5udhY2byImLi42YnImGi41f81V7qsz6wi/n9V6ldLaLIKNo/8JuoG4f5enp7e3o2IrZ49jh7uu92GrsU9hq60tI6+rp6urp6tjl7uFd0kUc5+boeuNZyf7GnD3U5TOK/mebaYgu87Phx3paEKygGIjQdv0drZb3pIO4fqlhLJyK4/hrqW/bYmkh8ZodteY9l7dzGs3rUr1npbXlGc7YzO7rvezj+/WpmJiEjcirjZqcY/FhNhGjhB3vQ8rY6gDw1hC44Tsoi9ufH9LvxL4DMufJ5jJSm/GnXZiEjcirjZqcgY6Bi4mcgYeGyKmduEJiPTIMFDjh799YnZ3J");
        private static int[] order = new int[] { 52,7,50,29,22,47,21,11,42,28,15,12,14,29,48,21,43,18,49,54,37,24,41,38,36,42,27,31,56,49,30,31,34,45,37,49,46,47,53,49,47,45,49,50,51,51,56,58,58,59,58,52,53,55,59,59,56,58,58,59,60 };
        private static int key = 232;

        public static byte[] Data() {
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
