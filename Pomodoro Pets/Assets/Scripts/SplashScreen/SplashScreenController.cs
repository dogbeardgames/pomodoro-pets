﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class SplashScreenController : MonoBehaviour {

    [SerializeField]
    Vector3 from, to;

    [SerializeField]
    float duration;

    [SerializeField]
    Transform logo;

	// Use this for initialization
	void Start () 
    {   
//        logo.localScale = new Vector3()
        logo.GetComponent<SpriteRenderer>().DOFade(1, duration);
        logo.DOScale(to, duration).OnComplete(SplashScreenOnComplete);
	}	

    #region METHODS

    void SplashScreenOnComplete()
    {
        SceneManager.LoadSceneAsync("InitialScene");
    }

    #endregion
}
