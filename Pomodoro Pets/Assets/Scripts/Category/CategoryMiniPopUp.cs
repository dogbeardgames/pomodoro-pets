﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class CategoryMiniPopUp : MonoBehaviour {

    public int index;
    public GameObject categoryMaintenance;

    Button btnYes, btnNo;
    Transform confirmationPanel;
    //private Button btnAdd;

    // Use this for initialization
    void Start () {
        transform.Find("btnEdit").GetComponent<Button>().onClick.AddListener(Edit);
        transform.Find("btnDelete").GetComponent<Button>().onClick.AddListener(Delete);
		//DataController.Instance.playerData.enableDrag = false;
    }

    // Update is called once per frame
    void Update () {

    }

    #region METHODS
    private void Edit()
    {
        categoryMaintenance = Instantiate(categoryMaintenance, transform.parent.transform.parent, false);
        categoryMaintenance.GetComponent<CategoryMaintenance>().SetUpEditCategoryUI(index);
        categoryMaintenance.GetComponent<CategoryMaintenance>().SetMaintenanceStateEvent(CategoryPanel.MaintenanceState.Edit);
    }

    private void Delete()
    {        
        //Debug.Log("Delete");
        confirmationPanel = transform.parent.Find("ConfirmationPanel").transform;
        btnYes = confirmationPanel.Find("btnYes").GetComponent<Button>();
        btnNo = confirmationPanel.Find("btnNo").GetComponent<Button>();
        btnYes.onClick.AddListener(ConfirmDelete);
        btnNo.onClick.AddListener(CancelDelete);
        confirmationPanel.gameObject.SetActive(true);

        confirmationPanel.Find("lblTitle").GetComponent<TextMeshProUGUI>().text = "Delete Category?";
        //confirmationDialog.transform.Find()
    }

    private void ConfirmDelete()
    {
        //Debug.Log("Confirm delete");


		//update task category index once task category is deleted.
		List<TaskItem> filteredTask = DataController.Instance.taskList.items.Where (x => x.categoryIndex == index).ToList ();

		for (int i = 0; i < filteredTask.Count; i++) {

			//Debug.Log ("Task List for this category" + filteredTask[i].task);
			filteredTask [i].categoryIndex = 0;
		}

		//Delete selected category
        DataController.Instance.categoryList.items.RemoveAt(index);

		GameObject showDialog = Instantiate (Resources.Load<GameObject>("Prefab/StoreDialog"), GameObject.Find("Canvas").transform, false);
		ShowDialog dialog = showDialog.GetComponent<ShowDialog>();
		dialog.SetCaption ("Category has been deleted.");
		dialog.SetConfirmCaption ("OK");
		dialog.DisableNO ();
		dialog.OnYesEvent += () => {
			Destroy(showDialog);
		};


        FindObjectOfType<CategoryPanel>().InitializeButtons();
        Destroy(GameObject.Find("PopUp"));
        Destroy(gameObject);

//		if (FindObjectOfType<CategoryPanel> () != null) {
//			FindObjectOfType<CategoryPanel> ().taskNotifPanel.transform.Find("Image").Find("TextMeshPro Text").GetComponent<TextMeshProUGUI>().text = "Category has been deleted";
//			FindObjectOfType<CategoryPanel> ().setActive ();
//		}
    }

    private void CancelDelete()
    {
        Destroy(GameObject.Find("PopUp"));
    }
    #endregion
}
