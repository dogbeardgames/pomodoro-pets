﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;
using System.Linq;

public class CategoryMaintenance : MonoBehaviour {

	public Dictionary<string,List<string>> enumDictionaryTask = new Dictionary<string,List<string>> ();

	[SerializeField]
	TextMeshProUGUI txtTitle, txtAsterisk;

	[SerializeField]
	TMP_InputField inputCategoryName;

	[SerializeField]
	TMP_Dropdown dropDownPetIcon;

	[SerializeField]
	Button btnBack, btnSave;

	//    [SerializeField]
	//    ScrollRect scrollRect;

	[SerializeField]
	private int index;

	// Use this for initialization
	void Start () {
		DataController.Instance.playerData.enableDrag = false;
	}

	// Update is called once per frame
	//	void Update () {
	//        
	//	}

	#region METHODS
	public void SetMaintenanceStateEvent(CategoryPanel.MaintenanceState state)
	{
		switch (state)
		{
		case CategoryPanel.MaintenanceState.Add:
			btnSave.onClick.AddListener(SaveAdd);
			break;
		case CategoryPanel.MaintenanceState.Edit:
			btnSave.onClick.AddListener(SaveEdit);
			break;
		default:
			break;
		}
	}

	public void SetUpAddcategoryUI()
	{
		txtTitle.text = "Add Category";

		//Debug.Log ("<color=yellow>" + "Start" + "</color>");
		LanguageCheckDropdown ();

		btnBack.onClick.AddListener(CloseForm);

		//txtTitle.text = "Add Category";       

		List<string> list = new List<string>();

		var categoryDic = enumDictionaryTask.Where (y => y.Key == "set_petIcon").ToDictionary (y => y.Key, y => y.Value);

		//Debug.Log ("<color=red>" + "filtering..." + "</color>" + categoryDic.Count);


		foreach (List<string> listpetIcons in categoryDic.Values) {
			foreach (var dataEnum in listpetIcons) {
				list.Add((dataEnum).ToString());
				//Debug.Log ("<color=blue>" + dataEnum + "</color>");
			}
		}
		dropDownPetIcon.AddOptions(list);
		list.Clear();

//		// add active pets options to drop down
//		for (int i = 0; i < DataController.Instance.playerData.playerPetList.Count; i++)
//		{
//			list.Add(DataController.Instance.playerData.playerPetList[i].petBreed);
//		}
//
//		dropDownPetIcon.AddOptions(list);
		//
	}

	public void SetUpEditCategoryUI(int _index)
	{

        CategoryPanel.ins.selectedIndex = _index;
        index = _index;
//        _index = CategoryPanel.ins.selectedIndex;

		txtTitle.text = "Edit Category";

		LanguageCheckDropdown ();
		List<string> list = new List<string>();

		var categoryDic = enumDictionaryTask.Where (y => y.Key == "set_petIcon").ToDictionary (y => y.Key, y => y.Value);

		//Debug.Log ("<color=red>" + "filtering..." + "</color>" + categoryDic.Count);


		foreach (List<string> listpetIcons in categoryDic.Values) {
			foreach (var dataEnum in listpetIcons) {
				list.Add((dataEnum).ToString());
				//Debug.Log ("<color=blue>" + dataEnum + "</color>");
			}
		}
		dropDownPetIcon.AddOptions(list);
		list.Clear();


		//Debug.Log("Edit! Index " + _index);

		btnBack.onClick.AddListener(CloseForm);

		//index = _index;

		//find label
		//txtTitle.text = "Edit Task";
		// add listener to close button
		//btnBack.onClick.AddListener(CloseForm);

		inputCategoryName.text = DataController.Instance.categoryList.items[_index].category;
		dropDownPetIcon.value = (int)DataController.Instance.categoryList.items[_index].petIconIndex;
//        dropDownPetIcon.value = (int)DataController.Instance.categoryList.items[index].petIconIndex;
		//
	}

	public void CloseForm()
	{
		Framework.SoundManager.Instance.PlaySFX("seIconTapPomodoro");

		Destroy(gameObject);

	}

	void SaveAdd()
	{
		//confirmDialog.SetActive(true);
		Framework.SoundManager.Instance.PlaySFX("seIconTapPomodoro");
		//Debug.Log("Erico - Save Add Cat");

		CategoryItem categoryItem = new CategoryItem();

		categoryItem.category = inputCategoryName.text.Trim();
		categoryItem.petIconIndex = dropDownPetIcon.value;


		if (categoryItem.category.Length == 0) {
			txtAsterisk.GetComponent<TextMeshProUGUI>().text = "required field";

			inputCategoryName.Select ();

		} else {

			DataController.Instance.categoryList.items.Add(categoryItem);
			//categoryItem.category = inputCategoryName.text == "" ? inputCategoryName.transform.Find("Placeholder").GetComponent<Text>().text : inputCategoryName.text.Trim();

			//dropDownPetIcon

			//DataController.Instance.taskList.items.Add(taskItem);

			CategoryPanel.ins.InitializeButtons();

			//Debug.Log("Pet index " + categoryItem.petIconIndex); 

			// TaskMaintence
			TaskMaintenance taskMaintenance = FindObjectOfType<TaskMaintenance>();
			if(taskMaintenance != null)
			{
				FindObjectOfType<TaskMaintenance>().UpdateCategory();
			}
			// TaskMaintenance

			GameObject showDialog = Instantiate (Resources.Load<GameObject>("Prefab/StoreDialog"), GameObject.Find("Canvas").transform, false);
			ShowDialog dialog = showDialog.GetComponent<ShowDialog>();
			dialog.SetCaption ("Category has been created.");
			dialog.SetConfirmCaption ("OK");
			dialog.DisableNO ();
			dialog.OnYesEvent += () => {
				Destroy(showDialog);
			};

			Destroy(GameObject.Find("PopUp"));
			Destroy(gameObject);

//			if (FindObjectOfType<CategoryPanel> () != null) {
//				FindObjectOfType<CategoryPanel> ().taskNotifPanel.transform.Find("Image").Find("TextMeshPro Text").GetComponent<TextMeshProUGUI>().text = "Category has been created.";
//				FindObjectOfType<CategoryPanel> ().setActive ();
//			}  

		} 
	}  

	void SaveEdit()
	{
		//DataController.Instance.playerData.isDeleting = true;
		GameObject.Find ("Pomodoro").GetComponent<TaskChecker> ().isDeleting = true;

		if (inputCategoryName.text.Length == 0) {
			txtAsterisk.GetComponent<TextMeshProUGUI> ().text = "required field";

			inputCategoryName.Select ();

		} else {
			DataController.Instance.categoryList.items [index].category = inputCategoryName.text;
			DataController.Instance.categoryList.items[index].petIconIndex = dropDownPetIcon.value;

			index = -1;

			CategoryPanel.ins.InitializeButtons ();

            PomodoroUI pomodoroUI = FindObjectOfType<PomodoroUI>();
            // udate mini icons just in case pet icon index has been changed
            pomodoroUI.TaskUI();

			GameObject showDialog = Instantiate (Resources.Load<GameObject>("Prefab/StoreDialog"), GameObject.Find("Canvas").transform, false);
			ShowDialog dialog = showDialog.GetComponent<ShowDialog>();
			dialog.SetCaption ("Category has been edited.");
			dialog.SetConfirmCaption ("OK");
			dialog.DisableNO ();
			dialog.OnYesEvent += () => {
				Destroy(showDialog);
			};

			Destroy (gameObject);


			//CategoryPanel.ins.setActive ();
//			if (FindObjectOfType<CategoryPanel> () != null) {
//				FindObjectOfType<CategoryPanel> ().taskNotifPanel.transform.Find("Image").Find("TextMeshPro Text").GetComponent<TextMeshProUGUI>().text = "Category has been edited";
//				FindObjectOfType<CategoryPanel> ().setActive ();
//			}
		}
		//DataController.Instance.playerData.isDeleting = false;
		GameObject.Find ("Pomodoro").GetComponent<TaskChecker> ().isDeleting = false;

	}

	public void Yes()
	{
		Destroy(gameObject);
	}

	public void No()
	{
		//confirmDialog.SetActive(false);
	}


	//Enum Collection json file load
	public void LoadEnumJson(string fileName)
	{


		string filePath = "";

		if (Application.platform == RuntimePlatform.Android)
		{
			// Android
			filePath = System.IO.Path.Combine(Application.streamingAssetsPath, fileName);

			// Android only use WWW to read file
			WWW reader = new WWW(filePath);
			while ( ! reader.isDone) {}

			string realPath = Application.persistentDataPath + fileName;
			System.IO.File.WriteAllBytes(realPath, reader.bytes);

			filePath = realPath;
		}
		else
		{
			// iOS
			filePath = System.IO.Path.Combine(Application.streamingAssetsPath, fileName);
		}

		if (File.Exists (filePath)) {
			//Debug.Log ("Test");
			string dataAsJson = File.ReadAllText (filePath);
			LocalizationDataList enumData = JsonUtility.FromJson<LocalizationDataList> (dataAsJson);

			for (int i = 0; i < enumData.items.Length; i++) 
			{
				List<string> enumlistcat = new List<string> ();
				for (int j = 0; j < enumData.items[i].value.Length; j++) {


					//Debug.Log ("<color=green>" + "Key: " +    enumData.items [i].key +    " ALL enum data: " + enumData.items [i].value[j] + "</color>");	


					enumlistcat.Add (enumData.items [i].value [j]); 
				}

				enumDictionaryTask.Add (enumData.items [i].key, enumlistcat);

				//enumlist.Add (enumData.items[i].value);
			}
			//			for (int i = 0; i < enumlist.Count; i++) {
			//				
			//				Debug.Log ("EnumList " + enumlist[i]);
			//			}
			//for (int i = 0; i < enumDictionary.Count; i++) {
			//	Debug.Log ("EnumDictionary Count: " + enumDictionary.Count);
			//}

			// Debug.Log ("Data loaded, dictionary contains: " + localizedText.Count + " entries");
		} else 
		{
			//Debug.LogError (filePath);
			//Debug.LogError ("Cannot find file!");
		}
	}


	void LanguageCheckDropdown()
	{
		//Localized dropdown depends on language selected in settings ***In progress 4/5/2016

		if (DataController.Instance.settings.languageIndex == 0 ) { // English
			LoadEnumJson("localizedtextDropdown_en.json");
			//Debug.Log("<color=green>" + "English" + "</color>");
		}
		else if (DataController.Instance.settings.languageIndex == 1 ) { // Spain
			LoadEnumJson("localizedtextDropdown_sp.json");
			//Debug.Log("<color=green>" + "Spanish" + "</color>");
		}
	}

	#endregion
}
