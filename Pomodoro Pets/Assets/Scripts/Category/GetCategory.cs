﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class GetCategory : MonoBehaviour {

	[SerializeField]
	Toggle btncategory;
	[SerializeField]
	Text txtcategory;

	string indexValue;

	void Start ()
	{
		txtcategory = GetComponent<Button> ().transform.FindChild ("Text").GetComponent<Text> ();
	}

	public void UpdateList()
	{
		AddCategory.instance.UpdateCategoryValue (int.Parse (txtcategory.text));
	}

}
