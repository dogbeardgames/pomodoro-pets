﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CategoryPanel : MonoBehaviour {
	public static CategoryPanel ins;

    [SerializeField]
    GameObject miniPopUp, categoryButton, contentParent, categoryMaintenancePanel;

    GameObject objMiniPopUp;

	public GameObject taskNotifPanel;

    public enum MaintenanceState
    {
        Add, Edit, Delete
    }
	[SerializeField]
	List<CategoryOptionsManager> lstOptionsMan;

	public List<CategoryOptionsManager> ListOptionsMan{
		get{ return lstOptionsMan;}
	}

    public int selectedIndex;



	[SerializeField]
	Transform catButtonGroup;

    // Use this for initialization
    void Start () {
		ins = this;
        InitializeButtons();
        //Debug.Log(Application.persistentDataPath);

		DataController.Instance.playerData.enableDrag = false;
    }

	public void CallTaskPanel(GameObject TaskPanelx)
	{
		GameObject taskpanel = Instantiate (TaskPanelx, GameObject.Find("Canvas").transform, false);
	}

	void OnDisable()
	{
		PomodoroUI.Instance.isMenuActive = false;
	}

    #region METHODS

	public void HideOptions()
	{
		for(int i=0; i<lstOptionsMan.Count; i++)
		{
			lstOptionsMan [i].HideOptionsButton ();
			//print (lstOptionsMan[i].gameObject.name);
		}
	}

	//popup when deleting,adding,editing category
	public void setActive()
	{		
		Instantiate (taskNotifPanel, GameObject.Find ("Canvas").transform, false);
		//Debug.Log ("Hello 1");
	}

	void GetManagers()
	{
		lstOptionsMan.Clear ();
		for(int i=0; i<catButtonGroup.childCount; i++)
		{
			if(catButtonGroup.GetChild(i).GetComponent<CategoryButton> () != null)
			lstOptionsMan.Add (catButtonGroup.GetChild(i).GetComponent<CategoryButton> ().optionsManager);
		}
	}
    //create buttons for every task
    public void InitializeButtons()
    {
        //clear child objects
        GameObject[] children = GameObject.FindGameObjectsWithTag("Category");
        contentParent.transform.DetachChildren();   
        //        for (int i = 0; i < contentParent.transform.childCount; i++)
        //        {
        //            DestroyImmediate(contentParent.transform.GetChild(i).gameObject);
        //        }            

        for (int i = 0; i < children.Length; i++)
        {
            DestroyImmediate(children[i]);
        }

        for (int i = 0; i < DataController.Instance.categoryList.items.Count; i++)
        {
            GameObject button = Instantiate(categoryButton, contentParent.transform, false);   
            button.GetComponent<CategoryButton>().SetUITask(DataController.Instance.categoryList.items[i]);
			button.GetComponent<CategoryButton> ().SetIndex (button.transform.GetSiblingIndex ());
			button.GetComponent<CategoryButton> ().locked = DataController.Instance.categoryList.items[i].isLocked;
        }      
		GetManagers ();
    }

    private void MiniPopUp(Button button, int selectedIndex, bool isLocked)
    {
//        Debug.Log("Mini Pop up");
//
//        objMiniPopUp = Instantiate(miniPopUp, transform.parent, false);
//        objMiniPopUp.name = "PopUp";
//        objMiniPopUp.GetComponent<Button>().onClick.AddListener(CloseMiniPopUp);
//        objMiniPopUp.transform.Find("MiniPopUp").transform.position = button.transform.position;

        if(isLocked)
        {
			objMiniPopUp.transform.Find("MiniPopUp").transform.Find("btnDelete").GetComponent<Button>().gameObject.SetActive(false);
        }

        CategoryMiniPopUp categoryMiniPopUp = objMiniPopUp.transform.Find("MiniPopUp").gameObject.AddComponent<CategoryMiniPopUp>();
        categoryMiniPopUp.index = selectedIndex;
        categoryMiniPopUp.categoryMaintenance = categoryMaintenancePanel;

        //Debug.Log("Selected index " + categoryMiniPopUp.index);
    }

    private void CloseMiniPopUp()
    {       
        Destroy(GameObject.Find("PopUp"));


    }

    public void AddTask()
    {
        GameObject categoryMaintenance = Instantiate(categoryMaintenancePanel, transform.parent, false);
        categoryMaintenance.name = "CategoryMaintenance";
        categoryMaintenance.GetComponent<CategoryMaintenance>().SetUpAddcategoryUI();
        categoryMaintenance.GetComponent<CategoryMaintenance>().SetMaintenanceStateEvent(MaintenanceState.Add);
		Framework.SoundManager.Instance.PlaySFX ("seIconTapPomodoro");
    }

    public void ClosePanel()
    {   
        Destroy(gameObject);
    }

    #endregion
}
