﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.UI;

[System.Serializable]
public class CategoryBase {

	public string category;
	public float session_duration;
	public float shortbreak_duration;
	public float longbreak_duration;
	//public Image petImage;

}    
