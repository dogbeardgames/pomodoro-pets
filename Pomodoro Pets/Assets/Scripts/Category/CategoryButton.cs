﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class CategoryButton : MonoBehaviour{

	[SerializeField]
	CategoryOptionsManager optionsMan;
    [SerializeField]
    GameObject resetButton, dragPanel;

    Toggle toggle;     

    bool ishilighted = false;
    bool isPressed = false;

    float timer;
	[SerializeField]
	int index;
	public bool locked;
    //public static GameObject highlightedObject;

    EnumCollection.MaintenanceButton maintenanceButton;

	public CategoryOptionsManager optionsManager{
		get{ return optionsMan;}
	}
    // Use this for initialization
    void Start () {

    }

//    void Update()
//    {
//        if(isPressed)
//        {
//            if(Time.time - timer >= 1)// 1 second
//            {
//                Highlight();
//                //transform.GetComponent<Toggle>().interactable = true;
//            }
//        }
//    }

    #region METHODS

	public void SetIndex(int index)
	{
		this.index = index;
		optionsMan.Index = this.index;
	}
    public void SetUITask(CategoryItem categoryItem)
    {        
        transform.Find("Label").GetComponent<TextMeshProUGUI>().text = categoryItem.category;  
        maintenanceButton = EnumCollection.MaintenanceButton.Task;
    }        

//    public void ValueChange()
//    {
//        if(toggle.isOn)
//        {
//            DataController.Instance.taskList.items[toggle.transform.GetSiblingIndex()].dateFinished = System.DateTime.Now;
//            DataController.Instance.taskList.items[toggle.transform.GetSiblingIndex()].isDone = true;
//        }
//        else
//        {
//            DataController.Instance.taskList.items[toggle.transform.GetSiblingIndex()].isDone = false;
//            Debug.Log(DataController.Instance.taskList.items[toggle.transform.GetSiblingIndex()].dateFinished.ToLongDateString() + " " + 
//                DataController.Instance.taskList.items[toggle.transform.GetSiblingIndex()].dateFinished.ToLongTimeString());
//        }
//    }     

    public void Reset()
    {
        transform.GetComponent<Button>().interactable = true;
        transform.Find("Label").GetComponent<Text>().fontStyle = FontStyle.Normal;
        dragPanel.SetActive(false);
        resetButton.SetActive(false);

        FindObjectOfType<ScrollDrag>().enabled = true;       

        //highlightedObject = null;
    }

//    void Highlight()
//    {
//        if (highlightedObject == null)
//        {
//            highlightedObject = gameObject;
//
//            Debug.Log("Should hilight");
//            ishilighted = true;
//            transform.Find("Label").GetComponent<Text>().fontStyle = FontStyle.Bold;
//            transform.GetComponent<Button>().interactable = false;
//            isPressed = false;
//            dragPanel.SetActive(true);
//            resetButton.SetActive(true);
//
//            FindObjectOfType<ScrollDrag>().enabled = false;
//
//            //GetComponent<Drag>().enabled = true;
//        }
//    }
    #endregion

//    #region IPointerDownHandler implementation
//    public void OnPointerDown(PointerEventData eventData)
//    {
//        if(eventData.selectedObject != null && !ishilighted)
//        {
//            Debug.Log("Selected " + eventData.selectedObject.name);
//            isPressed = true;
//            timer = Time.time;
//        }
//        else if(eventData.selectedObject != null && ishilighted)
//        {
//            Debug.Log("Back to normal");
//            ishilighted = !ishilighted;
//            transform.Find("Label").GetComponent<Text>().fontStyle = FontStyle.Normal;
//        }
//    }
//    #endregion

//    #region IPointerUpHandler implementation
//
//    void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
//    {
//        if(eventData.selectedObject != null)
//        {
//            Debug.Log("Selected up " + eventData.selectedObject.name);
//            isPressed = false;
//        }
//    }
//
//    #endregion
}
