﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CategorySlider : MonoBehaviour {

    Slider slider;

    // Use this for initialization
    void Awake () {
        slider = GetComponent<Slider>();
        //Debug.Log("Attached " + gameObject.name);
    }

    #region METHODS
    public void SliderValue(TMP_InputField inputField)
    {
        inputField.text = slider.value.ToString();   
    }
    #endregion
}
