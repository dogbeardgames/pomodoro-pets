﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class AddCategory : MonoBehaviour {
	[SerializeField]
	Slider session;

	[SerializeField]
	Slider shortbreak;

	[SerializeField]
	Slider longbreak;

	[SerializeField]
	Slider sessionBeforeLongBreak;

	[SerializeField]
	public InputField category;

	[SerializeField]
	Dropdown petIconTag;




	[SerializeField]
	GameObject parent,text,toggledisplay,btnupdate,textbtn;

	[SerializeField]
	CategoryDatabase categorydb;


	CategoryItem categoryItem = new CategoryItem();



	public static AddCategory instance;


	void Start()
	{
		
		instance = this;
		CategoryList ();

		PopulatePetIconDropdown ();// populate 
	}
		
	
	public void SaveCategoryData()//save category data;
	{
		categoryItem.category = category.text;
        categoryItem.petIconIndex = petIconTag.value;
		//categoryItem.petIcon = "";

		categorydb.items.Add (categoryItem);
	}
		




	void CategoryList()// get all category and display into toggle
	{
		for(int i = 0;i<categorydb.items.Count;i++)
		{

			//instantiate prefab for toggle/checkbox
			GameObject ToggleGameObject = Instantiate (toggledisplay);
			ToggleGameObject.transform.SetParent (parent.transform);
			ToggleGameObject.GetComponent<Toggle> 
			().transform.FindChild ("lblDisplay")
			.GetComponent<Text> ().text = categorydb.items [i].category;
			
			//instantiate prefab for button
			GameObject buttonupdateGameObject = Instantiate (btnupdate);
			buttonupdateGameObject.transform.SetParent (parent.transform);
			buttonupdateGameObject.GetComponent<Button> 
			().transform.FindChild ("Text")
			.GetComponent<Text> ().text =""+ (i);

			//int index = i;
		}
	}
		
	public void UpdateList()
	{
		string item = this.category.text;
		//Debug.Log (item);
	}


	void PopulatePetIconDropdown()//Populate PetIconTag dropdown
	{
			petIconTag.options.Add(new Dropdown.OptionData("Generic"));
			petIconTag.options.Add(new Dropdown.OptionData("Work"));
			petIconTag.options.Add(new Dropdown.OptionData("School"));
			petIconTag.options.Add(new Dropdown.OptionData("Play"));
			petIconTag.options.Add(new Dropdown.OptionData("Workout"));
	}

	public void UpdateCategoryValue (int index)
	{
		//Debug.Log (index);
        categorydb.items[index].category = category.text;
		//Debug.Log ("Edited");
	}
		

	public void selectPetIcon(String petImageTag )
	{
		
		switch (petImageTag) 
		{

		case "Play":
			//Debug.Log("You chose play icon");
			//gameObject.GetComponent<Image> ().sprite = "Play sprite here";

			break;

		case "Work":
			//Debug.Log("You chose work icon");
			//gameObject.GetComponent<Image> ().sprite = "work sprite here";
			break;

		case "Workout":
			//Debug.Log("You chose workout icon");
			//gameObject.GetComponent<Image> ().sprite = "workout sprite here";
			break;

		case "School":
			//Debug.Log("You chose school icon");
			//gameObject.GetComponent<Image> ().sprite = "school sprite here";
			break;

		case "Generic":
			//Debug.Log("You chose generic icon");
			//gameObject.GetComponent<Image> ().sprite = "generic sprite here";
			break;
			
		}

	}
		
}
