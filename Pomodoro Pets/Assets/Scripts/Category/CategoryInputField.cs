﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CategoryInputField : MonoBehaviour {

    TMP_InputField inputField;

	// Use this for initialization
    void Awake () {
        inputField = GetComponent<TMP_InputField>();
	}
	
	#region METHODS
    public void EndEdit(Slider slider)
    {		
        slider.value = float.Parse(inputField.text);
        inputField.text = slider.value.ToString();
    }
    #endregion
}
