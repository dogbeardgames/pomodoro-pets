﻿using UnityEngine;
using System.Collections;
#if UNITY_IOS
using NotificationServices = UnityEngine.iOS.NotificationServices;
#endif
//using NotificationType = UnityEngine.iOS.NotificationType;
//using LocalNotification = UnityEngine.iOS.LocalNotification;
//using NotificationServices = UnityEngine.iOS.NotificationServices;


public class LocalNotificationIos : MonoBehaviour {


	void Awake()
	{
        #if UNITY_IOS
		UnityEngine.iOS.NotificationServices.RegisterForNotifications(
		UnityEngine.iOS.NotificationType.Alert|
		UnityEngine.iOS.NotificationType.Badge|
		UnityEngine.iOS.NotificationType.Sound
		);
        #endif
	}

//	void Start()
//	{
//		#if UNITY_IOS
//		UnityEngine.iOS.NotificationServices.ClearLocalNotifications ();
//		#endif
//	}
//		
	public static void SetNotification(string message, int delayTime, int badgeNumber){
        #if UNITY_IOS
		UnityEngine.iOS.LocalNotification notif = new UnityEngine.iOS.LocalNotification ();
		notif.alertBody = message;
		notif.fireDate = System.DateTime.Now.AddSeconds(delayTime);
		notif.applicationIconBadgeNumber = -1;
		notif.alertAction = "Pomodoro Pets";
		notif.hasAction = true;
		UnityEngine.iOS.NotificationServices.ScheduleLocalNotification(notif);
        #endif
	}   
		
	public static void CancelNotification()
	{
		#if UNITY_IOS
		UnityEngine.iOS.LocalNotification notif = new UnityEngine.iOS.LocalNotification ();
        UnityEngine.iOS.NotificationServices.CancelLocalNotification (notif);
		#endif
	}

	public static void CancelAllNotification()
	{
		#if UNITY_IOS
		UnityEngine.iOS.LocalNotification notif = new UnityEngine.iOS.LocalNotification ();
		UnityEngine.iOS.NotificationServices.CancelAllLocalNotifications ();
		#endif
	}
}
