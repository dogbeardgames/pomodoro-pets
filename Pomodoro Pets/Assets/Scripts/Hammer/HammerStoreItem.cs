﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using System.Linq;
using UnityEngine.UI;
using TMPro;

public class HammerStoreItem : MonoBehaviour {  

    [SerializeField]
    string itemName;

    IAPButton iapButton;

    [SerializeField]
    TextMeshProUGUI text;

	// Use this for initialization
	void Start () 
    {
        Store store = FindObjectOfType<Store>();
        iapButton = GetComponent<IAPButton>();

        StoreStat();

        // Unity purchase
        iapButton.onPurchaseComplete.AddListener(store.CompletePurchase);
        iapButton.GetComponent<IAPButton>().onPurchaseFailed.AddListener(store.PurchaseFailed);

        GetComponent<Button>().onClick.AddListener(() =>
            {
                store.storeCallBack = ItemBought;
                ConfirmPurchase();
            });

	}

    #region METHODS

    void StoreStat()
    {
        HammerData hammer = DataController.Instance.hammerDatabase;
        if(hammer.Items.First(_hammer => _hammer.ItemName == this.itemName) != null)
        {
            if(hammer.Items.First(_hammer => _hammer.ItemName == this.itemName).isOwned)
            {
                GetComponent<Button>().interactable = false;
                text.text = "Owned";
                text.transform.parent.GetComponent<Image>().sprite = Store.ins.GetSpriteOwned();    
            }
        }
    }

    // call back when item is bought
    void ItemBought()
    {
        HammerData hammerData = DataController.Instance.hammerDatabase;
               
        if(hammerData.Items.First(hammer => hammer.ItemName == itemName) != null)
        {
            hammerData.Items.First(hammer => hammer.ItemName == itemName).isOwned = true;
        }

        StoreStat();
    }

    void ConfirmPurchase()
    {
        if (IAPButton.IAPButtonStoreManager.Instance.isInitialized)
        {
            Framework.SoundManager.Instance.PlaySFX("seIconTapPets");
            GameObject buyHammerObject = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), FindObjectOfType<Store>().transform.parent, false);
            ShowDialog confirmDialog = buyHammerObject.GetComponent<ShowDialog>();
            // set captions
            string currency = iapButton.GetProductPrice().Substring(3, 1);
            confirmDialog.SetCaption("Do you want to buy\n" + this.itemName + " For " + iapButton.GetProductPrice().Replace(currency, " ") + "?");
            confirmDialog.SetConfirmCaption("Yes");
            confirmDialog.SetCancelCaption("No");
            // set events
            confirmDialog.OnYesEvent += () =>
            {              
                iapButton.GetPurchaseProductMethod();
                //PetAdopted();
                Destroy(buyHammerObject);
            };
            confirmDialog.OnNoEvent += () =>
            {
                Destroy(buyHammerObject);
            };
        }
        else
        {
            GameObject affirmDialog = Instantiate(Resources.Load<GameObject>("Prefab/AffirmDialog"), GameObject.Find("Canvas").transform, false);
            AffirmDialog _affirmDialog = affirmDialog.GetComponent<AffirmDialog>();
            _affirmDialog.SetCaption("Can't connect to store.");
            _affirmDialog.SetConfirmCaption("OK");
            _affirmDialog.OnYesEvent += () => { Destroy(affirmDialog); };
        }
    }

    #endregion
}
