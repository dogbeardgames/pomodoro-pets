﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.SceneManagement;
using System.Linq;

public class RestorePurchase : MonoBehaviour {

    [SerializeField]
    IAPButton iapButton;     

    public static RestorePurchase Instance;

    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
            iapButton.onPurchaseComplete.AddListener(new RestorePurchases().RestorePurchase);
        }
        else
        {
            Destroy(gameObject);
        }
    }    

    public void RestoreScene()
    {
        SceneManager.LoadScene("Restore");  
    }
}
