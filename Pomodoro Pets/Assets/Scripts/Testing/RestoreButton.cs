﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;

public class RestoreButton : MonoBehaviour {   

    [SerializeField]
    GameObject AffirmDialog;

    Button button;

    void Awake()
    {
        if(Application.platform == RuntimePlatform.Android)
        {
            gameObject.SetActive(false);
        }
    }

	// Use this for initialization
	void Start () 
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(Restore);
	}
	
    public void Restore()
    {        
        if(Application.platform == RuntimePlatform.OSXPlayer
        || Application.platform == RuntimePlatform.IPhonePlayer
        || Application.platform == RuntimePlatform.tvOS)
        {            
            IAPButton.IAPButtonStoreManager.Instance.ExtensionProvider.GetExtension<IAppleExtensions>().RestoreTransactions(OnTransactionRestored);
            StartCoroutine("ProcessDialog");
        }
        else
        {
            //Debug.LogWarning(Application.platform.ToString() + " is not a supported platform for the Codeless IAP restore button");
        }
    }

    bool isTransactionSuccessful = false;
    void OnTransactionRestored(bool success)
    {
        isTransactionSuccessful = success;
    }

    IEnumerator ProcessDialog()
    {
        // create process dialog
        GameObject processdialogObject = Instantiate(Resources.Load<GameObject>("Prefab/PanelRestoreProcessing"), GameObject.Find("Canvas").transform, false);
        processdialogObject.name = "PanelRestoreProcessing";

        yield return new WaitForSeconds(5);

        //Debug.Log("<color=white>transaction extension, success " + isTransactionSuccessful.ToString());

        Destroy(GameObject.Find("PanelRestoreProcessing"));

        GameObject affirmDialog = Instantiate(AffirmDialog, GameObject.Find("Canvas").transform, false);
        AffirmDialog _affirmDialog = affirmDialog.GetComponent<AffirmDialog>();
        if(isTransactionSuccessful)
        {
            _affirmDialog.SetCaption("Restore Purchase Successful.");
        }
        else
        {
            _affirmDialog.SetCaption("Restore Purchase Failed.");
        }
        _affirmDialog.SetConfirmCaption("OK");
        _affirmDialog.OnYesEvent += () => { Destroy(affirmDialog); };
    }
}
