﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using System.Linq;
using UnityEngine.SceneManagement;

public class RestorePurchases{

    #region METHODS

    public void RestorePurchase(Product product)
    {
        //Debug.Log("<color=white> product " + product.definition.id);

        // apply restored purchases
        switch (product.definition.id) 
        {
            #region FULL COSTUME
            case "Bark Vader":
                FullCostume(product.definition.id);
                break;
            case "Cat-tain Americat":
                FullCostume(product.definition.id);
                break;
            case "Barking Bad":
                FullCostume(product.definition.id);
                break;
                #endregion

                #region HEAD COSTUME
            case "Hipster Fedora":
                HeadCostume("Fedora Hat");
                break;
            case "Panda Hat":
                HeadCostume(product.definition.id);
                break;
            case "Reverse Cap":
                HeadCostume(product.definition.id);
                break;
                #endregion

                #region BODY COSTUME
            case "Cat Collar":
                BodyCostume(product.definition.id);
                break;
            case "Dog Collar":
                BodyCostume(product.definition.id);
                break;
            case "Bowtie":
                BodyCostume(product.definition.id);
                break;
            case "Scarf":
                BodyCostume(product.definition.id);
                break;
                #endregion

                #region ACCESSORY COSTUME
            case "Shades":
                AccessoryCostume(product.definition.id);
                break;
            case "Geek Glasses":
                AccessoryCostume(product.definition.id);
                break;
            case "Pink Side Flower":
                AccessoryCostume(product.definition.id);
                break;
            case "Red Side Flower":
                AccessoryCostume(product.definition.id);
                break;
                #endregion

                #region BACKGROUND
            case "Space":
                Background(product.definition.id);
                break;
            case "Japan":
                Background(product.definition.id);
                break;
                #endregion

                #region HAMMERS
            case "Silver Hammer":
                Hammer(product.definition.id);
                break;
            case "Gold Hammer":
                Hammer(product.definition.id);
                break;
                #endregion

                #region DOG
            case "Husky":
                Dog(product.definition.id);
                break;
            case "Pug":
                Dog(product.definition.id);
                break;
            case "Rottweiler":
                Dog(product.definition.id);
                break;
            case "Yorkie":
                Dog(product.definition.id);
                break;
            case "Chow Chow":
                Dog(product.definition.id);
                break;
            case "Poodle":
                Dog(product.definition.id);
                break;
            case "Corgi":
                Dog(product.definition.id);
                break;
            case "Akita":
                Dog(product.definition.id);
                break;            
                #endregion

                #region CAT
            case "Siamese":
                Cat(product.definition.id);
                break;
            case "Maine Coon":
                Cat(product.definition.id);
                break;
            case "Bengal":
                Cat(product.definition.id);
                break;
            case "Sphynx":
                Cat(product.definition.id);
                break;
            case "Himalayan":
                Cat(product.definition.id);
                break;
            case "Munchkin":
                Cat(product.definition.id);
                break;
            case "Ragdoll":
                Cat(product.definition.id);
                break;
            case "Bombay":
                Cat(product.definition.id);
                break;
                #endregion
            default:
                break;
        }

    }

    public void RestoreScene()
    {        
        SceneManager.LoadScene("Restore");
    }

    public void Back()
    {
        SceneManager.LoadScene("Main");
    }

    #endregion

    #region STORE ITEMS

    void FullCostume(string product)
    {
        DataController.Instance.headDatabase.items.First(item => item.EquipmentName == product).isOwned = true;
        DataController.Instance.bodyDatabase.items.First(item => item.EquipmentName == product).isOwned = true;
    }

    void HeadCostume(string product)
    {
        DataController.Instance.headDatabase.items.First(item => item.EquipmentName == product).isOwned = true;
    }

    void BodyCostume(string product)
    {
        DataController.Instance.bodyDatabase.items.First(item => item.EquipmentName == product).isOwned = true;
    }

    void AccessoryCostume(string product)
    {
        DataController.Instance.AccessoriesDatabase.items.First(item => item.EquipmentName == product).isOwned = true;
    }

    void Background(string product)
    {
        DataController.Instance.backgroundDatabase.items.First(item => item.name == product).isOwned = true;
    }

    void Hammer(string product)
    {
        DataController.Instance.hammerDatabase.Items.First(item => item.ItemName == product).isOwned = true;
    }

    void Dog(string product)
    {
        DogPet dogPet;
        dogPet = DataController.Instance.dogDatabase.items.First(item => item.petName == product);
        dogPet.isOwned = true;
        DogAdapt(dogPet);
    }

    void Cat(string product)
    {
        CatPet catPet;
        catPet = DataController.Instance.catDatabase.items.First(item => item.petName == product);
        catPet.isOwned = true;
        CatAdapt(catPet);
    }

    #endregion

    #region PETADOPT

    void DogAdapt(DogPet _dogPet)
    {
        PetBase petBase = new PetBase();
        petBase.petType = EnumCollection.Pet.Dog;
        petBase.petName = _dogPet.breedname.ToString().Replace("_", " ");
        petBase.petBreed = _dogPet.breedname.ToString();
        petBase.petCategory = _dogPet.petCategory;
        petBase.hunger_thirst = 0;
        petBase.happiness = 0;
        petBase.energy = 0;
        petBase.cleanliness = 0;
        petBase.accessories = "none";
        petBase.headEquipment = "none";
        petBase.bodyEquipment = "none";
        petBase.isFoodFull = false;

        DogDatabase dogDB = DataController.Instance.dogDatabase;

        // add pet
        DataController.Instance.playerData.playerPetList.Add(petBase);
        //petbase.

        // check if in pomodoro, update pet in background if true
        if(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "Main")
        {
            PetHomeController.ins.SetPet();
        }
    }

    void CatAdapt(CatPet _catPet)
    {
        PetBase petBase = new PetBase();
        petBase.petType = EnumCollection.Pet.Cat;
        petBase.petName = _catPet.breedname.ToString().Replace("_", " ");
        petBase.petBreed = _catPet.breedname.ToString();
        petBase.petCategory = _catPet.petCategory;
        petBase.hunger_thirst = 0;
        petBase.happiness = 0;
        petBase.energy = 0;
        petBase.cleanliness = 0;
        petBase.accessories = "none";
        petBase.headEquipment = "none";
        petBase.bodyEquipment = "none";
        petBase.isFoodFull = false;

        DogDatabase dogDB = DataController.Instance.dogDatabase;

        // add pet
        DataController.Instance.playerData.playerPetList.Add(petBase);
        //petbase.

        // check if in pomodoro, update pet in background if true
        if(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "Main")
        {
            PetHomeController.ins.SetPet();
        }
    }        

    #endregion
}
