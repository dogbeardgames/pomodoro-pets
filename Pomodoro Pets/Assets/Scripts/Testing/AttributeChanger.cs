﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttributeChanger : MonoBehaviour {

    [SerializeField]
    InputField  inputHappiness,
                inputCleanliness,
                inputHunger_Thirst,
                inputEnergy;

    int petIndex;

	// Use this for initialization
	void Start () 
    {
        petIndex = DataController.Instance.playerData.currentPetIndex;
	}
	
    #region MONO

    void OnEnable()
    {
        RetrieveAttributeValues();
    }

    #endregion

    #region METHODS

    void RetrieveAttributeValues()
    {        
        inputHappiness.text = DataController.Instance.playerData.playerPetList[petIndex].happiness.ToString();
        inputCleanliness.text = DataController.Instance.playerData.playerPetList[petIndex].cleanliness.ToString();
        inputHunger_Thirst.text = DataController.Instance.playerData.playerPetList[petIndex].hunger_thirst.ToString();
        inputEnergy.text = DataController.Instance.playerData.playerPetList[petIndex].energy.ToString();
    }

    public void SaveChanges()
    {
        float.TryParse(inputHappiness.text, out DataController.Instance.playerData.playerPetList[petIndex].happiness);
        float.TryParse(inputCleanliness.text, out DataController.Instance.playerData.playerPetList[petIndex].cleanliness);
        float.TryParse(inputHunger_Thirst.text, out DataController.Instance.playerData.playerPetList[petIndex].hunger_thirst);
        float.TryParse(inputEnergy.text, out DataController.Instance.playerData.playerPetList[petIndex].energy);
    }

    #endregion
}
