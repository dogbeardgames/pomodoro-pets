﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class CheckTimeMaster : MonoBehaviour {

	DateTime currentDate;
	DateTime oldDate;



	public string saveLocation;
	public static CheckTimeMaster instance;
	// Use this for initialization
	void Awake () {
	//make an instance
		instance = this;

		//playerprefs
		saveLocation = "lastSavedDate";
	}

	//check current time 
	public int CheckDate()
	{
		currentDate = System.DateTime.Now;

		string tempString = DataController.Instance.playerData.savedTime;
	
		long tempLong = Convert.ToInt64 (tempString);

		DateTime oldDate = DateTime.FromBinary (tempLong);
		//print ("Old date" + oldDate);


		//Use - method and store to timespan

		TimeSpan difference = currentDate.Subtract (oldDate);
		//print ("Difference" + difference);

		return (int)difference.TotalSeconds;
	}
        	
    /// <summary>
    /// Saves the current time.
    /// </summary>
	public void SaveDate()
	{
		//PlayerPrefs.SetString (saveLocation, System.DateTime.Now.ToBinary ().ToString ());
		//print ("Saving this to playerprefs" + System.DateTime.Now);
		DataController.Instance.playerData.savedTime = System.DateTime.Now.ToBinary ().ToString ();

		//Debug.Log (DataController.Instance.playerData.savedTime);
	}
}
