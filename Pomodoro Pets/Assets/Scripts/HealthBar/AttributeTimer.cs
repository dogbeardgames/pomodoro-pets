﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;
using System.IO;

public class AttributeTimer : MonoBehaviour 
{	
	public Dictionary<string,List<string>> enumDictionarytimer = new Dictionary<string,List<string>> ();

	HappinessBar happinessBar = new HappinessBar ();

	//List
	public List<string> conditionalList  = new List<string>();

	public List<List<string>> conditionalListOfList = new List<List<string>> ();

	[SerializeField]
	public float ValueCounter;

	[SerializeField]
	public Image imgBar;

	public float totalValuetoDeduct = 0;
	//deduct happiness bar by total condition
	public float imgBarValue;


	//Timer
	float currCountdownValue;

	public int countValue; // 15 minutes

	int currentIndex;

	float counter = 0;

	int timerCounter = 0;
                                                           
	public bool isEnergyNextToBeAdded = false;

	public static AttributeTimer instance;

	[SerializeField]
	TimeComputationValue timeValue;

	public void Awake()
	{
		
		if(instance == null)
			{
			instance = this;
				DontDestroyOnLoad(gameObject);

			}
			else
			{
				Destroy(gameObject);
			}                          

		SceneManager.sceneLoaded += Scene;

	}
		
	public void Start()
	{          
//		for (int i = 0; i < DataController.Instance.playerData.playerPetList.Count; i++) 
//        {
//			Debug.Log ("<color=green>" +   "Happiness: " + DataController.Instance.playerData.playerPetList [i].happiness + "</color>");
//			Debug.Log ("<color=green>" +  "hunger: " + DataController.Instance.playerData.playerPetList [i].hunger_thirst + "</color>");
//			Debug.Log ("<color=green>" +  "clean: " + DataController.Instance.playerData.playerPetList [i].cleanliness + "</color>");
//			Debug.Log ("<color=green>" +  "energy: " + DataController.Instance.playerData.playerPetList [i].energy + "</color>");
//		}            
        instance = this;

		healthChecker ();		

		LanguageCheckDropdown ();
		//DeductfromStart ();
		//AssignPetLoveMeter (DataController.Instance.playerData.currentPetIndex);
		//counter = TimeComputationValue.instance.cycle_counter;

		//Debug.Log ("Hello + " + DataController.Instance.playerData.cycle_counter_timer);
		counter = DataController.Instance.playerData.cycle_counter_timer;		
		InitializePetAttributeList ();
		SceneLoaded ();


		//Pet Love Meter
		//AssignPetLoveMeter (DataController.Instance.playerData.currentPetIndex);

		//RotatePawIcon.instance.GaugeBarTwo();

		//DeductfromStart ();

	}        

	void Update()
	{
		GetQuitValue (DataController.Instance.playerData.currentPetIndex);
	}

	// scene event
	void SceneLoaded()
	{		
		StartCoroutine ("StartCountdownAll");
	}
		
	void Scene(Scene scene, LoadSceneMode mode)
	{
		if (scene.name == "Pets" || scene.name == "Main") {
			//Find happiness bar
			if(imgBar == null)
			{
				//imgBar = GameObject.FindGameObjectWithTag("HappinessBar").GetComponent<Image>();

				totalValuetoDeduct = 0.10f;

				//float fill = counter * totalValuetoDeduct;
				//imgBarValue = 1 - fill;
				//mgBar.fillAmount = imgBarValue;

				//imgBar.fillAmount -= totalValuetoDeduct;
			}
		}
	}

	//Timer code
	public IEnumerator StartCountdownAll()
	{
		//ResetList ();
		//Debug.Log("Start Count down " + currCountdownValue);
		currCountdownValue = countValue;

		//Debug.Log ("Count Value:" + countValue);

		while (currCountdownValue > -1)
		{	
			//Debug.Log (currCountdownValue);
			if (currCountdownValue == 0) {
				AllpetTimer ();//all pet
			} 
			yield return new WaitForSeconds(1.0f);
			currCountdownValue--;
		}

	}		

	//Reset Timer
	public void AllpetTimer()
	{

		//if(QuitValue()<10)
		//{
			
			counter += 1;
			currCountdownValue=countValue;
			ValueCounter=counter;

			//PetCondition (counter);

			PetCondition();
		//}

	}
		
	public void PetCondition() //conditionCounter -> number of cycle when ignoring pet for thier needs.
	{
		string xList = "";
		//ResetList ();


		if (DataController.Instance.playerData.playerPetList.Count > 0) 
        {			
			for (int i = 0; i < DataController.Instance.playerData.playerPetList.Count; i++) 
            {	
				ResetList (i);
				if (DataController.Instance.playerData.playerPetList [i].quitvalue != 10) 
                {
				
					//Debug.Log ("Index of Pet:" + i);
					if (DataController.Instance.playerData.playerPetList [i].cyclevalue % 5 != 0 && isEnergyNextToBeAdded == false) 
                    { // parent if condition

						//if (DataController.Instance.playerData.playerPetList [i].quitvalue != 10) {

						int lIndex = Random.Range (0, conditionalListOfList [i].Count);
						//int lIndex = Random.Range (0, conditionalList.Count);
						xList = conditionalListOfList [i] [lIndex];

						if (xList == "Happiness") 
                        {   // 0 for happiness
							//increase happiness counter
							//conditionalList.RemoveAt (0);


							//happiness_counter = DataController.Instance.playerData.playerPetList [i].happiness + 1;
							//DataController.Instance.playerData.playerPetList [i].happiness = happiness_counter ;

							DataController.Instance.playerData.playerPetList [i].happiness += 1;

							GetQuitValue (i);//add stats to get quit Value
							GetCycleValue (i);

							//Debug.Log ("Cycle" + DataController.Instance.playerData.playerPetList [i].cyclevalue + ":" + "List count left " + conditionalList.Count);
							//Debug.Log ("<Color=red>" + DataController.Instance.playerData.playerPetList [i].happiness + " Happiness Selected, index " + i + "</color>");


							//CheckList ();



						} 
                        else if (xList == "Cleanliness") 
                        {
                            //1 for cleanliness

							//increase Cleanliness counter
							//conditionalList.Remove (xList);

							//cleanliness_counter = DataController.Instance.playerData.playerPetList [i].cleanliness + 1;
							//DataController.Instance.playerData.playerPetList [i].cleanliness = cleanliness_counter ;
							DataController.Instance.playerData.playerPetList [i].cleanliness += 1;

							GetQuitValue (i);//add stats to get quit Value
							GetCycleValue (i);

							//Debug.Log ("Cycle" + DataController.Instance.playerData.playerPetList [i].cyclevalue + ":" + "List count left " + conditionalList.Count);
							//Debug.Log ("<Color=red>" + DataController.Instance.playerData.playerPetList [i].cleanliness + " Cleanliness Selected, index " + i + "</color>");
							//CheckList ();




						} 
                        else if (xList == "Hunger") 
                        {
                            //2 for hunger
							//conditionalList.RemoveAt (2);
							//increase Hunger counter
							//conditionalList.Remove (xList);

							//hunger_counter = DataController.Instance.playerData.playerPetList [i].hunger_thirst + 1;
							//DataController.Instance.playerData.playerPetList [i].hunger_thirst = hunger_counter ;
							DataController.Instance.playerData.playerPetList [i].hunger_thirst += 1;
							GetQuitValue (i);//add stats to get quit Value
							GetCycleValue (i);

							//Debug.Log ("Cycle" + DataController.Instance.playerData.playerPetList [i].cyclevalue + ":" + "List count left " + conditionalList.Count);
							//Debug.Log ("<Color=red>" + DataController.Instance.playerData.playerPetList [i].hunger_thirst + " Hunger Selected, index " + i + "</color>");
							//CheckList ();
						} 

					} 
                    else 
                    { 
                        //if(conditionCounter % 5 ==0 && isEnergyNextToBeAdded==true) 
						isEnergyNextToBeAdded = true;
						//Debug.Log ("Cycle" + DataController.Instance.playerData.playerPetList [i].cyclevalue + "List count left " + conditionalList.Count);

						//energyCounter = DataController.Instance.playerData.playerPetList [i].energy + 1;
						//DataController.Instance.playerData.playerPetList [i].energy = energyCounter ;
						DataController.Instance.playerData.playerPetList [i].energy += 1;
						GetQuitValue (i);//add stats to get quit Value
						GetCycleValue (i);

						isEnergyNextToBeAdded = false;
						//Debug.Log ("<color=blue>" + "Energy Counter:" + DataController.Instance.playerData.playerPetList [i].energy + "</color>");
					}

					conditionalListOfList [i].Remove (xList);
					//Pet Love Meter
					AssignPetLoveMeter();
					healthChecker ();
					//Notify user when healthbar is decrease to 75%
					NotifyWhenlessSeventyFive ();                   
				}
			}	
	    }
	}

	public void EnergyValue()
	{
		isEnergyNextToBeAdded = false;
	//	Debug.Log ("<color=blue>" + "Energy Counter:" + DataController.Instance.playerData.playerPetList[index].energy+ "</color>");
	}

	private void InitializePetAttributeList()
	{
		for (int i = 0; i < DataController.Instance.playerData.playerPetList.Count; i++) 
        {
			List<string> attribute = new List<string> ();
			attribute.Add ("Happiness");
			attribute.Add ("Cleanliness");
			attribute.Add ("Hunger");
			conditionalListOfList.Add (attribute);		
		}
	}

	public void ResetList(int index)
	{   
		if (conditionalListOfList [index].Count == 0) 
        {
			List<string> attribute = new List<string> ();
			attribute.Add ("Happiness");
			attribute.Add ("Cleanliness");
			attribute.Add ("Hunger");
			conditionalListOfList [index] = attribute;
		}
			
	}
	public void CheckList()
	{
		foreach (string mylist in conditionalList) 
        {
			//Debug.Log (mylist);
		}
	}

	void OnDisable()
	{
		// reset to 1 to avoid limitation of integer
		for (int i = 0; i < DataController.Instance.playerData.playerPetList.Count; i++)
        {
			if(DataController.Instance.playerData.playerPetList [i].cyclevalue == 20) 
			DataController.Instance.playerData.playerPetList [i].cyclevalue = 1;
		}
	}

	void GetQuitValue(int index)
	{
		float clean     = DataController.Instance.playerData.playerPetList [index].cleanliness;
		float hunger    = DataController.Instance.playerData.playerPetList [index].hunger_thirst;
		float happiness = DataController.Instance.playerData.playerPetList [index].happiness;
		float energy    = DataController.Instance.playerData.playerPetList [index].energy;
		float total = clean + hunger + happiness + energy;


		//if (DataController.Instance.playerData.playerPetList [index].quitvalue < 10) 
        //{
		DataController.Instance.playerData.playerPetList [index].quitvalue = total;
		if (DataController.Instance.playerData.playerPetList [index].quitvalue > 10f) 
		{
			DataController.Instance.playerData.playerPetList [index].quitvalue = 10;
		}
		//}
		//Debug.Log (DataController.Instance.playerData.playerPetList [index].petName);
	}

	private float QuitValue()
	{
		float qvalue = 0f;

		for(int x = 0; x<DataController.Instance.playerData.playerPetList.Count; x++)
		{
			qvalue = DataController.Instance.playerData.playerPetList[x].quitvalue;
		}

		return qvalue;
	}

	void GetCycleValue(int index)
	{
		DataController.Instance.playerData.playerPetList [index].cyclevalue +=1;

		//Debug.Log ("QuitValue Added");
	}

	public void AssignPetLoveMeter()
	{
        PetBase pBase = new PetBase();
		//List<PetBase> pBase = new List<PetBase> ();

		//pBase = DataController.Instance.playerData.playerPetList.Where(x=>x.petIndex == petIndex).ToList();
        pBase = DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex];

		float cleanliness = 0;
		float happiness   = 0;
		float hunger      = 0;
		float energy      = 0;

        cleanliness   = pBase.cleanliness;
        happiness     = pBase.happiness;
        hunger        = pBase.hunger_thirst;
        energy        = pBase.energy;

//		for (int i = 0; i < pBase.Count(); i++) {
//			
//			cleanliness   = pBase[i].cleanliness;
//			happiness     = pBase[i].happiness;
//			hunger        = pBase[i].hunger_thirst;
//			energy        = pBase[i].energy;
//
//			Debug.Log (cleanliness + " " + " " +  happiness + " " + hunger + " " + energy);
//
//		}
			
		float totalStats = cleanliness + happiness + hunger + energy;
		float totalgaugededuct = totalStats * 10; 

		//Debug.Log (totalStats);
		totalStats = totalStats * 0.10f;

		try {
			//imgBar.fillAmount = 1 - totalStats ;
			//RotatePawIcon.instance.BarValue -=  10; 

			RotatePawIcon.instance.BarValue = 100 - totalgaugededuct;
		} 
        catch (System.Exception ex) 
        {
			
		}

        RotatePawIcon.instance.UpdatePetMeter();
	}

	void NotifyWhenlessSeventyFive()
	{

		float cleanliness = 0;
		float happiness   = 0;
		float hunger      = 0;
		float energy      = 0;

		for (int i = 0; i < DataController.Instance.playerData.playerPetList.Count; i++) {

			cleanliness = DataController.Instance.playerData.playerPetList [i].cleanliness;
			hunger = DataController.Instance.playerData.playerPetList [i].hunger_thirst;
			energy = DataController.Instance.playerData.playerPetList [i].energy;
			happiness = DataController.Instance.playerData.playerPetList [i].happiness;
		}

		float total = cleanliness + hunger + energy + happiness;
		float totalpercent = total * 10;

		//get Total duration of session duration then pass it to notification params (delay time)
		float duration = DataController.Instance.settings.shortBreakDuration;
		float totalDurationNotifsBreak = duration * 60;
		//Debug.Log ((long)totalDurationNotif + "Duration long");

		List<string> notifbreaksessionFinished = new List<string>();// language
		//LoadEnumJson("localizedTextTheme_sp.json");
		//var themeDic = enumDictionarytimer.Where (y => y.Key == "set_theme").ToDictionary (y => y.Key, y => y.Value);
		var notifbreakfinished = enumDictionarytimer.Where (y => y.Key == "notif_petsnotif").ToDictionary (y => y.Key, y => y.Value);
		foreach (List<string> listlongbreak in notifbreakfinished.Values) 
        {
			foreach ( var notifBreakS in listlongbreak) 
            {
				notifbreaksessionFinished.Add (notifBreakS);
			}
		}

		string notifBF = "";
		for (int i = 0; i < notifbreaksessionFinished.Count; i++) 
        {
			notifBF = notifbreaksessionFinished [i]; 
		}

	
		if (totalpercent == 75) 
        {
			//Save Task duration here
			//PlainNotification.SendNotification((int)Time.deltaTime * 1, (long)totalDurationNotifsBreak, "Pomodoro Pets", "Break Session finished. Tap to continue to next session", new Color32(0xff, 0x44, 0x44, 255), true, true, true, "app_icon");
			PlainNotification.SendNotification((int)Time.deltaTime * 1, 1, "Pomodoro Pets", notifBF, new Color32(0xff, 0x44, 0x44, 255), true, true, true, "app_icon");

			//IOS
			#if UNITY_IOS
			LocalNotificationIos.SetNotification("notifBF",30, 10);
			#endif
		}            
	}
		
	//Enum Collection json file load
	public void LoadEnumJson(string fileName)
	{
		string filePath = "";

		if (Application.platform == RuntimePlatform.Android)
		{
			// Android
			filePath = System.IO.Path.Combine(Application.streamingAssetsPath, fileName);

			// Android only use WWW to read file
			WWW reader = new WWW(filePath);
			while ( ! reader.isDone) {}

			string realPath = Application.persistentDataPath + fileName;
			System.IO.File.WriteAllBytes(realPath, reader.bytes);

			filePath = realPath;
		}
		else
		{
			// iOS
			filePath = System.IO.Path.Combine(Application.streamingAssetsPath, fileName);
		}

		if (File.Exists (filePath)) {
			//Debug.Log ("Test");
			string dataAsJson = File.ReadAllText (filePath);
			LocalizationDataList enumData = JsonUtility.FromJson<LocalizationDataList> (dataAsJson);

			for (int i = 0; i < enumData.items.Length; i++) 
			{
				List<string> enumlist = new List<string> ();
				for (int j = 0; j < enumData.items[i].value.Length; j++) {


					//Debug.Log ("<color=green>" + "Key: " +    enumData.items [i].key +    " ALL enum data: " + enumData.items [i].value[j] + "</color>");	


					enumlist.Add (enumData.items [i].value [j]); 
				}

				enumDictionarytimer.Add (enumData.items [i].key, enumlist);

				//enumlist.Add (enumData.items[i].value);
			}
			//			for (int i = 0; i < enumlist.Count; i++) {
			//				
			//				Debug.Log ("EnumList " + enumlist[i]);
			//			}
			//for (int i = 0; i < enumDictionary.Count; i++) {
			//	Debug.Log ("EnumDictionary Count: " + enumDictionary.Count);
			//}

			// Debug.Log ("Data loaded, dictionary contains: " + localizedText.Count + " entries");
		} else 
		{
			//Debug.LogError (filePath);
			//Debug.LogError ("Cannot find file!");
		}
	}

	void LanguageCheckDropdown()
	{
		//Localized dropdown depends on language selected in settings ***In progress 4/5/2016

		if (DataController.Instance.settings.languageIndex == 0 ) { // English
			LoadEnumJson("localizedtextDropdown_en.json");
			//Debug.Log("English");
		}
		else if (DataController.Instance.settings.languageIndex == 1 ) { // Spain
			LoadEnumJson("localizedtextDropdown_sp.json");
			//Debug.Log("Spanish");
		}
	}        

	public void healthChecker()
	{

		//Debug.Log ("<color=green>" + "health checker" +  "</color>");
		float  happines,cleanliness,hunger,energy,total,total1,total2 = 0;

		for (int i = 0; i < DataController.Instance.playerData.playerPetList.Count; i++) {

			happines = DataController.Instance.playerData.playerPetList [i].happiness;
			cleanliness = DataController.Instance.playerData.playerPetList [i].cleanliness;
			hunger = DataController.Instance.playerData.playerPetList [i].hunger_thirst;
			energy = DataController.Instance.playerData.playerPetList [i].energy;
			total = happines + cleanliness + hunger + energy;
			total1 = total  * 10f;
			total2 = 100 - total1;
			//Debug.Log (total2);
		}

		if (total2 < 50) 
        {
			AchievementConditions.instance.HealthyPetCounterResetter();
		} 
        else 
        {		
//comment by job	
//			//call healthy pets method
//			try 
//            {
//				AchievementConditions.instance.HealthyPets();
//			} 
//            catch (System.Exception ex) 
//            {
//				
//			}
		}

	}                
}              