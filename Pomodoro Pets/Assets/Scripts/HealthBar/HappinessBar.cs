﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HappinessBar:MonoBehaviour {
	
    float fillAmount = 0.10f;
	public static HappinessBar instance;

	void Start()
	{
		instance = this;
	}

	public void ChangeBarValue_Abandoned(float imgbarValue,float valueDeduct,Image happyBar)
	{
		ChangeColor (imgbarValue,happyBar);
		happyBar.fillAmount -= valueDeduct;
	}
		
	void ChangeColor(float value,Image happyBar)//change the  color of bar depends on value
	{
		if (value > 0.50f) {
			happyBar.color = Color.green;
		} else {
			happyBar.color = Color.red;
		}
	}

	//
}



