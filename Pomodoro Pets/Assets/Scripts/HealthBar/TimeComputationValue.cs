﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class TimeComputationValue : MonoBehaviour {

	public float timer;
	public bool isEnergyNextToBeAdded =false;
	public float divided_value;

	public  int cycle_counter;


	int currentIndex; 


	[SerializeField]
	Text timerDisplay;

	[SerializeField]
	Image happyBarImage;

	public bool isQuitTimeAdded = false;

	public static TimeComputationValue instance;

	void Start()
	{
		InitializePetAttributeList ();

		//int BarValuetest = 100;

		currentIndex= DataController.Instance.playerData.currentPetIndex;

		float counter = QuitValue ();

		cycle_counter = 0;

		instance = this;

		//BarValue = happyBarImage.fillAmount; 

		//Update real time
	//	timer = CheckTimeMaster.instance.CheckDate();

		timer = TimeSave.instance.savedTime;

		//Debug.Log ("Timer Value" + timer);
		//Debug.Log ("Count Value: " + AttributeTimer.instance.countValue);

		if (DataController.Instance.playerData.isnewInstalled == true) {
			divided_value = 0;
		} else {
			divided_value = timer / AttributeTimer.instance.countValue;
		}
			
		//Debug.Log ("Divided Value: " + (int)divided_value);

		int u = 0;
		while (u < 10) {
			u++;	


				if (cycle_counter < (int)divided_value) {
					cycle_counter = cycle_counter + 1;
					counter = counter + 1;
					//if (QuitValue()<10) {
					//Debug.Log (BarValuetest);
					//AssignValue (counter); 
					AssignValue ();

				}
			}

	}

	//Code base for deducting happiness bar when game is resume after closing
	public void AssignValue() //conditionCounter -> number of cycle when ignoring pet for thier needs.
	{
		string xList = "";
		//ResetList ();
		if (DataController.Instance.playerData.isnewInstalled == false) {
			
		for (int i = 0; i < DataController.Instance.playerData.playerPetList.Count; i++) {		
 
			if (DataController.Instance.playerData.playerPetList [i].quitvalue < 10) {
				ResetList (i);
				//Debug.Log ("Index of Pet:" + i);

				if (DataController.Instance.playerData.playerPetList [i].cyclevalue % 5 != 0 && isEnergyNextToBeAdded == false) { // parent if condition

					int lIndex = Random.Range (0, AttributeTimer.instance.conditionalListOfList [i].Count);
					//int lIndex = Random.Range (0, conditionalList.Count);
					xList = AttributeTimer.instance.conditionalListOfList [i] [lIndex];

					if (xList == "Happiness") {// 0 for happiness
						//increase happiness counter
						//conditionalList.RemoveAt (0);


						//happiness_counter = DataController.Instance.playerData.playerPetList [i].happiness + 1;
						//DataController.Instance.playerData.playerPetList [i].happiness = happiness_counter ;
						DataController.Instance.playerData.playerPetList [i].happiness += 1;
						GetQuitValue (i);
						GetCycleValue (i);



						//Debug.Log ("Cycle" + DataController.Instance.playerData.playerPetList [i].cyclevalue + ":" + "List count left " + AttributeTimer.instance.conditionalList.Count);
						//Debug.Log ("<Color=red>" + DataController.Instance.playerData.playerPetList [i].happiness + " Happiness Selected, index " + i + "</color>");

						//AssignPetLoveMeter(6);
						//CheckList ();

					} else if (xList == "Cleanliness") {//1 for cleanliness

						//increase Cleanliness counter
						//conditionalList.Remove (xList);

						//cleanliness_counter = DataController.Instance.playerData.playerPetList [i].cleanliness + 1;
						//DataController.Instance.playerData.playerPetList [i].cleanliness = cleanliness_counter ;
						DataController.Instance.playerData.playerPetList [i].cleanliness += 1;
						GetQuitValue (i);
						GetCycleValue (i);



						//Debug.Log ("Cycle" + DataController.Instance.playerData.playerPetList [i].cyclevalue + ":" + "List count left " + AttributeTimer.instance.conditionalList.Count);
						//Debug.Log ("<Color=red>" + DataController.Instance.playerData.playerPetList [i].cleanliness + " Cleanliness Selected, index " + i + "</color>");
						//CheckList ();
						//AssignPetLoveMeter(6);

					} else if (xList == "Hunger") {//2 for hunger
						//conditionalList.RemoveAt (2);
						//increase Hunger counter
						//conditionalList.Remove (xList);

						//hunger_counter = DataController.Instance.playerData.playerPetList [i].hunger_thirst + 1;
						//DataController.Instance.playerData.playerPetList [i].hunger_thirst = hunger_counter ;
						DataController.Instance.playerData.playerPetList [i].hunger_thirst += 1;
						GetQuitValue (i);
						GetCycleValue (i);

						//Debug.Log ("Cycle" + DataController.Instance.playerData.playerPetList [i].cyclevalue + ":" + "List count left " + AttributeTimer.instance.conditionalList.Count);
						//Debug.Log ("<Color=red>" + DataController.Instance.playerData.playerPetList [i].hunger_thirst + " Hunger Selected, index " + i + "</color>");
						//CheckList ();
						//AssignPetLoveMeter(6);
					} 

				} else { //if(conditionCounter % 5 ==0 && isEnergyNextToBeAdded==true) 
					isEnergyNextToBeAdded = true;
					//Debug.Log ("Cycle" + DataController.Instance.playerData.playerPetList [i].cyclevalue + "List count left " + AttributeTimer.instance.conditionalList.Count);

					//energyCounter = DataController.Instance.playerData.playerPetList [i].energy + 1;
					//DataController.Instance.playerData.playerPetList [i].energy = energyCounter ;
					DataController.Instance.playerData.playerPetList [i].energy += 1;
					GetQuitValue (i);
					GetCycleValue (i);


					isEnergyNextToBeAdded = false;
					//Debug.Log ("<color=blue>" + "Energy Counter:" + DataController.Instance.playerData.playerPetList [i].energy + "</color>");

				}

				AttributeTimer.instance.conditionalListOfList [i].Remove (xList);

			}


		}
	}
		//AssignPetLoveMeter(6);	
	}

	public void EnergyValue()
	{
		isEnergyNextToBeAdded = false;
		//	Debug.Log ("<color=blue>" + "Energy Counter:" + DataController.Instance.playerData.playerPetList[index].energy+ "</color>");
	}

	private void InitializePetAttributeList()
	{
		for (int i = 0; i < DataController.Instance.playerData.playerPetList.Count; i++) {
			List<string> attribute = new List<string> ();
			attribute.Add ("Happiness");
			attribute.Add ("Cleanliness");
			attribute.Add ("Hunger");
			AttributeTimer.instance.conditionalListOfList.Add (attribute);
			//Debug.Log ("add");
		}
	}

	public void ResetList(int index)
	{   
		if (AttributeTimer.instance.conditionalListOfList [index].Count == 0) {
			List<string> attribute = new List<string> ();
			attribute.Add ("Happiness");
			attribute.Add ("Cleanliness");
			attribute.Add ("Hunger");
			AttributeTimer.instance.conditionalListOfList [index] = attribute;
		}

	}
	public void CheckList()
	{
		foreach (string mylist in AttributeTimer.instance.conditionalList) {
			//Debug.Log (mylist);
		}
	}

	void OnDisable()
	{
//		Debug.Log ("Saving data...");
//		Debug.Log ("Debug");
		//DataController.Instance.SavePlayerData();
		//SaveStats.instance.SaveHealthBarOnPet ();
	}

	private float QuitValue()
	{
		float qvalue=0f;
		for(int x=0;x<DataController.Instance.playerData.playerPetList.Count;x++)
		{
			qvalue = DataController.Instance.playerData.playerPetList[x].quitvalue;
		}

		return qvalue;
	}

	void GetCycleValue(int index)
	{


		DataController.Instance.playerData.playerPetList [index].cyclevalue +=1;



		//Debug.Log ("QuitValue Added");
	}

	void GetQuitValue(int index)
	{

		if (DataController.Instance.playerData.playerPetList [index].quitvalue < 10) {
			DataController.Instance.playerData.playerPetList [index].quitvalue +=1;
		}


		//Debug.Log ("QuitValue Added");
	}

//	private float QuitValue()
//	{
//		float qvalue=0f;
//		for(int x=0;x<DataController.Instance.playerData.playerPetList.Count;x++)
//		{
//			qvalue = DataController.Instance.playerData.playerPetList[x].quitvalue;
//		}
//
//		return qvalue;
//	}
//
//	void GetQuitValue(int index)
//	{
//		DataController.Instance.playerData.playerPetList [index].quitvalue = cycle_counter;
//
//		DataController.Instance.playerData.cycle_counter_timer =  cycle_counter;
//
//		Debug.Log ("QuitValue Added");
//
//		//DataController.Instance.playerData.playerPetList [index].quitvalue = counter;
//	}

	void OnApplicationQuit()
	{
		//CheckTimeMaster.instance.SaveDate ();
	}  

//	void AssignPetLoveMeter(int petIndex)
//	{
//		List<PetBase> pBase = new List<PetBase> ();
//
//		pBase = DataController.Instance.playerData.playerPetList.Where(x=>x.petIndex == petIndex).ToList();
//
//		float cleanliness = 0;
//		float happiness   = 0;
//		float hunger      = 0;
//		float energy      = 0;
//
//		for (int i = 0; i < pBase.Count(); i++) {
//
//			cleanliness   = pBase[i].cleanliness;
//			happiness     = pBase[i].happiness;
//			hunger        = pBase[i].hunger_thirst;
//			energy        = pBase[i].energy;
//
//			//Debug.Log (cleanliness + " " + " " +  happiness + " " + hunger + " " + energy);
//
//		}
//
//		float totalStats = cleanliness + happiness + hunger + energy;
//		totalStats = totalStats * 0.010f;
//		//GetComponent<AttributeTimer>().imgBar.fillAmount -= totalStats ;
//
//	}
}
