﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PawIconPosition : MonoBehaviour {

	public Image image;
	private RectTransform edgeRect;
	private RectTransform imgRect;

	public static PawIconPosition instance;
	public void Awake()
	{
		instance = this;
		imgRect = GetComponent<RectTransform> ();
		edgeRect = transform.GetChild (0).GetComponent<RectTransform> ();

	}

	public void PawIconPositionBar ()
	{
		edgeRect.localPosition = new Vector2 (image.fillAmount * imgRect.rect.width + 70, edgeRect.localPosition.y);
		//Debug.Log ("Updating local position " + image.fillAmount );

		//edgeRect.anchorMin = new Vector2 (edgeRect.anchorMin.x, image.fillAmount);
		//edgeRect.anchorMax = new Vector2 (edgeRect.anchorMax.x,image.fillAmount);
		//edgeRect.pivot = new Vector2(edgeRect.pivot.x,edgeRect.pivot.y);
		//edgeRect.anchoredPosition = Vector2.zero;
	}
}
