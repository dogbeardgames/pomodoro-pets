﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class CheckTimeMaster1 : MonoBehaviour {

	DateTime currentDate;
	DateTime oldDate;



	public string saveLocation;
	public static CheckTimeMaster1 instance;
	// Use this for initialization
	void Awake () {
	//make an instance
		instance = this;

		//playerprefs
		saveLocation = "lastSavedDate";
	}

	//check current time 
	public int CheckDate()
	{
		currentDate = System.DateTime.Now;

		string tempString = PlayerPrefs.GetString (saveLocation, "1");
	
		long tempLong = Convert.ToInt64 (tempString);

		DateTime oldDate = DateTime.FromBinary (tempLong);
		//print ("Old date" + oldDate);


		//Use - method and store to timespan

		TimeSpan difference = currentDate.Subtract (oldDate);
		//print ("Difference" + difference);

		return (int)difference.TotalSeconds;
	}

	//Saves the current time.
	public void SaveDate()
	{
		PlayerPrefs.SetString (saveLocation, System.DateTime.Now.ToBinary ().ToString ());
		//print ("Saving this to playerprefs" + System.DateTime.Now);
	}
}
