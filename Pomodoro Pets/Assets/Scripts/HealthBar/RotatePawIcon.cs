﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class RotatePawIcon : MonoBehaviour {
	


	[SerializeField]
	Image gaugeBar;

	[SerializeField]
	Text percentage;

	public RectTransform edgeRect;
	public RectTransform pawIcon;
	public float BarValue;

	public static RotatePawIcon instance;

	void Start()
	{		     	

//        GaugeInitialValue;

		instance = this;

        UpdatePetMeter();
	}


//	void Update()
//	{
//		GaugeBarTwo ();
//	}

    void LateUpdate()
    {
        UpdatePetMeter();
    }

	bool isBarvaluelesshundred=false;

	public void GaugeBarTwo()
	{        
		if (BarValue < 0) 
        {
			BarValue = 0f;
		} else if (BarValue > 100) 
        {
			BarValue = 100f;
		}


		if (BarValue < 100) 
        {
			isBarvaluelesshundred = true;
		}

		//edgeRect.localPosition = new Vector2 (pawIcon.fillAmount * imgRect.anchorMin.y,  edgeRect.anchoredPosition.x);
		//float fillAmount = gaugeBar.fillAmount;
		//float fillAmount = (gaugeBar.fillAmount  / 100.0f) * 190.0f/360;
		float amount = (BarValue / 100.0f) * 180.0f / 360;

		gaugeBar.fillAmount = amount;
        percentage.GetComponent<Text>().text = Mathf.FloorToInt(BarValue) + "%";


		float buttonAngle = amount * 310;

		try 
        {
			edgeRect.localEulerAngles = new Vector3 (0, 0, -buttonAngle);
			pawIcon.eulerAngles = new Vector3 (0f, 0f, 0f);
		} 
        catch (System.Exception ex) 
        {
			
		}
		//Pure Happiness achievement
		try {

			if(isBarvaluelesshundred == true)
			{
				if(BarValue == 100)
				{
					AchievementConditions.instance.PureHappiness();
					isBarvaluelesshundred = false;
				}
			}
				
		} 
        catch (System.Exception ex) 
        {
			
		}
	}


	float  GetPetAttributes()
	{
        PetBase pBase = new PetBase();

        /*pBase = DataController.Instance.playerData.playerPetList.Where(x=>x.petIndex == DataController.Instance.playerData.currentPetIndex).ToList();*/
        pBase = DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex];

		float cleanliness = 0;
		float happiness   = 0;
		float hunger      = 0;
		float energy      = 0;

        cleanliness   = pBase.cleanliness;
        happiness     = pBase.happiness;
        hunger        = pBase.hunger_thirst;
        energy        = pBase.energy;

//		for (int i = 0; i < pBase.Count(); i++) {
//
//			cleanliness   = pBase[i].cleanliness;
//			happiness     = pBase[i].happiness;
//			hunger        = pBase[i].hunger_thirst;
//			energy        = pBase[i].energy;
//
//			//Debug.Log (cleanliness + " " + " " +  happiness + " " + hunger + " " + energy);
//		}

		float totalStats = cleanliness + happiness + hunger + energy;
		float totalgaugededuct = totalStats * 10; 

		return  100-totalgaugededuct; 
	}


    float PreviousBarValue = 0;
    /// <summary>
    /// Updates the pet meter.
    /// </summary>
    /// 
    public void UpdatePetMeter()
    {                
        BarValue = GetPetAttributes();
        GaugeBarTwo();

        if(PreviousBarValue != BarValue && SpinePetMeter.heartInstance == null)
        {            
            PreviousBarValue = BarValue;

            // spine pet meter animation
            SpineMeterAnimation((int)BarValue);
        }    
    }
        
    void SpineMeterAnimation(int totalAttribute)
    {        
//        Debug.Log("<color=red>Pet Meter!</color>");

        //call evvent if meter is equal or below 70% -KIT
        if(totalAttribute <= 70)
        {
//            Debug.Log("<color=red>Below 70!!! " + totalAttribute + "</color>");
            if(onPetMeterBelow70 != null)
            {
                onPetMeterBelow70();
            }
        }
        else
        {
//            Debug.Log("<color=red>Above 70!!!</color>");
            if(onPetMeterAbove70 != null)
            {
                onPetMeterAbove70();
            }
        }
    }

    #region EVENTS

    public delegate void PetMeterDelegate();
    public static event PetMeterDelegate onPetMeterBelow70;
    public static event PetMeterDelegate onPetMeterAbove70;

    #endregion
}
