﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonType : MonoBehaviour {

    const string themeOjbect = "Button";

    void Start()
    {
        GetComponent<Image>().color = BackgroundThemeController.Instance.ThemeColor(themeOjbect);
    }
}
