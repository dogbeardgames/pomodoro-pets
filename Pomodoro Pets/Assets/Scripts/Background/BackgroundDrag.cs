﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BackgroundDrag : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

    float distance;
    float start;
    public static bool canDragBG = true;

	// Use this for initialization
	void Start () {        
        //Debug.Log("canvas width " + FindObjectOfType<Canvas>().GetComponent<RectTransform>().rect.width);
        distance = FindObjectOfType<Canvas>().GetComponent<RectTransform>().rect.width / 4;
        //Debug.Log("distance " + distance);
	}	

    #region IBeginDragHandler implementation

    public void OnBeginDrag(PointerEventData eventData)
    {
        if(canDragBG)
            start = eventData.position.x;
    }

    #endregion

    #region IDragHandler implementation

    public void OnDrag(PointerEventData eventData)
    {
        //Debug.Log("Dragging");
    }

    #endregion

    #region IEndDragHandler implementation

    public void OnEndDrag(PointerEventData eventData)
    {
		if(PetUIController.ACTION != PetUIController.PET_ACTION.SLEEP &&
			PetUIController.ACTION != PetUIController.PET_ACTION.SWING &&
			PetUIController.ACTION != PetUIController.PET_ACTION.SPA && 
			PetUIController.ACTION != PetUIController.PET_ACTION.FEED)
		{
			if (start < eventData.position.x && canDragBG)
			{
				//Debug.Log("Drag right");
				if(Mathf.Abs(start - eventData.position.x) >= distance)
				{
					// is there bubble particle playing
					if (FindObjectOfType<PetBathController>().IsBubblePlaying())
					{
						// Confirm dialog
						GameObject confirmDialogObj = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), FindObjectOfType<Canvas>().transform , false);
						ShowDialog confirmDialog = confirmDialogObj.GetComponent<ShowDialog>();
						confirmDialog.SetCaption("Are you sure you want to leave pet without rinsing?");
						confirmDialog.SetConfirmCaption("Yes");
						confirmDialog.SetCancelCaption("No");
						// confirm button;
						confirmDialog.OnYesEvent += () =>
						{
							FindObjectOfType<PetBathController>().StopAllBubbleParticles();
							FindObjectOfType<PetUIController>().Indoor();
							Destroy(confirmDialogObj);
						};
						// cancel button;
						confirmDialog.OnNoEvent += () =>
						{
							Destroy(confirmDialogObj);
						};
					}
					else
					{
						FindObjectOfType<PetUIController>().Indoor();
					}
				}
			}
			else if (start > eventData.position.x)
			{
				//Debug.Log("Drag left");
				if(Mathf.Abs(start - eventData.position.x) >= distance)
				{
					FindObjectOfType<PetUIController>().Outdoor();
				}
			}
		}

      
    }

    #endregion
}
