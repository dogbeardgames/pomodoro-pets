﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BackgroundThemeController : MonoBehaviour {

    public static BackgroundThemeController Instance;

    // dictionary of colors for every theme
    Dictionary<EnumCollection.Theme, Dictionary<string, Color>> themeDictionary = new Dictionary<EnumCollection.Theme, Dictionary<string, Color>>();

	[SerializeField]
	Sprite tLight, tDark;

	public EnumCollection.Theme selectedTheme;

    void Awake()
    {
		
        if(Instance == null)
        {
            Instance = this;
        }
        //light
        Dictionary<string, Color> lightTheme = new Dictionary<string, Color>();
        lightTheme.Add("Background", Color.white);
        lightTheme.Add("Font", Color.black);
        lightTheme.Add("Button", new Color32(188, 188, 188, 255));
        themeDictionary.Add(EnumCollection.Theme.Light, lightTheme);

        //dark
        Dictionary<string, Color> darkTheme = new Dictionary<string, Color>();
        darkTheme.Add("Background", Color.black);
        darkTheme.Add("Font", Color.white);
        darkTheme.Add("Button", Color.white);
        themeDictionary.Add(EnumCollection.Theme.Dark, darkTheme);


    }
	

//    void Start()
//    {
//        UpdateTheme((EnumCollection.Theme)DataController.Instance.settings.themeIndex);
//    }
	// Update is called once per frame
//	void Update () {
//		
//	}

    #region MyRegion
    public Color ThemeColor(string themeObjectType)
    {               
        return themeDictionary[(EnumCollection.Theme)DataController.Instance.settings.themeIndex][themeObjectType];
		//activeTheme = (EnumCollection.Theme)themeObjectType;
    }

	public Sprite  GetThemeBG()
	{               
		//return themeDictionary[(EnumCollection.Theme)DataController.Instance.settings.themeIndex][themeObjectType];
		//activeTheme = (EnumCollection.Theme)themeObjectType;
		if (DataController.Instance.settings.themeIndex == 1) {
			return tDark;
		} else if (DataController.Instance.settings.themeIndex == 0) {
			return tLight;
		}
		return null;
	}

	public Sprite  GetThemeBG(EnumCollection.Theme theme)
	{               
		//return themeDictionary[(EnumCollection.Theme)DataController.Instance.settings.themeIndex][themeObjectType];
		//activeTheme = (EnumCollection.Theme)themeObjectType;
		if (theme == EnumCollection.Theme.Dark) {
			return tDark;
		} else if (theme == EnumCollection.Theme.Light) {
			return tLight;
		}
		return null;
	}

    public void UpdateTheme(EnumCollection.Theme theme)
    {
//		
//		switch(theme)
//		{
//		case EnumCollection.Theme.Dark:
//			DataController.Instance.settings.themeIndex = 1;
//			break;
//		default:
//			DataController.Instance.settings.themeIndex = 0;
//			break;
//		}
	
		selectedTheme = theme;

        BackgroundType[] backgrounds = Resources.FindObjectsOfTypeAll<BackgroundType>();
        // backgrounds
        for (int i = 0; i < backgrounds.Length; i++)
        {           
			if (backgrounds [i].GetComponent<Image> () != null) {
				//Color c = themeDictionary[theme]["Background"];
				//c.a = backgrounds [i].GetComponent<Image> ().color.a;
				//backgrounds [i].GetComponent<Image> ().color = c;
				backgrounds [i].GetComponent<Image> ().sprite = GetThemeBG(theme);
			}
                
        }

        FontType[] fonts = Resources.FindObjectsOfTypeAll<FontType>();
        // fonts
        for (int i = 0; i < fonts.Length; i++)
        {
//            if(fonts[i].GetComponent<TextMeshProUGUI>() != null)
//                fonts[i].GetComponent<TextMeshProUGUI>().color = themeDictionary[theme]["Font"];
        }

        ButtonType[] buttons = Resources.FindObjectsOfTypeAll<ButtonType>();
        //Debug.Log(buttons.Length);
        // buttons
        for (int i = 0; i < buttons.Length; i++)
        {
            if(buttons[i].GetComponent<Image>() != null)
                buttons[i].GetComponent<Image>().color = themeDictionary[theme]["Button"];
        }
    }
    #endregion
}