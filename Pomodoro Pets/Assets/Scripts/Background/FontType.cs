﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FontType : MonoBehaviour {

    //const string themeOjbect = "Font";

    void Start()
    {
		//Change ();
    }

	void OnEnable()
	{
		//Change ();
	}

	void Change()
	{
		if(GetComponent<Image>() != null)
		{
			GetComponent<Image>().color = BackgroundThemeController.Instance.ThemeColor("Background");
		}
		else if(GetComponent<TextMeshProUGUI>() != null)
		{
			GetComponent<TextMeshProUGUI>().color = BackgroundThemeController.Instance.ThemeColor("Font");
		}
		else if(GetComponent<Text>() != null)
		{
			GetComponent<Text> ().color = BackgroundThemeController.Instance.ThemeColor ("Font");
		}
	}
}
