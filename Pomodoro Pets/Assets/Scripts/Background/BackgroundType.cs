﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BackgroundType : MonoBehaviour {

	//Color c;
 
	void Start()
	{
		Change ();
	}

	void OnEnable()
	{
		Change ();
	}

	void Change()
	{
		if(GetComponent<Image>() != null)
		{
//			c = BackgroundThemeController.Instance.ThemeColor("Background");
//			c.a = GetComponent<Image> ().color.a;
//			GetComponent<Image> ().color = c;
			GetComponent<Image> ().sprite = BackgroundThemeController.Instance.GetThemeBG();

		}
		else if(GetComponent<TextMeshProUGUI>() != null)
		{
			//GetComponent<TextMeshProUGUI>().color = BackgroundThemeController.Instance.ThemeColor("Font");
		}
	}
}
