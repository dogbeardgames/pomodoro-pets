﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;
using TMPro;

public class Timer : MonoBehaviour {	


	#region variables
	private Image timerImage;

	public  float   endTime;
	private float   m_time,	                
	m_preEndAlarm,
	m_timeCounter;

	public int  m_hour,
	m_min,
	m_sec;

	private bool m_isCounterEnabled, m_isPreEndAralmTriggered = false;
	private bool m_isPaused = false;

	private TextMeshProUGUI timerText;

	float sleepUntil = 0;


	int counterValue,focusCounter,pauseCounter;

	public float savedTime;

	public bool isLongbreakEnd =false;

	float deductShortBreak  =0;
	float deductSbreak  = 0;

	float deductLongBreak  =0;
	float deductLbreak  = 0;
	// Use this for initialization

	#endregion

	void Start () 
	{
		//btnStartTimer.onClick.AddListener (StartTimer);
		//savedTime=
		//check if the last ran(finished) timer is long break when app exited
		// added this code so that elapsed time and time saved on exit will not affect the duration timer


	}	


	#region PROPERTIES
	#endregion

	#region METHODS
	public void SetTimerValue(float durationInMinutes, GameObject _timerImage, TextMeshProUGUI _timerText)
	{

		m_time = durationInMinutes  * 60f;// multiply to 60 seconds to convert to seconds
		timerImage = _timerImage.GetComponent<Image>();
		timerText = _timerText;

		// ser color
		timerText.color = Color.green;
	}





	public void StartTimer()// start the timer
	{   
		DataController.Instance.playerData.isSkipped = false;

		deductShortBreak = DataController.Instance.settings.shortBreakDuration * 60 - DataController.Instance.playerData.timeLeftOnExit;
		deductSbreak = TimeSave.instance.savedTime + deductShortBreak;

		deductLongBreak = DataController.Instance.settings.longBreakDuration * 60 - DataController.Instance.playerData.timeLeftOnExit;
		deductLbreak = 10;


		//Debug.Log ("<color=red>" + "Deduct long break "+ deductLbreak  +  "</color>");

		Application.runInBackground = true;

		if(DataController.Instance.settings.isPreEndAlarmOn)
		{
			//compute pre end alarm
			m_preEndAlarm = m_time - (DataController.Instance.settings.alarmTime * 60f);
		}

		//calculate time left and subtract time consumed outside the app
		if (DataController.Instance.playerData.isTimerRunning) 
		{
			//endTime = UnityEngine.Time.time + m_time;
			if (DataController.Instance.playerData.sessionRunning)
			{
				//Debug.Log ("Saved TIme: " + savedTime  );

				//Debug.Log ("<color=red>" + "*********End Time runnuing**************" + CheckTimeMaster.instance.CheckDate () + "</color>");

				//Debug.Log ("<color=red>" + "*********timer saved**************" + DataController.Instance.playerData.timeLeftOnExit + "</color>");

				endTime = DataController.Instance.playerData.timeLeftOnExit - TimeSave.instance.savedTime;
				//Debug.Log ("End TIme R: " + TimeSave.instance.savedTime  );
			}

			//if task/session
			else if (DataController.Instance.playerData.sessionRunning && DataController.Instance.playerData.state == EnumCollection.PomodoroStates.TimerScreen) 
			{
				//Debug.Log ("Saved TIme: " + savedTime  );
				endTime = DataController.Instance.playerData.timeLeftOnExit - TimeSave.instance.savedTime;

				//Debug.Log ("<color=red>" + "***********End Time ruuning timerscreen************" + TimeSave.instance.savedTime + "</color>");

			}

			//if break screen
			else if (DataController.Instance.playerData.isfromIndoor == true && DataController.Instance.playerData.state == EnumCollection.PomodoroStates.BreakScreen) 
			{
				//Debug.Log ("<color=red>" + "You're in the break from pets");
				//Debug.Log ("<color=red>" + "Deduct short break "+ deductSbreak  +  "</color>");
				//Debug.Log ("<color=blue>" + "Unit engine time "+ deductSbreak  +  "</color>");
				endTime = UnityEngine.Time.time +  (DataController.Instance.playerData.timeLeftOnExit - 1);

				//change is from indoor value
				DataController.Instance.playerData.isfromIndoor = false;
			}

			//if long break screen
			else if (DataController.Instance.playerData.isfromIndoor == true && DataController.Instance.playerData.state == EnumCollection.PomodoroStates.LongBreakScreen) 
			{
				//////Debug.Log ("<color=red>" + "You're in the break from pets" + "</color>");
				//Debug.Log ("<color=red>" + "Deduct long break "+ deductLbreak  +  "</color>");
				//Debug.Log ("<color=blue>" + "Unit engine time "+ deductLbreak  +  "</color>");
				//endTime = (UnityEngine.Time.time + m_time)  - deductLbreak;
				endTime = UnityEngine.Time.time +  (DataController.Instance.playerData.timeLeftOnExit - 1);

				//change is from indoor value
				DataController.Instance.playerData.isfromIndoor = false;
			}

			else 
			{
				endTime = UnityEngine.Time.time + m_time;
			}
		} 

		else if (DataController.Instance.playerData.isTimerRunning ==false && DataController.Instance.playerData.state == EnumCollection.PomodoroStates.TimerScreen ) 
		{
			if (DataController.Instance.playerData.sessionRunning) 
			{				
				endTime = UnityEngine.Time.time + m_time;
			}
		} 

		//endTime = m_time;
		//m_timeCounter = 0;
		m_preEndAlarm = Time.time + m_preEndAlarm;

		StartCoroutine ("RunTimer");
		//btnStartTimer.gameObject.SetActive (false);
	}

	/// <summary>
	/// true = paused, false = playing
	/// </summary>
	/// <returns><c>true</c>, if toggle was played, <c>false</c> otherwise.</returns>
	public bool IsPaused()// for pause
	{
        DataController.Instance.playerData.isPaused = !DataController.Instance.playerData.isPaused;

		if(DataController.Instance.playerData.isPaused)//DataController.Instance.playerData.isTimerRunning
		{
//			DataController.Instance.playerData.isPaused = false;
			StartCoroutine("RunTimer");
			Time.timeScale = 1;
			return DataController.Instance.playerData.isPaused;
		}
		else
		{
//			DataController.Instance.playerData.isPaused = true;
			StopCoroutine("RunTimer");
			Time.timeScale = 0;
			return DataController.Instance.playerData.isPaused;
		}
	}

	public void ExtendOneMinute()// extend the timer for one minute
	{

		DataController.Instance.playerData.isTimeExtended = true;

		PlainNotification.CancelNotification (1);
		PlainNotification.CancelNotification (2);
		PlainNotification.CancelNotification (3);
		PlainNotification.CancelNotification (4);

		PlainNotification.CancelAllNotifications ();
		LocalNotificationIos.CancelAllNotification ();

		#if UNITY_IOS
		LocalNotificationIos.CancelAllNotification ();
		LocalNotificationIos.CancelNotification ();
		#endif

		//call sfx
		Framework.SoundManager.Instance.PlaySFX ("seIconTapPomodoro");

		endTime += 60f;
		m_preEndAlarm += 60f;
		m_time += 60f;

		m_isPreEndAralmTriggered = false;
		DataController.Instance.playerData.isTimerExtended = true;

		//call pre end alert and notifications

		//restart notification
		float min = m_min * 60;
		float sec = m_sec;
		float totalNotifTime = min + sec + 60;
		float preEndalarm = DataController.Instance.settings.alarmTime * 60;
		float finalPreEnd = totalNotifTime - preEndalarm;

		//get final preEnd

		float addedTime = totalNotifTime;
		float sessionTIme = DataController.Instance.settings.sessionDuration * 60;

		float totalpreEnd = totalNotifTime - preEndalarm;


		//Debug.Log(m_min);
		//Debug.Log(m_sec);

		//Debug.Log("total " + (long)totalNotifTime);
		//Debug.Log("total pre " + (long)finalPreEnd);
		//Debug.Log("final pre " + preEndalarm/60);



		//call notif
		PlainNotification.SendNotification(1, (long)totalNotifTime, "Pomodoro Pets", "Work  session finished. Tap to continue to next session", new Color32(0xff, 0x44, 0x44, 255), true, true, true, "app_icon");

		//pre end
		if (DataController.Instance.settings.alarmTime > 0) 
		{
			if(totalNotifTime > preEndalarm )
			{
				PlainNotification.SendNotification (4, (long)totalpreEnd, "Pomodoro Pets", "You have " + preEndalarm/60 + " " + "minute(s)" + " left until session end ", new Color32 (0xff, 0x44, 0x44, 255), true, true, true, "app_icon");
			}
		}
			
		//IOS
		#if UNITY_IOS
		LocalNotificationIos.SetNotification("Work session finished",(int)totalNotifTime, 1);

		if (DataController.Instance.settings.alarmTime > 0) 
		{
		if(totalNotifTime > preEndalarm )
		{
			LocalNotificationIos.SetNotification("You have " +  finalPreEnd  + " " + "minute(s)" + " left until session end",(int)totalpreEnd, 1);
		}
		}
		#endif



	}

	public void Skip()
	{   



		DataController.Instance.playerData.ispetHome = false;
		DataController.Instance.playerData.isfromIndoor = false;
		//switch flag isSKipped to true
		DataController.Instance.playerData.isSkipped = true;
		//check if user needed to award gold
	
		if (DataController.Instance.playerData.isTimerRunning && DataController.Instance.playerData.state == EnumCollection.PomodoroStates.TimerScreen) {
			GetComponent<PomodoroUI> ().GoldReward ();
			//Save this task
			SaveFinishTask.instance.AddTask (GetComponent<PomodoroController>().taskList[0]);
		}

		//DataController.Instance.playerData.durationTime = 0;
		//Debug.Log ("Cancel Notification");
		PlainNotification.CancelAllNotifications ();
		PlainNotification.CancelNotification (1);
		PlainNotification.CancelNotification (2);
		PlainNotification.CancelNotification (3);
		PlainNotification.CancelNotification (4);

		//cancel ios notif

		#if UNITY_IOS
		LocalNotificationIos.CancelAllNotification ();
		LocalNotificationIos.CancelNotification ();
		#endif

		//play sfx
		Framework.SoundManager.Instance.PlaySFX ("seIconTapPomodoro");


		//Save skipped task duration
		//Save short break task duration
		//Save long break task duration

		if (DataController.Instance.playerData.isTimerRunning && DataController.Instance.playerData.state == EnumCollection.PomodoroStates.TimerScreen ) 
		{
			//Save task duration
			//SaveFinishTask.instance.AddCompletedTaskDuration((float)x);
			//Debug.Log("duration  successfuly saved skipped...");

			//GoldReward ();

		} 
		else if (DataController.Instance.playerData.isTimerRunning && DataController.Instance.playerData.state == EnumCollection.PomodoroStates.BreakScreen ) 
		{
			float minute_second =(m_min * 60) + m_sec;
			float breakduration = DataController.Instance.settings.shortBreakDuration * 60;
			float breakValue = breakduration - minute_second;

			//Debug.Log("<color=red>" + "Break Time duration:---------------------" + minute_second+ "--------------------" +   "</color>");
			//Debug.Log("<color=red>" + "Break Time when skipped:---------------------" + breakduration +  "--------------------" +   "</color>");
			//Debug.Log("<color=red>" + "TOtal Break:---------------------" + breakValue +  "--------------------" +   "</color>");

			SaveFinishTask.instance.Addbreak(breakValue);
			//call and save break in achievements here
			AchievementConditions.instance.checkBreakTimeAchievement(breakValue);
			AchievementConditions.instance.WorkLifeBalance();
			//Debug.Log("short break success skipped...");
		} 
		else if (DataController.Instance.playerData.isTimerRunning && DataController.Instance.playerData.state == EnumCollection.PomodoroStates.LongBreakScreen ) 
		{
			float minute_second =(m_min * 60) + m_sec;
			float breakduration = DataController.Instance.settings.longBreakDuration * 60;
			float breakValue = breakduration - minute_second;

			//Debug.Log("<color=red>" + "Break Time duration:---------------------" + minute_second+ "--------------------" +   "</color>");
			//Debug.Log("<color=red>" + "Break Time when skipped:---------------------" + breakduration +  "--------------------" +   "</color>");
			//Debug.Log("<color=red>" + "TOtal Break:---------------------" + breakValue +  "--------------------" +   "</color>");

			SaveFinishTask.instance.Addbreak(breakValue);
			AchievementConditions.instance.checkBreakTimeAchievement(breakValue);
			AchievementConditions.instance.WorkLifeBalance();
			//Debug.Log("short break success ...");
		} 
			
		// skips current session. If no session are left to skip, then this will clear the Timer and will return the User to the Main Screen

		Time.timeScale = 1;
		EndTimer();
		//        if(DataController.Instance.playerData.currentSessionCount == DataController.Instance.settings.sessionCount)
		//        {
		//            // uncheck all done task
		//            DataController.Instance.taskList.items.Where(task => task.isDone == true).Select( item => item.isDone = false).ToList();
		//
		//            DataController.Instance.playerData.currentSessionCount = 1;
		//
		//            DataController.Instance.playerData.isTimerRunning = false;
		//
		//            Debug.Log("Skip last session");
		//            UnityEngine.SceneManagement.SceneManager.LoadScene("Main", UnityEngine.SceneManagement.LoadSceneMode.Single);
		//        }
		//        else
		//        {           
		//            Debug.Log("<color=red>SKIP</color>");
		//            switch (DataController.Instance.playerData.state)
		//            {
		//                case EnumCollection.PomodoroStates.TimerScreen:
		//                    //Destroy(PomodoroUI.pomodoroObject);
		//                    //DataController.Instance.playerData.state = EnumCollection.PomodoroStates.BreakScreen;
		//                    EndTimer();
		//                    break;
		//                case EnumCollection.PomodoroStates.BreakScreen:
		//                    //Destroy(PomodoroUI.pomodoroObject);
		//                    //DataController.Instance.playerData.state = EnumCollection.PomodoroStates.LongBreakScreen;
		//                    EndTimer();
		//                    break;
		//                case EnumCollection.PomodoroStates.LongBreakScreen:                    
		//                    GetComponent<PomodoroController>().GetActiveTasks();
		//                    GetComponent<PomodoroUI>().CreateTaskPlayingPanel();
		//                    DataController.Instance.playerData.currentSessionCount++;
		//                    break;
		//                default:
		//                    break;
		//            }
		//        }
	}

	public void StopSession()
	{
		DataController.Instance.playerData.isSkipped = false;
		DataController.Instance.playerData.ispetHome = false;
		DataController.Instance.playerData.isfromIndoor = false;
		//cancel notification
		PlainNotification.CancelAllNotifications ();
		PlainNotification.CancelNotification (1);
		PlainNotification.CancelNotification (2);
		PlainNotification.CancelNotification (3);
		PlainNotification.CancelNotification (4);

		#if UNITY_IOS
		LocalNotificationIos.CancelAllNotification ();
		LocalNotificationIos.CancelNotification ();
		#endif

		//play sfx
		Framework.SoundManager.Instance.PlaySFX ("seIconTapPomodoro");

		// clears time and returns user Main Screen
		Time.timeScale = 1;
		DataController.Instance.playerData.currentSessionCount = 1; 
		DataController.Instance.playerData.sessionCount++;

		DataController.Instance.playerData.isTimerRunning = false;
		DataController.Instance.playerData.sessionRunning = false;

		// reset done task
		// GetComponent<PomodoroController>().ResetDoneTask();

		UnityEngine.SceneManagement.SceneManager.LoadScene("Main", UnityEngine.SceneManagement.LoadSceneMode.Single);

		Debug.Log ("Session Stopped");
	}

	void EndTimer()// end timer, if last session = longbreak, else = shortbreak
	{
		
		// reset to false
		m_isPreEndAralmTriggered = false;

		//Debug.Log ("Done, time for some petting! Huehue");

		isLongbreakEnd = true;


		//panelEndTimerPopUp.gameObject.SetActive (true);
		//timerImage.fillAmount = 1;

		//Debug.Log("<color=red>SKIP SKIP SKIP SKIP</color>");

		if (DataController.Instance.playerData.state == EnumCollection.PomodoroStates.TimerScreen)
		{
			//call Fervid w 
			AchievementConditions.instance.FervidWorker();

			//call ProductiveDay Achievement if session is not skipped
			if(DataController.Instance.playerData.isSkipped == false)
			{
				//call this for stats saving when session is not skipped
				SaveFinishTask.instance.AddTask (GetComponent<PomodoroController>().taskList[0]);


				AchievementConditions.instance.AddproductiveDayTime (DataController.Instance.settings.sessionDuration * 60);
				AchievementConditions.instance.ProductiveDay ();
				DataController.Instance.playerData.isSkipped = false;


			}

			if (DataController.Instance.playerData.currentSessionCount == DataController.Instance.settings.sessionCount)
			{
				//Debug.Log("<color=red> should long break </color>");
				//long break
				GetComponent<PomodoroUI>().CreateLongBreak();

				// uncheck all done task
				//  GetComponent<PomodoroController>().ResetDoneTask();
				//Debug.Log("Long break!");

				//leftTime = 0; // reset left time
			}
			else          
			{
				AchievementConditions.instance.FervidWorker ();
				//call ProductiveDay Achievement if session is not skipped
				if(DataController.Instance.playerData.isSkipped == false)
				{
					AchievementConditions.instance.AddproductiveDayTime (DataController.Instance.settings.sessionDuration * 60);
					AchievementConditions.instance.ProductiveDay ();
					DataController.Instance.playerData.isSkipped = false;
				}
				//short break
				GetComponent<PomodoroUI>().CreateBreak();
				//Debug.Log("Short Break!");

				//leftTime = 0;// reset left time
			}
		}
		else if(DataController.Instance.playerData.state == EnumCollection.PomodoroStates.BreakScreen)
		{
			
			//Work life balance
			//call ProductiveDay Achievement if session is not skipped

			if(DataController.Instance.playerData.isSkipped == false)
			{
				//Debug.Log("<color=red> " + "after short break " + "</color>");

				float minute_second =(m_min * 60) + m_sec;
				float breakduration = DataController.Instance.settings.shortBreakDuration * 60;
				float breakValue = breakduration - minute_second;

				//Debug.Log("<color=red>" + "Break Time duration:---------------------" + minute_second+ "--------------------" +   "</color>");
				//Debug.Log("<color=red>" + "Break Time when skipped:---------------------" + breakduration +  "--------------------" +   "</color>");
				//Debug.Log("<color=red>" + "TOtal Break:---------------------" + breakValue +  "--------------------" +   "</color>");

				SaveFinishTask.instance.Addbreak(breakValue);
				//call and save break in achievements here
				AchievementConditions.instance.checkBreakTimeAchievement(breakValue);
				AchievementConditions.instance.WorkLifeBalance();
				//Debug.Log("short break success skipped...");

				AchievementConditions.instance.AddproductiveDayTime (DataController.Instance.settings.shortBreakDuration * 60);
				AchievementConditions.instance.ProductiveDay ();
				DataController.Instance.playerData.isSkipped = false;
			}
				
			// increment current session
			DataController.Instance.playerData.currentSessionCount++;

			// get active task then create playing task panel
			GetComponent<PomodoroController>().GetActiveTasks();
			GetComponent<PomodoroUI>().CreateTaskPlayingPanel();

			SaveFinishTask.instance.Addbreak(DataController.Instance.settings.shortBreakDuration);
			//call and save break in achievements here
//			AchievementConditions.instance.checkBreakTimeAchievement((int)DataController.Instance.settings.shortBreakDuration);
//			AchievementConditions.instance.WorkLifeBalance();

			//Debug.Log("short break success...");

		}
		else if(DataController.Instance.playerData.state == EnumCollection.PomodoroStates.LongBreakScreen)
		{
			



			//call ProductiveDay Achievement if session is not skipped
			if(DataController.Instance.playerData.isSkipped == false)
			{
				//Debug.Log("<color=red> " + "after long break " + "</color>");

				float minute_second =(m_min * 60) + m_sec;
				float breakduration = DataController.Instance.settings.longBreakDuration * 60;
				float breakValue = breakduration - minute_second;

				//Debug.Log("<color=red>" + "Break Time duration:---------------------" + minute_second+ "--------------------" +   "</color>");
				////Debug.Log("<color=red>" + "Break Time when skipped:---------------------" + breakduration +  "--------------------" +   "</color>");
				//Debug.Log("<color=red>" + "TOtal Break:---------------------" + breakValue +  "--------------------" +   "</color>");

				SaveFinishTask.instance.Addbreak(breakValue);
				//call and save break in achievements here
				AchievementConditions.instance.checkBreakTimeAchievement(breakValue);
				AchievementConditions.instance.WorkLifeBalance();
				//Debug.Log("short break success skipped...");
				
				AchievementConditions.instance.AddproductiveDayTime (DataController.Instance.settings.longBreakDuration * 60);
				AchievementConditions.instance.ProductiveDay ();
				DataController.Instance.playerData.isSkipped = false;
			}

			// uncheck all done task
			//DataController.Instance.taskList.items.Where(task => task.isDone == true).Select( item => item.isDone = false).ToList();

			DataController.Instance.playerData.currentSessionCount = 1;

			//DataController.Instance.playerData.isTimerRunning = false;
			// long break is done, will go back to main screen
			DataController.Instance.playerData.isTimerRunning = false;
			DataController.Instance.playerData.state = EnumCollection.PomodoroStates.TaskScreen;

			try {
				//save long break time
				SaveFinishTask.instance.Addbreak(DataController.Instance.settings.longBreakDuration);
				//call and save break in achievements here
//				AchievementConditions.instance.checkBreakTimeAchievement(DataController.Instance.settings.longBreakDuration);
//				AchievementConditions.instance.WorkLifeBalance();

			} catch (Exception ex) {

			}

			//7/8/2017
			if(!DataController.Instance.playerData.ispetHome)
				UnityEngine.SceneManagement.SceneManager.LoadScene("Main", UnityEngine.SceneManagement.LoadSceneMode.Single);

			DataController.Instance.playerData.state = EnumCollection.PomodoroStates.LongBreakScreen;
			DataController.Instance.playerData.isSkipped = false;

		}
	}

	private string timeFormat(float time)
	{
		string format = "";
		if (time > 1) 
		{
			format = "minutes";
		} 
		else 
		{
			format = "minute";
		}
		return format;
	}

	void CheckPreEndAlarm()
	{
		if(m_preEndAlarm < Time.time && m_isPreEndAralmTriggered == false)            
		{
			m_isPreEndAralmTriggered = true;

			if(DataController.Instance.settings.isPreEndAlarmOn)
			{
				if(DataController.Instance.settings.isVibrationOn)
				{
					Handheld.Vibrate();
					//Debug.Log("Vibrate Pre End Alarm");
				}
				//call pre end alarm notification
				//Debug.Log("Pre end alarm notif is on");
				//Debug.Log("Sound Pre End Alarm");
			}
		}
	}

	WaitForSeconds wfs = new WaitForSeconds(0.1f);//0.1f
	WaitForSeconds wfspreEnd = new WaitForSeconds(1f);// use for pre end alert sound

	private IEnumerator RunTimer()
	{	
		DataController.Instance.playerData.sessionRunning = false;
		Application.runInBackground = true;

		while (Time.time <= endTime) //UnityEngine.Time.time
		{
			
			// check pre end alarm

			float timeCheck = (m_min*60) + m_sec;
			bool isSoundActive = true;

			if (DataController.Instance.settings.isPreEndAlarmOn) {
				if (DataController.Instance.settings.alarmTime > 0) {
					if (DataController.Instance.settings.alarmTime * 60 == timeCheck) {
						yield return wfspreEnd;
						//DEFAULT
						if (DataController.Instance.settings.preEndAlertIndex == 0 && isSoundActive == true) {
							Framework.SoundManager.Instance.PlaySFX ("seAlarm");
							//Debug.Log ("test1");
						}
				//DOG
				else if (DataController.Instance.settings.preEndAlertIndex == 1 && isSoundActive == true) {
							Framework.SoundManager.Instance.PlaySFX ("seDogBarkSmall");
							//Debug.Log ("test2");
						}
				//CAT
				else if (DataController.Instance.settings.preEndAlertIndex == 2 && isSoundActive == true) {
							Framework.SoundManager.Instance.PlaySFX ("seCatMeow");	
							//Debug.Log ("test3");
						}
					}
				}
			}
			CheckPreEndAlarm ();


			//check time passed when returning to app
			//endTime = 

			//timer text
			m_min = (int)(endTime + 1 - Time.time) / 60;
			m_sec = (int)(endTime + 1 - Time.time) % 60;

			//m_min = (int)(m_time - lastCounter - Time.time) / 60;
			//m_sec = (int)(m_time - lastCounter - Time.time) % 60;
			timerText.text = m_min.ToString("00") + ":" + m_sec.ToString("00");
			float totalseconds = (m_min * 60) + m_sec;
			float duration = DataController.Instance.settings.sessionDuration * 60;
			DataController.Instance.playerData.goldTimer = duration  -  totalseconds;
			DataController.Instance.playerData.durationTime = duration  -  totalseconds;


			//image timer bar
			timerImage.fillAmount = Mathf.Clamp((float)((endTime - Time.time) / m_time), 0, 1);//UnityEngine.Time.time
			//Debug.Log((float)((endTime - Time.time) / m_time));
			//Debug.Log("time counter " + Time.time + ", end time " + endTime);


			//time counter
			//m_timeCounter += 0.05f;
			//Debug.Log (m_time);
			//Debug.Log (leftTime);
			//RotateIcon.instance.RotateCircle(20);

			yield return wfs;
		}
		EndTimer ();
	}


	void OnApplicationQuit()
	{
		//Debug.Log ("Quit");
		TimeSaver.instance.SaveDate ();

		int minutetoSeconds = m_min * 60;

		DataController.Instance.playerData.timeLeftOnExit = m_sec + minutetoSeconds;

		if (DataController.Instance.playerData.sessionRunning == false && DataController.Instance.playerData.state == EnumCollection.PomodoroStates.TaskScreen) {
			DataController.Instance.playerData.sessionRunning = false;

			//Debug.Log ("False saved");

		} else {
			DataController.Instance.playerData.sessionRunning = true;
			//Debug.Log ("True saved");
		}

	}

	#endregion


	//	DateTime oldDate = DateTime.Now;
	//
	//
	//	public void OnApplicationPause(bool paused)
	//	{
	//		if (paused) {
	//			DataController.Instance.playerData.timeState = "Pause";
	//
	//			Debug.Log ("<color=red>" + oldDate + "</color>");
	//
	//		} else {
	//			Debug.Log ("<color=red>" + (DateTime.Now - oldDate).TotalSeconds  + "</color>");
	//
	//		}
	//	}


	//	void Update()
	//	{
	//		if (Input.GetKey ("p")) {
	//			OnApplicationPause (true);
	//
	//		
	//		} else if (Input.GetKey ("r")) {
	//			OnApplicationPause (false);
	//			Debug.Log ("<color=red>" + (DateTime.Now - oldDate).TotalSeconds  + "</color>");
	//		}
	//	}

	//get 



}

