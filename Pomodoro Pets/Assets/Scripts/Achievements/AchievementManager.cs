﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AchievementManager : MonoBehaviour {

	public static AchievementManager instance;

	void Awake()
	{
		if(instance == null)
		{
			instance = this;

			DontDestroyOnLoad(gameObject);
		}
		else
		{
			Destroy(gameObject);
		}             
	}

	// Use this for initialization
	void Start ()
    {
		//AchievementConditions.instance.InvokeQueue ();@CHECK@
		AchievementConditions.instance.InvokeQueue ();  
	}
		
}
