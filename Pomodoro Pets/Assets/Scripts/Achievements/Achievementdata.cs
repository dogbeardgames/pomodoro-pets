﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Achievementdata {


	public int dailyAchievementCounter;
	public int targetDailyAchievementCounter;

	public int WeeklyAchievementCounter;
	public int targetWeeklyAchievementCounter;

	public int allTimeAchievementCounter;
	public int targetallTimeAchievementCounter;

	public List<AchievementBase> achievementList = new List<AchievementBase>();



}
