﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class AchievementController : MonoBehaviour {

	public static AchievementController Instance;

	//public AchievementItem achievementdata;
	public static string filename = "pomodoro_achievements";

	public AchievementDatabase achievementDatabase;



	void Awake()
	{
		if(Instance == null)
		{
			Instance = this;
			////achievementdata = new AchievementItem();
			//LoadPlayerData();
			DontDestroyOnLoad(gameObject);


		}
		else
		{
			Destroy(gameObject);
		}             
		//Debug.Log(Application.persistentDataPath);
	}

    void Start()
    {
        //call Login after 10 seconds
        //StartCoroutine(DelayTimeForDailyLogin());
    }

    // for achievements delay
    IEnumerator DelayTimeForDailyLogin()
    {
        yield return new WaitForSeconds (10);
        try {

          //  AchievementConditions.instance.DailyLogin();
        } 
        catch (System.Exception ex) 
        {

        }            
    }

//	void Start()
//	{
//		//Daily and weekLy Login Checker
//
//		if(Instance == null)
//		{
//			Instance = this;
//			LoadPlayerData();
//			DontDestroyOnLoad(gameObject);
//			achievementdata = new Achievementdata();
//			achievementdata = Data<Achievementdata>.Load(filename, achievementdata);
//		}
//		else
//		{
//			Destroy(gameObject);
//		}  
////		try {
////			AchievementConditions.instance.WeeklyLogin();
////		} catch (Exception ex) {
////		}
//
//	}
//	void Update()
//	{
//		if(Input.GetKeyUp(KeyCode.Escape))// press back in phone
//		{
//			Application.Quit();
//		}            		
//	}
//
//	public void SavePlayerData()
//	{
//		//Data<AchievementItem>.Save(filename, achievementdata);
//
//		PlayerPrefs.DeleteAll ();
//
//		#region achievement
//		PlayerPrefs.SetInt("achCOUNT", achievementDatabase.items.Count);
//		for (int i = 0; i < achievementDatabase.items.Count ; i++)
//		{
//			PlayerPrefs.SetString("achID" + i, achievementDatabase.items[i].achievementId);
//			PlayerPrefs.SetString("achNAME" + i, achievementDatabase.items[i].achName);
//			PlayerPrefs.SetString("achREQ" + i, achievementDatabase.items[i].requirements);
//			PlayerPrefs.SetInt("achTYPE" + i, (int)achievementDatabase.items[i].achievementtype);
//			PlayerPrefs.SetString("achISUNLOCKED" + i, achievementDatabase.items[i].isUnlocked.ToString());
//
//			PlayerPrefs.SetInt("achGOLDREWARD" + i, (int)achievementDatabase.items[i].goldReward);
//			PlayerPrefs.SetInt("achGEMREWARD" + i, (int)achievementDatabase.items[i].gemReward);
//			PlayerPrefs.SetInt("achpigREWARD" + i, (int)achievementDatabase.items[i].pigyReward);
//			PlayerPrefs.SetInt("achTARGETCOUNTER" + i,achievementDatabase.items[i].target_counter);
//			PlayerPrefs.SetInt("achCOUNTER" + i, achievementDatabase.items[i].counter);
//			PlayerPrefs.SetInt("achTARGETGOLD" + i, achievementDatabase.items[i].target_gold);
//			PlayerPrefs.SetInt("achTARGETGEM" + i, achievementDatabase.items[i].target_gem);
//			PlayerPrefs.SetString("achDATELOGIN" + i, achievementDatabase.items[i].dateLogin);
//		}
//		#endregion
//
//		Debug.Log ("<color=red>" + "Save Player Data" + "</color>");
//
//
//	}
//
//	public void LoadPlayerData()
//	{
//		//achievementdata = Data<AchievementItem>.Load(filename, achievementdata);
//
//		#region achievement
//		int achCOUNT = PlayerPrefs.GetInt("achCOUNT", achievementDatabase.items.Count);
//
//		for (int i = 0; i < achCOUNT ; i++)
//		{            
//			try
//			{
//				achievementDatabase.items[i].achievementId = PlayerPrefs.GetString("achID" + i, achievementDatabase.items[i].achievementId);
//				achievementDatabase.items[i].achName = PlayerPrefs.GetString("achNAME" + i, achievementDatabase.items[i].achName);
//				achievementDatabase.items[i].requirements = PlayerPrefs.GetString("achREQ" + i, achievementDatabase.items[i].requirements);
//				achievementDatabase.items[i].achievementtype = (EnumCollection.AchievemenType)PlayerPrefs.GetInt("achTYPE" + i, (int)achievementDatabase.items[i].achievementtype);
//
//				bool.TryParse(PlayerPrefs.GetString("achISUNLOCKED" + i, achievementDatabase.items[i].isUnlocked.ToString()), out achievementDatabase.items[i].isUnlocked);
//
//				achievementDatabase.items[i].goldReward = (uint)PlayerPrefs.GetInt("achGOLDREWARD" + i, (int)achievementDatabase.items[i].goldReward);
//				achievementDatabase.items[i].gemReward = (uint)PlayerPrefs.GetInt("achGEMREWARD" + i, (int)achievementDatabase.items[i].gemReward);
//				achievementDatabase.items[i].pigyReward = (uint)PlayerPrefs.GetInt("achpigREWARD" + i, (int)achievementDatabase.items[i].pigyReward);
//				achievementDatabase.items[i].target_counter = PlayerPrefs.GetInt("achTARGETCOUNTER" + i, achievementDatabase.items[i].target_counter);
//				achievementDatabase.items[i].counter = PlayerPrefs.GetInt("achCOUNTER" + i, achievementDatabase.items[i].counter);
//				achievementDatabase.items[i].target_gold = PlayerPrefs.GetInt("achTARGETGOLD" + i, achievementDatabase.items[i].target_gold);
//				achievementDatabase.items[i].target_gem = PlayerPrefs.GetInt("achTARGETGEM" + i, achievementDatabase.items[i].target_gem);
//				achievementDatabase.items[i].dateLogin = PlayerPrefs.GetString("achDATELOGIN" + i, achievementDatabase.items[i].dateLogin);
//			}
//			catch(ArgumentOutOfRangeException ex)
//			{                
//				AchievementItem achItem = new AchievementItem();
//
//				achItem.achievementId = PlayerPrefs.GetString("achID" + i);
//				achItem.achName = PlayerPrefs.GetString("achNAME" + i);
//				achItem.requirements = PlayerPrefs.GetString("achREQ" + i);
//				achItem.achievementtype = (EnumCollection.AchievemenType)PlayerPrefs.GetInt("achTYPE" + i);
//
//				bool.TryParse(PlayerPrefs.GetString("achISUNLOCKED" + i), out achItem.isUnlocked);
//
//				achItem.goldReward = (uint)PlayerPrefs.GetInt("achGOLDREWARD" + i);
//				achItem.gemReward = (uint)PlayerPrefs.GetInt("achGEMREWARD" + i);
//				achItem.pigyReward = (uint)PlayerPrefs.GetInt("achpigREWARD" + i);
//				achItem.target_counter = PlayerPrefs.GetInt("achTARGETCOUNTER" + i);
//				achItem.counter = PlayerPrefs.GetInt("achCOUNTER" + i);
//				achItem.target_gold = PlayerPrefs.GetInt("achTARGETGOLD" + i);
//				achItem.target_gem = PlayerPrefs.GetInt("achTARGETGEM" + i);
//				achItem.dateLogin = PlayerPrefs.GetString("achDATELOGIN" + i);
//
//				achievementDatabase.items.Add(achItem);
//
//			}
//		}
//		#endregion
//
//		Debug.Log ("<color=red>" + "Load Player Data" + "</color>");
//
//	} 
//
//	void OnDisable()
//	{
//		SavePlayerData();
//
//		Debug.Log ("Data is saving............................******************************************");
//
//	}
}
