﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using System.Threading;
using System.Linq;

public class HammerManager : MonoBehaviour {
	public static HammerManager ins;

	[SerializeField]
	PetShopItems petShopItems;

	[SerializeField]
	List<Hammer> hammer;

	#region animation
	[SerializeField]
	bool shown;
	RectTransform rect;
	public bool IsShown{
		get{ return shown; }
	}
	float rectY = 0;
	Vector2 showPos, hidePos;
	#endregion animation


	void Awake()
	{
		ins = this;
	}
	void Start () {
		rect = GetComponent<RectTransform> ();
		showPos = new Vector2 (rect.anchoredPosition.x, 108f);
		hidePos = new Vector2 (rect.anchoredPosition.x, 30f);



	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void InitHammers()
	{
		HammerData hammerdb = DataController.Instance.hammerDatabase;
		for(int i=0; i<hammerdb.Items.Length; i++)
		{
			for (int k = 0; k < hammer.Count; k++) {
				if (hammerdb.Items [i].ItemName == hammer[k].ItemName) {
					//print (hammer[k].ItemName + "BOOOO");
					if (hammerdb.Items [i].isOwned)
						hammer[k].owned = true;
				}
			}

		}
	}

	public void Show()
	{
		Color clr;

		InitHammers ();

		if (!shown) {
			transform.GetChild (0).gameObject.SetActive (true);
			//rect.DOAnchorPos (showPos,0.2f).OnComplete(()=>shown=true);
			hammer [0].gameObject.SetActive (true);
			if (hammer[1].owned) {
			//if (DataController.Instance.hammerDatabase.Items [1].isOwned) {
				hammer [1].gameObject.SetActive (true);
				//hammer [1].owned = true;
				clr = hammer [1].GetComponent<Image> ().color;
				clr.a = 1;
				hammer [1].GetComponent<Image> ().color = clr;
				//print ("LISA!!");
			} else {
				hammer [1].gameObject.SetActive (true);
				//hammer [1].owned = false;
				clr = hammer [1].GetComponent<Image> ().color;
				clr.a = 0.5f;
				hammer [1].GetComponent<Image> ().color = clr;
				//hammer [1].GetComponent<Image> ().color= new Color(clr.r, clr.g, clr.b, 150);
				//print("JISOO!");
			}
			if (hammer[2].owned) {
			//if (DataController.Instance.hammerDatabase.Items [2].isOwned) {
				hammer [2].gameObject.SetActive (true);
				//hammer [1].owned = true;
				clr = hammer [2].GetComponent<Image> ().color;
				clr.a = 1;
				hammer [2].GetComponent<Image> ().color = clr;
				//hammer [2].GetComponent<Image> ().color = new Color(clr.r, clr.g, clr.b, 255);
			} else {
				hammer [2].gameObject.SetActive (true);
				//hammer [1].owned = false;
				clr = hammer [2].GetComponent<Image> ().color;
				clr.a = 0.5f;
				clr = hammer [2].GetComponent<Image> ().color = clr;
				//hammer [2].GetComponent<Image> ().color = new Color(clr.r, clr.g, clr.b, 150);
			}
			

			shown = true;
		}
			



	}

	public void Hide()
	{
		if (shown) {
			transform.GetChild (0).gameObject.SetActive (false);
			//rect.DOAnchorPos (hidePos,0.2f).OnComplete(()=>shown=false);
			for(int i=0; i<hammer.Count; i++)
			{
				hammer [i].gameObject.SetActive (false);
			}
			shown = false;
		}
			
	}
}
