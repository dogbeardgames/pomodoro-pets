﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PopUp : MonoBehaviour {

	[SerializeField]
	Button btnNo,btndouble;
	public GameObject achievementName;
	public GameObject rewardPopup;
	public GameObject achDescriptionPopup;
	public GameObject PiggyPopUp;
	// Use this for initialization
	public void InstantiatePopUpAchievements (string achName) 
	{
		//Instantiate (achievementName, transform.position, transform.rotation);
		GameObject achievementPopUp = Instantiate (achievementName, GameObject.Find("Canvas").transform, false);
		achievementPopUp.transform.Find("textachievementName").GetComponent<TextMeshProUGUI>().text = achName;
		//Debug.Log ("test");
	}
	

}
