﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


[System.Serializable]
public class  AchievementBase {

	//description

	//public Sprite icon1;
	//public Sprite icon2;

	public string achievementId; //*
	public string achName; //* 
	public string requirements;//*

	public EnumCollection.AchievemenType achievementtype;

	public bool isUnlocked;
	//Sprite image;

	//rewards
	public uint goldReward;
	public uint gemReward;
	public uint pigyReward;

	//counter

	public int target_counter = 5;
	public int counter = 5;

	public int target_gold;
	public int target_gem;

	public string dateLogin;


	public  AchievementBase()
	{
		achievementId = "";
		achName = "";
		requirements = "";
		isUnlocked = false;
		goldReward = 0;
		gemReward = 0;

	}


}




