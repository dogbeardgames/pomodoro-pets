﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using System;
using UnityEngine.SceneManagement;

public class AchievementConditions : MonoBehaviour {

    public bool serving;
    public delegate void DoSomething();

    List<DoSomething> lstAction = new List<DoSomething>();
    Queue<DoSomething> queue = new Queue<DoSomething> ();

    [SerializeField]
    int queueCnt;
    [SerializeField]
    int servingIndex = 0;

	//checklist
	List<AchievementItem> weekCheckList = new List<AchievementItem> ();
	List<AchievementItem> dailyCheckList = new List<AchievementItem> ();

	//All Time no. 1 My FirstPet
	[SerializeField]
	Sprite SppowerTreats,SpenergyTreats;
	[SerializeField]
	GameObject giftDialog;

	public static AchievementConditions instance;



	void Start()
	{

		instance = this;

		weekCheckList = DataController.Instance.achievementDatabase.items.Where 
		(x => x.achievementId == "achWeekly5").ToList ();

		dailyCheckList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achDailySix").ToList ();


		AddToQueue (delegate {
			MyfirstPet();
		});
		AddToQueue (delegate {
			DailyLogin();
		});

		AddToQueue (delegate {
			WeeklyLogin();
		});

		AddToQueue (delegate {
			DailyIncome(); 
		});
		AddToQueue (delegate {
			DailyShopping();
		});
		AddToQueue (delegate {
			WeeklyIncome();
		});
		AddToQueue (delegate {
			WeeklyShopping();
		});
			
	}

    public void AddToQueue(DoSomething action)
    {
        //queue.Enqueue (action);
        lstAction.Add (action);
    }

    Coroutine co;
    IEnumerator IEInvokeQueue()
    {        
        while(true)
        {
			if (lstAction.Count != 0) {
				lstAction [servingIndex].Invoke ();
			}
           
            while(serving)
            {
                yield return new WaitForSeconds (0.1f);
            }
            servingIndex++;
            if (servingIndex >= lstAction.Count)
                servingIndex = 0;

            yield return new WaitForSeconds (0.1f);
        }
    }
    public void InvokeQueue()
    {
        //queueCnt = queue.Count;

        StartCoroutine (IEInvokeQueue());

    }

	#region All Time
	public void MyfirstPet()
	{
		//when pet is selected
		//pop up achievement
		//enable achievement for this id
		//change status to activated
		//Debug.Log("<color = red>" + " My first pet " + "</color>");

		//change new installed to false once user got the first achievement
		DataController.Instance.playerData.isnewInstalled = false;

        List<AchievementItem> alltimeList = DataController.Instance.achievementDatabase.items.Where  
        	(x => x.achievementId == "achallTime1").ToList ();

        alltimeList [0].counter = 1;

        if (alltimeList[0].isUnlocked == false)
        {
            if (alltimeList[0].counter == alltimeList[0].target_counter)
            {
				serving = true;

                GameObject achievementPopUp = Instantiate(gameObject.GetComponent<PopUp>().achievementName, GameObject.Find("Canvas").transform, false);

                achievementPopUp.transform.Find("Box/textachievementName").GetComponent<TextMeshProUGUI>().text = alltimeList[0].achName;
                achievementPopUp.transform.Find("Box/txtID").GetComponent<TextMeshProUGUI>().text = alltimeList[0].achievementId;

                //play sfx
                Framework.SoundManager.Instance.PlaySFX("seReward");
                //Rewarding
                //achievementPopUp.transform.Find ("btndouble").GetComponent<Button>().onClick.AddListener(() => RewardWithVideoAds(dailyList[0].goldReward * 2, dailyList[0].gemReward * 2,achievementPopUp,dailyList[0].pigyReward * 2));
                achievementPopUp.transform.Find("Box/btndouble").GetComponent<Button>().onClick.AddListener(() => VideoReward(achievementPopUp, alltimeList[0].goldReward * 2, alltimeList[0].gemReward * 2, alltimeList[0].pigyReward * 2, 1, 0));
                achievementPopUp.transform.Find("Box/btnNo").GetComponent<Button>().onClick.AddListener(() => Rewarding(alltimeList[0].goldReward, alltimeList[0].gemReward, achievementPopUp, alltimeList[0].pigyReward, 1, 0));
                AssignLabelImage(alltimeList[0].goldReward, alltimeList[0].gemReward, alltimeList[0].pigyReward, 1, 0, achievementPopUp);

                alltimeList[0].isUnlocked = true;

                //unlock collar 
                if (DataController.Instance.playerData.playerPetList[0].petType == EnumCollection.Pet.Cat)
                {
                    for (int i = 0; i < DataController.Instance.bodyDatabase.items.Count(); i++)
                    {
                        if (DataController.Instance.bodyDatabase.items[i].EquipmentName == "Cat Collar")
                        {
                            DataController.Instance.bodyDatabase.items[i].isOwned = true;
                        }
                    }
                }
                else if (DataController.Instance.playerData.playerPetList[0].petType == EnumCollection.Pet.Dog)
                {
                    for (int i = 0; i < DataController.Instance.bodyDatabase.items.Count(); i++)
                    {
                        if (DataController.Instance.bodyDatabase.items[i].EquipmentName == "Dog Collar")
                        {
                            DataController.Instance.bodyDatabase.items[i].isOwned = true;
                        }
                    }
                }
            }	
        }
	}

	//get 3 small dogs
	public void ThreeSmallDogs()
	{

			List<AchievementItem> alltimeList = DataController.Instance.achievementDatabase.items.Where 
				(x => x.achievementId == "achallTime2").ToList ();

				if (alltimeList [0].isUnlocked == false) 
				{
				alltimeList [0].counter = 3;
				GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
				achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achName;
				achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achievementId;
				AssignLabelImage (alltimeList[0].goldReward, alltimeList[0].gemReward,alltimeList[0].pigyReward,0,0,achievementPopUp);
				//play sfx
				Framework.SoundManager.Instance.PlaySFX ("seReward");

				achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button> ().onClick.AddListener (() => VideoReward (achievementPopUp, alltimeList [0].goldReward * 2, alltimeList [0].gemReward * 2, alltimeList [0].pigyReward * 2, 0, 0));
				achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button> ().onClick.AddListener (() => Rewarding (alltimeList [0].goldReward, alltimeList [0].gemReward, achievementPopUp, alltimeList [0].pigyReward, 0, 0));
				alltimeList [0].isUnlocked = true;
				}
	}

	//get 3 big dogs
	public void ThreeBigDog()
	{

				List<AchievementItem> alltimeList = DataController.Instance.achievementDatabase.items.Where  
				(x => x.achievementId == "achallTime3").ToList ();
				if (alltimeList [0].isUnlocked == false) 
				{
				alltimeList [0].counter = 3;
				GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
				achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achName;
				achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achievementId;
				AssignLabelImage (alltimeList[0].goldReward, alltimeList[0].gemReward,alltimeList[0].pigyReward,0,0,achievementPopUp);
				//play sfx
				Framework.SoundManager.Instance.PlaySFX ("seReward");
		
				achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button> ().onClick.AddListener (() => VideoReward (achievementPopUp, alltimeList [0].goldReward * 2, alltimeList [0].gemReward * 2, alltimeList [0].pigyReward * 2, 0, 0));
				achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button> ().onClick.AddListener (() => Rewarding (alltimeList [0].goldReward, alltimeList [0].gemReward, achievementPopUp, alltimeList [0].pigyReward, 0, 0));

				alltimeList [0].isUnlocked = true;
		}

	}

	//get 3 hairy cats
	public void ThreeHairyCat()
	{
		//if hairy cat == 3 
		//pop up achievement
		//enable achievement for this id
		//change status to activated


				List<AchievementItem> alltimeList = DataController.Instance.achievementDatabase.items.Where 
					(x => x.achievementId == "achallTime4").ToList ();

				if (alltimeList [0].isUnlocked == false) 
				{
				alltimeList [0].counter = 3;
				GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
				achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achName;
				achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achievementId;
				AssignLabelImage (alltimeList[0].goldReward, alltimeList[0].gemReward,alltimeList[0].pigyReward,0,0,achievementPopUp);
				//Debug.Log ("test");
				//Debug.Log ("Achievement successfully achieve!!! Get your reward");

				//play sfx
				Framework.SoundManager.Instance.PlaySFX ("seReward");

				achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button> ().onClick.AddListener (() => VideoReward (achievementPopUp, alltimeList [0].goldReward * 2, alltimeList [0].gemReward * 2, alltimeList [0].pigyReward * 2, 0, 0));
				achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button> ().onClick.AddListener (() => Rewarding (alltimeList [0].goldReward, alltimeList [0].gemReward, achievementPopUp, alltimeList [0].pigyReward, 0, 0));
				alltimeList [0].isUnlocked = true;
				}


	}

	//get 3 hairy cats
	public void ThreeShortHaired()
	{
		//if short haired cat == 3 
		//pop up achievement
		//enable achievement for this id
		//change status to activated


			List<AchievementItem> alltimeList = DataController.Instance.achievementDatabase.items.Where 
				(x => x.achievementId == "achallTime5").ToList ();

			if (alltimeList [0].isUnlocked == false) 
			{
				alltimeList [0].counter = 3;
			
				GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
				achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achName;
				achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achievementId;
				AssignLabelImage (alltimeList[0].goldReward, alltimeList[0].gemReward,alltimeList[0].pigyReward,0,0,achievementPopUp);
				//Debug.Log ("test");
				//Debug.Log ("Achievement successfully achieve!!! Get your reward");

				//play sfx
				Framework.SoundManager.Instance.PlaySFX ("seReward");
				achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button> ().onClick.AddListener (() => VideoReward (achievementPopUp, alltimeList [0].goldReward * 2, alltimeList [0].gemReward * 2,alltimeList[0].pigyReward * 2, 0, 0));
				achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button>().onClick.AddListener(() => Rewarding(alltimeList[0].goldReward, alltimeList[0].gemReward,achievementPopUp,alltimeList[0].pigyReward,0,0));
				alltimeList [0].isUnlocked = true;
			}

	}

	//get 3 pets
	public void ThreePets()
	{
		//if pets == 3 
		//pop up achievement
		//enable achievement for this id
		//change status to activated
		List<AchievementItem> alltimeList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achallTime6").ToList ();

		if (alltimeList [0].isUnlocked == false) 
		{
			GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
			achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achName;
			achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achievementId;
			AssignLabelImage (alltimeList[0].goldReward, alltimeList[0].gemReward,alltimeList[0].pigyReward,0,0,achievementPopUp,"cat");
			//play sfx
			Framework.SoundManager.Instance.PlaySFX ("seReward");
			achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button> ().onClick.AddListener (() => VideoReward (achievementPopUp, alltimeList [0].goldReward * 2, alltimeList [0].gemReward * 2, alltimeList [0].pigyReward * 2, 0, 0,"cat"));
			achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button> ().onClick.AddListener (() => Rewarding (alltimeList [0].goldReward, alltimeList [0].gemReward, achievementPopUp, alltimeList [0].pigyReward, 0, 0,"cat"));
			alltimeList [0].isUnlocked = true;
			
			//Reward Pet
			CatPetReward();
		}
	}

	//get 5 pets
	public void FivePets()
	{
		//if pets == 5
		//pop up achievement
		//enable achievement for this id
		//change status to activated

		List<AchievementItem> alltimeList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achallTime7").ToList ();

		if (alltimeList [0].isUnlocked == false) 
		{
			GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
			achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achName;
			achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achievementId;
			AssignLabelImage (alltimeList[0].goldReward, alltimeList[0].gemReward,alltimeList[0].pigyReward,0,0,achievementPopUp,"dog");
			//play sfx
			Framework.SoundManager.Instance.PlaySFX ("seReward");
			achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button> ().onClick.AddListener (() => VideoReward (achievementPopUp, alltimeList [0].goldReward * 2, alltimeList [0].gemReward * 2, alltimeList [0].pigyReward * 2, 0, 0,"dog"));
			achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button> ().onClick.AddListener (() => Rewarding (alltimeList [0].goldReward, alltimeList [0].gemReward, achievementPopUp, alltimeList [0].pigyReward, 0, 0,"dog"));
			alltimeList [0].isUnlocked = true;
			
			//Reward Pet
			DogPetReward ();
		}
	}

	//get 8 pets
	public void EightPets()
	{
		//if pets == 8
		//pop up achievement
		//enable achievement for this id
		//change status to activated

		List<AchievementItem> alltimeList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achallTime8").ToList ();
		if (alltimeList [0].isUnlocked == false) 
		{
			
			GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
			achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achName;
			achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achievementId;
			AssignLabelImage (alltimeList[0].goldReward, alltimeList[0].gemReward,alltimeList[0].pigyReward,0,0,achievementPopUp,"cat");
			//play sfx
			Framework.SoundManager.Instance.PlaySFX ("seReward");
			achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button> ().onClick.AddListener (() => VideoReward (achievementPopUp, alltimeList [0].goldReward * 2, alltimeList [0].gemReward * 2, alltimeList [0].pigyReward * 2, 0, 0,"cat"));
			achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button> ().onClick.AddListener (() => Rewarding (alltimeList [0].goldReward, alltimeList [0].gemReward, achievementPopUp, alltimeList [0].pigyReward, 0, 0,"cat"));
			alltimeList [0].isUnlocked = true;

			//Reward pet
			RewardLastPetInFervidWorker();

		}
	}

	//get 12 pets
	public void TwelvePets()
	{
		//if pets == 12
		//pop up achievement
		//enable achievement for this id
		//change status to activated
		List<AchievementItem> alltimeList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achallTime9").ToList ();

				GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
				achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achName;
				achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achievementId;
				AssignLabelImage (alltimeList[0].goldReward, alltimeList[0].gemReward,alltimeList[0].pigyReward,0,0,achievementPopUp);
				//play sfx
				Framework.SoundManager.Instance.PlaySFX ("seReward");
				achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button> ().onClick.AddListener (() => VideoReward (achievementPopUp, alltimeList [0].goldReward * 2, alltimeList [0].gemReward * 2, alltimeList [0].pigyReward * 2, 0, 0));
				achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button> ().onClick.AddListener (() => Rewarding (alltimeList [0].goldReward, alltimeList [0].gemReward, achievementPopUp, alltimeList [0].pigyReward, 0, 0));
				alltimeList [0].isUnlocked = true;

	}

	//get 6 cats
	public void SixCats()
	{
		//if cats == 6
		//pop up achievement
		//enable achievement for this id
		//change status to activated
		List<AchievementItem> alltimeList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achallTime10").ToList ();
		if (alltimeList [0].isUnlocked == false) 
		{
			
				GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
				achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achName;
				achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achievementId;
				AssignLabelImage (alltimeList[0].goldReward, alltimeList[0].gemReward,alltimeList[0].pigyReward,0,0,achievementPopUp);

				//play sfx
				Framework.SoundManager.Instance.PlaySFX ("seReward");

				achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button> ().onClick.AddListener (() => VideoReward (achievementPopUp, alltimeList [0].goldReward * 2, alltimeList [0].gemReward * 2, alltimeList [0].pigyReward * 2, 0,0));
				achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button> ().onClick.AddListener (() => Rewarding (alltimeList [0].goldReward, alltimeList [0].gemReward, achievementPopUp, alltimeList [0].pigyReward, 0,0));
				alltimeList [0].isUnlocked = true;

		}
	}

	//get 6 dogs
	public void SixDogs()
	{
		//if dogs == 6
		//pop up achievement
		//enable achievement for this id
		//change status to activated

		List<AchievementItem> alltimeList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achallTime11").ToList ();

		if (alltimeList [0].isUnlocked == false) 
		{
			
				GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
				achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achName;
				achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achievementId;
				AssignLabelImage (alltimeList[0].goldReward, alltimeList[0].gemReward,alltimeList[0].pigyReward,0,0,achievementPopUp);
				//play sfx
				Framework.SoundManager.Instance.PlaySFX ("seReward");

				achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button> ().onClick.AddListener (() => VideoReward (achievementPopUp, alltimeList [0].goldReward * 2, alltimeList [0].gemReward * 2, alltimeList [0].pigyReward * 2, 0,0));
				achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button> ().onClick.AddListener (() => Rewarding (alltimeList [0].goldReward, alltimeList [0].gemReward, achievementPopUp, alltimeList [0].pigyReward, 0,0));
				alltimeList [0].isUnlocked = true;
		}
	}

	//get 12 dogs
	public void TwelveDogs()
	{
		//if dogs == 12
		//pop up achievement
		//enable achievement for this id
		//change status to activated
		//if dogs == 6
		//pop up achievement
		//enable achievement for this id
		//change status to activated
		List<AchievementItem> alltimeList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achallTime12").ToList ();
		if (alltimeList [0].isUnlocked == false) 
		{
				GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
				achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achName;
				achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achievementId;
				AssignLabelImage (alltimeList[0].goldReward, alltimeList[0].gemReward,alltimeList[0].pigyReward,0,0,achievementPopUp);

				//play sfx
				Framework.SoundManager.Instance.PlaySFX ("seReward");

				achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button> ().onClick.AddListener (() => VideoReward (achievementPopUp, alltimeList [0].goldReward * 2, alltimeList [0].gemReward * 2, alltimeList [0].pigyReward * 2, 0, 0));
				achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button> ().onClick.AddListener (() => Rewarding (alltimeList [0].goldReward, alltimeList [0].gemReward, achievementPopUp, alltimeList [0].pigyReward, 0, 0));

		}
	}

	//get 12 cats
	public void TwelveCats()
	{
		//if cats == 12
		//pop up achievement
		//enable achievement for this id
		//change status to activated

		List<AchievementItem> alltimeList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achallTime13").ToList ();
		if (alltimeList [0].isUnlocked == false) 
		{
				GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
				achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achName;
				achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achievementId;
				AssignLabelImage (alltimeList[0].goldReward, alltimeList[0].gemReward,alltimeList[0].pigyReward,0,0,achievementPopUp);

				//play sfx
				Framework.SoundManager.Instance.PlaySFX ("seReward");

				achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button> ().onClick.AddListener (() => VideoReward (achievementPopUp, alltimeList [0].goldReward * 2, alltimeList [0].gemReward * 2, alltimeList [0].pigyReward * 2, 0, 0));
				achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button> ().onClick.AddListener (() => Rewarding (alltimeList [0].goldReward, alltimeList [0].gemReward, achievementPopUp, alltimeList [0].pigyReward, 0, 0));
				alltimeList [0].isUnlocked = true;

		}
	}

	//Pomodoro

	public void FirstJob()
	{
		//finished 1 work session
		//pop up achievement
		//enable achievement for this id
		//change status to activated
		//Instantiate (achievementName, transform.position, transform.rotation);

		List<AchievementItem> alltimeList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achallTime14").ToList ();

		if(alltimeList[0].isUnlocked==false)
		{

			//Debug.Log("First job");
			GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp>().achievementName, GameObject.Find("Canvas").transform, false);
			achievementPopUp.transform.Find("Box/textachievementName").GetComponent<TextMeshProUGUI>().text = alltimeList[0].achName;
			achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achievementId;
			AssignLabelImage (alltimeList[0].goldReward, alltimeList[0].gemReward,alltimeList[0].pigyReward,0,0,achievementPopUp);

			//play sfx
			Framework.SoundManager.Instance.PlaySFX ("seReward");

			achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button>().onClick.AddListener(()=> VideoReward(achievementPopUp,alltimeList[0].goldReward * 2, alltimeList[0].gemReward * 2,alltimeList[0].pigyReward * 2, 0, 0));
			achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button>().onClick.AddListener(() => Rewarding(alltimeList[0].goldReward, alltimeList[0].gemReward,achievementPopUp,alltimeList[0].pigyReward,0,0));


			alltimeList [0].isUnlocked = true;

		}	
	}

	//tested
	public void FervidWorker()
	{
		//Have atleast 1 session completed for 30 days straight
		//pop up achievement
		//enable achievement for this id
		//change status to activated
		List<AchievementItem> alltimeList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achallTimefervid").ToList ();

		//check if achievement is satisfied
		if (alltimeList [0].isUnlocked == false) 
		{
			
		if (alltimeList [0].dateLogin == "") 
		{
			alltimeList [0].dateLogin = DateTime.Today.AddDays(-1).ToString("MMMM dd, yyyy");
		}
	
		DateTime thisDate = Convert.ToDateTime (alltimeList [0].dateLogin);

		if ((DateTime.Today - thisDate).Days == 1) 
		{
			alltimeList [0].counter += 1;
			alltimeList [0].dateLogin = DateTime.Today.ToString("MMMM dd, yyyy");

		} 
		else if ((DateTime.Today - thisDate).Days > 1) 
		{
			alltimeList [0].counter = 0;
			alltimeList [0].dateLogin = DateTime.Today.ToString("MMMM dd, yyyy");
		}
			if (alltimeList [0].counter == 30) //Have atleast 1 session completed for 30 days straight
			{
				GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp>().achievementName, GameObject.Find("Canvas").transform, false);
				achievementPopUp.transform.Find("Box/textachievementName").GetComponent<TextMeshProUGUI>().text = alltimeList[0].achName;
				achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achievementId;
				AssignLabelImage (alltimeList[0].goldReward, alltimeList[0].gemReward,alltimeList[0].pigyReward,0,0,achievementPopUp);
				//play sfx
				Framework.SoundManager.Instance.PlaySFX ("seReward");

				achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button>().onClick.AddListener(()=> VideoReward(achievementPopUp,alltimeList[0].goldReward * 2, alltimeList[0].gemReward * 2,alltimeList[0].pigyReward * 2, 0, 0));
				achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button>().onClick.AddListener(() => Rewarding(alltimeList[0].goldReward, alltimeList[0].gemReward,achievementPopUp,alltimeList[0].pigyReward,0,0));
				alltimeList [0].isUnlocked = true;


			}
		}
	}


	public void Taskmaster()
	{
		//Finish 2500 task
		//pop up achievement
		//enable achievement for this id
		//change status to activated
		if (TaskDataController.Instance.completedTaskData.taskItem.Count == 2500) {
			List<AchievementItem> alltimeList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achallTime15").ToList ();
	
			if (alltimeList [0].isUnlocked == false) {
				GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
				achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achName;
				achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achievementId;
				AssignLabelImage (alltimeList [0].goldReward, alltimeList [0].gemReward, alltimeList [0].pigyReward, 0, 0, achievementPopUp);

				//play sfx
				Framework.SoundManager.Instance.PlaySFX ("seReward");

				achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button> ().onClick.AddListener (() => VideoReward (achievementPopUp, alltimeList [0].goldReward * 2, alltimeList [0].gemReward * 2, alltimeList [0].pigyReward * 2, 0, 0));
				achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button> ().onClick.AddListener (() => Rewarding (alltimeList [0].goldReward, alltimeList [0].gemReward, achievementPopUp, alltimeList [0].pigyReward, 0, 0));
				alltimeList [0].isUnlocked = true;	
			}
		}
	}

	//Monetization
	public void Shopaholic()
	{
		//spend 500 gems
		//pop up achievement
		//enable achievement for this id
		//change status to activated

			List<AchievementItem> alltimeList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achallTime16").ToList ();

			if (alltimeList [0].isUnlocked == false) {
				GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
				achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achName;
				achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achievementId;
				AssignLabelImage (alltimeList[0].goldReward, alltimeList[0].gemReward,alltimeList[0].pigyReward,0,0,achievementPopUp);
				//play sfx
				Framework.SoundManager.Instance.PlaySFX ("seReward");

				//Rewarding
				achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button> ().onClick.AddListener (() => VideoReward (achievementPopUp, alltimeList [0].goldReward * 2, alltimeList [0].gemReward * 2, alltimeList [0].pigyReward * 2, 0, 0));
				achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button> ().onClick.AddListener (() => Rewarding (alltimeList [0].goldReward, alltimeList [0].gemReward, achievementPopUp, alltimeList [0].pigyReward, 0, 0));
				alltimeList [0].isUnlocked = true;
			}

	}

	public void MyFirstPurchase()
	{
		//purchase any item from the shop for the first time using gem
		//pop up achievement
		//enable achievement for this id
		//change status to activated

		List<AchievementItem> alltimeList = DataController.Instance.achievementDatabase.items.Where 
		(x => x.achievementId == "achallTime17").ToList ();
			
		if (alltimeList [0].isUnlocked == false) 
		{
				//if (alltimeList [0].counter == 0) 
				//{
					GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
					//achievementPopUp.transform.SetSiblingIndex(15);

					achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achName;
					achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achievementId;
					AssignLabelImage (alltimeList[0].goldReward, alltimeList[0].gemReward,alltimeList[0].pigyReward,0,0,achievementPopUp);

					//play sfx
					Framework.SoundManager.Instance.PlaySFX ("seReward");
					achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button> ().onClick.AddListener (() => VideoReward (achievementPopUp, alltimeList [0].goldReward * 2, alltimeList [0].gemReward * 2, alltimeList [0].pigyReward * 2, 0, 0));
					achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button> ().onClick.AddListener (() => Rewarding (alltimeList [0].goldReward, alltimeList [0].gemReward, achievementPopUp, alltimeList [0].pigyReward, 0, 0));
					alltimeList [0].isUnlocked = true;

					alltimeList [0].counter = 1;
				//}
		}
	}


	public void RichKid()
	{
		//GEt 100000 gold
		//pop up achievement
		//enable achievement for this id
		//change status to activated

		List<AchievementItem> alltimeList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achallTime18").ToList ();
		
			if ( alltimeList [0].isUnlocked == false) {

				GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
				achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achName;
				achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achievementId;
				AssignLabelImage (alltimeList[0].goldReward, alltimeList[0].gemReward,alltimeList[0].pigyReward,0,0,achievementPopUp);

				achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button> ().onClick.AddListener (() => VideoReward (achievementPopUp, alltimeList [0].goldReward * 2, alltimeList [0].gemReward * 2, alltimeList [0].pigyReward * 2, 0, 0));
				achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button> ().onClick.AddListener (() => Rewarding (alltimeList [0].goldReward, alltimeList [0].gemReward, achievementPopUp, alltimeList [0].pigyReward, 0, 0));
				alltimeList [0].isUnlocked = true;
			}
	}



	public void AchangetoScenery()
	{
		//purchase your first background from shop and equip it
		//pop up achievement
		//enable achievement for this id
		//change status to activated
			List<AchievementItem> alltimeList = DataController.Instance.achievementDatabase.items.Where 
				(x => x.achievementId == "achallTime19").ToList ();

			if (alltimeList [0].isUnlocked == false) 
			{
				GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
				achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achName;
				achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achievementId;
				AssignLabelImage (alltimeList[0].goldReward, alltimeList[0].gemReward,alltimeList[0].pigyReward,0,0,achievementPopUp);

				//play sfx
				Framework.SoundManager.Instance.PlaySFX ("seReward");
		
				achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button> ().onClick.AddListener (() => VideoReward (achievementPopUp, alltimeList [0].goldReward * 2, alltimeList [0].gemReward * 2, alltimeList [0].pigyReward * 2, 0, 0));
				achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button> ().onClick.AddListener (() => Rewarding (alltimeList [0].goldReward, alltimeList [0].gemReward, achievementPopUp, alltimeList [0].pigyReward, 0, 0));
				alltimeList [0].isUnlocked = true;
			}

	}


	public void FrequentTraveller()
	{
		//Own three backgrounds(generic not included)
		//pop up achievement
		//enable achievement for this id
		//change status to activated
			List<AchievementItem> alltimeList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achallTime20").ToList ();

			if (alltimeList [0].isUnlocked == false) 
			{
				GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
				achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achName;
				achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achievementId;
				AssignLabelImage (alltimeList[0].goldReward, alltimeList[0].gemReward,alltimeList[0].pigyReward,0,0,achievementPopUp);
				//play sfx
				Framework.SoundManager.Instance.PlaySFX ("seReward");
			
				achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button> ().onClick.AddListener (() => VideoReward (achievementPopUp, alltimeList [0].goldReward * 2, alltimeList [0].gemReward * 2, alltimeList [0].pigyReward * 2, 0, 0));
				achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button> ().onClick.AddListener (() => Rewarding (alltimeList [0].goldReward, alltimeList [0].gemReward, achievementPopUp, alltimeList [0].pigyReward, 0, 0));
				alltimeList [0].isUnlocked = true;
			}

	}


	public void SuitUp()
	{
		//Have a pet equip with body head and accessory
		//pop up achievement
		//enable achievement for this id
		//change status to activated
			List<AchievementItem> alltimeList = DataController.Instance.achievementDatabase.items.Where 
				(x => x.achievementId == "achallTime21").ToList ();
				
			if (alltimeList [0].isUnlocked == false) 
			{
				GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
				achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achName;
				achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achievementId;
				AssignLabelImage (alltimeList[0].goldReward, alltimeList[0].gemReward,alltimeList[0].pigyReward,0,0,achievementPopUp);
				//play sfx
				Framework.SoundManager.Instance.PlaySFX ("seReward");

				achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button> ().onClick.AddListener (() => VideoReward (achievementPopUp, alltimeList [0].goldReward * 2, alltimeList [0].gemReward * 2, alltimeList [0].pigyReward * 2, 0, 0));
				achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button> ().onClick.AddListener (() => Rewarding (alltimeList [0].goldReward, alltimeList [0].gemReward, achievementPopUp, alltimeList [0].pigyReward, 0, 0));
				alltimeList [0].isUnlocked = true;
			}
	}


	public void ReadyForHallowen()
	{
		//Own a full set of costume(including any no generic background)
		//pop up achievement
		//enable achievement for this id
		//change status to activated

			List<AchievementItem> alltimeList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achallTime23").ToList ();

			if (alltimeList [0].isUnlocked == false) 
			{
				GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
				achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achName;
				achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achievementId;
				AssignLabelImage (alltimeList[0].goldReward, alltimeList[0].gemReward,alltimeList[0].pigyReward,0,0,achievementPopUp);
		
				//play sfx
				Framework.SoundManager.Instance.PlaySFX ("seReward");
	
				achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button> ().onClick.AddListener (() => VideoReward (achievementPopUp, alltimeList [0].goldReward * 2, alltimeList [0].gemReward * 2, alltimeList [0].pigyReward * 2, 0, 0));
				achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button> ().onClick.AddListener (() => Rewarding (alltimeList [0].goldReward, alltimeList [0].gemReward, achievementPopUp, alltimeList [0].pigyReward, 0, 0));
				alltimeList [0].isUnlocked = true;
			}

	}


	public void BigSpender()
	{
		//Spend 5000 gems and 1000000
		//pop up achievement
		//enable achievement for this id
		//change status to activated

			List<AchievementItem> alltimeList = DataController.Instance.achievementDatabase.items.Where (x => x.achievementId == "achallTime24").ToList ();
			if (alltimeList [0].isUnlocked == false) 
			{
				GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
				achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achName;
				achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = alltimeList [0].achievementId;
				AssignLabelImage (alltimeList[0].goldReward, alltimeList[0].gemReward,alltimeList[0].pigyReward,0,0,achievementPopUp);

				achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button> ().onClick.AddListener (() => VideoReward (achievementPopUp, alltimeList [0].goldReward * 2, alltimeList [0].gemReward * 2, alltimeList [0].pigyReward * 2, 0, 0));
				achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button> ().onClick.AddListener (() => Rewarding (alltimeList [0].goldReward, alltimeList [0].gemReward, achievementPopUp, alltimeList [0].pigyReward, 0, 0));
				alltimeList [0].isUnlocked = true;
			}

	}

	#endregion

	#region Weekly

//	public void HealthyPets()
//	{
//		//Maintain your pets to have above 50% for 5 days straight
//		//pop up achievement
//		//enable achievement for this id
//		//change status to activated
//
//		Debug.Log ("<color=blue> " + "Healthy Pets" + "</color>");
//
//		List<AchievementItem> weekList = DataController.Instance.achievementDatabase.items.Where 
//			(x => x.achievementId == "achWeekly1").ToList ();
//
//		DateTime thisDate = Convert.ToDateTime (weekList [0].dateLogin);
//		Debug.Log ("<color=blue> " + thisDate + "</color>");
//		Debug.Log ("<color=blue> " + DateTime.Today + "</color>");
//		Debug.Log ("<color=red> " + ( DateTime.Today - thisDate ).Days + "</color>");
//
//		if (weekList [0].dateLogin == "none") 
//		{
//			weekList [0].dateLogin = DateTime.Now.AddDays(-1).ToString ("MMMM dd, yyyy");
//		}
//
//
//		if (( DateTime.Today - thisDate).Days == 1) 
//		{
//			weekList [0].counter += 1;
//			weekList [0].dateLogin = DateTime.Now.ToString ("MMMM dd, yyyy");
//			Debug.Log ("not eqaul");
//
//
//			if (weekList [0].counter == weekList [0].target_counter) 
//			{
//				GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
//				achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = weekList [0].achName;
//				achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = weekList [0].achievementId;
//				AssignLabelImage (weekList[0].goldReward, weekList[0].gemReward,weekList[0].pigyReward,0,0,achievementPopUp);
//				//play sfx
//				Framework.SoundManager.Instance.PlaySFX ("seReward");
//
//				achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button>().onClick.AddListener(()=> VideoReward(achievementPopUp,weekList[0].goldReward * 2, weekList[0].gemReward * 2,weekList[0].pigyReward * 2, 0, 0));
//				achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button> ().onClick.AddListener (() => Rewarding (weekList [0].goldReward, weekList [0].gemReward, achievementPopUp, weekList [0].pigyReward, 0, 0));
//
//				weekList [0].counter = 0;
//
//				//add 1 to counter
//				weekCheckList[0].counter += 1;
//				//call weeklyChecklist if satisfied
//				WeeklyChecklist();
//			}
//		} 
//		else if ((thisDate - DateTime.Today).Days == 0) 
//		{
//			weekList [0].counter = weekList [0].counter;
//		}
//
//		else 
//		{
//			weekList [0].counter = 0;
//		}
//	}
	public void HealthyPets()
	{
		//Maintain your pets to have above 50% for 5 days straight
		//pop up achievement
		//enable achievement for this id
		//change status to activated

		List<AchievementItem> weekList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achWeekly1").ToList ();

		if (weekList [0].counter == 5) 
		{
			GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
			achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = weekList [0].achName;
			achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = weekList [0].achievementId;
			AssignLabelImage (weekList[0].goldReward, weekList[0].gemReward,weekList[0].pigyReward,0,0,achievementPopUp);
			//play sfx
			Framework.SoundManager.Instance.PlaySFX ("seReward");

			achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button>().onClick.AddListener(()=> VideoReward(achievementPopUp,weekList[0].goldReward * 2, weekList[0].gemReward * 2,weekList[0].pigyReward * 2, 0, 0));
			achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button> ().onClick.AddListener (() => Rewarding (weekList [0].goldReward, weekList [0].gemReward, achievementPopUp, weekList [0].pigyReward, 0, 0));

			weekList [0].counter = 0;
			weekList [0].isUnlocked = true;

			//add 1 to counter
			weekCheckList[0].counter += 1;
			//call weeklyChecklist if satisfied
			WeeklyChecklist();
		}
	}

	public void WeeklyLogin()
	{
		//Login for 5days straight
		//pop up achievement
		//enable achievement for this id
		//change status to activated
		List<AchievementItem> weekList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achWeekly2").ToList ();

		if (weekList [0].counter == weekList [0].target_counter) 
		{

			//Debug.Log ("Weekly login");
			serving = true;
			GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Pomodoro").transform, false);
			achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = weekList [0].achName;
			achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = weekList [0].achievementId;
			AssignLabelImage (weekList[0].goldReward, weekList[0].gemReward,weekList[0].pigyReward,0,0,achievementPopUp);
			//play sfx
			Framework.SoundManager.Instance.PlaySFX ("seReward");

			achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button>().onClick.AddListener(()=> VideoReward(achievementPopUp,weekList[0].goldReward * 2, weekList[0].gemReward * 2,weekList[0].pigyReward * 2, 0, 0));
			achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button>().onClick.AddListener(() => Rewarding(weekList[0].goldReward, weekList[0].gemReward,achievementPopUp,weekList[0].pigyReward,0,0));
			weekList [0].counter = 0;
			weekList [0].isUnlocked = true;
			weekList [0].target_gold += 1;
			//add 1 to counter
			weekCheckList[0].counter = 1;
			//call weeklyChecklist if satisfied
			WeeklyChecklist();
		}
	}

	IEnumerator DelayWorkBalance()
	{
		yield return new WaitForSeconds(1);

		List<AchievementItem> weekList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achWeekly3").ToList ();
		if (weekList [0].counter == 1 || weekList [0].counter % 7 == 0) {
			if (weekList [0].target_counter >= 108000) { // check if break time satisfies condition(30 hours = 108000 seconds)
				GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
				achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = weekList [0].achName;
				achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = weekList [0].achievementId;
				AssignLabelImage (weekList [0].goldReward, weekList [0].gemReward, weekList [0].pigyReward, 0, 0, achievementPopUp);
				//play sfx
				Framework.SoundManager.Instance.PlaySFX ("seReward");

				achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button> ().onClick.AddListener (() => VideoReward (achievementPopUp, weekList [0].goldReward * 2, weekList [0].gemReward * 2, weekList [0].pigyReward * 2, 0, 0));
				achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button> ().onClick.AddListener (() => Rewarding (weekList [0].goldReward, weekList [0].gemReward, achievementPopUp, weekList [0].pigyReward, 0, 0));
				weekList [0].target_counter = 0;

				weekList [0].isUnlocked = true;	
			
				//add 1 to counter
				weekCheckList [0].counter += 1;
				//call weeklyChecklist if satisfied
				WeeklyChecklist ();
			}
		}
	}
		
	public void WorkLifeBalance()
	{
		//Have a total of 30 hours of break time for 7 days
		//pop up achievement
		//enable achievement for this id
		//change status to activated
		//target counter = accumulated break

//		List<AchievementItem> weekList = DataController.Instance.achievementDatabase.items.Where 
//			(x => x.achievementId == "achWeekly3").ToList ();
//
//		if (weekList [0].target_counter >= 22) // check if break time satisfies condition(30 hours = 108000 seconds)
//		{
//			if (DataController.Instance.playerData.state == EnumCollection.PomodoroStates.LongBreakScreen) {
//				
//			}
//			else 
//			{
//				GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
//				achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = weekList [0].achName;
//				achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = weekList [0].achievementId;
//
//				//play sfx
//				Framework.SoundManager.Instance.PlaySFX ("seReward");
//
//				achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button> ().onClick.AddListener (() => VideoReward (achievementPopUp, weekList [0].goldReward * 2, weekList [0].gemReward * 2, weekList [0].pigyReward * 2, 0, 0));
//				achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button> ().onClick.AddListener (() => Rewarding (weekList [0].goldReward, weekList [0].gemReward, achievementPopUp, weekList [0].pigyReward, 0, 0));
//				weekList [0].target_counter = 0;	
//				weekList [0].isUnlocked = true;	
//
//				//add 1 to counter
//				weekCheckList [0].counter += 1;
//				//call weeklyChecklist if satisfied
//				WeeklyChecklist ();
//			}
//		}

		StartCoroutine("DelayWorkBalance");
	}

	// method for saving break time in achievementdatabase

	public void checkBreakTimeAchievement(float breakTime)
	{
		bool counterOn = false;

		//Debug.Log("<color=red>" + "Break Time duration checkBreakTimeAchievement :---------------------" + breakTime+ "--------------------" +   "</color>");

		List<AchievementItem> weekList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achWeekly3").ToList ();



		if (weekList [0].dateLogin == "") 
		{
			weekList [0].dateLogin = DateTime.Today.AddDays(-1).ToString("MMMM dd, yyyy");
		}

		DateTime counterDate = Convert.ToDateTime (weekList [0].dateLogin);

		if ((DateTime.Today - counterDate).Days == 1)
		{   
			//Debug.Log ("<color=red>" + "Counter 1111111111111111" + "</color>");
			weekList [0].counter += 1;
		}
			
		if (weekList [0].dateLogin != DateTime.Now.ToString ("MMMM dd, yyyy")) 
		{
			weekList [0].dateLogin = DateTime.Now.ToString ("MMMM dd, yyyy");
		}

		DateTime thisDate = Convert.ToDateTime (weekList [0].dateLogin);


		if ((DateTime.Today - thisDate).Days == 1 || (DateTime.Today - thisDate).Days == 0) 
		{
			weekList [0].target_counter += (int)breakTime;
		} 
			
		else 
		{
			weekList [0].target_counter = 0;
			weekList [0].dateLogin = DateTime.Now.ToString ("MMMM dd, yyyy");
		}

		 

	}


	public void PassionateWorker()
	{
		//Have atleast one session completed for 7 days straight
		//pop up achievement
		//enable achievement for this id
		//change status to activated

		List<AchievementItem> weekList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achWeekly4").ToList ();

		if (weekList [0].dateLogin == "") 
		{
			weekList [0].dateLogin = DateTime.Today.AddDays(-1).ToString("MMMM dd, yyyy");
		}

		DateTime thisDate = Convert.ToDateTime (weekList [0].dateLogin);

		//check if days is not skipped(consecutive days)
		if ((DateTime.Today - thisDate).Days == 1) 
		{
			weekList [0].counter += 1;
			weekList [0].dateLogin = DateTime.Now.ToString ("MMMM dd, yyyy");

			if (weekList [0].target_counter == weekList [0].counter)
			{
				GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
				achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = weekList [0].achName;
				achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = weekList [0].achievementId;
				AssignLabelImage (weekList[0].goldReward, weekList[0].gemReward,weekList[0].pigyReward,0,0,achievementPopUp);
				//play sfx
				Framework.SoundManager.Instance.PlaySFX ("seReward");

				achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button>().onClick.AddListener(()=> VideoReward(achievementPopUp,weekList[0].goldReward * 2, weekList[0].gemReward * 2,weekList[0].pigyReward * 2, 0, 0));
				achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button>().onClick.AddListener(() => Rewarding(weekList[0].goldReward, weekList[0].gemReward,achievementPopUp,weekList[0].pigyReward,0,0));
				weekList [0].counter = 0;	
				weekList [0].isUnlocked = true;	

				//add 1 to counter
				weekCheckList[0].counter += 1;
				//call weeklyChecklist if satisfied
				WeeklyChecklist();

			}
		} 

		//check if date is the same as today
		else if ((DateTime.Today - thisDate).Days == 0) 
		{
			//Debug.Log("<color=red>" +"same days"+ "</color>");
			weekList[0].counter = weekList[0].counter;
		}

		//check if not consecutive days
		else 
		{
			//Debug.Log("<color=red>" +"No consecutive days"+ "</color>");
			weekList[0].counter = 0;
		}

	}
		
	public void WeeklyChecklist()
	{
		//Complete all Weekly Achievement
		//pop up achievement
		//enable achievement for this id
		//change status to activated
		List<AchievementItem> weekList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achWeekly5").ToList ();

		if (weekList[0].counter == weekList[0].target_counter) 
		{
			GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
			achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = weekList [0].achName;
			achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = weekList [0].achievementId;
			AssignLabelImage (weekList[0].goldReward, weekList[0].gemReward,weekList[0].pigyReward,0,0,achievementPopUp);
				//play sfx
			Framework.SoundManager.Instance.PlaySFX ("seReward");

			achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button> ().onClick.AddListener (() => VideoReward (achievementPopUp, weekList [0].goldReward * 2, weekList [0].gemReward * 2, weekList [0].pigyReward * 2, 0, 0));
			achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button> ().onClick.AddListener (() => Rewarding (weekList [0].goldReward, weekList [0].gemReward, achievementPopUp, weekList [0].pigyReward, 0, 0));
			weekList [0].isUnlocked = true;

			//reset counter when achievement is unlocked
			weekList [0].counter = 0;

		}
	}

//	public void WeeklyShopping()
//	{
//		//Purchase for a total of 10000
//		//pop up achievement
//		//enable achievement for this id
//		//change status to activated
//
//		List<AchievementItem> weekListLogin = DataController.Instance.achievementDatabase.items.Where 
//			(x => x.achievementId == "achWeekly2").ToList ();
//
//		if(weekListLogin[0].target_gold == 1)
//		{
//			//Debug.Log ("<color=blue>" +"Weekly Shopping"+ "</color>");
//
//			List<AchievementItem> weekList = DataController.Instance.achievementDatabase.items.Where 
//				(x => x.achievementId == "achWeekly6").ToList ();
//
//			weekList [0].counter = (int)DataController.Instance.playerData.goldspent - weekList [0].target_gold;
//
//			if (weekList [0].counter >= 100) 
//			{
//				weekList [0].dateLogin = DateTime.Now.ToString ("MMMM dd, yyyy");
//
//				GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
//				achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = weekList [0].achName;
//				achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = weekList [0].achievementId;
//				AssignLabelImage (weekList[0].goldReward, weekList[0].gemReward,weekList[0].pigyReward,0,0,achievementPopUp);
//
//				achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button>().onClick.AddListener(()=> VideoReward(achievementPopUp,weekList[0].goldReward * 2, weekList[0].gemReward * 2,weekList[0].pigyReward * 2, 0, 0));
//				achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button>().onClick.AddListener(() => Rewarding(weekList[0].goldReward, weekList[0].gemReward,achievementPopUp,weekList[0].pigyReward,0,0));
//
//				weekList [0].counter = 0;
//				weekList [0].target_gold = (int)DataController.Instance.playerData.goldspent;
//				weekListLogin [0].target_gold = 0;
//
//				//add 1 to counter
//				weekCheckList[0].counter += 1;
//				//call weeklyChecklist if satisfied
//				WeeklyChecklist();
//			}
//		}
//	}

	public void WeeklyShopping()
	{
		//Purchase for a total of 10000
		//pop up achievement
		//enable achievement for this id
		//change status to activated

		//List<AchievementItem> weekListLogin = DataController.Instance.achievementDatabase.items.Where 
		//	(x => x.achievementId == "achWeekly2").ToList ();

		//if(weekListLogin[0].target_gold == 1)
		//{
		//	Debug.Log ("<color=blue>" +"Weekly Shopping"+ "</color>");

		List<AchievementItem> weekList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achWeekly6").ToList ();

		if (weekList [0].dateLogin == "") 
		{
			weekList [0].dateLogin = DateTime.Now.AddDays(-1).ToString ("MMMM dd, yyyy");
		}

		weekList [0].counter = (int)DataController.Instance.playerData.goldspent - weekList [0].target_gold;

		DateTime counterDate = Convert.ToDateTime (weekList [0].dateLogin);

		if((DateTime.Today - counterDate).Days == 1 || (DateTime.Today - counterDate).Days > 6 )
		{
		//	Debug.Log ("weekly achievement..........");
			if (weekList [0].counter >= 10000) 
			{
				weekList [0].dateLogin = DateTime.Now.ToString ("MMMM dd, yyyy");

				GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
				achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = weekList [0].achName;
				achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = weekList [0].achievementId;
				AssignLabelImage (weekList[0].goldReward, weekList[0].gemReward,weekList[0].pigyReward,0,0,achievementPopUp);

				achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button>().onClick.AddListener(()=> VideoReward(achievementPopUp,weekList[0].goldReward * 2, weekList[0].gemReward * 2,weekList[0].pigyReward * 2, 0, 0));
				achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button>().onClick.AddListener(() => Rewarding(weekList[0].goldReward, weekList[0].gemReward,achievementPopUp,weekList[0].pigyReward,0,0));

				weekList [0].counter = 0;
				weekList [0].isUnlocked = true;
				weekList [0].target_gold = (int)DataController.Instance.playerData.goldspent;
				//weekListLogin [0].target_gold = 0;

				//add 1 to counter
				weekCheckList[0].counter += 1;
				//call weeklyChecklist if satisfied
				WeeklyChecklist();
			}
		}
	}

//	public void WeeklyIncome()
//	{
//		//Get 5000 gold
//		//pop up achievement
//		//enable achievement for this id
//		//change status to activated
//
//		List<AchievementItem> weekListLogin = DataController.Instance.achievementDatabase.items.Where 
//			(x => x.achievementId == "achWeekly2").ToList ();
//		
//		if(weekListLogin[0].target_gold == 1)
//		{
//			//Debug.Log ("<color=blue>" +"Weekly Income"+ "</color>");
//
//			List<AchievementItem> weekList = DataController.Instance.achievementDatabase.items.Where 
//			(x => x.achievementId == "achWeekly7").ToList ();
//
//				weekList [0].counter = (int)DataController.Instance.playerData.goldacquired - weekList [0].target_gold;
//
//				if (weekList [0].counter >= 100) 
//				{
//					Debug.Log ("<color=blue>" +"weekList [0].counter >= 20"+ "</color>");
//					weekList [0].dateLogin = DateTime.Now.ToString ("MMMM dd, yyyy");
//
//					GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
//					achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = weekList [0].achName;
//					achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = weekList [0].achievementId;
//					AssignLabelImage (weekList[0].goldReward, weekList[0].gemReward,weekList[0].pigyReward,0,0,achievementPopUp);
//						
//					achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button>().onClick.AddListener(()=> VideoReward(achievementPopUp,weekList[0].goldReward * 2, weekList[0].gemReward * 2,weekList[0].pigyReward * 2, 0, 0));
//					achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button>().onClick.AddListener(() => Rewarding(weekList[0].goldReward, weekList[0].gemReward,achievementPopUp,weekList[0].pigyReward,0,0));
//
//					weekList [0].counter = 0;
//					weekList [0].target_gold = (int)DataController.Instance.playerData.goldacquired;
//					//weekListLogin [0].target_gold = 0;
//					//add 1 to counter
//					weekCheckList[0].counter += 1;
//					//call weeklyChecklist if satisfied
//					WeeklyChecklist();
//					weekListLogin [0].target_gold = 0;
//				}
//		}
//		
//	}
	public void WeeklyIncome()
	{
		//Get 5000 gold
		//pop up achievement
		//enable achievement for this id
		//change status to activated


		List<AchievementItem> weekList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achWeekly7").ToList ();

		if (weekList [0].dateLogin == "") 
		{
			weekList [0].dateLogin = DateTime.Now.AddDays(-1).ToString ("MMMM dd, yyyy");
		}
		weekList [0].counter = (int)DataController.Instance.playerData.goldacquired - weekList [0].target_gold;

		DateTime counterDate = Convert.ToDateTime (weekList [0].dateLogin);

		if ((DateTime.Today - counterDate).Days == 1 || (DateTime.Today - counterDate).Days > 6) 
		{
			//Debug.Log ("weekly income achievement..........");
			if (weekList [0].counter >= 5000) 
			{
				//Debug.Log ("<color=blue>" + "weekList [0].counter >= 20" + "</color>");
				weekList [0].dateLogin = DateTime.Now.ToString ("MMMM dd, yyyy");

				GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
				achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = weekList [0].achName;
				achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = weekList [0].achievementId;
				AssignLabelImage (weekList [0].goldReward, weekList [0].gemReward, weekList [0].pigyReward, 0, 0, achievementPopUp);

				achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button> ().onClick.AddListener (() => VideoReward (achievementPopUp, weekList [0].goldReward * 2, weekList [0].gemReward * 2, weekList [0].pigyReward * 2, 0, 0));
				achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button> ().onClick.AddListener (() => Rewarding (weekList [0].goldReward, weekList [0].gemReward, achievementPopUp, weekList [0].pigyReward, 0, 0));

				//fix for logged issue not graying out when this is claimed
				weekList [0].isUnlocked = true;

				weekList [0].counter = 0;
				weekList [0].target_gold = (int)DataController.Instance.playerData.goldacquired;
				//weekListLogin [0].target_gold = 0;
				//add 1 to counter
				weekCheckList [0].counter += 1;
				//call weeklyChecklist if satisfied
				WeeklyChecklist ();
			}
		}


	}

	#endregion

	#region daily

	public string sysTime;

	public void PureHappiness()
	{
		//Get your pet's love meter to 100 (once a day)
		//pop up achievement
		//enable achievement for this id
		//change status to activated
		List<AchievementItem> dailyList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achDailyOne").ToList ();
		
		if (dailyList [0].dateLogin != DateTime.Now.ToString ("MMMM dd, yyyy")) 
		{
			dailyList [0].dateLogin = DateTime.Now.ToString ("MMMM dd, yyyy");

			GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
			achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = dailyList [0].achName;
			achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = dailyList [0].achievementId;
			AssignLabelImage (dailyList[0].goldReward, dailyList[0].gemReward,dailyList[0].pigyReward,0,0,achievementPopUp);

			dailyList [0].isUnlocked = true;

			achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button>().onClick.AddListener(()=> VideoReward(achievementPopUp,dailyList[0].goldReward * 2, dailyList[0].gemReward * 2,dailyList[0].pigyReward * 2, 0, 0));
			achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button>().onClick.AddListener(() => Rewarding(dailyList[0].goldReward, dailyList[0].gemReward,achievementPopUp,dailyList[0].pigyReward,0,0));

			//add 1 to counter
			dailyCheckList[0].counter += 1;
			//call weeklyChecklist if satisfied
			DailyChecklist();

		} 
		//comment by Job - > goes locked 
//		else 
//		{
//			dailyList [0].isUnlocked = false;
//		}
	}

	public void AGoodMeal(EnumCollection.FoodItemLevel foodLevel)
	{
		//Feed your pet a level 3 food item(once a day)
		//pop up achievement
		//enable achievement for this id
		//change status to activated

		if (foodLevel == EnumCollection.FoodItemLevel.L3) 
		{
			List<AchievementItem> dailyList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achDailyTwo").ToList ();

			if (dailyList [0].dateLogin != DateTime.Now.ToString ("MMMM dd, yyyy")) 
			{
				dailyList [0].dateLogin = DateTime.Now.ToString ("MMMM dd, yyyy");

				GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
				achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = dailyList [0].achName;
				achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = dailyList [0].achievementId;
				AssignLabelImage (dailyList[0].goldReward, dailyList[0].gemReward,dailyList[0].pigyReward,0,0,achievementPopUp);

				achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button>().onClick.AddListener(()=> VideoReward(achievementPopUp,dailyList[0].goldReward * 2, dailyList[0].gemReward * 2,dailyList[0].pigyReward * 2, 0, 0));
				achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button>().onClick.AddListener(() => Rewarding(dailyList[0].goldReward, dailyList[0].gemReward,achievementPopUp,dailyList[0].pigyReward,0,0));

				dailyList [0].target_gold = (int)DataController.Instance.playerData.goldacquired;
				dailyList [0].isUnlocked = true;


				//add 1 to counter
				dailyCheckList[0].counter += 1;
				//call weeklyChecklist if satisfied
				DailyChecklist();
			}
		}
	}

//	public void DailyLogin()
//	{
//
//		Debug.Log ("<color=blue>" + "Daily Login" + "</color>");
//
//		List<AchievementItem> dailyList = DataController.Instance.achievementDatabase.items.Where 
//			(x => x.achievementId == "achDailyThree").ToList ();
//
//		List<AchievementItem> weekList = DataController.Instance.achievementDatabase.items.Where 
//			(x => x.achievementId == "achWeekly2").ToList ();
//
//		if (dailyList [0].dateLogin != DateTime.Now.ToString ("MMMM dd, yyyy")) 
//		{
//			//change dailyIncome counter value to get the last gold of player
//			List<AchievementItem> dailyListIncome = DataController.Instance.achievementDatabase.items.Where 
//				(x => x.achievementId == "achDailySeven").ToList ();
//			//change weekly counter value to get the last gold of player
//			List<AchievementItem> weeklyListIncome = DataController.Instance.achievementDatabase.items.Where 
//				(x => x.achievementId == "achWeekly7").ToList ();
//			//change weekly counter value to get the last gold of player
//			List<AchievementItem> dailyListShopping = DataController.Instance.achievementDatabase.items.Where 
//				(x => x.achievementId == "achDailyEight").ToList ();
//			//change weekly counter value to get the last gold of player
//			List<AchievementItem> weeklyListShopping = DataController.Instance.achievementDatabase.items.Where 
//				(x => x.achievementId == "achWeekly6").ToList ();
//
//			dailyListIncome [0].target_gold = (int)DataController.Instance.playerData.goldacquired;
//			weeklyListIncome [0].target_gold = (int)DataController.Instance.playerData.goldacquired;
//			dailyListShopping[0].target_gold = (int)DataController.Instance.playerData.goldspent;
//			weeklyListShopping [0].target_gold = (int)DataController.Instance.playerData.goldspent;
//
//			//Daily login Condition
//			dailyList [0].dateLogin = DateTime.Now.ToString ("MMMM dd, yyyy");
//			Debug.Log ("not eqaul");
//
//			GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
//			achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = dailyList [0].achName;
//			achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = dailyList [0].achievementId;
//
//			AssignLabelImage (dailyList [0].goldReward, dailyList [0].gemReward, dailyList [0].pigyReward, 0, 0,achievementPopUp);
//
//			achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button>().onClick.AddListener(()=> VideoReward(achievementPopUp,dailyList[0].goldReward * 2, dailyList[0].gemReward * 2,dailyList[0].pigyReward * 2, 0, 0));
//			achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button>().onClick.AddListener(() => Rewarding(dailyList[0].goldReward, dailyList[0].gemReward,achievementPopUp,dailyList[0].pigyReward,0,0));
//			weekList [0].counter += 1;
//			weekList [0].target_gold += 1;
//
//			//add 1 to counter
//			dailyCheckList[0].counter += 1;
//			//call weeklyChecklist if satisfied
//			DailyChecklist();
//
//			//reset daily achievementlist 
//			ResetDailyStatus ();
//
//		} 
//		else 
//		{
//			
//		}
//		dailyList [0].isUnlocked = true;
//
//	}

	public void DailyLogin()
	{

		//Debug.Log ("<color=blue>" + "Daily Login" + "</color>");


		List<AchievementItem> dailyList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achDailyThree").ToList ();

		List<AchievementItem> weekList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achWeekly2").ToList ();

		if (dailyList [0].dateLogin != DateTime.Now.ToString ("MMMM dd, yyyy")) 
		{
			serving = true;
			//change dailyIncome counter value to get the last gold of player
			List<AchievementItem> dailyListIncome = DataController.Instance.achievementDatabase.items.Where 
				(x => x.achievementId == "achDailySeven").ToList ();
			//change weekly counter value to get the last gold of player
			List<AchievementItem> weeklyListIncome = DataController.Instance.achievementDatabase.items.Where 
				(x => x.achievementId == "achWeekly7").ToList ();
			//change weekly counter value to get the last gold of player
			List<AchievementItem> dailyListShopping = DataController.Instance.achievementDatabase.items.Where 
				(x => x.achievementId == "achDailyEight").ToList ();
			//change weekly counter value to get the last gold of player
			List<AchievementItem> weeklyListShopping = DataController.Instance.achievementDatabase.items.Where 
				(x => x.achievementId == "achWeekly6").ToList ();

			dailyListIncome [0].target_gold = (int)DataController.Instance.playerData.goldacquired;
			weeklyListIncome [0].target_gold = (int)DataController.Instance.playerData.goldacquired;
			dailyListShopping[0].target_gold = (int)DataController.Instance.playerData.goldspent;
			weeklyListShopping [0].target_gold = (int)DataController.Instance.playerData.goldspent;

			//Daily login Condition
			dailyList [0].dateLogin = DateTime.Now.ToString ("MMMM dd, yyyy");
			//Debug.Log ("not eqaul");

			GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);

//			//achievement queing
//			if(GameObject.Find("panelPopupAchivement 1(Clone)") !=null)
//			{
//				//achievementPopUp.transform.SetParent (GameObject.Find ("Pomodoro").transform);
//				achievementPopUp.transform.SetSiblingIndex (0);
//			}


			achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = dailyList [0].achName;
			achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = dailyList [0].achievementId;

			AssignLabelImage (dailyList [0].goldReward, dailyList [0].gemReward, dailyList [0].pigyReward, 0, 0,achievementPopUp);

			achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button>().onClick.AddListener(()=> VideoReward(achievementPopUp,dailyList[0].goldReward * 2, dailyList[0].gemReward * 2,dailyList[0].pigyReward * 2, 0, 0));

			achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button>().onClick.AddListener(() => Rewarding(dailyList[0].goldReward, dailyList[0].gemReward,achievementPopUp,dailyList[0].pigyReward,0,0));


			weekList [0].counter += 1;
			weekList [0].target_gold += 1;

			//add 1 to counter
			dailyCheckList[0].counter = 1;
			//call weeklyChecklist if satisfied
			DailyChecklist();

			//reset daily achievementlist 
			ResetDailyStatus ();

			//use for checking health
			HealthChecker();
			HealthyPets ();

		} 
		else 
		{

		}
		dailyList [0].isUnlocked = true;

	}
		
	public void ProductiveDay()
	{
		//Complete a 2 hour work cycle (Work, short break, long break) without skipping
		List<AchievementItem> dailyList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achDailyFour").ToList ();

		//add time to achievement database

		//when date is not equal to date.now.
		if (dailyList [0].dateLogin != DateTime.Now.ToString ("MMMM dd, yyyy")) 
		{
			dailyList [0].target_counter = 1;
			dailyList [0].counter = 0;
			dailyList [0].dateLogin = DateTime.Now.ToString ("MMMM dd, yyyy");
		}

		//when date is equal to date now
		// (target_counter) -> 1 = achievement is available for redeem, 0 = achievement is not available for redeem
		if (dailyList [0].target_counter == 1) 
		{
			if (dailyList [0].dateLogin == DateTime.Now.ToString ("MMMM dd, yyyy") || dailyList [0].dateLogin == "") 
			{
				dailyList [0].dateLogin = DateTime.Now.ToString ("MMMM dd, yyyy");
				if (dailyList [0].counter >= 7200) //7200
				{ 
					GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
					achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = dailyList [0].achName;
					achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = dailyList [0].achievementId;
					AssignLabelImage (dailyList [0].goldReward, dailyList [0].gemReward, dailyList [0].pigyReward, 0, 0,achievementPopUp);

					achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button> ().onClick.AddListener (() => VideoReward (achievementPopUp, dailyList [0].goldReward * 2, dailyList [0].gemReward * 2, dailyList [0].pigyReward * 2, 0, 0));
					achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button> ().onClick.AddListener (() => Rewarding (dailyList [0].goldReward, dailyList [0].gemReward, achievementPopUp, dailyList [0].pigyReward, 0, 0));

					dailyList [0].isUnlocked = true;
					dailyList [0].counter = 0;
					dailyList [0].target_counter = 0;

					//add 1 to counter
					dailyCheckList[0].counter += 1;
					//call weeklyChecklist if satisfied
					DailyChecklist();
				}
			}
		}
	}

	public void AddproductiveDayTime(float time)
	{
		List<AchievementItem> dailyList = DataController.Instance.achievementDatabase.items.Where 
		(x => x.achievementId == "achDailyFour").ToList ();
		dailyList[0].counter += (int)time;
		//Debug.Log("<color=red> " + dailyList[0].achName + "Time value: "+ (int)time +  "</color>");
	}
		
	public void MultiTasking()
	{
		//Finish 25 tasks
		//pop up achievement
		//enable achievement for this id
		//change status to activated
		List<AchievementItem> dailyList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achDailyFive").ToList ();
		if (dailyList [0].dateLogin != DateTime.Now.ToString ("MMMM dd, yyyy")) 
		{
			
			if (TaskDataController.Instance.completedTaskData.taskItem.Count !=0 && TaskDataController.Instance.completedTaskData.taskItem.Count % 25 == 0) 
			{
			dailyList [0].dateLogin = DateTime.Now.ToString ("MMMM dd, yyyy");

			//Debug.Log ("not eqaul");
			GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
			achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = dailyList [0].achName;
			achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = dailyList [0].achievementId;
			AssignLabelImage (dailyList [0].goldReward, dailyList [0].gemReward, dailyList [0].pigyReward, 0, 0,achievementPopUp);
				//play sfx
			Framework.SoundManager.Instance.PlaySFX ("seReward");

			
			achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button> ().onClick.AddListener (() => VideoReward (achievementPopUp, dailyList [0].goldReward * 2, dailyList [0].gemReward * 2,dailyList[0].pigyReward * 2, 0, 0));
			achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button>().onClick.AddListener(() => Rewarding(dailyList[0].goldReward, dailyList[0].gemReward,achievementPopUp,dailyList[0].pigyReward,0,0));
			dailyList [0].isUnlocked = true;
			
			//add 1 to counter
			dailyCheckList[0].counter += 1;
			//call weeklyChecklist if satisfied
			DailyChecklist();

			}
		}

	}

	public void DailyChecklist()
	{
		//Complete all Daily Achivements
		//pop up achievement
		//enable achievement for this id
		//change status to activated
		//Debug.Log ("Daily CheckList");

		List<AchievementItem> dailyList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achDailySix").ToList ();

		if (dailyList[0].counter == dailyList[0].target_counter) 
		{
			GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
			achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = dailyList [0].achName;
			achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = dailyList [0].achievementId;
			AssignLabelImage (dailyList [0].goldReward, dailyList [0].gemReward, dailyList [0].pigyReward, 0, 0,achievementPopUp);
			//play sfx
			Framework.SoundManager.Instance.PlaySFX ("seReward");

			achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button> ().onClick.AddListener (() => VideoReward (achievementPopUp, dailyList [0].goldReward * 2, dailyList [0].gemReward * 2, dailyList [0].pigyReward * 2, 0, 0));
			achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button> ().onClick.AddListener (() => Rewarding (dailyList [0].goldReward, dailyList [0].gemReward, achievementPopUp, dailyList [0].pigyReward, 0, 0));
			dailyList [0].isUnlocked = true;
			//reset counter when achievement is unlocked
			dailyList [0].counter = 0;

		}
	}

	public void DailyIncome()
	{
		//Get 500 gold
		//pop up achievement
		//enable achievement for this id
		//change status to activated
		List<AchievementItem> dailyList = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achDailySeven").ToList ();

		if (dailyList [0].dateLogin != DateTime.Now.ToString ("MMMM dd, yyyy")) 
		{
			dailyList [0].counter = (int)DataController.Instance.playerData.goldacquired - dailyList [0].target_gold;

			if (dailyList [0].counter >= 500) 
			{
				dailyList [0].dateLogin = DateTime.Now.ToString ("MMMM dd, yyyy");

				GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
				//achievement queing
				//if(GameObject.Find("panelPopupAchivement 1(Clone)") !=null)
				//{
					//achievementPopUp.transform.SetParent (GameObject.Find ("Pomodoro").transform);
				//	achievementPopUp.transform.SetSiblingIndex (0);
				//}

				achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = dailyList [0].achName;
				achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = dailyList [0].achievementId;
				AssignLabelImage (dailyList [0].goldReward, dailyList [0].gemReward, dailyList [0].pigyReward, 0, 0,achievementPopUp);

				achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button>().onClick.AddListener(()=> VideoReward(achievementPopUp,dailyList[0].goldReward * 2, dailyList[0].gemReward * 2,dailyList[0].pigyReward * 2, 0, 0));
				achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button>().onClick.AddListener(() => Rewarding(dailyList[0].goldReward, dailyList[0].gemReward,achievementPopUp,dailyList[0].pigyReward,0,0));

				dailyList [0].counter = 0;
				dailyList [0].target_gold = (int)DataController.Instance.playerData.goldacquired;
				dailyList [0].isUnlocked = true;

				//add 1 to counter
				dailyCheckList[0].counter += 1;
				//call weeklyChecklist if satisfied
				DailyChecklist();
			}
		}
	}

	public void DailyShopping()
	{
		//Purchase for a total of 500
		//pop up achievement
		//enable achievement for this id
		//change status to activated
		List<AchievementItem> dailyList = DataController.Instance.achievementDatabase.items.Where (x => x.achievementId == "achDailyEight").ToList ();

		if (dailyList [0].dateLogin != DateTime.Now.ToString ("MMMM dd, yyyy")) 
		{
			dailyList [0].counter = (int)DataController.Instance.playerData.goldspent - dailyList [0].target_gold;

			if (dailyList [0].counter >= 500) 
			{
				dailyList [0].dateLogin = DateTime.Now.ToString ("MMMM dd, yyyy");

				//GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas"), false);
				GameObject achievementPopUp = Instantiate (gameObject.GetComponent<PopUp> ().achievementName, GameObject.Find ("Canvas").transform, false);
				//achievementPopUp.transform.SetAsLastSibling ();

				achievementPopUp.transform.Find ("Box/textachievementName").GetComponent<TextMeshProUGUI> ().text = dailyList [0].achName;
				achievementPopUp.transform.Find ("Box/txtID").GetComponent<TextMeshProUGUI> ().text = dailyList [0].achievementId;
				AssignLabelImage (dailyList [0].goldReward, dailyList [0].gemReward, dailyList [0].pigyReward, 0, 0,achievementPopUp);

				achievementPopUp.transform.Find ("Box/btndouble").GetComponent<Button>().onClick.AddListener(()=> VideoReward(achievementPopUp,dailyList[0].goldReward * 2, dailyList[0].gemReward * 2,dailyList[0].pigyReward * 2, 0, 0));
				achievementPopUp.transform.Find ("Box/btnNo").GetComponent<Button>().onClick.AddListener(() => Rewarding(dailyList[0].goldReward, dailyList[0].gemReward,achievementPopUp,dailyList[0].pigyReward,0,0));

				dailyList [0].counter = 0;
				dailyList [0].target_gold = (int)DataController.Instance.playerData.goldspent;
				dailyList [0].isUnlocked = true;

				//add 1 to counter
				dailyCheckList[0].counter += 1;
				//call weeklyChecklist if satisfied
				DailyChecklist();
			}
		}
	}

	#endregion

	#region BirdReward
	public void PiggyRewardList (uint gold, uint energyTreats, uint powerTreats)
	{
		string powerReward = "";
		string energyReward = "";
		string goldReward = "";

		GameObject piggyPopUp = Instantiate (gameObject.GetComponent<PopUp> ().PiggyPopUp, GameObject.Find ("Canvas").transform, false);


		if (gold > 0) 
		{
			if (gold > 1) 
			{
				goldReward += gold;
			} 
			else 
			{
				goldReward += gold;
			}
		}

		else 
		{
			piggyPopUp.transform.Find ("Panel").Find("gold").gameObject.SetActive (false);
			//piggyPopUp.transform.Find ("Panel").Find("lblGold").gameObject.SetActive (false);
		}
			
			
		if (energyTreats > 0) 
		{
			
			if (energyTreats > 1) 
			{
				energyReward += energyTreats;
			} 
			else 
			{
				energyReward += energyTreats;
			}
		}

		else 
		{
			piggyPopUp.transform.Find ("Panel").Find("energy").gameObject.SetActive (false);
			//piggyPopUp.transform.Find ("Panel").Find("lblEnergy").gameObject.SetActive (false);
		}

		if (powerTreats > 0) 
		{
			if (powerTreats > 1) 
			{
				powerReward += powerTreats;
			} 
			else 
			{
				powerReward += powerTreats;
			}
		}

		else 
		{
			piggyPopUp.transform.Find ("Panel").Find("power").gameObject.SetActive (false);
			//piggyPopUp.transform.Find ("Panel").Find("lblPower").gameObject.SetActive (false);
		}
			
		//print ("TERESA!");


		piggyPopUp.transform.Find ("Panel/gold/lblGold").GetComponent<TextMeshProUGUI> ().text = goldReward;
		piggyPopUp.transform.Find ("Panel/energy/lblEnergy").GetComponent<TextMeshProUGUI> ().text = "x" + energyReward;
		piggyPopUp.transform.Find ("Panel/power/lblPower").GetComponent<TextMeshProUGUI> ().text = "x" + powerReward;


		piggyPopUp.transform.Find ("btnclose").GetComponent<Button>().onClick.AddListener(() => closeWIndow(piggyPopUp));	
		piggyPopUp.transform.Find ("btnclose").GetComponent<Button>().onClick.AddListener(() => PiggyBankButton.instance.RebuildPig());	
	}
	#endregion

	public void BirdReward()
	{
		//print ("FANNY!");
		//---------------------------------------------------------------------------------------------------
		int reward = GetRandomReward();
		GameObject dialog = Instantiate(giftDialog, GameObject.Find("Canvas").transform, false);
		BirdGiftDialog confirmDialog = dialog.GetComponent<BirdGiftDialog>();

		// set captions
		confirmDialog.SetCaption("Bird reward!");
		if(reward == 0)
			confirmDialog.SetTextReward ("60", reward);
		else if(reward == 1)
			confirmDialog.SetTextReward ("1", reward);
		else
			confirmDialog.SetTextReward ("x1", reward);
			
		confirmDialog.OnDoubleEvent += () => {
			BirdGiftVideoAdReward (2, reward);
			Destroy (dialog);
		};
		confirmDialog.OnNoEvent += () =>  { GiftReward(1, reward);};
	
	
	}

	void BirdGiftVideoAdReward(uint multiplier, int rewardIndex)
	{
		//print ("VIDEO AD");
		VideoAds.Instance.CallVideoAds ();
		VideoAds.Instance.onVideoAdsFinished.RemoveAllListeners ();
		VideoAds.Instance.onVideoAdsFinished.AddListener(delegate {
			GiftReward (multiplier, rewardIndex);	
		} );
		//GiftReward (multiplier, rewardIndex);
	}

	int GetRandomReward()
	{
		return UnityEngine.Random.Range(0,3);;
	}

	void GiftReward(uint multiplier, int rewardIndex)
	{
		

		uint value = 0;
		string rewardInfo = "";
		switch(rewardIndex)
		{
		case 0:
			//gold
			value = 60 * multiplier;
			DataController.Instance.playerData.gold += value;
			DataController.Instance.playerData.goldacquired += value;
			rewardInfo = "Bird Reward";//value + " gold acquired!";
			break;
		case 1:
			//gem
			value = 1 * multiplier;
			DataController.Instance.playerData.gem += value;
			DataController.Instance.playerData.gemacquired += value;
			rewardInfo = "Bird Reward";//value + " gem acquired!";
			break;
		case 2:
			//energy treat

			value = 1 * multiplier;
			DataController.Instance.energyTreatDatabase.item [0].stock += value;
			rewardInfo = "Bird Reward"; //value + " energy treat acquired!";
			break;
		}
			
		//------------------------------------------------------------
		GameObject dialog = Instantiate(giftDialog, GameObject.Find("Canvas").transform, false);
		BirdGiftDialog confirmDialog = dialog.GetComponent<BirdGiftDialog>();
		confirmDialog.SetCaption (rewardInfo);
		if (rewardIndex != 2) 
		{
			confirmDialog.SetTextReward ("" + value, rewardIndex);
		} 
		else 
		{
			confirmDialog.SetTextReward ("x" + value, rewardIndex);
		}

		confirmDialog.SetConfirmCaption ("OK");
		confirmDialog.DisableNO ();
		confirmDialog.OnYesEvent += () => {Destroy(dialog);};
	}
		
	#region Rewarding

	public void Rewarding(uint gold,uint gem,GameObject achPanel,uint pigy,uint accessory,uint pet)
	{

		GameObject rewardPopUP = Instantiate (gameObject.GetComponent<PopUp> ().rewardPopup, GameObject.Find ("Canvas").transform, false);

		string rewardGold = "";
		string rewardGem = "";
		string rewardAccessory = "";
		string rewardPiggy = "";
		string rewardPet = "";

		if (gem > 0) 
		{
			//DataController.Instance.playerData.gem += gem;
			//DataController.Instance.playerData.gemacquired += gem;
			if (gem > 1) 
			{
				rewardGem +=  gem;
			} else
			{
				rewardGem += gem;
			}
		} 
		else 
		{
			rewardPopUP.transform.Find ("box/Panel").Find("gem").gameObject.SetActive (false);
			//rewardPopUP.transform.Find ("box/Panel/gem").Find("lblGem").gameObject.SetActive (false);
		}

		if (gold > 0) 
		{
			//DataController.Instance.playerData.gold += gold;
			//Add to gold acquired
			//DataController.Instance.playerData.goldacquired += gold;
			if (gold > 1) 
			{
				rewardGold += gold;
			} else 
			{
				rewardGold += gold;
			}


		}
		else 
		{
			rewardPopUP.transform.Find ("box/Panel").Find("gold").gameObject.SetActive (false);
			//rewardPopUP.transform.Find ("box/Panel").Find("lblGold").gameObject.SetActive (false);

		}
			
		if (accessory > 0) 
		{
			//DataController.Instance.playerData.gold += gold;
			if (accessory > 1) 
			{
				rewardAccessory += accessory;
			} else 
			{
				rewardAccessory += accessory;
			}
		}
		else 
		{
			rewardPopUP.transform.Find ("box/Panel").Find("accessories").gameObject.SetActive (false);
		}


		if (pet > 0) 
		{
			//DataController.Instance.playerData.gold += gold;
			if (pet > 1)
			{
				rewardPet += pet;
			} else
			{
				rewardPet += pet;
			}
		}
		else 
		{
			rewardPopUP.transform.Find ("box/Panel").Find("petCat").gameObject.SetActive (false);
			rewardPopUP.transform.Find ("box/Panel").Find("petDog").gameObject.SetActive (false);
		}
			
		if (pigy > 0)
		{
			//DataController.Instance.playerData.pigyBank += pigy;
			if (DataController.Instance.playerData.pigyBank > 100) 
			{
				DataController.Instance.playerData.pigyBank = 100;
			}


			if (pigy > 1) 
			{
				rewardPiggy += pigy;
			} else 
			{
				rewardPiggy += pigy;
			}
		}

		else 
		{
			rewardPopUP.transform.Find ("box/Panel").Find("piggy").gameObject.SetActive (false);
			//rewardPopUP.transform.Find ("box/Panel").Find("lblPiggy").gameObject.SetActive (false);
		}
		Destroy (achPanel); 

		//assign Reward for each label
		rewardPopUP.transform.Find ("box/Panel/gold/lblGold").GetComponent<TextMeshProUGUI> ().text = rewardGold;
		rewardPopUP.transform.Find ("box/Panel/gem/lblGem").GetComponent<TextMeshProUGUI> ().text = rewardGem;
		rewardPopUP.transform.Find ("box/Panel/piggy/lblPiggy").GetComponent<TextMeshProUGUI> ().text = rewardPiggy;

		rewardPopUP.transform.Find ("box/btnclose").GetComponent<Button>().onClick.AddListener(() => GiveReward(gold,gem,pigy));	
		rewardPopUP.transform.Find ("box/btnclose").GetComponent<Button>().onClick.AddListener(() => closeWIndow(rewardPopUP));				
	}


	//overload Rewarding function
	public void Rewarding(uint gold,uint gem,GameObject achPanel,uint pigy,uint accessory,uint pet,string petLabel)
	{
		//Debug.LogError ("rewarding pet label: " + petLabel);

		GameObject rewardPopUP = Instantiate (gameObject.GetComponent<PopUp> ().rewardPopup, GameObject.Find ("Canvas").transform, false);

		string rewardGold = "";
		string rewardGem = "";
		string rewardAccessory = "";
		string rewardPiggy = "";
		string rewardPet = "";

		if (gem > 0) 
		{
			//DataController.Instance.playerData.gem += gem;
			//DataController.Instance.playerData.gemacquired += gem;
			if (gem > 1) 
			{
				rewardGem +=  gem;
			} else
			{
				rewardGem += gem;
			}
		} 
		else 
		{
			rewardPopUP.transform.Find ("box/Panel").Find("gem").gameObject.SetActive (false);
			//rewardPopUP.transform.Find ("box/Panel/gem").Find("lblGem").gameObject.SetActive (false);
		}

		if (gold > 0) 
		{
			//DataController.Instance.playerData.gold += gold;
			//Add to gold acquired
			//DataController.Instance.playerData.goldacquired += gold;
			if (gold > 1) 
			{
				rewardGold += gold;
			} else 
			{
				rewardGold += gold;
			}


		}
		else 
		{
			rewardPopUP.transform.Find ("box/Panel").Find("gold").gameObject.SetActive (false);
			//rewardPopUP.transform.Find ("box/Panel").Find("lblGold").gameObject.SetActive (false);

		}

		if (accessory > 0) 
		{
			//DataController.Instance.playerData.gold += gold;
			if (accessory > 1) 
			{
				rewardAccessory += accessory;
			} else 
			{
				rewardAccessory += accessory;
			}
		}
		else 
		{
			rewardPopUP.transform.Find ("box/Panel").Find("accessories").gameObject.SetActive (false);
		}


		//set cat and dog image
		if (petLabel == "dog") 
		{
			rewardPopUP.transform.Find ("box/Panel").Find("petCat").gameObject.SetActive (false);
		} 
		else if (petLabel == "cat") 
		{
			rewardPopUP.transform.Find ("box/Panel").Find("petDog").gameObject.SetActive (false);
		}

		if (pigy > 0)
		{
			//DataController.Instance.playerData.pigyBank += pigy;
			if (DataController.Instance.playerData.pigyBank > 100) 
			{
				DataController.Instance.playerData.pigyBank = 100;
			}


			if (pigy > 1) 
			{
				rewardPiggy += pigy;
			} else 
			{
				rewardPiggy += pigy;
			}
		}

		else 
		{
			rewardPopUP.transform.Find ("box/Panel").Find("piggy").gameObject.SetActive (false);
			//rewardPopUP.transform.Find ("box/Panel").Find("lblPiggy").gameObject.SetActive (false);
		}
		Destroy (achPanel); 

		//assign Reward for each label
		rewardPopUP.transform.Find ("box/Panel/gold/lblGold").GetComponent<TextMeshProUGUI> ().text = rewardGold;
		rewardPopUP.transform.Find ("box/Panel/gem/lblGem").GetComponent<TextMeshProUGUI> ().text = rewardGem;
		rewardPopUP.transform.Find ("box/Panel/piggy/lblPiggy").GetComponent<TextMeshProUGUI> ().text = rewardPiggy;

		rewardPopUP.transform.Find ("box/btnclose").GetComponent<Button>().onClick.AddListener(() => GiveReward(gold,gem,pigy));	
		rewardPopUP.transform.Find ("box/btnclose").GetComponent<Button>().onClick.AddListener(() => closeWIndow(rewardPopUP));				
	}

	private static string FormatNumber(long num)
	{
		// Ensure number has max 3 significant digits (no rounding up can happen)
		long i = (long)Math.Pow(10, (int)Math.Max(0, Math.Log10(num) - 2));
		num = num / i * i;

		if (num >= 1000000000)
			return (num / 1000000000D).ToString("0.##") + "B";
		if (num >= 1000000)
			return (num / 1000000D).ToString("0.##") + "M";
		if (num >= 1000)
			return (num / 1000D).ToString("0.##") + "K";

		return num.ToString("#,0");
	}


	void GiveReward(uint gold,uint gem, uint pigy)
	{
		Scene scene = SceneManager.GetActiveScene ();
		DataController.Instance.playerData.gold += gold;
		DataController.Instance.playerData.goldacquired += gold;

		DataController.Instance.playerData.gem += gem;
		DataController.Instance.playerData.gemacquired += gem;

		DataController.Instance.playerData.pigyBank += pigy;

		// set piggy points to maximum value
		if (DataController.Instance.playerData.pigyBank > 100) 
		{
			DataController.Instance.playerData.pigyBank = 100;
		}

		// gold animation
		if (gold > 0) {
			

			if (scene.name == "Main") 
			{
				PomodoroUI.Instance.GoldAnimation (FormatNumber((long)gold));
				//print ("Ronald");
			} else 
			{
				//print ("Job");
				PomodoroUI.Instance.GoldAnimation (FormatNumber((long)gold));
			}
		}
	
//		//if finish pop up is active
//		// re parent to show after this dialog
//		if(GameObject.Find ("StoreDialog(Clone)") != null)
//		{
//			GameObject.Find ("StoreDialog(Clone)").transform.SetAsLastSibling ();
//		}
//
//		//if finish pop up is active
//		// re parent to show after this dialog
//		if (GameObject.Find ("panelPopupAchivement 1(Clone)") != null)
//		{
//			GameObject.Find ("panelPopupAchivement 1(Clone)").transform.SetAsLastSibling ();
//		}

		if(Store.ins != null)
		{
			Store.ins.UpdateVirtualMoney ();
		}

			
	}

	public void RewardWithVideoAds(uint gold,uint gem,GameObject achPanel,uint pigy,uint accessory,uint pet)
	{
		string rewardGold = "";
		string rewardGem = "";
		string rewardAccessory = "";
		string rewardPiggy = "";
		string rewardPet = "";

		GameObject rewardPopUP = Instantiate (gameObject.GetComponent<PopUp> ().rewardPopup, GameObject.Find ("Canvas").transform, false);

		if (gem > 0) 
		{
			DataController.Instance.playerData.gem += gem;
			DataController.Instance.playerData.gemacquired += gem;
			if (gem > 1) 
			{
				rewardGem = rewardGem + gem;
			} else 
			{
				rewardGem = rewardGem + gem;
			}
		}

		if (gold > 0) 
		{
			DataController.Instance.playerData.gold += gold;
			DataController.Instance.playerData.goldacquired += gold;
			if (gold > 1)
			{
				rewardGold = rewardGold + gold;
			} else
			{
				rewardGold = rewardGold + gold;
			}
				
		}

		if (pigy > 0)
		{
			DataController.Instance.playerData.pigyBank += pigy;
			if (DataController.Instance.playerData.pigyBank > 100) 
			{
				DataController.Instance.playerData.pigyBank = 100;
			}

			if (pigy > 1)
			{
				rewardPiggy = rewardPiggy + pigy;
			} else
			{
				rewardPiggy = rewardPiggy + pigy;
			}
		}

		if (pet > 0) 
		{
			//DataController.Instance.playerData.gold += gold;
			if (pet > 1)
			{
				rewardPet += pet;
			} else
			{
				rewardPet += pet;
			}
		}
			

		else 
		{
			rewardPopUP.transform.Find ("box/Panel").Find("imgPetCat").gameObject.SetActive (false);
			rewardPopUP.transform.Find ("box/Panel").Find("imgPetDog").gameObject.SetActive (false);
		}

		if (accessory > 0) 
		{
			//DataController.Instance.playerData.gold += gold;
			if (accessory > 1) 
			{
				rewardAccessory += accessory;
			} else 
			{
				rewardAccessory += accessory;
			}
		}
		else 
		{
			rewardPopUP.transform.Find ("box/Panel").Find("imgAccessories").gameObject.SetActive (false);
		}
				
		Destroy (achPanel);



		rewardPopUP.transform.Find ("box/Panel/lblGold").GetComponent<TextMeshProUGUI> ().text = rewardGold;
		rewardPopUP.transform.Find ("box/Panel/lblGem").GetComponent<TextMeshProUGUI> ().text = rewardGem;
		rewardPopUP.transform.Find ("box/Panel/lblPiggy").GetComponent<TextMeshProUGUI> ().text = rewardPiggy;

		rewardPopUP.transform.Find ("box/btnclose").GetComponent<Button>().onClick.AddListener(() => closeWIndow(rewardPopUP));	
	}
		

	public void closeWIndow(GameObject closePanel)
	{
		serving = false;
		Destroy (closePanel);
	}


	public void VideoReward(GameObject closepanel,uint gold, uint gem, uint piggyReward,uint accessory,uint pet)
	{

		if (Application.internetReachability != NetworkReachability.NotReachable) {

			//call video ads
			VideoAds.Instance.CallVideoAds ();

			Destroy (closepanel);

			VideoAds.Instance.onVideoAdsFinished.RemoveAllListeners ();
			//check if video ads successfuly finished
			VideoAds.Instance.onVideoAdsFinished.AddListener (delegate {

				//GiveReward(gold,gem,piggyReward);

				GameObject rewardPopUP = Instantiate (gameObject.GetComponent<PopUp> ().rewardPopup, GameObject.Find ("Canvas").transform, false);

				string rewardGold = "";
				string rewardGem = "";
				string rewardAccessory = "";
				string rewardPiggy = "";
				string rewardPet = "";

				if (gem > 0) {
					//DataController.Instance.playerData.gem += gem;
					if (gem > 1) {
						rewardGem += gem;
					} else {
						rewardGem += gem;
					}
				} else {
					rewardPopUP.transform.Find ("box/Panel").Find ("gem").gameObject.SetActive (false);
					//rewardPopUP.transform.Find ("box/Panel").Find("gGem").gameObject.SetActive (false);
				}

				if (gold > 0) {
					//DataController.Instance.playerData.gold += gold;
					if (gold > 1) {
						rewardGold += gold;
					} else {
						rewardGold += gold;
					}
				} else {
					rewardPopUP.transform.Find ("box/Panel").Find ("gold").gameObject.SetActive (false);
					//rewardPopUP.transform.Find ("box/Panel").Find("lblGold").gameObject.SetActive (false);
				}

				if (piggyReward > 0) {
					//DataController.Instance.playerData.pigyBank += piggyReward;
					if (piggyReward > 1) {
						rewardPiggy += piggyReward;
					} else {
						rewardPiggy += piggyReward;
					}
				} else {
					rewardPopUP.transform.Find ("box/Panel").Find ("piggy").gameObject.SetActive (false);
					//rewardPopUP.transform.Find ("box/Panel").Find("lblPiggy").gameObject.SetActive (false);
				}


				if (pet > 0) {
					//DataController.Instance.playerData.gold += gold;
					if (pet > 1) {
						rewardPet += pet;
					} else {
						rewardPet += pet;
					}
				} else {
					rewardPopUP.transform.Find ("box/Panel").Find ("petCat").gameObject.SetActive (false);
					rewardPopUP.transform.Find ("box/Panel").Find ("petDog").gameObject.SetActive (false);
				}

				if (accessory > 0) {
					//DataController.Instance.playerData.gold += gold;
					if (accessory > 1) {
						rewardAccessory += accessory;
					} else {
						rewardAccessory += accessory;
					}
				} else {
					rewardPopUP.transform.Find ("box/Panel").Find ("accessories").gameObject.SetActive (false);
				}

	
			
				rewardPopUP.transform.Find ("box/Panel/gold/lblGold").GetComponent<TextMeshProUGUI> ().text = rewardGold;
				rewardPopUP.transform.Find ("box/Panel/gem/lblGem").GetComponent<TextMeshProUGUI> ().text = rewardGem;
				rewardPopUP.transform.Find ("box/Panel/piggy/lblPiggy").GetComponent<TextMeshProUGUI> ().text = rewardPiggy;
		
				rewardPopUP.transform.Find ("box/btnclose").GetComponent<Button> ().onClick.AddListener (() => GiveReward (gold, gem, piggyReward));	
				rewardPopUP.transform.Find ("box/btnclose").GetComponent<Button> ().onClick.AddListener (() => closeWIndow (rewardPopUP));	

			});
		} 
		else 
		{
			GameObject finishTaskPopUps = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), GameObject.Find("Canvas").transform, false);
			ShowDialog dialog = finishTaskPopUps.GetComponent<ShowDialog>();

			dialog.DisableNO ();

			dialog.SetCaption("There seems to be a problem with your connection.");
			dialog.SetConfirmCaption("OK");
			// set events
			dialog.OnYesEvent += () =>
			{
				//Destroy (this.gameObject);	
			};
		}
	}

	//overload video reward for achievement having free pet

	public void VideoReward(GameObject closepanel,uint gold, uint gem, uint piggyReward,uint accessory,uint pet, string petLabel)
	{

		if (Application.internetReachability != NetworkReachability.NotReachable) {


			//call video ads
			VideoAds.Instance.CallVideoAds ();

			Destroy (closepanel);

			VideoAds.Instance.onVideoAdsFinished.RemoveAllListeners ();
			//check if video ads successfuly finished
			VideoAds.Instance.onVideoAdsFinished.AddListener (delegate {

				//GiveReward(gold,gem,piggyReward);

				GameObject rewardPopUP = Instantiate (gameObject.GetComponent<PopUp> ().rewardPopup, GameObject.Find ("Canvas").transform, false);

				string rewardGold = "";
				string rewardGem = "";
				string rewardAccessory = "";
				string rewardPiggy = "";
				string rewardPet = "";

				if (gem > 0) {
					//DataController.Instance.playerData.gem += gem;
					if (gem > 1) {
						rewardGem += gem;
					} else {
						rewardGem += gem;
					}
				} else {
					rewardPopUP.transform.Find ("box/Panel").Find ("gem").gameObject.SetActive (false);
					//rewardPopUP.transform.Find ("box/Panel").Find("gGem").gameObject.SetActive (false);
				}

				if (gold > 0) {
					//DataController.Instance.playerData.gold += gold;
					if (gold > 1) {
						rewardGold += gold;
					} else {
						rewardGold += gold;
					}
				} else {
					rewardPopUP.transform.Find ("box/Panel").Find ("gold").gameObject.SetActive (false);
					//rewardPopUP.transform.Find ("box/Panel").Find("lblGold").gameObject.SetActive (false);
				}

				if (piggyReward > 0) {
					//DataController.Instance.playerData.pigyBank += piggyReward;
					if (piggyReward > 1) {
						rewardPiggy += piggyReward;
					} else {
						rewardPiggy += piggyReward;
					}
				} else {
					rewardPopUP.transform.Find ("box/Panel").Find ("piggy").gameObject.SetActive (false);
					//rewardPopUP.transform.Find ("box/Panel").Find("lblPiggy").gameObject.SetActive (false);
				}
					
				//set cat and dog image
				if (petLabel == "dog") 
				{
					rewardPopUP.transform.Find ("box/Panel").Find("petCat").gameObject.SetActive (false);
				} 
				else if (petLabel == "cat") 
				{
					rewardPopUP.transform.Find ("box/Panel").Find("petDog").gameObject.SetActive (false);
				}


				if (accessory > 0) {
					//DataController.Instance.playerData.gold += gold;
					if (accessory > 1) {
						rewardAccessory += accessory;
					} else {
						rewardAccessory += accessory;
					}
				} else {
					rewardPopUP.transform.Find ("box/Panel").Find ("accessories").gameObject.SetActive (false);
				}



				rewardPopUP.transform.Find ("box/Panel/gold/lblGold").GetComponent<TextMeshProUGUI> ().text = rewardGold;
				rewardPopUP.transform.Find ("box/Panel/gem/lblGem").GetComponent<TextMeshProUGUI> ().text = rewardGem;
				rewardPopUP.transform.Find ("box/Panel/piggy/lblPiggy").GetComponent<TextMeshProUGUI> ().text = rewardPiggy;

				rewardPopUP.transform.Find ("box/btnclose").GetComponent<Button> ().onClick.AddListener (() => GiveReward (gold, gem, piggyReward));	
				rewardPopUP.transform.Find ("box/btnclose").GetComponent<Button> ().onClick.AddListener (() => closeWIndow (rewardPopUP));	

			});
		} 
		else 
		{
			GameObject finishTaskPopUps = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), GameObject.Find("Canvas").transform, false);
			ShowDialog dialog = finishTaskPopUps.GetComponent<ShowDialog>();

			dialog.DisableNO ();

			dialog.SetCaption("There seems to be a problem with your connection.");
			dialog.SetConfirmCaption("OK");
			// set events
			dialog.OnYesEvent += () =>
			{
				//Destroy (this.gameObject);	
			};
		}
	}
	#endregion





	//Checker
    /// <summary>
    /// Count total pets
    /// </summary>
	public void PetCountChecker()
	{
		if (DataController.Instance.playerData.playerPetList.Count == 12)
		{
			TwelvePets ();
		} 
		else if (DataController.Instance.playerData.playerPetList.Count == 8) 
		{
			EightPets ();
			//free last pet
		} 
		else if (DataController.Instance.playerData.playerPetList.Count == 5)
		{
			FivePets ();
			//free dog
		} 
		else if (DataController.Instance.playerData.playerPetList.Count == 3) 
		{
			ThreePets ();
			//free cat
		}
		else if (DataController.Instance.playerData.playerPetList.Count == 1) 
		{
			MyfirstPet ();
		} 
	}
    /// <summary>
    /// Check Total Small Dogs
    /// </summary>
	public void SmallDogChecker()
	{
		int counter = 0;
		for (int i = 0; i < DataController.Instance.dogDatabase.items.Length; i++) 
		{
			if (DataController.Instance.dogDatabase.items [i].isOwned == true && DataController.Instance.dogDatabase.items [i].petCategory == EnumCollection.PetCategory.Small) 
			{
				counter++;
			}
		}
		if (counter == 3) 
		{
			ThreeSmallDogs ();
		}
	}
    /// <summary>
    /// Check Total Big Dogs
    /// </summary>
	public void BigDogChecker()
	{
		int counter = 0;
		for (int i = 0; i < DataController.Instance.dogDatabase.items.Length; i++) 
		{
			if (DataController.Instance.dogDatabase.items [i].isOwned == true && DataController.Instance.dogDatabase.items [i].petCategory == EnumCollection.PetCategory.Big) 
			{
				counter++;
			}
		}

		if (counter == 3) 
		{
			ThreeBigDog ();
		}
	}
    /// <summary>
    /// Three Short Haired Cat
    /// </summary>
	public void ThreeShortHairedCat()
	{
		int counter = 0;

		for (int i = 0; i < DataController.Instance.catDatabase.items.Length; i++) 
		{
			if (DataController.Instance.catDatabase.items [i].isOwned == true && DataController.Instance.catDatabase.items [i].petCategory == EnumCollection.PetCategory.ShortHair)
			{
				//Debug.Log ("<color=red>" + DataController.Instance.dogDatabase.items [i].isOwned + "</color>");
				counter++;
			}
		}

		if (counter == 3)
		{
			ThreeShortHaired ();
		}
	}
    /// <summary>
    /// Three Haired cat.
    /// </summary>
	public void CheckThreeHairyCat()
	{

		int counter = 0;

		for (int i = 0; i < DataController.Instance.catDatabase.items.Length; i++)
		{
			if (DataController.Instance.catDatabase.items [i].isOwned == true && DataController.Instance.catDatabase.items [i].petCategory == EnumCollection.PetCategory.Hairy) 
			{
				counter++;
			}
		}

		if (counter == 3) 
		{
			ThreeHairyCat ();
		}
	}
    /// <summary>
    /// Total Dogs Owned
    /// </summary>
	public void DogCounter()
	{
		int counter = 0;

		//Debug.Log("Dog Counter");

		for (int i = 0; i < DataController.Instance.dogDatabase.items.Length; i++) 
		{
			if (DataController.Instance.dogDatabase.items [i].isOwned == true) 
			{
				counter++;
			}
		}

		if (counter == 6) 
		{
				//Debug.Log ("Six Dogs");
				SixDogs ();
		}
		else if (counter == 10) 
		{
			TwelveDogs ();
		}
		
	}
    /// <summary>
    /// Total Cats Owned
    /// </summary>
	public void CatCounter()
	{
		int counter = 0;
		for (int i = 0; i < DataController.Instance.catDatabase.items.Length; i++) 
		{
			if (DataController.Instance.catDatabase.items [i].isOwned == true) 
			{
				counter++;
			}
		}

		if (counter == 6 ) 
		{
				SixCats ();
		}
			
		else if(counter==10)
		{
				TwelveCats ();
		}
	}
    /// <summary>
    /// Total Gems Spent
    /// </summary>
	public void GemSpent()
	{
		if (DataController.Instance.playerData.gemspent > 1) 
		{
			MyFirstPurchase ();
		}
		if (DataController.Instance.playerData.gemspent >= 500) 
		{
			Shopaholic ();
		}

        CallBigSpender();
	}        	
    /// <summary>
    /// Spent 1,000,000 gold and 5,000 spent gem
    /// </summary>
	public void CallBigSpender()
	{
		if (DataController.Instance.playerData.goldspent >= 1000000 && DataController.Instance.playerData.gemspent >= 5000) 
		{
			BigSpender ();
		}
	}
    /// <summary>
    /// Gold acquired 100,000
    /// </summary>
	public void CallRichKid()
	{
		if (DataController.Instance.playerData.goldacquired >= 100000) 
		{
			RichKid ();
		}

        CallBigSpender();
	}        
    /// <summary>
    /// Total backgrounds aquired
    /// </summary>
	public void AchievementBackground()
	{
		int counter = 0;

		for (int i = 0; i < DataController.Instance.backgroundDatabase.items.Length; i++) 
		{
			if (DataController.Instance.backgroundDatabase.items [i].isOwned == true ) 
			{
				counter++;
			}
		}

		if (counter == 3) 
		{
			FrequentTraveller ();
		}
		else if(counter == 2)
		{
			AchangetoScenery();
		}
	}

	//SuitUP,Ready for Hollowen
    /// <summary>
    /// Checks total suits
    /// </summary>
	public void Suits()
	{                   
		bool isSuitUp = false;

		bool hasFullsetOfCostume = false;
		bool hasNoGenericBG = false;
		int counter = 0;

		for (int i = 0; i < DataController.Instance.playerData.playerPetList.Count; i++) 
		{
			if (DataController.Instance.playerData.playerPetList    [i].headEquipment != "none"
				&& DataController.Instance.playerData.playerPetList [i].bodyEquipment != "none"
				&& DataController.Instance.playerData.playerPetList [i].accessories   != "none")
			{
				isSuitUp = true;
			}
		}

		if (isSuitUp == true) 
		{
			SuitUp ();
		}
			
		//ready for holloween

		for (int i = 0; i < DataController.Instance.backgroundDatabase.items.Length; i++) 
		{
			if (DataController.Instance.backgroundDatabase.items [i].isOwned == true ) 
			{
				counter++;
			}
		}

		//check if bark vader is owned
		for (int i = 0; i < DataController.Instance.bodyDatabase.items.Count(); i++) 
		{
			if (DataController.Instance.bodyDatabase.items [i].EquipmentName == "Bark Vader" && DataController.Instance.bodyDatabase.items [i].isOwned == true 
				&&  DataController.Instance.headDatabase.items [i].EquipmentName == "Bark Vader" && DataController.Instance.bodyDatabase.items [i].isOwned) 
			{
				hasFullsetOfCostume = true;
			}
		}
		//check if cattain americat is owned
		for (int i = 0; i < DataController.Instance.bodyDatabase.items.Count(); i++) 
		{
			if (DataController.Instance.bodyDatabase.items [i].EquipmentName == "Cat-tain Americat" && DataController.Instance.bodyDatabase.items [i].isOwned == true 
				&&  DataController.Instance.headDatabase.items [i].EquipmentName == "Cat-tain Americat" && DataController.Instance.bodyDatabase.items [i].isOwned) 
			{
				hasFullsetOfCostume = true;
			}
		}

		//check if Barking Bad is owned
		for (int i = 0; i < DataController.Instance.bodyDatabase.items.Count(); i++) 
		{
			if (DataController.Instance.bodyDatabase.items [i].EquipmentName == "Barking Bad" && DataController.Instance.bodyDatabase.items [i].isOwned == true 
				&&  DataController.Instance.headDatabase.items [i].EquipmentName == "Barking Bad" && DataController.Instance.bodyDatabase.items [i].isOwned) 
			{
				hasFullsetOfCostume = true;
			}
		}

		if (hasFullsetOfCostume == true && counter > 1) 
		{
			ReadyForHallowen ();
		}
	}
		
	int GetRandomCat()
	{
		int catIndex = 0;

		List<int> catrewardList = new List<int> ();
		PetBase persian = DataController.Instance.playerData.playerPetList.FirstOrDefault (b => b.petBreed.Contains ("Persian"));
		PetBase abyssinian = DataController.Instance.playerData.playerPetList.FirstOrDefault (b => b.petBreed.Contains ("Abyssinian"));

		if (persian == null) {
			catrewardList.Add (1);
		} else {
			//Debug.LogError ("persian not  available");
		}

		if (abyssinian == null) {
			catrewardList.Add (2);
		} else {
			//Debug.LogError ("aby not  available");
		}

		int petCatIndex = catrewardList[UnityEngine.Random.Range(0,catrewardList.Count-1)];

		//Debug.Log("Selected pet is " + petCatIndex + " 1 is persian 2 is aby");

		return petCatIndex;
	}

	int GetRandomDog()
	{
		int dogIndex = 0;

		List<int> dogrewardList = new List<int> ();
		PetBase beagle = DataController.Instance.playerData.playerPetList.FirstOrDefault (b => b.petBreed.Contains ("Beagle"));
		PetBase golden = DataController.Instance.playerData.playerPetList.FirstOrDefault (b => b.petBreed.Contains ("Golden_Retriever"));

		if (beagle == null) 
		{
			dogrewardList.Add (1);
		} else 
		{
			//Debug.LogError ("beagle not  available");
		}

		if (golden == null)
		{
			dogrewardList.Add (2);
		}
		else
		{
			//Debug.LogError ("golden not  available");
		}

		dogIndex = dogrewardList[UnityEngine.Random.Range(0,dogrewardList.Count-1)];
		//Debug.Log("Selected pet is " + dogIndex + " 1 is beagle 2 is golden");
		return dogIndex;
	}

	/// <summary>
	/// Cat pet reward.
	/// </summary>
	void CatPetReward()
	{
		AdoptCat (GetRandomCat ());
	}

	/// <summary>
	/// Dog pet reward.
	/// </summary>


	void AdoptCat(int index)
	{
		PetBase petBase = new PetBase();
		if (index == 1) {
			petBase.petName  = "Persian";
			petBase.petBreed = "Persian";
			petBase.petType  = EnumCollection.Pet.Cat;
			petBase.petCategory = EnumCollection.PetCategory.Hairy;

			var catList = DataController.Instance.catDatabase.items.FirstOrDefault (p => p.breedname == EnumCollection.CatBreed.Persian);
			catList.isOwned = true;

		}
		if (index == 2) 
		{
			petBase.petName = "Abyssinian";
			petBase.petBreed = "Abyssinian";
			petBase.petType = EnumCollection.Pet.Cat;
			petBase.petCategory = EnumCollection.PetCategory.ShortHair;

			var catList = DataController.Instance.catDatabase.items.FirstOrDefault (p => p.breedname == EnumCollection.CatBreed.Abyssinian);
			catList.isOwned = true;
		}

		DataController.Instance.playerData.playerPetList.Add(petBase);

        // Create pet
        if(SceneManager.GetActiveScene().name == "Main")
        {
            FindObjectOfType<PetHomeController>().SetPet();
        }
	}

	void DogPetReward()
	{
		AdoptDog(GetRandomDog());
	}

	void AdoptDog(int index)
	{
		PetBase petBase = new PetBase();
		//Debug.Log ("Index:" + index);

		if (index == 1) {
			petBase.petName  = "Beagle";
			petBase.petBreed = "Beagle";
			petBase.petType  = EnumCollection.Pet.Dog;
			petBase.petCategory = EnumCollection.PetCategory.Small;
			DataController.Instance.playerData.playerPetList.Add(petBase);

			var dogList = DataController.Instance.dogDatabase.items.FirstOrDefault (p => p.breedname == EnumCollection.DogBreed.Beagle);
			dogList.isOwned = true;

		}
		if (index == 2) 
		{
			petBase.petName  = "Golden Retriever";
			petBase.petBreed = "Golden_Retriever";
			petBase.petType  = EnumCollection.Pet.Dog;
			petBase.petCategory = EnumCollection.PetCategory.Big;
			DataController.Instance.playerData.playerPetList.Add(petBase);

			var dogList = DataController.Instance.dogDatabase.items.FirstOrDefault (p => p.breedname == EnumCollection.DogBreed.Golden_Retriever);
			dogList.isOwned = true;
		}

        // create pet instance
        if(SceneManager.GetActiveScene().name == "Main")
        {
            FindObjectOfType<PetHomeController>().SetPet();
        }
	}


	//Claim free reward 

	public void ClaimFreeReward(int rewardIndex)
	{
		
		//print ("VIDEO AD");
		//VideoAds.Instance.CallVideoAds (0, 0, 0);
		VideoAds.Instance.onVideoAdsFinished.RemoveAllListeners ();
		VideoAds.Instance.onVideoAdsFinished.AddListener(delegate {
			ClaimReward (rewardIndex);	
		} );
		//GiftReward (multiplier, rewardIndex);
	}


//	void ClaimReward(int rewardIndex)
//	{
//		int value = 0;
//		string rewardInfo = "";
//		switch(rewardIndex)
//		{
//		case 0:
//			//gold
//			value = 150;
//			DataController.Instance.playerData.gold += 150;
//			DataController.Instance.playerData.goldacquired += 150;
//			rewardInfo = "Claim Free Reward";//value + " gold acquired!";
//			Store.ins.UpdateVirtualMoney();
//		//	Destroy(GameObject.Find("GiftDialog(Clone)"));
//			break;
//		case 1:
//			//gem
//			value = 1;
//			DataController.Instance.playerData.gem += 1;
//			DataController.Instance.playerData.gemacquired += 1;
//			rewardInfo = "Claim Free Reward";//value + " gem acquired!";
//			Store.ins.UpdateVirtualMoney();
//			break;
//		case 2:
//			//energy treat
//			value = 1;
//			DataController.Instance.energyTreatDatabase.item [0].stock += 1;
//			rewardInfo = "Claim Free Reward"; //value + " energy treat acquired!";
//			Store.ins.UpdateVirtualMoney();
//			break;
//		}
//
//		//------------------------------------------------------------
//		GameObject dialog = Instantiate(giftDialog, GameObject.Find("Canvas").transform, false);
//		BirdGiftDialog confirmDialog = dialog.GetComponent<BirdGiftDialog>();
//		confirmDialog.SetCaption (rewardInfo);
//		if(rewardIndex == 2)
//			confirmDialog.SetTextReward ("x" + value, rewardIndex);
//		else
//			confirmDialog.SetTextReward ("" + value, rewardIndex);
//
//		confirmDialog.SetConfirmCaption ("Ok");
//		confirmDialog.DisableNO ();
//		confirmDialog.OnYesEvent += () => {Destroy(dialog);};
//
//		Framework.SoundManager.Instance.PlaySFX("seReward");
//	}

	void ClaimReward(int rewardIndex)
	{
		uint value = 0;
		string rewardInfo = "";
		switch(rewardIndex)
		{
		case 0:
			//gold
			value = 150;
			DataController.Instance.playerData.gold += value;
			DataController.Instance.playerData.goldacquired += value;
			rewardInfo = "Claim Free Reward";//value + " gold acquired!";
			Store.ins.UpdateVirtualMoney();
			//	Destroy(GameObject.Find("GiftDialog(Clone)"));
			break;
		case 1:
			//gem
			value = 1;
			DataController.Instance.playerData.gem += value;
			DataController.Instance.playerData.gemacquired += value;
			rewardInfo = "Claim Free Reward";//value + " gem acquired!";
			Store.ins.UpdateVirtualMoney();
			break;
		case 2:
			//energy treat
			value = 2;
			DataController.Instance.energyTreatDatabase.item [0].stock += value;

			PetFoodItem petFoodItem = DataController.Instance.foodDatabase.item.First (item => item.itemName == "Energy Treat");
			if(petFoodItem != null)
			{
				petFoodItem.stock += value;
			}
			Store.ins.UpdateFoodItem ();
			rewardInfo = "Claim Free Reward"; //value + " energy treat acquired!";
			break;
		}

		//------------------------------------------------------------

		GameObject dialog = Instantiate(giftDialog, GameObject.Find("Canvas").transform, false);
		BirdGiftDialog confirmDialog = dialog.GetComponent<BirdGiftDialog>();
		confirmDialog.SetCaption (rewardInfo);
		if(rewardIndex == 2)
			confirmDialog.SetTextReward ("x" + value, rewardIndex);
		else
			confirmDialog.SetTextReward ("" + value, rewardIndex);

		confirmDialog.SetConfirmCaption ("OK");
		confirmDialog.DisableNO ();
		confirmDialog.OnYesEvent += () => {Destroy(dialog);};
		Framework.SoundManager.Instance.PlaySFX("seReward");
	}

	//panel popup achivement  image and label 

	public void AssignLabelImage(uint gold,uint gem,uint pigy,uint accessory,uint pet,GameObject rewardPopUP)
	{
		
		string rewardGold = "";
		string rewardGem = "";
		string rewardAccessory = "";
		string rewardPiggy = "";
		string rewardPet = "";

		if (gem > 0) 
		{
			if (gem > 1) 
			{
				rewardGem +=  gem;
			} else
			{
				rewardGem += gem;
			}
		} 
		else 
		{
			rewardPopUP.transform.Find ("Box/Panel").Find("gem").gameObject.SetActive (false);
			//rewardPopUP.transform.Find ("Box/Panel").Find("lblGem").gameObject.SetActive (false);
		}

		if (gold > 0) 
		{
			if (gold > 1) 
			{
				rewardGold += gold;
			} else 
			{
				rewardGold += gold;
			}
				
		}
		else 
		{
			rewardPopUP.transform.Find ("Box/Panel").Find("gold").gameObject.SetActive (false);
			//rewardPopUP.transform.Find ("Box/Panel").Find("lblGold").gameObject.SetActive (false);

		}

		if (accessory > 0) 
		{
			//DataController.Instance.playerData.gold += gold;
			if (accessory > 1) 
			{
				rewardAccessory += accessory;
			} else 
			{
				rewardAccessory += accessory;
			}
		}
		else 
		{
			rewardPopUP.transform.Find ("Box/Panel").Find("accessories").gameObject.SetActive (false);
		}


		if (pet > 0) 
		{
			//DataController.Instance.playerData.gold += gold;
			if (pet > 1)
			{
				rewardPet += pet;
			} else
			{
				rewardPet += pet;
			}
		}
		else 
		{
			rewardPopUP.transform.Find ("Box/Panel").Find("petCat").gameObject.SetActive (false);
			rewardPopUP.transform.Find ("Box/Panel").Find("petDog").gameObject.SetActive (false);
		}

		if (pigy > 0)
		{

			if (pigy > 1) 
			{
				rewardPiggy += pigy;
			} else 
			{
				rewardPiggy += pigy;
			}
		}

		else 
		{
			rewardPopUP.transform.Find ("Box/Panel").Find("piggy").gameObject.SetActive (false);
			//rewardPopUP.transform.Find ("Box/Panel").Find("piggy").gameObject.SetActive (false);
		}

		//assign Reward for each label
		rewardPopUP.transform.Find ("Box/Panel/gold/lblGold").GetComponent<TextMeshProUGUI> ().text = rewardGold;
		rewardPopUP.transform.Find ("Box/Panel/gem/lblGem").GetComponent<TextMeshProUGUI> ().text = rewardGem;
		rewardPopUP.transform.Find ("Box/Panel/piggy/lblPiggy").GetComponent<TextMeshProUGUI> ().text = rewardPiggy;
					
	}

	//overload assign image
	public void AssignLabelImage(uint gold,uint gem,uint pigy,uint accessory,uint pet,GameObject rewardPopUP,string petlabel)
	{

		//Debug.LogError ("PetLable: " +petlabel);

		string rewardGold = "";
		string rewardGem = "";
		string rewardAccessory = "";
		string rewardPiggy = "";
		string rewardPet = "";


		if (gem > 0) 
		{
			if (gem > 1) 
			{
				rewardGem +=  gem;
			} else
			{
				rewardGem += gem;
			}
		} 
		else 
		{
			rewardPopUP.transform.Find ("Box/Panel").Find("gem").gameObject.SetActive (false);
			//rewardPopUP.transform.Find ("Box/Panel").Find("lblGem").gameObject.SetActive (false);
		}

		if (gold > 0) 
		{
			if (gold > 1) 
			{
				rewardGold += gold;
			} else 
			{
				rewardGold += gold;
			}

		}
		else 
		{
			rewardPopUP.transform.Find ("Box/Panel").Find("gold").gameObject.SetActive (false);
			//rewardPopUP.transform.Find ("Box/Panel").Find("lblGold").gameObject.SetActive (false);

		}

		if (accessory > 0) 
		{
			//DataController.Instance.playerData.gold += gold;
			if (accessory > 1) 
			{
				rewardAccessory += accessory;
			} else 
			{
				rewardAccessory += accessory;
			}
		}
		else 
		{
			rewardPopUP.transform.Find ("Box/Panel").Find("accessories").gameObject.SetActive (false);
		}

		//set cat and dog image
		if (petlabel == "dog") 
		{
			rewardPopUP.transform.Find ("Box/Panel").Find("petCat").gameObject.SetActive (false);
		} 
		else if (petlabel == "cat") 
		{
			rewardPopUP.transform.Find ("Box/Panel").Find("petDog").gameObject.SetActive (false);
		}
	

		if (pigy > 0)
		{

			if (pigy > 1) 
			{
				rewardPiggy += pigy;
			} else 
			{
				rewardPiggy += pigy;
			}
		}

		else 
		{
			rewardPopUP.transform.Find ("Box/Panel").Find("piggy").gameObject.SetActive (false);
			//rewardPopUP.transform.Find ("Box/Panel").Find("piggy").gameObject.SetActive (false);
		}
			


		//assign Reward for each label
		rewardPopUP.transform.Find ("Box/Panel/gold/lblGold").GetComponent<TextMeshProUGUI> ().text = rewardGold;
		rewardPopUP.transform.Find ("Box/Panel/gem/lblGem").GetComponent<TextMeshProUGUI> ().text = rewardGem;
		rewardPopUP.transform.Find ("Box/Panel/piggy/lblPiggy").GetComponent<TextMeshProUGUI> ().text = rewardPiggy;

	}

	//daily status

	void ResetDailyStatus()
	{
		List<AchievementItem> dailyListReset = DataController.Instance.achievementDatabase.items.Where  
			(x => x.achievementtype == EnumCollection.AchievemenType.Daily && x.achievementId != "achDailyThree").ToList ();

		for (int i = 0; i < dailyListReset.Count; i++) 
		{
			//Debug.Log("<color=green>" + dailyListReset[i].achName + "</color>");
			dailyListReset [i].isUnlocked = false;	
		}
	}

	public void HealthChecker()
	{
		List<int> checker = new List<int> ();
		float  happiness,cleanliness,hunger,energy,total;

		for (int i = 0; i < DataController.Instance.playerData.playerPetList.Count; i++) {

			happiness = DataController.Instance.playerData.playerPetList [i].happiness;
			cleanliness = DataController.Instance.playerData.playerPetList [i].cleanliness;
			hunger = DataController.Instance.playerData.playerPetList [i].hunger_thirst;
			energy = DataController.Instance.playerData.playerPetList [i].energy;
			total = happiness + cleanliness + hunger + energy;

			float petMeter = 100 - (total * 10);

			if (petMeter >=50) 
			{

				checker.Add (1);
			} 
			else 
			{
				checker.Add (0);
			}
		}

		if (checker.Contains (0)) 
		{
			HealthyPetCounterResetter ();
		}
		else
		{
			HealthyPetCounterIncrement ();
		}

	} 

	public void HealthyPetCounterResetter()
	{
		List<AchievementItem> weekListResseter = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achWeekly1").ToList ();

		weekListResseter [0].counter = 0;
	}

	public void HealthyPetCounterIncrement()
	{
		List<AchievementItem> weekListResseter = DataController.Instance.achievementDatabase.items.Where 
			(x => x.achievementId == "achWeekly1").ToList ();

		weekListResseter [0].counter += 1;

	}

	/// <summary>
	/// Rewards the last pet in fervid worker.
	/// </summary>
	public void RewardLastPetInFervidWorker()
	{
		List<int> fervidRewardList = new List<int> ();

		PetBase beagle = DataController.Instance.playerData.playerPetList.FirstOrDefault (b => b.petBreed.Contains ("Beagle"));
		PetBase golden = DataController.Instance.playerData.playerPetList.FirstOrDefault (b => b.petBreed.Contains ("Golden_Retriever"));
		PetBase persian = DataController.Instance.playerData.playerPetList.FirstOrDefault (b => b.petBreed.Contains ("Persian"));
		PetBase aby = DataController.Instance.playerData.playerPetList.FirstOrDefault (b => b.petBreed.Contains ("Abyssinian"));


		if (persian == null) {
			fervidRewardList.Add (1);
		} else {
			//Debug.LogError ("persian not  available");
		}

		if (aby == null) {
			fervidRewardList.Add (2);
		} else {
			//Debug.LogError ("aby not  available");
		}

		if (beagle == null) {
			fervidRewardList.Add (3);
		} else {
			//Debug.LogError ("beagle not  available");
		}

		if (golden == null) {
			fervidRewardList.Add (4);
		} else {
			//Debug.LogError ("golden not  available");
		}


		foreach (var item in fervidRewardList) {
			//Debug.LogError ("Items: " + item);
		}


		//randomize List
		int petIndex = fervidRewardList[UnityEngine.Random.Range(0,fervidRewardList.Count-1)];
		//Debug.LogError ("Random pet index is: " + petIndex);

		//reward pet
		try {
			FreePetInFervidWorker (petIndex);
		} catch (ArgumentException ex) {
			//Debug.LogError (ex);
		}



	}

	void FreePetInFervidWorker(int petIndex)
	{
		PetBase petBase = new PetBase();

		CatPet persian = DataController.Instance.catDatabase.items.FirstOrDefault(x=>x.breedname == EnumCollection.CatBreed.Persian);
		CatPet abyssinian = DataController.Instance.catDatabase.items.FirstOrDefault(x=>x.breedname == EnumCollection.CatBreed.Abyssinian);
		DogPet beagle = DataController.Instance.dogDatabase.items.FirstOrDefault(x=>x.breedname == EnumCollection.DogBreed.Beagle);
		DogPet golden = DataController.Instance.dogDatabase.items.FirstOrDefault(x=>x.breedname == EnumCollection.DogBreed.Golden_Retriever);


		if(petIndex ==  1)
		{
			petBase.petName  = "Persian";
			petBase.petBreed = "Persian";
			petBase.petType  = EnumCollection.Pet.Cat;
			petBase.petCategory = EnumCollection.PetCategory.Hairy;
			DataController.Instance.playerData.playerPetList.Add(petBase);

			persian.isOwned = true;
		}
			
		else if(petIndex ==  2)
		{
			petBase.petName  = "Abyssinian";
			petBase.petBreed = "Abyssinian";
			petBase.petType  = EnumCollection.Pet.Cat;
			petBase.petCategory = EnumCollection.PetCategory.ShortHair;
			DataController.Instance.playerData.playerPetList.Add(petBase);

			abyssinian.isOwned = true;
		}

		else if(petIndex ==  3)
		{
			petBase.petName  = "Beagle";
			petBase.petBreed = "Beagle";
			petBase.petType  = EnumCollection.Pet.Dog;
			petBase.petCategory = EnumCollection.PetCategory.Small;
			DataController.Instance.playerData.playerPetList.Add(petBase);

			beagle.isOwned = true;

		}

		else if(petIndex ==  4)
		{
			petBase.petName  = "Golden Retriever";
			petBase.petBreed = "Golden_Retriever";
			petBase.petType  = EnumCollection.Pet.Dog;
			petBase.petCategory = EnumCollection.PetCategory.Big;
			DataController.Instance.playerData.playerPetList.Add(petBase);

			golden.isOwned = true;
		}    

        // create pets instance
        if(SceneManager.GetActiveScene().name == "Main")
        {
            FindObjectOfType<PetHomeController>().SetPet();
        }
	}
}




