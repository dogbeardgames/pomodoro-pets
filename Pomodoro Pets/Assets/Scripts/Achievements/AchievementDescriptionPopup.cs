﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Linq;

public class AchievementDescriptionPopup : MonoBehaviour {

	[SerializeField]
	AchievementDatabase achievementdatabase;
	[SerializeField]
	ItemAchievementPopUp popUp;
	ItemAchievement item;
	int maxLenPerLine = 25;

	[SerializeField]
	bool showed;
	bool hasInitialized = false;

	public ItemAchievementPopUp PopUp{
		get{ return popUp;}
	}

	void Start()
	{
		item = GetComponent<ItemAchievement> ();
	}
	public void DisplayAchievementDetails (Button button) {
		HammerManager.ins.Hide ();
		Framework.SoundManager.Instance.PlaySFX ("seIconTapPomodoro");
		transform.SetAsLastSibling ();
		AchievementSettingsUIManager.ins.HidePopUps();
		popUp.gameObject.SetActive (true);

		if (!showed) {
			string achname = button.name;

			List<AchievementItem> achdescList = achievementdatabase.items.Where
				(x => x.achievementId == achname).ToList ();

			string acheveName = achdescList [0].achName;
			string requirements = achdescList [0].requirements;

			string rewardList = "";// "Gold " + achdescList [0].goldReward + "," + "Gem " + achdescList [0].gemReward + "," + "Piggy bank " + achdescList [0].pigyReward;
			if(achdescList[0].goldReward != 0)
			{
				//(s) removed by job as per bill
				rewardList = achdescList[0].goldReward.ToString() + " Gold\n";
				popUp.StrecthVertical (10f);
			}
			if(achdescList[0].gemReward != 0)
			{
				//(s) removed by job as per bill
				rewardList += achdescList[0].gemReward.ToString() + " Gem\n";
				popUp.StrecthVertical (10f);
			}

			if(achdescList[0].pigyReward != 0)
			{
				//(s) removed by job as per bill
				rewardList += achdescList[0].pigyReward.ToString() + " Piggy bank point\n";
				popUp.StrecthVertical (10f);
			}
			if(achdescList[0].accessory != 0)
			{
				rewardList += "1 Accessory\n";
				popUp.StrecthVertical (10f);
			}
			if(achdescList[0].pet != 0)
			{
				rewardList += "1 Pet\n";
				popUp.StrecthVertical (10f);
			}
			popUp.SetPopUp (acheveName, null);

		
			if (!hasInitialized) {
				popUp.txtContent.text = "";
				popUp.StrecthVertical (10f);
			
				Fit2(popUp.txtContent, requirements);
				popUp.txtContent.text += "\n";
				popUp.StrecthVertical (10f);
			
				popUp.txtContent.text += "Reward:\n";
				popUp.StrecthVertical (10f);
			
				//Fit (popUp.txtContent, rewardList);
				popUp.txtContent.text += rewardList;
				popUp.StrecthVertical (10f);

			}

			
			hasInitialized = true;


		} 
		popUp.Animate();
		showed = true;
	}


	void Fit(TextMeshProUGUI text, string newString)
	{
		int cntr = 0;
		for(int i=0; i<newString.Length; i++)
		{
			if (cntr < maxLenPerLine) {
				text.text += newString [i];
			} else {
				//check if character is not separated
				if (!newString [i - 1].Equals ("") && !newString [i].Equals ("")) {
					int _cntr = 0;
					for (int strIndex = text.text.Length - 1; strIndex > 0; strIndex--) {
						_cntr++;
						if (text.text.Substring (strIndex).Equals ("")) {
							text.text.Replace (text.text.Substring (0, text.text.Length), 
								text.text.Substring (0, text.text.Length - _cntr) + "\n" + text.text.Substring (text.text.Length - _cntr, text.text.Length));
							popUp.StrecthVertical (10f);
							break;
						}
					}
				} else {
					text.text += "\n" + newString[i];
					//text.text = newString.Insert(i, "\n");
					popUp.StrecthVertical (10f);
					//transform.localPosition = new Vector2 (transform.localPosition.x, transform.localPosition.y - 10f);
				}
				cntr = 0;
			}
			cntr++;
		}

	//	text.text = newString;

	}


	void Fit2(TextMeshProUGUI text, string newString)
	{
		int cntr = 0;
		for(int i=0; i<newString.Length; i++)
		{
			if (cntr < maxLenPerLine) {
				text.text += newString [i];
			} 
			else
			{
				//check if character is not separated
				if (!char.IsWhiteSpace(newString [i - 1]) && !char.IsWhiteSpace(newString [i])) 
				{
					int _charCntr = 0;
					for (int strIndex = text.text.Length - 1; strIndex > 0; strIndex--) 
					{
						_charCntr++;
						if (char.IsWhiteSpace(text.text.ToString()[strIndex])) 
						{
							//cut the word without empty space on index 0
							string _cutString = text.text.Substring(text.text.Length - (_charCntr-1)) + newString[i];
							//concatenate with strings with new line
							text.text = text.text.Substring (0, text.text.Length - _charCntr)+ "\n" + _cutString;
							popUp.StrecthVertical (10f);

							//set cntr value to characters counted for wording
							cntr = _charCntr - 1;
							break;
						}
					}
				} 
				else 
				{
					text.text += "\n" + newString[i];
					cntr = 0;
					popUp.StrecthVertical (10f);
				}
				//end
			}
			cntr++;
		}
	}

	

}
