﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Spine.Unity;


public class PiggyBankButton : MonoBehaviour {

	[SerializeField]
	TextMeshProUGUI piggyText;

	uint value=0;
	// Use this for initialization
	string animationPlaying = "";
	bool broken;
	SkeletonAnimation piggyAnim;

	public static PiggyBankButton instance;

		
	void Start () {
		instance = this;
		InvokeRepeating ("UpdateState",1f,1f);
		piggyAnim = AchievementSettingsUIManager.ins.PiggyBank.GetComponent<SkeletonAnimation> ();
	}


	void LateUpdate()
	{
		value = DataController.Instance.playerData.pigyBank;
		piggyText.gameObject.GetComponent<TextMeshProUGUI> ().text = value.ToString ();
	}
	public void Click()
	{ 
		if (!HammerManager.ins.IsShown) {
			HammerManager.ins.Show ();
			//HammerManager.ins.gameObject.SetActive(true);
		} else {
			//HammerManager.ins.gameObject.SetActive (false);
			HammerManager.ins.Hide ();
		}
	}
		
	public void Break()
	{
		piggyAnim.AnimationName = "break";
		broken = true;
		animationPlaying = "break";
		piggyAnim.loop = false;

	}

	public void RebuildPig()
	{
		piggyAnim.AnimationName = "empty";
		broken = false;
		animationPlaying = "empty";
		piggyAnim.loop = false;
	}

	public void ShowPiggy(bool b)
	{
		GameObject pig = GameObject.Find ("Spine GameObject (piggybank)(Clone)");
		pig.GetComponent<MeshRenderer> ().enabled = b;
		transform.GetChild (0).gameObject.SetActive (b);
	}

	void UpdateState()
	{
		if (!broken) {
			if (piggyAnim != null) {
				if (value < 50 && animationPlaying != "empty") {
					piggyAnim.AnimationName = "empty";
					animationPlaying = "empty";
					piggyAnim.loop = true;
				} else if (value >= 50 && value < 100 && animationPlaying != "half") {
					piggyAnim.AnimationName = "half";
					animationPlaying = "half";
					piggyAnim.loop = false;
				} else if (value >= 100 && animationPlaying != "full") {
					piggyAnim.AnimationName = "full";
					animationPlaying = "full";
					piggyAnim.loop = false;
				}

			} else {

			}
		}

	}
}
