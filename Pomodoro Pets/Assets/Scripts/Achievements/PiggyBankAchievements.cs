﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PiggyBankAchievements : MonoBehaviour {
	public static PiggyBankAchievements ins;

	[SerializeField]
	PetShopItems petitemdb;

	// Use this for initialization
	void Start () 
	{
		ins = this;
	}

//	void Hammered(string hammer)
//	{
//		switch (hammer) {
//
//		case "Normal":
//			NormalHammer ();
//			break;
//
//		case "Silver":
//			SilverHammer ();
//			break;
//
//		case "Gold":
//			GoldHammer ();
//			break;
//
//		default:
//			break;
//		}
//
//	}
		
	IEnumerator IEShowReward(uint gold, uint energyTreats, uint powerTreats, uint decPiggy)
	{
		
		Framework.SoundManager.Instance.PlaySFX ("sePiggyBankSmash");
		yield return new WaitForSeconds (1f);

		AchievementConditions.instance.PiggyRewardList (gold, energyTreats, powerTreats);
		DataController.Instance.playerData.goldacquired += gold;

		SetRewardResult(decPiggy, gold);
		HammerManager.ins.Hide();

	}

	void SetRewardResult(uint decDataPiggyBank, uint incDataGold)
	{

		DataController.Instance.playerData.pigyBank -= decDataPiggyBank;
		DataController.Instance.playerData.gold += incDataGold;
	}



	public void NormalHammer()
	{
		
		if (DataController.Instance.playerData.pigyBank >= 50 && DataController.Instance.playerData.pigyBank < 100) 
		{
			StartCoroutine (IEShowReward(300,0,0, 50));
			PiggyBankButton.instance.Break ();
		}

		if(DataController.Instance.playerData.pigyBank >= 100)
		{
			for (int i = 0; i < petitemdb.item.Length; i++) 
			{
				if (petitemdb.item [i].itemName == "Energy Treat") 
				{
					petitemdb.item [i].stock += 2;
				}
			}
			StartCoroutine (IEShowReward(600,2,0, 100));

			PiggyBankButton.instance.Break ();
		
			//Debug.Log("<color=red>" + "100 Normal Hammer" + "</color>");
		}

		if(DataController.Instance.playerData.pigyBank <  50)
		{
			GameObject window = Instantiate (Resources.Load<GameObject>("Prefab/StoreDialog"), GameObject.Find("Canvas").transform, false);
			ShowDialog dialog = window.GetComponent<ShowDialog> ();
			dialog.DisableNO ();
			dialog.SetCaption ("You should have at least 50 \npiggy points.");
			dialog.OnYesEvent += () => {
				Destroy(window);
			};

		}

		//Debug.Log("<color=red>" + "Normal Hammer" + "</color>");
	}

	public void SilverHammer()
	{
		if (DataController.Instance.playerData.pigyBank >= 50 && DataController.Instance.playerData.pigyBank < 100) {


			for (int i = 0; i < petitemdb.item.Length; i++) 
			{
				if (petitemdb.item [i].itemName == "Energy Treat")
				{
					petitemdb.item [i].stock += 2;
				}
			}


			for (int i = 0; i < petitemdb.item.Length; i++) 
			{
				if (petitemdb.item [i].itemName == "Power Treat") 
				{
					petitemdb.item [i].stock += 2;
				}
			}

		
			StartCoroutine (IEShowReward(600,2,0, 50));

			PiggyBankButton.instance.Break ();
		
			//Debug.Log("<color=red>" + "50 Silver Hammer" + "</color>");
		}

		else if(DataController.Instance.playerData.pigyBank >= 100)
		{	

			for (int i = 0; i < petitemdb.item.Length; i++) 
			{
				if (petitemdb.item [i].itemName == "Energy Treat") 
				{
					petitemdb.item [i].stock += 5;
				}
			}

			for (int i = 0; i < petitemdb.item.Length; i++) 
			{
				if (petitemdb.item [i].itemName == "Power Treat") 
				{
					petitemdb.item [i].stock += 5;
				}
			}
		
			StartCoroutine (IEShowReward(1200,5,5, 100));

			PiggyBankButton.instance.Break ();
		

			//Debug.Log("<color=red>" + "100 Silver Hammer" + "</color>");
		}

		else if(DataController.Instance.playerData.pigyBank <  50)
		{
			GameObject window = Instantiate (Resources.Load<GameObject>("Prefab/StoreDialog"), GameObject.Find("Canvas").transform, false);
			ShowDialog dialog = window.GetComponent<ShowDialog> ();
			dialog.DisableNO ();
			dialog.SetCaption ("You should have at least 50 \npiggy points.");
			dialog.OnYesEvent += () => {
				Destroy(window);
			};
		
		}

		//Debug.Log("<color=red>" + "Silver Hammer" + "</color>");
	}

	public void GoldHammer()
	{


		if (DataController.Instance.playerData.pigyBank >= 50 && DataController.Instance.playerData.pigyBank < 100) 
		{

			for (int i = 0; i < petitemdb.item.Length; i++) 
			{
				if (petitemdb.item [i].itemName == "Energy Treat") 
				{
					petitemdb.item [i].stock += 5;
				}
			}


			for (int i = 0; i < petitemdb.item.Length; i++) 
			{
				if (petitemdb.item [i].itemName == "Power Treat") 
				{
					petitemdb.item [i].stock += 5;
				}
			}


			StartCoroutine (IEShowReward(1500,5,5, 50));
			PiggyBankButton.instance.Break ();
		
			//Debug.Log("<color=red>" + "50 GoldHammer Hammer" + "</color>");
		}

		else if(DataController.Instance.playerData.pigyBank >= 100)
		{

			for (int i = 0; i < petitemdb.item.Length; i++) 
			{
				if (petitemdb.item [i].itemName == "Energy Treat") 
				{
					petitemdb.item [i].stock += 10;
				}
			}

			for (int i = 0; i < petitemdb.item.Length; i++)
			{
				if (petitemdb.item [i].itemName == "Power Treat") 
				{
					petitemdb.item [i].stock += 10;
				}
			}
			//AchievementConditions.instance.PiggyRewardList (3000,10,10);
			StartCoroutine (IEShowReward(3000,10,10, 100));
			PiggyBankButton.instance.Break ();
			//DataController.Instance.playerData.gold +=	3000;
			//DataController.Instance.playerData.pigyBank = 0;
			//Debug.Log("<color=red>" + "100 Gold Hammer" + "</color>");
		}

		else if(DataController.Instance.playerData.pigyBank <  50)
		{
			GameObject window = Instantiate (Resources.Load<GameObject>("Prefab/StoreDialog"), GameObject.Find("Canvas").transform, false);
			ShowDialog dialog = window.GetComponent<ShowDialog> ();
			dialog.DisableNO ();
			dialog.SetCaption ("You should have at least 50 \npiggy points.");
			dialog.OnYesEvent += () => {
				Destroy(window);
			};
		
		}

		//Debug.Log("<color=red>" + "Gold Hammer" + "</color>");
	}
}
