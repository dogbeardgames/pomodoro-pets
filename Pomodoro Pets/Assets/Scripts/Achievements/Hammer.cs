﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Spine.Unity;
using System.Linq;

public class Hammer : MonoBehaviour {

	[SerializeField]
	PiggyBankButton piggy;

	[SerializeField]
	string itemName;

	public string ItemName{
		get{ return itemName;}
	}

	public bool owned;

	void Start () {
		
//		if(hammer.Items.First(_hammer => _hammer.ItemName == this.itemName) != null)
//		{
//			if(hammer.Items.First(_hammer => _hammer.ItemName == this.itemName).isOwned)
//			{
//				owned = true;
//			}
//		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Click()
	{
		if (owned) {
			BreakPiggy ();
		} else {
			CallStore ();
		}
	}
	void CallStore()
	{
		GameObject store = (GameObject)Instantiate(Resources.Load<GameObject>("Prefab/StorePanel"), GameObject.Find("Canvas").transform, false);
		//store.transform.Find("btnBack").GetComponent<Button>().onClick.AddListener(() => store.SetActive(false));
		store.GetComponent<Store>().OpenStore("toggleItems");
		store.transform.Find ("LAYOUT_ROOT/MAIN_CONTENT/ItemsPanel/MAIN_CONTENT/Scroll View").GetComponent<ScrollRect>().verticalNormalizedPosition = 0f;
		store.GetComponent<Store> ().OnClose += delegate {
			PiggyBankButton.instance.ShowPiggy(true);
		};
		PiggyBankButton.instance.ShowPiggy(false);
		HammerManager.ins.Hide();
	}

	void BreakPiggy()
	{
		GameObject useHammerObject = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), GameObject.Find("Canvas").transform, false);
		ShowDialog confirmDialog = useHammerObject.GetComponent<ShowDialog>();
		// set captions
		confirmDialog.SetCaption("Are you sure you want use this " + itemName + "?");
		confirmDialog.SetConfirmCaption("Yes");
		confirmDialog.SetCancelCaption("No");
		// set events
		confirmDialog.OnYesEvent += () => 
		{              
			SelectHammer();
			Destroy(useHammerObject);
		};
		confirmDialog.OnNoEvent += () => 
		{
			Destroy(useHammerObject);
		};
	}

	void SelectHammer()
	{
		if (itemName == "Silver Hammer") {
			PiggyBankAchievements.ins.SilverHammer ();	
		} else if (itemName == "Gold Hammer") {
			PiggyBankAchievements.ins.GoldHammer ();
		} else {
			PiggyBankAchievements.ins.NormalHammer ();
		}
	}

}
