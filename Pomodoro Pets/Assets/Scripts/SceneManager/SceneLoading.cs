﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoading : MonoBehaviour {

    [SerializeField]
    Image loadingImage;

	// Use this for initialization
	void Start () {
        StartCoroutine("_LoadScene");
	}		

//    public void LoadScene(string _sceneName)
//    {
//        
//    }

    IEnumerator _LoadScene()
    {
        yield return new WaitForSeconds(1);
        AsyncOperation asyncOp = SceneManager.LoadSceneAsync(SceneController.Instance.sceneToLoad, LoadSceneMode.Single);
        //loadingImage.fillAmount = asyncOp.progress;      
        //Debug.Log("Scene load done " + SceneController.Instance.sceneToLoad);

        while(true)
        {
           // Debug.Log(asyncOp.progress);
            loadingImage.fillAmount = asyncOp.progress + 0.10f;

            yield return asyncOp.isDone;
        }            
    }
}
