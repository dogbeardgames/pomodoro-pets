﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour {
  
    public static SceneController Instance; 


    public string sceneToLoad;

    void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

	// Use this for initialization
//	void Start () 
//    {
//        
//	}		

    public void SceneToLoad(string sceneName)
    {
        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }

    public void LoadingScene(string sceneToLoad)
    {
        this.sceneToLoad = sceneToLoad;
        //Debug.Log("scene to load " + sceneToLoad);
        SceneManager.LoadScene("Loading", LoadSceneMode.Single);
    }
}
