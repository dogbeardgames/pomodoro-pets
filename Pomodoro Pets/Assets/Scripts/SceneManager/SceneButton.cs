﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneButton : MonoBehaviour {

    [SerializeField]
    string sceneToLoad;

    Button button;

	// Use this for initialization
	void Start () {
		
        button = GetComponent<Button>();
        button.onClick.AddListener(() => SceneController.Instance.LoadingScene(this.sceneToLoad));
		button.onClick.AddListener(() => DataController.Instance.playerData.ispetHome = true);
		button.onClick.AddListener(() => DataController.Instance.playerData.isfromIndoor = true);

		button.onClick.AddListener(() =>  CheckTimeMaster.instance.CheckDate ());

	}	
		

}
