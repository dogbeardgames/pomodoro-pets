﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class LineNumberManager : MonoBehaviour 
{
	public static LineNumberManager instance;
	public TextMeshProUGUI[] lineNumbers;

	void Start()
	{
		instance = this;
	}

	/// <summary>
	/// graph line generator.
	/// </summary>
	/// <param name="number">Number.</param>
	public void GraphLineGenerator (float number ) 
	{
		float highestNumber = 0;

		if (number.ToString ().Length == 1 || number < 11) {
			highestNumber = 10;
		} 
		else 
		{
			float lastNumber = number % 10;

			if (lastNumber > 0) {
				highestNumber = (number + 10) - lastNumber;
			} 
			else 
			{
				highestNumber = number;
			}
		}
			
		//generate number
		Transform[] trans = GameObject.Find ("numberChart").transform.GetComponentsInChildren <Transform>(true);

		float line = highestNumber; 


		float divider = line / 10f;

		for(int i=0; i<lineNumbers.Length; i++)
		{
			lineNumbers[i].text = line.ToString ();
			line -= divider;
		}
		//
		//		foreach (var item in trans) 
		//		{
		//			Debug.Log ("<color=blue>" + item.name + "</color>");
		//
		//			//dash
		//			if (item.name != "numberChart") {	
		//				if (item.name != "line") {	
		//					item.GetComponent<TextMeshProUGUI> ().text = line.ToString ();
		//
		//					line -= divider;
		//
		//				}
		//			}
		//		}


	}
	/// <summary>
	/// Categories the graph line generator.
	/// </summary>
	/// <param name="number">Number.</param>
	public void CategoryGraphLineGenerator (float number ) 
	{
		float highestNumber = 0;

		if (number.ToString ().Length == 1 || number < 11) {
			highestNumber = 10;
		} 
		else 
		{
			float lastNumber = number % 10;

			if (lastNumber > 0) {
				highestNumber = (number + 10) - lastNumber;
			} 
			else 
			{
				highestNumber = number;
			}
		}
			
		float line = highestNumber; 

		float divider = line / 10f;

		for(int i=0; i<lineNumbers.Length; i++)
		{
			lineNumbers [i].text = line.ToString ();
			line -= divider;
		}

	}

	/// <summary>
	/// Generates the line number.
	/// </summary>
	/// <returns>The line number.</returns>
	/// <param name="lnumber">Lnumber.</param>
	public float GenerateLineNumber(float lnumber)
	{
		lnumber = lnumber + 0.5f;
		lnumber = Mathf.RoundToInt (lnumber);

		//Debug.Log("<color=red>" + "Generated Number: " +  lnumber + "</color>");
		return lnumber * 10;
	}

	float GenerateLineNumberWhole(float mnumber)
	{
		mnumber = mnumber + 0.5f;
		mnumber = Mathf.RoundToInt (mnumber);

		//Debug.Log("<color=red>" + "Generated Number: " +  mnumber + "</color>");
		return mnumber;
	}


	float generateFlexibleNumber(float number)
	{
		float highestNumber = 0;

		if (number.ToString ().Length == 1 || number < 11) {
			highestNumber = 10;
		} 
		else 
		{
			float lastNumber = number % 10;

			if (lastNumber > 0) {
				highestNumber = (number + 10) - lastNumber;
			} 
			else 
			{
				highestNumber = number;
			}
		}
		return highestNumber;

		Debug.Log ("Highest Number is: " + highestNumber);
	}

}
