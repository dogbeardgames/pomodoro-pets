﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodStatReward : MonoBehaviour {

    Dictionary<EnumCollection.FoodItemLevel, float> foodStatBoost;
    Dictionary<EnumCollection.FoodItemLevel, float> foodGoldReward;

	// Use this for initialization
	void Start () {
        // food stat boost
        foodStatBoost = new Dictionary<EnumCollection.FoodItemLevel, float>();
        foodStatBoost.Add(EnumCollection.FoodItemLevel.L1, 0.4f);
        foodStatBoost.Add(EnumCollection.FoodItemLevel.L2, 0.4f);
        foodStatBoost.Add(EnumCollection.FoodItemLevel.L3, 0.5f);
        foodStatBoost.Add(EnumCollection.FoodItemLevel.L4, 0.6f);
        foodStatBoost.Add(EnumCollection.FoodItemLevel.L5, 0.7f);
        foodStatBoost.Add(EnumCollection.FoodItemLevel.L6, 0.8f);
        foodStatBoost.Add(EnumCollection.FoodItemLevel.L7, 0.9f);
        foodStatBoost.Add(EnumCollection.FoodItemLevel.EnergyTreat, 0.7f);
        foodStatBoost.Add(EnumCollection.FoodItemLevel.PowerTreat, 1f);

        // gold reward per food
        foodGoldReward = new Dictionary<EnumCollection.FoodItemLevel, float>();
        foodGoldReward.Add(EnumCollection.FoodItemLevel.L1, 4);
        foodGoldReward.Add(EnumCollection.FoodItemLevel.L2, 7);
        foodGoldReward.Add(EnumCollection.FoodItemLevel.L3, 21);
        foodGoldReward.Add(EnumCollection.FoodItemLevel.L4, 29);
        foodGoldReward.Add(EnumCollection.FoodItemLevel.L5, 32);
        foodGoldReward.Add(EnumCollection.FoodItemLevel.L6, 64);
        foodGoldReward.Add(EnumCollection.FoodItemLevel.L7, 80);
        foodGoldReward.Add(EnumCollection.FoodItemLevel.EnergyTreat, 10);
        foodGoldReward.Add(EnumCollection.FoodItemLevel.PowerTreat, 25);
	}
	
	// Update is called once per frame
//	void Update () {
//		
//	}

    #region METHODS
    public float StatBoost(EnumCollection.FoodItemLevel foodItemLevel)
    {
        if (foodStatBoost.ContainsKey(foodItemLevel))
            return foodStatBoost[foodItemLevel];
        else
            return 0;
    }

    public float GoldReward(EnumCollection.FoodItemLevel foodItemLevel)
    {
        if (foodGoldReward.ContainsKey(foodItemLevel))
            return foodGoldReward[foodItemLevel];
        else
            return 0;
    }
    #endregion
}
