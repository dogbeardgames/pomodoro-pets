﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using UnityEngine.UI;
using System.Globalization;
using System.IO;
using TMPro;
using UnityEngine.EventSystems;

public class StatController : MonoBehaviour {

	public Dictionary<string,List<string>> enumDictionaryCategory = new Dictionary<string,List<string>> ();
	List <int> filteredCategory = new List<int> (); 

	[SerializeField]
	BarChart barChart;

	public List<ListOfReports> listOfFilteredByDate = new List<ListOfReports> ();

	public List<filteredByDate> listfiltertest = new List<filteredByDate> ();// for testing

	public List<ListOfReports> listForReports = new List<ListOfReports>();

	public Dictionary<DayOfWeek, List<ListOfReports>> daysDictionary = new Dictionary<DayOfWeek, List<ListOfReports>> ();

	public Dictionary<string, List<ListOfReports>> monthDictionary = new Dictionary<string, List<ListOfReports>> ();

	public List<string> weekList = new List<string>();


	[SerializeField]
	CategoryDatabase categoryDb;

    private string selectedFilter;

	float breaktimeduration;
	float taskcompleted;

	float break_; 
	float task;

	[SerializeField]
	TextMeshProUGUI txtCompleted, txtBreak, txtTotal,txtCompletedDummy,txtBreakDummy;

	[SerializeField]
	TMP_Dropdown dpTimeSpan;

	[SerializeField]
	ToggleGroup togglegroup;

	[SerializeField]
    TextMeshProUGUI effeciency, bycategory;

	public static StatController instance;


	void Start()
	{			
		instance = this;
		DataController.Instance.playerData.enableDrag = false;

		// add category options to drop down
		List<string> list = new List<string>();

		//Debug.Log("<color=red>" + "filtering..." + "</color>");
		var statDic = enumDictionaryCategory.Where (y => y.Key == "set_statistics").ToDictionary (y => y.Key, y => y.Value);

		//Debug.Log ("<color=red>" + "filtering..." + "</color>" + statDic.Count);


		foreach (List<string> setListStats in statDic.Values) {
			foreach (var dataEnum in setListStats) {
				list.Add((dataEnum).ToString());

				//Debug.Log ("<color=blue>" + dataEnum + "</color>");
			}
		}
		dpTimeSpan.AddOptions(list);
		list.Clear();

		for (int i = 0; i < listfiltertest.Count; i++) {
			//Debug.Log ("Category Index: " + listfiltertest [i].category_index);
		}

		dpTimeSpan.onValueChanged.AddListener (delegate {
			selectedValue(dpTimeSpan);
		});
			
		// default data
		DestroyGameObjectBarChart ();

		//Debug.Log ("All Time");
		// display values in label for 7 days
		CountThreeMonthsDuration();
		CountAllbreakTime ();
		GetTotalDuration ();

		bycategory.enabled = false;
		effeciency.enabled = true;

	}

	/// <summary>
	/// Dropdown selected value.
	/// </summary>
	/// <param name="dpTimeSpan">Dp time span.</param>
	void selectedValue(TMP_Dropdown dpTimeSpan )
	{

		IEnumerable<Toggle> activeToggles = togglegroup.ActiveToggles ();

		foreach (Toggle tg in activeToggles) {
			//Debug.Log ("Active toggle is" + tg.name);			
            selectedFilter = tg.name;
		}
			
		listForReports.Clear ();
		if (dpTimeSpan.value == 0 && selectedFilter =="toggleCategory") { // All time
			//Debug.Log ("First DropDown chosen");
			//Debug.Log ("Third DropDown chosen");
			DestroyGameObjectBarChart ();
			DisplayFilteredTesting (5000);
			GroupCategory ();
			barChart.DisplayGraphForCategory ();

			//Display values in Label
			CountThreeMonths();
			CountAllbreakTime ();
			GetTotalDuration ();

			bycategory.enabled = true;
			effeciency.enabled = false;

		}

		else if (dpTimeSpan.value == 2 && selectedFilter =="toggleCategory") { //7 days
			DestroyGameObjectBarChart ();
			DisplayFilteredTesting (7);
			GroupCategory ();
			barChart.DisplayGraphForCategory ();

			// display values in label for 7 days
			CountSevenDays();
			CountSevenDaysDuration();
			GetTotalDuration ();
			bycategory.enabled = true;
			effeciency.enabled = false;


		}

		else if (dpTimeSpan.value == 1  && selectedFilter =="toggleCategory") {// last 3 months

			DestroyGameObjectBarChart ();
			//Debug.Log ("Third DropDown chosen");

			//GenerateWeek ();// group task by date
			DisplayFilteredTesting (90);
			GroupCategory ();
			barChart.DisplayGraphForCategory ();

			// display values in label for 3 months
			CountAllbreakTime ();
			CountThreeMonthsDuration();
			GetTotalDuration();
			bycategory.enabled = true;
			effeciency.enabled = false;

		}

		//start Task


		else if(dpTimeSpan.value == 0  && selectedFilter =="toggleTask")//All Time for Task
		{
			DestroyGameObjectBarChart ();
			barChart.TaskAllTime ();
			//Debug.Log ("All Time");
			//Display values in Label
			CountThreeMonthsDuration();
			CountAllbreakTime ();
			GetTotalDuration ();

			bycategory.enabled = false;
			effeciency.enabled = true;

		}

	
		else if(dpTimeSpan.value == 1  && selectedFilter =="toggleTask")//Last 3 months for task
		{
			DestroyGameObjectBarChart ();
			barChart.lastThreemonth ();

			// display values in label for 3 months
			CountAllbreakTime ();
			CountThreeMonthsDuration();
			GetTotalDuration();

			bycategory.enabled = false;
			effeciency.enabled = true;
		}

		else if(dpTimeSpan.value == 2 &&   selectedFilter =="toggleTask")//Last 7 days for task
		{
			DestroyGameObjectBarChart ();
			barChart.Week ();

			// display values in label for 7 days
			CountSevenDays();
			CountSevenDaysDuration();
			GetTotalDuration ();
			bycategory.enabled = false;
			effeciency.enabled = true;
		}


	}

	/// <summary>
	/// selected toggle.
	/// </summary>
	public void toggleSelect()
	{
		listForReports.Clear ();
		if (dpTimeSpan.value == 0 && EventSystem.current.currentSelectedGameObject.name =="toggleCategory") { // All time
			//Debug.Log ("First DropDown chosen");
			//Debug.Log ("Third DropDown chosen");
			DestroyGameObjectBarChart ();
			DisplayFilteredTesting (5000);
			GroupCategory ();
			barChart.DisplayGraphForCategory ();

			///Display values in Label
			CountThreeMonths();
			CountAllbreakTime ();
			GetTotalDuration ();

			bycategory.enabled = true;
			effeciency.enabled = false;

		}

		else if (dpTimeSpan.value == 2 && EventSystem.current.currentSelectedGameObject.name =="toggleCategory") {// last 7 days
			DestroyGameObjectBarChart ();
			DisplayFilteredTesting (7);
			GroupCategory ();
			barChart.DisplayGraphForCategory ();

			// display values in label for 7 days
			CountSevenDays();
			CountSevenDaysDuration();
			GetTotalDuration ();
			bycategory.enabled = true;
			effeciency.enabled = false;
			
			
		}

		else if (dpTimeSpan.value == 1  && EventSystem.current.currentSelectedGameObject.name =="toggleCategory") {// last 3 months

			DestroyGameObjectBarChart ();
			//Debug.Log ("Third DropDown chosen");

			//GenerateWeek ();// group task by date
			DisplayFilteredTesting (90);
			GroupCategory ();
			barChart.DisplayGraphForCategory ();

			// display values in label for 3 months
			CountAllbreakTime ();
			CountThreeMonthsDuration();
			GetTotalDuration();
			bycategory.enabled = true;
			effeciency.enabled = false;
		
		}
			

		//start Task
	
		else if(dpTimeSpan.value == 0  && EventSystem.current.currentSelectedGameObject.name =="toggleTask")//All Time for Task
		{
			DestroyGameObjectBarChart ();
			barChart.TaskAllTime ();
			//Debug.Log ("All Time");
			// display values in label for 7 days
			//Display values in Label
			countAllCompltedTaskDuration ();
			CountThreeMonthsDuration();
			GetTotalDuration ();

			bycategory.enabled = false;
			effeciency.enabled = true;

		}
			
		else if(dpTimeSpan.value == 1  && EventSystem.current.currentSelectedGameObject.name =="toggleTask")//Last 3 months for task
		{
			DestroyGameObjectBarChart ();
			barChart.lastThreemonth ();

			// display values in label for 3 months
			CountAllbreakTime ();
			CountThreeMonthsDuration();
			GetTotalDuration();

			bycategory.enabled = false;
			effeciency.enabled = true;
		}


		else if(dpTimeSpan.value == 2  && EventSystem.current.currentSelectedGameObject.name =="toggleTask")//Last 7 days for task
		{
			DestroyGameObjectBarChart ();
			barChart.Week ();

			// display values in label for 7 days
			CountSevenDays();
			CountSevenDaysDuration();
			GetTotalDuration ();
			bycategory.enabled = false;
			effeciency.enabled = true;
		}


	}
		
	public Transform barchildToDelete;

	/// <summary>
	/// Destroys the game object bar chart.
	/// </summary>
	void DestroyGameObjectBarChart()//destroy charts when new chart is instantiated. Use this to avoid duplicate of charts.
	{

		for (int i = 0; i < barchildToDelete.childCount; i++) 
		{
			Destroy (barchildToDelete.GetChild(i).gameObject);
		}

	}

	/// <summary>
	/// Gets the task by date.
	/// </summary>
	/// <returns>The taskby date.</returns>
	/// <param name="days">Days.</param>
	/// <param name="allTaskByDate">All task by date.</param>
	public List<TaskItem> GetTaskbyDate(int days,List<TaskItem> allTaskByDate) // filter by day
	{
		return allTaskByDate.Where(x=> ( DateTime.Now-x.dateFinished).TotalDays <= days).ToList();
	}

	/// <summary>
	/// Displays the filtered task.
	/// </summary>
	/// <param name="days">Days.</param>
	public void DisplayFilteredTask(int days = 7)
	{		
		List<TaskItem> FilteredTask = GetTaskbyDate(days,TaskDataController.Instance.completedTaskData.taskItem);

		for(int x=0; x < FilteredTask.Count; x++)
		{
			ListOfReports filteredByDate = new ListOfReports();
			//Debug.Log ("Filtered task " + FilteredTask[x].categoryIndex + ", " + FilteredTask[x].task );
			//Add filtered data to new List
			filteredByDate.task = FilteredTask [x].task;
			filteredByDate.category = FilteredTask [x].categoryIndex.ToString();
			filteredByDate.dateStarted = FilteredTask [x].dateNow;
			filteredByDate.dateFinished = FilteredTask [x].dateFinished;
			filteredByDate.session = FilteredTask [x].session;

			listOfFilteredByDate.Add (filteredByDate);

		}	

		//var distinctCount = listOfFilteredByDate.Select(o=>o.session).Distinct();
			//countDistinctSessionInADay ();

	}

	/// <summary>
	/// Counts the distinct session in A day.
	/// </summary>
	void countDistinctSessionInADay()
	{
		var countofDistincSession = listOfFilteredByDate.Select(x=>x.session ).Distinct();

		foreach (var item in countofDistincSession) {

			//Debug.Log ("Count of session"  + item);
		}
	
	}
		
	/// <summary>
	/// Displays the filtered testing.
	/// </summary>
	/// <param name="days">Days.</param>
	public void DisplayFilteredTesting(int days) // by category filter for x days/months
	{		
		listfiltertest.Clear ();
		List<TaskItem> FilteredTask = GetTaskbyDate(days,TaskDataController.Instance.completedTaskData.taskItem);

		for(int x=0; x < FilteredTask.Count; x++)
		{
			filteredByDate filteredtest= new filteredByDate();
			//Debug.Log ("Filtered task " + FilteredTask[x].categoryIndex + ", " + FilteredTask[x].task );

			//Add filtered data to new List
			filteredtest.task = FilteredTask [x].task;
			filteredtest.category_index = FilteredTask [x].categoryIndex;
			filteredtest.dateStarted = FilteredTask [x].dateNow;
			filteredtest.dateFinished = FilteredTask [x].dateFinished;
			filteredtest.session = FilteredTask [x].session;
			listfiltertest.Add (filteredtest);

			//Debug.Log("<color=blue>" + "Filtered index: " + filteredtest.category_index + "</color>");
		}	
			
	}

	/// <summary>
	/// Groups the category.
	/// </summary>
	void GroupCategory()
	{
		listForReports.Clear ();
		IEnumerable<IGrouping<int,filteredByDate>> categoryLists = listfiltertest.GroupBy (b => b.category_index);

		foreach(IGrouping<int,filteredByDate> categorygroup in categoryLists)
		{
			//Debug.Log ("AllTimeByCategory");
			ListOfReports lcategory = new ListOfReports ();
			foreach (filteredByDate task in categorygroup) 
			{
				//Debug.Log ("<color=blue>" + "Category " + categorygroup.Key + "</color>");
				//print(task.task + " " );
				lcategory.dateFinished = task.dateFinished;
				lcategory.task = task.task;
				lcategory.dateStarted = task.dateStarted;
				lcategory.category = categoryDb.items [categorygroup.Key].category;
			}
				
			//Debug.Log ("Category " + categoryDb.items [listfiltertest [categorygroup.Key].category_index].category);
			lcategory.task_count = categorygroup.Count ();
			listForReports.Add (lcategory);
		}
			
	}

	/// <summary>
	/// Generates the weekly stats.
	/// </summary>
	void GenerateWeek()//generate week
	{		
		//Debug.Log (daysDictionary.Count +  " is the count of dictionary before adding");

		daysDictionary.Clear ();

		for (int i = 0; i < listOfFilteredByDate.Count; i++) {
			
			List<ListOfReports> listOfReports ;

			listOfReports = listOfFilteredByDate.Where (list => list.dateFinished.DayOfWeek == (DayOfWeek)i).ToList();
			daysDictionary.Add (((DayOfWeek)i), listOfReports);
		
		}
		// categorized by day, (dictionary)
		for (int i = 0; i < daysDictionary.Count; i++) {
			for (int j = 0; j < daysDictionary[((DayOfWeek)i)].Count; j++) {	
				//Debug.Log ("Day " + ((DayOfWeek)i) + " date finished " + daysDictionary [((DayOfWeek)i)][j].dateFinished);

			//Debug.Log ("Day " + ((DayOfWeek)i) + " date finished " + daysDictionary [((DayOfWeek)i)][j].dateFinished);
			}
		}
		//Debug.Log ("Pass " + daysDictionary.Count);

		//display graph

//		for (int i = 0; i < value; i++) {
//			weekList.Add(""+ DateTime.Now.Date.AddDays (-i).DayOfWeek);
//			//Debug.Log (""+ DateTime.Now.Date.AddDays (-i).DayOfWeek);
//		}
//
//		//testing
//		for (int i = 0; i < weekList.Count; i++) {
//			Debug.Log (weekList[i]);
//		}
	}

	/// <summary>
	/// Gets the break time.
	/// </summary>
	/// <returns>The break time.</returns>
	/// <param name="days">Days.</param>
	/// <param name="allBreak">All break.</param>
	public List<breakList> GetBreakTime(int days,List<breakList> allBreak) // filter by day
	{
		return allBreak.Where(x=> ( DateTime.Now-x.breakDate).TotalDays <= days).ToList();
	}

	/// <summary>
	/// Counts  allbreak time.
	/// </summary>
	public  void CountAllbreakTime()
	{
		float breakCount = 0;

		for (int i = 0; i < TaskDataController.Instance.completedTaskData.breakList.Count; i++) 
		{
			breakCount = breakCount + TaskDataController.Instance.completedTaskData.breakList [i].breakTime;
		}

		var timeSpan = TimeSpan.FromSeconds(breakCount);
		int hour = timeSpan.Hours;
		int minutes = timeSpan.Minutes;
		int seconds = timeSpan.Seconds;

		txtBreak.text = breakCount.ToString();

		txtBreakDummy.text = hour.ToString() + "h: " +  minutes.ToString() + "m";

	}

	/// <summary>
	/// Count three months.
	/// </summary>
	public  void CountThreeMonths()
	{


		//count number of days in last 3 months
		int daysinMonth=0;
		for (int i = 0; i < 3; i++) {

			daysinMonth= daysinMonth +  DateTime.DaysInMonth (DateTime.Now.AddMonths (-i).Year, DateTime.Now.AddMonths (-i).Month);
		}
	
		List<breakList> filteredBreakCount = GetBreakTime(daysinMonth,TaskDataController.Instance.completedTaskData.breakList);


		float breakCount = 0;

		for (int i = 0; i < filteredBreakCount.Count(); i++) 
		{
			breakCount = breakCount + filteredBreakCount[i].breakTime;

		}

		var timeSpan = TimeSpan.FromSeconds(breakCount);
		int hour = timeSpan.Hours;
		int minutes = timeSpan.Minutes;
		int seconds = timeSpan.Seconds;

		txtBreak.text = breakCount.ToString();

		txtBreakDummy.text = hour.ToString() + "h: " +  minutes.ToString() + "m ";

	}

	/// <summary>
	/// Counts the seven days.
	/// </summary>
	public  void CountSevenDays()//count breaktime in 7 days
	{

		List<breakList> filteredBreakCount = GetBreakTime(7,TaskDataController.Instance.completedTaskData.breakList);


		for (int i = 0; i < filteredBreakCount.Count(); i++) {
			//Debug.Log (filteredBreakCount [i].breakTime);
		}

		float breakCount = 0;

		for (int i = 0; i < filteredBreakCount.Count(); i++) 
		{
			breakCount = breakCount + filteredBreakCount[i].breakTime;

		}
			
		var timeSpan = TimeSpan.FromSeconds(breakCount);
		int hour = timeSpan.Hours;
		int minutes = timeSpan.Minutes;
		int seconds = timeSpan.Seconds;

		txtBreak.text = breakCount.ToString();

		txtBreakDummy.text = hour.ToString() + "h: " +  minutes.ToString() + "m " ;

	}

	/// <summary>
	/// Gets the completed duration.
	/// </summary>
	/// <returns>The break time.</returns>
	/// <param name="days">Days.</param>
	/// <param name="allDuration">All duration.</param>
	public List<completedDuration> GetBreakTime(int days,List<completedDuration> allDuration) // filter by day
	{
		return allDuration.Where(x=> ( DateTime.Now-x.completeDate).TotalDays <= days).ToList();
	}

	/// <summary>
	/// Gets the duration time.
	/// </summary>
	/// <returns>The duration time.</returns>
	/// <param name="days">Days.</param>
	/// <param name="completedDuration">Completed duration.</param>
	public List<TaskItem> GetDurationTime(int days,List<TaskItem> completedDuration) // filter by day
	{
		return completedDuration.Where(x=> ( DateTime.Now-x.dateFinished).TotalDays <= days).ToList();
	}
		
	/// <summary>
	/// Counts the duration of the all complted task.
	/// </summary>
	public  void countAllCompltedTaskDuration()
	{
		//Debug.Log ("countAllCompltedTaskDuration");
		int durationCount = 0;

		for (int i = 0; i < TaskDataController.Instance.completedTaskData.completedTaskList.Count; i++) 
		{
			durationCount = durationCount + (int)TaskDataController.Instance.completedTaskData.completedTaskList [i].cDuration;
		}
			
		var timeSpan = TimeSpan.FromSeconds(durationCount);
		int hour = timeSpan.Hours;
		int minutes = timeSpan.Minutes;
		int seconds = timeSpan.Seconds;

		txtCompleted.text = durationCount.ToString().Trim();

		txtCompletedDummy.text = hour.ToString() + "h: " +  minutes.ToString() + "m ";

	}

	/// <summary>
	/// Counts the duration of the three months.
	/// </summary>
	public  void CountThreeMonthsDuration()
	{

		//Debug.Log ("CountThreeMonthsDuration");
		//count number of days in last 3 months
		int daysinMonth=0;
		for (int i = 0; i < 3; i++) {

			daysinMonth= daysinMonth +  DateTime.DaysInMonth (DateTime.Now.AddMonths (-i).Year, DateTime.Now.AddMonths (-i).Month);
		}

		List<TaskItem> filteredDuration = GetDurationTime(daysinMonth,TaskDataController.Instance.completedTaskData.taskItem);


		int durationCount = 0;

		for (int i = 0; i < filteredDuration.Count(); i++) 
		{
			durationCount = durationCount + (int)filteredDuration[i].finishDuration;

		}
			
		var timeSpan = TimeSpan.FromSeconds(durationCount);
		int hour = timeSpan.Hours;
		int minutes = timeSpan.Minutes;
		int seconds = timeSpan.Seconds;

		txtCompleted.text = durationCount.ToString().Trim();

		txtCompletedDummy.text = hour.ToString() + "h: " +  minutes.ToString() + "m";

	}

	/// <summary>
	/// Counts the duration of the seven days.
	/// </summary>
	public  void CountSevenDaysDuration()//count breaktime in 7 days
	{
		//Debug.Log ("CountSevenDaysDuration");

		List<TaskItem> filteredDuration = GetDurationTime(7,TaskDataController.Instance.completedTaskData.taskItem);


		for (int i = 0; i < filteredDuration.Count; i++) {
			//Debug.Log (filteredDuration [i].finishDuration);
		}

		int durationCount = 0;

		for (int i = 0; i < filteredDuration.Count; i++) 
		{
			//Debug.Log (filteredDuration.Count + "duration Count");

			durationCount = durationCount + (int)filteredDuration[i].finishDuration;

			//Debug.Log (durationCount + "duration Count");

		}

		var timeSpan = TimeSpan.FromSeconds(durationCount);
		int hour = timeSpan.Hours;
		int minutes = timeSpan.Minutes;
		int seconds = timeSpan.Seconds;

		txtCompleted.text = durationCount.ToString().Trim();

		txtCompletedDummy.text = hour.ToString() + "h: " +  minutes.ToString() + "m ";
	}

	/// <summary>
	/// Gets the total duration.
	/// </summary>
	public void GetTotalDuration()
	{

		//Debug.Log ("Get total duration");
		float breakT    = float.Parse(txtBreak.text);
		float duration = float.Parse(txtCompleted.text);

		int sum = (int)breakT + (int)duration;

		var timeSpan = TimeSpan.FromSeconds(sum);
		int hour = timeSpan.Hours;
		int minutes = timeSpan.Minutes;
		int seconds = timeSpan.Seconds;

		txtTotal.text = hour.ToString() + "h: " +  minutes.ToString() + "m ";
	}

	/// <summary>
	/// Loads the enum json.
	/// </summary>
	/// <param name="fileName">File name.</param>
	public void LoadEnumJson(string fileName)
	{


		string filePath = "";

		if (Application.platform == RuntimePlatform.Android)
		{
			// Android
			filePath = System.IO.Path.Combine(Application.streamingAssetsPath, fileName);

			// Android only use WWW to read file
			WWW reader = new WWW(filePath);
			while ( ! reader.isDone) {}

			string realPath = Application.persistentDataPath + fileName;
			System.IO.File.WriteAllBytes(realPath, reader.bytes);

			filePath = realPath;
		}
		else
		{
			// iOS
			filePath = System.IO.Path.Combine(Application.streamingAssetsPath, fileName);
		}

		if (File.Exists (filePath)) {
			//Debug.Log ("Test");
			string dataAsJson = File.ReadAllText (filePath);
			LocalizationDataList enumData = JsonUtility.FromJson<LocalizationDataList> (dataAsJson);

			for (int i = 0; i < enumData.items.Length; i++) 
			{
				List<string> enumlistcat = new List<string> ();
				for (int j = 0; j < enumData.items[i].value.Length; j++) {


					//Debug.Log ("<color=green>" + "Key: " +    enumData.items [i].key +    " ALL enum data: " + enumData.items [i].value[j] + "</color>");	


					enumlistcat.Add (enumData.items [i].value [j]); 
				}

				enumDictionaryCategory.Add (enumData.items [i].key, enumlistcat);

				//enumlist.Add (enumData.items[i].value);
			}
			//			for (int i = 0; i < enumlist.Count; i++) {
			//				
			//				Debug.Log ("EnumList " + enumlist[i]);
			//			}
			//for (int i = 0; i < enumDictionary.Count; i++) {
			//	Debug.Log ("EnumDictionary Count: " + enumDictionary.Count);
			//}

			// Debug.Log ("Data loaded, dictionary contains: " + localizedText.Count + " entries");
		} else 
		{
			//Debug.LogError (filePath);
			//Debug.LogError ("Cannot find file!");
		}
	}

	/// <summary>
	/// Languages the check dropdown.
	/// </summary>
	void LanguageCheckDropdown()
	{
		//Localized dropdown depends on language selected in settings ***In progress 4/5/2016

		if (DataController.Instance.settings.languageIndex == 0 ) { // English
			LoadEnumJson("localizedtextDropdown_en.json");
			//Debug.Log("<color=green>" + "English" + "</color>");
		}
		else if (DataController.Instance.settings.languageIndex == 1 ) { // Spain
			LoadEnumJson("localizedtextDropdown_sp.json");
			//Debug.Log("<color=green>" + "Spanish" + "</color>");
		}
	}

	/// <summary>
	/// Raises the disable event.
	/// </summary>
	//enable dragging to pethomescreen
	void OnDisable()
	{
		DataController.Instance.playerData.enableDrag = true;
	}
}
	
/// <summary>
/// List of reports.
/// </summary>
public class  ListOfReports
{
	public int task_count;
	public string category;
	public string task;
	public DateTime dateStarted;
	public DateTime dateFinished;
	public uint session;
}
	
/// <summary>
/// filteredByDate list.
/// </summary>	
public class  filteredByDate
{
	//public int task_count;
	public int category_index;
	public string task;
	public DateTime dateStarted;
	public DateTime dateFinished;
    public uint session;
}





 



 
