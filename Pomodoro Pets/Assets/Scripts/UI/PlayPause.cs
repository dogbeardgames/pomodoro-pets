﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayPause : MonoBehaviour {

    [SerializeField]
    Sprite spritePlay, spritePause;

    [SerializeField]
    Color colorPlay, colorPause;

	// Use this for initialization
	void Start () {
		
	}
	
	#region METHODS
    public void Play()
    {
		//play sfx
		Framework.SoundManager.Instance.PlaySFX ("seIconTapPomodoro");
        GetComponent<Image>().sprite = spritePlay;
        GetComponent<Image>().color = colorPlay;

        transform.Find("Text").GetComponent<Text>().text = "Play";
        //Debug.Log("Play");
		DataController.Instance.playerData.isPaused = false;
    }

    public void Pause()
    {
		//play sfx
		//Framework.SoundManager.Instance.PlaySFX ("seIconTapPomodoro");
        GetComponent<Image>().sprite = spritePause;
        GetComponent<Image>().color = colorPause;

        transform.Find("Text").GetComponent<Text>().text = "Pause";
        //Debug.Log("Pause");
		DataController.Instance.playerData.isPaused = true;
    }
    #endregion
}
