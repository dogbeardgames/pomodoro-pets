﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Drag : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler {

    Vector3 initialPos;
    Vector3 vect3;

    float posDistance; 
    void Start () {   
        
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        initialPos = transform.parent.localPosition;
    }

    public void OnDrag(PointerEventData eventData)
    {
        //Debug.Log("Dragging " + transform.parent.gameObject.name);
        vect3 = eventData.position;
        vect3.y = initialPos.y;
        //Debug.Log("Pointer position " + eventData.position + ", object position " + transform.parent.position);
        transform.parent.localPosition = vect3;
    }
        
    public void OnEndDrag(PointerEventData eventData)
    {
//        GetComponent<Toggle>().interactable = true;
//        GetComponentInParent<VerticalLayoutGroup>().enabled = true;
        if(Mathf.Abs(transform.parent.position.x - initialPos.x) >= 200)
        {
            transform.parent.gameObject.SetActive(false);
            //Debug.Log("Delete!!!");
            FindObjectOfType<ScrollDrag>().enabled = true;
        }
        else
        {
            transform.parent.position = initialPos;
        }
    }        
}
