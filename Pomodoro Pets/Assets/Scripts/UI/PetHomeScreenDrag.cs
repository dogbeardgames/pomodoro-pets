﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
using Framework;
using UnityEngine.Events;
public class PetHomeScreenDrag : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerDownHandler {
	public static PetHomeScreenDrag ins;

	PointerEventData eventDataOnUpdate;

	//public RectTransform recPetHomeScreen;
	[SerializeField]
    CustomScrollView petHomeCustomScrollView;   

   // ScrollRect petHomeScrollRect;

    MainScreenDrag msd;

	[SerializeField]
    Vector2 m_initPos;
	bool screenshotMode;

	[SerializeField]
	Button btnback;

	[SerializeField]
    bool isInLeftMost;
	[SerializeField]
	PetHomePanController panController;
	Vector2 beginDragPos;
	[SerializeField]
	bool mouseDown = false;

	[SerializeField]
	bool moving = false;
	public bool IsInLeftMost{
		get{ return isInLeftMost;}
	}
	public void ScreenshotMode(bool b)
	{
		screenshotMode = b;
	}
	void Awake()
	{
		ins = this;
	}

	public void EnableContent()
	{
		petHomeCustomScrollView.Interactible = true;
	}

	void Start () {

		//print ("custom scroll");
        //petHomeScrollRect = GameObject.Find("PetHomeScrollView").GetComponent<ScrollRect>();
        msd = FindObjectOfType<MainScreenDrag>();

		if (DataController.Instance.playerData.ispetHome == true) 
		{
			transform.position = new Vector3 (0, 0, 0);
//			SoundManager.Instance.BGMOutside.clip = PetsHomeAudioClips.ins.GetBGMClip("bgm" + DataController.Instance.playerData.backGround + "Home");
			if(DataController.Instance.settings.isBGMOn)
			{
				SoundManager.Instance.PlayBGM("bgm" + DataController.Instance.playerData.backGround + "Home", true);			
				SoundManager.Instance.FadeInInside ();		
			}					
		}

		//when back button clicked
		btnback.onClick.AddListener(OnBackButton);
	}

	void Update()
	{
		if (DataController.Instance.playerData.ispetHome == true) {
			//print ("custom scroll");
			//recPetHomeScreen.GetComponent<ScrollRect> ().horizontal = true;
		}
		if(screenshotMode)
		{
			SpineMeshEnabler.ins.EnableMeshRenderer (false);
		}
	}
	

    #region MyRegion


    #endregion

    #region IBeginDragHandler implementation

	public bool IsDraggingToLeftMost()
	{
		//print ("dragging to left most");
		//if(beginDragPos.x < Input.mousePosition.x && mouseDown && petHomeScrollRect.horizontalNormalizedPosition <= 0.1)// && !screenshotMode)
		if(beginDragPos.x < Input.mousePosition.x && mouseDown)// && !screenshotMode)
		{
			//print ("to left most!");
			return true;
		}
	
		return false;
	}

	public void OnPointerDown(PointerEventData eData)
	{
		//print ("HOLLLA " + eData.position.x);

		if (eData.position.x > Screen.width*0.5f) {
			//print ("NYAAAAA!");
			//petHomeScrollRect.horizontal = false;

		} else if(eData.position.x < Screen.width*0.5f) {
			//print ("WEEEEEE");
			//petHomeScrollRect.horizontal = true;

		}
	}

    public void OnBeginDrag(PointerEventData eventData)
    {
        //Debug.Log("Screen begin drag");

        m_initPos = eventData.position;
		//print("ooooooooooooooooo " + m_initPos.x);
		beginDragPos = m_initPos;
		mouseDown = true;
		//print("scroll=" + petHomeScrollRect.horizontalNormalizedPosition + " pan=" + panController.getMaxLeft);

		if (panController != null) {
		//	print ("custom scroll");
//			if (petHomeScrollRect.horizontalNormalizedPosition <= panController.getMaxLeft + 0.02) 
//			{
//				if (m_initPos.x < Screen.width*0.5f) {
//					isInLeftMost = true;
//				
//				}
//			}
			if (m_initPos.x < Screen.width*0.5f && petHomeCustomScrollView.Content.localPosition.x >= petHomeCustomScrollView.MaxLeft) {
				isInLeftMost = true;

			}
		}
		else 
		{
			
//			if (petHomeScrollRect.horizontalNormalizedPosition <= 0) 
//			{
//				if (m_initPos.x < Screen.width*0.5f) {
//					isInLeftMost = true;
//				
//				}
//			}
			if (m_initPos.x < Screen.width*0.5f && petHomeCustomScrollView.Content.localPosition.x >= petHomeCustomScrollView.MaxLeft) {
				//print ("LEFT LEFT LEFT");
				isInLeftMost = true;

			}
		}
        
      //  Debug.Log("begin drag, is in left most? " + isInLeftMost + ", normalized pos " + petHomeScrollRect.horizontalNormalizedPosition);

	//	DataController.Instance.playerData.isDragEnd = true;

    }
    #endregion

    #region IDragHandler implementation

    public void OnDrag(PointerEventData eventData)
    {
        //Debug.Log("Screen drag, " + (eventData.position.x - m_initPos.x));
		mouseDown = true;
    }
    #endregion

    #region IEndDragHandler implementation

    public void OnEndDrag(PointerEventData eventData)
	{
		mouseDown = false;
			// check if drag left or right
		if (eventData.position.x > m_initPos.x && (isInLeftMost || !panController.LeftPanButton.gameObject.activeInHierarchy) && !moving && !screenshotMode && PetHomeController.ins.isOnPetHome)
		{
    		// dragged right
    		//Debug.Log ("Dragged left");
    		// calculate drag distance
    		if (Mathf.Abs (eventData.position.x - m_initPos.x) >= msd.dragDistance && FlyingBirdManager.ins.IsGiftActive()) 
    		{
            ReturnToPomodoro();
    		}

		}



		//moving = false;
	}

    #endregion

    #region METHODS

    public void ReturnToPomodoro()
    {
        // reset scroll pos
        petHomeCustomScrollView.SetContentXInitialPosition ();

        //print ("DISABLE SCROLL ON THIS EVENT!");
        //Debug.Log ("Unload Pet Home Screen");    
        SoundManager.Instance.FadeOutInside ();
        //print ("STOP home BGM here");
        moving = true;
        isInLeftMost = false;
        //petHomeScrollRect.enabled = false;

        petHomeCustomScrollView.Interactible = false;
        DataController.Instance.playerData.ispetHome = false;
        PetHomeController.ins.isOnPetHome = false;
        DOTween.Kill ("inertia",false);
        petHomeCustomScrollView.transform.DOLocalMoveX(petHomeCustomScrollView.Content.GetComponent<RectTransform>().rect.width/2, 0.75f, false).OnComplete(()=> LeavePetHome()).SetId("petshome");

        //return back the orientation
        if (Screen.orientation == ScreenOrientation.LandscapeLeft)
        {
            Screen.orientation = ScreenOrientation.Portrait;
            RectTransform rt = GameObject.Find("Foreground").GetComponent<RectTransform> ();
            rt.localScale = new Vector3 (1, 1, 1);

            //show camera
            if (GameObject.Find ("btnScreenShot") != null) 
            {
                GameObject.Find ("btnScreenShot").gameObject.SetActive (true);
            }

        }
    }

    void LeavePetHome()
    {
        moving = false;
        DOTween.Kill ("petshome", false);
        petHomeCustomScrollView.SetContentXInitialPosition ();

        //7/04/2017
        //      DataController.Instance.playerData.ispetHome = false;
        //      PetHomeController.ins.isOnPetHome = false;
    }

    public void OnBackButton()
    {
        //print ("BACK TO PETS HOME AFTER SCREENSHOT");
        //Debug.Log ("Unload Pet Home Screen");    
        SoundManager.Instance.FadeOutInside ();
        //print ("STOP home BGM here");
        //petHomeScrollRect.transform.DOLocalMoveX (petHomeScrollRect.GetComponent<RectTransform> ().rect.width, 0.75f, false);

        DataController.Instance.playerData.ispetHome = true;
        PetHomeController.ins.isOnPetHome = true;

        //return back the orientation
        if (Screen.orientation == ScreenOrientation.LandscapeLeft)
        {
            Screen.orientation = ScreenOrientation.Portrait;
        }

        //btnback.gameObject.SetActive (false);

        RectTransform rt = transform.Find("Foreground").GetComponent<RectTransform> ();
        rt.localScale = new Vector3 (1, 1, 1);
        transform.Find ("Midground").GetComponent<RectTransform> ().localScale = rt.localScale;
        transform.Find ("Background").GetComponent<RectTransform> ().localScale = rt.localScale;
    }

    #endregion	
}
