﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class ScrollDrag : ScrollRect {       

    Vector3 pos;

    public bool isDragging = false;

    TaskButton taskButton;

    /// <summary>
    /// Always route initialize potential drag event to parents
    /// </summary>
    public override void OnInitializePotentialDrag (PointerEventData eventData)
    {        
        base.OnInitializePotentialDrag (eventData);
    }

    /// <summary>
    /// Drag event
    /// </summary>
    public override void OnDrag (PointerEventData eventData)
    {        
        base.OnDrag (eventData);
        isDragging = true;
    }

    /// <summary>
    /// Begin drag event
    /// </summary>
    public override void OnBeginDrag (PointerEventData eventData)
    {        
        base.OnBeginDrag (eventData);
        //Debug.Log("Begin drag");
    }

    /// <summary>
    /// End drag event
    /// </summary>
    public override void OnEndDrag (PointerEventData eventData)
    {       
        base.OnEndDrag (eventData);
        isDragging = false;
    }                                  
}
