﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThoughtBubbleController : MonoBehaviour {

    [SerializeField]
    GameObject thoughBubble;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
//	void Update () {
//		
//	}

    #region METHODS
    /// <summary>
    /// Enable thought bubble for pet interaction
    /// </summary>
    public void EnableThoughtBubble()
    {
        thoughBubble.gameObject.SetActive(true);
    }
    /// <summary>
    /// Disable thought bubble for pet interaction
    /// </summary>
    public void DisableThoughtBubble()
    {
        thoughBubble.gameObject.SetActive(false);
    }

    #endregion
}
