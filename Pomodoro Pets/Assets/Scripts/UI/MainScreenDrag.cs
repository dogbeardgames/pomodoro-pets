﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MainScreenDrag : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {
	
	PointerEventData eventDataOnUpdate;

	public RectTransform recPetHomeScreen;

    public float dragDistance;

    private Vector2 m_initPos;
	Vector2 permanentEventDataPos;

	void Awake()
	{

	}

	// Use this for initialization
	void Start () 
	{
        dragDistance = GetComponent<RectTransform>().rect.width / 4;

		recPetHomeScreen = GetComponent<RectTransform> ();

//		if (DataController.Instance.playerData.ispetHome == true) 
//		{
//			transform.position = new Vector3 (0, 0, 0);
//		}	
	
	}


		
    #region IBeginDragHandler implementation



    public void OnBeginDrag(PointerEventData eventData)
    {
		
			//Debug.Log ("Screen begin drag at " + eventData.position.x);
			m_initPos = eventData.position;
			DataController.Instance.playerData.isDragEnd = false;

    }
    #endregion

    #region IDragHandler implementation

    public void OnDrag(PointerEventData eventData)
    {
      //  Debug.Log("Screen drag, " + (eventData.position.x - m_initPos.x));
    }
    #endregion

    #region IEndDragHandler implementation

    public void OnEndDrag(PointerEventData eventData)
	{
		if (!PomodoroUI.Instance.thereIsActiveDialog)
		{
			if (!PomodoroUI.Instance.isMenuActive && !DataController.Instance.playerData.isTimerRunning) {

				if (eventData.position.x < m_initPos.x && m_initPos.x > Screen.width * 0.5f) {// dragged left
					Debug.Log("Dragged left");
					// calculate drag distance
					if (Mathf.Abs (eventData.position.x - m_initPos.x) >= dragDistance) {
						permanentEventDataPos = eventData.position;

						GetComponent<PomodoroUI> ().HomeButton ();
						DataController.Instance.playerData.ispetHome = true;
						PetHomeController.ins.isOnPetHome = true;      
						PetHomeScreenDrag.ins.EnableContent ();
					}	


					//DataController.Instance.playerData.isDragEnd = true;
				}
			}
			else if (!PomodoroUI.Instance.isMenuActive && DataController.Instance.playerData.isTimerRunning && 
				DataController.Instance.playerData.state != EnumCollection.PomodoroStates.TimerScreen) {
				{
					if (eventData.position.x < m_initPos.x && m_initPos.x > Screen.width * 0.5f) {// dragged left
						//Debug.Log("Dragged left");
						HideGoldAnimation.instance.HideAnimation ();
						// calculate drag distance
						if (Mathf.Abs (eventData.position.x - m_initPos.x) >= dragDistance) {
							permanentEventDataPos = eventData.position;

							GetComponent<PomodoroUI> ().HomeButton ();
							DataController.Instance.playerData.ispetHome = true;
							PetHomeController.ins.isOnPetHome = true;
							PetHomeScreenDrag.ins.EnableContent ();
						}	

					}
				}
			}
			else 
			{				
				//please finish your session first
				//GetComponent<PomodoroUI> ().PopUpHome ();
			}


			if (eventData.position.x < m_initPos.x && m_initPos.x > Screen.width * 0.5f) 
			{
				// calculate drag distance
				if (Mathf.Abs (eventData.position.x - m_initPos.x) >= dragDistance) {
					permanentEventDataPos = eventData.position;

					//please finish your session first
					GetComponent<PomodoroUI> ().PopUpHome ();
				}	
			}
		}

	
	}



    #endregion
}
