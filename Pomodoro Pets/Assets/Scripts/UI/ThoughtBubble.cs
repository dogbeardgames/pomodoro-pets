﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using Spine.Unity;

public class ThoughtBubble : MonoBehaviour {

    public int petIndex, startTime, interval;

    private int m_counter = 0;

    [SerializeField]
    bool isForHome;

    //place holder
    Color heart = Color.red, exclamation = Color.yellow, happinessColor = Color.blue, hunger_thirstColor = Color.green, cleanlinessColor = Color.white, energyColor = Color.magenta;

    [SerializeField]
    Sprite sprtHeart, sprtExclamation, sprtHappiness, sprtHunger_Thirst_Dog, sprtHunger_Thirst_Cat, sprtCleanliness, sprtEnergy;

    [SerializeField]
    SpriteRenderer sr;
    //end place holder

    WaitForSeconds timeInterval;// interval for the thought bubble UI

	public static bool enable = true;

	SkeletonAnimation petSpineAnim;
	// Use this for initialization
	void Start ()
    {                                
        if(!isForHome)
        {
            petIndex = DataController.Instance.playerData.currentPetIndex;
        }

        Invoke("StartThoughtBubble", startTime);
		petSpineAnim = GetComponent<SkeletonAnimation> ();
		//print (petSpineAnim.SkeletonDataAsset.name);
		//print (petSpineAnim.skeletonDataAsset.name);
		if(petSpineAnim.SkeletonDataAsset.name == "Pug_SkeletonData")
		{
			transform.GetChild(0).localPosition = new Vector2 (transform.GetChild(0).localPosition.x, 5f);
		}
	}
	
    #region METHODS
    IEnumerator _Timer()
    {        
        timeInterval = new WaitForSeconds(interval);

        float happiness, hunger_thirst, cleanliness, energy;

		if (isForHome)
		{            
			WaitForSeconds wfsPetHome = new WaitForSeconds(1);
			while (true)
			{                
				happiness = DataController.Instance.playerData.playerPetList[petIndex].happiness;
				hunger_thirst = DataController.Instance.playerData.playerPetList[petIndex].hunger_thirst;
				cleanliness = DataController.Instance.playerData.playerPetList[petIndex].cleanliness;
				energy = DataController.Instance.playerData.playerPetList[petIndex].energy;

				//invisible
				if(enable)
				sr.enabled = true;

				float total = happiness + hunger_thirst + cleanliness + energy;

				//Debug.Log("Pet index " + petIndex + " happiness " + happiness + ", hunger_thirst " + hunger_thirst + ", cleanliness " + cleanliness + ", energy" + energy);

				if (total >= 3)
				{                    
					//                    sr.color = exclamation;
					//                    Debug.Log("<color=red>Thought bubble exclamation point!</color>");
					sr.sprite = sprtExclamation;
				}
                else if(total == 0)
				{
					//                    sr.color = heart;
					//                    Debug.Log("<color=red>Thought bubble heart!</color>");
					sr.sprite = sprtHeart;
				}
                else
                {
                    sr.sprite = null;
                }
				yield return wfsPetHome;
				sr.enabled = false;
				yield return timeInterval;

			}
		}
		// is the thought bubble used in pet screen
		else
		{            
			EnumCollection.Pet petCategory = DataController.Instance.playerData.playerPetList[petIndex].petType;

			while(true)
			{                
				happiness = DataController.Instance.playerData.playerPetList[petIndex].happiness;
				hunger_thirst = DataController.Instance.playerData.playerPetList[petIndex].hunger_thirst;
				cleanliness = DataController.Instance.playerData.playerPetList[petIndex].cleanliness;
				energy = DataController.Instance.playerData.playerPetList[petIndex].energy;

				switch (m_counter)
				{
				case 0:
					if (happiness > 0)// happiness thought bubble
					{
						if(enable)
						sr.enabled = true;
						//sr.color = happinessColor;
						sr.sprite = sprtHappiness;
						yield return timeInterval;
						sr.enabled = false;
						yield return timeInterval;
					}
					break;
				case 1:
					if (hunger_thirst > 0)// hunger_thirst thought bubble
					{
						if(enable)
						sr.enabled = true;
						//sr.color = hunger_thirstColor;

						if(petCategory == EnumCollection.Pet.Dog)
							sr.sprite = sprtHunger_Thirst_Dog;
						else if(petCategory == EnumCollection.Pet.Cat)
							sr.sprite = sprtHunger_Thirst_Cat;

						yield return timeInterval;
						sr.enabled = false;
						yield return timeInterval;
					}
					break;
				case 2:
					if (cleanliness > 0)// cleanliness thought bubble
					{       
						if(enable)
						sr.enabled = true;
						//sr.color = cleanlinessColor;
						sr.sprite = sprtCleanliness;
						yield return timeInterval;
						sr.enabled = false;
						yield return timeInterval;
					}
					break;
				case 3:
					if (energy > 0)// energy thought bubble
					{              
						if(enable)
						sr.enabled = true;
						//sr.color = energyColor;
						sr.sprite = sprtEnergy;
						yield return timeInterval;
						sr.enabled = false;
						yield return timeInterval;
					}
					break;
				default:                         
					yield return new WaitForSeconds(0);
					break;
				}

				if (m_counter > 3)
				{
					m_counter = 0;
				}
				else
				{
					m_counter++;
				}                    
			}                
		}

       
    }

    void StartThoughtBubble()
    {
        //Debug.Log("Start coroutine, pet");
        StartCoroutine("_Timer");
    }
    #endregion
}
