﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using UnityEngine.UI;

[System.Serializable]
public class LocalizedText : MonoBehaviour {


	public string key;

	//public TextMeshProUGUI txtToLocalize;
	TextMeshProUGUI text;
	void Start()
	{
		LocalizationManager.OnLanguageChange += Language;
		//Language();
	}
    void OnEnable()
    {
        LocalizationManager.OnLanguageChange += Language;
        //Language();
    }

    void OnDisable()
    {
        LocalizationManager.OnLanguageChange -= Language;
    }


	void OnDestroy()
	{
		LocalizationManager.OnLanguageChange -= Language;
		//print ("CRY " + gameObject.name);
	}
    // language
    void Language()
	{ 

		if(LocalizationManager.instance.GetLocalizedValue(key) != null)
		{
        //Text text = GetComponent<Text> ();
			try{
				if(GetComponent<TextMeshProUGUI>() != null)
				{
					text = GetComponent<TextMeshProUGUI>();
					text.text = LocalizationManager.instance.GetLocalizedValue (key);
				}

			}catch(Exception ex){
				//print (ex.ToString().ToUpper());
				//print("game object " + gameObject.transform.parent.gameObject.name);
			}
       		
        	
        // Debug.Log("Text mesh " + text.name);
       
            
        }
        //Debug.Log ("Text Changed");
    }

//	// language
//	public void LanguageTextUI()
//	{
//
//		//Text text = GetComponent<Text> ();
//		TextMeshProUGUI text = GetComponent<TextMeshProUGUI>();
//		//Debug.Log("game object " + gameObject.name);
//		// Debug.Log("Text mesh " + text.name);
//		if(LocalizationManager.instance.GetLocalizedValue(txtToLocalize.text) != null)
//		{
//			text.text = LocalizationManager.instance.GetLocalizedValue (txtToLocalize.text);
//		}
//		//Debug.Log ("Text Changed");
//	}
}
