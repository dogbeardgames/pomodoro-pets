﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LocalizationManager : MonoBehaviour {

    public static LocalizationManager instance;

	private Dictionary<string, string> localizedText  = new Dictionary<string, string> ();
    private bool isReady = false;
    private string missingTextString = "Localized text not found";

    // Use this for initialization
    void Awake () 
    {
        if (instance == null) 
        {
            instance = this;
			DontDestroyOnLoad (gameObject);
        } 
        else            
        {
            Destroy (gameObject);
        }        
    }

    void Start()
    {
        LanguageCheck();
    }

    // initiAL
    void LanguageCheck()
    {
        //Localized Data depends on language selected in settings

        if (DataController.Instance.settings.languageIndex == 0 ) { // English
            LoadLocalizedText("localizedText_en.json");
            //Debug.Log("English");
        }
        else if (DataController.Instance.settings.languageIndex == 1 ) { // Spain
            LoadLocalizedText("localizedText_sp.json");
            //Debug.Log("Spanish");
        }
    }




    public void UpdateLanguage(EnumCollection.Language language)
    {
        switch (language)
        {
            case EnumCollection.Language.English:
                LoadLocalizedText("localizedText_en.json");
                ChangeLanguageEvent();
                break;
            case EnumCollection.Language.Spanish:
                LoadLocalizedText("localizedText_sp.json");
                ChangeLanguageEvent();
                break;
            default:
                break;
        }
    }

    public void LoadLocalizedText(string fileName)
    {
		localizedText  = new Dictionary<string, string> ();

        //string filePath = Path.Combine (Application.streamingAssetsPath, fileName);

        
		string filePath = "";

		if (Application.platform == RuntimePlatform.Android)
		{
			// Android
			filePath = System.IO.Path.Combine(Application.streamingAssetsPath, fileName);

			// Android only use WWW to read file
			WWW reader = new WWW(filePath);
			while ( ! reader.isDone) {}

			string realPath = Application.persistentDataPath + fileName;
			System.IO.File.WriteAllBytes(realPath, reader.bytes);

			filePath = realPath;
		}
		else
		{
			// iOS
			filePath = System.IO.Path.Combine(Application.streamingAssetsPath, fileName);
		}
			

		if (File.Exists (filePath)) {

			//Debug.Log ("Test");
            string dataAsJson = File.ReadAllText (filePath);
            LocalizationData loadedData = JsonUtility.FromJson<LocalizationData> (dataAsJson);

            for (int i = 0; i < loadedData.items.Length; i++) 
            {
                localizedText.Add (loadedData.items [i].key, loadedData.items [i].value);  

                // temp
//                if(loadedData.items[i].value.Substring(0, 1) == "[")
//                {
//                    Debug.Log("Array of object " + loadedData.items[i].key);
//                }
            }

           // Debug.Log ("Data loaded, dictionary contains: " + localizedText.Count + " entries");
        } else 
        {
			//Debug.LogError (filePath);
            //Debug.LogError ("Cannot find file!");
        }

        isReady = true;
    }

    public string GetLocalizedValue(string key)
    {
        string result = missingTextString;
        if (localizedText.ContainsKey (key)) 
        {
            result = localizedText [key];
        }

        return result;

    }

    public bool GetIsReady()
    {
        return isReady;
    }

    // change language event
    void ChangeLanguageEvent()
    {
        if (OnLanguageChange != null)
            OnLanguageChange();
    }

    public delegate void ChangeLaguage();
    public static event ChangeLaguage OnLanguageChange;





}