﻿
using System.Collections.Generic;

[System.Serializable]
public class EnumItem
{
	public string key { get; set; }
	public List<string> value { get; set; }
}
[System.Serializable]
public class RootObject
{
	public List<EnumItem> items { get; set; }
}