﻿[System.Serializable]
public class LocalizationData 
{
    public LocalizationItem[] items;
}

[System.Serializable]
public class LocalizationItem
{
    public string key;
    public string value;
}

[System.Serializable]
public class LocalizationDataList
{
	public LocalizationItems[] items;
}

[System.Serializable]
public class LocalizationItems
{
	public string key;
	public string[] value;
}
