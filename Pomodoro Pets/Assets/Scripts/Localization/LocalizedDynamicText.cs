﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using UnityEngine.UI;

[System.Serializable]
public class LocalizedDynamicText : MonoBehaviour {

	public TextMeshProUGUI txtToLocalize;

	void Start()
	{
		LocalizationManager.OnLanguageChange += LanguageTextUI;
		LanguageTextUI();
	}
	void OnEnable()
	{
		LocalizationManager.OnLanguageChange += LanguageTextUI;
		LanguageTextUI();
	}

	void OnDisable()
	{
		LocalizationManager.OnLanguageChange -= LanguageTextUI;
	}




	// language
	public void LanguageTextUI()
	{

		//Text text = GetComponent<Text> ();
		TextMeshProUGUI text = GetComponent<TextMeshProUGUI>();
		//Debug.Log("game object " + gameObject.name);
		// Debug.Log("Text mesh " + text.name);
		if(LocalizationManager.instance.GetLocalizedValue(txtToLocalize.text) != null)
		{
			text.text = LocalizationManager.instance.GetLocalizedValue (txtToLocalize.text);
		}
		//Debug.Log ("Text Changed");
	}
}
