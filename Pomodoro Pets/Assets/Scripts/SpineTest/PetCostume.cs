﻿using System.Collections;
using UnityEngine;
using Spine.Unity;
using Spine.Unity.Modules;
using Spine;
using System.Linq;
using System.Collections.Generic;
using System;
using Spine.Unity;

public class PetCostume : MonoBehaviour {

    [SerializeField]
    List<PetAnim> spa;

    [SerializeField]
    List<PetAnim> spaSleep;

    [SerializeField]
    List<PetAnim> swing;

    SkeletonAnimation skeletonAnimation;

    PetAssets petAssets;

    List<int> equippedBodyList;
    List<int> equippedHeadList;
    List<int> equippedAccessoriesList;

    void Awake()
    {
        petAssets = FindObjectOfType<PetAssets>();
        skeletonAnimation = GetComponent<SkeletonAnimation>();
        equippedBodyList = new List<int>();
        equippedHeadList = new List<int>();
        equippedAccessoriesList = new List<int>();
    }

	// Use this for initialization
	void Start () {        
//        Sprite[] sprite = Resources.LoadAll<Sprite>("Akita/Accessories/AkitaAccessories");
//        Sprite s_Sprite = sprite.Single(s => s.name == "geekGlasses");
//
//        this.sprite.GetComponent<SpriteRenderer>().sprite = s_Sprite;
        //skeletonRenderer.skeleton.att
	}
	
	// Update is called once per frame
//	void Update () {
//
//	}               

    public void ApplyHead(string key)
    {
        try
        {
            string path = DataController.Instance.headDatabase.items.FirstOrDefault(item => item.key == key).value;
            string slotName = DataController.Instance.headDatabase.items.FirstOrDefault(item => item.key == key).slot;
            string breed = DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].petBreed;

            Sprite[] headSprites = Resources.LoadAll<Sprite>(path + breed + "Head");
            Sprite headSprite;

            //Debug.Log(path + breed + "Head, key, " + key);

            headSprite = headSprites.FirstOrDefault(s => s.name == key);

            // Reset head costume
            ResetHead();

            // Add head costume slot
            equippedHeadList.Add(skeletonAnimation.skeleton.FindSlotIndex(slotName));
            //Debug.Log("Slot name " + slotName + ", " + headSprite);
            //Debug.Log("spine slot " + skeletonAnimation.skeleton.FindSlot(slotName));

            // Attach Sprite
            skeletonAnimation.skeleton.AttachUnitySprite(slotName, headSprite);

//            // save equipement
            DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].headEquipment = key;
            GetComponent<Pet>().headEquipment = key;
        }
        catch(NullReferenceException e)
        {
            //Debug.Log(e.Message);
        }
    }

    public void ApplyBody(string key)
    {   
        try
        {
            string breed = DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].petBreed;  
            //Debug.Log(key);
            //Debug.Log("Body " + petAssets.bodyDatabase.items.FirstOrDefault(item => item.key == key).slot);

            //if(petBodyCostumeDictionary.item.Single(item => item.key == key).slot == "" || petBodyCostumeDictionary.item.Single(item => item.key == key).slot == null)
            if (DataController.Instance.bodyDatabase.items.FirstOrDefault(item => item.key == key).slot == "" || DataController.Instance.bodyDatabase.items.FirstOrDefault(item => item.key == key).slot == null)
            {
                //Debug.Log("slot empty");

                //Debug.Log(petAssets.bodyDatabase.items.Single(item => item.key == key).slot);

                string path = DataController.Instance.bodyDatabase.items.First(item => item.key == key).value;

                //Debug.Log(path + breed + key);
                Sprite[] bodySprites = Resources.LoadAll<Sprite>(path + breed + key);//Akita/Body/AkitaBBad

                Sprite BackLeftLeg = null,
                BackLeftLeg2 = null,
                BackRightLeg = null,
                BackRightLeg2 = null,
                FrontLeftLeg = null,
                FrontLeftLeg2 = null,
                FrontRightLeg = null,
                FrontRightLeg2 = null,
                LowerBody = null,
                UpperBody = null,
                UpperBody2 = null,
                Back = null,
                Shield = null;

                BackLeftLeg = bodySprites.First(s => s.name == key + "BackLeftLeg");
                BackLeftLeg2 = bodySprites.First(s => s.name == key + "BackLeftLeg2");
                BackRightLeg = bodySprites.First(s => s.name == key + "BackRightLeg");
                BackRightLeg2 = bodySprites.First(s => s.name == key + "BackRightLeg2");
                FrontLeftLeg = bodySprites.First(s => s.name == key + "FrontLeftLeg");
                FrontLeftLeg2 = bodySprites.First(s => s.name == key + "FrontLeftLeg2");
                FrontRightLeg = bodySprites.First(s => s.name == key + "FrontRightLeg");
                FrontRightLeg2 = bodySprites.First(s => s.name == key + "FrontRightLeg2");
                LowerBody = bodySprites.First(s => s.name == key + "LowerBody");
                UpperBody = bodySprites.First(s => s.name == key + "UpperBody");
                if(DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].petType == EnumCollection.Pet.Dog)
                {
                    UpperBody2 = bodySprites.FirstOrDefault(s => s.name == key + "UpperBody2");
                }

                if (key == "BVader")
                {
                    Back = bodySprites.FirstOrDefault(s => s.name == key + "Cape");
                }

                if(key == "Catain")
                {
                    Shield = bodySprites.FirstOrDefault(s => s.name == key + "Shield");
                }

                // Reset full costume slot

                ResetBody();

                // Add full costume slot
                equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("backLeftLeg"));
                equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("backLeftLeg2"));
                equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("backRightLeg"));
                equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("backRightLeg2"));
                equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("frontLeftLeg"));
                equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("frontLeftLeg2"));
                equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("frontRightLeg"));
                equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("frontRightLeg2"));
                equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("lowerBody"));
                equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("upperBody"));
                if(DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].petType == EnumCollection.Pet.Dog)
                {
                    equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("upperBody2"));
                }

                if (key == "BVader")
                {
                    equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("back"));
                    skeletonAnimation.skeleton.AttachUnitySprite("back", Back);
                }

                if(key == "Catain")
                {
                    equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("shield"));
                    skeletonAnimation.skeleton.AttachUnitySprite("shield", Shield);
                }

                // show if slot is available
                //Debug.Log("<color=white> slot " + skeletonAnimation.skeleton.FindSlot("backLeftLeg") + "</color>");
                //Debug.Log("<color=white> slot " + skeletonAnimation.skeleton.FindSlot("backLeftLeg2") + "</color>");
                //Debug.Log("<color=white> slot " + skeletonAnimation.skeleton.FindSlot("backRightLeg") + "</color>");
                //Debug.Log("<color=white> slot " + skeletonAnimation.skeleton.FindSlot("backRightLeg2") + "</color>");
                //Debug.Log("<color=white> slot " + skeletonAnimation.skeleton.FindSlot("frontLeftLeg") + "</color>");
                //Debug.Log("<color=white> slot " + skeletonAnimation.skeleton.FindSlot("frontLeftLeg2") + "</color>");
                //Debug.Log("<color=white> slot " + skeletonAnimation.skeleton.FindSlot("frontRightLeg") + "</color>");
                //Debug.Log("<color=white> slot " + skeletonAnimation.skeleton.FindSlot("frontRightLeg2") + "</color>");
                //Debug.Log("<color=white> slot " + skeletonAnimation.skeleton.FindSlot("lowerBody") + "</color>");
                //Debug.Log("<color=white> slot " + skeletonAnimation.skeleton.FindSlot("upperBody") + "</color>");

                skeletonAnimation.skeleton.AttachUnitySprite("backLeftLeg", BackLeftLeg);
                skeletonAnimation.skeleton.AttachUnitySprite("backLeftLeg2", BackLeftLeg2);
                skeletonAnimation.skeleton.AttachUnitySprite("backRightLeg", BackRightLeg);
                skeletonAnimation.skeleton.AttachUnitySprite("backRightLeg2", BackRightLeg2);
                skeletonAnimation.skeleton.AttachUnitySprite("frontLeftLeg", FrontLeftLeg);
                skeletonAnimation.skeleton.AttachUnitySprite("frontLeftLeg2", FrontLeftLeg2);
                skeletonAnimation.skeleton.AttachUnitySprite("frontRightLeg", FrontRightLeg);
                skeletonAnimation.skeleton.AttachUnitySprite("frontRightLeg2", FrontRightLeg2);
                skeletonAnimation.skeleton.AttachUnitySprite("lowerBody", LowerBody);
                skeletonAnimation.skeleton.AttachUnitySprite("upperBody", UpperBody);
                if(DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].petType == EnumCollection.Pet.Dog)
                {
                    skeletonAnimation.skeleton.AttachUnitySprite("upperBody2", UpperBody2);
                }

                // save equipement
                DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].bodyEquipment = key;
                GetComponent<Pet>().bodyEquipment = key;
            }
            else
            {  
                string path = DataController.Instance.bodyDatabase.items.FirstOrDefault(item => item.key == key).value;

                Sprite[] sprites = Resources.LoadAll<Sprite>(path + breed + "NonSet");// Body/AkitaBBad, Body/AkitaNonSet

                Sprite body;

                body = sprites.FirstOrDefault(s => s.name == key);

                // Reset full costume slot

                ResetBody();

                //Debug.Log("pet slot " + DataController.Instance.bodyDatabase.items.FirstOrDefault(item => item.key == key).slot);
                // Add single costume slot
                equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex(DataController.Instance.bodyDatabase.items.FirstOrDefault(item => item.key == key).slot));

                skeletonAnimation.Skeleton.AttachUnitySprite(DataController.Instance.bodyDatabase.items.FirstOrDefault(item => item.key == key).slot, body);

                // save equipement
                DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].bodyEquipment = key;
                GetComponent<Pet>().bodyEquipment = key;
            }  
        }
        catch(NullReferenceException e)
        {
//            Debug.LogError(e.Message);
        }
    }

    public void ApplyAccessories(string key)
    {
        try
        {
            string path = DataController.Instance.AccessoriesDatabase.items.FirstOrDefault(item => item.key == key).value;
            string slotName = DataController.Instance.AccessoriesDatabase.items.FirstOrDefault(item => item.key == key).slot;
            string breed = DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].petBreed;

            Sprite[] accessorySprites = Resources.LoadAll<Sprite>(path + breed + "Accessories");
            Sprite accessorySprite;

            accessorySprite = accessorySprites.FirstOrDefault(s => s.name == key);

            // Reset head costume
            ResetAccessories();

            //Debug.Log("Equipped accessories " + equippedAccessoriesList.Count());
            //Debug.Log("skeleton renderer " + skeletonAnimation.skeleton.GetType());
            //Debug.Log("slot name " + slotName + " sprite " + accessorySprite.name);
            // Add head costume slot
            equippedAccessoriesList.Add(skeletonAnimation.skeleton.FindSlotIndex(slotName));

            // Attach Sprite
            skeletonAnimation.skeleton.AttachUnitySprite(slotName, accessorySprite);

            // save equipement
            DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].accessories = key;
            GetComponent<Pet>().accessories = key;
        }
        catch(NullReferenceException e)
        {
            
        }
    }

    public void ApplySleepCostume()
    {
        //Debug.Log("<color=red>apply cat sleep costume</color>");
        Pet pet = FindObjectOfType<Pet>();

        if (pet != null)
        {
            pet.GetComponent<SkeletonAnimation>().Initialize(true);

            ApplySleepHead(pet.headEquipment, pet.petBreed);
            ApplySleepBody(pet.bodyEquipment, pet.petBreed);
            ApplySleepAccessory(pet.accessories, pet.petBreed);
        }
    }
    /// <summary>
    /// Cat sleep head equipment
    /// </summary>
    /// <param name="key">Key.</param>
    /// <param name="_catBreed">Cat breed.</param>
    void ApplySleepHead(string key, string _catBreed)
    {
        try
        {            
            if (_catBreed == EnumCollection.CatBreed.Abyssinian.ToString() ||
                _catBreed == EnumCollection.CatBreed.Bengal.ToString() ||
                _catBreed == EnumCollection.CatBreed.Bombay.ToString() ||
                _catBreed == EnumCollection.CatBreed.Siamese.ToString() ||
                _catBreed == EnumCollection.CatBreed.Sphynx.ToString())
            {
                string path = DataController.Instance.headDatabase.items.FirstOrDefault(item => item.key == key).value;
                string slotName = DataController.Instance.headDatabase.items.FirstOrDefault(item => item.key == key).slot;
                string breed = DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].petBreed;

                Sprite[] headSprites = Resources.LoadAll<Sprite>(path + breed + "Head");
                Sprite headSprite;

                //Debug.Log(path + breed + "Head, key, " + key);

                // change first letter to upper case
//                key = key.First().ToString().ToUpper() + key.Substring(1);

                headSprite = headSprites.FirstOrDefault(s => s.name == key);

                // Reset head costume
                ResetHead();

                // Add head costume slot
                equippedHeadList.Add(skeletonAnimation.skeleton.FindSlotIndex(slotName));
                //Debug.Log("Slot name " + slotName + ", " + headSprite);

                // Attach Sprite
                skeletonAnimation.skeleton.AttachUnitySprite(slotName, headSprite);

//                // save equipement
//                DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].headEquipment = key;
//                GetComponent<Pet>().headEquipment = key;
            } 
            else
            {
                if(key == "pandaHat" || key == "CatainMask")
                {
                    string path = DataController.Instance.headDatabase.items.FirstOrDefault(item => item.key == key).value;
                    string slotName = "sleepHeadCostume";
                    string breed = DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].petBreed;

                    Sprite[] headSprites = Resources.LoadAll<Sprite>(path + breed + "Head");
                    Sprite headSprite;

                    //Debug.Log(path + breed + "Head, key, " + key);

                    // change first letter to upper case
                    key = "sleep" + key.First().ToString().ToUpper() + key.Substring(1);

                    headSprite = headSprites.FirstOrDefault(s => s.name == key);

                    // Reset head costume
                    ResetHead();

                    // Add head costume slot
                    equippedHeadList.Add(skeletonAnimation.skeleton.FindSlotIndex("sleepHeadCostume"));
                    //Debug.Log("Slot name " + slotName + ", " + headSprite);

                    // Attach Sprite
                    skeletonAnimation.skeleton.AttachUnitySprite(slotName, headSprite);
                }
            }
        }
        catch(NullReferenceException e)
        {
//            Debug.LogError(e.Message);
        }
    }
    /// <summary>
    /// Cat sleep body equipment
    /// </summary>
    /// <param name="key">Key.</param>
    /// <param name="_catBreed">Cat breed.</param>
    void ApplySleepBody(string key, string _catBreed)
    {
        try
        {
            if (_catBreed == EnumCollection.CatBreed.Abyssinian.ToString() ||
                _catBreed == EnumCollection.CatBreed.Bengal.ToString() ||
                _catBreed == EnumCollection.CatBreed.Bombay.ToString() ||
                _catBreed == EnumCollection.CatBreed.Siamese.ToString() ||
                _catBreed == EnumCollection.CatBreed.Sphynx.ToString())
            {
                string breed = DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].petBreed;

                if (DataController.Instance.bodyDatabase.items.FirstOrDefault(item => item.key == key).slot == "" || DataController.Instance.bodyDatabase.items.FirstOrDefault(item => item.key == key).slot == null)
                {
                    //Debug.Log("<color=red>SLOT EMPTY!</color>");

                    string path = DataController.Instance.bodyDatabase.items.First(item => item.key == key).value;

                    //Debug.Log("<color=red>" + path + breed + key + "</color>");
                    
                    Sprite[] bodySprites = Resources.LoadAll<Sprite>(path + breed + key);

                    Sprite  SleepBody,
                            SleepLegFront,
                            SleepLegBack,
                            Shield;
                    
                    //Debug.Log("<color=red>" + key + "</color>");

                    SleepBody = bodySprites.First(s => s.name == key + "SleepBody");
                    SleepLegFront = bodySprites.First(s => s.name == key + "SleepLegFront");
                    SleepLegBack = bodySprites.First(s => s.name == key + "SleepLegBack");
                    Shield = bodySprites.First(s => s.name == key + "Shield");

                    ResetBody();

                    // Add full costume slot
                    equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("sleepBody"));
                    equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("sleepLegFront"));
                    equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("sleepLegBack"));
                    equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("shield"));

                    // show if slot is available
                    //Debug.Log("<color=white> slot " + skeletonAnimation.skeleton.FindSlot("sleepBody") + "</color>");
                    //Debug.Log("<color=white> slot " + skeletonAnimation.skeleton.FindSlot("sleepLegFront") + "</color>");
                    //Debug.Log("<color=white> slot " + skeletonAnimation.skeleton.FindSlot("sleepLegBack") + "</color>");
                    //Debug.Log("<color=white> slot " + skeletonAnimation.skeleton.FindSlot("shield") + "</color>");

                    skeletonAnimation.skeleton.AttachUnitySprite("sleepBody", SleepBody);
                    skeletonAnimation.skeleton.AttachUnitySprite("sleepLegFront", SleepLegFront);
                    skeletonAnimation.skeleton.AttachUnitySprite("sleepLegBack", SleepLegBack);
                    skeletonAnimation.skeleton.AttachUnitySprite("shield", Shield);
                }
                else
                {
                    //Debug.Log("<color=red>NONSET!!!" + key + "</color>");
                    if(key == "bowtie")
                    {
                        string path = DataController.Instance.bodyDatabase.items.FirstOrDefault(item => item.key == key).value;

                        Sprite[] sprites = Resources.LoadAll<Sprite>(path + breed + "NonSet");// Body/AkitaBBad, Body/AkitaNonSet

                        Sprite body;

                        //Debug.Log("<color=red>" + key + "</color>");

                        body = sprites.FirstOrDefault(s => s.name == key);

                        // Reset full costume slot

                        ResetBody();

                        // Add single costume slot
                        equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("bodyCostume2"));//sleepBodyCostume

                        skeletonAnimation.Skeleton.AttachUnitySprite("bodyCostume2", body);//sleepBodyCostume                                        
                    }
                    else
                    {
                        string path = DataController.Instance.bodyDatabase.items.FirstOrDefault(item => item.key == key).value;

                        Sprite[] sprites = Resources.LoadAll<Sprite>(path + breed + "NonSet");// Body/AkitaBBad, Body/AkitaNonSet

                        Sprite body;

                        // change key first letter to capital
                        key = "sleep" + key.First().ToString().ToUpper() + key.Substring(1);

                        //Debug.Log("<color=red>" + key + "</color>");

                        body = sprites.FirstOrDefault(s => s.name == key);

                        // Reset full costume slot

                        ResetBody();

                        // Add single costume slot
                        equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("bodyCostume"));//sleepBodyCostume

                        skeletonAnimation.Skeleton.AttachUnitySprite("bodyCostume", body);//sleepBodyCostume 
                    }
                }
            } 
            // long hair cats
            else
            {
                string breed = DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].petBreed;

                if (DataController.Instance.bodyDatabase.items.FirstOrDefault(item => item.key == key).slot == "" || DataController.Instance.bodyDatabase.items.FirstOrDefault(item => item.key == key).slot == null)
                {
                    //Debug.Log("<color=red>SLOT EMPTY!</color>");

                    string path = DataController.Instance.bodyDatabase.items.First(item => item.key == key).value;

                    //Debug.Log("<color=red>" + path + breed + key + "</color>");

                    Sprite[] bodySprites = Resources.LoadAll<Sprite>(path + breed + key);

                    Sprite  SleepRightLegFront,
                    SleepRightLegBack,
                    SleepBody,
                    SleepLeftLegBack,
                    SleepLeftLegFront,
                    Shield;

                    //Debug.Log("<color=red>" + key + "</color>");

                    SleepBody = bodySprites.First(s => s.name == "sleep" + key + "Body");
                    SleepRightLegFront = bodySprites.First(s => s.name == "sleep" + key + "RightLegFront");
                    SleepRightLegBack = bodySprites.First(s => s.name == "sleep" + key + "RightLegBack");
                    SleepLeftLegFront = bodySprites.First(s => s.name == "sleep" + key + "LeftLegFront");
                    SleepLeftLegBack = bodySprites.First(s => s.name == "sleep" + key + "LeftLegBack");
                    Shield = bodySprites.First(s => s.name == key + "Shield");

                    ResetBody();

                    // debug costumes
//                    Debug.Log("<color=redBody costumes>" + SleepBody.name + "</color>");
//                    Debug.Log("<color=redBody costumes>" + SleepRightLegFront.name + "</color>");
//                    Debug.Log("<color=redBody costumes>" + SleepRightLegBack.name + "</color>");
//                    Debug.Log("<color=redBody costumes>" + SleepLeftLegFront.name + "</color>");
//                    Debug.Log("<color=redBody costumes>" + SleepLeftLegBack.name + "</color>");
//                    Debug.Log("<color=redBody costumes>" + Shield.name + "</color>");

                    // Add full costume slot
                    equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("sleepCatainBody"));
                    equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("sleepCatainRightLegFront"));
                    equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("sleepCatainRightLegBack"));
                    equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("sleepCatainLeftLegFront"));
                    equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("sleepCatainLeftLegBack"));
                    equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("shield"));

                    // show if slot is available
//                    Debug.Log("<color=white> slot " + skeletonAnimation.skeleton.FindSlot("sleepCatainBody") + "</color>");
//                    Debug.Log("<color=white> slot " + skeletonAnimation.skeleton.FindSlot("sleepCatainRightLegFront") + "</color>");
//                    Debug.Log("<color=white> slot " + skeletonAnimation.skeleton.FindSlot("sleepCatainRightLegBack") + "</color>");
//                    Debug.Log("<color=white> slot " + skeletonAnimation.skeleton.FindSlot("sleepCatainLeftLegFront") + "</color>");
//                    Debug.Log("<color=white> slot " + skeletonAnimation.skeleton.FindSlot("sleepCatainLeftLegBack") + "</color>");
                    //Debug.Log("<color=white> slot " + skeletonAnimation.skeleton.FindSlot("shield") + "</color>");


                    skeletonAnimation.skeleton.AttachUnitySprite("sleepCatainBody", SleepBody);
                    skeletonAnimation.skeleton.AttachUnitySprite("sleepCatainRightLegFront", SleepRightLegFront);
                    skeletonAnimation.skeleton.AttachUnitySprite("sleepCatainRightLegBack", SleepRightLegBack);
                    skeletonAnimation.skeleton.AttachUnitySprite("sleepCatainLeftLegFront", SleepLeftLegFront);
                    skeletonAnimation.skeleton.AttachUnitySprite("sleepCatainLeftLegBack", SleepLeftLegBack);
                    skeletonAnimation.skeleton.AttachUnitySprite("shield", Shield);
                }
                else
                {
                    string path = DataController.Instance.bodyDatabase.items.FirstOrDefault(item => item.key == key).value;
                    string slotName = "sleepBodyCostume";
                    Sprite[] sprites = Resources.LoadAll<Sprite>(path + breed + "NonSet");// Body/AkitaBBad, Body/AkitaNonSet

                    Sprite body;

                    // change key first letter to capital
                    key = "sleep" + key.First().ToString().ToUpper() + key.Substring(1);

                    //Debug.Log("<color=red>" + key + "</color>");

                    body = sprites.FirstOrDefault(s => s.name == key);

                    // Reset full costume slot

                    ResetBody();

                    // Add single costume slot
                    equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex(slotName));//sleepBodyCostume

                    skeletonAnimation.Skeleton.AttachUnitySprite(slotName, body);//sleepBodyCostume 
                }
            }
        }
        catch(NullReferenceException e)
        {
//            Debug.LogError(e.Message);
        }
    }
    /// <summary>
    /// Cat sleep accessory equipment
    /// </summary>
    void ApplySleepAccessory(string key, string _catBreed)
    {
        try
        {
            if(_catBreed == EnumCollection.CatBreed.Himalayan.ToString() ||
               _catBreed == EnumCollection.CatBreed.Maine_Coon.ToString() ||
               _catBreed == EnumCollection.CatBreed.Munchkin.ToString() || 
               _catBreed == EnumCollection.CatBreed.Persian.ToString() ||
               _catBreed == EnumCollection.CatBreed.Ragdoll.ToString())
            {
                string path = DataController.Instance.AccessoriesDatabase.items.FirstOrDefault(item => item.key == key).value;
//                string slotName = DataController.Instance.AccessoriesDatabase.items.FirstOrDefault(item => item.key == key).slot;
                string slotName = "sleepAccessoryCostume";
                string breed = DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].petBreed;

                Sprite[] accessorySprites = Resources.LoadAll<Sprite>(path + breed + "Accessories");
                Sprite accessorySprite;

                key = "sleep" + key.First().ToString().ToUpper() + key.Substring(1);
                
                //Debug.Log("<color=red>accessory key " + key + "</color>");
                
                accessorySprite = accessorySprites.FirstOrDefault(s => s.name == key);

                // Reset head costume
                ResetAccessories();

                //Debug.Log("Equipped accessories " + equippedAccessoriesList.Count());
                //Debug.Log("slot name " + slotName + " sprite " + accessorySprite.name);
                //Debug.Log("skeleton renderer " + skeletonAnimation.skeleton.GetType());
                // Add head costume slot
                equippedAccessoriesList.Add(skeletonAnimation.skeleton.FindSlotIndex(slotName));

                // Attach Sprite
                skeletonAnimation.skeleton.AttachUnitySprite(slotName, accessorySprite);
            }
            else
            {
                string path = DataController.Instance.AccessoriesDatabase.items.FirstOrDefault(item => item.key == key).value;
                //                string slotName = DataController.Instance.AccessoriesDatabase.items.FirstOrDefault(item => item.key == key).slot;
                string slotName = "accessoryCostume";
                string breed = DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].petBreed;

                Sprite[] accessorySprites = Resources.LoadAll<Sprite>(path + breed + "Accessories");
                Sprite accessorySprite;

//                key = "sleep" + key.First().ToString().ToUpper() + key.Substring(1);

                //Debug.Log("<color=red>accessory key " + key + "</color>");

                accessorySprite = accessorySprites.FirstOrDefault(s => s.name == key);

                // Reset head costume
                ResetAccessories();

                //Debug.Log("Equipped accessories " + equippedAccessoriesList.Count());
                //Debug.Log("slot name " + slotName + " sprite " + accessorySprite.name);
                //Debug.Log("skeleton renderer " + skeletonAnimation.skeleton.GetType());
                // Add head costume slot
                equippedAccessoriesList.Add(skeletonAnimation.skeleton.FindSlotIndex(slotName));

                // Attach Sprite
                skeletonAnimation.skeleton.AttachUnitySprite(slotName, accessorySprite);
            }
        }
        catch(NullReferenceException e)
        {
//            Debug.LogError(e.Message);   
        }
    }

    void ResetHead()
    {
        //Debug.Log("Head Count " + equippedHeadList.Count);

        // Reset slot
        if (equippedHeadList.Count > 0)
        {
            for (int i = 0; i < equippedHeadList.Count; i++)
            {
                skeletonAnimation.skeleton.SetSlotAttachmentToSetupPose(equippedHeadList[i]);
            }                    
        }
        equippedHeadList.Clear();
    }

    void ResetBody()
    {
        //Debug.Log("Body Count " + equippedBodyList.Count);
        
        // Reset slot
        if (equippedBodyList.Count > 0)
        {
            for (int i = 0; i < equippedBodyList.Count; i++)
            {
                skeletonAnimation.skeleton.SetSlotAttachmentToSetupPose(equippedBodyList[i]);
            }                   
        }
        equippedBodyList.Clear();
    }

    void ResetAccessories()
    {
        // Reset slot
        if (equippedAccessoriesList.Count > 0)
        {
            for (int i = 0; i < equippedAccessoriesList.Count; i++)
            {                
                skeletonAnimation.skeleton.SetSlotAttachmentToSetupPose(equippedAccessoriesList[i]);
            }                    
        }
        equippedAccessoriesList.Clear();
    }

    public void UnEquipHead()
    {
        ResetHead();

        // save equipement
        DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].headEquipment = "none";
        GetComponent<Pet>().headEquipment = "none";
    }

    public void UnEquipBody()
    {
        ResetBody();

        // save equipement
        DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].bodyEquipment = "none";
        GetComponent<Pet>().bodyEquipment = "none";
    }

    public void UnEquipAccessories()
    {
        ResetAccessories();

        // save equipement
        DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].accessories = "none";
        GetComponent<Pet>().accessories = "none";
    }

    // attach sprites for swing
    public void AttachSwing()
    {
        for (int i = 0; i < swing.Count; i++)
        {
            //Debug.Log("<color=red>slot " + skeletonAnimation.skeleton.FindSlot(swing[i].slotname) + "</color>");
            skeletonAnimation.skeleton.AttachUnitySprite(swing[i].slotname, swing[i].sprite);
        }
    }
    // dettach sprite for swing
    public void DetachSwing()
    {
        for (int i = 0; i < swing.Count; i++)
        {
            skeletonAnimation.skeleton.SetSlotAttachmentToSetupPose(skeletonAnimation.skeleton.FindSlotIndex(swing[i].slotname));
        }
    }

    public void RemoveAllEquipment()
    {
        ResetAccessories();
        ResetBody();
        ResetHead();
    }

    // ReApplyEquipment to cat, called after sleep for cat
    public void ReApplyEquipment()
    {
        Pet pet = FindObjectOfType<Pet>();
        string accessory = "", head = "", body = "";
        if(pet != null)
        {
            accessory = pet.accessories;
            head = pet.headEquipment;
            body = pet.bodyEquipment;
            ResetAccessories();
            ResetHead();
            ResetBody();
//            // reset pet data
            pet.GetComponent<SkeletonAnimation>().Initialize(true);
            ApplyAccessories(accessory);
            ApplyHead(head);
            ApplyBody(body);
            //Debug.Log("<color=red>Re apply cat equipment</color>");
        }
    }
    // attach sprites for spa
    public void AttachSpa()
    {
        //Debug.Log("Pet category " + EnumCollection.PetCategory.Hairy);
        for (int i = 0; i < spa.Count; i++)
        {
            if (skeletonAnimation.GetComponent<Pet>().petCategory == EnumCollection.PetCategory.Hairy)
                skeletonAnimation.skeleton.AttachUnitySprite(spaSleep[i].slotname, spaSleep[i].sprite);
            else
                skeletonAnimation.skeleton.AttachUnitySprite(spa[i].slotname, spa[i].sprite);
        }
    }
    // dettach sprite for spa
    public void DetachSpa()
    {
        //Debug.Log("Pet category " + EnumCollection.PetCategory.Hairy);
        for (int i = 0; i < spa.Count; i++)
        {
            if (skeletonAnimation.GetComponent<Pet>().petCategory == EnumCollection.PetCategory.Hairy)
                skeletonAnimation.skeleton.SetSlotAttachmentToSetupPose(skeletonAnimation.skeleton.FindSlotIndex(spaSleep[i].slotname));
            else
                skeletonAnimation.skeleton.SetSlotAttachmentToSetupPose(skeletonAnimation.skeleton.FindSlotIndex(spa[i].slotname));
        }
        FindObjectOfType<Pet>().Initialize();
    }
}

[System.Serializable]
public class PetAnim
{
    public string slotname;
    public Sprite sprite;
}
