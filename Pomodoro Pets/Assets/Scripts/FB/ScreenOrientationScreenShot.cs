﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenOrientationScreenShot : MonoBehaviour 
{
	[SerializeField]
	Transform contentToCapture;

	ScreenshotSkeletonAnimationTarget[] animTarget;
	[SerializeField]
	GameObject screenshotControlPanel;


	public void changeScreenOrientation()
	{
		//Debug.Log ("%&^&^&^&");
		DataController.Instance.playerData.isPaused = true;
		Screen.orientation = ScreenOrientation.AutoRotation;
		Screen.orientation = ScreenOrientation.LandscapeLeft;
		//changeScreen ();
		//hide pan arrow
		//HidePan ();


		//RectTransform rt = foreground.GetComponent<RectTransform> ();
//		if (contentToCapture != null) {
//			contentToCapture.localPosition = new Vector2 (0,0);
//		} else {
			contentToCapture.localPosition = new Vector2 (0,0);
			RectTransform rt = GameObject.Find("Foreground").GetComponent<RectTransform> ();
			rt.localScale = new Vector3 (1f, 1f, 1f);
		PetHomeScreenDrag.ins.transform.Find("Midground").GetComponent<RectTransform> ().localScale = rt.localScale;
		PetHomeScreenDrag.ins.transform.Find("Background").GetComponent<RectTransform> ().localScale = rt.localScale;

		//}
		PauseAnimations();

		//06/28/2017 not better
//		screenshotControlPanel.SetActive (false);
//		Invoke ("Screenshot", 10f);
//		Invoke ("EnableScreenShotControlPanel",11f);
	
	}

	void Screenshot()
	{
		//06/28/2017 not better
		//Screenshotter.ins.Screenshot ();
	}

	void EnableScreenShotControlPanel()
	{
		//06/28/2017 not better
		//screenshotControlPanel.SetActive (true);
	}

	public void PauseAnimations()
	{
		animTarget = FindObjectsOfType (typeof(ScreenshotSkeletonAnimationTarget)) as ScreenshotSkeletonAnimationTarget[];
		for(int i=0; i<animTarget.Length; i++)
		{
			animTarget [i].Pause ();
		}
		ThoughtBubble.enable = false;
		FlyingBirdManager.ins.Active = false;
	}

	public void PlayAnimations()
	{
		for(int i=0; i<animTarget.Length; i++)
		{
			animTarget [i].Play ();
		}
		ThoughtBubble.enable = true;
		FlyingBirdManager.ins.Active = true;
	}
	
	public void changeScreenOrientationToPortrait()
	{
		//return back the orientation
		if (Screen.orientation == ScreenOrientation.LandscapeLeft) 
		{
			Screen.orientation = ScreenOrientation.Portrait;

			//show pan arrow
			ShowPan ();

			RectTransform rt = GameObject.Find("Foreground").GetComponent<RectTransform> ();
			rt.localScale = new Vector3 (1, 1, 1);
		}
	}

	public void changeScreen()
	{
		Screen.orientation = ScreenOrientation.Portrait;
		Screen.orientation = ScreenOrientation.LandscapeLeft;
	}


	public void ChangeScreenShotDialogSize()
	{
		//dialog panel
		RectTransform rect  = GameObject.Find ("panelScreenShotDialog").GetComponent<RectTransform>();
		rect.sizeDelta = new Vector2 (500,250);

		//input dialog
		RectTransform rectinput  = GameObject.Find ("inputDescription").GetComponent<RectTransform>();
		rectinput.sizeDelta = new Vector2 (400,120);
		rectinput.localPosition = new Vector2 (0,25);

		//dialog button back
		RectTransform rectback  = GameObject.Find ("btncancel").GetComponent<RectTransform>();
		rectback.sizeDelta = new Vector2 (60,60);
		rectback.localPosition = new Vector2 (-150,-70);

		//dialog button upload
		RectTransform rectupload  = GameObject.Find ("btnupload").GetComponent<RectTransform>();
		rectupload.sizeDelta = new Vector2 (60,60);
		rectupload.localPosition = new Vector2 (150,-70);

	}

	public void HidePan()
	{
		GameObject.Find ("BOT_CONTENT").gameObject.SetActive (false);
	}

	public void ShowPan()
	{
		GameObject.Find ("BOT_CONTENT").gameObject.SetActive (true);
	}

	public void ForceLandscapeLeft()
	{
		#if UNITY_ANDROID && !UNITY_EDITOR
		StartCoroutine(ForceAndFixLandscape());
		#else
		Screen.orientation = ScreenOrientation.LandscapeLeft;
		#endif

	}

	IEnumerator ForceAndFixLandscape()
	{
		ScreenOrientation prev = ScreenOrientation.Portrait;
		for (int i = 0; i < 3; i++)
		{
			Screen.orientation = 
				(prev == ScreenOrientation.Portrait ? ScreenOrientation.LandscapeLeft : ScreenOrientation.Portrait);
			yield return new WaitWhile(() => {
				return prev == Screen.orientation;
			});
			prev = Screen.orientation; 
			yield return new WaitForSeconds(0.5f); //this is an arbitrary wait value -- it may need tweaking for different iPhones!
		}
	}



}
