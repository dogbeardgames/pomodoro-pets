﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;
using System;
using UnityEngine.UI;
using System.IO;


public class FbShare_Pets : MonoBehaviour {


    bool isLogIn = false;


    //hide before taking screenshot
    public GameObject hideGameObject; 

    public int captureWidth = 3840;
    public int captureHeight = 2160;

    bool isTakingScreenShot = false;


    private Texture2D lastResponseTexture;
    private string lastResponse = "";
    private string ApiQuery = "";

    [SerializeField]
    InputField postDescription;





    // Use this for initialization
    void Start () {
        InitFb ();
        //      if (FB.IsLoggedIn) {
        //          isLogIn = true;
        //          //FB.LogInWithReadPermissions(new List<string>() { "public_profile", "email", "user_friends" }, this.HandleResults);
        //          //SharePets ();
        //
        //
        //      } 
    }
    private void CallFBLogin()
    {
        FB.LogInWithReadPermissions(new List<string>() { "public_profile", "email", "user_friends" }, this.HandleResults);
    }

    private void CallFBLoginForPublish()
    {
        // It is generally good behavior to split asking for read and publish
        // permissions rather than ask for them all at once.
        //
        // In your own game, consider postponing this call until the moment
        // you actually need it.
        FB.LogInWithPublishPermissions(new List<string>() { "publish_actions" }, this.HandleResults);

    }

    public void HandleResults(IResult result)
    {


        // if(!isTakingScreenShot)
        //  {
        //  CallFBLoginForPublish ();
        //   StartCoroutine( TakeScreenShot() );
        // }
        //Debug.Log ("Test");
    }

    public void InitFb()
    {

        FB.Init(this.OnInitComplete, this.OnHideUnity);
        //Debug.Log("FB.Init() called with " + FB.AppId);

    }

    private void OnInitComplete()
    {
        //Debug.Log ("Success");

    }

    private void OnHideUnity(bool isGameShown)
    {
        //Debug.Log ("Success");

    }


    public IEnumerator TakeScreenShot()
    {   

        //hide dialogbox before taking screenshot
        // hide optional game object if set
        if (hideGameObject != null) hideGameObject.SetActive(false);

        //InitFb ();
        CallFBLoginForPublish ();
        //Debug.Log("taking screenshot");
        yield return new WaitForEndOfFrame();

        var width = Screen.width;//Screen.width;
        var height = Screen.height;
        var text = new Texture2D((int)width, height, TextureFormat.RGB24, false);

        text.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        text.Apply();

        byte[] screenshot = text.EncodeToPNG();

        //6/28/2017 not better
        //byte[] screenshot = Screenshotter.ins.GetScreenshotOutPut ().EncodeToPNG ();

        var wwwForm = new WWWForm();

        wwwForm.AddBinaryData("image", screenshot, "FacebookShare");//image captured to share
        //wwwForm.AddField ("message", postDescription.gameObject.GetComponent<Text> ().text);//description entered by the user
        wwwForm.AddField ("message", postDescription.text);//description entered by the user

        string destination = Path.Combine(Application.persistentDataPath, "screenshot.png");

        File.WriteAllBytes(destination, screenshot);


        FB.API("me/photos", HttpMethod.POST, Callback, wwwForm);
        /*
        if(isTakingScreenShot)
        {
            isTakingScreenShot = !isTakingScreenShot;
        }
        */
    }


    //function called from a button
    public void ButtonShareScreenshot ()
    {   
        // hide optional game object if set
        if (hideGameObject != null) hideGameObject.SetActive(false);
        StartCoroutine( TakeScreenShot() );
    }

    void Callback(IGraphResult result)
    /*{

        IDictionary<string,object> dict = result.ResultDictionary;
        string fbname = dict ["name"].ToString ();
        //Debug.Log ("FB Name: " + fbname);

    }*/
    {
        lastResponseTexture = null;
        if (result.Error != null) {
            lastResponse = "Error Response:\n" + result.Error;
            //Debug.Log (lastResponse);
            CallPopUpError ();


        } else if (!ApiQuery.Contains ("/picture")) {
            lastResponse = "Success Response:\n" ;
            //Debug.Log (lastResponse);

            //PlainNotification.SendNotification(3, 1, "Pomodoro Pets", "ScreenShot successfully uploaded.", new Color32(0xff, 0x44, 0x44, 255), true, true, true, "app_icon");

            CallPopup ();
        }

        else
        {
            lastResponseTexture = result.Texture;
            lastResponse = "Successssss Response:\n";
            //Debug.Log (lastResponse);

            CallPopup ();
        }
    }    


    //upon clicking camera button
    //all buttons will be hidden except for fb(share) button
    [SerializeField]
    GameObject[] buttonsToHide;


    [SerializeField]
    Button btnFbShare;

    [SerializeField]
    GameObject share;


    public void ReadyScreenshot()
    {

        //Debug.Log ("ScreenSHotter");
        if (DataController.Instance.playerData.isTakingScreenShot) {
            //play sfx
            Framework.SoundManager.Instance.PlaySFX ("seCameraClick");

            for (int i = 0; i < buttonsToHide.Length; i++) {

                buttonsToHide [i].SetActive (false);
            }
            share.SetActive (true);
            DataController.Instance.playerData.isTakingScreenShot = false;

            //panelofFbShare = gameObject.transform.GetChild ();    
        } 
        else
        {
            for (int i = 0; i < buttonsToHide.Length; i++) {
                buttonsToHide [i].SetActive (true);
            }
            share.SetActive (false);
            DataController.Instance.playerData.isTakingScreenShot = true;
        }


    }

    public void LandScapeMode()
    {      
        Screen.orientation = ScreenOrientation.LandscapeLeft;
    }

    void CallPopup()
    {
        GameObject finishTaskPopUps = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), GameObject.Find("Canvas").transform, false);
        ShowDialog dialog = finishTaskPopUps.GetComponent<ShowDialog>();

        dialog.DisableNO ();

        dialog.SetCaption("Shared successfully.");
        dialog.SetConfirmCaption("OK");
        // set events
        dialog.OnYesEvent += () =>
            {
                //Destroy (this.gameObject);    
            };
    }

    void CallPopUpError()
    {
        GameObject finishTaskPopUps = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), GameObject.Find("Canvas").transform, false);
        ShowDialog dialog = finishTaskPopUps.GetComponent<ShowDialog>();

        dialog.DisableNO ();

        dialog.SetCaption("Oops. There seems to be a problem with the connection");
        dialog.SetConfirmCaption("OK");
        // set events
        dialog.OnYesEvent += () =>
            {
                //Destroy (this.gameObject);    
            };
    }

}
