﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnumCollection : MonoBehaviour {

    public enum Pet
    {
        Dog,
        Cat
    };

	public enum Equipment 
    {
        Head, 
        Body, 
        //Shoes, 
        Accessories
    };

	public enum DogBreed
    {
        Beagle,
        Golden_Retriever,
        Husky,
        Pug,
        Yorkie,
        Chow_Chow,
        Chihuahua,
        Poodle,
        Akita,
        Corgi,
        GermanSheperd,
        Rottweiler
    };

    public enum CatBreed
    {
        Persian,
        Abyssinian,
        Siamese,
        Maine_Coon,
        Bengal,
        Sphynx,
        Himalayan,
        Munchkin,
        Ragdoll,
        Bombay
    };

    public enum PomodoroStates
    {
        PomodoroMain,
        TimerScreen,
        BreakScreen,
        LongBreakScreen,
        TaskScreen,
        AddEditTaskScreen,      
        CategoryScreen,
        AddEditCategoryScreen
    }; 

    public enum MaintenanceStates
    {
        Null, 
        Add, 
        Edit, 
        Delete
    }

    public enum MaintenanceButton
    {
        Task, 
        Category
    }

    public enum PetIcon
    {        
        Work, 
        School,
        Workout,
        Play,
        Generic
    }

    public enum PetUIController
    {
        Indoor, 
        Outdoor
    }

    public enum Alert
    {
        Default, 
        Dog_Bark, 
        Cat_Meow
    }

    public enum Theme
    {
        Light,
        Dark       
        //Sepia
    }

	public enum Theme_spanish
	{
		Ligero,
		Oscuro
	}

    public enum Language
    {
        English,
        Spanish,
        Italian,
        French,
        German,
        Potugese,
        Japanese,
        Traditional_Chinese,
        Simplified_Chinese
    }

    public enum Currency
    {
        Gem,
        Gold
    }

    public enum PetCategory
    {
        ShortHair,
        Hairy,
        Big,
        Small
    }

	public enum AchievemenType
	{
		Daily,
		Weekly,
		OneTime
	}      

    public enum FoodItemLevel
    {
        L1, 
        L2, 
        L3, 
        L4, 
        L5, 
        L6, 
        L7, 
        LessThan25, 
        Equal25, 
        GreaterThan25,
        EnergyTreat,
        PowerTreat
    }
}
