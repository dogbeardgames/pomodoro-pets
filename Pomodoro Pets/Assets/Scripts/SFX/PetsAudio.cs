﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PetsAudio : MonoBehaviour {

    public static PetsAudio Instance;            

    void Awake()
    {
        Instance = this;
//        if(Instance == null)
//        {
//            Instance = this;
//            DontDestroyOnLoad(gameObject);
//        }
//        else
//        {
//            Destroy(gameObject);
//        }
    }

	// Use this for initialization
	void Start () 
    {              
	}

    #region METHODS
    // generic button
    public void ButtonSFX()
    {
        Framework.SoundManager.Instance.PlaySFX("seIconTapPets");
    }
    // camera button
    public void CameraButtonSFX()
    {
        Framework.SoundManager.Instance.PlaySFX("seCameraClick");
    }
    // fb button
    public void FBButtonSFX()
    {
        Framework.SoundManager.Instance.PlaySFX("seIconTapPomodoro");
    }
    // pet gauge increase
    public void PetGaugeIncreaseSFX()
    {
        Pet pet = FindObjectOfType<Pet>();
        if (pet != null)
        {
            if (pet.petCategory == EnumCollection.PetCategory.Small)
            {
                Framework.SoundManager.Instance.PlaySFX("seDogBarkSmall");
            }
            else if (pet.petCategory == EnumCollection.PetCategory.Big)
            {
                Framework.SoundManager.Instance.PlaySFX("seDogBarkBig");
            }
            else
            {
                Framework.SoundManager.Instance.PlaySFX("seCatMeow");
            }
        }
    }
    // sleep
    public void PetSleepSFX()
    {
        Framework.SoundManager.Instance.PlaySFX("seSnore", true);
    }
    // stop sleep SFX
    public void StopPetSFX()
    {
        Framework.SoundManager.Instance.StopSFX();
    }
    // scroll
    public void ScrollSFX()
    {
        Framework.SoundManager.Instance.PlaySFX("seItemCarouselShuffle");
    }
    // gold
    public void GoldSFX()
    {
        Framework.SoundManager.Instance.PlaySFX("sePiggyBankInsertCoin");
    }

    // swing
    public void SwingSFX()
    {
        Framework.SoundManager.Instance.PlaySFX("seSqueakyswing", true);   
    }
    //sponge
    public void SpongeSFX()
    {
        Framework.SoundManager.Instance.PlaySFX("seItemCarouselShuffle"); 
    }
    // bubble sfx
    public void BubbleSFX()
    {
        Framework.SoundManager.Instance.PlaySFX("seBubblepop"); 
    }
    // water
    public void WaterSFX()
    {
        Framework.SoundManager.Instance.PlaySFX("seWaterSprinkle", true); 
    }
    #endregion
}
