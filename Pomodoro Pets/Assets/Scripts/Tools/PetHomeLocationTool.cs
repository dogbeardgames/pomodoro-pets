﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif
public class PetHomeLocationTool : MonoBehaviour {

    public PetHomeLocation petHomeLocation;
    public GameObject[] petLocation;
    public int petHomelocationIndex;    	

    #if UNITY_EDITOR

    [ContextMenu("Get Pet Placement")]
    void PetPlacement()
    {
		UnityEditor.EditorUtility.SetDirty (petHomeLocation);
        petHomeLocation.Items[petHomelocationIndex].Position.Clear();
        petHomeLocation.Items[petHomelocationIndex].Rotation.Clear();
        petHomeLocation.Items[petHomelocationIndex].Scale.Clear();
        petHomeLocation.Items[petHomelocationIndex].isSleepPosture.Clear();

        // loop to pet count
        for (int i = 0; i < petLocation.Length; i++)
        {
            petHomeLocation.Items[petHomelocationIndex].Position.Add(petLocation[i].transform.localPosition);
            petHomeLocation.Items[petHomelocationIndex].Rotation.Add(petLocation[i].transform.localEulerAngles);
            petHomeLocation.Items[petHomelocationIndex].Scale.Add(petLocation[i].transform.localScale);
            petHomeLocation.Items[petHomelocationIndex].isSleepPosture.Add(petLocation[i].gameObject.tag == "sleep" ? true : false);
        }

        //Debug.Log("Command done!");
    }

    #endif
}
