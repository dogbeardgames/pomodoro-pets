﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class PetBathTools : MonoBehaviour {

    [SerializeField]
    GameObject Pet;
    [SerializeField]
    DogDatabase dogDB;
    [SerializeField]
    CatDatabase catDB;

    [SerializeField]
    GameObject thoughtBubblePos;

    [SerializeField]
    int petIndex;

	// Use this for initialization
	void Start () {
		
	} 

    #region CONTEXT

    #if UNITY_EDITOR

    [ContextMenu("Dog Pet Collider Data")]
    void DogPetColliderData()
    {
        EditorUtility.SetDirty(dogDB);

        dogDB.items[petIndex].colliderSize = Pet.GetComponent<BoxCollider2D>().size;
        dogDB.items[petIndex].colliderOffset = Pet.GetComponent<BoxCollider2D>().offset;

        //Debug.Log("Dog Pet Collider Done!");
    }

    [ContextMenu("Cat Pet Collider Data")]
    void CatPetColliderData()
    {
        EditorUtility.SetDirty(catDB);

        catDB.items[petIndex].colliderSize = Pet.GetComponent<BoxCollider2D>().size;
        catDB.items[petIndex].colliderOffset = Pet.GetComponent<BoxCollider2D>().offset;

        Debug.Log("Cat Pet Collider Done!");
    }

    [ContextMenu("Dog Bubble Transform Position")]
    void DogBubbleTransformPosition()
    {
        EditorUtility.SetDirty(dogDB);

        int count = Selection.gameObjects.Length;
        if(count == 0)
        {
            Debug.LogError("Select an Object in the hierarchy");
        }
        else
        {
            dogDB.items[petIndex].bubblePosition.Clear();
            for (int i = 0; i < count; i++)
            {
                dogDB.items[petIndex].bubblePosition.Add(Selection.gameObjects[i].transform.localPosition);
            }
            Debug.Log("Dog Bubble Transform Done!");
        }
    }

    [ContextMenu("Cat Bubble Transform Position")]
    void CatBubbleTransformPosition()
    {
        EditorUtility.SetDirty(catDB);

        int count = Selection.gameObjects.Length;
        if(count == 0)
        {
            Debug.LogError("Select an Object in the hierarchy");
        }
        else
        {
            catDB.items[petIndex].bubblePosition.Clear();
            for (int i = 0; i < count; i++)
            {
                catDB.items[petIndex].bubblePosition.Add(Selection.gameObjects[i].transform.localPosition);
            }
            Debug.Log("Cat Bubble Transform Done!");
        }
    }

    [ContextMenu("Dog Thought Bubble Position")]
    void DogThoughtBubble()
    {
        EditorUtility.SetDirty(dogDB);

        dogDB.items[petIndex].thoughtBubblePos = thoughtBubblePos.transform.localPosition;
    }

    [ContextMenu("Cat Thought Bubble Position")]
    void CatThoughtBubble()
    {
        EditorUtility.SetDirty(catDB);

        catDB.items[petIndex].thoughtBubblePos = thoughtBubblePos.transform.localPosition;
    }

    #endif

    #endregion
}
