﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
#endif
public class ResetDataTools : MonoBehaviour {

	//Button
//	[SerializeField]
//	Button btnresetPlayerData;

	//scriptable objects
	[SerializeField]
	TaskDatabase taskdb;

	[SerializeField]
	CategoryDatabase catdb;

	[SerializeField]
	AchievementDatabase achdb;

    [SerializeField]
    DogDatabase dogDB;

    [SerializeField]
    CatDatabase catDB;

    [SerializeField]
    HammerData hammerDB;

    [SerializeField]
    Backgrounds backgroundDB;

    [SerializeField]
    HeadDatabase headDB;

    [SerializeField]
    BodyDatabase bodyDB;

    [SerializeField]
    AcceDatabase accessoriesDB;

    [SerializeField]
    PetPlaythings playThingsDB;

	// Use this for initialization
	void Start () {
		//btnresetPlayerData.GetComponent<Button>().onClick.AddListener (InitializeReset);
//		btnresetPlayerData.GetComponent<Button>().onClick.AddListener(() => InitializeReset());

	}
		
    #if UNITY_EDITOR

    [ContextMenu("Reset Data")]
	void InitializeReset()
	{        
        EditorUtility.SetDirty(taskdb);
        EditorUtility.SetDirty(catdb);
        EditorUtility.SetDirty(achdb);
        EditorUtility.SetDirty(dogDB);
        EditorUtility.SetDirty(hammerDB);
        EditorUtility.SetDirty(backgroundDB);
        EditorUtility.SetDirty(headDB);
        EditorUtility.SetDirty(bodyDB);
        EditorUtility.SetDirty(accessoriesDB);
        EditorUtility.SetDirty(playThingsDB);

        // reset hammer
        for (int i = 0; i < hammerDB.Items.Length; i++)
        {
            if(i != 0)
            {
                hammerDB.Items[i].isOwned = false;  
            }
        }
        // reset background
        for (int i = 0; i < backgroundDB.items.Length; i++)
        {
            if(i != 0)
            {
                backgroundDB.items[i].isOwned = false;  
            }
        }
        // reset accessories
        for (int i = 0; i < accessoriesDB.items.Length; i++)
        {
            accessoriesDB.items[i].isOwned = false;
        }
        // reset body
        for (int i = 0; i < bodyDB.items.Length; i++)
        {
            bodyDB.items[i].isOwned = false;
        }
        // reset head
        for (int i = 0; i < headDB.items.Length; i++)
        {
            headDB.items[i].isOwned = false;
        }
        // reset plaything
        for (int i = 0; i < playThingsDB.items.Length; i++)
        {
            if(i > 1)
            {
                playThingsDB.items[i].isOwned = false;
            }
        }
//		//reset pet count
//		DataController.Instance.playerData.playerPetList.Clear();
//
//		DataController.Instance.playerData.ownedSlot = 1;
//		DataController.Instance.playerData.openSlot = 1;
//		DataController.Instance.playerData.sessionCount = 1;
//		DataController.Instance.playerData.backGround = "Generic";
//		DataController.Instance.playerData.ispetHome = false;
//
//		//currency
//		DataController.Instance.playerData.gold = 0;
//		DataController.Instance.playerData.gem = 0;
//		DataController.Instance.playerData.pigyBank = 0;
//
//
//		DataController.Instance.playerData.goldacquired = 0;
//		DataController.Instance.playerData.goldspent = 0;
//		DataController.Instance.playerData.gemacquired = 0;
//		DataController.Instance.playerData.gemspent = 0;
//
//		DataController.Instance.playerData.targetGoldPerDay = 0;
//
//	
//		DataController.Instance.playerData.DateoftheDay = "none";
//		DataController.Instance.playerData.isnewInstalled = true;
//		DataController.Instance.playerData.isTimerRunning = false; 
//		DataController.Instance.playerData.timeLeftOnExit = 0;
//		DataController.Instance.playerData.sessionRunning = false;    	
//		DataController.Instance.playerData.isTimerExtended = false;
//		DataController.Instance.playerData.timeExtended = 0;
//		DataController.Instance.playerData.isTakingScreenShot = false;
//		DataController.Instance.playerData.isPaused = false;
//		DataController.Instance.playerData.timeState = "";
//		DataController.Instance.playerData.durationTime = 0;
//		DataController.Instance.playerData.breakDuration = 0;
//		DataController.Instance.playerData.goldTimer = 0;
//		DataController.Instance.playerData.isSkipped = false;
//		DataController.Instance.playerData.isTimeExtended = false;
//
//		//use to check if dragging ends
//		DataController.Instance.playerData.isDragEnd = false;
//		DataController.Instance.playerData.isfromIndoor = false;
//		DataController.Instance.playerData.isAddingNewTask = true;
//		DataController.Instance.playerData.datePaused = "none";
//		DataController.Instance.playerData.dateResumed = "none";
//		DataController.Instance.playerData.totalpauseTime = 0;
//		DataController.Instance.playerData.closingState = "";
//		DataController.Instance.playerData.pauseTime = "";
//		DataController.Instance.playerData.isDeleting = false;
//		DataController.Instance.playerData.enableDrag = true;
//		DataController.Instance.playerData.taskpanelChecker = true;
//		DataController.Instance.playerData.savedTime = "";

        // delete playerprefs

        PlayerPrefs.DeleteAll();

		//remove all task
		taskdb.items.Clear ();

		//reset achievements
		for (int i = 0; i < achdb.items.Count; i++) {
			achdb.items [i].isUnlocked = false;
			achdb.items [i].dateLogin = "";

			//Debug.Log ("<color=red>" + "test: "+ achdb.items[i].achievementId + "</color>");
		}
			
		//remove all category
		catdb.items.Clear();

        // refer to EnumCollection
		//add category
		CategoryItem catItem = new CategoryItem();
		catItem.category = "Generic";
		catItem.petIconIndex = 4;
		catItem.isLocked = true;

		CategoryItem catItem1 = new CategoryItem();
		catItem1.category = "Work";
		catItem1.petIconIndex = 0;
		catItem1.isLocked = true;

		CategoryItem catItem2 = new CategoryItem();
		catItem2.category = "School";
		catItem2.petIconIndex = 1;
		catItem2.isLocked = true;

		CategoryItem catItem3 = new CategoryItem();
		catItem3.category = "Workout";
		catItem3.petIconIndex = 2;
		catItem3.isLocked = true;

		CategoryItem catItem4 = new CategoryItem();
		catItem4.category = "Play";
		catItem4.petIconIndex = 3;
		catItem4.isLocked = true;

		catdb.items.Add(catItem);
		catdb.items.Add(catItem1);
		catdb.items.Add(catItem2);
		catdb.items.Add(catItem3);
		catdb.items.Add(catItem4);


        // reset pet owned property
        for (int i = 0; i < dogDB.items.Length; i++)
        {
            dogDB.items[i].isOwned = false;
        }

        // reset pet owned property
        for (int i = 0; i < catDB.items.Length; i++)
        {
            catDB.items[i].isOwned = false;
        }

        Debug.Log("<color=green>Reset data done!</color>");
	}

    [ContextMenu("Delete Player data")]
    void DeletePLayerData()
    {        
        Data.DeleteFile(DataController.filename);
    }

    #endif
}
