﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Backgrounds : ScriptableObject {

    public Background[] items;
}


[System.Serializable]
public class Background
{
    public string name;
    public bool isOwned;
    public Sprite image;
    public bool isEquipped;
}