﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class CategoryDatabase : ScriptableObject
{	
	public List<CategoryItem> items;
}
	
[System.Serializable]
public class CategoryItem
{
	//public EnumCollection.Breeds breedname;
	public string category;	

    public int petIconIndex;

    public bool isLocked;
}