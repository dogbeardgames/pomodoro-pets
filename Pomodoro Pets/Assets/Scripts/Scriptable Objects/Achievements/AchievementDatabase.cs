﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class AchievementDatabase : ScriptableObject
{	
	public List<AchievementItem> items;
}

[System.Serializable]
public class AchievementItem
{
	public string achievementId; //*
	public string achName; //* 
	public string requirements;//*

	public EnumCollection.AchievemenType achievementtype;

	public bool isUnlocked;
	//Sprite image;

	//rewards
	public uint goldReward;
	public uint gemReward;
	public uint pigyReward;
	public uint accessory;
	public uint pet;

	//counter

	public int target_counter;
	public int counter;

	public int target_gold;
	public int target_gem;

	public string dateLogin;

}