﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity.Modules;
using Spine.Unity;

[System.Serializable]
public class DogPet
{
	public EnumCollection.DogBreed breedname;
	public EnumCollection.PetCategory petCategory;
	//public Sprite petImage;
	public string petName;
	public bool isOwned;
	public SkeletonDataAsset skeleton;

	public Vector2 colliderSize, colliderOffset;
	public List<Vector3> bubblePosition;

	public Vector3 thoughtBubblePos;

	//    //Clone constructor
	//    public DogPet(DogPet clone)
	//    {
	//        this.breedname = clone.breedname;
	//        this.petCategory = clone.petCategory;
	//        this.petImage = clone.petImage;
	//        this.petName = clone.petName;
	//        this.isOwned = clone.isOwned;
	//        this.skeleton = clone.skeleton;
	//    }
}