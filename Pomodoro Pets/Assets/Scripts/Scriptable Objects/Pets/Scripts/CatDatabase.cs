﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[System.Serializable]
public class CatDatabase : ScriptableObject
{
    public CatPet[] items;
}  
