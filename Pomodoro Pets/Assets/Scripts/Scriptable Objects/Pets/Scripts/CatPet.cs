﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

[System.Serializable]
public class CatPet
{
    public EnumCollection.CatBreed breedname;
    public EnumCollection.PetCategory petCategory;
    //public Sprite petImage;
    public string petName;
    public bool isOwned;

    public Vector2 colliderSize, colliderOffset;
    public List<Vector3> bubblePosition;

    public SkeletonDataAsset skeleton;

    public Vector3 thoughtBubblePos;

    //Clone constructor
//    public CatPet(CatPet clone)
//    {
//        this.breedname = clone.breedname;
//        this.petCategory = clone.petCategory;
//        //this.petImage = clone.petImage;
//        this.petName = clone.petName;
//        this.isOwned = clone.isOwned;
//        this.skeleton = clone.skeleton;
//    }
}
