﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
//using UnityEditor;
using System.Collections.Generic;

[System.Serializable]
public class DogDatabase : ScriptableObject
{	    
    [SerializeField]
    public DogPet[] items;
}	