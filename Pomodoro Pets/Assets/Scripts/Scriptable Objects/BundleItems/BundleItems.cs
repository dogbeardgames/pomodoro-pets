﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BundleItems : ScriptableObject 
{
    public Bundle_Item[] bundleItems;
}

[System.Serializable]
public class Bundle_Item
{
    public string bundleItemName;
    public bool isOwned;
}

