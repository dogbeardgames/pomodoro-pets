﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PetShopFoodItems : ScriptableObject {

    public PetFoodItem[] item;
}

[System.Serializable]
public class PetFoodItem
{
    public enum FoodType
    {
        Food, Treat
    }

    public string itemName;
    public Sprite image;
    public bool isConsummable;
    public bool isInfinite;
    public uint stock;
    public EnumCollection.FoodItemLevel foodItemLevel;
    public FoodType foodType;
}
