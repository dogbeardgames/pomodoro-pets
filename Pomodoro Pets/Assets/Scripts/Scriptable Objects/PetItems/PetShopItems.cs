﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetShopItems : ScriptableObject {

    public PetItem[] item;
}

[System.Serializable]
public class PetItem
{
    public string itemName;
    public Sprite image;
    public bool isConsummable;
    public bool isInfinite;
    public uint stock;
}
