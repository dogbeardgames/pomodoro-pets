﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

[System.Serializable]
public class TaskDatabase : ScriptableObject
{	
	public List<TaskItem> items;
}
	
[System.Serializable]
public class TaskItem
{
	//public EnumCollection.Breeds breedname;
	public string task;
    public int categoryIndex;
	public DateTime  dateNow = DateTime.Now;
	public DateTime dateFinished = DateTime.Now;
	public int activePetIndex;
    public bool isDone;

    public uint session;

	public float finishDuration;
}

[System.Serializable]
public class TaskDB
{
    public List<TaskItem> items;
}

