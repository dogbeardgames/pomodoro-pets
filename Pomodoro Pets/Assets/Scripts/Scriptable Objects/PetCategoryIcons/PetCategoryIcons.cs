﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetCategoryIcons : ScriptableObject {

    public PetCategoryIconsItem[] item;

}

[System.Serializable]
public class PetCategoryIconsItem
{
    public string petBreed;
    public Sprite none, work, school, workOut, play;
}
