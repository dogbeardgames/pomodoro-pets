﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HammerData : ScriptableObject 
{

    public HammerItem[] Items;
}

[System.Serializable]
public class HammerItem
{
    public string ItemName;
    public bool isOwned;
}
