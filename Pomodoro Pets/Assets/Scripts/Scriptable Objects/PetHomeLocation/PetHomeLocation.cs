﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PetHomeLocation : ScriptableObject {

    public PetHomeLocationData[] Items;
}

[System.Serializable]
public class PetHomeLocationData
{
    public string LocationName;
    public List<Vector3> Position;
    public List<Vector3> Rotation;
    public List<Vector3> Scale;
    public List<bool> isSleepPosture;
}
