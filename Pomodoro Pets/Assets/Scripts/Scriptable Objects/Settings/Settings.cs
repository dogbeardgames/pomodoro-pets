﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Settings : ScriptableObject {

    public readonly float sessionMinDuration = 1f, 
                sessionMaxDuration = 300f;                

    public readonly float shortBreakMinDuration = 3f, 
                shortBreakMaxDuration = 30f;                

    public readonly float longBreakMinDuration = 5f, 
                longBreakMaxDuration = 120f;

    public readonly float minSessionCount = 1, 
                maxSessionCount = 8;

    public readonly float minPreEndAlarmTime = 1, 
                maxPreEndAlarmTime = 10;
                

    public readonly float minVolume = 0, 
                maxVolume = 100; 
                
    public float    sessionDuration,
                    shortBreakDuration,
                    longBreakDuration,
                    sessionCount,
                    alarmTime,
                    volume;

    public bool isPreEndAlarmOn = false;
    public bool isVibrationOn = true;
	public bool isBGMOn = true;
    public int notifAlertIndex = 0;
    public int preEndAlertIndex = 0;
    public int themeIndex = 0;
    public int languageIndex = 0;
}

[System.Serializable]
public class SettingsDB
{
    public readonly float sessionMinDuration = 1f, 
    sessionMaxDuration = 300f;                

    public readonly float shortBreakMinDuration = 3f, 
    shortBreakMaxDuration = 30f;                

    public readonly float longBreakMinDuration = 5f, 
    longBreakMaxDuration = 120f;

    public readonly float minSessionCount = 1, 
    maxSessionCount = 8;

    public readonly float minPreEndAlarmTime = 1, 
    maxPreEndAlarmTime = 10;


    public readonly float minVolume = 0, 
    maxVolume = 100; 

    public float    sessionDuration,
    shortBreakDuration,
    longBreakDuration,
    sessionCount,
    alarmTime,
    volume;

    public bool isPreEndAlarmOn = false;
    public bool isVibrationOn = true;
	public bool isBGMOn = true;

    public int notifAlertIndex = 0;
    public int preEndAlertIndex = 0;
    public int themeIndex = 0;
    public int languageIndex = 0;
}
