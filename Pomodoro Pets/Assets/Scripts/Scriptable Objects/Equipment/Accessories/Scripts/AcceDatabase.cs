﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[System.Serializable]
public class AcceDatabase : ScriptableObject
{
	public Accessories[] items;
}    