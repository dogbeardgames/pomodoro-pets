﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Accessories
{   
    public string EquipmentName;   
    public Sprite image;
    public bool isOwned;

    public string key;
    public string value;
    public string slot;

    public bool isForDog;
    public bool isForCat;

    //Clone constructor
    public Accessories(Accessories clone)
    {
        this.EquipmentName = clone.EquipmentName;
        this.image = clone.image;
        this.isOwned = clone.isOwned;

        this.isForDog = clone.isForDog;
        this.isForCat = clone.isForCat;
    }
}
