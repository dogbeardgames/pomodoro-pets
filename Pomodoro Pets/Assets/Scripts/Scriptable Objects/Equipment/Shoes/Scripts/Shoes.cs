﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Shoes
{    
    public string EquipmentName;
    public Sprite image;
    public bool isOwned;

    //Clone constructor
    public Shoes(Shoes clone)
    {
        this.EquipmentName = clone.EquipmentName;
        this.image = clone.image;
        this.isOwned = clone.isOwned;
    }
}