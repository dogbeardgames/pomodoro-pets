﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class BodyDatabase : ScriptableObject
{
	public BodyEquipment[] items;
}    