﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BodyEquipment
{    
    public string EquipmentName;
    public Sprite image;
    public bool isOwned;

    public string key;
    public string value;
    public string slot;

    public bool isForDog;
    public bool isForCat;

    //Clone constructor
    public BodyEquipment(BodyEquipment clone)
    {
        this.EquipmentName = clone.EquipmentName;
        this.key = clone.key;
        this.image = clone.image;
        this.isOwned = clone.isOwned;

        this.isForDog = clone.isForDog;
        this.isForCat = clone.isForCat;
    }
}
