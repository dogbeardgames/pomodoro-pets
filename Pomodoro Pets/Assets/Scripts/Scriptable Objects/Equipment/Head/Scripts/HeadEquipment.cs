﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HeadEquipment
{    
    public string EquipmentName;
    public Sprite image;
    public bool isOwned;

    public string key;
    public string value;
    public string slot;

    public bool isForDog;
    public bool isForCat;

    //Clone constructor
    public HeadEquipment(HeadEquipment clone)
    {
        this.EquipmentName = clone.EquipmentName;
        this.image = clone.image;
        this.isOwned = clone.isOwned;

        this.isForDog = clone.isForDog;
        this.isForCat = clone.isForCat;
    }
}
