﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[System.Serializable]
public class HeadDatabase : ScriptableObject
{
	public HeadEquipment[] items;
}    