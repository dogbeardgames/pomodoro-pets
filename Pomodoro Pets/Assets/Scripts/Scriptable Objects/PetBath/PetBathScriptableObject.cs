﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PetBathScriptableObject : ScriptableObject {

    public List<PetBathItem> Items;
}

[System.Serializable]
public class PetBathItem
{
    // image used for the store    
    public Sprite image;

    // object to be instatiated when choosed
    public GameObject bathObject;  
}