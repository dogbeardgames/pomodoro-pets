﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LoadingTween : MonoBehaviour {

	// Use this for initialization
	void Start () 
    {		
        
	}		

    void FixedUpdate()
    {
        // rotate icon and loop
        Vector3 rotation = transform.rotation.eulerAngles;
        rotation.z -= Time.deltaTime * 100;
        transform.rotation = Quaternion.Euler(rotation);
    }
}
