﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExclamationPositionTracker : MonoBehaviour {

    Transform tracker;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
        transform.position = tracker.position;
	}

    #region METHODS

    public void SetTracker(Transform _transform)
    {
        tracker = _transform;
    }

    #endregion
}
