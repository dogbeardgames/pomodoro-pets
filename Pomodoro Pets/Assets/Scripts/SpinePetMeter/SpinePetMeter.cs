﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class SpinePetMeter : MonoBehaviour {

    [SerializeField]
    GameObject exclamation, heart;

    public static GameObject exclamationInstance, heartInstance;

    byte heartLoopCounter = 0;

	// Use this for initialization
	void Start () 
    {
        
	}	           

    #region MONO
    // event for pet meter
    void OnEnable()
    {
//        Debug.Log("<color=red>Add Pet Meter Event!</color>");

        //AttributeTimer attributeTimer = FindObjectOfType<AttributeTimer>();
        RotatePawIcon.onPetMeterBelow70 += CallExclamationAnimation;
        RotatePawIcon.onPetMeterAbove70 += DestroyExclamation;
    }

    void OnDisable()
    {
        //AttributeTimer attributeTimer = FindObjectOfType<AttributeTimer>();
        RotatePawIcon.onPetMeterBelow70 -= CallExclamationAnimation;
        RotatePawIcon.onPetMeterAbove70 -= DestroyExclamation;
    }

    #endregion

    #region METHODS

    public void CallExclamationAnimation()
    {
//        Debug.Log("<color=red>Exclamation!</color>");
		if (exclamationInstance == null) 
        {
			exclamationInstance = Instantiate (exclamation);
//            exclamationInstance = Instantiate(exclamation, parent.transform, false);

            exclamationInstance.name = "exclamation";

			exclamationInstance.GetComponent<SkeletonAnimation> ().loop = true;
			exclamationInstance.GetComponent<SkeletonAnimation> ().AnimationName = "animation";
			exclamationInstance.GetComponent<ExclamationPositionTracker> ().SetTracker (GameObject.Find ("_Tracker").transform);
		} 
//        else 
//        {
//			exclamationInstance.SetActive (true);
//		}
    }

    public void CallHeartAnimation()
    {        
        // destroy if exclamation instance is present
        if(exclamationInstance != null)
        {
            //Debug.Log("Heart not null!!!");
            DestroyExclamation();
        }

        if (heartInstance == null)
        {
            heartInstance = Instantiate(heart);
//        heartInstance = Instantiate(heart, parent.transform, false);

            heartInstance.name = "heart";

            heartInstance.GetComponent<SkeletonAnimation>().loop = true;
            heartInstance.GetComponent<SkeletonAnimation>().AnimationName = "animation";
            heartInstance.GetComponent<ExclamationPositionTracker> ().SetTracker (GameObject.Find ("_Tracker").transform);
            // callback once animation is complete
            heartInstance.GetComponent<SkeletonAnimation>().state.Complete += HeartEndAnimationCallback;
        }
    }

    void HeartEndAnimationCallback(Spine.TrackEntry entry)
    {
        heartLoopCounter++;

        //Debug.Log("<color=red>Heart end animation callback, heart loop counter " + heartLoopCounter + "</color>");

        if(heartLoopCounter == 2)
        {
            heartLoopCounter = 0;

            DestroyHeart();
        }            
    }

    void DestroyHeart()
    {        
//        if(exclamationInstance != null)
//        {
//            exclamationInstance.SetActive(true);// kit
//        }

        FindObjectOfType<RotatePawIcon>().UpdatePetMeter();

        DestroyImmediate(heartInstance);
    }

    void DestroyExclamation()
    {
        //Debug.Log("<color=red>Destroy Exclamation!</color>");
        if (exclamationInstance != null)
        {
            //Debug.Log("Destroying exclamation. " + exclamationInstance.name);
            DestroyImmediate(exclamationInstance);
//            exclamationInstance.SetActive(false);

        }            
    }

    #endregion
}
