﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using System;
public class PetActionReward : MonoBehaviour {

    Dictionary<EnumCollection.FoodItemLevel, System.Action> claimRewardDict = new Dictionary<EnumCollection.FoodItemLevel, System.Action>();

    public static PetActionReward Instance;


    void Awake()
    {
        Instance = this;
    }
	[SerializeField]
	Vector2 acquiredTextAppearPos;
	// Use this for initialization
	void Start () {        
        claimRewardDict.Add(EnumCollection.FoodItemLevel.L1, Level1);
        claimRewardDict.Add(EnumCollection.FoodItemLevel.L2, Level2);
        claimRewardDict.Add(EnumCollection.FoodItemLevel.L3, Level3);
        claimRewardDict.Add(EnumCollection.FoodItemLevel.L4, Level4);
        claimRewardDict.Add(EnumCollection.FoodItemLevel.L5, Level5);
        claimRewardDict.Add(EnumCollection.FoodItemLevel.L6, Level6);
        claimRewardDict.Add(EnumCollection.FoodItemLevel.L7, Level7);
        claimRewardDict.Add(EnumCollection.FoodItemLevel.LessThan25, LessThan25);
        claimRewardDict.Add(EnumCollection.FoodItemLevel.Equal25, Equal25);
        claimRewardDict.Add(EnumCollection.FoodItemLevel.GreaterThan25, GreaterThan25);

		//acquiredTextAppearPos = new Vector2((Screen.width/2.4f)*-1, (Screen.height/2) * 0.8f);
	//	Transform screenshot = GameObject.Find ("btnScreenShot").transform;

	//	acquiredTextAppearPos = screenshot.localPosition; //new Vector2 (screenshot.position.x, );
	}	

    public void ClaimReward(EnumCollection.FoodItemLevel foodItemLevel)
    {
        if(claimRewardDict.ContainsKey(foodItemLevel))
        {
            System.Action action = claimRewardDict[foodItemLevel];

            action();
        }
    }

    #region METHODS
    void Level1()
    {
        // audio
        if(PetsAudio.Instance != null)
            PetsAudio.Instance.GoldSFX();
        //effect
//        StartCoroutine(GoldAcquired("4"));
//        GoldAcquired("4");
        GoldAcquiredText("4");
        //add gold acquired
        DataController.Instance.playerData.goldacquired += 4;
        DataController.Instance.playerData.gold = (uint)Mathf.Clamp(DataController.Instance.playerData.gold + 4, 0, 999999999);

    }
    void Level2()
    {
        // audio
        if(PetsAudio.Instance != null)
            PetsAudio.Instance.GoldSFX();
        //effect
//        StartCoroutine(GoldAcquired("7"));
//        GoldAcquired("7");
        GoldAcquiredText("7");
        //add gold acquired
        DataController.Instance.playerData.goldacquired += 7;
        DataController.Instance.playerData.gold = (uint)Mathf.Clamp(DataController.Instance.playerData.gold + 7, 0, 999999999);
    }
    void Level3()
    {
        // audio
        if(PetsAudio.Instance != null)
            PetsAudio.Instance.GoldSFX();
        //effect
//        StartCoroutine(GoldAcquired("21"));
//        GoldAcquired("21");
        GoldAcquiredText("21");
        //add gold acquired
        DataController.Instance.playerData.goldacquired += 21;
        DataController.Instance.playerData.gold = (uint)Mathf.Clamp(DataController.Instance.playerData.gold + 21, 0, 999999999);
    }
    void Level4()
    {
        // audio
        if(PetsAudio.Instance != null)
            PetsAudio.Instance.GoldSFX();
        //effect
//        StartCoroutine(GoldAcquired("29"));
//        GoldAcquired("29");
        GoldAcquiredText("29");
        //add gold acquired
        DataController.Instance.playerData.goldacquired += 29;
        DataController.Instance.playerData.gold = (uint)Mathf.Clamp(DataController.Instance.playerData.gold + 29, 0, 999999999);
    }

    void Level5()
    {
        // audio
        if(PetsAudio.Instance != null)
            PetsAudio.Instance.GoldSFX();
        //effect
//        StartCoroutine(GoldAcquired("32"));
//        GoldAcquired("32");
        GoldAcquiredText("32");
        //add gold acquired
        DataController.Instance.playerData.goldacquired += 32;
        DataController.Instance.playerData.gold = (uint)Mathf.Clamp(DataController.Instance.playerData.gold + 32, 0, 999999999);
    }

    void Level6()
    {
        // audio
        if(PetsAudio.Instance != null)
            PetsAudio.Instance.GoldSFX();
        //effect
//        StartCoroutine(GoldAcquired("64"));
//        GoldAcquired("64");
        GoldAcquiredText("64");
        //add gold acquired
        DataController.Instance.playerData.goldacquired += 64;
        DataController.Instance.playerData.gold = (uint)Mathf.Clamp(DataController.Instance.playerData.gold + 64, 0, 999999999);
    }

    void Level7()
    {
        // audio
        PetsAudio.Instance.GoldSFX();
        //effect
//        StartCoroutine(GoldAcquired("80"));
//        GoldAcquired("80");
        GoldAcquiredText("80");
        //add gold acquired
        DataController.Instance.playerData.goldacquired += 80;
        DataController.Instance.playerData.gold = (uint)Mathf.Clamp(DataController.Instance.playerData.gold + 80, 0, 999999999);
    }

    void LessThan25()
    {
        // audio
        if(PetsAudio.Instance != null)
            PetsAudio.Instance.GoldSFX();
        //effect
//        StartCoroutine(GoldAcquired("5"));
//        GoldAcquired("5");
        GoldAcquiredText("5");
        //add gold acquired
        DataController.Instance.playerData.goldacquired += 5;
        DataController.Instance.playerData.gold = (uint)Mathf.Clamp(DataController.Instance.playerData.gold + 5, 0, 999999999);
    }

    void Equal25()
    {
        // audio
        if(PetsAudio.Instance != null)
            PetsAudio.Instance.GoldSFX();
        //effect
//        StartCoroutine(GoldAcquired("10"));
//        GoldAcquired("10");
        GoldAcquiredText("10");       
        //add gold acquired
        DataController.Instance.playerData.goldacquired += 10;
        DataController.Instance.playerData.gold = (uint)Mathf.Clamp(DataController.Instance.playerData.gold + 10, 0, 999999999);
    }

    void GreaterThan25()
    {
        // audio
        if(PetsAudio.Instance != null)
            PetsAudio.Instance.GoldSFX();
        //effect
//        StartCoroutine(GoldAcquired("15"));
//        GoldAcquired("15");
        GoldAcquiredText("15");
        //add gold acquired
        DataController.Instance.playerData.goldacquired += 15;
        DataController.Instance.playerData.gold = (uint)Mathf.Clamp(DataController.Instance.playerData.gold + 15, 0, 999999999);
    }

    // gold reward for food eaten by pet
    public void FoodGoldReward(uint goldRewardAmount)
    {
        // audio
		if(PetsAudio.Instance != null)
       		 PetsAudio.Instance.GoldSFX();
		
		string strRewardAmount = FormatNumber.GetFormat ((long)goldRewardAmount);//goldRewardAmount.ToString();
        GoldAcquiredText(strRewardAmount);
        //add gold acquired
        DataController.Instance.playerData.goldacquired += goldRewardAmount;
        DataController.Instance.playerData.gold = (uint)Mathf.Clamp(DataController.Instance.playerData.gold + goldRewardAmount, 0, 999999999);
    }

    // gold reward for play things
    public void PlayThingReward(uint goldRewardAmount)
    {
        // audio
        if(PetsAudio.Instance != null)
            PetsAudio.Instance.GoldSFX();

		string strRewardAmount = FormatNumber.GetFormat((long)goldRewardAmount); //goldRewardAmount.ToString();

        GoldAcquiredText(strRewardAmount);
        //add gold acquired
        DataController.Instance.playerData.goldacquired += goldRewardAmount;
        DataController.Instance.playerData.gold = (uint)Mathf.Clamp(DataController.Instance.playerData.gold + goldRewardAmount, 0, 999999999);
    }

    public void UserDefinedGoldReward(uint goldReward)
    {
        // audio
        if(PetsAudio.Instance != null)
            PetsAudio.Instance.GoldSFX();

		string _goldReward = FormatNumber.GetFormat((long)goldReward); //goldReward.ToString();

        GoldAcquiredText(_goldReward);         
        //add gold acquired
        DataController.Instance.playerData.goldacquired += goldReward;
        DataController.Instance.playerData.gold = (uint)Mathf.Clamp(DataController.Instance.playerData.gold += goldReward, 0, 999999998);
    }

    public void UserDefinedGemReward(uint gemReward)
    {
		string _gemReward = FormatNumber.GetFormat ((long)gemReward); //gemReward.ToString();

        GemAcquiredText(_gemReward);
        DataController.Instance.playerData.gem = (uint)Mathf.Clamp(DataController.Instance.playerData.gem + gemReward, 0, 999999999);
    }

    public void ReduceGold(uint goldToReduce)
    {
		string _goldReward = FormatNumber.GetFormat((long)goldToReduce); //goldToReduce.ToString();
        //effect
//        StartCoroutine(GoldReduce(_goldReward));
//        GoldReduce(_goldReward);
        GoldReduceText(_goldReward);
        DataController.Instance.playerData.gold = (uint)Mathf.Clamp(DataController.Instance.playerData.gold - goldToReduce, 0, 999999999);

		//add spent gold to gold spent
		DataController.Instance.playerData.goldspent += goldToReduce;
    }

    public void ReduceGem(uint gemToReduce)
    {
		string _gemReward = FormatNumber.GetFormat ((long)gemToReduce); //gemToReduce.ToString();
//        StartCoroutine(GemReduce(_gemReward));
//        GemReduce(_gemReward);
        GemReduceText(_gemReward);
        DataController.Instance.playerData.gem = (uint)Mathf.Clamp(DataController.Instance.playerData.gem - gemToReduce, 0, 999999999);

		//add spent gold to gold spent
		DataController.Instance.playerData.gemspent += gemToReduce;
    }

//    public void TryLevel()
//    {
//        ClaimReward(EnumCollection.FoodItemLevel.L6);
//    }        

    #endregion

    #region TWEENS
	//_ = not used

    void _GoldAcquiredObject(string strGold)
    {

//        GameObject goldObject = Instantiate(Resources.Load<GameObject>("Prefab/btnGold"), FindObjectOfType<Canvas>().transform, false);
//        goldObject.GetComponent<Button>().interactable = false;
        Gold _gold = FindObjectOfType<Gold>();

        uint currentGold = DataController.Instance.playerData.gold;
        TextMeshProUGUI textMesh = _gold.transform.Find("Text").GetComponent<TextMeshProUGUI>();

        //textMesh.text = currentGold.ToString();

        if(DataController.Instance.playerData.gold < 10000)
        {
            string alteredStrGold = "1";

            uint incremental, gold = uint.Parse(strGold);
            uint remainder;
            int counter = 1;
            while (counter < strGold.Length)
            {
                alteredStrGold = alteredStrGold + "0";
                counter++;
            }

            incremental = uint.Parse(alteredStrGold);
            remainder = gold % incremental;
            //Debug.Log("gold is: " + gold + ", incremental: " + incremental + ", remainder: " + remainder);

            // incremental
            for (uint i = incremental; i <= gold; i += incremental)
            {
                currentGold += incremental;
                textMesh.text = currentGold.ToString();
//                yield return new WaitForSeconds(0.15f);
            }
            // remainder
            if(remainder > 0)
            {
                currentGold += remainder;
                textMesh.text = currentGold.ToString();
            }                
        }

//        yield return new WaitForSeconds(2f); kit huehue
    }

    void _GoldAcquired(string strGold)
    {
//		GameObject goldObject = (GameObject)Instantiate(Resources.Load<GameObject>("Prefab/btnGold"), FindObjectOfType<Canvas>().transform, false);
//        goldObject.GetComponent<Button>().interactable = false;
        uint currentGold = DataController.Instance.playerData.gold;
//        TextMeshProUGUI textMesh = goldObject.transform.Find("Text").GetComponent<TextMeshProUGUI>();

        //textMesh.text = currentGold.ToString();

        if(DataController.Instance.playerData.gold < 10000)
        {
            string alteredStrGold = "1";

            uint incremental, gold = uint.Parse(strGold);
            uint remainder;
            int counter = 1;
            while (counter < strGold.Length)
            {
                alteredStrGold = alteredStrGold + "0";
                counter++;
            }

            incremental = uint.Parse(alteredStrGold);
            remainder = gold % incremental;
            //Debug.Log("gold is: " + gold + ", incremental: " + incremental + ", remainder: " + remainder);

            // incremental
            for (uint i = incremental; i <= gold; i += incremental)
            {
                currentGold += incremental;
//                textMesh.text = currentGold.ToString();
//                yield return new WaitForSeconds(0.15f);
            }
            // remainder
            if(remainder > 0)
            {
                currentGold += remainder;
//                textMesh.text = currentGold.ToString();
            }                
        }

//        yield return new WaitForSeconds(2f); kit huehue
        // tween then destroy

//		goldObject.GetComponent<Image>().DOFade(0, 2).OnComplete(() => Destroy(goldObject));
	
        //gold
//        FindObjectOfType<Gold>();
    }

    void GoldAcquiredText(string gold)
    {
//		//add gold acquired
//		DataController.Instance.playerData.goldacquired += Convert.ToUInt32(gold);

		Transform screenshot = GameObject.Find ("btnScreenShot").transform;
        GameObject tweeningText = Instantiate(Resources.Load<GameObject>("Prefab/tweeningText"), screenshot, false);
        tweeningText.GetComponent<TextMeshProUGUI>().text = "+" + gold;
		tweeningText.transform.localPosition = Vector2.zero;

		tweeningText.transform.localPosition = new Vector2 (0, tweeningText.transform.localPosition.y + (screenshot.GetComponent<RectTransform>().rect.height/2));

		tweeningText.transform.DOLocalMoveY(screenshot.GetComponent<RectTransform>().rect.height * 1.8f, 3, false).OnComplete(() =>
			{
				tweeningText.GetComponent<TextMeshProUGUI>().DOFade(0, 0.0001f).OnComplete(() => Destroy(tweeningText));
				//tweeningText.transform.DOLocalMoveY(target.localPosition.y, 3f, false)
			}
		);
			
    }

    void _GemAcquired(string strGem)
    {
//        GameObject gemObject = Instantiate(Resources.Load<GameObject>("Prefab/btnGems"), FindObjectOfType<Canvas>().transform, false);
//        gemObject.GetComponent<Button>().interactable = false;
        uint currentGem = DataController.Instance.playerData.gem;
//        TextMeshProUGUI textMesh = gemObject.transform.Find("Text").GetComponent<TextMeshProUGUI>();

        //textMesh.text = currentGold.ToString();

        if(DataController.Instance.playerData.gem < 10000)
        {
            string alteredStrGem = "1";

            uint incremental, gem = uint.Parse(strGem);
            uint remainder;
            int counter = 1;
            while (counter < strGem.Length)
            {
                alteredStrGem = alteredStrGem + "0";
                counter++;
            }

            incremental = uint.Parse(alteredStrGem);
            remainder = gem % incremental;
//            Debug.Log("gold is: " + gem + ", incremental: " + incremental + ", remainder: " + remainder);

            // incremental
            for (uint i = incremental; i <= gem; i += incremental)
            {
                currentGem += incremental;
//                textMesh.text = currentGem.ToString();
//                yield return new WaitForSeconds(0.15f);
            }
            // remainder
            if(remainder > 0)
            {
                currentGem += remainder;
//                textMesh.text = currentGem.ToString();
            }                
        }

//        yield return new WaitForSeconds(2f); kit huehue
        // tween then destroy
//        gemObject.GetComponent<Image>().DOFade(0, 1).OnComplete(() => Destroy(gemObject));
    }

    void GemAcquiredText(string gem)
    {
		//add gem acquired
		//changed code from DataController.Instance.playerData.goldacquired
		DataController.Instance.playerData.gemacquired += Convert.ToUInt32(gem);

        Transform screenshot = GameObject.Find ("btnScreenShot").transform;
        GameObject tweeningText = Instantiate(Resources.Load<GameObject>("Prefab/tweeningText"), screenshot, false);
        tweeningText.GetComponent<TextMeshProUGUI>().text = "+" + gem;
        tweeningText.transform.localPosition = Vector2.zero;

		tweeningText.transform.localPosition = new Vector2 (0, tweeningText.transform.localPosition.y + (screenshot.GetComponent<RectTransform>().rect.height/2));

		tweeningText.transform.DOLocalMoveY(screenshot.GetComponent<RectTransform>().rect.height * 1.8f, 3, false).OnComplete(() =>
			{
				tweeningText.GetComponent<TextMeshProUGUI>().DOFade(0, 0.0001f).OnComplete(() => Destroy(tweeningText));
				//tweeningText.transform.DOLocalMoveY(target.localPosition.y, 3f, false)
			}
		);

    }

    void _GoldReduce(string strGold)
    {
        //add gold acquired
        DataController.Instance.playerData.gold -= Convert.ToUInt32(strGold);

//        GameObject goldObject = Instantiate(Resources.Load<GameObject>("Prefab/btnGold"), FindObjectOfType<Canvas>().transform, false);
//        goldObject.GetComponent<Button>().interactable = false;
        uint currentGold = DataController.Instance.playerData.gold;
//        TextMeshProUGUI textMesh = goldObject.transform.Find("Text").GetComponent<TextMeshProUGUI>();

        //textMesh.text = currentGold.ToString();

        if(DataController.Instance.playerData.gold < 10000)
        {
            string alteredStrGold = "1";

            uint decremental, gold = uint.Parse(strGold);
            uint remainder;
            int counter = 1;
            while (counter < strGold.Length)
            {
                alteredStrGold = alteredStrGold + "0";
                counter++;
            }

            decremental = uint.Parse(alteredStrGold);
            remainder = gold % decremental;
            //Debug.Log("gold is: " + gold + ", incremental: " + decremental + ", remainder: " + remainder);

            // decremental
            for (uint i = gold; i > 0; i -= decremental)
            {
                currentGold -= decremental;
//                textMesh.text = currentGold.ToString();
//                yield return new WaitForSeconds(0.15f);
            }
            // remainder
            if(remainder > 0)
            {
                currentGold -= remainder;
//                textMesh.text = currentGold.ToString();
            }                
        }

//        yield return new WaitForSeconds(2f); kit huehue
        // tween then destroy
//        goldObject.GetComponent<Image>().DOFade(0, 1).OnComplete(() => Destroy(goldObject));
    }

    void GoldReduceText(string gold)
    {
        Transform screenshot = GameObject.Find ("btnScreenShot").transform;
        GameObject tweeningText = Instantiate(Resources.Load<GameObject>("Prefab/tweeningText"), screenshot, false);
        tweeningText.GetComponent<TextMeshProUGUI>().text = "-" + gold;
        tweeningText.transform.localPosition = Vector2.zero;

		tweeningText.transform.localPosition = new Vector2 (0, tweeningText.transform.localPosition.y + (screenshot.GetComponent<RectTransform>().rect.height/2));

		tweeningText.transform.DOLocalMoveY(screenshot.GetComponent<RectTransform>().rect.height * 1.8f, 3, false).OnComplete(() =>
			{
				tweeningText.GetComponent<TextMeshProUGUI>().DOFade(0, 0.0001f).OnComplete(() => Destroy(tweeningText));
				//tweeningText.transform.DOLocalMoveY(target.localPosition.y, 3f, false)
			}
		);
    }

    void _GemReduce(string strGem)
    {
        //add gold acquired
        DataController.Instance.playerData.gem -= Convert.ToUInt32(strGem);

//        GameObject gemObject = Instantiate(Resources.Load<GameObject>("Prefab/btnGems"), FindObjectOfType<Canvas>().transform, false);
//        gemObject.GetComponent<Button>().interactable = false;
        uint currentGem = DataController.Instance.playerData.gem;
//        TextMeshProUGUI textMesh = gemObject.transform.Find("Text").GetComponent<TextMeshProUGUI>();

        //textMesh.text = currentGold.ToString();

        if(DataController.Instance.playerData.gem < 10000)
        {
            string alteredStrGem = "1";

            uint decremental, gem = uint.Parse(strGem);
            uint remainder;
            int counter = 1;
            while (counter < strGem.Length)
            {
                alteredStrGem = alteredStrGem + "0";
                counter++;
            }

            decremental = uint.Parse(alteredStrGem);
            remainder = gem % decremental;
            //Debug.Log("gold is: " + gem + ", incremental: " + decremental + ", remainder: " + remainder);

            // decremental
            for (uint i = gem; i > 0; i -= decremental)
            {
                currentGem -= decremental;
//                textMesh.text = currentGem.ToString();
//                yield return new WaitForSeconds(0.15f);
            }
            // remainder
            if(remainder > 0)
            {
                currentGem -= remainder;
//                textMesh.text = currentGem.ToString();
            }                
        }

//        yield return new WaitForSeconds(2f); kit huehue
        // tween then destroy
//        gemObject.GetComponent<Image>().DOFade(0, 1).OnComplete(() => Destroy(gemObject));
    }

    void GemReduceText(string gem)
    {
        Transform screenshot = GameObject.Find ("btnScreenShot").transform;
        GameObject tweeningText = Instantiate(Resources.Load<GameObject>("Prefab/tweeningText"), screenshot, false);
        tweeningText.GetComponent<TextMeshProUGUI>().text = "-" + gem;
        tweeningText.transform.localPosition = Vector2.zero;

		tweeningText.transform.localPosition = new Vector2 (0, tweeningText.transform.localPosition.y + (screenshot.GetComponent<RectTransform>().rect.height/2));

		tweeningText.transform.DOLocalMoveY(screenshot.GetComponent<RectTransform>().rect.height * 1.8f, 3, false).OnComplete(() =>
			{
				tweeningText.GetComponent<TextMeshProUGUI>().DOFade(0, 0.0001f).OnComplete(() => Destroy(tweeningText));
				//tweeningText.transform.DOLocalMoveY(target.localPosition.y, 3f, false)
			}
		);
    }

    #endregion
}  