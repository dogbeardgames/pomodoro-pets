﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Linq;
using TMPro;

public class PetButtonItem : MonoBehaviour{

    //public string itemName;
    public EnumCollection.FoodItemLevel foodItemLevel;
    // food type
    public PetFoodItem.FoodType foodType;
	// Use this for initialization
	void Start () {
		
	}

    #region METHODS
    public void UpdateFoodItem()
    {        
        PetFoodItem petFoodItem = DataController.Instance.foodDatabase.item.First(food => food.itemName == gameObject.name);
        //Debug.Log("Update food item, pet food item " + petFoodItem.itemName + ", stock " + petFoodItem.stock);

        if(!petFoodItem.isInfinite && petFoodItem.stock > 0)
        {
            //Debug.Log("<color=blue>Should update food item</color>");
            transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "x " + petFoodItem.stock;
            GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        }
//        if(!DataController.Instance.foodDatabase.item.First(food => food.isInfinite) && DataController)
    }
    #endregion
}
