﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using Spine;
using Spine.Unity.Modules;
using DG.Tweening;

public class AnimationController : MonoBehaviour {

    SkeletonAnimation skeletonAnimation;

    bool isPlaying = false;
    int loopCounter = 0;

    public System.Action callBack;

    void Awake()
    {
        skeletonAnimation = GetComponent<SkeletonAnimation>();
    }

    public void Initialize()
    {
        skeletonAnimation.Initialize(true);
    }

    public void SetAnimationState()
    {
        skeletonAnimation.AnimationState.Complete += Complete;
    }

    public void Idle()
    {        
        //Debug.Log("Idle");
        skeletonAnimation.loop = true;
        skeletonAnimation.AnimationName = "idle";

        //skeletonAnimation.Initialize(true);

    }
        
    public void Eating()
    {
        isPlaying = true;

        // hide thought bubble
        FindObjectOfType<ThoughtBubbleController>().DisableThoughtBubble();

        skeletonAnimation.loop = true;
        skeletonAnimation.AnimationName = "eating";

    }

    public void Swing()
    {      
        if (GetComponent<Pet>().petType == EnumCollection.Pet.Cat)
        {
            transform.DOMoveY(0.05f, 0.5f).SetEase(Ease.Linear);
        }
        else
        {
            transform.DOMoveY(1f, 0.5f).SetEase(Ease.Linear);
        }

        // hide thought bubble
        FindObjectOfType<ThoughtBubbleController>().DisableThoughtBubble();

        isPlaying = true;

        skeletonAnimation.loop = true;
        skeletonAnimation.AnimationName = "swing";

        // test
        GetComponent<PetCostume>().ReApplyEquipment();

        FindObjectOfType<PetCostume>().AttachSwing();

        // play swing sfx
        PetsAudio.Instance.SwingSFX();

        skeletonAnimation.AnimationState.Complete += Complete;
    }

    public void ShakeHead()
    {
		PetUIController.ACTION = PetUIController.PET_ACTION.FEED;
        isPlaying = true;
        skeletonAnimation.loop = false;
        skeletonAnimation.AnimationName = "shakeHead";
    }

    public void Sleeping()
    {		
        isPlaying = true;
        skeletonAnimation.loop = true;
        skeletonAnimation.AnimationName = "sleeping";

        // audio
        PetsAudio.Instance.PetSleepSFX();
    }

    public void Spa()
    {
        isPlaying = true;
        skeletonAnimation.AnimationName = "spa";
        StartCoroutine("_Spa");
    }

    void Complete(Spine.TrackEntry entry)
    {
        //Debug.Log("<color=red>entry " + entry.Animation.Name + "</color>");
       
        switch (entry.Animation.Name)
        {
            // pet is eating
            case "eating":
                if (loopCounter <= 0)
                {
                    loopCounter++;   
                }
                else
                {
                    // Debug.Log("Done eating");
                    loopCounter = 0;
                    // show thought bubble
                    FindObjectOfType<ThoughtBubbleController>().EnableThoughtBubble();
                    // destroy food
                    Destroy(FindObjectOfType<PetFood>().gameObject);
                    //Update Pet Meter
                    FindObjectOfType<RotatePawIcon>().UpdatePetMeter();
				PetUIController.ACTION = PetUIController.PET_ACTION.NONE;
					Idle();
                    if(callBack != null)
                    {
                        callBack();
                    }
                }
                break;
            // pet is idle
//            case "idle":
//                break;
            // pet shake head
            case "shakeHead":
                // destroy food
                Destroy(FindObjectOfType<PetFood>().gameObject);
				PetUIController.ACTION = PetUIController.PET_ACTION.NONE;
                Idle();
                if (callBack != null)
                    callBack();    
                break;
          /*case "sleeping":
                if(loopCounter <= 1)
                {
                    loopCounter++;
                }
                else
                {
                    loopCounter = 0;
                    Idle();
                }
                break;  */          
            // pet swing
            case "swing":
                if(loopCounter <= 1)
                {
                    loopCounter++;
                    //Debug.Log("<color=red>if... Swing Loop Counter " + loopCounter + "</color>");
                }
                else
                {      
                    //Debug.Log("<color=red>else... Swing Loop Counter " + loopCounter + "</color>");
                    if (GetComponent<Pet>().petType == EnumCollection.Pet.Cat)
                    {
                        // return to normal position
                        transform.DOMoveY(-2.341f, 0.5f).SetEase(Ease.Linear);
                    }
                    else
                    {
                        transform.DOMoveY(-2.341f, 0.5f).SetEase(Ease.Linear);
                    }
                    loopCounter = 0;
                    // show thought bubble
                    FindObjectOfType<ThoughtBubbleController>().EnableThoughtBubble();
                    FindObjectOfType<PetCostume>().DetachSwing();
                    // stop swing sfx
                    PetsAudio.Instance.StopPetSFX();
                    //Update Pet Meter
                    FindObjectOfType<RotatePawIcon>().UpdatePetMeter();
					PetUIController.ACTION = PetUIController.PET_ACTION.NONE;
                    Idle();
                    if(callBack != null)
                    {
                        callBack();
                    }
					
                }
                break;
//            default:
//                break;
        }
        //else if(entry.Animation.Name )
    }

    IEnumerator _Spa()
    {
        Pet pet = GetComponent<Pet>();
		//print ("SPASI");
        // hide thought bubble
        FindObjectOfType<ThoughtBubbleController>().DisableThoughtBubble();
        PetCostume petCostume = FindObjectOfType<PetCostume>();
//        if (pet.petType == EnumCollection.Pet.Dog)
//        {
//            PetCostume petCostume = FindObjectOfType<PetCostume>();
//            if(petCostume != null)
//            {
//                petCostume.RemoveAllEquipment();
//                petCostume.AttachSpa();
//            }
//        }
//        else
//        {
//            PetCostume petCostume = FindObjectOfType<PetCostume>();
//            if(petCostume != null)
//            {
//                petCostume.RemoveAllEquipment();
//            }
//        }
        petCostume.RemoveAllEquipment();
        // sfx
        PetsAudio.Instance.PetSleepSFX();


        for (int i = 0; i < 5; i++)
        {
			while(ScreenshotPetModuleManager.screenshotMode)
			{
				//pause timer on screenshotmode
				yield return new WaitForSeconds (0.2f);
			}
            yield return new WaitForSeconds(1);
        }

        isPlaying = false;
        // sfx off
        PetsAudio.Instance.StopPetSFX();
        // show thought bubble
        FindObjectOfType<ThoughtBubbleController>().EnableThoughtBubble();
//        if (pet.petType == EnumCollection.Pet.Dog)
//        {            
//            PetCostume petCostume = FindObjectOfType<PetCostume>();
//            if(petCostume != null)
//            {
//                petCostume.DetachSpa();
//                petCostume.ReApplyEquipment();
//
//            }
//        }
//        else
//        {
//            PetCostume petCostume = FindObjectOfType<PetCostume>();
//            if(petCostume != null)
//            {                
//                petCostume.ReApplyEquipment();
//
//            }
//        }
        petCostume.ReApplyEquipment();
        //Update Pet Meter
		if(!ScreenshotPetModuleManager.screenshotMode)
        FindObjectOfType<RotatePawIcon>().UpdatePetMeter();
//        Initialize();
		PetUIController.ACTION = PetUIController.PET_ACTION.NONE;
        Idle();

        // Reapply equipment
        GetComponent<PetCostume>().ReApplyEquipment();

        skeletonAnimation.AnimationState.Complete += Complete;

        if(callBack != null)
        {
            callBack();
        }                
    }
}
