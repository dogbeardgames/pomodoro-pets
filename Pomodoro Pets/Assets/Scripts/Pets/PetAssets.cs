﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Spine.Unity;
using System.Linq;

public class PetAssets : MonoBehaviour {

//    public DogDatabase dogDatabase;
//    public CatDatabase catDatabase;
//    public AcceDatabase accessoriesDatabase;
//    public BodyDatabase bodyDatabase;
//    public HeadDatabase headDatabase;

    public GameObject petObject;

    //public GameObject petSelectButton;

    void Start()
    {
        
    }

    #region PetAssets METHODS

    // gets the sprite per pet
    public Sprite PetSprite(int _petTypeIndex, int _index)
    {
        switch (_petTypeIndex)
        {
            case 0:
//                return dogDatabase.items[_index].petImage;
                return null;//DataController.Instance.dogDatabase.items[_index].petImage;
            case 1:
                return null;//DataController.Instance.catDatabase.items[_index].petImage;
            default:
                return null;
        }
    }

    public SkeletonDataAsset PetSpine(int _petTypeIndex, int _index)
    {
        switch (_petTypeIndex)
        {
            case 0:
                return DataController.Instance.dogDatabase.items[_index].skeleton;
            case 1:
                return DataController.Instance.catDatabase.items[_index].skeleton;
            default:
                return null;
        }
    }

    public SkeletonDataAsset PetSpine(EnumCollection.Pet petType, string breedName)
    {
        //Debug.Log("breed name " + breedName);
        SkeletonDataAsset skeletonDataAsset = null;

        switch (petType)
        {
            case EnumCollection.Pet.Dog: 
                skeletonDataAsset = DataController.Instance.dogDatabase.items.First(pet => pet.breedname.ToString() == breedName).skeleton;
                return skeletonDataAsset;
                break;
            case EnumCollection.Pet.Cat:
                skeletonDataAsset = DataController.Instance.catDatabase.items.First(pet => pet.breedname.ToString() == breedName).skeleton;
                return skeletonDataAsset;
                break;
            default:
                return skeletonDataAsset;
                break;
        }
    }

    // players owned pet(s)
    public bool PetIsOwned(int _petTypeIndex, int _index)
    {
        switch (_petTypeIndex)
        {
            case 0:
                return DataController.Instance.dogDatabase.items[_index].isOwned;
            case 1:
                return DataController.Instance.catDatabase.items[_index].isOwned;
            default:
                return false;
        }
    }
        
    public Sprite AccesoriesSprite(int index)
    {
        //return accessoriesDatabase.items[index].image;
        return DataController.Instance.AccessoriesDatabase.items[index].image;
    }

    public Sprite BodySprite(int index)
    {
        //return bodyDatabase.items[index].image;
        return DataController.Instance.bodyDatabase.items[index].image;
    }

    public Sprite HeadSprite(int index)
    {
        return DataController.Instance.headDatabase.items[index].image;
    }

    public bool AccessoryIsOwned(int index)
    {
        //return accessoriesDatabase.items[index].isOwned;
        return DataController.Instance.AccessoriesDatabase.items[index].isOwned;
    }

    public bool BodyEquipmentIsOwned(int index)
    {
        return DataController.Instance.bodyDatabase.items[index].isOwned;
    }

    public bool HeadEquipmentIsOwned(int index)
    {
        return DataController.Instance.headDatabase.items[index].isOwned;
    }        

    public void ClearChildGameObject(Transform parent)
    {
        for (int i = 0; i < parent.childCount; i++)
        {
            Destroy(parent.GetChild(i).gameObject);
        }
    }

    #endregion
}
