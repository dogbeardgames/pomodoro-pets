﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Spine.Unity.Modules;
using Spine.Unity;
using System.Linq;

public class PetController : MonoBehaviour {

    public static PetController Instance;

//    [SerializeField]
//    GameObject 
//        PetStorePanel, //panel for pet selection
//        petDialogBox, // dialog stating "You do not have any pets, please adopt one in the PetStore"
//        parentPetCollection;

    Pet pet;
    //PetStore petStore;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

	// Use this for initialization
	void Start () 
	{
        pet = GetComponent<PetAssets>().petObject.GetComponent<Pet>();
//        // Will change once moldule integration starts
//        if (!Data<PlayerData>.FileExist (DataController.filename)) 
//        {
//			Debug.Log ("No File... Creating one!");
//
//            if(DataController.Instance.playerData.playerPetList.Count == 0 || DataController.Instance.playerData.playerPetList == null)
//            {
//                GetComponent<PetAssets>().petDialogBox.gameObject.SetActive(true);
//                pet.gameObject.SetActive(false);
//            }
//		} 
//        else 
//        {
//            if (DataController.Instance.playerData.playerPetList.Count == 0 || DataController.Instance.playerData.playerPetList == null)
//            {
//                GetComponent<PetAssets>().petDialogBox.gameObject.SetActive(true);
//                pet.gameObject.SetActive(false);
//            }
//            else
//            {
//                //Load player data
//                DataController.Instance.LoadPlayerData();
//                Debug.Log("Pet index " + DataController.Instance.playerData.currentPetIndex);
//            }
//		}

        //ChangePet (playerData.currenPetIndex);
        //ChangePetType();

        //change pet in pet collection if you have one
//        if(DataController.Instance.playerData.currentPetIndex >= 0)
//        {           
//            SetInitialPet(DataController.Instance.playerData.currentPetIndex);
//            Debug.Log("you have " + DataController.Instance.playerData.playerPetList.Count);
//        }
//        else
//        {
//            Debug.Log("No pets owned");
//        }
	}
	
	// Update is called once per frame
//	void Update () {
//		
//	}        

    #region METHODS
    public void SetPet(EnumCollection.Pet petType, string petBreed)
    {
        PetBase petBase = DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex];
        pet.petName = petBase.petName;
        pet.petBreed = petBase.petBreed;
        pet.happiness = petBase.happiness;
        pet.hunger_thirst = petBase.hunger_thirst;
        pet.cleanliness = petBase.cleanliness;
        pet.energy = petBase.energy;
        pet.petType = petBase.petType;
        pet.petCategory = petBase.petCategory;

        pet.isFoodFull = petBase.isFoodFull;

        pet.accessories = petBase.accessories;
        pet.bodyEquipment = petBase.bodyEquipment;
        pet.headEquipment = petBase.headEquipment;

        if(petBase.petType == EnumCollection.Pet.Dog)
        {            
            DogDatabase dogDB = DataController.Instance.dogDatabase;
            DogPet dogPet = dogDB.items.First(_pet => _pet.breedname.ToString() == petBase.petBreed);
            if(dogPet != null)
            {
                // set bath bubble(s)
                pet.bubblePosition = dogPet.bubblePosition;
                // set pet collider
                BoxCollider2D boxCollider2D = pet.GetComponent<BoxCollider2D>();
                boxCollider2D.size = dogPet.colliderSize;
                boxCollider2D.offset = dogPet.colliderOffset;
                // set thought bubble position
                pet.transform.Find("ThoughtBubble").transform.localPosition = dogPet.thoughtBubblePos;
            }
        }
        else
        {
            CatDatabase catDB = DataController.Instance.catDatabase;
            CatPet catPet = catDB.items.First(_pet => _pet.breedname.ToString() == petBase.petBreed);
            if(catPet != null)
            {
                // set bath bubble(s)
                pet.bubblePosition = catPet.bubblePosition;
                // set pet collider
                BoxCollider2D boxCollider2D = pet.GetComponent<BoxCollider2D>();
                boxCollider2D.size = catPet.colliderSize;
                boxCollider2D.offset = catPet.colliderOffset;
                // set thought bubble position
                pet.transform.Find("ThoughtBubble").transform.localPosition = catPet.thoughtBubblePos;
            }
        }

        // pet type
        if(petType == EnumCollection.Pet.Dog)
        {            
            pet.GetComponent<SkeletonAnimation>().skeletonDataAsset = GetComponent<PetAssets>().PetSpine(EnumCollection.Pet.Dog, petBreed);
        }
        else if(petType == EnumCollection.Pet.Cat)
        {
            pet.GetComponent<SkeletonAnimation>().skeletonDataAsset = GetComponent<PetAssets>().PetSpine(EnumCollection.Pet.Cat, petBreed);            
        }

        //create bubble
        FindObjectOfType<PetBathController>().CreateBubbles();

        // set animation and looping;
        //pet.GetComponent<AnimationController>().Idle();

        // initialize spine
        pet.GetComponent<AnimationController>().Initialize();
        pet.GetComponent<AnimationController>().SetAnimationState();

        // Set equipment
        PetCostume petCostume = GetComponent<PetAssets>().petObject.GetComponent<PetCostume>();
        petCostume.ApplyAccessories(pet.accessories);
        petCostume.ApplyHead(pet.headEquipment);
        petCostume.ApplyBody(pet.bodyEquipment);
    }
    #endregion
}
