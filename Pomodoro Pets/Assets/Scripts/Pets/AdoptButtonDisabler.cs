﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdoptButtonDisabler : MonoBehaviour {

    [SerializeField]
    Button[] buttons;

	// Use this for initialization
	void Start () {
		
	}	

    public void DiableButtons()
    {
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].interactable = false;
        }
    }
}
