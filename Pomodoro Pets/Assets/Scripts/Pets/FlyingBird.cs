﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.EventSystems;

public class FlyingBird : MonoBehaviour, IPointerDownHandler {

	[SerializeField]
	Vector2 endPos;

	RectTransform rect;
	[SerializeField]
	GameObject spineGameObject;
	GameObject spineGameObjectRef;

	[SerializeField]
	GameObject gift;
	GameObject giftRef;

	[SerializeField]
	GameObject pointToBird;

	bool clicked;

	[SerializeField]
	Vector2 origin;

	public bool Clicked{
		get{ return clicked;}
	}

	public GameObject spineBird{
		get{ return spineGameObjectRef;}
	}


	void Awake()
	{
		rect = GetComponent<RectTransform> ();	
	}
	void Start()
	{
		//rect = GetComponent<RectTransform> ();	
		//Invoke ("ShowUp",5f);
		//ShowUp();

	}

	void Update()
	{
		//print (Mathf.Abs(Camera.main.transform.position.x - transform.position.x));
		//print ("cam " + Camera.main.transform.position.x + " bird" + transform.position.x);
//		if(!PetHomeController.ins.isOnPetHome && gameObject.activeInHierarchy)
//		{
//			Hide ();
//		}
	}

	void OnEnable()
	{
		//ShowUp();
	}
	void OnDisable()
	{
		//print ("Oi!");
	}

	public void ShowUp()
	{
		//print ("CATCH ME IF YOU CAN!");
		gameObject.SetActive(true);
		clicked = false;
		if (spineGameObjectRef == null) {
			spineGameObject.GetComponent<SetPositionToUIPosition>().SetTarget(transform,false);
			spineGameObjectRef = Instantiate (spineGameObject);
			transform.localPosition = new Vector2 (origin.x, Random.Range(100f,200f));
			//rect.DOLocalMove(new Vector2(endPos.x, transform.localPosition.y), 20f).OnComplete(()=>spineGameObjectRef.SetActive(false));

		} else {
			//print ("DEM!");
			//transform.localPosition = origin;

			transform.localPosition = new Vector2 (origin.x, Random.Range(100f,200f));
			spineGameObjectRef.SetActive (true);

		}
		FlyingBirdManager.ins.birdLocator.SetYPos (transform.localPosition.y);
		rect.DOLocalMove(new Vector2(endPos.x, transform.localPosition.y), 20f).OnComplete(()=> Hide()).SetId("flyingBird");
	}

	public void Hide()
	{
		if (spineGameObjectRef != null) 
		{
			spineGameObjectRef.SetActive (false);
			//gameObject.SetActive (false);
			//FlyingBirdManager.ins.birdLocator.gameObject.SetActive(false);
			FlyingBirdManager.ins.birdLocator.BirdPointer.SetActive(false);
			FlyingBirdManager.ins.birdLocator.activated = false;
			//print ("HIDE!");	
		}
	}


	public void SpawnGift()
	{
		FlyingBirdManager.ins.EnableGiftPanel (true);
		if (giftRef == null) {
			giftRef = (GameObject)Instantiate (gift, transform.position, Quaternion.identity, transform.parent.parent.parent);
			//giftRef.GetComponent<FlyingBirdGift> ().Show ();

		} else {
			giftRef.SetActive (true);


		}
		//giftRef.GetComponent<FlyingBirdGift> ().Show ();
		FlyingBirdManager.ins.flyingBirdGift = giftRef.GetComponent<FlyingBirdGift> ();
		FlyingBirdManager.ins.flyingBirdGift.Show ();
		giftRef.transform.position = transform.position;
		FlyingBirdManager.ins.EnablePetHome (false);
		gift.transform.position = transform.position;
	}









	public void OnPointerDown(PointerEventData data)
	{
		if (!clicked) {
			Framework.SoundManager.Instance.PlaySFX ("seBirdChirp");
			SpawnGift ();
			clicked = true;
		}

	}

		

}
