﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class PetLocation : MonoBehaviour {

    TextMeshProUGUI tmPro;

    Button button;

    public string locationName;

    public static PetLocation petLocation;

	// Use this for initialization
	void Start () {
        button = GetComponent<Button>();
        button.onClick.AddListener(SetApply);

        if(DataController.Instance.playerData.backGround == locationName)
        {
            transform.Find("Image").gameObject.SetActive(true);
        }
        else
        {
            transform.Find("Image").gameObject.SetActive(false);
        }
        if(DataController.Instance.backgroundDatabase.items.First(location => location.name == locationName).isEquipped)
        {                
            petLocation = this;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    #region METHODS
    public void SetApply()
    {        
        if(!DataController.Instance.backgroundDatabase.items.First(location => location.name == locationName).isOwned)
        {
            FindObjectOfType<PetUIController>().Store("toggleCostumes");
        }
        else
        {
            if(!DataController.Instance.backgroundDatabase.items.First(location => location.name == locationName).isEquipped)
            {                
                ChangeLocation();
            }
        }
    }

    void UnEquip()
    {
        transform.Find("Image").gameObject.SetActive(false);
        DataController.Instance.backgroundDatabase.items.First(location => location.name == this.locationName).isEquipped = false;
    }

    public void Unlock()
    {
        GetComponent<Image>().color = new Color32(255, 255, 255, 255);
    }

    public void ChangeLocation()
    {

        //Debug.Log("Change location");
        petLocation.UnEquip();

        petLocation = this;

        transform.Find("Image").gameObject.SetActive(true);

        DataController.Instance.playerData.backGround = this.locationName;
        DataController.Instance.backgroundDatabase.items.First(location => location.name == this.locationName).isEquipped = true;
        PetUIController petUIController = FindObjectOfType<PetUIController>();
        petUIController.SetBackground();
        petUIController.SetAudio();
    }
    #endregion
}
