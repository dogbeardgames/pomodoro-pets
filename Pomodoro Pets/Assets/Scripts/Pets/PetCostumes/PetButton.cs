﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class PetButton : MonoBehaviour {

    public EnumCollection.Equipment costumeType;

    public string key;
    public bool isOwned;
    PetCostume petCostume;
    Button button;   

    public static string equipmentName;
    public static GameObject equippedButtonObject;

	// Use this for initialization
	void Start () {
        petCostume = FindObjectOfType<PetCostume>();
        button = GetComponent<Button>();
        SetApply();
	}	

    #region METHODS
    public void SetApply()
    {
        if (petCostume != null)
        {
            switch (costumeType)
            {
                case EnumCollection.Equipment.Accessories:
                    button.onClick.AddListener(() => 
                        {
                            PetButton petButton = GetComponent<PetButton>();
                            if(petButton.isOwned && (PetButton.equipmentName == gameObject.name))
                            {                     
                                // set caption
                                PetButton.equipmentName = "";
                                // unequip
                                petCostume.UnEquipAccessories();
                                // update button
                                if(PetButton.equippedButtonObject != null)
                                {
                                    PetButton.equippedButtonObject.transform.Find("Equipped").gameObject.SetActive(false);
                                    //Debug.Log("<color=red>previous equip disabled</color>");
                                }
                                transform.Find("Equipped").gameObject.SetActive(false);
                            }
                            else if(petButton.isOwned && (PetButton.equipmentName != gameObject.name))
                            {                                
                                PetButton.equipmentName = gameObject.name;

                                petCostume.ApplyAccessories(key);
                                // update button
                                if(PetButton.equippedButtonObject != null)
                                {
                                    PetButton.equippedButtonObject.transform.Find("Equipped").gameObject.SetActive(false);
                                    //Debug.Log("<color=red>previous equip disabled</color>");
                                }
                                PetButton.equippedButtonObject = gameObject;
                                transform.Find("Equipped").gameObject.SetActive(true);

                                // achievement SUIT
                                AchievementConditions.instance.Suits();
                            }
//                            //change equipment
//                            else if(petButton.key == DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].accessories)
//                            {
//                                petCostume.ApplyAccessories(key, true);
//                            }
                            else
                            {
                                //Debug.Log("Open store Accessories");

//                                List<Button> buttons = new List<Button>();
//                                for (int i = 0; i < transform.parent.childCount; i++) 
//                                {
//                                    if(transform.parent.GetChild(i).gameObject.name == gameObject.name)
//                                    {
//                                        Debug.Log("Found gameobject, name: " + gameObject.name);
//                                        buttons.Add(transform.parent.GetChild(i).GetComponent<Button>());
//                                    }
//                                }
                                FindObjectOfType<PetUIController>().Store("toggleCostumes");
                            }
                        });
                    break;
                case EnumCollection.Equipment.Body:
                    button.onClick.AddListener(() => 
                        {
                            PetButton petButton = GetComponent<PetButton>();
                            if(petButton.isOwned && (PetButton.equipmentName == gameObject.name))
                            {
                                PetButton.equipmentName = "";
                                petCostume.UnEquipBody();
                                // update button
                                if(PetButton.equippedButtonObject != null)
                                {
                                    PetButton.equippedButtonObject.transform.Find("Equipped").gameObject.SetActive(false);
                                    //Debug.Log("<color=red>previous equip disabled</color>");
                                }
                                transform.Find("Equipped").gameObject.SetActive(false);
                            }

                            else if(petButton.isOwned && (PetButton.equipmentName != gameObject.name))
                            {
                                PetButton.equipmentName = gameObject.name;

                                petCostume.ApplyBody(key);
                                // update button
                                if(PetButton.equippedButtonObject != null)
                                {
                                    PetButton.equippedButtonObject.transform.Find("Equipped").gameObject.SetActive(false);
                                    //Debug.Log("<color=red>previous equip disabled</color>");
                                }
                                PetButton.equippedButtonObject = gameObject;
                                transform.Find("Equipped").gameObject.SetActive(true);

                                // achievement SUIT
                                AchievementConditions.instance.Suits();
                            }
                            else
                            {
                                //Debug.Log("Open store Body");


                                FindObjectOfType<PetUIController>().Store("toggleCostumes");
                            }
                        });
                    break;
                case EnumCollection.Equipment.Head:                    
                    button.onClick.AddListener(() =>
                        {
                            PetButton petButton = GetComponent<PetButton>();
                            if(petButton.isOwned && (PetButton.equipmentName == gameObject.name))
                            {
                                PetButton.equipmentName = "";
                                petCostume.UnEquipHead();
                                // update button
                                if(PetButton.equippedButtonObject != null)
                                {
                                    PetButton.equippedButtonObject.transform.Find("Equipped").gameObject.SetActive(false);
                                    //Debug.Log("<color=red>previous equip disabled</color>");
                                }
                                transform.Find("Equipped").gameObject.SetActive(false);
                            }
                            else if(petButton.isOwned && (PetButton.equipmentName != gameObject.name))
                            {
                                PetButton.equipmentName = gameObject.name;

                                petCostume.ApplyHead(key);
                                // update button
                                if(PetButton.equippedButtonObject != null)
                                {
                                    PetButton.equippedButtonObject.transform.Find("Equipped").gameObject.SetActive(false);
                                    //Debug.Log("<color=red>previous equip disabled</color>");
                                }
                                PetButton.equippedButtonObject = gameObject;
                                transform.Find("Equipped").gameObject.SetActive(true);

                                // achievement SUIT
                                AchievementConditions.instance.Suits();
                            }
                            else
                            {
                                //Debug.Log("Open store Head");

//                                List<Button> buttons = new List<Button>();
//                                for (int i = 0; i < transform.parent.childCount; i++) 
//                                {
//                                    if(transform.parent.GetChild(i).gameObject.name == gameObject.name)
//                                    {
//                                        Debug.Log("Found gameobject, name: " + gameObject.name);
//                                        buttons.Add(transform.parent.GetChild(i).GetComponent<Button>());
//                                    }
//                                }
                                FindObjectOfType<PetUIController>().Store("toggleCostumes");
                            }
                        });
                    break;

                default:
                    break;
            }
        }
    }        

    public void Unlock()
    {
        if(petCostume != null)
        {
            switch (costumeType)
            {
                case EnumCollection.Equipment.Accessories:
                    if(DataController.Instance.AccessoriesDatabase.items.First(item => item.key == key).isOwned)
                    {
                        isOwned = true;
                        GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                    }
                    break;
                case EnumCollection.Equipment.Body:
                    if(DataController.Instance.bodyDatabase.items.First(item => item.key == key).isOwned)
                    {
                        isOwned = true;
                        GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                    }
                    break;
                case EnumCollection.Equipment.Head:
                    if(DataController.Instance.headDatabase.items.First(item => item.key == key).isOwned)
                    {
                        isOwned = true;
                        GetComponent<Image>().color = new Color32(255, 255, 255, 255);
                    }
                    break;                 
            }
        }
    }
    #endregion
}
