﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class PetHomePositionTracker : MonoBehaviour {

    public Transform objectToTrack;

    private int m_petIndex;

	// Use this for initialization
	void Start () {
        GetComponent<ThoughtBubble>().petIndex = objectToTrack.GetComponent<PetHomeButton>().petIndex;
        m_petIndex = objectToTrack.GetComponent<PetHomeButton>().petIndex;
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.position = objectToTrack.position;	

        if(objectToTrack.name == "sleep")
        {
            GetComponent<SkeletonAnimation>().AnimationName = "sleeping";
            GetComponent<SkeletonAnimation>().loop = true;
        }else
        {
            GetComponent<SkeletonAnimation>().AnimationName = "idle";
            GetComponent<SkeletonAnimation>().loop = true;
        }
//		if(!PetHomeScreenDrag.ins.IsDraggingToLeftMost())
//		{
//			transform.position = objectToTrack.position;	
//		}

        
	}
}
