﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PetHomePlacementController : MonoBehaviour {

//    [SerializeField]
//    PetHomeLocation petHomeLocation;

	// Use this for initialization
	void Start () {
		
	}	

    #region METHODS

    public void SetPetPlacement(GameObject[] pets)
    {        
        string location = DataController.Instance.playerData.backGround;

        if(DataController.Instance.petHomeLocation.Items.First(loc => loc.LocationName == location) != null)
        {
            PetHomeLocationData petHomeLocationData = DataController.Instance.petHomeLocation.Items.First(loc => loc.LocationName == location);

            //Debug.Log("sleep posture count " + petHomeLocationData.isSleepPosture.Count);

            for (int i = 0; i < pets.Length; i++)
            {
                //Debug.Log("Set placement");

                GameObject pet = pets[i];
                // set name to sleep
                if(petHomeLocationData.isSleepPosture[i])
                {
                    pet.name = "sleep"; 
                }
                // gameObjectPosition
                pet.transform.localPosition = petHomeLocationData.Position[i];
                // gameobjectRotation
                //pet.transform.Rotate(petHomeLocationData.Rotation[i]);
                pet.transform.localRotation = Quaternion.Euler(petHomeLocationData.Rotation[i]);
                //Debug.Log("<color=red>Rotation " + petHomeLocationData.Rotation[i] + "</color>");
            }
        }
    }

    #endregion
}   
