﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PetHomeBackground : MonoBehaviour {

    [SerializeField]
    Image background, midground, foreground;

	// Use this for initialization
	void Start () {
        SetBackground();
	}

    #region METHODS
    public void SetBackground()
    {
        string strBG = DataController.Instance.playerData.backGround;

        background.sprite = Resources.Load<Sprite>("BGMain/" + strBG + "/" + strBG + "Background");
        midground.sprite = Resources.Load<Sprite>("BGMain/" + strBG + "/" + strBG + "Midground");
        foreground.sprite = Resources.Load<Sprite>("BGMain/" + strBG + "/" + strBG + "Foreground");
    }
    #endregion
}
