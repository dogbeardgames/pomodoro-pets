﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PetHomeButton : MonoBehaviour {

    public int petIndex, startTime;

	// Use this for initialization
	void Start () {
        //GetComponent<Button>().onClick.AddListener(SelectPet);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    #region METHODS
    public void SelectPet()
    {

		//Change orientation
		if(Screen.orientation == ScreenOrientation.Landscape)
		{
			//Debug.Log("Changing orientation to portrait");
			Screen.orientation = ScreenOrientation.Portrait;
		}

        // Pass the pet index of the selected pet
        DataController.Instance.playerData.currentPetIndex = petIndex;
        //Debug.Log("Pet index " + petIndex);
        //Change scene to Pets
        //SceneManager.LoadScene("PetsModule", LoadSceneMode.Single);
		PetHomeController.ins.isOnPetHome = false;
		DataController.Instance.playerData.ispetHome = false;
        // ============================================================v
        SceneController.Instance.LoadingScene("PetsModule");


		//Save current break minutes
		if(DataController.Instance.playerData.isTimerRunning && DataController.Instance.playerData.state == EnumCollection.PomodoroStates.BreakScreen)
		{
			//Debug.Log("<color=blue>" +"Time left on exit"+ "</color>");

			int minutetoSeconds = GameObject.Find ("Pomodoro").GetComponent<Timer> ().m_min * 60;

			DataController.Instance.playerData.timeLeftOnExit = GameObject.Find ("Pomodoro").GetComponent<Timer> ().m_sec + minutetoSeconds;

			CheckTimeMaster.instance.SaveDate ();
		}

		//Save current break minutes
		else if(DataController.Instance.playerData.isTimerRunning && DataController.Instance.playerData.state == EnumCollection.PomodoroStates.LongBreakScreen)
		{
			//Debug.Log("<color=blue>" +"Time left on exit"+ "</color>");

			int minutetoSeconds = GameObject.Find ("Pomodoro").GetComponent<Timer> ().m_min * 60;

			DataController.Instance.playerData.timeLeftOnExit = GameObject.Find ("Pomodoro").GetComponent<Timer> ().m_sec + minutetoSeconds;

			CheckTimeMaster.instance.SaveDate ();
		}
		Framework.SoundManager.Instance.PlaySFX ("seIconTapPets");
    }
    #endregion
}
