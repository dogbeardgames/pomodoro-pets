﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PetHomePanButton : MonoBehaviour, IPointerUpHandler, IPointerDownHandler {

	[SerializeField]
	PetHomePanController panController;

	[SerializeField]
	short leftRight; //0=left 1=right
	[SerializeField]
	CustomScrollView scroll;

	[SerializeField]
	bool moving = false;
	[SerializeField]
	float panSpeed = 1f;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (leftRight == 0 && moving) {
			//scrollRect.horizontalNormalizedPosition -= panSpeed * Time.deltaTime;

			if ((scroll.Content.localPosition.x + (panSpeed * Time.deltaTime)) < scroll.MaxLeft) {
				scroll.Content.localPosition = new Vector2 (scroll.Content.localPosition.x + panSpeed * Time.deltaTime, scroll.Content.localPosition.y);
			} else {
				scroll.Content.localPosition = new Vector2 (scroll.MaxLeft, scroll.Content.localPosition.y);
			}
			panController.UpdatePanButtons ();
		} 
		else if (leftRight == 1 && moving)
		{
			
			if ((scroll.Content.localPosition.x - (panSpeed * Time.deltaTime)) > scroll.MaxRight) {
				scroll.Content.localPosition = new Vector2 (scroll.Content.localPosition.x - panSpeed * Time.deltaTime, scroll.Content.localPosition.y);
			} else {
				scroll.Content.localPosition = new Vector2 (scroll.MaxRight, scroll.Content.localPosition.y);
			}
			panController.UpdatePanButtons ();
		}
	}

	void FixedUpdate()
	{	
		
	}
	public void OnPointerDown(PointerEventData eData)
	{
		moving = true;
	}

	public void OnPointerUp(PointerEventData eData)
	{
		moving = false;
	}


	void OnDisable()
	{
		moving = false;
	}
}
