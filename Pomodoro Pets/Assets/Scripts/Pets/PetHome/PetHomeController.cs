﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Framework;
using Spine.Unity;
using System.Linq;

public class PetHomeController : MonoBehaviour {
	public static PetHomeController ins;

    [SerializeField]
    Button swapBtn, goHomeBtn,cancelSwapBtn,storageBtn;

    [SerializeField]
    GameObject swapText;

    [SerializeField]
    GameObject pets,fakePet;  

    [SerializeField]
    Transform scrollViewTransform;

    //[SerializeField]
    public GameObject[] petPlacementObject;

    [SerializeField]

    GameObject[] glows;

	[SerializeField]
	PetsHomeAudioClips petHomeClipsResources;

	public bool isOnPetHome;

    PetCostume pc;
    int currentPos;

    //	public void PlayBGMClip(string name)
    //	{
    //		SoundManager.Instance.BGMInside.clip = petHomeClipsResources.GetBGMClip (name);
    //		SoundManager.Instance.BGMInside.Play();
    //	}


    void Awake()
	{
		ins = this;

        // Set pet location object
        //GetComponent<PetHomePlacementController>().SetPetPlacement(petPlacementObject);
	}


    // for achievements delay
//	IEnumerator DelayTimeForDailyLogin()
//	{
//		yield return new WaitForSeconds (10);
//		try {
//
//			AchievementConditions.instance.DailyLogin();
//		} 
//		catch (System.Exception ex) 
//		{
//
//		}
//
//
//	}

	// Use this for initialization
	void Start () 
    {
        SetPet();
        pc = FindObjectOfType<PetCostume>();
	}
	
	// Update is called once per frame
//	void Update () {
//		
//	}


    //Temp
//    void OnDisable()
//    {
//        DataController.Instance.SavePlayerData();
//    }

    #region METHODS
    public void ShowPetHome()
    {
        // tween the scroll view
		DataController.Instance.playerData.ispetHome = true;
		PetHomeController.ins.isOnPetHome = true;
        scrollViewTransform.transform.DOLocalMoveX(0, 0.75f, false);
		scrollViewTransform.GetChild (0).GetChild (0).GetComponent<RaycastSwipe> ().enabled = true;
    }
	public void ShowPetHomeQuick()
	{
		// tween the scroll view
		DataController.Instance.playerData.ispetHome = true;
		PetHomeController.ins.isOnPetHome = true;
		scrollViewTransform.transform.localPosition = Vector2.zero;// DOLocalMoveX(0, 0.75f, false);
	}


    public void SetPet()
    {
        // delete existing pets
        GameObject[] _pets = GameObject.FindGameObjectsWithTag("Pet");
        for (int i = 0; i < _pets.Count(); i++)
        {
            Destroy(_pets[i].gameObject);
        }

        //Set pet location object
        GetComponent<PetHomePlacementController>().SetPetPlacement(petPlacementObject);

        //print (gameObject.name + " HOLLER!");
        List<GameObject> randomPet = new List<GameObject>(petPlacementObject);

        //int count = DataController.Instance.playerData.playerPetList.Count;
		int count = Mathf.Clamp(DataController.Instance.playerData.playerPetList.Count, 0, 12);
        for (int i = 0; i < DataController.Instance.playerData.playerPetList.Count; i++)
        {
            if(i < 12)
            {
                GameObject pet = Instantiate(pets);
                PetsDisplayed.Add(pet);
                pet.transform.position = petPlacementObject[i].transform.position;
                pet.transform.localRotation = petPlacementObject[i].transform.localRotation;
                pet.GetComponent<PetHomePositionTracker>().objectToTrack = petPlacementObject[i].transform;
                pet.GetComponent<ThoughtBubble>().startTime = petPlacementObject[i].GetComponent<PetHomeButton>().startTime;

                // ==================================================================================================================



                currentPos = i;
                pet.GetComponent<OutdoorsPet>().PetPosition = i;
                pet.GetComponent<OutdoorsCostume>().ApplyHead(DataController.Instance.playerData.playerPetList[i].headEquipment, i);
                pet.GetComponent<OutdoorsCostume>().ApplyBody(DataController.Instance.playerData.playerPetList[i].bodyEquipment, i);
                pet.GetComponent<OutdoorsCostume>().ApplyAccessories(DataController.Instance.playerData.playerPetList[i].accessories, i);

                // ==================================================================================================================

                // set button to interactable
                petPlacementObject[i].GetComponent<Button>().interactable = true;

                // set spine pet skin
                if (DataController.Instance.playerData.playerPetList[i].petType == EnumCollection.Pet.Dog)
                {
                    //Debug.Log("<color=red>dog spine skin</color>");
                    SkeletonAnimation sk = pet.GetComponent<SkeletonAnimation>();
                    sk.skeletonDataAsset =
                        DataController.Instance.dogDatabase.items.First(_pet => _pet.breedname.ToString() == DataController.Instance.playerData.playerPetList[i].petBreed).skeleton;
                    sk.Initialize(true);
                }
                else
                {
                    //Debug.Log("<color=red>cat spine skin</color>");
                    SkeletonAnimation sk = pet.GetComponent<SkeletonAnimation>();
                    sk.skeletonDataAsset =
                        DataController.Instance.catDatabase.items.First(_pet => _pet.breedname.ToString() == DataController.Instance.playerData.playerPetList[i].petBreed).skeleton;
                    sk.Initialize(true);
                }

                // set object to sleep if position is for sleep
                if (petPlacementObject[i].name == "sleep")
                {
                    pet.GetComponent<SkeletonAnimation>().AnimationName = "sleeping";
                    pet.GetComponent<SkeletonAnimation>().loop = true;
                    sleepingPos = true;
                }

                //activates button
                petPlacementObject[i].GetComponent<Button>().interactable = true;

                //compute interval
                if (count % 2 == 0 && count != 2)// even
                    pet.GetComponent<ThoughtBubble>().interval = (count / 2) - 1;
                else if (count == 1 || count == 2)
                    pet.GetComponent<ThoughtBubble>().interval = 1;
                else// odd
                {
                    pet.GetComponent<ThoughtBubble>().interval = (count / 2);
                }
                pet.name = DataController.Instance.playerData.playerPetList[i].petName;

                if (DataController.Instance.playerData.playerPetList[i].petType == EnumCollection.Pet.Dog)
                {
                    if (DataController.Instance.playerData.playerPetList[i].isInStorage)
                    {
                        ps.DogsInStorage.Add(PetsDisplayed[i]);
                        pet.SetActive(false);
                    }
                    else
                    {
                        pet.SetActive(true);
                    }
                }
                else
                {
                    if (DataController.Instance.playerData.playerPetList[i].isInStorage)
                    {
                        ps.CatsInStorage.Add(PetsDisplayed[i]);
                        pet.GetComponent<MeshRenderer>().enabled = false;
                        pet.transform.GetChild(0).gameObject.SetActive(false);
                    }
                    else
                    {
                        pet.SetActive(true);
                        pet.GetComponent<MeshRenderer>().enabled = true;
                        pet.transform.GetChild(0).gameObject.SetActive(true);
                    }
                }
                randomPet.Add(pet);
                Debug.Log(i + " " + DataController.Instance.playerData.playerPetList.Count);
            }
            else
            {
                GameObject pet = Instantiate(pets);
                PetsDisplayed.Add(pet);
                pet.transform.position = petPlacementObject[i].transform.position;
                pet.transform.localRotation = petPlacementObject[i].transform.localRotation;
                pet.GetComponent<PetHomePositionTracker>().objectToTrack = petPlacementObject[i].transform;
                pet.GetComponent<ThoughtBubble>().startTime = petPlacementObject[i].GetComponent<PetHomeButton>().startTime;

                // ==================================================================================================================



                currentPos = i;
                pet.GetComponent<OutdoorsPet>().PetPosition = i;
                pet.GetComponent<OutdoorsCostume>().ApplyHead(DataController.Instance.playerData.playerPetList[i].headEquipment, i);
                pet.GetComponent<OutdoorsCostume>().ApplyBody(DataController.Instance.playerData.playerPetList[i].bodyEquipment, i);
                pet.GetComponent<OutdoorsCostume>().ApplyAccessories(DataController.Instance.playerData.playerPetList[i].accessories, i);

                // ==================================================================================================================

                // set button to interactable
                petPlacementObject[i].GetComponent<Button>().interactable = true;

                // set spine pet skin
                if (DataController.Instance.playerData.playerPetList[i].petType == EnumCollection.Pet.Dog)
                {
                    //Debug.Log("<color=red>dog spine skin</color>");
                    ps.DogsInStorage.Add(PetsDisplayed[i]);
                    SkeletonAnimation sk = pet.GetComponent<SkeletonAnimation>();
                    sk.skeletonDataAsset =
                        DataController.Instance.dogDatabase.items.First(_pet => _pet.breedname.ToString() == DataController.Instance.playerData.playerPetList[i].petBreed).skeleton;
                    sk.Initialize(true);
                }
                else
                {
                    //Debug.Log("<color=red>cat spine skin</color>");
                    ps.CatsInStorage.Add(PetsDisplayed[i]);
                    SkeletonAnimation sk = pet.GetComponent<SkeletonAnimation>();
                    sk.skeletonDataAsset =
                        DataController.Instance.catDatabase.items.First(_pet => _pet.breedname.ToString() == DataController.Instance.playerData.playerPetList[i].petBreed).skeleton;
                    sk.Initialize(true);
                }

                // set object to sleep if position is for sleep
                if (petPlacementObject[i].name == "sleep")
                {
                    pet.GetComponent<SkeletonAnimation>().AnimationName = "sleeping";
                    pet.GetComponent<SkeletonAnimation>().loop = true;
                    sleepingPos = true;
                }

                //activates button
                petPlacementObject[i].GetComponent<Button>().interactable = true;

                //compute interval
                if (count % 2 == 0 && count != 2)// even
                    pet.GetComponent<ThoughtBubble>().interval = (count / 2) - 1;
                else if (count == 1 || count == 2)
                    pet.GetComponent<ThoughtBubble>().interval = 1;
                else// odd
                {
                    pet.GetComponent<ThoughtBubble>().interval = (count / 2);
                }
                pet.name = DataController.Instance.playerData.playerPetList[i].petName;

                //if (DataController.Instance.playerData.playerPetList[i].petType == EnumCollection.Pet.Dog)
                //{
                //    if (DataController.Instance.playerData.playerPetList[i].isInStorage)
                //    {
                //        pet.SetActive(false);
                //    }
                //    else
                //    {
                //        pet.SetActive(true);
                //    }
                //}
                //else
                //{
                //    if (DataController.Instance.playerData.playerPetList[i].isInStorage)
                //    {
                //        ps.CatsInStorage.Add(PetsDisplayed[i]);
                //        pet.GetComponent<MeshRenderer>().enabled = false;
                //        pet.transform.GetChild(0).gameObject.SetActive(false);
                //    }
                //    else
                //    {
                //        pet.SetActive(true);
                //        pet.GetComponent<MeshRenderer>().enabled = true;
                //        pet.transform.GetChild(0).gameObject.SetActive(true);
                //    }
                //}
                randomPet.Add(pet);
            }
        }

        //for (int i = 13; i < DataController.Instance.playerData.playerPetList.Count; i++)
        //{
        //    
        //    GameObject pet = Instantiate(pets);
        //    PetsDisplayed.Add(pet);
        //    pet.transform.position = petPlacementObject[i].transform.position;
        //    pet.transform.localRotation = petPlacementObject[i].transform.localRotation;
        //    pet.GetComponent<PetHomePositionTracker>().objectToTrack = petPlacementObject[i].transform;
        //    pet.GetComponent<ThoughtBubble>().startTime = petPlacementObject[i].GetComponent<PetHomeButton>().startTime;

        //    currentPos = i;
        //    pet.GetComponent<OutdoorsPet>().PetPosition = i;
        //    pet.GetComponent<OutdoorsCostume>().ApplyHead(DataController.Instance.playerData.playerPetList[i].headEquipment, i);
        //    pet.GetComponent<OutdoorsCostume>().ApplyBody(DataController.Instance.playerData.playerPetList[i].bodyEquipment, i);
        //    pet.GetComponent<OutdoorsCostume>().ApplyAccessories(DataController.Instance.playerData.playerPetList[i].accessories, i);

        //    pet.name = DataController.Instance.playerData.playerPetList[i].petName;

        //    if (DataController.Instance.playerData.playerPetList[i].petType == EnumCollection.Pet.Dog)
        //    {
        //        //Debug.Log("<color=red>dog spine skin</color>");
        //        ps.DogsInStorage.Add(PetsDisplayed[i]);
        //        SkeletonAnimation sk = pet.GetComponent<SkeletonAnimation>();
        //        sk.skeletonDataAsset =
        //            DataController.Instance.dogDatabase.items.First(_pet => _pet.breedname.ToString() == DataController.Instance.playerData.playerPetList[i].petBreed).skeleton;
        //        sk.Initialize(true);
        //    }
        //    else
        //    {
        //        ps.CatsInStorage.Add(PetsDisplayed[i]);
        //        //Debug.Log("<color=red>cat spine skin</color>");
        //        SkeletonAnimation sk = pet.GetComponent<SkeletonAnimation>();
        //        sk.skeletonDataAsset =
        //            DataController.Instance.catDatabase.items.First(_pet => _pet.breedname.ToString() == DataController.Instance.playerData.playerPetList[i].petBreed).skeleton;
        //        sk.Initialize(true);
        //    }
            

        //    if (count % 2 == 0 && count != 2)// even
        //        pet.GetComponent<ThoughtBubble>().interval = (count / 2) - 1;
        //    else if (count == 1 || count == 2)
        //        pet.GetComponent<ThoughtBubble>().interval = 1;
        //    else// odd
        //    {
        //        pet.GetComponent<ThoughtBubble>().interval = (count / 2);
        //    }
        //    //
        //    pet.SetActive(false);
        //}
        //Debug.Log("count / 2 = " + (count / 2) + ", remaining " + (count % 2));

        //print("CUSTOM SCROLL EDITED THIS");
    }

    [Header("Storage")]

    public PetStorage ps;
    public bool sendToStorage;

    [Header("Swap Variables")]
    public int curSelected;
    public int curDestination;
    bool sleepingPos;

    public List<GameObject> PetsDisplayed = new List<GameObject>();

    public bool haveCurSelected;
    bool swapReady;
    
    public void GoHome()
    {
        PetsDisplayed[curSelected].GetComponent<PetHomePositionTracker>().objectToTrack.GetComponent<PetHomeButton>().SelectPet();
    }

    public void OnPetClick(GameObject place)
    {
        if(ps.SetInStorage)
        {
            curSelected = place.GetComponent<PetHomeButton>().petIndex;
            PetsDisplayed[curSelected].SetActive(false);
            
            if(DataController.Instance.playerData.playerPetList[curSelected].petType.ToString() == "Dog")
            {
                DataController.Instance.playerData.playerPetList[curSelected].isInStorage = true;
                ps.DogsInStorage.Add(PetsDisplayed[curSelected]);
            }
            else
            {
                DataController.Instance.playerData.playerPetList[curSelected].isInStorage = true;
                ps.CatsInStorage.Add(PetsDisplayed[curSelected]);
            }
            ps.SetInStorage = false;
        }
        else
        {
            if (haveCurSelected)
            {
                if (swapReady)
                {
                    if(sendToStorage)
                    { 
                        curDestination = place.GetComponent<PetHomeButton>().petIndex;

                        fromStorageSwap();
                    }
                    else
                    {
                        curDestination = place.GetComponent<PetHomeButton>().petIndex;
                        if (curDestination != curSelected)
                        {
                            SwapProcess();
                        }
                    }
                }
            }
            else
            {
                haveCurSelected = true;
                curSelected = place.GetComponent<PetHomeButton>().petIndex;
                storageBtn.gameObject.SetActive(false);
                swapBtn.gameObject.SetActive(true);
                goHomeBtn.gameObject.SetActive(true);
                swapReady = false;
            }
        }
    }

    public void Swap()
    {
        int count = Mathf.Clamp(DataController.Instance.playerData.playerPetList.Count, 0, 12);
        if(count >= 2)
        {
            swapReady = true;
            swapBtn.gameObject.SetActive(false);
            goHomeBtn.gameObject.SetActive(false);
            storageBtn.gameObject.SetActive(false);
            cancelSwapBtn.gameObject.SetActive(true);
            swapText.SetActive(true);


            for (int i = 0; i < /*petPlacementObject.Length*/ count; i++)
            {
                if (sendToStorage)
                {
                    glows[i].SetActive(true);
                    glows[i].transform.position = new Vector3(petPlacementObject[i].transform.position.x, petPlacementObject[i].transform.position.y /*+ 1.8f*/, petPlacementObject[i].transform.position.z);
                }
                else
                {
                    if (i != curSelected)
                    {
                        glows[i].SetActive(true);
                        glows[i].transform.position = new Vector3(petPlacementObject[i].transform.position.x, petPlacementObject[i].transform.position.y /*+ 1.8f*/, petPlacementObject[i].transform.position.z);
                    }
                }
            }
            foreach (GameObject tb in PetsDisplayed)
            {
                tb.transform.GetChild(0).gameObject.SetActive(false);
            }
        }

    }

    public void CancelSwap()
    {
        foreach (GameObject tb in PetsDisplayed)
        {
            tb.transform.GetChild(0).gameObject.SetActive(true);
        }
        storageBtn.gameObject.SetActive(true);
        haveCurSelected = false;
        swapReady = false;
        sendToStorage = false;
        swapText.SetActive(false);
        cancelSwapBtn.gameObject.SetActive(false);
        for (int i = 0; i < petPlacementObject.Length; i++)
        {
            glows[i].SetActive(false);
        }
    }

    void SwapProcess()
    {
        PetBase tempPetListIndex = DataController.Instance.playerData.playerPetList[curSelected];
        DataController.Instance.playerData.playerPetList[curSelected] = DataController.Instance.playerData.playerPetList[curDestination];
        DataController.Instance.playerData.playerPetList[curDestination] = tempPetListIndex;

        Transform tempObject = PetsDisplayed[curSelected].GetComponent<PetHomePositionTracker>().objectToTrack;
        PetsDisplayed[curSelected].GetComponent<PetHomePositionTracker>().objectToTrack = PetsDisplayed[curDestination].GetComponent<PetHomePositionTracker>().objectToTrack;
        PetsDisplayed[curDestination].GetComponent<PetHomePositionTracker>().objectToTrack = tempObject;
        
        GameObject petDisplayIndex = PetsDisplayed[curSelected];
        PetsDisplayed[curSelected] = PetsDisplayed[curDestination];
        PetsDisplayed[curDestination] = petDisplayIndex;

        CancelSwap();
        haveCurSelected = false;

    }

    void fromStorageSwap()
    {
        if (DataController.Instance.playerData.playerPetList[curSelected].petType.ToString() == "Dog")
        {
            ps.DogsInStorage.Remove(ps.DogsInStorage.Find(x => x.name.Contains(PetsDisplayed[curSelected].name)));
        }
        else
        {
            ps.CatsInStorage.Remove(ps.CatsInStorage.Find(x => x.name.Contains(PetsDisplayed[curSelected].name)));
        }

        if (DataController.Instance.playerData.playerPetList[curDestination].petType.ToString() == "Dog")
        {
            ps.DogsInStorage.Add(PetsDisplayed[curDestination]);
        }
        else
        {
            ps.CatsInStorage.Add(PetsDisplayed[curDestination]);
        }

        PetBase tempPetListIndex = DataController.Instance.playerData.playerPetList[curSelected];
        DataController.Instance.playerData.playerPetList[curSelected] = DataController.Instance.playerData.playerPetList[curDestination];
        DataController.Instance.playerData.playerPetList[curDestination] = tempPetListIndex;

        DataController.Instance.playerData.playerPetList[curSelected].isInStorage = true;
        DataController.Instance.playerData.playerPetList[curDestination].isInStorage = false;

        Transform tempObject = PetsDisplayed[curSelected].GetComponent<PetHomePositionTracker>().objectToTrack;
        PetsDisplayed[curSelected].GetComponent<PetHomePositionTracker>().objectToTrack = PetsDisplayed[curDestination].GetComponent<PetHomePositionTracker>().objectToTrack;
        PetsDisplayed[curDestination].GetComponent<PetHomePositionTracker>().objectToTrack = tempObject;

        GameObject petDisplayIndex = PetsDisplayed[curSelected];
        PetsDisplayed[curSelected] = PetsDisplayed[curDestination];
        PetsDisplayed[curDestination] = petDisplayIndex;

        PetsDisplayed[curDestination].SetActive(true);
        PetsDisplayed[curSelected].SetActive(false);
        
        if (curSelected == curDestination)
        {
            PetsDisplayed[curSelected].SetActive(true);
            PetsDisplayed[curDestination].SetActive(true);
            if (DataController.Instance.playerData.playerPetList[curSelected].petType.ToString() == "Dog")
            {
                ps.DogsInStorage.Remove(ps.DogsInStorage.Find(x => x.name.Contains(PetsDisplayed[curSelected].name)));
            }
            else
            {
                ps.CatsInStorage.Remove(ps.CatsInStorage.Find(x => x.name.Contains(PetsDisplayed[curSelected].name)));
            }
        }
        
        CancelSwap();
        haveCurSelected = false;
    }
    #endregion
}
