﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PetBathController : MonoBehaviour {

    public static PetBathController Instance;

    public enum Bathstate {Sponge, Hose}; 

    [SerializeField]
    GameObject bubbles;

    public Bathstate bathState;

    public GameObject hose, sponge;

    [SerializeField]
    Pet pet;

    public List<GameObject> bubblesObj;

	// Use this for initialization
	void Start () 
    {
        Instance = this;
	}	

    #region METHODS

    public void CreateBubbles()
    {
        Bubble bubble = FindObjectOfType<Bubble>();

        if (bubble == null)
        {
            List<Vector3> bubblesPos = pet.bubblePosition;

            for (int i = 0; i < bubblesPos.Count; i++)
            {
                GameObject bubbleObject = Instantiate(bubbles, pet.transform);
                bubbleObject.name = "bubble";
                bubbleObject.transform.localPosition = bubblesPos[i];
                bubbleObject.transform.localScale = new Vector3(1, 1, 1);

                bubblesObj.Add(bubbleObject);
            }
        }
    }

    public void AddBubbleCounterScript()
    {
        pet.gameObject.AddComponent<PetBubbleCounter>();
    }

    /// <summary>
    /// Checks if there is a bubble particle playing
    /// </summary>
    /// <returns><c>true</c>, if there is a bubble particle playing, <c>false</c> otherwise.</returns>
    public bool BubbleCounter()
    {
        for (int i = 0; i < bubblesObj.Count; i++)
        {
            if(!bubblesObj[i].GetComponent<ParticleSystem>().isPlaying)
            {
                //Debug.Log("<color=red>Particle not playing</color>");
                return false;
            }
        }
            
        //Debug.Log("<color=red>All particles playing!</color>");
        return true;
    }

    /// <summary>
    /// Determines whether this instance is playing bubble.
    /// </summary>
    /// <returns><c>true</c> if this instance is playing bubble; otherwise, <c>false</c>.</returns>
    public bool IsBubblePlaying()
    {
        for (int i = 0; i < bubblesObj.Count; i++)
        {
            if(bubblesObj[i].GetComponent<ParticleSystem>().isPlaying)
            {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Stops all bubble particles.
    /// </summary>
    public void StopAllBubbleParticles()
    {
        Destroy(FindObjectOfType<PetBubbleCounter>());
        for (int i = 0; i < bubblesObj.Count; i++)
        {
            bubblesObj[i].GetComponent<ParticleSystem>().Stop();
            bubblesObj[i].GetComponent<ParticleSystem>().Clear(true);
        }
    }

    public void CheckBubbleExisting(System.Action action, Toggle selectedToggle)
    {        
        if (FindObjectOfType<PetBathController>().IsBubblePlaying())
        {            
            // Confirm dialog
            GameObject confirmDialogObj = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), FindObjectOfType<Canvas>().transform , false);
            ShowDialog confirmDialog = confirmDialogObj.GetComponent<ShowDialog>();
            confirmDialog.SetCaption("Are you sure you want to leave pet without rinsing?");
            confirmDialog.SetConfirmCaption("Yes");
            confirmDialog.SetCancelCaption("No");
            // confirm button;
            confirmDialog.OnYesEvent += () =>
                {
                    FindObjectOfType<PetBathController>().StopAllBubbleParticles();
//                    FindObjectOfType<PetUIController>().Indoor();

                    if(FindObjectOfType<PetBathBase>() != null)
                    {
                        Destroy(FindObjectOfType<PetBathBase>().gameObject);
                    }

                    // play passed action
                    if(action != null)
                    {
                        action();
                    }
                    Destroy(confirmDialogObj);
                };
            // cancel button;
            confirmDialog.OnNoEvent += () =>
                {
                    // turn off pressed toggle
                    selectedToggle.isOn = false;
                    Destroy(confirmDialogObj);
                };
        }
        else
        {
//            FindObjectOfType<PetUIController>().Indoor();
            // play passed action
            if(action != null)
            {
                action();
            }
        }
    }

    #endregion
}
