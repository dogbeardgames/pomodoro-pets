﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.SceneManagement;
//
//public class ButtonClock : MonoBehaviour {
//
//	public void LoadMainScene()
//	{
//		DataController.Instance.playerData.isfromIndoor = true;
//		DataController.Instance.playerData.ispetHome = false;
//		SceneController.Instance.LoadingScene ("Main");
//
//		CheckTimeMaster.instance.CheckDate ();
//
//
//	}
//
//	void Start()
//	{
//		//Debug.Log ("<color=blue>" + "Your are in petsHome" + "</color>");
//		if (DataController.Instance.playerData.state == EnumCollection.PomodoroStates.BreakScreen || DataController.Instance.playerData.state == EnumCollection.PomodoroStates.LongBreakScreen)
//		{
//			StartCoroutine ("BreakTimerUp", 10);
//		}
//	}
//
//	IEnumerator BreakTimerUp(float time)
//	{
//		while (time > 0) 
//		{
//			Debug.Log (time--);
//			yield return new WaitForSeconds (1);
//			DataController.Instance.playerData.timeLeftOnExit = (int)time;
//
//		}
//
//		//Debug.Log ("Break Complete");
//		//Call popup
//		CallPopUp();
//	}
//
//	void CallPopUp()
//	{
//		GameObject window = Instantiate (Resources.Load<GameObject>("Prefab/StoreDialog"),GameObject.Find("Canvas").transform, false);
//		ShowDialog dialog = window.GetComponent <ShowDialog>();
//		dialog.SetCaption ("Break session finished.");
//		dialog.SetConfirmCaption ("Ok");
//		dialog.DisableNO ();
//		dialog.OnYesEvent += delegate {
//			DataController.Instance.playerData.ispetHome = false;
//			SceneController.Instance.LoadingScene ("Main");
//			if (DataController.Instance.playerData.state == EnumCollection.PomodoroStates.BreakScreen)
//			{
//				DataController.Instance.playerData.state = EnumCollection.PomodoroStates.TimerScreen;
//			}
//			else if (DataController.Instance.playerData.state == EnumCollection.PomodoroStates.LongBreakScreen)
//			{
//				DataController.Instance.playerData.currentSessionCount = 1;
//				DataController.Instance.playerData.state = EnumCollection.PomodoroStates.TaskScreen;
//			}
//		};
//	}
//}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonClock : MonoBehaviour {

	public void LoadMainScene()
	{
		
		DataController.Instance.playerData.isfromIndoor = true;
		DataController.Instance.playerData.ispetHome = false;
		SceneController.Instance.LoadingScene ("Main");

		CheckTimeMaster.instance.CheckDate ();


	}

	void Start()
	{
		//Debug.Log ("<color=blue>" + "Your are in petsHome" + "</color>");
		if (DataController.Instance.playerData.state == EnumCollection.PomodoroStates.BreakScreen || DataController.Instance.playerData.state == EnumCollection.PomodoroStates.LongBreakScreen)
		{
			StartCoroutine ("BreakTimerUp",DataController.Instance.playerData.timeLeftOnExit);
		}
	}

	IEnumerator BreakTimerUp(float time)
	{
		while (time > 0) 
		{
			//Debug.Log (time--);
			yield return new WaitForSeconds (1);
			DataController.Instance.playerData.timeLeftOnExit = (int)time;


			// show pre end alarm warning
			float breakTime = 0;
			if(DataController.Instance.settings.isPreEndAlarmOn == true)
			{
				if (DataController.Instance.playerData.state == EnumCollection.PomodoroStates.BreakScreen)
				{
					breakTime = DataController.Instance.settings.alarmTime * 60;
					if(time==breakTime)
					{
						CallPopUpPreEndAlarmTime (time);
						////Debug.LogError("short breaktimerup");
					}
				}
				else if (DataController.Instance.playerData.state == EnumCollection.PomodoroStates.LongBreakScreen)
				{
					breakTime = DataController.Instance.settings.alarmTime * 60;
					if(time==breakTime)
					{
						CallPopUpPreEndAlarmTime (time);
						////Debug.LogError("long breaktimerup");
					}
				}
			}


		}
		//Debug.Log ("Break Complete");
		//Call popup
		CallPopUp();
	}

	void CallPopUp()
	{


		if (GameObject.Find ("StoreDialog(Clone)") != null) {
			Destroy (GameObject.Find ("StoreDialog(Clone)"));
		}

		//Debug.LogError("CallPopup");
		GameObject window = Instantiate (Resources.Load<GameObject>("Prefab/StoreDialog"),GameObject.Find("Canvas").transform, false);
		ShowDialog dialog = window.GetComponent <ShowDialog>();
		dialog.SetConfirmCaption ("OK");

		if (DataController.Instance.playerData.state == EnumCollection.PomodoroStates.BreakScreen)
		{
			dialog.SetCaption ("Your short break session is over.");
		}
		else if (DataController.Instance.playerData.state == EnumCollection.PomodoroStates.LongBreakScreen)
		{
			dialog.SetCaption ("Your long break session is over.");
		}
		dialog.DisableNO ();
		dialog.OnYesEvent += delegate {

			DataController.Instance.playerData.ispetHome = false;
			SceneController.Instance.LoadingScene ("Main");
			if (DataController.Instance.playerData.state == EnumCollection.PomodoroStates.BreakScreen)
			{
				DataController.Instance.playerData.state = EnumCollection.PomodoroStates.TimerScreen;
			}
			else if (DataController.Instance.playerData.state == EnumCollection.PomodoroStates.LongBreakScreen)
			{
			    DataController.Instance.playerData.currentSessionCount = 1;
				DataController.Instance.playerData.state = EnumCollection.PomodoroStates.TaskScreen;

			}


			if (DataController.Instance.playerData.currentSessionCount == 1) {
				DataController.Instance.playerData.isTimerRunning = false;
			}
		};


	}


	void CallPopUpPreEndAlarmTime(float time)
	{
		time = DataController.Instance.settings.alarmTime;

		GameObject window = Instantiate (Resources.Load<GameObject>("Prefab/StoreDialog"),GameObject.Find("Canvas").transform, false);
		ShowDialog dialog = window.GetComponent <ShowDialog>();
		if (DataController.Instance.playerData.state == EnumCollection.PomodoroStates.BreakScreen)
		{
			dialog.SetCaption ("You have " + time + " " + timeFormat(time) + " left in your short break session.");
		}
		else if (DataController.Instance.playerData.state == EnumCollection.PomodoroStates.LongBreakScreen)
		{
			dialog.SetCaption ("You have " + time + " " + timeFormat(time) + " left in your long break session.");
		}

		dialog.SetConfirmCaption ("OK");
		dialog.DisableNO ();
		dialog.OnYesEvent += delegate {

		};
	}

	private string timeFormat(float time)
	{
		string format = "";
		if (time > 1) 
		{
			format = "minutes";
		} 
		else 
		{
			format = "minute";
		}
		return format;
	}
}
