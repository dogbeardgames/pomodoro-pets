﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI;

public class PetUIController : MonoBehaviour {

    public static PetUIController Instance;

    float offset;

    bool isTransitioning = false;

    [SerializeField]
    Toggle toggleBath, toggleFood, toggleSleep, togglePlayPet, toggleCostume;

    [SerializeField]
    Sprite lampOn, lampOff;

    private Toggle SelectedToggle;

    [SerializeField]
    ToggleGroup costumeToggleGroup;

    Vector3 bottomPanelPos;

    [SerializeField]
    RectTransform   bottomMenuPanel, 
                    costumePanel, 
                    horizontalContent, 
                    horizontalICT;

    [SerializeField]
    Button btnStore;

    [SerializeField]
    GameObject  item, 
                location, 
                food, 
                storePanel, 
                objFoodItem, 
                playThings;                

    [HideInInspector]
    GameObject playThingObject = null;

    [SerializeField]
    PetBathController petBathController;

    [SerializeField]
    Transform btnToInside, btnToOutside;

    ToggleGroup toggleGroup;

    SpriteRenderer bgIndoor, bgOutdoor, bgIndoorSleep;

    EnumCollection.PetUIController petUIState = EnumCollection.PetUIController.Outdoor;
	// Use this for initialization

	public enum PET_ACTION { NONE, FEED, SLEEP, SPONGE, HOSE, BALL, SWING, SPA };
	public static PET_ACTION ACTION;

	[SerializeField]
	AnimationController animCntrlr;
	[SerializeField]
	GameObject[] blockers;

	void Start () {

        Instance = this;

        btnStore.onClick.AddListener(() => Store(""));

        //Find Sprite Renderer first
        bgIndoor = GameObject.Find("BackGroundIndoor").GetComponent<SpriteRenderer>();
        bgIndoorSleep = GameObject.Find("BackGroundIndoorSleep").GetComponent<SpriteRenderer>();
        bgOutdoor = GameObject.Find("BackGroundOutdoor").GetComponent<SpriteRenderer>();
        toggleGroup = GameObject.Find("BottomMenuPanel").GetComponent<ToggleGroup>();

        // btnToInSide/btnToOutSide
        btnToInside.GetComponent<Button>().onClick.AddListener(Indoor);
        btnToOutside.GetComponent<Button>().onClick.AddListener(Outdoor);

        offset = bottomMenuPanel.GetComponent<RectTransform>().rect.height * 3;
        bottomPanelPos = bottomMenuPanel.transform.position;

        // set background
        SetBackground();

        SetAudio();

        PetIndoor(0, 1);     
		if (petSleepController == null) {
			petSleepController = GameObject.Find ("btnPetSleep").GetComponent<PetSleep> ();
		}

        // repeat rate edited by Kit, previous value = 1f
		CheckFirstSleepOnceOnStart();
		if (petSleepLamp == null) {
			petSleepLamp = GameObject.Find ("btnPetSleep").GetComponent<Image> ();
		}

	}
	
	// Update is called once per frame
//	void Update () {
//		
//	}

	public void EscapeFromScreenshot()
	{
		bool b = false;

		if (ACTION == PET_ACTION.SWING || ACTION == PET_ACTION.SPA) {
			b = true;
		}	
		for(int i=0; i<blockers.Length; i++)
		{
			blockers [i].SetActive (b);
		}
	}

	public void EnableBlockers(bool b)
	{
		
		for(int i=0; i<blockers.Length; i++)
		{
			blockers [i].SetActive (b);
		}

	}

    #region METHODS

    // set background, check bg in datacontroller, find sprite in resource folder, pass to sprite renderer
    public void SetBackground()
    {
        //Debug.Log("Set location " + DataController.Instance.playerData.backGround);
        string strBG = DataController.Instance.playerData.backGround;
        Sprite[] bg = Resources.LoadAll<Sprite>("BG/" + strBG + "/" + strBG + "BG");
        
        bgIndoor.sprite = bg.First(_bg => _bg.name == strBG + "Inside");
        bgIndoorSleep.sprite = bg.First(_bg => _bg.name == strBG + "InsideLampOff");
        bgOutdoor.sprite = bg.First(_bg => _bg.name == strBG + "Outside");
    }

    public void SetAudio()
    {
        if (!Framework.SoundManager.Instance.isBGMPlaying("bgm" + DataController.Instance.playerData.backGround + "InOut") && DataController.Instance.settings.isBGMOn)            
        {
            //Debug.Log("<color=red>PLAY BGM!!!</color>");
            Framework.SoundManager.Instance.PlayBGM("bgm" + DataController.Instance.playerData.backGround + "InOut", true);
//            Framework.SoundManager.Instance.PlayOutsideBGM("bgm" + DataController.Instance.playerData.backGround + "InOut", true);
        }
    }

    public void Indoor()
    {
        if(!isTransitioning)
        {
            PetIndoor(0.5f, 0.5f);
            ChangeBackgroundToIndoor();
            ClearChildItems();
        }
    }

    public void Outdoor()
    {
        if(!isTransitioning)
        {
            PetOutdoor(0.5f, 0.5f);
            ChangeBackgroundToOutdoor();
            ClearChildItems();
        }
    }

    void PetIndoor(float exitDuration, float enterDuration)
    {       
        // mute outside bgm
//      Framework.SoundManager.Instance.FadeOutOutside();
//		Framework.SoundManager.Instance.FadeInInside ();

        if (petUIState != EnumCollection.PetUIController.Indoor)
        {                      
            petUIState = EnumCollection.PetUIController.Indoor;

//            Vector3 pos = bottomMenuPanel.transform.position;
            Vector3 pos = bottomPanelPos;

            pos.y -= offset;

            bottomMenuPanel.DOMoveY(pos.y, exitDuration, false).OnComplete(() =>
                {
                    //activate designated buttons
                    toggleFood.gameObject.SetActive(true);
                    toggleSleep.gameObject.SetActive(true);

                    toggleBath.gameObject.SetActive(false);
                    togglePlayPet.gameObject.SetActive(false);

                    pos.y += offset;
                    bottomMenuPanel.DOMoveY(pos.y, enterDuration, false);
                }); 
        }
    }

    void PetOutdoor(float exitDuration, float enterDuration)
    {
        // unmute outside bgm
//      Framework.SoundManager.Instance.FadeInOutside();
//		Framework.SoundManager.Instance.FadeOutInside ();

        if(petUIState != EnumCollection.PetUIController.Outdoor)
        {                     
            petUIState = EnumCollection.PetUIController.Outdoor;
            
//            Vector3 pos = bottomMenuPanel.transform.position;
            Vector3 pos = bottomPanelPos;

            pos.y -= offset;

            bottomMenuPanel.DOMoveY(pos.y, exitDuration, false).OnComplete(() =>
                {
                    //activate designated buttons
                    toggleFood.gameObject.SetActive(false);
                    toggleSleep.gameObject.SetActive(false);

                    toggleBath.gameObject.SetActive(true);
                    togglePlayPet.gameObject.SetActive(true);

                    pos.y += offset;
                    bottomMenuPanel.DOMoveY(pos.y, enterDuration, false);
                }); 
        }
    }

    void ChangeBackgroundToIndoor()
    {
        isTransitioning = true;

        btnToInside.gameObject.SetActive(false);

        bgIndoor.transform.DOMoveX(0, 1, false).OnComplete(() =>
            {
                isTransitioning = false;
                // enable btnToOutSide
                btnToOutside.gameObject.SetActive(true);
            });
        
        bgOutdoor.transform.DOMoveX(7.52f, 1, false);
        ClearChildItems();
        horizontalContent.gameObject.SetActive(false);
        horizontalICT.gameObject.SetActive(false);

        if(SelectedToggle != null)
            SelectedToggle.isOn = false;

        SelectedToggle = null;
    }

    void ChangeBackgroundToOutdoor()
    {
        isTransitioning = true;

        btnToOutside.gameObject.SetActive(false);

        bgIndoor.transform.DOMoveX(-7.52f, 1, false).OnComplete(() =>
            {
                isTransitioning = false;
                // enable btnToInSide
                btnToInside.gameObject.SetActive(true);
            });
        
        bgOutdoor.transform.DOMoveX(0f, 1, false);
        ClearChildItems();
        horizontalContent.gameObject.SetActive(false);
        horizontalICT.gameObject.SetActive(false);

        if(SelectedToggle != null)
            SelectedToggle.isOn = false;
        
        SelectedToggle = null;
    }

    public void ToggleChange(string ItemType)
    {
        // reset to default scroll setings
        horizontalICT.Find("HorizontalCT").GetComponent<ScrollRect>().enabled = true;

        switch (ItemType)
        {
            case "Food":  
                SelectedToggle = toggleFood;
                if(SelectedToggle.isOn)
                {
                    Food();
                }
                else
                {
                    DisablePanel();
                    SelectedToggle = null;
                }
                break;
            case "Bath":
                SelectedToggle = toggleBath;
                if(SelectedToggle.isOn)
                {
                    Bath();
                }
                else
                {
                    DisablePanel();
                    SelectedToggle = null;
                }
                break;
            case "Play":
                // Outside, check if in bath state
			//print("SPONGE");
                SelectedToggle = togglePlayPet;
                if(SelectedToggle.isOn)
                {
                    PetBathController.Instance.CheckBubbleExisting(Play, SelectedToggle);
                }
                else
                {
                    DisablePanel();
                    SelectedToggle = null;
                }
                break;
            case "Costume":  
                // Outside, check if in bath state
                //Debug.Log("Costume toggle " + SelectedToggle.isOn);
                SelectedToggle = toggleCostume;
                if (SelectedToggle.isOn)
                {                            
                    PetBathController.Instance.CheckBubbleExisting(Costume, SelectedToggle);
                }
                else
                {
                    DisablePanel();
                    SelectedToggle = null;
                }                        
                break;
            case "Sleep":
                SelectedToggle = toggleSleep;
                if(SelectedToggle.isOn)
                {
                    Sleep();
                }
                break;
            default:
                break;
        }
    }

    void Food()
    {               

        ClearChildItems();

        horizontalICT.gameObject.SetActive(true);
        horizontalContent.gameObject.SetActive(true);       

//        for (int i = 0; i < 3; i++)
//        {
//            GameObject foodItem = Instantiate(item, horizontalContent, false);
//            foodItem.name = "Food Item " + (i + 1);
//            foodItem.transform.Find("Text").GetComponent<Text>().text = "Food Item " + (i + 1);
//        }
        // food items
        for (int i = 0; i < DataController.Instance.foodDatabase.item.Count(); i++)
        {
			
            GameObject foodItem = Instantiate(food, horizontalContent, false);
            foodItem.name = DataController.Instance.foodDatabase.item[i].itemName;
            foodItem.GetComponent<Image>().sprite = DataController.Instance.foodDatabase.item[i].image;
            foodItem.GetComponent<PetButtonItem>().foodItemLevel = DataController.Instance.foodDatabase.item[i].foodItemLevel; 
            foodItem.GetComponent<PetButtonItem>().foodType = DataController.Instance.foodDatabase.item[i].foodType;

            //if(DataController.Instance.foodDatabase.item[i].stock == 0 && !DataController.Instance.foodDatabase.item[i].isInfinite)
            if(DataController.Instance.foodDatabase.item[i].stock == 0 && !DataController.Instance.foodDatabase.item[i].isInfinite)
            {
                //Debug.Log("stock is 0");
                foodItem.GetComponent<Image>().color = new Color32(255, 255, 255, 128);
                foodItem.GetComponent<Button>().onClick.AddListener(() => FoodInstatiate(foodItem.name) /*Store("toggleItems")*/);
                foodItem.transform.Find("Stock").GetComponent<TextMeshProUGUI>().text = "x " + DataController.Instance.foodDatabase.item[i].stock.ToString();
            }
            else if(DataController.Instance.foodDatabase.item[i].stock > 0 && !DataController.Instance.foodDatabase.item[i].isInfinite)
            {
//                ClearChildItems();
//                horizontalICT.gameObject.SetActive(true);
//                horizontalContent.gameObject.SetActive(true);
                foodItem.transform.Find("Stock").GetComponent<TextMeshProUGUI>().text = "x " + DataController.Instance.foodDatabase.item[i].stock.ToString();
                foodItem.GetComponent<Button>().onClick.AddListener(() =>
                    {
                        FoodInstatiate(foodItem.name);
//                        GameObject food = Instantiate(objFoodItem);
//                        food.GetComponent<SpriteRenderer>().sprite = foodItem.GetComponent<Image>().sprite;
//                        // set food item level
//                        food.GetComponent<PetFood>().foodItemLevel = foodItem.GetComponent<PetButtonItem>().foodItemLevel;
//                        food.GetComponent<PetFood>().Initialize();
//
//                        toggleFood.isOn = false;
//
//                        // decrease inventory
//                        DataController.Instance.foodDatabase.item.First(_food => _food.itemName == gameObject.name).stock--;
                    });
                //Debug.Log("Instantiate food");
            }
            else if(DataController.Instance.foodDatabase.item[i].isInfinite)
            {
                foodItem.transform.Find("Stock").GetComponent<TextMeshProUGUI>().text = "";
                foodItem.GetComponent<Button>().onClick.AddListener(() =>
                    {
                        FoodInstatiate(foodItem.name);
//                        GameObject food = Instantiate(objFoodItem);
//                        food.GetComponent<SpriteRenderer>().sprite = foodItem.GetComponent<Image>().sprite;
//                        // set food item level
//                        food.GetComponent<PetFood>().foodItemLevel = foodItem.GetComponent<PetButtonItem>().foodItemLevel;
//                        food.GetComponent<PetFood>().Initialize();
//
//                        toggleFood.isOn = false;
                    });
            }
        }
    }

    // create pet food
    void FoodInstatiate(string itemName)
    {
        PetFoodItem petFoodItem = DataController.Instance.foodDatabase.item.First(food => food.itemName == itemName);

        if (petFoodItem != null)
        {
            if (petFoodItem.stock == 0 && !petFoodItem.isInfinite)
            {
                Store("toggleItems");
            }
            else if(petFoodItem.stock > 0 && !petFoodItem.isInfinite)
            {
                if (FindObjectOfType<PetFood>() == null)
                {
					ACTION = PET_ACTION.FEED;
					EnableBlockers (true);
                    GameObject food = Instantiate(objFoodItem);
                    food.GetComponent<SpriteRenderer>().sprite = petFoodItem.image;
                    // set food item level
                    food.GetComponent<PetFood>().foodItemLevel = petFoodItem.foodItemLevel;
                    // set food type
                    food.GetComponent<PetFood>().foodType = petFoodItem.foodType;
                    food.GetComponent<PetFood>().Initialize();

                    toggleFood.isOn = false;

                    // decrease inventory
                    DataController.Instance.foodDatabase.item.First(_food => _food.itemName == itemName).stock--;    
                }
            }
            else if(petFoodItem.isInfinite)
            {
                if (FindObjectOfType<PetFood>() == null)
                {
					ACTION = PET_ACTION.FEED;
					EnableBlockers (true);
                    GameObject food = Instantiate(objFoodItem);
                    food.GetComponent<SpriteRenderer>().sprite = petFoodItem.image;
                    // set food item level
                    food.GetComponent<PetFood>().foodItemLevel = petFoodItem.foodItemLevel;
                    // set food type
                    food.GetComponent<PetFood>().foodType = petFoodItem.foodType;
                    food.GetComponent<PetFood>().Initialize();

                    toggleFood.isOn = false;
                }
            }
        }
    }

    void Bath()
    {
        ClearChildItems();

        horizontalICT.gameObject.SetActive(true);
        horizontalContent.gameObject.SetActive(true);
        costumePanel.gameObject.SetActive(false);

        int itemCount = DataController.Instance.petBathItemDatabase.Items.Count();

        List<PetBathItem> petBathItem = DataController.Instance.petBathItemDatabase.Items;

        // disable scroll
        horizontalICT.Find("HorizontalCT").GetComponent<ScrollRect>().enabled = false;
        horizontalContent.localPosition = new Vector3(30.5f, -8.5f, 1); 

        for (int i = 0; i < itemCount; i++)
        {
            GameObject bathItem = Instantiate(item, horizontalContent, false);
            GameObject objectToInstantiate = DataController.Instance.petBathItemDatabase.Items[i].bathObject;
            string objectName = DataController.Instance.petBathItemDatabase.Items[i].bathObject.name;

            bathItem.name = petBathItem[i].image.name;
            bathItem.GetComponent<Image>().sprite = petBathItem[i].image;
            bathItem.GetComponent<Button>().onClick.AddListener(() =>
                {
                    if(playThingObject == null)
                    {
                        playThingObject = Instantiate(objectToInstantiate);
                        playThingObject.name = objectName;

                        // disable panels
                        horizontalICT.gameObject.SetActive(false);
                        horizontalContent.gameObject.SetActive(false);

                        // disable toggle
                        toggleBath.isOn = false;
                    }
                    else if(playThingObject.name == objectName)
                    {
                        Destroy(playThingObject);

                        // disable panels
                        horizontalICT.gameObject.SetActive(false);
                        horizontalContent.gameObject.SetActive(false);

                        // disable toggle
                        toggleBath.isOn = false;
                    }
                    else
                    {
                        Destroy(playThingObject);  

                        playThingObject = Instantiate(objectToInstantiate);
                        playThingObject.name = objectName;

                        // disable panels
                        horizontalICT.gameObject.SetActive(false);
                        horizontalContent.gameObject.SetActive(false);

                        // disable toggle
                        toggleBath.isOn = false;
                    }
                });
        }
    }

    void Play()
    {
        RemoveActiveObject();

        ClearChildItems();

        horizontalICT.gameObject.SetActive(true);
        horizontalContent.gameObject.SetActive(true);
        costumePanel.gameObject.SetActive(false);

        List<PlayThings> playthingsList = new List<PlayThings>();

        // disable scroll
        horizontalICT.Find("HorizontalCT").GetComponent<ScrollRect>().enabled = false;
        horizontalContent.transform.localPosition = new Vector3(42.5f, -8.5f, 1); 


        // play things for dog
        if(DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].petType == EnumCollection.Pet.Dog)
        {
            playthingsList = DataController.Instance.playThingsDatabase.items.Where(_playThing => _playThing.isForDog == true).ToList();
        }
        // play things for cat
        else
        {
            playthingsList = DataController.Instance.playThingsDatabase.items.Where(_playThing => _playThing.isForCat == true).ToList();
        }

        //play things
        //int playThingCount = DataController.Instance.playThingsDatabase.items.Count();
        int playThingCount = playthingsList.Count; 

        for (int i = 0; i < playThingCount; i++) 
        {
            if (i != 0)
            {
                GameObject playThingObj = Instantiate(playThings, horizontalContent, false);

                PlayThings _playThings = playthingsList[i];

                playThingObj.name = playthingsList[i].itemName;
                playThingObj.GetComponent<Image>().sprite = playthingsList[i].image;
                playThingObj.GetComponent<Button>().onClick.AddListener(() => PlaythingButtonEvent(_playThings));
                //set sprite
                //locationObj.GetComponent<Image>().sprite = 
                if (playthingsList[i].isOwned)
                {
                    if (playThingObject != null && playThingObject.name == playthingsList[i].itemName)
                    {
                        //if (DataController.Instance.backgroundDatabase.items[j].isEquipped)
                        playThingObj.transform.Find("Image").gameObject.SetActive(true);
                    }
                }
                else
                {
                    playThingObj.GetComponent<Image>().color = new Color32(255, 255, 255, 128);
                }
            }
        }            
    }
        
    void PlaythingButtonEvent(PlayThings _playThing)
    {
        if(_playThing.isOwned)
        {
            if(playThingObject == null)
            {
                // disable panels
                horizontalICT.gameObject.SetActive(false);
                horizontalContent.gameObject.SetActive(false);

                // disable toggle
                togglePlayPet.isOn = false;

                if (_playThing.interActionType == PlayThings.InteractionType.Interaction)
                {
                    // disable panels
                    horizontalICT.gameObject.SetActive(false);
                    horizontalContent.gameObject.SetActive(false);

                    // disable toggle
                    togglePlayPet.isOn = false;
					ACTION = PET_ACTION.BALL;
                    playThingObject = Instantiate(Resources.Load<GameObject>("Prefab/PlayThingObject"));
                    playThingObject.name = _playThing.itemName;
                    playThingObject.GetComponent<SpriteRenderer>().sprite = _playThing.image;
                }
                else
                {
                    // disable panels
                    horizontalICT.gameObject.SetActive(false);
                    horizontalContent.gameObject.SetActive(false);

                    // disable toggle
                    togglePlayPet.isOn = false;

                    if(_playThing.itemName.ToLower() == "swing")
                    {
						ACTION = PET_ACTION.SWING;
						EnableBlockers (true);
						//6/30/2017
						//GameObject blocker = Instantiate(Resources.Load<GameObject>("Prefab/PanelBlocker"), FindObjectOfType<Canvas>().transform, false);

                        FindObjectOfType<AnimationController>().Swing();
                        FindObjectOfType<AnimationController>().callBack = () => 
                            {
                                PetBase pet = DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex];
                                if(pet.happiness > 0)
                                {
                                    FindObjectOfType<PetActionReward>().PlayThingReward(20);
                                    FindObjectOfType<PetInteractionController>().FillHappiness(0.7f);
                                    // deduction for happiness
                                    pet.totalHappinessDeducted += 0.7f;
                                    if(pet.totalHappinessDeducted >= 1)
                                    {
                                        pet.totalHappinessDeducted -= 1;
                                        FindObjectOfType<SpinePetMeter>().CallHeartAnimation();
                                    }
								ACTION = PET_ACTION.NONE;
                                    //Debug.Log("Callback for Swing! Animation done!"); 
                                }
								EnableBlockers(false);
                               // Destroy(blocker);
                            };
                        //Debug.Log("Swing animation");
                    }
                    else if(_playThing.itemName.ToLower() == "spa")
                    {
						ACTION = PET_ACTION.SPA;
						EnableBlockers (true);
						//6/30/2017
                        //GameObject blocker = Instantiate(Resources.Load<GameObject>("Prefab/PanelBlocker"), FindObjectOfType<Canvas>().transform, false);

                        FindObjectOfType<AnimationController>().Spa();

                        FindObjectOfType<AnimationController>().callBack = () => 
                            {
                                PetBase pet = DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex];
                                if(pet.happiness > 0)
                                {
                                    FindObjectOfType<PetActionReward>().PlayThingReward(40);
                                    FindObjectOfType<PetInteractionController>().FillHappiness(0.8f);
                                    // deduction for happiness
                                    pet.totalHappinessDeducted += 0.8f;
                                    if(pet.totalHappinessDeducted >= 1)
                                    {
                                        pet.totalHappinessDeducted -= 1;
                                        FindObjectOfType<SpinePetMeter>().CallHeartAnimation();
                                    }

                                    //Debug.Log("Callback for Spa! Animation done!");
                                }
								ACTION = PET_ACTION.NONE;
								EnableBlockers(false);
                                //Destroy(blocker);
                            };
                        //Debug.Log("Spa animation");
                    }
                }
            }
            else if(playThingObject.name == _playThing.itemName)
            {
                Destroy(playThingObject);

                // disable panels
                horizontalICT.gameObject.SetActive(false);
                horizontalContent.gameObject.SetActive(false);

                // disable toggle
                togglePlayPet.isOn = false;
            }
            else
            {
                Destroy(playThingObject);

                // disable panels
                horizontalICT.gameObject.SetActive(false);
                horizontalContent.gameObject.SetActive(false);

                // disable toggle
                togglePlayPet.isOn = false;

                if (_playThing.interActionType == PlayThings.InteractionType.Interaction)
                {
                    // disable panels
                    horizontalICT.gameObject.SetActive(false);
                    horizontalContent.gameObject.SetActive(false);

                    // disable toggle
                    togglePlayPet.isOn = false;
					ACTION = PET_ACTION.BALL;
                    playThingObject = Instantiate(Resources.Load<GameObject>("Prefab/PlayThingObject"));
                    playThingObject.name = _playThing.itemName;
                    playThingObject.GetComponent<SpriteRenderer>().sprite = _playThing.image;
                }
                else
                {
                    // disable panels
                    horizontalICT.gameObject.SetActive(false);
                    horizontalContent.gameObject.SetActive(false);

                    // disable toggle
                    togglePlayPet.isOn = false;

                    if(_playThing.itemName.ToLower() == "swing")
                    {
						ACTION = PET_ACTION.SWING;
						EnableBlockers (true);
						//6/30/2017
                        //GameObject blocker = Instantiate(Resources.Load<GameObject>("Prefab/PanelBlocker"), FindObjectOfType<Canvas>().transform, false);

                        FindObjectOfType<AnimationController>().Swing();
                        FindObjectOfType<AnimationController>().callBack = () => 
                            {
                                FindObjectOfType<PetActionReward>().PlayThingReward(20);
								ACTION = PET_ACTION.NONE;
                                //Debug.Log("Callback for Swing! Animation done!"); 
								EnableBlockers(false);
                                //Destroy(blocker);
                            };
                        //Debug.Log("Swing animation");
                    }
                    else if(_playThing.itemName.ToLower() == "spa")
                    {
						ACTION = PET_ACTION.SPA;
                        //6/30/2017
						//GameObject blocker = Instantiate(Resources.Load<GameObject>("Prefab/PanelBlocker"), FindObjectOfType<Canvas>().transform, false);
						EnableBlockers(true);
                        FindObjectOfType<AnimationController>().Spa();
                        FindObjectOfType<AnimationController>().callBack = () => 
                            {
                                FindObjectOfType<PetActionReward>().PlayThingReward(40);
							ACTION = PET_ACTION.NONE;
                                //Debug.Log("Callback for Spa! Animation done!");
							EnableBlockers(false);
                                //Destroy(blocker);
                            };
                        //Debug.Log("Spa animation");
                    }
                }
            }                        
        }
        else
        {
            Store("toggleItems");
        }
    }

    void Costume()
    {        
        RemoveActiveObject();

        horizontalICT.gameObject.SetActive(true);
        horizontalContent.gameObject.SetActive(true);
        costumePanel.gameObject.SetActive(true);     

        CostumeToggle();
    }

    void Sleep()
    {
		//Ronald_Sleep ();

//		toggleSleep.isOn = true;
//		Color32 color = new Color32 (90, 90, 90, 255);
//		GameObject blocker = Instantiate (Resources.Load<GameObject> ("Prefab/PanelBlocker"), FindObjectOfType<Canvas> ().transform, false);
//		FindObjectOfType<AnimationController> ().Sleeping ();
//		GameObject.Find ("btnPetSleep").GetComponent<Image> ().sprite = lampOn;
//		//bgIndoorSleep.DOFade(1, 3).OnComplete(() => StartCoroutine("_Sleep"));
//		bgIndoorSleep.GetComponent<SpriteRenderer> ().DOColor (color, 3).OnComplete (() => StartCoroutine (_Sleep (blocker)));
//		//Debug.Log("Will play sleep");	
//		print ("GOOD NIGHT!");
    }

	Coroutine _sleepCoroutine;
	PetSleep petSleepController;
	Image petSleepLamp;

	void CheckPetSleeping()
	{		
		if(!ScreenshotPetModuleManager.screenshotMode)
		{
			if (petSleepController.IsStillSleeping ()) 
			{
				if (!petSleepController.isSleeping) 
				{
					ManualSleep ();
					petSleepController.isSleeping = true;
					//CancelInvoke ("CheckPetSleeping");
				}

				//print ("still asleep");
			} 
			else 
			{
				if (petSleepController.isSleeping) 
				{
					ManualSleep ();
					petSleepController.isSleeping = false;
					//CancelInvoke ("CheckPetSleeping");
					// Compute gold reward, energy
					petSleepController.SleepDone();
					petSleepController.SleepReward();
				}
				// should reflect sleep changes, when outside pet home
				else if(DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].sleepDate != 0)
				{
					petSleepController.SleepDone();
					petSleepController.SleepReward();
					//Debug.Log("<color=green>SLEEP DONE, REFLECT REWARD CHANGES</color>");
				}
				//print ("pet is awake");
			}
		}

	}

	void CheckFirstSleepOnceOnStart()
	{
		if (petSleepController.IsStillSleeping ()) {
			ACTION = PET_ACTION.SLEEP;
			//FindObjectOfType<AnimationController> ().Sleeping ();			
            FindObjectOfType<AnimationController>().Sleeping();
			//GameObject.Find ("btnPetSleep").GetComponent<Image> ().sprite = lampOn;
			if (petSleepLamp == null) {
				if (GameObject.Find ("btnPetSleep") != null) {
					petSleepLamp = GameObject.Find ("btnPetSleep").GetComponent<Image> ();
				}
			}
			petSleepLamp.sprite = lampOn;

			bgIndoorSleep.GetComponent<SpriteRenderer> ().color = new Color32 (90, 90, 90, 255);

			petSleepController.isSleeping = true;

            petSleepController.LockDragAndButtons();
            // apply cat sleep costume
            if (DataController.Instance.playerData.playerPetList [DataController.Instance.playerData.currentPetIndex].petType == EnumCollection.Pet.Cat) {
                FindObjectOfType<PetCostume> ().ApplySleepCostume ();
            }

		} else {			
			ACTION = PET_ACTION.NONE;
            FindObjectOfType<AnimationController>().Idle();
		}
		InvokeRepeating ("CheckPetSleeping",1f,1f);
	}

	public void ManualSleep()
	{
		Color32 color;
		GameObject blocker;
        
		//6/30/2017
		//petSleepController = GameObject.Find ("btnPetSleep").GetComponent<PetSleep>();

		switch(petSleepController.isSleeping)
		{
		case true:
			toggleSleep.isOn = false;

                // disable buttons
			petSleepController.UnlockDragAndButtons ();

			color = new Color32 (255, 255, 255, 0);
			blocker = Instantiate (Resources.Load<GameObject> ("Prefab/PanelBlocker"), GameObject.Find("Canvas").transform, false);

			FindObjectOfType<AnimationController> ().Idle ();

                // re apply equipment
			if (DataController.Instance.playerData.playerPetList [DataController.Instance.playerData.currentPetIndex].petType == EnumCollection.Pet.Cat) {
				FindObjectOfType<PetCostume> ().ReApplyEquipment ();

				//set animation state
				FindObjectOfType<AnimationController> ().SetAnimationState ();
			}                    

                //petSleepController.ResetSleep();

			//GameObject.Find ("btnPetSleep").GetComponent<Image> ().sprite = lampOff;
			petSleepLamp.sprite = lampOff;
			//StopCoroutine (_sleepCoroutine);
			bgIndoorSleep.GetComponent<SpriteRenderer> ().DOColor (color, 3).OnComplete (() => 
    				Destroy (blocker)
			);                    
			ACTION = PET_ACTION.NONE;
    			//print ("GOOD MORNING!");
			    break;
		default:
			toggleSleep.isOn = true;
    		
                // disable buttons
			petSleepController.LockDragAndButtons ();

			color = new Color32 (90, 90, 90, 255);
			blocker = Instantiate (Resources.Load<GameObject> ("Prefab/PanelBlocker"), FindObjectOfType<Canvas> ().transform, false);

			FindObjectOfType<AnimationController> ().Sleeping ();

                // apply cat sleep costume
			if (DataController.Instance.playerData.playerPetList [DataController.Instance.playerData.currentPetIndex].petType == EnumCollection.Pet.Cat) {
				FindObjectOfType<PetCostume> ().ApplySleepCostume ();
			}

			GameObject.Find ("btnPetSleep").GetComponent<Image> ().sprite = lampOn;

    			//_coroutine = _Sleep (blocker);
			bgIndoorSleep.GetComponent<SpriteRenderer> ().DOColor (color, 3).OnComplete (() => 
    			//_sleepCoroutine = StartCoroutine (_Sleep (blocker))
    				Destroy (blocker)
			);    		    
			ACTION = PET_ACTION.SLEEP;
    			//print ("GOOD NIGHT!");
    			break;
		}
	}
       

//    IEnumerator _Sleep(GameObject _blocker)
//    {        
//        Color32 color = new Color32(255, 255, 255, 0);
//		Destroy(_blocker);
//        //Debug.Log("Should play sleep");
//        for (int i = 0; i < 20; i++)
//        {
//			print ("Waking up in " + (20-i) );
//            yield return new WaitForSeconds(1);
//        }
//			
//		//pa morning
//        bgIndoorSleep.GetComponent<SpriteRenderer>().DOColor(color, 3).OnComplete(() => 
//            {
//                FindObjectOfType<AnimationController>().Idle(); 
//                GameObject.Find("btnPetSleep").GetComponent<Image>().sprite = lampOff;
//                toggleSleep.isOn = false;
//               
//            });        
//    }

    void ClearChildItems()
    {                
        horizontalContent.DetachChildren();
        GameObject[] itemsButton = GameObject.FindGameObjectsWithTag("ItemButton");

        for (int i = 0; i < itemsButton.Length; i++)
        {            
            Destroy(itemsButton[i]);
        }
    }

    void DisablePanel()
    {
        horizontalICT.gameObject.SetActive(false);
        horizontalContent.gameObject.SetActive(false);
        costumePanel.gameObject.SetActive(false);
    }

//    public void Store(string tabName = null, params Button[] button)
//    {
//        GameObject store = Instantiate(storePanel, FindObjectOfType<Canvas>().transform, false);
//        store.GetComponent<Store>().OpenStore(tabName);
//        store.GetComponent<Store>().buttonToActivate = button;
//        Debug.Log("Tab name " + tabName);
//        store.name = "StorePanel";
//    }

    public void Store(string tabName = null)
    {
		GameObject store = Instantiate(storePanel, GameObject.Find("Canvas").transform, false);
        //store.transform.Find("btnBack").GetComponent<Button>().onClick.AddListener(() => Destroy(storePanel));
        store.GetComponent<Store>().OpenStore(tabName);
        //Debug.Log("Tab name " + tabName);
        store.name = "StorePanel";
        //Debug.Log("open store");
        SpineMeshEnabler.ins.EnableMeshRenderer(false);
    }

    public void CostumeToggle()
    {
        //PetAssets petAssets = GetComponent<PetAssets>();

        string costumeType = costumeToggleGroup.ActiveToggles().Single().name;

        ClearChildItems();

        switch (costumeType)
        {
            case "toggleHead":
                //Debug.Log("Head");
                HeadCostume();
                break;
            case "toggleBody":
                //Debug.Log("Body");
                BodyCostume();
                break;
            case "toggleAccessories":
                //Debug.Log("Accessories");
                AccessoryCostume(); 
                break;
            case "toggleLocation":
                Location();
                break;
            default:
                break;
        }
    }

    // CostumesPanel
    void HeadCostume()
    {        
        int currentPetIndex = DataController.Instance.playerData.currentPetIndex;

        // set equipment
        PetButton.equipmentName = DataController.Instance.playerData.playerPetList[currentPetIndex].headEquipment;

        Sprite headSprite = null;

        for (int i = 0; i < DataController.Instance.headDatabase.items.Count(); i++)
        {
            GameObject costumeItem;

            PetButton petButton;
            switch (DataController.Instance.playerData.playerPetList[currentPetIndex].petType)
            {
                case EnumCollection.Pet.Dog:
                    if(DataController.Instance.headDatabase.items[i].isForDog)
                    {
                        costumeItem = Instantiate(item, horizontalContent, false);

                        petButton = costumeItem.AddComponent<PetButton>();
                        petButton.key = DataController.Instance.headDatabase.items[i].key;
                        petButton.costumeType = EnumCollection.Equipment.Head;
                        petButton.isOwned = DataController.Instance.headDatabase.items[i].isOwned;
                        headSprite = DataController.Instance.headDatabase.items[i].image;

                        costumeItem.GetComponent<Image>().sprite = headSprite;

                        // set pet button name
                        petButton.name = DataController.Instance.headDatabase.items[i].key;

                        CostumeImage(petButton.isOwned, costumeItem.GetComponent<Image>());

                        // check if this is the equipped item
                        if(PetButton.equipmentName == DataController.Instance.headDatabase.items[i].key)
                        {
                            PetButton.equippedButtonObject = costumeItem;
                            costumeItem.transform.Find("Equipped").gameObject.SetActive(true);
                        }
                        break;
                    }
                    else
                    {
                        break;
                    }
                case EnumCollection.Pet.Cat:
                    if(DataController.Instance.headDatabase.items[i].isForCat)
                    {
                        costumeItem = Instantiate(item, horizontalContent, false);

                        petButton = costumeItem.AddComponent<PetButton>();

                        petButton.key = DataController.Instance.headDatabase.items[i].key;
                        petButton.costumeType = EnumCollection.Equipment.Head;
                        petButton.isOwned = DataController.Instance.headDatabase.items[i].isOwned;
                        headSprite = DataController.Instance.headDatabase.items[i].image;

                        costumeItem.GetComponent<Image>().sprite = headSprite;

                        // set pet button name
                        petButton.name = DataController.Instance.headDatabase.items[i].key;

                        CostumeImage(petButton.isOwned, costumeItem.GetComponent<Image>());

                        // check if this is the equipped item
                        if(PetButton.equipmentName == DataController.Instance.headDatabase.items[i].key)
                        {
                            PetButton.equippedButtonObject = costumeItem;
                            costumeItem.transform.Find("Equipped").gameObject.SetActive(true);
                        }
                        break;
                    }
                    else
                    {
                        break;
                    }              
            }                    
        }               
    }

    void BodyCostume()
    {
        int currentPetIndex = DataController.Instance.playerData.currentPetIndex;
        Sprite headSprite = null;

        // set equipment
        PetButton.equipmentName = DataController.Instance.playerData.playerPetList[currentPetIndex].bodyEquipment;

        for (int i = 0; i < DataController.Instance.bodyDatabase.items.Count(); i++)
        {
            GameObject costumeItem;

            PetButton petButton;
            switch (DataController.Instance.playerData.playerPetList[currentPetIndex].petType)
            {
                case EnumCollection.Pet.Dog:
                    if(DataController.Instance.bodyDatabase.items[i].isForDog)
                    {
                        costumeItem = Instantiate(item, horizontalContent, false);

                        petButton = costumeItem.AddComponent<PetButton>();

                        petButton.key = DataController.Instance.bodyDatabase.items[i].key;
                        petButton.costumeType = EnumCollection.Equipment.Body;
                        petButton.isOwned = DataController.Instance.bodyDatabase.items[i].isOwned;
                        headSprite = DataController.Instance.bodyDatabase.items[i].image;

                        costumeItem.GetComponent<Image>().sprite = headSprite;

                        // set pet button name
                        petButton.name = DataController.Instance.bodyDatabase.items[i].key;

                        CostumeImage(petButton.isOwned, costumeItem.GetComponent<Image>());

                        // check if this is the equipped item
                        if(PetButton.equipmentName == DataController.Instance.bodyDatabase.items[i].key)
                        {
                            PetButton.equippedButtonObject = costumeItem;
                            costumeItem.transform.Find("Equipped").gameObject.SetActive(true);
                        }
                        break;
                    }
                    else
                    {
                        break;
                    }
                case EnumCollection.Pet.Cat:
                    if(DataController.Instance.bodyDatabase.items[i].isForCat)
                    {
                        costumeItem = Instantiate(item, horizontalContent, false);

                        petButton = costumeItem.AddComponent<PetButton>();

                        petButton.key = DataController.Instance.bodyDatabase.items[i].key;
                        petButton.costumeType = EnumCollection.Equipment.Body;
                        petButton.isOwned = DataController.Instance.bodyDatabase.items[i].isOwned;
                        headSprite = DataController.Instance.bodyDatabase.items[i].image;

                        costumeItem.GetComponent<Image>().sprite = headSprite;

                        // set pet button name
                        petButton.name = DataController.Instance.bodyDatabase.items[i].key;

                        CostumeImage(petButton.isOwned, costumeItem.GetComponent<Image>());

                        // check if this is the equipped item
                        if(PetButton.equipmentName == DataController.Instance.bodyDatabase.items[i].key)
                        {
                            PetButton.equippedButtonObject = costumeItem;
                            costumeItem.transform.Find("Equipped").gameObject.SetActive(true);
                        }
                        break;
                    }
                    else
                    {
                        break;
                    }              
            }                    
        }               
    }

    void AccessoryCostume()
    {
        int currentPetIndex = DataController.Instance.playerData.currentPetIndex;
        Sprite headSprite = null;

        // set equipment
        PetButton.equipmentName = DataController.Instance.playerData.playerPetList[currentPetIndex].accessories;

        for (int i = 0; i < DataController.Instance.AccessoriesDatabase.items.Count(); i++)
        {
            GameObject costumeItem;

            PetButton petButton;
            switch (DataController.Instance.playerData.playerPetList[currentPetIndex].petType)
            {
                case EnumCollection.Pet.Dog:
                    if(DataController.Instance.AccessoriesDatabase.items[i].isForDog)
                    {
                        costumeItem = Instantiate(item, horizontalContent, false);

                        petButton = costumeItem.AddComponent<PetButton>();

                        petButton.key = DataController.Instance.AccessoriesDatabase.items[i].key;
                        petButton.costumeType = EnumCollection.Equipment.Accessories;
                        petButton.isOwned = DataController.Instance.AccessoriesDatabase.items[i].isOwned;
                        headSprite = DataController.Instance.AccessoriesDatabase.items[i].image;

                        costumeItem.GetComponent<Image>().sprite = headSprite;

                        // set pet button name
                        petButton.name = DataController.Instance.AccessoriesDatabase.items[i].key;

                        CostumeImage(petButton.isOwned, costumeItem.GetComponent<Image>());

                        // check if this is the equipped item
                        if(PetButton.equipmentName == DataController.Instance.AccessoriesDatabase.items[i].key)
                        {
                            PetButton.equippedButtonObject = costumeItem;
                            costumeItem.transform.Find("Equipped").gameObject.SetActive(true);
                        }

                        break;
                    }
                    else
                    {
                        break;
                    }
                case EnumCollection.Pet.Cat:
                    if(DataController.Instance.AccessoriesDatabase.items[i].isForCat)
                    {
                        costumeItem = Instantiate(item, horizontalContent, false);

                        petButton = costumeItem.AddComponent<PetButton>();

                        petButton.key = DataController.Instance.AccessoriesDatabase.items[i].key;
                        petButton.costumeType = EnumCollection.Equipment.Accessories;
                        petButton.isOwned = DataController.Instance.AccessoriesDatabase.items[i].isOwned;
                        headSprite = DataController.Instance.AccessoriesDatabase.items[i].image;

                        costumeItem.GetComponent<Image>().sprite = headSprite;

                        // set pet button name
                        petButton.name = DataController.Instance.AccessoriesDatabase.items[i].key;

                        CostumeImage(petButton.isOwned, costumeItem.GetComponent<Image>());

                        // check if this is the equipped item
                        if(PetButton.equipmentName == DataController.Instance.AccessoriesDatabase.items[i].key)
                        {
                            PetButton.equippedButtonObject = costumeItem;
                            costumeItem.transform.Find("Equipped").gameObject.SetActive(true);
                        }

                        break;
                    }
                    else
                    {
                        break;
                    }              
            }                    
        }               
    }

    void Location()
    {        
        int locationCount = DataController.Instance.backgroundDatabase.items.Count();

        for (int j = 0; j < locationCount; j++) {
            GameObject locationObj = Instantiate(location, horizontalContent, false);
            locationObj.GetComponent<PetLocation>().locationName = DataController.Instance.backgroundDatabase.items[j].name;
            locationObj.GetComponent<Image>().sprite = DataController.Instance.backgroundDatabase.items[j].image;
            //set sprite
            //locationObj.GetComponent<Image>().sprite = 
            if(DataController.Instance.backgroundDatabase.items[j].isOwned)
            {
                if(DataController.Instance.backgroundDatabase.items[j].isEquipped)
                    locationObj.transform.Find("Image").gameObject.SetActive(true);
            }
            else
            {
                locationObj.GetComponent<Image>().color = new Color32(255, 255, 255, 128);
            }
        }            
    }
    //CostumesPanel

    /// <summary>
    /// Set transparency of costumes not owned
    /// </summary>
    void CostumeImage(bool isOwned, Image image)
    {
        //Debug.Log("Is owned " + isOwned);
        if(!isOwned)
        {
            image.color = new Color32(255, 255, 255, 128);
        }
    }
        
    void RemoveActiveObject()
    {
        // remove active object
        if(playThingObject != null)
        {
            Destroy(playThingObject);
        }
    }

    #endregion
}



