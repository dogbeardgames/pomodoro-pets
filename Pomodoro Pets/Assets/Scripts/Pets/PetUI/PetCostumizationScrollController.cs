﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PetCostumizationScrollController : MonoBehaviour {

    [SerializeField]
    ScrollRect scrollRect;

    [SerializeField]
    Button buttonLeft, buttonRight;

	// Use this for initialization
	void Start () 
    {
//        scrollRect.onValueChanged.AddListener(ScrollMove);
//        buttonLeft.onClick.AddListener(ScrollLeft);
//        buttonRight.onClick.AddListener(ScrollRight);
	}


    #region METHODS

    // called every move of the scroll
    void ScrollMove(Vector2 vectorTwo)
    {
        //Debug.Log("<color=red> Vector2" + vectorTwo + "</color>");

        // middle
        if(vectorTwo.x > 0 && vectorTwo.x < 1)
        {
            ////Debug.Log("<color=red>Middle</color>");
            buttonLeft.gameObject.SetActive(true);
            buttonRight.gameObject.SetActive(true);
        }
        // rightmost
        else if(vectorTwo.x >= 1)
        {
            //Debug.Log("<color=red>right most</color>");
            buttonLeft.gameObject.SetActive(true);
            buttonRight.gameObject.SetActive(false);
        }
        // leftmost
        else if(vectorTwo.x <= 0)
        {
            //Debug.Log("<color=red>left most</color>");
            buttonLeft.gameObject.SetActive(false);
            buttonRight.gameObject.SetActive(true);
        }
    }

    void ScrollLeft()
    {
//        scrollRect.horizontalNormalizedPosition = 0;
        scrollRect.DOHorizontalNormalizedPos(0, 0.6f);
    }

    void ScrollRight()
    {
//        scrollRect.horizontalNormalizedPosition = 1;
        scrollRect.DOHorizontalNormalizedPos(1, 0.6f);
    }

    #endregion
}
