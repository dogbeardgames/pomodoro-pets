﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPetInteraction : MonoBehaviour {

    Transform itemPosition;
    Vector3 pos;
    Vector2 dragStartPos;

    Rigidbody2D rigidBody2D;

    [SerializeField]
    byte maxInteractionCount;

    byte interactionCount = 0;

    float startTime, endTime;
    // Use this for initialization
    void Start () {
        itemPosition = transform;
        rigidBody2D = GetComponent<Rigidbody2D>();

        // enable bg drag
        BackgroundDrag.canDragBG = false;
    }

    // Update is called once per frame
//    void Update() {
//
//    }


    void OnDisable()
    {
        // disable bg drag
        BackgroundDrag.canDragBG = true;
    }

	void OnDestroy()
	{
		PetUIController.ACTION = PetUIController.PET_ACTION.NONE;
	}

    #region MOUSEEVENT
    void OnMouseDown()
    {
		PetUIController.ACTION = PetUIController.PET_ACTION.BALL;
        //Debug.Log("can drag bg " + BackgroundDrag.canDragBG);
        dragStartPos = Input.mousePosition;
        // get start time
        startTime = Time.time;
    }
    void OnMouseDrag()
    {
        //Debug.Log("mouse position " + Camera.main.ScreenToWorldPoint(Input.mousePosition));
        pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        pos.x = Mathf.Clamp(pos.x, -Screen.width / 2, Screen.width / 2);
        pos.y = Mathf.Clamp(pos.y, -2, Screen.height / 2);
        pos.z = -1;
        itemPosition.position = pos;
    }

    void OnMouseUp()
    {
        PetBase pet = DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex];

        // get end time
        endTime = Time.time;

        //Debug.Log("mouse up " + BackgroundDrag.canDragBG);
        //Debug.Log("velocity " + rigidBody2D.velocity + ", distance " + ((Vector2)Input.mousePosition - dragStartPos).magnitude + ", time " + (endTime - startTime));

        // compute distance/time for force
        float distance = ((Vector2)Input.mousePosition - dragStartPos).magnitude;
        float time = endTime - startTime;
        float force = Mathf.Clamp(((distance / time) / 100), 0, 50);

        //Debug.Log("force " + force);

        // add force to rigid body
        rigidBody2D.AddForce((((Vector2)Input.mousePosition - dragStartPos) * force));
        dragStartPos = Vector3.zero;

        if(pet.happiness > 0)
        {            
            if(interactionCount < maxInteractionCount - 1)
            {
                interactionCount++;
            }
            else
            {
                // reset interaction count
                interactionCount = 0;
                // call stats 
                //Debug.Log("<color=red>interaction done</color>");
                PetInteractionController petInteractionController = FindObjectOfType<PetInteractionController>();

                // monitor total deducted happiness
                petInteractionController.FillHappiness(0.4f);
//                PetActionReward.Instance.UserDefinedGoldReward(4);
                FindObjectOfType<PetActionReward>().UserDefinedGoldReward(4);
                pet.totalHappinessDeducted += 0.4f;

                //Update Pet Meter
                FindObjectOfType<RotatePawIcon>().UpdatePetMeter();

                if(pet.totalHappinessDeducted >= 1)
                {
                    pet.totalHappinessDeducted -= 1;
                    FindObjectOfType<SpinePetMeter>().CallHeartAnimation();
                }
            }   
        }        
    }

	void OnCollisionEnter2D(Collision2D col)
	{
//        if (pet.petType == EnumCollection.Pet.Cat)
//        {
//            Framework.SoundManager.Instance.PlaySFX("seBouncingYarn");	
//        }
//        else
//        {
//            Framework.SoundManager.Instance.PlaySFX("seBouncingBall");  
//        }

        Framework.SoundManager.Instance.PlaySFX("seBouncingYarn");
	}
    #endregion
}
