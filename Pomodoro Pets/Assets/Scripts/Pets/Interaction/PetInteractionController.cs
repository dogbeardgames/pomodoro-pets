﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetInteractionController : MonoBehaviour {

    PetBase petbase;
    Pet petObject;

	// Use this for initialization
	void Start () {        
        //petbase = GetComponent<PetAssets>().petObject
	}	

    #region METHODS
    //fill the happiness attribute, if food not full
    public void FillHappiness(float value)
    {
        petbase = DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex];
        petObject = GetComponent<PetAssets>().petObject.GetComponent<Pet>();
        petbase.happiness = Mathf.Clamp((petbase.happiness - value), 0, 3);
        petObject.happiness = Mathf.Clamp((petObject.happiness - value), 0, 3);
    }

    //fill the hunger_thirst attribute
    public void FillHunger_Thirst(float value)
    {
        petbase = DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex];
        petObject = GetComponent<PetAssets>().petObject.GetComponent<Pet>();

        //Debug.Log("Decrease happiness");
        petbase.hunger_thirst = Mathf.Clamp((petbase.hunger_thirst - value), 0, 3);
        petObject.hunger_thirst = Mathf.Clamp((petObject.hunger_thirst - value), 0, 3);

//        //check if hunger is full and fed once
//        if(petbase.hunger_thirst == 0 && petbase.isFoodFull)
//        {
//            PlayDeclineHunger_Thirst(petObject);
//        }
//        //check if hunger is full
//        else if(petbase.hunger_thirst == 0 && petbase.isFoodFull == false)
//        {
//            petObject.isFoodFull = true;
//            petbase.isFoodFull = true;
//        }
//        // not full
//        else
//        {
//            
//        }            
    }

    //fill the cleanliness attribute
    public void FillCleanliness(float value)
    {
        petbase = DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex];
        petObject = GetComponent<PetAssets>().petObject.GetComponent<Pet>();
        petbase.cleanliness = Mathf.Clamp((petbase.cleanliness - value), 0, 3);
        petObject.cleanliness = Mathf.Clamp((petObject.cleanliness - value), 0, 3);
        //Debug.Log("<color=red>Fill cleanliness</color>");
    }

    //fill the energy attribute
    public void FillEnergy(float value)
    {
        petbase = DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex];
        petObject = GetComponent<PetAssets>().petObject.GetComponent<Pet>();
        petbase.energy = Mathf.Clamp((petbase.energy - value), 0, 3);
        petObject.energy = Mathf.Clamp((petObject.energy - value), 0, 3);
    }    

    // when attributes are already full then interacted twice, if food full
    public void PlayDeclineHunger_Thirst(Pet _pet)// full happiness
    {        
        //Debug.Log("Player pet decline animation");
    }  

    public bool isFoodFull()
    {
        petbase = DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex];
        petObject = GetComponent<PetAssets>().petObject.GetComponent<Pet>();

        if(petbase.hunger_thirst == 0 && petbase.isFoodFull)
        {
            //PlayDeclineHunger_Thirst(petObject);
            return true;
        }
        //check if hunger is full
        else if(petbase.hunger_thirst == 0 && petbase.isFoodFull == false)
        {
            petObject.isFoodFull = true;
            petbase.isFoodFull = true;
            return false;
        }
        // not full
        else
        {
            petObject.isFoodFull = false;
            petbase.isFoodFull = false;
            return false;
        }
    }

    public bool isEnergyFull()
    {
        petbase = DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex];
        petObject = GetComponent<PetAssets>().petObject.GetComponent<Pet>();

        if(petbase.energy == 0 && petbase.isEnergyFull)
        {
            //PlayDeclineHunger_Thirst(petObject);
            return true;
        }
        //check if hunger is full
        else if(petbase.energy == 0 && petbase.isEnergyFull == false)
        {
            petObject.isEnerygFull = true;
            petbase.isEnergyFull = true;
            return false;
        }
        // not full
        else
        {
            petObject.isEnerygFull = false;
            petbase.isEnergyFull = false;
            return false;
        }
    }
    #endregion
}
