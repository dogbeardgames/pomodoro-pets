﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Box2DScaler : MonoBehaviour {

    Vector2 scale;
    BoxCollider2D bc2D;
    RectTransform rectTransform;

	// Use this for initialization
	void Start () {
        bc2D = GetComponent<BoxCollider2D>();
        rectTransform = GetComponent<RectTransform>();
        scale = new Vector2(rectTransform.rect.width, rectTransform.rect.height);
        bc2D.size = scale;
	}
}
