﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

public class PlayThing : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
    #region METHODS

    public void UpdatePlayThingObject()
    {
        if(DataController.Instance.playThingsDatabase.items.First(playThing => playThing.itemName == this.gameObject.name) != null)
        {
            if(DataController.Instance.playThingsDatabase.items.First(playThing => playThing.itemName == this.gameObject.name).isOwned)
            {                
                GetComponent<Image>().color = new Color32(255, 255, 255, 255);
            }
        }
    }

    #endregion
}
