﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sponge : PetBathBase {

    int count = 0;

    Vector3 pos;

    public PetBathController.Bathstate bathState;

    bool isCreatingBubble = false;

    public bool isHolding = false, isplayingSFX = false;

    // total time interacting with the pet
    float totalTimeInteracting = 0;

    Rigidbody2D rb2D;

    public static Sponge Instance;

    //PetBathController
    PetBathController petBathController;

	// Use this for initialization
	void Start () 
    {
        Instance = this;

        bathState = FindObjectOfType<PetBathController>().bathState;
        BackgroundDrag.canDragBG = false;

        // get petBathcontroller
        petBathController = FindObjectOfType<PetBathController>();

//        if (bathState == PetBathController.Bathstate.Sponge)
//        {            
//            isUsable = true;
//        }
	}	

    void LateUpdate()
    {
        isplayingSFX = false;
    }

    #region MONO

    void OnMouseDown()
    {
        if (bathState == PetBathController.Bathstate.Sponge)
        {
			PetUIController.ACTION = PetUIController.PET_ACTION.SPONGE;
            //Debug.Log("Sponge Mouse Down"); 
            isHolding = true;
        }
    }

    void OnMouseUp()
    {
        if (bathState == PetBathController.Bathstate.Sponge)
        {
            //Debug.Log("Sponge Mouse Up"); 
            isHolding = false;
        }
    }

    void OnMouseDrag()
    {        
        pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        pos.z = transform.position.z;
        pos.y = Mathf.Clamp(pos.y, -2.891337f, Screen.height);
        transform.position = pos;

        // if not playing, play it then change it's value to true
//        if(!isplayingSFX)
//        {
//            isplayingSFX = !isplayingSFX;
//            PetsAudio.Instance.SpongeSFX();
//        }

//        if(isInboundary)
//        {                
//            Debug.Log("<color=red>Is in boundary</color>");
//        }            
    }

//    // complete sponge session for the pet
//    void SpongeComplete()
//    {
//        // call heart animation
//        // change state to sponge
//    }

    void OnDisable()
    {
        // disable bg drag
        BackgroundDrag.canDragBG = true;
    }                           
   
    void OnTriggerStay2D(Collider2D collider2D)
    {
        //Debug.Log("Collide");

        if (collider2D.name == "bubble")
        {
            //disable collider
            collider2D.GetComponent<ParticleSystem>().Play();
            //Debug.Log("<color=red>On Collider Stay " + collider2D.name + "</color>");
            if (petBathController.BubbleCounter() && !isCreatingBubble)
            {
                isCreatingBubble = true;
                petBathController.bathState = PetBathController.Bathstate.Hose;
                petBathController.AddBubbleCounterScript();
                // play bubble sfx
                PetsAudio.Instance.BubbleSFX();
            }           
        }
    }

    #endregion
}
