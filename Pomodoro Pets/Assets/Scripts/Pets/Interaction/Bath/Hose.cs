﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework;

public class Hose : PetBathBase {

    Vector3 pos;

    [SerializeField]
    ParticleSystem particleSystem;

	// Use this for initialization
	void Start () 
    {
//        FindObjectOfType<PetBathController>().bathState = PetBathController.Bathstate.Hose;
        BackgroundDrag.canDragBG = false;
        PetsAudio.Instance.WaterSFX();
	}
		
    #region MONO

    void OnMouseDown()
    {
        //Debug.Log("Hose Mouse Down"); 
		PetUIController.ACTION = PetUIController.PET_ACTION.HOSE;
		//SoundManager.Instance.PlaySFX ("seWaterSprinkle", true);
		SoundManager.Instance.SetSFXAndPlay("seWaterSprinkle", true);
	}

    void OnMouseDrag()
    {
        pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        pos.z = transform.position.z;
        pos.y = Mathf.Clamp(pos.y, -2.891337f, Screen.height);
        transform.position = pos;
    }

//    void OnCollision2D(Collision2D collision2D)
//    {
//        Debug.Log("<color=red>Collision, " + collision2D.gameObject.name + "</color>");
//    }

//    void OnParticleCollision(GameObject particleObject)
//    {
////        Debug.Log("<color=red>Collision, " + particleObject.name + "</color>");
//    }                

    void OnDisable()
    {
        // disable bg drag
		SoundManager.Instance.SFX.Stop();
        BackgroundDrag.canDragBG = true;
    }

    #endregion
}   