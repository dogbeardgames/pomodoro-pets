﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetBubbleCounter : MonoBehaviour {

    ParticleSystem[] _particleSystem;

    bool isDestroying = false;

	// Use this for initialization
	void Start () {
        _particleSystem = transform.GetComponentsInChildren<ParticleSystem>();
	}
	
	// Update is called once per frame
	void LateUpdate () 
    {
        for (int i = 0; i < _particleSystem.Length; i++)
        {
            if(_particleSystem[i].IsAlive(true))
            {
//                Debug.Log("<color=red>particle is alive</color>");
                return;
            }                   
        }  

//        Debug.Log("<color=red>no more live particle</color>");
        // check is cleanliness is more than zero
        if (!isDestroying)
        {
            isDestroying = true;
            PetBase pet = DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex];
            if (pet.cleanliness > 0)
            {
                // Gold reward
                FindObjectOfType<PetActionReward>().UserDefinedGoldReward(30);
                // cleanliness needed variable
                float cleanlinessNeeded = DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].cleanliness;
                // apply stat boost
                FindObjectOfType<PetInteractionController>().FillCleanliness(cleanlinessNeeded);
//                pet.totalCleanlinessDeducted += 1;
//                if(pet.totalCleanlinessDeducted >= 1)
//                {
//                    pet.totalCleanlinessDeducted -= 1;
//                    FindObjectOfType<SpinePetMeter>().CallHeartAnimation();
//                }
                // heart animation
                FindObjectOfType<SpinePetMeter>().CallHeartAnimation();
                // change bath state back to sponge
                FindObjectOfType<PetBathController>().bathState = PetBathController.Bathstate.Sponge;
                Destroy(this);
            }
            else
            {
                // change bath state back to sponge
                FindObjectOfType<PetBathController>().bathState = PetBathController.Bathstate.Sponge;
                Destroy(this);
            }
        }
	}   
}