﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PetPlaySprite : MonoBehaviour {

    [SerializeField]
    Sprite dogPlay, catPlay;

	// Use this for initialization
	void Start () {
        if(DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].petType == EnumCollection.Pet.Dog)
        {
            GetComponent<Image>().sprite = dogPlay;
        }
        else if(DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].petType == EnumCollection.Pet.Cat)
        {
            GetComponent<Image>().sprite = catPlay;
        }
	}
}
