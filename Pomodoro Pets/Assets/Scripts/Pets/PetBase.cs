﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity.Modules;

[System.Serializable]
public class PetBase {
    
	public EnumCollection.Pet petType;
    public EnumCollection.PetCategory petCategory;
//    public int petIndex;
//    public int petTypeIndex;

	public string petName;
    public string petBreed;

    public float happiness = 0;
    public float hunger_thirst = 0;
    public float cleanliness = 0;
    public float energy = 0;
    public float quitvalue = 0;
	public float cyclevalue = 1;

    public float totalHappinessDeducted = 0;
    public float totalHunger_ThirstDeducted = 0;
    public float totalCleanlinessDeducted = 0;
    public float totalEnergyDeducted = 0;

    public bool isFoodFull;
    public bool isEnergyFull;

	public string headEquipment = "none";
	public string bodyEquipment = "none";
	//public string shoesEquipment;
	public string accessories ="none"; 

	public long sleepDate;



    public bool isInStorage;

    public PetBase()
    {
        happiness = 0;
        hunger_thirst = 0;
        cleanliness = 0;
        energy = 0;
		quitvalue= 0;
		cyclevalue=1;

        headEquipment = "none";
        bodyEquipment = "none";
        //shoesEquipment = "none";
        accessories = "none";

        isFoodFull = false;
    }
}    
