﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class FlyingBirdGift : MonoBehaviour {

	[SerializeField]
	RectTransform rect;
	[SerializeField]
	Vector2 originalSize;
	[SerializeField]
	bool clicked;

	[SerializeField]
	Button btn;


	public bool Clicked{
		get{ return clicked;}

	}

	//Vector2 pos;
	void Awake()
	{
		
	}
	void Start () {
		//rect = GetComponent<RectTransform> ();
		originalSize = new Vector2 (53.8f,53.8f);
		//btn = GetComponent<Button> ();
		btn.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		//keep this for reference ****
//		if (!PomodoroUI.Instance.isMenuActive) {
//			if(Input.GetMouseButtonDown(0))
//			{
//				if(Vector2.Distance(transform.position, Camera.main.ScreenToWorldPoint (Input.mousePosition)) <= 1f)
//				{
//					ShowReward ();
//				}
//					
//
//			}
//		}
			
	}

	void FixedUpdate()
	{

	}



	public void Show()
	{
		clicked = false;
		rect.sizeDelta = originalSize;
		btn.enabled = false;
		Invoke ("Drop",1f);
	}

	void Drop()
	{
		
		rect.DOLocalMoveY (0f,2f).OnComplete(()=>Grow());

	}

	void Grow()
	{
		rect.DOSizeDelta (rect.sizeDelta * 2f, 1f).OnComplete(()=> btn.enabled = true);
		rect.DOLocalMoveX (0f,1f);
	}

	public void ShowReward()
	{
		Framework.SoundManager.Instance.PlaySFX ("seReward");
		clicked = true;
		//print ("YOU WIN FIVE THOUSAND PIECES!");
		gameObject.SetActive (false);
		FlyingBirdManager.ins.EnableGiftPanel (false);
		//get bird reward
		AchievementConditions.instance.BirdReward ();
	}

	void OnDisable()
	{
		clicked = true;
	}

}
