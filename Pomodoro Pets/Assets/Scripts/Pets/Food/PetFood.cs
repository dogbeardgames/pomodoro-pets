﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Spine.Unity;

public class PetFood : MonoBehaviour {

    Vector3 startPos;
    Spine.Bone bone;
    Spine.ExposedList<Spine.Bone> bones;

    public EnumCollection.FoodItemLevel foodItemLevel;
    public PetFoodItem.FoodType foodType;

    PetInteractionController petInteractionController;
    AnimationController animationController;

	// Use this for initialization
	void Start () {
        animationController = FindObjectOfType<AnimationController>();
        petInteractionController = FindObjectOfType<PetInteractionController>();
	}	

//	// Update is called once per frame
//	void Update () {
//		
//	}
           
    #region METHODS
    public void Initialize()
    {
        SkeletonAnimation spineAnimation = GameObject.Find("Pet").GetComponent<SkeletonAnimation>();
        bone = spineAnimation.Skeleton.FindBone("mouth");
        Vector3 petTransform = spineAnimation.transform.TransformPoint(bone.WorldX, bone.WorldY, 0);
        startPos = transform.position;

        startPos.x =  petTransform.x;
        transform.position = startPos;
        startPos.y = -2.35f;
        //startPos.x = bone.WorldX;
        transform.DOMoveY(startPos.y, 1, false).SetEase(Ease.OutBounce).OnComplete(CheckFoodConsumption);
    }

    void CallEatAnimation()
    {        
		PetUIController.ACTION = PetUIController.PET_ACTION.FEED;
		//6/30/2017
        //GameObject blocker = Instantiate(Resources.Load<GameObject>("Prefab/PanelBlocker"), FindObjectOfType<Canvas>().transform, false);
		PetUIController.Instance.EnableBlockers(true);
        animationController.Eating();
        animationController.callBack = () =>
        {
            StatBoostCallback();
			PetUIController.ACTION = PetUIController.PET_ACTION.NONE;
			PetUIController.Instance.EnableBlockers(false);
            //Destroy(blocker);
        };
    }

    void CallEatDeclineAnimation()
    {
        if (animationController != null)
        {
            animationController.ShakeHead();
			animationController.callBack = () =>
			{
				PetUIController.ACTION = PetUIController.PET_ACTION.NONE;
				PetUIController.Instance.EnableBlockers(false);
				//Destroy(blocker);
			};
        }
    }

    void CheckFoodConsumption()
    {
        PetInteractionController pet = FindObjectOfType<PetInteractionController>();
        AnimationController animController = FindObjectOfType<AnimationController>();
        if(pet != null)
        {
            // for food
            if (foodType == PetFoodItem.FoodType.Food)
            {
                if (pet.isFoodFull())
                {
                    // decline
                    //Debug.Log("Decline food");
                    CallEatDeclineAnimation();
                }
                else
                {
                    CallEatAnimation();
                }
            }
            // for treats
            else
            {
                if(pet.isEnergyFull())
                {
                    CallEatDeclineAnimation();
                }
                else
                {
                    CallEatAnimation();
                }
            }
        }
    }

    void StatBoostCallback()
    {
        PetBase pet = DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex];

        //Debug.Log("Done eating, give stat boost, food item level " + foodItemLevel.ToString());
        FoodStatReward foodStatReward = FindObjectOfType<FoodStatReward>();
        PetActionReward petActionReward = FindObjectOfType<PetActionReward>();
        if (foodType == PetFoodItem.FoodType.Food)
        {
            if (foodStatReward != null && pet.hunger_thirst > 0)
            {
                float fillAmount = foodStatReward.StatBoost(foodItemLevel);
                //Debug.Log("fill amount hunger " + fillAmount);

                petInteractionController.FillHunger_Thirst(fillAmount);
                // gold reward
                if (petActionReward != null)
                {
                    // gold reward for every food eaten
                    petActionReward.FoodGoldReward((uint)foodStatReward.GoldReward(foodItemLevel));
                }

                // food achievement
                AchievementConditions.instance.AGoodMeal(foodItemLevel);

                pet.totalHunger_ThirstDeducted += foodStatReward.StatBoost(foodItemLevel);
                if (pet.totalHunger_ThirstDeducted >= 1)
                {                
                    // animate heart spine animation after eating pet food
                    FindObjectOfType<SpinePetMeter>().CallHeartAnimation();
                    pet.totalHunger_ThirstDeducted -= 1;
                } 
            }
        }
        else
        {
            if (foodStatReward != null && pet.energy > 0)
            {
                float fillAmount = foodStatReward.StatBoost(foodItemLevel);
                //Debug.Log("fill amount energy " + fillAmount);

                petInteractionController.FillEnergy(fillAmount);
                // gold reward
                if (petActionReward != null)
                {
                    // gold reward for every food eaten
                    petActionReward.FoodGoldReward((uint)foodStatReward.GoldReward(foodItemLevel));
                }   

                pet.totalEnergyDeducted += foodStatReward.StatBoost(foodItemLevel);
                if (pet.totalEnergyDeducted >= 1)
                {                
                    // animate heart spine animation after eating pet food
                    FindObjectOfType<SpinePetMeter>().CallHeartAnimation();
                    pet.totalEnergyDeducted -= 1;
                }  
            }                
        }
    }
    #endregion
}
