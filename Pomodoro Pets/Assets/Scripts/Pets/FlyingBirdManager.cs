﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class FlyingBirdManager : MonoBehaviour {

	public static FlyingBirdManager ins;

	//changed by job bird time interval to 20 minutes
	int maxTime = 1200;

	[SerializeField]
	FlyingBird flyingBirdUI;

	DateTime currentDate;
	DateTime oldDate;

	[SerializeField]
	ScrollRect petHomeScrollRect;

	public FlyingBirdLocator birdLocator;
	[SerializeField]
	GameObject giftPanel;

	FlyingBirdGift birdGift;

	[SerializeField]
	bool timeOut; //if bird was not click when pethome screen hide

	[SerializeField]
	bool active = true;

	public bool Active{
		set{ 
			active = value;
			if (active == false) {
				DOTween.Pause ("flyingBird");
			} else {
				DOTween.Play ("flyingBird");
			}
		}
		get { return active;}
	}

	public FlyingBird flyingBird{
		get{ return flyingBirdUI;}
	}

	public FlyingBirdGift flyingBirdGift{
		set{
			if (birdGift == null) {
				birdGift = value;
				
			}

		}
		get{ 
			return birdGift;
		}
	} 

	public void EnableGiftPanel(bool b)
	{
		giftPanel.SetActive (b);
	}
	public bool IsGiftActive()
	{
		if (birdGift == null) {
			return true;
		} else {
			if (birdGift.Clicked) {
				return true;
			} else {
				return false;
			}
		}
	}


	void Start()
	{
		ins = this;
		InvokeRepeating("CheckTime",1f,1f);
		//DontDestroyOnLoad (gameObject);
		//CheckTime ();
	}

	void Update()
	{
		
		if (SceneManager.GetActiveScene ().name == "Main" && active)
		{
			if((!PetHomeController.ins.isOnPetHome && !DataController.Instance.playerData.ispetHome))
			{
				timeOut = true;	
				if(!flyingBird.Clicked)// && flyingBird.gameObject.activeInHierarchy)
				{
					flyingBird.Hide ();

				}
			}
			else if(PetHomeController.ins.isOnPetHome && DataController.Instance.playerData.ispetHome)
			{
				try
				{
					timeOut = false;
					if (!flyingBird.Clicked )//&& !flyingBird.gameObject.activeInHierarchy) 
					{
						flyingBird.gameObject.SetActive (true);
						flyingBird.spineBird.gameObject.SetActive (true);
						birdLocator.gameObject.SetActive (true);
							
					}	
				}catch(Exception ex) 
				{
					//print (ex + " trigger when came from pets module.");
				}
			}
		}

	
	}

	void CheckTime()
	{
		if (SceneManager.GetActiveScene ().name == "Main" && active)
		{
			if(PetHomeController.ins.isOnPetHome)
			{
				if (GetTimeSpan().TotalSeconds >= maxTime) 
				{
					if (birdGift == null) {
						flyingBirdUI.ShowUp ();
						//birdLocator.gameObject.SetActive (true);
						birdLocator.Go ();
						Framework.SoundManager.Instance.PlaySFX ("seBirdChirp");
						DataController.Instance.playerData.fylingBirdShitDate = DateTime.Now.ToBinary ();
						if (birdGift != null) {

							birdGift.gameObject.SetActive (false);
							//print ("DISABLE GIFT HERE");
						}
						//print ("-->" + GetTimeSpan().TotalSeconds.ToString() + ">=?" + maxTime );
						//print("A");
					}
					else 
					{
						if (!birdGift.gameObject.activeInHierarchy) 
						{
							flyingBirdUI.ShowUp ();
							//birdLocator.gameObject.SetActive (true);
							birdLocator.Go ();
							Framework.SoundManager.Instance.PlaySFX ("seBirdChirp");
							DataController.Instance.playerData.fylingBirdShitDate = DateTime.Now.ToBinary ();

						}
					}

				} else {


				}
			}
		}


	}


	public TimeSpan GetTimeSpan()
	{
		currentDate = System.DateTime.Now;


		long tempLong = Convert.ToInt64 (DataController.Instance.playerData.fylingBirdShitDate);

		DateTime oldDate = DateTime.FromBinary (tempLong);
	
		TimeSpan difference = currentDate.Subtract (oldDate);
		return difference;
	
	}


	public void EnablePetHome(bool b)
	{
		//petHomeScrollRect.enabled = b;
	}

	void OnDisable()
	{
		//DataController.Instance.playerData.fylingBirdShitDate = DateTime.Now.ToBinary ();
		//print ("wowowowowoowowowoowowow" + DataController.Instance.playerData.fylingBirdShitDate);
	}

	void OnApplicationQuit()
	{

	}
}
