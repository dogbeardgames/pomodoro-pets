﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlyingBirdLocator : MonoBehaviour {

	//float leftXpos, rightXpos;
	[SerializeField]
	RectTransform left, right;

	[SerializeField]
	Vector2 leftPos, rightPos;
	//[SerializeField]
	//Transform center;

	//RectTransform rect;
	[SerializeField]
	GameObject spineExclamation;
	GameObject tempExclamation;

	float w;
	public bool activated;

	RectTransform rt;

	void Awake()
	{
		//Vector2 screenBounds = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));
		//w = rectParent.rect.width / 2;
//		w = Camera.main.pixelRect.width/2;
//		leftPos = new Vector2((w-(w * 0.2f))*-1f,0f);
//		rightPos = new Vector2(w-(w * 0.2f),0f);

		rt = GetComponent<RectTransform> ();
	}

	void Start () {
		//rect = center.GetComponent<RectTransform> ();
		//InvokeRepeating ("FindTheBird",1f,1f);
		//float x = Screen.width - (Screen.width + (Screen.width * 0.5f);
		//leftPos = new Vector2(Screen.width - (Screen.width + (Screen.width * 0.5f)), 0f);

	}

	public void ToRight()
	{
//		rt.anchorMin = new Vector2 (1f,0.5f);
//		rt.anchorMax = new Vector2 (1f,0.5f);
//		rt.localPosition = new Vector2 (0f, rt.localPosition.y);
		rt.localPosition = new Vector2(right.localPosition.x - rt.rect.width/2, rt.localPosition.y);
	}
	public void ToLeft()
	{
//		rt.anchorMin = new Vector2 (0f,0.5f);
//		rt.anchorMax = new Vector2 (0f,0.5f);
//		rt.localPosition = new Vector2 (0f, rt.localPosition.y);
		rt.localPosition = new Vector2(left.localPosition.x + rt.rect.width/2, rt.localPosition.y);
	}

	public void SetYPos(float y)
	{
//		leftPos = new Vector2 (leftPos.x, y);
//		rightPos = new Vector2 (rightPos.x, y);
		rt.localPosition = new Vector2(rt.localPosition.x, y);
	}

	// Update is called once per frame
	void FixedUpdate () {

		if(activated)
		{
			FindTheBird ();
		}
		
	}
	public void Go()
	{
		//InvokeRepeating ("FindTheBird",1f,1f);
		activated = true;
		if (tempExclamation == null) {
			tempExclamation = (GameObject)Instantiate (spineExclamation);
			//tempExclamation.transform.SetParent (transform.parent);
			tempExclamation.GetComponent<SetPositionToUIPosition> ().SetTarget (transform, false);
		} else {
			tempExclamation.SetActive (true);
		}
	}

	public GameObject BirdPointer{
		set{ tempExclamation = value;}
		get{ return tempExclamation;}
	}

	public void FindTheBird()
	{
		if (FlyingBirdManager.ins.flyingBird.gameObject.activeInHierarchy && !FlyingBirdManager.ins.flyingBird.Clicked) {
			if (Mathf.Abs (Camera.main.transform.position.x - FlyingBirdManager.ins.flyingBird.transform.position.x) >= 3f) {
				if (Camera.main.transform.position.x > FlyingBirdManager.ins.flyingBird.spineBird.transform.localPosition.x) {
					//transform.localPosition = leftPos;
					ToLeft ();
				} else {
					ToRight ();
					//transform.localPosition = rightPos;
				}
				//gameObject.SetActive (true);
				tempExclamation.SetActive (true);
			
			} else {
				//gameObject.SetActive (false);
				tempExclamation.SetActive (false);
			}

		} else {
			//gameObject.SetActive (false);
			tempExclamation.SetActive (false);
		}

	}
}
