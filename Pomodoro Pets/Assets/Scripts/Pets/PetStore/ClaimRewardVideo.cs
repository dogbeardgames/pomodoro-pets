﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClaimRewardVideo : MonoBehaviour {

	[SerializeField]
	Button btnclaimGold,btnclaimGem,btnclaimEnergyTreats;

	// Use this for initialization
	void Start () 
	{
		Initialized ();
	}
	
	//initialize

	void Initialized()
	{
		btnclaimGold.onClick.AddListener (claimGold);
		btnclaimGem.onClick.AddListener (claimGem);
		btnclaimEnergyTreats.onClick.AddListener (claimEnergyTreats);
	}

	void claimGold()
	{
		//Debug.Log ("Claim Gold");
		FindObjectOfType<VideoAds> ().ClaimFreeRewardsGold (0);
	}

	void claimGem()
	{
		Debug.Log ("Claim Gem");
		FindObjectOfType<VideoAds> ().ClaimFreeRewardsGem (1);
	}


	void claimEnergyTreats()
	{
		//Debug.Log ("Claim energytreats");
		FindObjectOfType<VideoAds> ().ClaimFreeRewardsEtreats (2);
	}

}
