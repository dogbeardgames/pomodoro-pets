﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using TMPro;
using System.Linq;
using UnityEngine.UI;

public class PetInAppStore : MonoBehaviour {

    [SerializeField]
    public enum Currency { inapp, gem };
    public Currency currency;

    [SerializeField]
    uint gemPrice;

    [SerializeField]
    string breedName;

    [SerializeField]
    EnumCollection.Pet petType;

    [SerializeField]
    TextMeshProUGUI text;

    [SerializeField]
    bool isFree;

    IAPButton iapButton;

    // Use this for initialization
    void Start () {

        Store store = FindObjectOfType<Store>();
        iapButton = GetComponent<IAPButton>();

        StoreStat();

        GetComponent<Button>().onClick.AddListener(() =>
            {
                store.storeCallBack = PetAdopted;  
                //                store.OpenDialog(this.breedName.Replace("_", " ") + "\n adopted!", () => {});
                //                Framework.SoundManager.Instance.PlaySFX("seBuy");
            });

        if(isFree)
        {
            //Debug.Log("Product is free");
            GetComponent<Button>().onClick.AddListener(ConfirmAdoptDialog);
        }
        else if (store != null && isFree == false)
        {
            if (currency == Currency.inapp)
            {
                //Debug.Log("Store not null");
                GetComponent<Button>().onClick.AddListener(ConfirmPaidAdoptionDialog);
                iapButton.GetComponent<IAPButton>().onPurchaseComplete.AddListener(store.CompletePurchase);
                iapButton.GetComponent<IAPButton>().onPurchaseFailed.AddListener(store.PurchaseFailed);
            }
            else if(currency == Currency.gem)
            {
                GetComponent<Button>().onClick.AddListener(ConfirmAdoptByGem);
            }
        }
    }

    public void StoreStat()
    {
        //Setting Item State
        //Debug.Log("callback " + this.breedName.Replace(" ", ""));

        if(petType == EnumCollection.Pet.Dog)
        {
            DogDatabase dogDB = DataController.Instance.dogDatabase;

            if(dogDB.items.First(pet => pet.breedname.ToString() == this.breedName) != null)
            {
                //Debug.Log("Has " + this.breedName);
                if(dogDB.items.First(pet => pet.breedname.ToString() == this.breedName).isOwned)
                {
                    GetComponent<Button>().interactable = false;
                    text.text = "Owned";
                    text.transform.parent.GetComponent<Image>().sprite = Store.ins.GetSpriteOwned ();
                }
            }
        }
        else if(petType == EnumCollection.Pet.Cat)
        {
            CatDatabase catDb = DataController.Instance.catDatabase;

            if(catDb.items.First(pet => pet.breedname.ToString() == this.breedName) != null)
            {
                //Debug.Log("Has " + this.breedName);
                if(catDb.items.First(pet => pet.breedname.ToString() == this.breedName).isOwned)
                {
                    GetComponent<Button>().interactable = false;
                    text.text = "Owned";
                    text.transform.parent.GetComponent<Image>().sprite = Store.ins.GetSpriteOwned();
                }
            }
        }
    }

    public void PetAdopted()
    {
        if(petType == EnumCollection.Pet.Dog)
        {
            DogDatabase dogDB = DataController.Instance.dogDatabase;

            if(dogDB.items.First(pet => pet.breedname.ToString() == this.breedName) != null)
            {
                dogDB.items.First(pet => pet.breedname.ToString() == this.breedName).isOwned = true;
                PetBase petbase = new PetBase();
                petbase.petType = EnumCollection.Pet.Dog;
                petbase.petName = this.breedName.Replace("_", " ");
                petbase.petBreed = dogDB.items.First(pet => pet.breedname.ToString() == this.breedName).breedname.ToString();
                petbase.petCategory = dogDB.items.First(pet => pet.breedname.ToString() == this.breedName).petCategory;
                petbase.hunger_thirst = 0;
                petbase.happiness = 0;
                petbase.energy = 0;
                petbase.cleanliness = 0;
                petbase.accessories = "none";
                petbase.headEquipment = "none";
                petbase.bodyEquipment = "none";
                petbase.isFoodFull = false;

                // add pet
                DataController.Instance.playerData.playerPetList.Add(petbase);
                //petbase.
            }
        }
        else if(petType == EnumCollection.Pet.Cat)
        {
            CatDatabase catDb = DataController.Instance.catDatabase;

            if(catDb.items.First(pet => pet.breedname.ToString() == this.breedName) != null)
            {
                catDb.items.First(pet => pet.breedname.ToString() == this.breedName).isOwned = true;
                PetBase petbase = new PetBase();
                petbase.petType = EnumCollection.Pet.Cat;
                petbase.petName = this.breedName.Replace("_", " ");
                petbase.petBreed = catDb.items.First(pet => pet.breedname.ToString() == this.breedName).breedname.ToString();
                petbase.petCategory = catDb.items.First(pet => pet.breedname.ToString() == this.breedName).petCategory;
                petbase.hunger_thirst = 0;
                petbase.happiness = 0;
                petbase.energy = 0;
                petbase.cleanliness = 0;
                petbase.accessories = "none";
                petbase.headEquipment = "none";
                petbase.bodyEquipment = "none";
                petbase.isFoodFull = false;

                // add pet
                DataController.Instance.playerData.playerPetList.Add(petbase);
            }
        }

        StoreStat();

        // check if in pomodoro, update pet in background if true
        if(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "Main")
        {
            PetHomeController.ins.SetPet();
        }
    }

    void ConfirmAdoptDialog()
    {                
        Framework.SoundManager.Instance.PlaySFX ("seIconTapPets");
        GameObject adoptPetObject = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), FindObjectOfType<Store>().transform.parent, false);
        ShowDialog confirmDialog = adoptPetObject.GetComponent<ShowDialog>();
        // set captions
        confirmDialog.SetCaption("Do you want to Adopt\n" + this.breedName.Replace("_", " ") + "?");
        confirmDialog.SetConfirmCaption("Adopt");
        confirmDialog.SetCancelCaption("No");
        // set events
        confirmDialog.OnYesEvent += () => 
            {                

                PetAdopted();
                Store store = FindObjectOfType<Store>();

                if(store != null)
                {
                    store.OpenDialog(this.breedName.Replace("_", " ") + "\n adopted!", () => {Destroy(adoptPetObject);});
                    Framework.SoundManager.Instance.PlaySFX("seBuy");
                }

            };
        confirmDialog.OnNoEvent += () => 
            {
                Destroy(adoptPetObject);
            };
    }

    void ConfirmPaidAdoptionDialog()
    {                       
        if (DataController.Instance.playerData.playerPetList.Count < 33) 
        {
            if (IAPButton.IAPButtonStoreManager.Instance.isInitialized)
            {
                GameObject adoptPetObject = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), FindObjectOfType<Store>().transform.parent, false);
                ShowDialog confirmDialog = adoptPetObject.GetComponent<ShowDialog>();
                //Debug.Log("<color=red>Store Not Null, BEFORE</color>");
                // set captions

                //Debug.Log("Price " + iapButton.GetProductPrice());
#if UNITY_ANDROID || UNITY_IOS
//                string currency = iapButton.GetProductPrice().Substring(3, 1);
//                //
//                confirmDialog.SetCaption("Do you want to Adopt\n" + this.breedName.Replace("_", " ") + " For " + iapButton.GetProductPrice().Replace(currency, " ") + "?");
#endif

#if UNITY_EDITOR

                confirmDialog.SetCaption("Do you want to Adopt\n" + this.breedName.Replace("_", " ") + " For " + iapButton.GetProductPrice());

#endif
                //                confirmDialog.SetCaption("Do you want to Adopt\n" + this.breedName.Replace("_", " ") + " For " + iapButton.GetProductPrice() + "?");
                confirmDialog.SetConfirmCaption("Adopt");
                confirmDialog.SetCancelCaption("No");
                // set events
                confirmDialog.OnYesEvent += () =>
                    {                    
                        Framework.SoundManager.Instance.PlaySFX("seIconTapPets");
                        iapButton.GetPurchaseProductMethod();
                        //PetAdopted();
                        Destroy(adoptPetObject);
                    };
                confirmDialog.OnNoEvent += () =>
                    {
                        Destroy(adoptPetObject);
                    };
                //Debug.Log("<color=red>Store Not Null, AFTER</color>");
            }
            else
            {
                GameObject affirmDialog = Instantiate(Resources.Load<GameObject>("Prefab/AffirmDialog"), GameObject.Find("Canvas").transform, false);
                AffirmDialog _affirmDialog = affirmDialog.GetComponent<AffirmDialog>();
                _affirmDialog.SetCaption("Can't connect to store.");
                _affirmDialog.SetConfirmCaption("OK");
                _affirmDialog.OnYesEvent += () => { Destroy(affirmDialog); };
            }       
        } 
        else 
        {
            GameObject dialog = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), GameObject.Find("Canvas").transform, false);
            ShowDialog show = dialog.GetComponent<ShowDialog>();
            show.SetCaption("Sorry! You have already reached the maximum allowed number of pets.");
            show.SetConfirmCaption ("OK");
            show.DisableNO ();
            show.OnYesEvent += delegate {
                Destroy(dialog);
            };
        }

    }

    void ConfirmAdoptByGem()
    {
        //open buy dialog box
        GameObject petshopConfirmDialog = Instantiate (Resources.Load<GameObject> ("Prefab/StoreDialog"), FindObjectOfType<Canvas> ().transform, false);
        ShowDialog confirmDialog = petshopConfirmDialog.GetComponent<ShowDialog> ();
        confirmDialog.SetCaption ("Do you want to Adopt\n" + breedName.Replace("_", " ") + " for " + gemPrice + " gems?");
        confirmDialog.SetConfirmCaption ("Adopt");
        confirmDialog.OnYesEvent += () => {
            if (DataController.Instance.playerData.gem >= gemPrice) 
            {                
                // reduce gem
                FindObjectOfType<PetActionReward>().ReduceGem(gemPrice);
                Store.ins.UpdateVirtualMoney();

                // update inventory
                //                FindObjectOfType<Store>().UpdateFoodItem();

                PetAdopted();

                PurchaseCompleteDialog();

                //achievements
                AchievementConditions.instance.GemSpent();
                Destroy (petshopConfirmDialog);
            } 
            else 
            {
                NotEnoughGemGold (currency.ToString());
            }

        };
        confirmDialog.OnNoEvent += () => {
            Destroy (petshopConfirmDialog);
        };
    }

    void PurchaseCompleteDialog()
    {
        GameObject petshopConfirmDialog = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), FindObjectOfType<Canvas>().transform, false);
        ShowDialog confirmDialog = petshopConfirmDialog.GetComponent<ShowDialog>();
        confirmDialog.SetCaption("You adopted " + breedName.Replace("_", " ") + "!");
        confirmDialog.SetConfirmCaption ("OK");
        confirmDialog.DisableNO ();
        confirmDialog.OnYesEvent += () =>
            {
                Destroy(petshopConfirmDialog);
            };
        Framework.SoundManager.Instance.PlaySFX ("seBuy");
    }

    void NotEnoughGemGold(string currency)
    {
        GameObject petshopConfirmDialog = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), FindObjectOfType<Canvas>().transform, false);
        ShowDialog confirmDialog = petshopConfirmDialog.GetComponent<ShowDialog>();
        confirmDialog.SetCaption("Not enough " + currency);
        confirmDialog.SetConfirmCaption ("OK");
        confirmDialog.DisableNO ();
        confirmDialog.OnYesEvent += () =>
            {
                Destroy(petshopConfirmDialog);
            };

    }  
}
