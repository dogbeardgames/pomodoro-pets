﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

// script for food item shop
public class FoodStoreItem : MonoBehaviour {

    public EnumCollection.Currency currency;

    Button button;

    [SerializeField]
    string itemName;

    [SerializeField]
    uint price;

	// Use this for initialization
	void Start () {
        button = GetComponent<Button>();
        button.onClick.AddListener(Purchase);
	}
	
    #region METHODS
    void Purchase()
    {
		Framework.SoundManager.Instance.PlaySFX ("seIconTapPets");
        if(currency == EnumCollection.Currency.Gem)
        {
			//open buy dialog box
			GameObject petshopConfirmDialog = Instantiate (Resources.Load<GameObject> ("Prefab/StoreDialog"), FindObjectOfType<Canvas> ().transform, false);
			ShowDialog confirmDialog = petshopConfirmDialog.GetComponent<ShowDialog> ();
			confirmDialog.SetCaption ("Buy " + itemName + " for " + price + " gems?");
			confirmDialog.SetConfirmCaption ("Buy");
			confirmDialog.OnYesEvent += () => {
				if (DataController.Instance.playerData.gem >= price) 
				{
					// add to food database
					PetFoodItem petFoodItem = DataController.Instance.foodDatabase.item.First (item => item.itemName == this.itemName);
                    if(petFoodItem != null)
                    {
					    petFoodItem.stock++;
                    }
//					DataController.Instance.playerData.gem -= price;

                    // reduce gem
                    FindObjectOfType<PetActionReward>().ReduceGem((uint)price);
					Store.ins.UpdateVirtualMoney();
					
                    // update inventory
                    FindObjectOfType<Store>().UpdateFoodItem();
					PurchaseCompleteDialog();

                    //achievements
                    AchievementConditions.instance.GemSpent();
					Destroy (petshopConfirmDialog);
				} 
				else 
				{
					NotEnoughGemGold (currency.ToString());
				}

			};
			confirmDialog.OnNoEvent += () => {
				Destroy (petshopConfirmDialog);
			};

        }
        else if(currency == EnumCollection.Currency.Gold)
        {
            try
            {
    			//open buy dialog box
    			GameObject petshopConfirmDialog = Instantiate (Resources.Load<GameObject> ("Prefab/StoreDialog"), FindObjectOfType<Canvas> ().transform, false);
    			ShowDialog confirmDialog = petshopConfirmDialog.GetComponent<ShowDialog> ();
    			confirmDialog.SetCaption ("Buy " + itemName + " for " + price + " gold?");
    			confirmDialog.SetConfirmCaption ("Buy");
    			confirmDialog.OnYesEvent += () => {
    				if (DataController.Instance.playerData.gold >= price) 
    				{
    					// add to food database
    					PetFoodItem petFoodItem = DataController.Instance.foodDatabase.item.First (item => item.itemName == this.itemName);
                        if(petFoodItem != null)
                        {
    					    petFoodItem.stock++;
                        }

    //					DataController.Instance.playerData.gold -= price;

                        // reduce gold
                        FindObjectOfType<PetActionReward>().ReduceGold((uint)price);
    					Store.ins.UpdateVirtualMoney();
    					//PetActionReward.Instance.ReduceGold(price); 

    					// update inventory
    					FindObjectOfType<Store> ().UpdateFoodItem ();
    					PurchaseCompleteDialog ();

                        //achievements
                        AchievementConditions.instance.CallRichKid();

                        //Debug.Log("<color=red>Done Purchasing!!!</color>");

    					Destroy (petshopConfirmDialog);
    				} 
    				else 
    				{
    					NotEnoughGemGold (currency.ToString());
    				}


    			};
    			confirmDialog.OnNoEvent += () => {
    				Destroy (petshopConfirmDialog);
    			};  
            }
            catch(System.Exception ex)
            {
                //Debug.Log(ex.Message);
            }
        }
    }

	void PurchaseCompleteDialog()
	{
		GameObject petshopConfirmDialog = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), FindObjectOfType<Canvas>().transform, false);
		ShowDialog confirmDialog = petshopConfirmDialog.GetComponent<ShowDialog>();
		confirmDialog.SetCaption(itemName + " acquired!");
		confirmDialog.SetConfirmCaption ("OK");
		confirmDialog.DisableNO ();
		confirmDialog.OnYesEvent += () =>
		{
			Destroy(petshopConfirmDialog);
		};
		Framework.SoundManager.Instance.PlaySFX ("seBuy");
	}

	void NotEnoughGemGold(string currency)
	{
		GameObject petshopConfirmDialog = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), FindObjectOfType<Canvas>().transform, false);
		ShowDialog confirmDialog = petshopConfirmDialog.GetComponent<ShowDialog>();
		confirmDialog.SetCaption("Not enough " + currency);
		confirmDialog.SetConfirmCaption ("OK");
		confirmDialog.DisableNO ();
		confirmDialog.OnYesEvent += () =>
		{
			Destroy(petshopConfirmDialog);
		};

	}        
    #endregion       
}
