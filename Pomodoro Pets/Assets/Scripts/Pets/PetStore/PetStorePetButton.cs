﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PetStorePetButton : MonoBehaviour {

    Button button;

	// Use this for initialization
	void Start () {
        button = GetComponent<Button>();
        button.onClick.AddListener(PetSelect);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    #region METHOD
    void PetSelect()
    {
        //Debug.Log("Selected " + transform.Find("Text").GetComponent<Text>().text);
    }
    #endregion
}
