﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Purchasing;
using System.Linq;
using UnityEngine.UI;

public class FullSetInAppStore : MonoBehaviour {

    [SerializeField]
    public enum Currency { inapp, gem };
    public Currency currency;

    [SerializeField]
    uint gemPrice;

    [SerializeField]
    string itemName;

    [SerializeField]
    TextMeshProUGUI text;

    IAPButton iapButton;

	bool isPurchased = false;
	// Use this for initialization
	void Start () {
		
        Store store = FindObjectOfType<Store>();
        iapButton = GetComponent<IAPButton>();

        StoreStat();
		if (!isPurchased) {
            if (currency == Currency.inapp)
            {
                GetComponent<Button>().onClick.AddListener(() =>
                    {
                        store.storeCallBack = ItemPurchased;
                        ItemClick();
                    });

                iapButton.GetComponent<IAPButton>().onPurchaseComplete.AddListener(store.CompletePurchase);
                iapButton.GetComponent<IAPButton>().onPurchaseFailed.AddListener(store.PurchaseFailed);
            }	
            else if(currency == Currency.gem)
            {
                GetComponent<Button>().onClick.AddListener(ItemClickGem);
            }
		} 
		else 
		{
			//MsgItemPurchased ();
		}
        
	}

    #region METHODS
    void StoreStat()
    {
        HeadDatabase headDB = DataController.Instance.headDatabase;
        BodyDatabase bodyDB = DataController.Instance.bodyDatabase;

        bool isHeadOwned = false, isBodyOwned = false, isAccessoryOwned = false;

        if(headDB.items.First(item => item.EquipmentName == this.itemName) != null)
        {
            isHeadOwned = headDB.items.First(item => item.EquipmentName == this.itemName).isOwned;
        }

        if(bodyDB.items.First(item => item.EquipmentName == this.itemName) != null)
        {
            isBodyOwned = bodyDB.items.First(item => item.EquipmentName == this.itemName).isOwned;
        }

        if(isHeadOwned & isBodyOwned)
        {
            GetComponent<Button>().interactable = false;
            text.text = "Owned";
			text.transform.parent.GetComponent<Image> ().sprite = Store.ins.GetSpriteOwned ();
			isPurchased = true;
        }            
    }



    void ItemPurchased()
    {
        //Debug.Log("Item Purchase callback item name " + this.itemName);

        HeadDatabase headDB = DataController.Instance.headDatabase;
        BodyDatabase bodyDB = DataController.Instance.bodyDatabase;
        AcceDatabase accessoriesDB = DataController.Instance.AccessoriesDatabase;

        if(headDB.items.First(item => item.EquipmentName == this.itemName) != null)
        {
            headDB.items.First(item => item.EquipmentName == this.itemName).isOwned = true;
        }

        if(bodyDB.items.First(item => item.EquipmentName == this.itemName) != null)
        {
            bodyDB.items.First(item => item.EquipmentName == this.itemName).isOwned = true;
        }

        StoreStat();
    }      

//	void MsgItemPurchased()
//	{
//		GameObject buyDialog = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), FindObjectOfType<Store>().transform.parent, false);
//		ShowDialog confirmDialog = buyDialog.GetComponent<ShowDialog>();
//		// set captions
//		confirmDialog.SetCaption("You already have this item.");
//		confirmDialog.SetConfirmCaption("OK");
//		confirmDialog.DisableNO ();
//		// set events
//		confirmDialog.OnYesEvent += () =>{ Destroy(buyDialog);};
//	}

	void ItemClick()
	{
        if (IAPButton.IAPButtonStoreManager.Instance.isInitialized)
        {
            if (!isPurchased)
            {
                GameObject buyDialog = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), FindObjectOfType<Store>().transform.parent, false);
                ShowDialog confirmDialog = buyDialog.GetComponent<ShowDialog>();
                // set captions
                string currency = iapButton.GetProductPrice().Substring(3, 1);
                confirmDialog.SetCaption(itemName + " for " + iapButton.GetProductPrice().Replace(currency, " "));
                confirmDialog.SetConfirmCaption("Buy");
                confirmDialog.SetCancelCaption("No");
                // set events
                confirmDialog.OnYesEvent += iapButton.GetPurchaseProductMethod;

                confirmDialog.OnNoEvent += () =>
                {
                    Destroy(buyDialog);
                };
                Framework.SoundManager.Instance.PlaySFX("seIconTapPets");
            }
        }
        else
        {
            GameObject affirmDialog = Instantiate(Resources.Load<GameObject>("Prefab/AffirmDialog"), GameObject.Find("Canvas").transform, false);
            AffirmDialog _affirmDialog = affirmDialog.GetComponent<AffirmDialog>();
            _affirmDialog.SetCaption("Can't connect to store.");
            _affirmDialog.SetConfirmCaption("OK");
            _affirmDialog.OnYesEvent += () => { Destroy(affirmDialog); };
        }
	}

    void ItemClickGem()
    {
        //open buy dialog box
        GameObject petshopConfirmDialog = Instantiate (Resources.Load<GameObject> ("Prefab/StoreDialog"), FindObjectOfType<Canvas> ().transform, false);
        ShowDialog confirmDialog = petshopConfirmDialog.GetComponent<ShowDialog> ();
        confirmDialog.SetCaption (itemName + " for " + gemPrice + " gems?");
        confirmDialog.SetConfirmCaption ("Buy");
        confirmDialog.SetCancelCaption("No");
        confirmDialog.OnYesEvent += () => {
            if (DataController.Instance.playerData.gem >= gemPrice) 
            {                
                // reduce gem
                FindObjectOfType<PetActionReward>().ReduceGem(gemPrice);
                Store.ins.UpdateVirtualMoney();

                // update inventory
                //                FindObjectOfType<Store>().UpdateFoodItem();

                ItemPurchased();

                PurchaseCompleteDialog();

                //achievements
                AchievementConditions.instance.GemSpent();
                Destroy (petshopConfirmDialog);
            } 
            else 
            {
                NotEnoughGemGold (currency.ToString());
            }

        };
        confirmDialog.OnNoEvent += () => {
            Destroy (petshopConfirmDialog);
        };
    }

    void PurchaseCompleteDialog()
    {
        GameObject petshopConfirmDialog = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), FindObjectOfType<Canvas>().transform, false);
        ShowDialog confirmDialog = petshopConfirmDialog.GetComponent<ShowDialog>();
        confirmDialog.SetCaption("You have purchased " + itemName + "!");
        confirmDialog.SetConfirmCaption ("OK");
        confirmDialog.DisableNO ();
        confirmDialog.OnYesEvent += () =>
            {
                Destroy(petshopConfirmDialog);
            };
        Framework.SoundManager.Instance.PlaySFX ("seBuy");
    }

    void NotEnoughGemGold(string currency)
    {
        GameObject petshopConfirmDialog = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), FindObjectOfType<Canvas>().transform, false);
        ShowDialog confirmDialog = petshopConfirmDialog.GetComponent<ShowDialog>();
        confirmDialog.SetCaption("Not enough " + currency);
        confirmDialog.SetConfirmCaption ("OK");
        confirmDialog.DisableNO ();
        confirmDialog.OnYesEvent += () =>
            {
                Destroy(petshopConfirmDialog);
            };

    } 
    #endregion      
}
