﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Purchasing;
using System.Linq;
using UnityEngine.UI;

public class BGStoreItem : MonoBehaviour {

    [SerializeField]
    string itemName;

    [SerializeField]
    TextMeshProUGUI text;

    IAPButton iapButton;

	// Use this for initialization
	void Start () {
        Store store = FindObjectOfType<Store>();
        iapButton = GetComponent<IAPButton>();

        StoreStat();

        GetComponent<Button>().onClick.AddListener(() =>
            {
                store.storeCallBack = ItemPurchased;   
                //Debug.Log("<color=red>CLICKED!</color>");
            });

        if (iapButton != null)
        {
            GetComponent<Button>().onClick.AddListener(ConfirmBackgroundPurchase);
            iapButton.GetComponent<IAPButton>().onPurchaseComplete.AddListener(store.CompletePurchase);
            iapButton.GetComponent<IAPButton>().onPurchaseFailed.AddListener(store.PurchaseFailed);
        }
	}
	
    #region METHODS

    public void StoreStat()
    {
        Backgrounds backgrounds = DataController.Instance.backgroundDatabase;
        // if equipped
        if(backgrounds.items.First(item => item.name == this.itemName).isOwned)
        {
            GetComponent<Button>().interactable = false;

            if(iapButton != null)
                iapButton.enabled = false;

            text.text = "Owned";
			text.transform.parent.GetComponent<Image>().sprite = Store.ins.GetSpriteOwned ();
        }
    }

    public void ItemPurchased()
    {
        //Debug.Log("Item purchased " + this.itemName);

        // set item as owned
        DataController.Instance.playerData.backGround = this.itemName;
        DataController.Instance.backgroundDatabase.items.First(item => item.name == this.itemName).isOwned = true;
        iapButton.enabled = false;

        //Equip();

        StoreStat();
    }

    void ConfirmBackgroundPurchase()
    {
        if (IAPButton.IAPButtonStoreManager.Instance.isInitialized)
        {
            Framework.SoundManager.Instance.PlaySFX("seIconTapPets");
            GameObject backgroundObject = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), FindObjectOfType<Store>().transform.parent, false);
            ShowDialog confirmDialog = backgroundObject.GetComponent<ShowDialog>();
            // set captions
            string currency = iapButton.GetProductPrice().Substring(3, 1);
            confirmDialog.SetCaption("Do you want to purchase\n" + itemName + " For " + iapButton.GetProductPrice().Replace(currency, " ") + "?");
            confirmDialog.SetConfirmCaption("Yes");
            confirmDialog.SetCancelCaption("No");
            // set events
            confirmDialog.OnYesEvent += () =>
            {                           
                iapButton.GetPurchaseProductMethod();
                //PetAdopted();
                Destroy(backgroundObject);
            };
            confirmDialog.OnNoEvent += () =>
            {
                Destroy(backgroundObject);
            };
        }
        else
        {
            GameObject affirmDialog = Instantiate(Resources.Load<GameObject>("Prefab/AffirmDialog"), GameObject.Find("Canvas").transform, false);
            AffirmDialog _affirmDialog = affirmDialog.GetComponent<AffirmDialog>();
            _affirmDialog.SetCaption("Can't connect to store.");
            _affirmDialog.SetConfirmCaption("OK");
            _affirmDialog.OnYesEvent += () => { Destroy(affirmDialog); };
        }
    }

//    void Equip()
//    {
//        if (DataController.Instance.backgroundDatabase.items.First(item => item.name == this.itemName).isOwned)
//        {
//            // change equip
//            string strEquippedBG = DataController.Instance.playerData.backGround;
//            DataController.Instance.backgroundDatabase.items.First(item => item.name == strEquippedBG).isEquipped = false;
//
//            // change equipped bg
//            DataController.Instance.backgroundDatabase.items.First(item => item.name == this.itemName).isEquipped = true;
//
//            // pass equipped bg to DataController
//            DataController.Instance.playerData.backGround = this.itemName;
//
//            PetUIController petUIController = FindObjectOfType<PetUIController>();
//            petUIController.SetBackground();
//            petUIController.SetAudio();
//        }
//    }
    #endregion
}
