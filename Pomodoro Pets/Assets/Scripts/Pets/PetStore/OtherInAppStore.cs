﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using TMPro;
using System.Linq;
using UnityEngine.UI;



public class OtherInAppStore : MonoBehaviour {

	[SerializeField]
	string itemDesc;

	[SerializeField]
	EnumCollection.Currency currency;

	[SerializeField]
	uint value;

	[SerializeField]
	TextMeshProUGUI text;


	IAPButton iapButton;

	void Start () {
		Store store = FindObjectOfType<Store>();
        if (store != null)
        {
            iapButton = GetComponent<IAPButton>();
                      
            GetComponent<Button>().onClick.AddListener(() =>
                {                                
                    ItemClick();
                });

            iapButton.GetComponent<IAPButton>().onPurchaseComplete.AddListener(store.CompletePurchase);
            iapButton.GetComponent<IAPButton>().onPurchaseFailed.AddListener(store.PurchaseFailed);
        }
	}		

	void ItemClick()
	{
        if (IAPButton.IAPButtonStoreManager.Instance.isInitialized)
        {
            Framework.SoundManager.Instance.PlaySFX("seIconTapPets");
            GameObject buyDialog = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), FindObjectOfType<Store>().transform.parent, false);
            ShowDialog confirmDialog = buyDialog.GetComponent<ShowDialog>();
            // set captions

             string currency = iapButton.GetProductPrice().Substring(3, 1);

            confirmDialog.SetCaption(itemDesc + " for \n" + iapButton.GetProductPrice().Replace(currency, " ") + "?");
//            confirmDialog.SetCaption(itemDesc + " for \n" + iapButton.GetProductPrice() + "?");
            confirmDialog.SetConfirmCaption("Buy");
            confirmDialog.SetCancelCaption("No");
            // set events
            confirmDialog.OnYesEvent += () =>
            {
                iapButton.GetPurchaseProductMethod();
                Destroy(buyDialog);	
            };
            confirmDialog.OnNoEvent += () =>
            {
                Destroy(buyDialog);
            };
        }
        else
        {
            GameObject affirmDialog = Instantiate(Resources.Load<GameObject>("Prefab/AffirmDialog"), GameObject.Find("Canvas").transform, false);
            AffirmDialog _affirmDialog = affirmDialog.GetComponent<AffirmDialog>();
            _affirmDialog.SetCaption("Can't connect to store.");
            _affirmDialog.SetConfirmCaption("OK");
            _affirmDialog.OnYesEvent += () => { Destroy(affirmDialog); };
        }
	}        
}
