﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BundleInAppStoreHelper{

    public delegate void bundle(BundleItem[] _bundleItem);

    Dictionary<string, bundle> bundleItemDictionary;

    public BundleInAppStoreHelper()
    {
        bundleItemDictionary = new Dictionary<string, bundle>();
        bundleItemDictionary.Add("Starter Pack", StarterPack);
        bundleItemDictionary.Add("Hoarder Pack", HoarderPack);
        bundleItemDictionary.Add("Friends in Space", FriendsInSpace);
        bundleItemDictionary.Add("Treasure Chest", TreasureChest);
        bundleItemDictionary.Add("Bucket of Treats", BucketOfTreats);
    }

    #region MyMETHODS

    void StarterPack(BundleItem[] bundleItems)
    {
        // get akita
        bundleItems[0].buttonToUpdate.GetComponent<PetInAppStore>().PetAdopted();
        bundleItems[1].buttonToUpdate.GetComponent<BGStoreItem>().ItemPurchased();

        GameObject.FindObjectOfType<PetActionReward>().UserDefinedGoldReward(15000);
        DataController.Instance.energyTreatDatabase.item[0].stock += 10; // energy treat 
        DataController.Instance.energyTreatDatabase.item[1].stock += 10; // power treat

//        Debug.Log("<color=white>item 1 featured item name " + bundleItems[0].featuredItemName + ".</color>");
//        Debug.Log("<color=white>item 2 featured item name " + bundleItems[1].featuredItemName + ".</color>");
    }

    void HoarderPack(BundleItem[] bundleItems)
    {
        GameObject.FindObjectOfType<PetActionReward>().UserDefinedGoldReward(15000);
        DataController.Instance.energyTreatDatabase.item[0].stock += 25; // energy treat 
        DataController.Instance.energyTreatDatabase.item[1].stock += 25; // power treat
        Debug.Log("HOARDD");
    }

    void FriendsInSpace(BundleItem[] bundleItems)
    {
        bundleItems[0].buttonToUpdate.GetComponent<PetInAppStore>().PetAdopted();
        bundleItems[1].buttonToUpdate.GetComponent<BGStoreItem>().ItemPurchased();
        bundleItems[2].buttonToUpdate.GetComponent<PetInAppStore>().PetAdopted();
        DataController.Instance.energyTreatDatabase.item[1].stock += 20;
    }

    void TreasureChest(BundleItem[] bundleItems)
    {
        GameObject.FindObjectOfType<PetActionReward>().UserDefinedGoldReward(75000);
        GameObject.FindObjectOfType<PetActionReward>().UserDefinedGemReward(800);
    }

    void BucketOfTreats(BundleItem[] bundleItems)
    {
        DataController.Instance.energyTreatDatabase.item[0].stock += 10;
        DataController.Instance.foodDatabase.item[4].stock += 20;
    }

    public bundle GetBundleMethod(string key)
    {
        bundle bundleMethod;
        // get methods in dictionary based on key parameter
        bundleItemDictionary.TryGetValue(key, out bundleMethod);

        return bundleMethod;
    }

    #endregion
}
