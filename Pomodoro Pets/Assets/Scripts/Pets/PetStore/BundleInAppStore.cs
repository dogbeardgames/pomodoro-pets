﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Purchasing;

public class BundleInAppStore : MonoBehaviour {

    [SerializeField]
    string productName;

    [SerializeField]
    BundleItem[] bundleItems;

    IAPButton iapButton;

	// Use this for initialization
	void Start()
    {
        Store store = FindObjectOfType<Store>();
        iapButton = GetComponent<IAPButton>();


        GetComponent<Button>().onClick.AddListener(() =>
            {
//                store.storeCallBack = () => 
//                    {
//                        bundleItems[3].buttonToUpdate.gameObject.GetComponent<PetInAppStore>().PetAdopted();
//                        bundleItems[4].buttonToUpdate.gameObject.GetComponent<BGStoreItem>().ItemPurchased();
//                    }; 
                BundleInAppStoreHelper.bundle bundleHelper;
                bundleHelper = new BundleInAppStoreHelper().GetBundleMethod(productName);
                store.storeCallBack = () => 
                    {
                        bundleHelper(bundleItems);
                    };
            });

        if(iapButton != null)
        {
            GetComponent<Button>().onClick.AddListener(Purchase);
            iapButton.GetComponent<IAPButton>().onPurchaseComplete.AddListener(store.CompletePurchase);
            iapButton.GetComponent<IAPButton>().onPurchaseFailed.AddListener(store.PurchaseFailed);
        }
	}	

    #region METHODS

    void Purchase()
    {
        if(IAPButton.IAPButtonStoreManager.Instance.isInitialized)
        {
            Framework.SoundManager.Instance.PlaySFX("seIconTapPets");
            Framework.SoundManager.Instance.PlaySFX("seIconTapPets");
            GameObject backgroundObject = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), FindObjectOfType<Store>().transform.parent, false);
            ShowDialog confirmDialog = backgroundObject.GetComponent<ShowDialog>();

            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
            {
                // set captions
                string currency = iapButton.GetProductPrice().Substring(3, 1);
                confirmDialog.SetCaption("Do you want to purchase\n" + productName + " For " + iapButton.GetProductPrice().Replace(currency, " ") + "?");
            }
            else
            {
                confirmDialog.SetCaption("Do you want to purchase\n" + productName + " For " + iapButton.GetProductPrice() + "?");
            }
            confirmDialog.SetConfirmCaption("Yes");
            confirmDialog.SetCancelCaption("No");
            // set events
            confirmDialog.OnYesEvent += () =>
                {                           
                    iapButton.GetPurchaseProductMethod();
                    //PetAdopted();
                    Destroy(backgroundObject);
                };
            confirmDialog.OnNoEvent += () =>
                {
                    Destroy(backgroundObject);
                };
        }
        else
        {
            GameObject affirmDialog = Instantiate(Resources.Load<GameObject>("Prefab/AffirmDialog"), GameObject.Find("Canvas").transform, false);
            AffirmDialog _affirmDialog = affirmDialog.GetComponent<AffirmDialog>();
            _affirmDialog.SetCaption("Can't connect to store.");
            _affirmDialog.SetConfirmCaption("OK");
            _affirmDialog.OnYesEvent += () => { Destroy(affirmDialog); };
        }
    }

    void StoreStat()
    {
        BundleItems bundleItems = new BundleItems();

    }

    #endregion
}

[System.Serializable]
public class BundleItem
{
    public Transform buttonToUpdate;
    public string featuredItemName;
    public Sprite itemImage;
    public uint quantity;
}
