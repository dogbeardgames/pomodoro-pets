﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using TMPro;

// script for food item shop
public class FoodStorePlayItem : MonoBehaviour {

	public EnumCollection.Currency currency;

	Button button;

	[SerializeField]
	string itemName;

	[SerializeField]
	uint price;

	[SerializeField]
	TextMeshProUGUI txtState;
	// Use this for initialization
	void Start () {
		button = GetComponent<Button>();

		if(DataController.Instance.playThingsDatabase.items.First (item => item.itemName == this.itemName).isOwned)
		{
			button.interactable = false;
			txtState.text = "Owned";
			//txtState.transform.parent.GetComponent<Image>().color = Color.green;
			txtState.transform.parent.GetComponent<Image>().sprite = Store.ins.GetSpriteOwned();
		}
		else
		{
			button.onClick.AddListener(Purchase);
		}
	}

	#region METHODS
	void Purchase()
	{
		Framework.SoundManager.Instance.PlaySFX ("seIconTapPets");
		if(currency == EnumCollection.Currency.Gem)
		{
			//open buy dialog box
			GameObject petshopConfirmDialog = Instantiate (Resources.Load<GameObject> ("Prefab/StoreDialog"), FindObjectOfType<Canvas> ().transform, false);
			ShowDialog confirmDialog = petshopConfirmDialog.GetComponent<ShowDialog> ();
			confirmDialog.SetCaption ("Buy " + itemName + " for " + price + " gems?");
			confirmDialog.SetConfirmCaption ("Buy");
			confirmDialog.OnYesEvent += () => {
				if (DataController.Instance.playerData.gem >= price) 
				{
                    DataController.Instance.playThingsDatabase.items.First(item => item.itemName == this.itemName).isOwned = true;

//					DataController.Instance.playerData.gem -= price;
                    // reduce gems
                    FindObjectOfType<PetActionReward>().ReduceGem(price);
					txtState.text = "Owned";
					txtState.transform.parent.GetComponent<Image>().sprite = Store.ins.GetSpriteOwned();
					button.interactable = false;
					Store.ins.UpdateVirtualMoney();
					//PetActionReward.Instance.ReduceGem(price);  
					PurchaseCompleteDialog ();

                    //achievements
                    AchievementConditions.instance.GemSpent();
                    AchievementConditions.instance.CallBigSpender();
					Destroy (petshopConfirmDialog);
				} 
				else 
				{
					NotEnoughGemGold (currency.ToString());
				}

			};
			confirmDialog.OnNoEvent += () => {
				Destroy (petshopConfirmDialog);
			};

		}
		else if(currency == EnumCollection.Currency.Gold)
		{
			//open buy dialog box
			GameObject petshopConfirmDialog = Instantiate (Resources.Load<GameObject> ("Prefab/StoreDialog"), FindObjectOfType<Canvas> ().transform, false);
			ShowDialog confirmDialog = petshopConfirmDialog.GetComponent<ShowDialog> ();
			confirmDialog.SetCaption ("Buy " + itemName + " for " + price + " gold?");
			confirmDialog.SetConfirmCaption ("Buy");
			confirmDialog.OnYesEvent += () => {
				if (DataController.Instance.playerData.gold >= price) 
				{
					
					DataController.Instance.playThingsDatabase.items.First (item => item.itemName == this.itemName).isOwned= true;

					//comment by Job. 
					//DataController.Instance.playerData.gold -= price;
					button.interactable = false;
					txtState.text = "Owned";
					txtState.transform.parent.GetComponent<Image>().sprite = Store.ins.GetSpriteOwned();
//					txtState.transform.parent.GetComponent<Image>().color = Color.green;
					Store.ins.UpdateVirtualMoney();
                    // update gold spent
                    FindObjectOfType<PetActionReward>().ReduceGold(price);
					//PetActionReward.Instance.ReduceGold(price); 

					// update inventory
					FindObjectOfType<Store> ().UpdateFoodItem ();
					PurchaseCompleteDialog ();

                    //achievements
                    AchievementConditions.instance.CallRichKid();
                    AchievementConditions.instance.CallBigSpender();
					Destroy (petshopConfirmDialog);
				} 
				else 
				{
					NotEnoughGemGold (currency.ToString());
				}


			};
			confirmDialog.OnNoEvent += () => {
				Destroy (petshopConfirmDialog);
			};




		}
	}

	void PurchaseCompleteDialog()
	{
		GameObject petshopConfirmDialog = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), FindObjectOfType<Canvas>().transform, false);
		ShowDialog confirmDialog = petshopConfirmDialog.GetComponent<ShowDialog>();
		confirmDialog.SetCaption(itemName + " acquired!");
		confirmDialog.SetConfirmCaption ("OK");
		confirmDialog.DisableNO ();
		confirmDialog.OnYesEvent += () =>
		{
            // update Item
                Store.ins.UpdatePlayThingsObject();

			Destroy(petshopConfirmDialog);
		};
		Framework.SoundManager.Instance.PlaySFX ("seBuy");
	}

	void NotEnoughGemGold(string currency)
	{
		GameObject petshopConfirmDialog = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), FindObjectOfType<Canvas>().transform, false);
		ShowDialog confirmDialog = petshopConfirmDialog.GetComponent<ShowDialog>();
		confirmDialog.SetCaption("Not enough " + currency);
		confirmDialog.SetConfirmCaption ("OK");
		confirmDialog.DisableNO ();
		confirmDialog.OnYesEvent += () =>
		{
			Destroy(petshopConfirmDialog);
		};

	}
	#endregion       
}
