﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Purchasing;
using System.Linq;
using UnityEngine.UI;

public class EquipmentInAppStore : MonoBehaviour {

    [SerializeField]
    public enum Currency { inapp, gem };
    public Currency currency;

    [SerializeField]
    uint gemPrice;

    [SerializeField]
    string itemName;

    [SerializeField]
    TextMeshProUGUI text;

    IAPButton iapButton;

    [SerializeField]
    EnumCollection.Equipment equipmentType;

	// Use this for initialization
	void Start () {
        
        Store store = FindObjectOfType<Store>();
        iapButton = GetComponent<IAPButton>();

        StoreStat();

        if (currency == Currency.inapp)
        {
            GetComponent<Button>().onClick.AddListener(() =>
                {
                    store.storeCallBack = ItemPurchased;
                    ItemClick();
                });

            iapButton.GetComponent<IAPButton>().onPurchaseComplete.AddListener(store.CompletePurchase);
            iapButton.GetComponent<IAPButton>().onPurchaseFailed.AddListener(store.PurchaseFailed);
        }
        else if(currency == Currency.gem)
        {
            GetComponent<Button>().onClick.AddListener(ItemPurchaseByGem);
        }
	}

    #region METHODS

	void ItemClick()
	{
        if (IAPButton.IAPButtonStoreManager.Instance.isInitialized)
        {
            Framework.SoundManager.Instance.PlaySFX("seIconTapPets");
            GameObject buyDialog = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), FindObjectOfType<Store>().transform.parent, false);
            ShowDialog confirmDialog = buyDialog.GetComponent<ShowDialog>();
            // set captions
            string currency = iapButton.GetProductPrice().Substring(3, 1);
            confirmDialog.SetCaption("Get " + itemName + " for " + iapButton.GetProductPrice().Replace(currency, " ") + "?");
            confirmDialog.SetConfirmCaption("Buy");
            confirmDialog.SetCancelCaption("No");
            // set events
            confirmDialog.OnYesEvent += () =>
            {
                iapButton.GetPurchaseProductMethod();
                Destroy(buyDialog);
            };

            confirmDialog.OnNoEvent += () =>
            {
                Destroy(buyDialog);
            };
        }
        else
        {
            GameObject affirmDialog = Instantiate(Resources.Load<GameObject>("Prefab/AffirmDialog"), GameObject.Find("Canvas").transform, false);
            AffirmDialog _affirmDialog = affirmDialog.GetComponent<AffirmDialog>();
            _affirmDialog.SetCaption("Can't connect to store.");
            _affirmDialog.SetConfirmCaption("OK");
            _affirmDialog.OnYesEvent += () => { Destroy(affirmDialog); };
        }
	}

    void StoreStat()
    {
        if(equipmentType == EnumCollection.Equipment.Accessories)
        {
            AcceDatabase accessoriesDB = DataController.Instance.AccessoriesDatabase;

            if(accessoriesDB.items.First(item => item.EquipmentName == this.itemName) != null)
            {
                //Debug.Log("Has " + this.itemName);
                if(accessoriesDB.items.First(item => item.EquipmentName == this.itemName).isOwned)
                {
                    GetComponent<Button>().interactable = false;
                    text.text = "Owned";
					text.transform.parent.GetComponent<Image>().sprite = Store.ins.GetSpriteOwned ();
                }
            }
        }
        else if(equipmentType == EnumCollection.Equipment.Body)
        {
            BodyDatabase bodyDB = DataController.Instance.bodyDatabase;

            if(bodyDB.items.First(item => item.EquipmentName == this.itemName) != null)
            {
                //Debug.Log("Has " + this.itemName);
                if(bodyDB.items.First(item => item.EquipmentName == this.itemName).isOwned)
                {
                    //Debug.Log("Bought " + this.itemName);

                    GetComponent<Button>().interactable = false;
                    text.text = "Owned";
					text.transform.parent.GetComponent<Image>().sprite = Store.ins.GetSpriteOwned ();
                }
            }
        }
        else if(equipmentType == EnumCollection.Equipment.Head)
        {
            HeadDatabase headDB = DataController.Instance.headDatabase;

            if(headDB.items.First(item => item.EquipmentName == this.itemName) != null)
            {
                //Debug.Log("Has " + this.itemName);
                if(headDB.items.First(item => item.EquipmentName == this.itemName).isOwned)
                {
                    GetComponent<Button>().interactable = false;
                    text.text = "Owned";
					text.transform.parent.GetComponent<Image>().sprite = Store.ins.GetSpriteOwned ();
                }
            }
        }
    }

    void ItemPurchased()
    {
        //Debug.Log("Item Purchase callback " + equipmentType + " item name " + this.itemName);

        if(equipmentType == EnumCollection.Equipment.Accessories)
        {
            AcceDatabase accessoriesDB = DataController.Instance.AccessoriesDatabase;

            if(accessoriesDB.items.First(item => item.EquipmentName == this.itemName) != null)
            {
                accessoriesDB.items.First(item => item.EquipmentName == this.itemName).isOwned = true;
            }
        }
        else if(equipmentType == EnumCollection.Equipment.Body)
        {
            BodyDatabase bodyDB = DataController.Instance.bodyDatabase;

            if(bodyDB.items.First(item => item.EquipmentName == this.itemName) != null)
            {
                bodyDB.items.First(item => item.EquipmentName == this.itemName).isOwned = true;
            }
        }
        else if(equipmentType == EnumCollection.Equipment.Head)
        {
            HeadDatabase headDB = DataController.Instance.headDatabase;

            if(headDB.items.First(item => item.EquipmentName == this.itemName) != null)
            {
                headDB.items.First(item => item.EquipmentName == this.itemName).isOwned = true;
            }
        }

        StoreStat();
    }   

    void ItemPurchaseByGem()
    {
        //open buy dialog box
        GameObject petshopConfirmDialog = Instantiate (Resources.Load<GameObject> ("Prefab/StoreDialog"), FindObjectOfType<Canvas> ().transform, false);
        ShowDialog confirmDialog = petshopConfirmDialog.GetComponent<ShowDialog> ();
        confirmDialog.SetCaption ("Get " + itemName + " for " + gemPrice + " gems?");
        confirmDialog.SetConfirmCaption ("Buy");
        confirmDialog.SetCancelCaption("No");
        confirmDialog.OnYesEvent += () => {
            if (DataController.Instance.playerData.gem >= gemPrice) 
            {                
                // reduce gem
                FindObjectOfType<PetActionReward>().ReduceGem(gemPrice);
                Store.ins.UpdateVirtualMoney();

                // update inventory
                //                FindObjectOfType<Store>().UpdateFoodItem();

                ItemPurchased();

                PurchaseCompleteDialog();

                //achievements
                AchievementConditions.instance.GemSpent();
                Destroy (petshopConfirmDialog);
            } 
            else 
            {
                NotEnoughGemGold (currency.ToString());
            }

        };
        confirmDialog.OnNoEvent += () => {
            Destroy (petshopConfirmDialog);
        };
    }

    void PurchaseCompleteDialog()
    {
        GameObject petshopConfirmDialog = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), FindObjectOfType<Canvas>().transform, false);
        ShowDialog confirmDialog = petshopConfirmDialog.GetComponent<ShowDialog>();
        confirmDialog.SetCaption("You have purchased " + itemName + "!");
        confirmDialog.SetConfirmCaption ("OK");
        confirmDialog.DisableNO ();
        confirmDialog.OnYesEvent += () =>
            {
                Destroy(petshopConfirmDialog);
            };
        Framework.SoundManager.Instance.PlaySFX ("seBuy");
    }

    void NotEnoughGemGold(string currency)
    {
        GameObject petshopConfirmDialog = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), FindObjectOfType<Canvas>().transform, false);
        ShowDialog confirmDialog = petshopConfirmDialog.GetComponent<ShowDialog>();
        confirmDialog.SetCaption("Not enough " + currency);
        confirmDialog.SetConfirmCaption ("OK");
        confirmDialog.DisableNO ();
        confirmDialog.OnYesEvent += () =>
            {
                Destroy(petshopConfirmDialog);
            };

    }  
    #endregion		
}
