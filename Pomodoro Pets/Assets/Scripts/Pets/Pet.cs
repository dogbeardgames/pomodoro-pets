﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Pet : MonoBehaviour {

    public EnumCollection.Pet petType;
    public EnumCollection.PetCategory petCategory;
//    //index in petLits
//    public int petIndex;

    public string petName;
    public string petBreed;

    public float happiness;
    public float hunger_thirst;
    public float cleanliness;
    public float energy;

    public string headEquipment;
    public string bodyEquipment;
    public string accessories;
   
    public bool isFoodFull;
    public bool isEnerygFull;

    public List<Vector3> bubblePosition;

	// Use this for initialization
	void Start () {  
        //Debug.Log("current pet index " + DataController.Instance.playerData.currentPetIndex);

        Initialize();
	}	

    public void Initialize()
    {
        EnumCollection.Pet petType = DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].petType;
        string petBreed = DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex].petBreed;

        PetController.Instance.SetPet(petType, petBreed);
    }

//    void OnMouseDown()
//    {
//        Debug.Log("Pet Clicked");
//        //GetComponent<AnimationController>().Swing();
//    }          
}
