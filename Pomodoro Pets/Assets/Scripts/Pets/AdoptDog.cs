﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;

public class AdoptDog : MonoBehaviour {

    Button button;

    [SerializeField]
    string petName;

    [SerializeField]
    EnumCollection.DogBreed dogBreed;

    [SerializeField]
    int petIndex;

    [SerializeField]
    EnumCollection.PetCategory petCategory;

    [SerializeField]
    EnumCollection.Pet petType;

	// Use this for initialization
	void Start () {
        button = GetComponent<Button>();
        button.onClick.AddListener(Adopt);
	}	

    #region METHODS
    void Adopt()
    {
        PetBase petBase = new PetBase();
//        petBase.petIndex = petIndex;
//        petBase.petTypeIndex = (int)petType;
        petBase.petName = petName;
        petBase.petBreed = dogBreed.ToString();
        petBase.petType = EnumCollection.Pet.Dog;
        petBase.petCategory = petCategory;

        DataController.Instance.playerData.playerPetList.Add(petBase);

        DataController.Instance.dogDatabase.items.First(pet => pet.breedname == dogBreed).isOwned = true;

//        Debug.Log(DataController.Instance.playerData.playerPetList[0].petIndex);
//        Debug.Log(DataController.Instance.playerData.playerPetList[0].petTypeIndex);
        //Debug.Log(DataController.Instance.playerData.playerPetList[0].petName);
//        Debug.Log(DataController.Instance.playerData.playerPetList[0].petBreed);
//
//        Debug.Log(DataController.Instance.playerData.playerPetList[0].happiness);
//        Debug.Log(DataController.Instance.playerData.playerPetList[0].hunger_thirst);
//        Debug.Log(DataController.Instance.playerData.playerPetList[0].cleanliness);
//        Debug.Log(DataController.Instance.playerData.playerPetList[0].energy);
//
//        Debug.Log(DataController.Instance.playerData.playerPetList[0].accessories);
//        Debug.Log(DataController.Instance.playerData.playerPetList[0].headEquipment);
//        Debug.Log(DataController.Instance.playerData.playerPetList[0].bodyEquipment);

        DataController.Instance.playerData.currentPetIndex = 0;

        // not fresh install anymore
        DataController.Instance.playerData.isnewInstalled = false;

        StartCoroutine("Delay");

    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(2);
        //SceneManager.LoadScene("Main", LoadSceneMode.Single);
        SceneController.Instance.LoadingScene("Main");
    }

    #endregion
}


