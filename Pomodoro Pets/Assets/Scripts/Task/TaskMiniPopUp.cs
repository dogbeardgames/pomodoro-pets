﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TaskMiniPopUp : MonoBehaviour {

    public int index;
    public GameObject taskMaintenance, btnDelete;

    Button btnYes, btnNo;
    Transform confirmationPanel;
    //private Button btnAdd;

	// Use this for initialization
	void Start () {
        transform.Find("btnEdit").GetComponent<Button>().onClick.AddListener(Edit);
        transform.Find("btnDelete").GetComponent<Button>().onClick.AddListener(Delete);
        btnDelete = transform.Find("btnDelete").gameObject;

//		if (DataController.Instance.taskList.items.Count == 1) {
//			btnDelete.gameObject.SetActive (false);
//		}

		if (btnDelete != null) {
			if (DataController.Instance.playerData.isTimerRunning) {
				btnDelete.gameObject.SetActive (false);
			}
		}

	}
	

    #region METHODS
    private void Edit()
    {
        taskMaintenance = Instantiate(taskMaintenance, transform.parent.transform.parent, false);
        taskMaintenance.GetComponent<TaskMaintenance>().SetUpEditTaskUI(index);
        taskMaintenance.GetComponent<TaskMaintenance>().SetMaintenanceStateEvent(EnumCollection.MaintenanceStates.Edit);
    }

    private void Delete()
    {        
		
		//DataController.Instance.playerData.isDeleting = true;
		GameObject.Find ("Pomodoro").GetComponent<TaskChecker> ().isDeleting = true;

        //Debug.Log("Delete");
        confirmationPanel = transform.parent.Find("ConfirmationPanel").transform;
        btnYes = confirmationPanel.Find("btnYes").GetComponent<Button>();
        btnNo = confirmationPanel.Find("btnNo").GetComponent<Button>();
        btnYes.onClick.AddListener(ConfirmDelete);
        btnNo.onClick.AddListener(CancelDelete);
        confirmationPanel.gameObject.SetActive(true);

        confirmationPanel.Find("lblTitle").GetComponent<TextMeshProUGUI>().text = "Delete Task?";
        //confirmationDialog.transform.Find()

		//test 

    }

    private void ConfirmDelete()
    {

		//DataController.Instance.playerData.isDeleting = true;
		//DataController.Instance.playerData.isAddingNewTask = true;
		GameObject.Find ("Pomodoro").GetComponent<TaskChecker> ().isDeleting = true;
		GameObject.Find ("Pomodoro").GetComponent<TaskChecker> ().isAddingNewTask = true;
			


		PomodoroUI pu = FindObjectOfType<PomodoroUI>();

        TaskPanel taskPanel = FindObjectOfType<TaskPanel>();

        //Debug.Log("Confirm delete");
        DataController.Instance.taskList.items.RemoveAt(index);

		pu.DisableNotaskPanel ();
        taskPanel.InitializeButtons();       
        Destroy(GameObject.Find("PopUp"));
        Destroy(gameObject);

		pu.TaskUI();

		//DataController.Instance.playerData.isDeleting = false;
		GameObject.Find ("Pomodoro").GetComponent<TaskChecker> ().isDeleting = false;


		if (FindObjectOfType<TaskPanel> () != null) {
			FindObjectOfType<TaskPanel> ().taskNotifPanel.transform.Find("Image").Find("TextMeshPro Text").GetComponent<TextMeshProUGUI>().text = "Task has been deleted";
			FindObjectOfType<TaskPanel> ().setActive ();

		}   

    }

    private void CancelDelete()
    {
		//DataController.Instance.playerData.isDeleting = false;
		GameObject.Find ("Pomodoro").GetComponent<TaskChecker> ().isDeleting = false;

        Destroy(GameObject.Find("PopUp"));
    }
    #endregion
}
