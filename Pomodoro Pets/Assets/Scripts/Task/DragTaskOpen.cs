﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public class DragTaskOpen : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

    Vector3 buttonPosition;
    Vector3 startingPos;
    float offSet, xAxis, touchPosOffset;
    float taskPanelOffset;

    Transform taskPanel;
    Transform defaultPos;
    // Use this for initialization
    void Start () {        
        offSet = GameObject.Find("Pomodoro").GetComponent<RectTransform>().rect.height / 4f;
        defaultPos = transform;
        //Debug.Log("<color=green>default pos " + defaultPos.position + "</color>");
    }

    // Update is called once per frame
    //  void Update () {
    //      
    //  }

    #region Interface

    #region IBeginDragHandler implementation

    public void OnBeginDrag(PointerEventData eventData)
    {      
        if(DataController.Instance.playerData.state == EnumCollection.PomodoroStates.TimerScreen ||
           DataController.Instance.playerData.state == EnumCollection.PomodoroStates.BreakScreen ||
           DataController.Instance.playerData.state == EnumCollection.PomodoroStates.LongBreakScreen)
        {
            startingPos = transform.position;
            xAxis = startingPos.x; //Camera.main.ScreenToWorldPoint(startingPos).x;
            touchPosOffset = startingPos.y - Camera.main.ScreenToWorldPoint(eventData.position).y;
            //Debug.Log(xAxis);

    //        PomodoroUI pomodoroUI = FindObjectOfType<PomodoroUI>();
    //
    //        if (PomodoroUI.activeObject == null || PomodoroUI.activeObject.name != "taskPanel")
    //        {
    //            //GameObject taskPanel = Instantiate(pomodoroUI.taskPanel, pomodoroUI.gameObject, false);
    //            GameObject taskPanel = Instantiate(pomodoroUI.taskPanel, pomodoroUI.transform, false);
    //            taskPanel.name = "taskPanel";
    //            PomodoroUI.activeObject = taskPanel;
    //
    //            taskPanelOffset = transform.position.y - taskPanel.transform.position.y;
    //        }
        }        
    }

    #endregion

    #region IDragHandler implementation

    public void OnDrag(PointerEventData eventData)
    {        

//		if (DataController.Instance.playerData.taskpanelChecker == true) {
//			eventData.pointerDrag = null;
//		}
        if(DataController.Instance.playerData.state == EnumCollection.PomodoroStates.TimerScreen ||
            DataController.Instance.playerData.state == EnumCollection.PomodoroStates.BreakScreen ||
            DataController.Instance.playerData.state == EnumCollection.PomodoroStates.LongBreakScreen)
        {
            buttonPosition = Camera.main.ScreenToWorldPoint(eventData.position);
            buttonPosition.x = xAxis;
            buttonPosition.z = startingPos.z;
            buttonPosition.y = touchPosOffset + buttonPosition.y;
            transform.position = buttonPosition;//Camera.main.ScreenToWorldPoint(buttonPosition);
//            Debug.Log("Mouse position " + Input.mousePosition);
//            Debug.Log("Button position " + transform.position);

    //        if(PomodoroUI.activeObject == null || PomodoroUI.activeObject.name == "taskPanel")
    //        {
    //            Vector3 taskPanelPos = PomodoroUI.activeObject.transform.position;
    //            taskPanelPos.y = transform.position.y - taskPanelOffset;
    //            PomodoroUI.activeObject.transform.position = taskPanelPos;
    //        }
        }
    }

    #endregion

    #region IEndDragHandler implementation

    public void OnEndDrag(PointerEventData eventData)
    {
        if (DataController.Instance.playerData.state == EnumCollection.PomodoroStates.TimerScreen ||
           DataController.Instance.playerData.state == EnumCollection.PomodoroStates.BreakScreen ||
           DataController.Instance.playerData.state == EnumCollection.PomodoroStates.LongBreakScreen)
        {
            //if()
            //Debug.Log("pos " + (startingPos.y - eventData.position.y) + ", offset " + offSet);

            //Debug.Log("<color=red> " + "drag!!!" + Mathf.Abs(startingPos.y - eventData.position.y) +  "offset " + offSet + "</color>");

//            if (Mathf.Abs(startingPos.y - eventData.position.y) >= offSet)
            if (eventData.position.y - startingPos.y >= offSet)
            {            
//            transform.DOLocalMoveY((offSet * 2) + startingPos.y, 1, false).OnComplete(() => transform.position = startingPos);
                transform.DOLocalMoveY(-720, 1, false).OnComplete(() => Destroy(PomodoroUI.activeObject));//transform.position = defaultPos.position
                //Debug.Log("<color=red> " + "local pos y " + transform.localPosition.y + "</color>");
                //Debug.Log("default pos y " + defaultPos.localPosition.y);
				PomodoroUI.Instance.isMenuActive = false;
                //PomodoroUI.activeObject.transform.DOLocalMoveY(PomodoroUI.activeObject.GetComponent<TaskPanel>().yPos, 1, false);
            }
            else
            {
                //Debug.Log("<color=red>not enough drag, drag task open.</color>");
//            transform.position = startingPos;
                transform.DOMove(startingPos, 0.5f, false);
                //            Vector3 taskPanelPos = startingPos;
                //            taskPanelPos.y = startingPos.y - taskPanelOffset;
                //            taskPanelPos.x = 0;
                ////            PomodoroUI.activeObject.transform.position = taskPanelPos;
                //            PomodoroUI.activeObject.transform.DOMove(taskPanelPos, 0.5f, false);
                ////              transform.position = startingPos;
            }
        }
    }
    #endregion
    #endregion
}
