﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using System.Linq;

public class TaskPanel : MonoBehaviour {

	public static TaskPanel ins;


    [SerializeField]
    GameObject miniPopUp, taskButton, contentParent, taskMaintenancePanel;

    GameObject objMiniPopUp;

	public GameObject taskNotifPanel;

    private int m_selectedIndex;

    public readonly float yPos = -0f;
	[SerializeField]
	List<TaskOptionsManager> lstOptionsMan;
	public List<TaskOptionsManager> ListOptionsMan{
		get{ return lstOptionsMan;}
	}

	[SerializeField]
	PanController panController;

	void Awake()
	{
		ins = this;
	}

	void Start () {
        InitializeButtons();
        CheckTaskState();
        //Debug.Log(Application.persistentDataPath);

		//setActive ();

		DataController.Instance.playerData.enableDrag = false;
	}

	public void HideOptions()
	{
		for(int i=0; i<lstOptionsMan.Count; i++)
		{
			lstOptionsMan [i].HideOptionsButton ();
			//print (lstOptionsMan[i].gameObject.name);
		}
	}
	// Update is called once per frame
	void Update () {
		
	}

	void OnDisable()
	{
		PomodoroUI.Instance.isMenuActive = false;
	}

    #region METHODS
    //create buttons for every task
    public void InitializeButtons(bool isSorting = false)
    {
        //clear child objects
        GameObject[] children = GameObject.FindGameObjectsWithTag("Task");
        contentParent.transform.DetachChildren();   
//        for (int i = 0; i < contentParent.transform.childCount; i++)
//        {
//            DestroyImmediate(contentParent.transform.GetChild(i).gameObject);
//        }            

        for (int i = 0; i < children.Length; i++)
        {
            DestroyImmediate(children[i]);
        }

		//sort tasklist by state: finished or unfinished

		//var sortedTaskList = DataController.Instance.taskList.items.OrderByDescending (x => x.isDone==false).ToList();

        List<TaskItem> doneList = new List<TaskItem>();
        List<TaskItem> notDoneList = new List<TaskItem>();
        List<TaskItem> allList = new List<TaskItem>();

        doneList = DataController.Instance.taskList.items.Where(task => task.isDone == true).ToList();
        notDoneList = DataController.Instance.taskList.items.Where(task => task.isDone == false).ToList();
        allList.AddRange(notDoneList);
        allList.AddRange(doneList);

        //Debug.Log("<color=blue> Not done " + notDoneList.Count + "</color>");
        //Debug.Log("<color=blue> Done " + doneList.Count + "</color>");
        //Debug.Log("<color=blue> Total " + allList.Count + "</color>");

        DataController.Instance.taskList.items = allList;

		lstOptionsMan.Clear ();
		for (int i = 0; i < allList.Count; i++)
        {
            GameObject button = Instantiate(taskButton, contentParent.transform, false);
			//assignment of task
            button.GetComponent<TaskButton>().SetUITask(DataController.Instance.taskList.items[i]);

			//addListener for more options
			button.GetComponent<TaskButton> ().SetIndex (button.transform.GetSiblingIndex());
			lstOptionsMan.Add (button.GetComponent<TaskButton> ().optionsManager);
//            button.transform.Find("btnMiniPopUp").GetComponent<Button>().onClick.AddListener(() => 
//                {
//                    MiniPopUp(button.transform.Find("btnMiniPopUp").GetComponent<Button>(), button.transform.GetSiblingIndex());
//                });	            
        }  
	        
		panController.UpdatePanButtons ();
    }        

    public void SortTask()
    {
        List<TaskItem> doneList = new List<TaskItem>();
        List<TaskItem> notDoneList = new List<TaskItem>();
        List<TaskItem> allList = new List<TaskItem>();

        doneList = DataController.Instance.taskList.items.Where(task => task.isDone == true).ToList();
        notDoneList = DataController.Instance.taskList.items.Where(task => task.isDone == false).ToList();
        allList.AddRange(notDoneList);
        allList.AddRange(doneList);

        //Debug.Log("<color=blue> Not done " + notDoneList.Count + "</color>");
        //Debug.Log("<color=blue> Done " + doneList.Count + "</color>");
        //Debug.Log("<color=blue> Total " + allList.Count + "</color>");

        DataController.Instance.taskList.items = allList;

        for (int i = 0; i < allList.Count; i++)
        {
            GameObject button = contentParent.transform.GetChild(i).gameObject;
               
            button.GetComponent<TaskButton>().SetUITask(DataController.Instance.taskList.items[i]);
			button.GetComponent<TaskButton> ().SetIndex (button.transform.GetSiblingIndex());

//            button.transform.Find("btnMiniPopUp").GetComponent<Button>().onClick.RemoveAllListeners();
//            button.transform.Find("btnMiniPopUp").GetComponent<Button>().onClick.AddListener(() => 
//                {
//                    MiniPopUp(button.transform.Find("btnMiniPopUp").GetComponent<Button>(), button.transform.GetSiblingIndex());
//                });             
        }
    }

	public void setActive()
	{		
		Instantiate (taskNotifPanel, GameObject.Find ("Canvas").transform, false);
		//Debug.Log ("Hello 1");
	}

    private void CheckTaskState()
    {
        if (DataController.Instance.playerData.isTimerRunning)
        {
            TextMeshProUGUI tmp = transform.Find("LblTaskBreakName").GetComponent<TextMeshProUGUI>();
            switch (DataController.Instance.playerData.state)
            {
			case EnumCollection.PomodoroStates.TimerScreen:
				PomodoroController pomodoroController = FindObjectOfType<PomodoroController> ();

				if (pomodoroController != null) 
				{
					tmp.text = "TASKS";   
				}
                break;

                case EnumCollection.PomodoroStates.BreakScreen:
				//tmp.text = "Short Break";    
                break;

                case EnumCollection.PomodoroStates.LongBreakScreen:
				//tmp.text = "Long Break";    
                break;
            }
        }            
    }

//    private void MiniPopUp(Button button, int selectedIndex)
//    {
//        Debug.Log("Mini Pop up");
//
//        objMiniPopUp = Instantiate(miniPopUp, transform.parent, false);
//        objMiniPopUp.name = "PopUp";
//        objMiniPopUp.GetComponent<Button>().onClick.AddListener(CloseMiniPopUp);
//
//        Vector3 pos = button.transform.position;
//        pos.x -= 0.5f;
//        objMiniPopUp.transform.Find("MiniPopUp").transform.position = pos;
//
//		//initialization of TaskMiniPopUp
//        TaskMiniPopUp taskMiniPopUp = objMiniPopUp.transform.Find("MiniPopUp").gameObject.AddComponent<TaskMiniPopUp>();
//        taskMiniPopUp.index = selectedIndex;
//        taskMiniPopUp.taskMaintenance = taskMaintenancePanel;
//
//        Debug.Log("Selected index " + taskMiniPopUp.index);
//    }

//    private void CloseMiniPopUp()
//    {       
//        Destroy(GameObject.Find("PopUp"));
//    }

    public void AddTask()
    {
        GameObject taskMaintenance = Instantiate(taskMaintenancePanel, transform.parent, false);
        taskMaintenance.name = "TaskMaintenance";
        taskMaintenance.GetComponent<TaskMaintenance>().SetUpAddTaskUI();
        taskMaintenance.GetComponent<TaskMaintenance>().SetMaintenanceStateEvent(EnumCollection.MaintenanceStates.Add);
		Framework.SoundManager.Instance.PlaySFX("seIconTapPomodoro");
    }

    public void ClosePanel()
    {
		//Debug.Log ("Back.......");



		if (DataController.Instance.taskList.items.Count == DataController.Instance.taskList.items.Where(item => item.isDone == true).Count()) {
			UnityEngine.SceneManagement.SceneManager.LoadScene ("Main", UnityEngine.SceneManagement.LoadSceneMode.Single);
		}

		if (DataController.Instance.playerData.taskpanelChecker == true) {
			Destroy (gameObject);
		} else {
			transform.transform.DOLocalMoveY(-720f, 1, false).OnComplete(() =>
			{
			Destroy (gameObject);
			});  
		}

		DataController.Instance.playerData.taskpanelChecker = false;
		Framework.SoundManager.Instance.PlaySFX("seIconTapPomodoro");
       
    }	
    #endregion
}
