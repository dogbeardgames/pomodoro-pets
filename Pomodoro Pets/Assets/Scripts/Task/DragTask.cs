﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;

public class DragTask : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

    Vector3 buttonPosition;
    Vector3 startingPos;
    float offSet, xAxis;
    float taskPanelOffset;

	// Use this for initialization
	void Start () {        
        offSet = GameObject.Find("Pomodoro").GetComponent<RectTransform>().rect.height / 4f;
	}
	
	// Update is called once per frame
//	void Update () {
//		
//	}

    #region Interface

    #region IBeginDragHandler implementation

    public void OnBeginDrag(PointerEventData eventData)
    {        
        if (DataController.Instance.playerData.state == EnumCollection.PomodoroStates.TimerScreen ||
           DataController.Instance.playerData.state == EnumCollection.PomodoroStates.BreakScreen ||
           DataController.Instance.playerData.state == EnumCollection.PomodoroStates.LongBreakScreen)
        {
            startingPos = transform.position;
//        xAxis = startingPos.x; //Camera.main.ScreenToWorldPoint(startingPos).x;
            //Debug.Log(xAxis);

            PomodoroUI pomodoroUI = FindObjectOfType<PomodoroUI>();

            if (PomodoroUI.activeObject == null || PomodoroUI.activeObject.name != "TaskPanel")
            {
                //GameObject taskPanel = Instantiate(pomodoroUI.taskPanel, pomodoroUI.gameObject, false);
                GameObject taskPanel = Instantiate(pomodoroUI.taskPanel, pomodoroUI.transform, false);
                taskPanel.name = "TaskPanel";
                PomodoroUI.activeObject = taskPanel;

                taskPanelOffset = transform.position.y - taskPanel.transform.position.y;
            }
        }
    }

    #endregion

    #region IDragHandler implementation

    public void OnDrag(PointerEventData eventData)
    {        
        if (DataController.Instance.playerData.state == EnumCollection.PomodoroStates.TimerScreen ||
           DataController.Instance.playerData.state == EnumCollection.PomodoroStates.BreakScreen ||
           DataController.Instance.playerData.state == EnumCollection.PomodoroStates.LongBreakScreen)
        {
            buttonPosition = Camera.main.ScreenToWorldPoint(eventData.position);
            buttonPosition.x = startingPos.x;
            buttonPosition.z = startingPos.z;
            transform.position = buttonPosition;//Camera.main.ScreenToWorldPoint(buttonPosition);
//            Debug.Log("Mouse position " + Input.mousePosition);
//            Debug.Log("Button position " + transform.position);

            if (PomodoroUI.activeObject == null || PomodoroUI.activeObject.name == "TaskPanel")
            {
                DataController.Instance.playerData.taskpanelChecker = false;

                Vector3 taskPanelPos = PomodoroUI.activeObject.transform.position;
                taskPanelPos.y = transform.position.y - taskPanelOffset;
                PomodoroUI.activeObject.transform.position = taskPanelPos;
            }
        }
    }

    #endregion

    #region IEndDragHandler implementation

    public void OnEndDrag(PointerEventData eventData)
    {
        //if()
        if (DataController.Instance.playerData.state == EnumCollection.PomodoroStates.TimerScreen ||
           DataController.Instance.playerData.state == EnumCollection.PomodoroStates.BreakScreen ||
           DataController.Instance.playerData.state == EnumCollection.PomodoroStates.LongBreakScreen)
        {
            if (eventData.position.y - startingPos.y >= offSet)
            {
                //Debug.Log("<color=red>" + "Offset" + offSet + "</color>");
				HideGoldAnimation.instance.HideAnimation ();
                transform.DOLocalMoveY(0 + startingPos.y, 1, false).OnComplete(() => transform.position = startingPos);
//            transform.DOLocalMoveY((offSet * 2) + startingPos.y, 1, false).OnComplete(() => transform.position = startingPos);
                //Debug.Log("local pos y " + PomodoroUI.activeObject.GetComponent<TaskPanel>().yPos);
                PomodoroUI.activeObject.transform.DOLocalMoveY(PomodoroUI.activeObject.GetComponent<TaskPanel>().yPos, 1, false);
				PomodoroUI.Instance.isMenuActive = true;
				//print ("Ronald");
            }
            else
            {
				//print ("Job");
                //Debug.Log("<color=red>Not engough drag, drag task</color>");
//            transform.position = startingPos;
                transform.DOMove(startingPos, 0.5f, false);
                Vector3 taskPanelPos = startingPos;
                taskPanelPos.y = startingPos.y - taskPanelOffset;
                taskPanelPos.x = 0;
//            PomodoroUI.activeObject.transform.position = new Vector3(0, 720, 0);
                PomodoroUI.activeObject.transform.DOMove(taskPanelPos, 0.5f, false);
//            PomodoroUI.activeObject.transform.position = taskPanelPos;
            }
        }
    }

    #endregion
    #endregion
}
