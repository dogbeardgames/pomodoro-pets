﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;



public class Task : MonoBehaviour {

	[SerializeField] 
	Button btnclick,btnAddTask;

	[SerializeField]
	Text texttask,texttaskdisplay ;

	[SerializeField]
	GameObject parent,text,toggledisplay;

	[SerializeField]
	Transform Panel;

	[SerializeField]
	Dropdown categoryDropdown;

	[SerializeField]
	Dropdown activePetDropdown;

	[SerializeField]
	TaskDatabase taskdb;

	[SerializeField]
	CategoryDatabase categorydb;




	TaskItem taskitem = new TaskItem();

	PlayerData playerdata = new PlayerData ();

	public static  Task Instance;
	 
	#region Methods

	void Start()
	{

		Instance = this;
	
		PopulateCategoryDropdown ();//populate category dropdown
		PopulateActivePetDropdown();//populate activePet dropdown

		btnclick.onClick.AddListener (GetText);


		for(int i = 0;i<taskdb.items.Count;i++)//display all task
		{
			
			GameObject ToggleGameObject = Instantiate (toggledisplay);
			ToggleGameObject.transform.SetParent (parent.transform);
			ToggleGameObject.GetComponent<Toggle> 
			().transform.FindChild("lblDisplaytask")
			.GetComponent<Text>().text = taskdb.items[i].task;
			DisableGameObject ();
			Panel.gameObject.SetActive (false);
            string index = taskdb.items[i].task.ToString();

		}	
			
	}

	public void GetText()//save task and display in ListView
	{
		
			Panel.gameObject.SetActive (false);
			GameObject ToggleGameObject = Instantiate (toggledisplay);
			ToggleGameObject.transform.SetParent (parent.transform);
			ToggleGameObject.GetComponent<Toggle> ().transform.FindChild("lblDisplaytask").GetComponent<Text>().text = texttask.text;
			string tasktodisplay = texttask.text;//task value to save

			taskitem.task 		= tasktodisplay;
            taskitem.categoryIndex = categoryDropdown.value;
            taskitem.activePetIndex	= activePetDropdown.value;
			taskitem.dateNow 	= System.DateTime.Now;

			taskdb.items.Add (taskitem);
        	DisableGameObject ();

	}

	void Suc()//callback 
	{
		//Debug.Log ("Successfully Saved");
		//Debug.Log (Application.persistentDataPath);
	}

	void DisableGameObject() //disable game object(Save button)
	{
		if (taskdb.items.Count == 10) {
			
			btnclick.gameObject.SetActive (false);
			btnAddTask.gameObject.SetActive (false);
			btnAddTask.gameObject.SetActive (false);
		}
	}

	public void EnableGameObject() //disable game object(Save button)
	{
			btnclick.gameObject.SetActive (true);
			btnAddTask.gameObject.SetActive (true);
			btnAddTask.gameObject.SetActive (true);
	}
		

	void PopulateCategoryDropdown()//Populate category dropdown
	{
		
		for(int x = 0;x<categorydb.items.Count ;x++)
		{
			categoryDropdown.options.Add(new Dropdown.OptionData(categorydb.items[x].category));
		}
	}

	void PopulateActivePetDropdown()//Populate category dropdown
	{

		for(int x = 0;x<DataController.Instance.playerData.playerPetList.Count ;x++)
		{
			//Debug.Log ("Hello Job");
			activePetDropdown.options.Add(new Dropdown.OptionData(DataController.Instance.playerData.playerPetList[x].petName));
		}
	}


	void OnDisable()
	{
		DataController.Instance.SavePlayerData ();
	}
		
	#endregion
}