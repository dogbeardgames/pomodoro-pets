﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TaskOptionsManager : MonoBehaviour {

	[SerializeField]
	Button btnDelete, btnEdit, btnRedo;
	[SerializeField]
	int index;

	//public GameObject taskMaintenance, btnDelete;

	Button btnYes, btnNo;

	[SerializeField]
	GameObject taskMaintenance;
	[SerializeField]
	GameObject messageDialog;
	TaskItem taskItem;

	TaskButtonOptions btnOptions;
	bool taskButtonOptionsShow;

	public void SetTaskItem(TaskItem item)
	{
		taskItem = item;
		btnOptions = GetComponent<TaskButtonOptions> ();
		btnOptions.SetTaskItem (item);
	}
	public int Index{
		set{ 
			index = value;
		}
	}

	void Start () {

		Invoke ("DisableDeleteButton", 1f);
		btnDelete.onClick.AddListener(Delete);
		btnEdit.onClick.AddListener (Edit);
		btnRedo.onClick.AddListener (ReDo);
		GetComponent<Button> ().onClick.AddListener (HideOptions);


	

	}

	void DisableDeleteButton()
	{
		if (DataController.Instance.playerData.isTimerRunning) 
		{
			//Debug.Log ("Deleting task is impossible");
			btnDelete.gameObject.SetActive (false);
		}
	}

	public void HideOptionsButton()
	{
		if (!taskButtonOptionsShow) {
			btnOptions.Hide ();
		}
		taskButtonOptionsShow = false;
	}

	void HideOptions()
	{
		//print ("");
		taskButtonOptionsShow = true;
		TaskPanel.ins.HideOptions ();

		//btnOptions.Show ();
	}

	#region METHODS
	private void Edit()
	{
		GameObject taskEdit = Instantiate(taskMaintenance, GameObject.Find("Canvas").transform, false);
		taskEdit.GetComponent<TaskMaintenance>().SetUpEditTaskUI(index);
		taskEdit.GetComponent<TaskMaintenance>().SetMaintenanceStateEvent(EnumCollection.MaintenanceStates.Edit);
		Framework.SoundManager.Instance.PlaySFX("seIconTapPomodoro");
	}

	private void Delete()
	{        

		//DataController.Instance.playerData.isDeleting = true;
		GameObject.Find ("Pomodoro").GetComponent<TaskChecker> ().isDeleting = true;

		//Debug.Log("Delete");
		GameObject showDialog = Instantiate (messageDialog, GameObject.Find("Canvas").transform, false);
		ShowDialog dialog = showDialog.GetComponent<ShowDialog>();
		dialog.SetCaption ("Are you sure you want to delete this task?");
		dialog.SetConfirmCaption ("Yes");
		dialog.SetCancelCaption("No");
		dialog.OnYesEvent += () => {
			ConfirmDelete();
			Destroy(showDialog);
		};
		dialog.OnNoEvent += () => {
			//Framework.SoundManager.Instance.PlaySFX("seIconTapPomodoro");
			Destroy(showDialog);
		};

		//confirmationPanel.gameObject.SetActive(true);

		//confirmationPanel.Find("lblTitle").GetComponent<TextMeshProUGUI>().text = "Delete Task?";
		Framework.SoundManager.Instance.PlaySFX("seIconTapPomodoro");
	}

	private void ConfirmDelete()
	{
		//DataController.Instance.playerData.isDeleting = true;
		//DataController.Instance.playerData.isAddingNewTask = true;
		GameObject.Find ("Pomodoro").GetComponent<TaskChecker> ().isDeleting = true;
		GameObject.Find ("Pomodoro").GetComponent<TaskChecker> ().isAddingNewTask = true;
	

		//Debug.Log("Confirm delete");
		DataController.Instance.taskList.items.RemoveAt(index);

		PomodoroUI.Instance.DisableNotaskPanel ();  
		TaskPanel.ins.InitializeButtons ();
		PomodoroUI.Instance.TaskUI ();

		//D/taController.Instance.playerData.isDeleting = false;
		GameObject.Find ("Pomodoro").GetComponent<TaskChecker> ().isDeleting = false;


		GameObject showDialog = Instantiate (Resources.Load<GameObject>("Prefab/StoreDialog"), GameObject.Find("Canvas").transform, false);
		ShowDialog dialog = showDialog.GetComponent<ShowDialog>();
		dialog.SetCaption ("Task has been deleted");
		dialog.SetConfirmCaption ("OK");
		dialog.DisableNO ();
		dialog.OnYesEvent += () => {
			Destroy(showDialog);
		};
//		if (FindObjectOfType<TaskPanel> () != null) {
//			TaskPanel.ins.taskNotifPanel.transform.Find("Image").Find("TextMeshPro Text").GetComponent<TextMeshProUGUI>().text = "Task has been deleted";
//			TaskPanel.ins.setActive ();
//
//		}   

	}

	public void ReDo()
	{
		TaskItem item = new TaskItem ();
		item.task = taskItem.task;
		item.categoryIndex = taskItem.categoryIndex;
		item.activePetIndex = taskItem.activePetIndex;

		DataController.Instance.taskList.items.Add (item);
		DataController.Instance.taskList.items.RemoveAt(index);

		TaskPanel.ins.InitializeButtons ();
		PomodoroUI.Instance.TaskUI ();
		PomodoroUI.Instance.DisablePlayButton ();
		PomodoroUI.Instance.DisableNotaskPanel ();
		Framework.SoundManager.Instance.PlaySFX("seIconTapPomodoro");
		//TaskPanel.ins.setActive ();
	}

	#endregion
}
