﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using System.Linq;

public class TaskButton : MonoBehaviour{

	[SerializeField]
	TaskOptionsManager optionsMan;

	[SerializeField]
	GameObject resetButton, dragPanel;


	Toggle toggle;     

	bool ishilighted = false;
	bool isPressed = false;

	float timer;
	[SerializeField]
	bool isDoneRight;
	//public static GameObject highlightedObject;

	EnumCollection.MaintenanceButton maintenanceButton;

	public TaskOptionsManager optionsManager{
		get{ return optionsMan;}
	}

	// Use this for initialization
	void Start () {
		if (!DataController.Instance.playerData.isTimerRunning) {
			GetComponent<Toggle> ().interactable = false;
		} else {
			//DataController.Instance.playerData.isAddingNewTask = true;
			GameObject.Find("Pomodoro").GetComponent<TaskChecker>().isAddingNewTask = true;

		}

        //Debug.Log("<color=red>Sibling index " + transform.GetSiblingIndex() + "</color>");

	}

	//    void Update()
	//    {
	//        if(isPressed)
	//        {
	//            if(Time.time - timer >= 1)// 1 second
	//            {
	//                Highlight();
	//                //transform.GetComponent<Toggle>().interactable = true;
	//            }
	//        }
	//    }


	public void SetIndex(int index)
	{
		//print ("INDEX is = " + index);
		optionsMan.Index = index;
	}
	#region METHODS
   
    public void SetUITask(TaskItem taskItem)
	{
		toggle = GetComponent<Toggle>();

        toggle.interactable = !toggle.isOn;
		isDoneRight = taskItem.isDone;
        // On Value Change event
        if(!taskItem.isDone)
        {                        
			optionsMan.SetTaskItem (taskItem);
            toggle.onValueChanged.AddListener(ValueChange);

        } 
        else
        {
			optionsMan.SetTaskItem (taskItem);
            toggle.onValueChanged.RemoveAllListeners();
			toggle.interactable = false;
        }

        toggle.isOn = taskItem.isDone;

		if (toggle.isOn) {
			transform.Find ("Label").GetComponent<TextMeshProUGUI> ().text =  "<s>"+taskItem.task+"</s>"; 
		} else {
			transform.Find("Label").GetComponent<TextMeshProUGUI>().text = taskItem.task;  
		}

		maintenanceButton = EnumCollection.MaintenanceButton.Task;        		                             
	}        

	void ClickAndDone()
	{
		//Task list
		//print("CLICK AND DONE");
		//Debug.Log ("Task list count " + DataController.Instance.taskList.items.Count + " Task list sibling index " + (transform.GetSiblingIndex ()));
		DataController.Instance.taskList.items [toggle.transform.GetSiblingIndex ()].dateFinished = System.DateTime.Now;
		DataController.Instance.taskList.items [toggle.transform.GetSiblingIndex ()].isDone = true;

		//save  done task  
		var comparedTask = TaskDataController.Instance.completedTaskData.taskItem.Where (y => y.task == DataController.Instance.taskList.items [transform.GetSiblingIndex ()].task).ToList ();
		//if (comparedTask.Count == 0) 
		//{

		SaveFinishTask.instance.AddTask (DataController.Instance.taskList.items [toggle.transform.GetSiblingIndex ()]);



		//DataController.Instance.playerData.isDeleting = true;
		GameObject.Find ("Pomodoro").GetComponent<TaskChecker> ().isDeleting = true;

		AchievementConditions.instance.FervidWorker();
		//AchievementConditions.instance.FirstJob ();
		//call passionate worker

		AchievementConditions.instance.Taskmaster();
		//MultiTasking
		AchievementConditions.instance.MultiTasking();

		//AchievementConditions.instance.MultiTasking();
		AchievementConditions.instance.PassionateWorker();

		//}

		//Update Pomodoro screen


		if (DataController.Instance.playerData.state == EnumCollection.PomodoroStates.TimerScreen) 
		{
			PomodoroUI.Instance.TaskUI ();
			//pu.TaskUI ();
		}

		if (DataController.Instance.playerData.state == EnumCollection.PomodoroStates.TaskScreen) 
		{
			//pu.TaskUI();
		}

		//save totalDuration
		//achievement popup

		//finish 2500 task
		//				try {
		//					if(TaskDataController.Instance.completedTaskData.taskItem.Count == 2500)
		//					{
		//					AchievementConditions.instance.Taskmaster();
		//					}
		//				} catch (System.Exception ex) {}
		//				//finish 25 task
		//				try {
		//					AchievementConditions.instance.MultiTasking();
		//				} catch (System.Exception ex) {}





		TaskPanel.ins.InitializeButtons();
	}



    public void ValueChange(bool isTrue)
	{
		
		//if (DataController.Instance.playerData.isAddingNewTask == false) 
		if (GameObject.Find("Pomodoro").GetComponent<TaskChecker>().isAddingNewTask = true) 
        {
            if (isTrue) 
            {
				GetComponent<Toggle> ().isOn = false;
				GameObject showDialog = Instantiate (Resources.Load<GameObject>("Prefab/StoreDialog"), GameObject.Find("Canvas").transform, false);
				ShowDialog dialog = showDialog.GetComponent<ShowDialog>();
				dialog.SetCaption ("Finish task?");
				dialog.SetConfirmCaption ("Yes");
				dialog.SetCancelCaption ("No");
				dialog.OnYesEvent += () => {
					ClickAndDone();
					Destroy(showDialog);
				};
				dialog.OnNoEvent += () => {
					Destroy(showDialog);
				};

			} 
            else 
            {
				DataController.Instance.taskList.items [toggle.transform.GetSiblingIndex ()].isDone = false;
			}
			Framework.SoundManager.Instance.PlaySFX("seIconTapPomodoro");
		} 
        else 
        {
			//Debug.Log ("Not adding new task");
		}      

		//RONALD commented this
       // FindObjectOfType<TaskPanel>().InitializeButtons();
	}     



	public void Reset()
	{
//		transform.GetComponent<Toggle>().interactable = true;
//		transform.Find("Label").GetComponent<TextMeshProUGUI>().fontStyle = FontStyles.Normal;
//		dragPanel.SetActive(false);
//		resetButton.SetActive(false);
//
//		FindObjectOfType<ScrollDrag>().enabled = true;       

		//highlightedObject = null;
	}

	//    void Highlight()
	//    {
	//        if (highlightedObject == null)
	//        {
	//            highlightedObject = gameObject;
	//
	//            Debug.Log("Should hilight");
	//            ishilighted = true;
	//            transform.Find("Label").GetComponent<TextMeshProUGUI>().fontStyle = FontStyles.Bold;
	//            transform.GetComponent<Toggle>().interactable = false;
	//            isPressed = false;
	//            dragPanel.SetActive(true);
	//            resetButton.SetActive(true);
	//
	//            FindObjectOfType<ScrollDrag>().enabled = false;
	//
	//            //GetComponent<Drag>().enabled = true;
	//        }
	//    }
	#endregion

	#region IPointerDownHandler implementation
	//    public void OnPointerDown(PointerEventData eventData)
	//    {
	//        if(eventData.selectedObject != null && !ishilighted)
	//        {
	//            Debug.Log("Selected " + eventData.selectedObject.name);
	//            isPressed = true;
	//            timer = Time.time;
	//        }
	//        else if(eventData.selectedObject != null && ishilighted)
	//        {
	//            Debug.Log("Back to normal");
	//            ishilighted = !ishilighted;
	//            transform.Find("Label").GetComponent<TextMeshProUGUI>().fontStyle = FontStyles.Normal;
	//        }
	//    }
	#endregion

	#region IPointerUpHandler implementation

	//    void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
	//    {
	//        if(eventData.selectedObject != null)
	//        {
	//            Debug.Log("Selected up " + eventData.selectedObject.name);
	//            isPressed = false;
	//        }
	//    }

	#endregion   
}
