﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class SaveFinishTask : MonoBehaviour {


	public static SaveFinishTask instance;


	void Start()
	{		
		instance = this;

		for (int x = 0; x < TaskDataController.Instance.completedTaskData.taskItem.Count; x++) {
		//	Debug.Log (" finished " + TaskDataController.Instance.completedTaskData.breakList[x].breakDate);
		}
			
		for (int i = 0; i < TaskDataController.Instance.completedTaskData.breakList.Count; i++) {
			//Debug.Log (" finished " + TaskDataController.Instance.completedTaskData.breakList[i].breakDate);
		}
	}
		
    public void AddTask(TaskItem item)
	{

//		float lastDuration = 0;
//
//
//		if (TaskDataController.Instance.completedTaskData.taskItem.Count > 0) {
//			var lastItem = TaskDataController.Instance.completedTaskData.taskItem.Last ();
//			lastDuration = lastItem.finishDuration;
//
//			Debug.Log ("<color=red>" + "Duration difference: "  + lastItem + "</color>");
//		}

//		TaskItem tItem = new TaskItem ();
//
//		tItem.activePetIndex = 1;
//		tItem.categoryIndex = 2;
//		tItem.task = "pomodoro3";
//		tItem.dateFinished = DateTime.Now;
//		tItem.dateNow = new DateTime(2017,02,26,01,02,00);
//		tItem.isDone = true;

		//for (int i = 2; i >= 1; i--) {
		//	for (int o = 0; o < 2; o++) {
		TaskItem tItem = new TaskItem ();

        tItem.activePetIndex = item.activePetIndex;
        tItem.categoryIndex = item.categoryIndex;
        tItem.task = item.task;
        tItem.dateFinished = DateTime.Now;
		//tItem.dateNow = new DateTime (2017, 03, 05 , 01, 02, 00);
		tItem.isDone = true;
		tItem.session = DataController.Instance.playerData.sessionCount; 

		tItem.finishDuration = DataController.Instance.playerData.durationTime; 

		TaskDataController.Instance.completedTaskData.taskItem.Add (tItem);

		//Debug.Log ("Task Added");

		//reset to 0 when task is finished
		DataController.Instance.playerData.durationTime = 0;

	}

	public void Addbreak(float breakDuration)
	{
		breakList breakList = new breakList ();
        breakList.breakTime = breakDuration;
		breakList.breakDate = DateTime.Now; //DateTime.Now;

		TaskDataController.Instance.completedTaskData.breakList.Add (breakList);
	}


	public void AddCompletedTaskDuration(float totalDuration)
	{
		completedDuration durationList = new completedDuration ();
		durationList.cDuration = totalDuration;
		durationList.completeDate = DateTime.Now; //DateTime.Now;
		durationList.session = DataController.Instance.playerData.sessionCount;

		TaskDataController.Instance.completedTaskData.completedTaskList.Add (durationList);
	}
		
}
