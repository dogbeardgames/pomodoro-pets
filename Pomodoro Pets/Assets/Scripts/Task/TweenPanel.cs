﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TweenPanel : MonoBehaviour {

	float currCountdownValue;
	bool isOn = true;
	// Use this for initialization
	void Start () {
		StartCoroutine (StartCountdown());
	}

	public void animatePanel()
	{	
		if (isOn == true) {
			transform.transform.DOScale (new Vector3 (1, 1, 1), .001f);
			isOn = false;
		} 
		else {
			transform.transform.DOScale (new Vector3 (1.1f, 1.1f, 1.1f), .001f);
			isOn = true;
		}
	}


	public IEnumerator StartCountdown(float countdownValue = 500)
	{
		currCountdownValue = countdownValue;
		while (currCountdownValue > 0)
		{
			if (currCountdownValue % .5f == 0) {
				animatePanel ();
			}
			//Debug.Log("Countdown: " + currCountdownValue);
			yield return new WaitForSeconds(1.0f);
			currCountdownValue--;
		}
	}
}
