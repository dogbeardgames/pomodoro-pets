﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using System.IO;
using System.Linq;

public class TaskMaintenance : MonoBehaviour {
	public Dictionary<string,List<string>> enumDictionaryCategory = new Dictionary<string,List<string>> ();

	[SerializeField]
	TMP_Dropdown categoryDropDown, activePetDropDown;

	[SerializeField]
	Button btnBack, btnSave;

	[SerializeField]
	TextMeshProUGUI txtTitle,textAsterisk;   

	[SerializeField]
	TMP_InputField inputField;

	//[SerializeField]
	//GameObject notificationPanel;

	int index;      

	// Use this for initialization
	void Start () {

		DataController.Instance.playerData.enableDrag = false;
	}

	// Update is called once per frame
	void Update () {

	}

	void OnDisable()
	{
		PomodoroUI.Instance.isMenuActive = false;
	}

	#region METHODS
	public void SetMaintenanceStateEvent(EnumCollection.MaintenanceStates state, System.Action _callback = null)
	{
		switch (state)
		{
		case EnumCollection.MaintenanceStates.Add:
			btnSave.onClick.AddListener(() => SaveAdd(_callback));
			break;
		case EnumCollection.MaintenanceStates.Edit:
			btnSave.onClick.AddListener(() => SaveEdit(_callback));
			break;
		default:
			break;
		}
	}

	public void UpdateCategory()
	{
		List<string> list = new List<string>();
		categoryDropDown.ClearOptions();
		for (int i = 0; i < DataController.Instance.categoryList.items.Count; i++)
		{
			list.Add(DataController.Instance.categoryList.items[i].category);
		}

		list.Add("Add Category");
		categoryDropDown.AddOptions(list);
		list.Clear();
	}

	public void SetUpAddTaskUI()
	{

		//DataController.Instance.playerData.isAddingNewTask = true;
		GameObject.Find("Pomodoro").GetComponent<TaskChecker>().isAddingNewTask = true;

		//DataController.Instance.playerData.isDeleting = true;
		GameObject.Find("Pomodoro").GetComponent<TaskChecker>().isDeleting = true;

		//Debug.Log ("<color=yellow>" + "Start" + "</color>");
		//LanguageCheckDropdown ();

		transform.Find("txtTitle").GetComponent<TextMeshProUGUI>().text = "Add Task";

		btnBack.onClick.AddListener(CloseForm);

		// add category options to drop down
		List<string> list = new List<string>();

		//list.Add("None");
		for (int i = 0; i < DataController.Instance.categoryList.items.Count; i++)
		{
			list.Add(DataController.Instance.categoryList.items[i].category);
		}


		list.Add("Add Category");
		categoryDropDown.AddOptions(list);
		list.Clear();


		//category dropdown ***done

		//Debug.Log("<color=red>" + "filtering..." + "</color>");
		var categoryDic = enumDictionaryCategory.Where (y => y.Key == "set_category").ToDictionary (y => y.Key, y => y.Value);
		//
		//		Debug.Log ("<color=red>" + "filtering..." + "</color>" + categoryDic.Count);
		//
		//
		//		foreach (List<string> listcategory in categoryDic.Values) {
		//			foreach (var dataEnum in listcategory) {
		//				list.Add((dataEnum).ToString());
		//
		//				Debug.Log ("<color=blue>" + dataEnum + "</color>");
		//			}
		//		}
		//		list.Add("Add Category");
		//		categoryDropDown.AddOptions(list);
		//
		//
		//		list.Clear();

		// add active pets options to drop down
		for (int i = 0; i < DataController.Instance.playerData.playerPetList.Count; i++)
		{
			string petbreed = DataController.Instance.playerData.playerPetList [i].petBreed.Replace ("_", " ");

			list.Add(petbreed);
		}

		activePetDropDown.AddOptions(list);
		//
	}

	public void SetUpEditTaskUI(int _index)
	{
		//UI only
		////Debug.Log("Edit! Index " + index);

		index = _index;

		//find label
		transform.Find("txtTitle").GetComponent<TextMeshProUGUI>().text = "Edit Task";
		// add listener to close button
		btnBack.onClick.AddListener(CloseForm);

		inputField.text = DataController.Instance.taskList.items[_index].task;

		//* add category options to drop down
		List<string> list = new List<string>();
		//list.Add("None");
		for (int i = 0; i < DataController.Instance.categoryList.items.Count; i++)
		{
			list.Add(DataController.Instance.categoryList.items[i].category);
		}            

		list.Add("Add Category");
		categoryDropDown.AddOptions(list);
		categoryDropDown.value = DataController.Instance.taskList.items[_index].categoryIndex;
		//*END
		list.Clear();
		//* add active pets options to drop down
		for (int i = 0; i < DataController.Instance.playerData.playerPetList.Count; i++)
		{
			string petbreed = DataController.Instance.playerData.playerPetList [i].petBreed.Replace ("_", " ");

			list.Add(petbreed);
		}

		activePetDropDown.AddOptions(list);
		activePetDropDown.value = DataController.Instance.taskList.items[_index].activePetIndex;
		//*END
	}

	public void CheckValue()
	{
		//Debug.Log(categoryDropDown.captionText.text);
		if (categoryDropDown.captionText.text == "Add Category")
		{
			categoryDropDown.value = index;
			//Debug.Log("Open Category Maintenance");
			FindObjectOfType<PomodoroUI>().InitializeCategory();
		}
	}

	public void CloseForm()
	{
		//DataController.Instance.playerData.enableDrag = true;
		Destroy(gameObject);
		Destroy(GameObject.Find("PopUp"));


	}

	void SaveAdd(System.Action callBack = null)
	{
		PomodoroUI pomodoroUI = FindObjectOfType<PomodoroUI>();
		Framework.SoundManager.Instance.PlaySFX("seIconTapPomodoro");
		if(pomodoroUI != null)
		{
			pomodoroUI.TaskUI();
			pomodoroUI.DisablePlayButton ();
			pomodoroUI.DisableNotaskPanel ();
		}
			
		//DataController.Instance.playerData.isAddingNewTask = true;
		GameObject.Find("Pomodoro").GetComponent<TaskChecker>().isAddingNewTask = true;

		//confirmDialog.SetActive(true);
		TaskItem taskItem = new TaskItem();

		//if (inputField.text.Length > 0) {
		//taskItem.task = inputField.text == "" ? inputField.transform.Find ("Placeholder").GetComponent<TextMeshProUGUI> ().text : inputField.text;
		taskItem.task = inputField.text.Trim();
		//taskItem.categoryIndex = categoryDropDown.captionText.text == "None" ? -1 : categoryDropDown.value;
		taskItem.categoryIndex = categoryDropDown.value;
		taskItem.activePetIndex = activePetDropDown.value;


		if (taskItem.task.Length == 0) {
			textAsterisk.GetComponent<TextMeshProUGUI> ().text = "Required field";

			inputField.Select ();

		} else {


			DataController.Instance.taskList.items.Add (taskItem);

			//destroy add task dialog

			if (GameObject.Find ("AlertDialog(Clone)") != null) 
			{
				Destroy (GameObject.Find ("AlertDialog(Clone)"));
			}

			if (FindObjectOfType<TaskPanel> () != null) {
				FindObjectOfType<TaskPanel> ().InitializeButtons ();
			}

			//			if (GameObject.Find("PopUp") != null)
			//			{
			//				Destroy(GameObject.Find("PopUp"));
			//			}
			//
			//			if (callBack != null)
			//				callBack();
	
			// update pet task icon
			if (pomodoroUI != null) {
				pomodoroUI.TaskUI ();
				pomodoroUI.DisablePlayButton ();
				pomodoroUI.DisableNotaskPanel ();


			}
			GameObject showDialog = Instantiate (Resources.Load<GameObject>("Prefab/StoreDialog"), GameObject.Find("Canvas").transform, false);
			ShowDialog dialog = showDialog.GetComponent<ShowDialog>();
			dialog.SetCaption ("Task has been created.");
			dialog.SetConfirmCaption ("OK");
			dialog.DisableNO ();
			dialog.OnYesEvent += () => {
				Destroy(showDialog);
			};
			Destroy (gameObject);





			//Show notif pop up
			//Instantiate (gameObject.GetComponent<TaskPanel> ().taskNotifPanel, GameObject.Find ("Canvas").transform, false);
			//notificationPanel.SetActive (true);
			//FindObjectOfType<TaskPanel>().taskNotifPanel.SetActive(true);

//			if (FindObjectOfType<TaskPanel> () != null) {
//				FindObjectOfType<TaskPanel> ().taskNotifPanel.transform.Find("Image").Find("TextMeshPro Text").GetComponent<TextMeshProUGUI>().text = "Task has been created.";
//				FindObjectOfType<TaskPanel> ().setActive ();
//
//			}   
		}
	}        

	void SaveEdit(System.Action callBack = null)
	{
		Framework.SoundManager.Instance.PlaySFX("seIconTapPomodoro");

		//DataController.Instance.playerData.isDeleting = true;
		GameObject.Find("Pomodoro").GetComponent<TaskChecker>().isDeleting = true;


		if (inputField.text.Length == 0) {
			textAsterisk.GetComponent<TextMeshProUGUI> ().text = "Required field";
			inputField.Select ();

		} else {
			//DataController.Instance.playerData.isAddingNewTask = true;
			GameObject.Find("Pomodoro").GetComponent<TaskChecker>().isAddingNewTask = true;
			DataController.Instance.taskList.items [index].task = inputField.text;
			DataController.Instance.taskList.items [index].categoryIndex = categoryDropDown.captionText.text == "None" ? -1 : categoryDropDown.value;
			DataController.Instance.taskList.items [index].activePetIndex = activePetDropDown.value;

			index = -1;

			FindObjectOfType<TaskPanel> ().InitializeButtons ();

			// update pet task icon
			PomodoroUI pomodoroUI = FindObjectOfType<PomodoroUI> ();

			if (pomodoroUI != null) {
				pomodoroUI.TaskUI ();
			}

			if (callBack != null)
				callBack ();


			GameObject showDialog = Instantiate (Resources.Load<GameObject>("Prefab/StoreDialog"), GameObject.Find("Canvas").transform, false);
			ShowDialog dialog = showDialog.GetComponent<ShowDialog>();
			dialog.SetCaption ("Task has been edited.");
			dialog.SetConfirmCaption ("OK");
			dialog.DisableNO ();
			dialog.OnYesEvent += () => {
				Destroy(showDialog);
			};
		

			Destroy (gameObject);
			Destroy (GameObject.Find ("PopUp"));

//			if (FindObjectOfType<TaskPanel> () != null) {
//				FindObjectOfType<TaskPanel> ().taskNotifPanel.transform.Find("Image").Find("TextMeshPro Text").GetComponent<TextMeshProUGUI>().text = "Task has been edited";
//				FindObjectOfType<TaskPanel> ().setActive ();
//
//			}   
		}
	}

	public void Yes()
	{
		
		Destroy(gameObject);
	}

	public void No()
	{
		//confirmDialog.SetActive(false);
	}


	//Enum Collection json file load
	public void LoadEnumJson(string fileName)
	{
		string filePath = "";

		if (Application.platform == RuntimePlatform.Android)
		{
			// Android
			filePath = System.IO.Path.Combine(Application.streamingAssetsPath, fileName);

			// Android only use WWW to read file
			WWW reader = new WWW(filePath);
			while ( ! reader.isDone) {}

			string realPath = Application.persistentDataPath + fileName;
			System.IO.File.WriteAllBytes(realPath, reader.bytes);

			filePath = realPath;
		}
		else
		{
			// iOS
			filePath = System.IO.Path.Combine(Application.streamingAssetsPath, fileName);
		}

		if (File.Exists (filePath)) {
			//Debug.Log ("Test");
			string dataAsJson = File.ReadAllText (filePath);
			LocalizationDataList enumData = JsonUtility.FromJson<LocalizationDataList> (dataAsJson);

			for (int i = 0; i < enumData.items.Length; i++) 
			{
				List<string> enumlistcat = new List<string> ();
				for (int j = 0; j < enumData.items[i].value.Length; j++) {


					//Debug.Log ("<color=green>" + "Key: " +    enumData.items [i].key +    " ALL enum data: " + enumData.items [i].value[j] + "</color>");	


					enumlistcat.Add (enumData.items [i].value [j]); 
				}

				enumDictionaryCategory.Add (enumData.items [i].key, enumlistcat);

				//enumlist.Add (enumData.items[i].value);
			}
			//			for (int i = 0; i < enumlist.Count; i++) {
			//				
			//				Debug.Log ("EnumList " + enumlist[i]);
			//			}
			//for (int i = 0; i < enumDictionary.Count; i++) {
			//	Debug.Log ("EnumDictionary Count: " + enumDictionary.Count);
			//}

			// Debug.Log ("Data loaded, dictionary contains: " + localizedText.Count + " entries");
		} else 
		{
			//Debug.LogError (filePath);
			//Debug.LogError ("Cannot find file!");
		}
	}


	void LanguageCheckDropdown()
	{
		//Localized dropdown depends on language selected in settings ***In progress 4/5/2016

		if (DataController.Instance.settings.languageIndex == 0 ) { // English
			LoadEnumJson("localizedtextDropdown_en.json");
			//Debug.Log("<color=green>" + "English" + "</color>");
		}
		else if (DataController.Instance.settings.languageIndex == 1 ) { // Spain
			LoadEnumJson("localizedtextDropdown_sp.json");
			//Debug.Log("<color=green>" + "Spanish" + "</color>");
		}
	}




	#endregion
}
