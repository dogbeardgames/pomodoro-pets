﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TaskDataController : MonoBehaviour {

	public static TaskDataController Instance;

	public TaskCompletedData completedTaskData;

	public static string filename = "pomodoroTaskFinished";


	void Awake()
	{
		if(Instance == null)
		{
			Instance = this;
		//	DontDestroyOnLoad(gameObject);
			completedTaskData = new TaskCompletedData();
			completedTaskData = Data<TaskCompletedData>.Load(filename, completedTaskData);
		}
		else
		{
			Destroy(gameObject);
		}                          
	}

//	void Start()
//	{
//		SavePlayerData();
//	}
	public void SaveFinishTaskPlayerData()
	{
		Data<TaskCompletedData>.Save(filename, completedTaskData);

		//PlayerPrefs.Save();
	}

	public void LoadPlayerData()
	{
		completedTaskData = Data<TaskCompletedData>.Load(filename, completedTaskData);
	} 

	void OnDisable()
	{
		//SaveFinishTaskPlayerData();
		//PlayerPrefs.Save();
	}

	void OnApplicationQuit()
	{
		//SaveFinishTaskPlayerData();
		//PlayerPrefs.Save();
	}
}
