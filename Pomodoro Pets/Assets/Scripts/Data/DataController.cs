﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
using System;
using UnityEngine.UI;
using UnityEngine.Purchasing;
using Facebook.Unity;

public class DataController : MonoBehaviour {

    public static DataController Instance;

    public PlayerData playerData;
    public static string filename = "pomodoro";

    public EnumCollection.PomodoroStates gameState;

    public TaskDatabase taskList;
    public CategoryDatabase categoryList;
    public Settings settings;      
    public CatDatabase catDatabase;
    public DogDatabase dogDatabase;
    public HammerData hammerDatabase;
    public AcceDatabase AccessoriesDatabase;
    public HeadDatabase headDatabase;
    public BodyDatabase bodyDatabase;
    public PetShopFoodItems foodDatabase;
    public PetPlaythings playThingsDatabase;
    public Backgrounds backgroundDatabase;
	public AchievementDatabase achievementDatabase;
    public PetHomeLocation petHomeLocation;
    public PetBathScriptableObject petBathItemDatabase;
    public PetShopItems energyTreatDatabase;
    public BundleItems bundleItemDatabase;

	int totalleftTimeonPause = 0;

    public static bool isPurchasingInitialied;

    void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            playerData = new PlayerData();
            //taskList = new TaskDatabase();
            //categoryList = new CategoryDatabase();
            //settings = new Settings();
            //catDatabase = new CatDatabase();
            //dogDatabase = new DogDatabase();
            //AccessoriesDatabase = new AcceDatabase();
            //headDatabase = new HeadDatabase();
            //bodyDatabase = new BodyDatabase();
            //foodDatabase = new PetShopFoodItems();
            //backgroundDatabase = new Backgrounds();
            //playerData = Data<PlayerData>.Load(filename, playerData);
            LoadPlayerData();
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }             
        //Debug.Log(Application.persistentDataPath);
    }
        
    void Start()
    {


        //intitalize tweeing
        DOTween.Init();

        //             initial screen
		if (playerData.playerPetList.Count == 0) {
			// call pet adopt scene
			SceneController.Instance.LoadingScene ("PetAdopt");
			// SceneManager.LoadScene("PetAdopt", LoadSceneMode.Single);
		} 

		if (playerData.isnewInstalled == true) {
			// call pet adopt scene
			SceneController.Instance.LoadingScene ("Tutorial");
		}
        else
        {
            // go to task scene
            // SceneManager.LoadScene("Main", LoadSceneMode.Single);
			if(SceneManager.GetActiveScene().name != "Main")
            SceneController.Instance.LoadingScene("Main");
        }       

        playerData.ispetHome = false;


    }
		

    public void SavePlayerData()
    {        
        Data<PlayerData>.Save(filename, playerData);
		TaskDataController.Instance.SaveFinishTaskPlayerData ();

        PlayerPrefs.DeleteAll();

        #region TASK
        PlayerPrefs.SetInt("taskCOUNT", taskList.items.Count);
        for (int i = 0; i < taskList.items.Count ; i++)
        {
            PlayerPrefs.SetInt("taskAPI" + i, taskList.items[i].activePetIndex);
            PlayerPrefs.SetInt("taskCAT" + i, taskList.items[i].categoryIndex);
            PlayerPrefs.SetString("taskDATE" + i, taskList.items[i].dateFinished.ToString());
            PlayerPrefs.SetString("taskDATENOW" + i, taskList.items[i].dateNow.ToString());
            PlayerPrefs.SetString("taskISDONE" + i, taskList.items[i].isDone.ToString());
            PlayerPrefs.SetInt("taskDATENOW" + i, (int)taskList.items[i].session);
            PlayerPrefs.SetString("taskTASK" + i, taskList.items[i].task);//taskList.items[i].task
        }
        #endregion

		#region achievement
		PlayerPrefs.SetInt("achCOUNT", achievementDatabase.items.Count);
		for (int i = 0; i < achievementDatabase.items.Count ; i++)
		{
			PlayerPrefs.SetString("achID" + i, achievementDatabase.items[i].achievementId);
			PlayerPrefs.SetString("achNAME" + i, achievementDatabase.items[i].achName);
			PlayerPrefs.SetString("achREQ" + i, achievementDatabase.items[i].requirements);
			PlayerPrefs.SetInt("achTYPE" + i, (int)achievementDatabase.items[i].achievementtype);
			PlayerPrefs.SetString("achISUNLOCKED" + i, achievementDatabase.items[i].isUnlocked.ToString());

			PlayerPrefs.SetInt("achGOLDREWARD" + i, (int)achievementDatabase.items[i].goldReward);
			PlayerPrefs.SetInt("achGEMREWARD" + i, (int)achievementDatabase.items[i].gemReward);
			PlayerPrefs.SetInt("achpigREWARD" + i, (int)achievementDatabase.items[i].pigyReward);
			PlayerPrefs.SetInt("achTARGETCOUNTER" + i,achievementDatabase.items[i].target_counter);
			PlayerPrefs.SetInt("achCOUNTER" + i, achievementDatabase.items[i].counter);
			PlayerPrefs.SetInt("achTARGETGOLD" + i, achievementDatabase.items[i].target_gold);
			PlayerPrefs.SetInt("achTARGETGEM" + i, achievementDatabase.items[i].target_gem);
			PlayerPrefs.SetString("achDATELOGIN" + i, achievementDatabase.items[i].dateLogin);
		}
		#endregion

        #region CATEGORY
        PlayerPrefs.SetInt("categoryCOUNT", categoryList.items.Count);
        for (int i = 0; i < categoryList.items.Count; i++) 
        {
            PlayerPrefs.SetString("catCAT" + i, categoryList.items[i].category);
            PlayerPrefs.SetInt("catPETINDEX" + i, categoryList.items[i].petIconIndex);
            PlayerPrefs.SetString("catLOCATION" + i, categoryList.items[i].isLocked.ToString());
        }
        #endregion
        #region SETTINGS
        PlayerPrefs.SetFloat("setALARM", settings.alarmTime);
        PlayerPrefs.SetString("setPREENDALARM", settings.isPreEndAlarmOn.ToString());
        PlayerPrefs.SetString("setVIBRATION", settings.isVibrationOn.ToString());
		PlayerPrefs.SetString("setBGM", settings.isBGMOn.ToString());
        PlayerPrefs.SetInt("setLANG", settings.languageIndex);
        PlayerPrefs.SetFloat("setLONGBREAK", settings.longBreakDuration);
        PlayerPrefs.SetInt("setNOTIFALERT", settings.notifAlertIndex);
        PlayerPrefs.SetInt("setPREENDALERT", settings.preEndAlertIndex);
        PlayerPrefs.SetFloat("setSESSIONCOUNT", settings.sessionCount);
        PlayerPrefs.SetFloat("setSESSIONDURATION", settings.sessionDuration);
        PlayerPrefs.SetFloat("setSHORTBREAKDURATION", settings.shortBreakDuration);
        PlayerPrefs.SetInt("setTHEME", settings.themeIndex);
        PlayerPrefs.SetFloat("setVOLUME", settings.volume);
        #endregion
        #region CAT
        for (int i = 0; i < catDatabase.items.Length; i++) 
        {
            PlayerPrefs.SetString("catISOWNED" + i, catDatabase.items[i].isOwned.ToString());
        }
        #endregion
        #region DOG
        for (int i = 0; i < dogDatabase.items.Length; i++) 
        {
            PlayerPrefs.SetString("dogISOWNED" + i, dogDatabase.items[i].isOwned.ToString());
        }
        #endregion  
        #region HAMMER
        for (int i = 0; i < hammerDatabase.Items.Length; i++) 
        {
            PlayerPrefs.SetString("hammerISOWNED" + i, hammerDatabase.Items[i].isOwned.ToString());
        }
        #endregion  
        #region ACCESSORIES
        for (int i = 0; i < AccessoriesDatabase.items.Length; i++) 
        {            
            PlayerPrefs.SetString("accISOWNED" + i, AccessoriesDatabase.items[i].isOwned.ToString());
        }
        #endregion
        #region HEADEQUIPMENT
        for (int i = 0; i < headDatabase.items.Length; i++) 
        {
            PlayerPrefs.SetString("headISOWNED" + i, headDatabase.items[i].isOwned.ToString());
        }
        #endregion
        #region BODYEQUIPMENT
        for (int i = 0; i < bodyDatabase.items.Length; i++) 
        {
            PlayerPrefs.SetString("bodyEQUIPMENT" + i, bodyDatabase.items[i].isOwned.ToString());
        }
        #endregion
        #region FOOD
        for (int i = 0; i < foodDatabase.item.Length; i++) 
        {
            PlayerPrefs.SetInt("foodSTOCK" + i, (int)foodDatabase.item[i].stock);
        }
        #endregion
        #region BACKGROUND
        for (int i = 0; i < backgroundDatabase.items.Length; i++) 
        {
            PlayerPrefs.SetString("bgISEQUIPPED" + i, backgroundDatabase.items[i].isEquipped.ToString());
            PlayerPrefs.SetString("bgISOWNED" + i, backgroundDatabase.items[i].isOwned.ToString());
        }
        #endregion
        #region PLAYTHINGS
        for (int i = 0; i < playThingsDatabase.items.Length; i++) 
        {
            PlayerPrefs.SetString("playThingIsOwned" + i, playThingsDatabase.items[i].isOwned.ToString());   
        }
        #endregion

        PlayerPrefs.Save();
    }

    public void LoadPlayerData()
    {
        playerData = Data<PlayerData>.Load(filename, playerData);
		//Debug.Log ("is in pet home? " + playerData.ispetHome);
        #region TASK
        int taskCOUNT = PlayerPrefs.GetInt("taskCOUNT", taskList.items.Count);

        for (int i = 0; i < taskCOUNT ; i++)
        {            
            try
            {
                taskList.items[i].activePetIndex = PlayerPrefs.GetInt("taskAPI" + i, taskList.items[i].activePetIndex);
                taskList.items[i].categoryIndex = PlayerPrefs.GetInt("taskCAT" + i, taskList.items[i].categoryIndex);
                DateTime.TryParse(PlayerPrefs.GetString("taskDATE" + i, taskList.items[i].dateFinished.ToString()), out taskList.items[i].dateFinished);
                DateTime.TryParse(PlayerPrefs.GetString("taskDATENOW" + i, taskList.items[i].dateNow.ToString()), out taskList.items[i].dateNow);
                bool.TryParse(PlayerPrefs.GetString("taskISDONE" + i, taskList.items[i].isDone.ToString()), out taskList.items[i].isDone);
                taskList.items[i].session = (uint)PlayerPrefs.GetInt("taskDATENOW" + i, (int)taskList.items[i].session);
                taskList.items[i].task = PlayerPrefs.GetString("taskTASK" + i, taskList.items[i].task);
            }
            catch(ArgumentOutOfRangeException ex)
            {                
                TaskItem taskItem = new TaskItem();
                taskItem.activePetIndex = PlayerPrefs.GetInt("taskAPI" + i);
                taskItem.categoryIndex = PlayerPrefs.GetInt("taskCAT" + i);
                DateTime.TryParse(PlayerPrefs.GetString("taskDATE" + i), out taskItem.dateFinished);
                DateTime.TryParse(PlayerPrefs.GetString("taskDATENOW" + i), out taskItem.dateNow);
                bool.TryParse(PlayerPrefs.GetString("taskISDONE" + i), out taskItem.isDone);
                taskItem.session = (uint)PlayerPrefs.GetInt("taskDATENOW" + i);
                taskItem.task = PlayerPrefs.GetString("taskTASK" + i);
                taskList.items.Add(taskItem);
            }
        }
        #endregion

		#region achievement
		int achCOUNT = PlayerPrefs.GetInt("achCOUNT", achievementDatabase.items.Count);

		for (int i = 0; i < achCOUNT ; i++)
		{            
			try
			{
				achievementDatabase.items[i].achievementId = PlayerPrefs.GetString("achID" + i, achievementDatabase.items[i].achievementId);
				achievementDatabase.items[i].achName = PlayerPrefs.GetString("achNAME" + i, achievementDatabase.items[i].achName);
				achievementDatabase.items[i].requirements = PlayerPrefs.GetString("achREQ" + i, achievementDatabase.items[i].requirements);
				achievementDatabase.items[i].achievementtype = (EnumCollection.AchievemenType)PlayerPrefs.GetInt("achTYPE" + i, (int)achievementDatabase.items[i].achievementtype);

				bool.TryParse(PlayerPrefs.GetString("achISUNLOCKED" + i, achievementDatabase.items[i].isUnlocked.ToString()), out achievementDatabase.items[i].isUnlocked);

				achievementDatabase.items[i].goldReward = (uint)PlayerPrefs.GetInt("achGOLDREWARD" + i, (int)achievementDatabase.items[i].goldReward);
				achievementDatabase.items[i].gemReward = (uint)PlayerPrefs.GetInt("achGEMREWARD" + i, (int)achievementDatabase.items[i].gemReward);
				achievementDatabase.items[i].pigyReward = (uint)PlayerPrefs.GetInt("achpigREWARD" + i, (int)achievementDatabase.items[i].pigyReward);
				achievementDatabase.items[i].target_counter = PlayerPrefs.GetInt("achTARGETCOUNTER" + i, achievementDatabase.items[i].target_counter);
				achievementDatabase.items[i].counter = PlayerPrefs.GetInt("achCOUNTER" + i, achievementDatabase.items[i].counter);
				achievementDatabase.items[i].target_gold = PlayerPrefs.GetInt("achTARGETGOLD" + i, achievementDatabase.items[i].target_gold);
				achievementDatabase.items[i].target_gem = PlayerPrefs.GetInt("achTARGETGEM" + i, achievementDatabase.items[i].target_gem);
				achievementDatabase.items[i].dateLogin = PlayerPrefs.GetString("achDATELOGIN" + i, achievementDatabase.items[i].dateLogin);
			}
			catch(ArgumentOutOfRangeException ex)
			{                
				AchievementItem achItem = new AchievementItem();

				achItem.achievementId = PlayerPrefs.GetString("achID" + i);
				achItem.achName = PlayerPrefs.GetString("achNAME" + i);
				achItem.requirements = PlayerPrefs.GetString("achREQ" + i);
				achItem.achievementtype = (EnumCollection.AchievemenType)PlayerPrefs.GetInt("achTYPE" + i);

				bool.TryParse(PlayerPrefs.GetString("achISUNLOCKED" + i), out achItem.isUnlocked);

				achItem.goldReward = (uint)PlayerPrefs.GetInt("achGOLDREWARD" + i);
				achItem.gemReward = (uint)PlayerPrefs.GetInt("achGEMREWARD" + i);
				achItem.pigyReward = (uint)PlayerPrefs.GetInt("achpigREWARD" + i);
				achItem.target_counter = PlayerPrefs.GetInt("achTARGETCOUNTER" + i);
				achItem.counter = PlayerPrefs.GetInt("achCOUNTER" + i);
				achItem.target_gold = PlayerPrefs.GetInt("achTARGETGOLD" + i);
				achItem.target_gem = PlayerPrefs.GetInt("achTARGETGEM" + i);
				achItem.dateLogin = PlayerPrefs.GetString("achDATELOGIN" + i);

				achievementDatabase.items.Add(achItem);

			}
		}
		#endregion
        #region CATEGORY
        int categoryCOUNT = PlayerPrefs.GetInt("categoryCOUNT");
        for (int i = 0; i < categoryCOUNT; i++) 
        {
            try
            {
                categoryList.items[i].category = PlayerPrefs.GetString("catCAT" + i, categoryList.items[i].category);
                categoryList.items[i].petIconIndex = PlayerPrefs.GetInt("catPETINDEX" + i, categoryList.items[i].petIconIndex);
                bool.TryParse(PlayerPrefs.GetString("catLOCATION" + i, categoryList.items[i].isLocked.ToString()), out categoryList.items[i].isLocked);
            }
            catch(ArgumentOutOfRangeException ex)
            {
                CategoryItem catItem = new CategoryItem();
                catItem.category = PlayerPrefs.GetString("catCAT" + i);
                catItem.petIconIndex = PlayerPrefs.GetInt("catPETINDEX" + i);
                bool.TryParse(PlayerPrefs.GetString("catLOCATION" + i), out catItem.isLocked);
                categoryList.items.Add(catItem);
            }
        }
        #endregion
        #region SETTINGS
        settings.alarmTime = PlayerPrefs.GetFloat("setALARM", settings.alarmTime);
        bool.TryParse(PlayerPrefs.GetString("setPREENDALARM", "false"), out settings.isPreEndAlarmOn);
        bool.TryParse(PlayerPrefs.GetString("setVIBRATION", "true"), out settings.isVibrationOn);
		bool.TryParse(PlayerPrefs.GetString("setBGM", "true"), out settings.isBGMOn);
        settings.languageIndex = PlayerPrefs.GetInt("setLANG", settings.languageIndex);
        settings.longBreakDuration = PlayerPrefs.GetFloat("setLONGBREAK", settings.longBreakDuration);
        settings.notifAlertIndex = PlayerPrefs.GetInt("setNOTIFALERT", settings.notifAlertIndex);
        settings.preEndAlertIndex = PlayerPrefs.GetInt("setPREENDALERT", settings.preEndAlertIndex);
        settings.sessionCount = PlayerPrefs.GetFloat("setSESSIONCOUNT", settings.sessionCount);
        settings.sessionDuration = PlayerPrefs.GetFloat("setSESSIONDURATION", settings.sessionDuration);
        settings.shortBreakDuration = PlayerPrefs.GetFloat("setSHORTBREAKDURATION", settings.shortBreakDuration);
        settings.themeIndex = PlayerPrefs.GetInt("setTHEME", settings.themeIndex);
        settings.volume = PlayerPrefs.GetFloat("setVOLUME", settings.volume);

        #endregion
        #region CAT
        for (int i = 0; i < catDatabase.items.Length; i++) 
        {            
            bool.TryParse(PlayerPrefs.GetString("catISOWNED" + i, catDatabase.items[i].isOwned.ToString()), out catDatabase.items[i].isOwned);
        }
        #endregion
        #region DOG
        for (int i = 0; i < dogDatabase.items.Length; i++) 
        {            
            bool.TryParse(PlayerPrefs.GetString("dogISOWNED" + i, dogDatabase.items[i].isOwned.ToString()), out dogDatabase.items[i].isOwned);
        }
        #endregion
        #region HAMMER
        for (int i = 0; i < hammerDatabase.Items.Length; i++) 
        {            
            bool.TryParse(PlayerPrefs.GetString("hammerISOWNED" + i, hammerDatabase.Items[i].isOwned.ToString()), out hammerDatabase.Items[i].isOwned);
        }
        #endregion
        #region ACCESSORIES
        for (int i = 0; i < AccessoriesDatabase.items.Length; i++) 
        {
            bool.TryParse(PlayerPrefs.GetString("accISOWNED" + i, AccessoriesDatabase.items[i].isOwned.ToString()), out AccessoriesDatabase.items[i].isOwned);
        }
        #endregion
        #region HEADEQUIPMENT
        for (int i = 0; i < headDatabase.items.Length; i++) 
        {            
            bool.TryParse(PlayerPrefs.GetString("headISOWNED" + i, headDatabase.items[i].isOwned.ToString()), out headDatabase.items[i].isOwned);
        }
        #endregion
        #region BODYEQUIPMENT
        for (int i = 0; i < bodyDatabase.items.Length; i++) 
        {
            bool.TryParse(PlayerPrefs.GetString("bodyEQUIPMENT" + i, bodyDatabase.items[i].isOwned.ToString()), out bodyDatabase.items[i].isOwned);
        }
        #endregion
        #region FOOD
        for (int i = 0; i < foodDatabase.item.Length; i++) 
        {
            foodDatabase.item[i].stock = (uint)PlayerPrefs.GetInt("foodSTOCK" + i, (int)foodDatabase.item[i].stock);
        }
        #endregion
        #region BACKGROUND
        for (int i = 0; i < backgroundDatabase.items.Length; i++) 
        {
            bool.TryParse(PlayerPrefs.GetString("bgISEQUIPPED" + i, backgroundDatabase.items[i].isEquipped.ToString()), out backgroundDatabase.items[i].isEquipped);
            bool.TryParse(PlayerPrefs.GetString("bgISOWNED" + i, backgroundDatabase.items[i].isOwned.ToString()), out backgroundDatabase.items[i].isOwned);
        }
        #endregion
        #region PLAYTHINGS
        for (int i = 0; i < playThingsDatabase.items.Length; i++) 
        {
            bool.TryParse(PlayerPrefs.GetString("playThingIsOwned" + i, playThingsDatabase.items[i].isOwned.ToString()), out playThingsDatabase.items[i].isOwned);
        }
        #endregion
        //Debug.Log("data loaded");

        //Debug.Log("task " + taskList.items[0].task);
        //taskDB.items[0].task = "huehue";
    } 

    void OnApplicationQuit()
    {				
		SavePlayerData();
    }           

    void OnDisable()
    {
		CheckTimeMaster.instance.SaveDate();       		
        //Debug.Log("On disable");
        SavePlayerData();
    }               

    #region SAVE

//    public void Save

    #endregion

	#region onApplicationPause
	bool isPaused = false;

	void SavePauseTime()
	{		
		//paused
		if (isPaused) {
			CheckTimeMaster.instance.SaveDate ();		
            if (FindObjectOfType<Timer>() != null)
            {
                playerData.timeLeftOnExit = (int)FindObjectOfType<Timer>().endTime;
            }
            
		}

		//resume
		else if(!isPaused)
		{
			CheckTimeMaster.instance.CheckDate();

			//Debug.Log("<color=red>" +"Time on pause: " + CheckTimeMaster.instance.CheckDate() +  "</color>");
			//Debug.Log (CheckTimeMaster.instance.CheckDate ());

			try 
            {
				if(DataController.Instance.playerData.isPaused==true)
				{
					FindObjectOfType<Timer> ().endTime = DataController.Instance.playerData.timeLeftOnExit - CheckTimeMaster.instance.CheckDate ();
				}
				else{
					FindObjectOfType<Timer> ().endTime = DataController.Instance.playerData.timeLeftOnExit - 0;
				}
			} 
            catch (Exception ex) 
            {
				//Debug.Log (ex.Message);
			}				
		}

		//print (gameObject.name);
	}

	void OnApplicationFocus(bool hasFocus)
	{
		isPaused = !hasFocus;
		SavePauseTime ();

	}

	void OnApplicationPause(bool pauseStatus)
	{
		isPaused = pauseStatus;
		DataController.Instance.playerData.timeState = "Pause";

		SavePauseTime ();

        SavePlayerData();
	}
	#endregion    		
}
		

