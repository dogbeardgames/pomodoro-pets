﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TimeSave : MonoBehaviour {

	public float savedTime;
	public static TimeSave instance;
	// Use this for initialization
	void Start () {
		instance = this;
		//Update real time
		savedTime=CheckTimeMaster.instance.CheckDate();
		//PlayerPrefs.DeleteKey ("saveLocation");
	}

//	void OnGUI()
//	{
//
//		GUI.Label (new Rect (10, 150, 100, 50), Convert.ToInt64(savedTime).ToString ());
//	}

	void OnApplicationQuit()
	{
		CheckTimeMaster.instance.SaveDate ();
	}
		
}
