﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class gaugeInitialValue : MonoBehaviour {

	// Use this for initialization
	void Start () {
		DeductfromStart ();
	}
	
	void DeductfromStart()
	{
		//List<PetBase> pBase = new List<PetBase> ();
        PetBase pBase = new PetBase();


		//pBase = DataController.Instance.playerData.playerPetList.Where(x=>x.petIndex == DataController.Instance.playerData.currentPetIndex).ToList();
        pBase = DataController.Instance.playerData.playerPetList[DataController.Instance.playerData.currentPetIndex];

		float cleanliness = 0;
		float happiness   = 0;
		float hunger      = 0;
		float energy      = 0;

        cleanliness   = pBase.cleanliness;
        happiness     = pBase.happiness;
        hunger        = pBase.hunger_thirst;
        energy        = pBase.energy;

//		for (int i = 0; i < pBase.Count(); i++) {
//
//			cleanliness   = pBase[i].cleanliness;
//			happiness     = pBase[i].happiness;
//			hunger        = pBase[i].hunger_thirst;
//			energy        = pBase[i].energy;
//
//
//			//Debug.Log (cleanliness + " " + " " +  happiness + " " + hunger + " " + energy);
//
//		}

		float totalStats = cleanliness + happiness + hunger + energy;
		float totalgaugededuct = totalStats * 10; 

		RotatePawIcon.instance.BarValue -=  totalgaugededuct; 
	}
}
