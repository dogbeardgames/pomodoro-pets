﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData  {

    public int ownedSlot = 1;
    public int openSlot = 1;
    public int maxSlot = 4;
    public int currentPetIndex = -1;	
   	public int cycle_counter_timer;
    public int currentSessionCount = 1;
    public int currentTaskIndex = 0;   

    public uint sessionCount = 1;

    public string backGround = "Generic";

	public bool ispetHome = false;

    //currency
    public uint gold = 0;
    public uint gem = 0;
	public uint pigyBank = 0;


	//gem acquired and gold acquired
	public uint goldacquired = 0;
	public uint gemacquired = 0;

	//gem spent and gold spent
	public uint goldspent= 0;
	public uint gemspent = 0;


	public int targetGoldPerDay = 0;
	public string DateoftheDay = "none";

	public bool isnewInstalled = true;

    public List<PetBase> playerPetList = new List<PetBase>();

    public bool isTimerRunning = false; 

	public int timeLeftOnExit = 0;

    public EnumCollection.PomodoroStates state;

	public bool sessionRunning = false;    	

	public bool isTimerExtended = false;

	public int timeExtended = 0;

    public bool isTakingScreenShot = false;

    public bool isPaused = false;

	public string timeState = "";

	public float durationTime = 0;

	public int breakDuration = 0;

	public float goldTimer = 0;

	public bool isSkipped = false;

	public bool isTimeExtended = false;


	//use to check if dragging ends
	public bool isDragEnd = false;



	public bool isfromIndoor = false;




	public string datePaused = "none";
	public string dateResumed = "none";

	public float totalpauseTime = 0;

	public string closingState = "";




	public string pauseTime = "";

	public float pauseDate = 0;



	public long fylingBirdShitDate;

	public bool enableDrag = true;

	public bool taskpanelChecker = true;

	public string savedTime = "";


}
