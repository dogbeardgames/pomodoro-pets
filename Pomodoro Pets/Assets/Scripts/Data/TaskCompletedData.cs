﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[System.Serializable]
public class TaskCompletedData {

	public List<TaskItem> taskItem = new List<TaskItem>();

	public List<breakList> breakList = new List<breakList>();

	public List<completedDuration> completedTaskList = new List<completedDuration>();

}


[System.Serializable]
public class breakList
{
	public float breakTime;
	public DateTime breakDate;
}

[System.Serializable]
public class completedDuration
{
	public float cDuration;
	public DateTime completeDate;
	public uint session;
}


