﻿using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Runtime.Serialization;

public class Data<T> {

    //#endregion

	//Save Generic Type
	public static void Save(string filename, T data, Action action)// Action = callback
    {     
        try
        {
            if(filename == "" || filename == null)
            {
                filename = Application.productName;
            }

            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath + "/" + filename + ".db");        

            binaryFormatter.Serialize(file, data);
            file.Close();

            if (action != null)
                action();
            }
        catch(SerializationException se)
        {
            //Debug.Log("Error opening file due to serialization " + se.Source);
        }
        catch(Exception se)
        {
            //Debug.Log("Error opening file due to serialization " +  se.Source);
        }
    }
	
	//Save Generic Type
	public static void Save(string filename, T data)// Action = callback
	{     
        try
        {
    		if(filename == "" || filename == null)
    		{
    			filename = Application.productName;
    		}

    		BinaryFormatter binaryFormatter = new BinaryFormatter();
    		FileStream file = File.Create(Application.persistentDataPath + "/" + filename + ".db");        

    		binaryFormatter.Serialize(file, data);
    		file.Close();
        }
        catch(SerializationException se)
        {
            //Debug.Log("Error opening file due to serialization " + se.Message);
        }
        catch(Exception se)
        {
            //Debug.Log("Error opening file due to serialization " +  se.Message);
        }
	}
	
	//Load Generic Type
    public static T Load(string filename, T data, Action action)// Action = callback
    {     
        try
        {
            if(filename == "" || filename == null)
            {
                filename = Application.productName;
            }

            if(File.Exists(Application.persistentDataPath + "/" + filename + ".db"))
            {
                //Debug.Log("File exist");
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                FileStream file = File.Open(Application.persistentDataPath + "/" + filename + ".db", FileMode.Open);

                data = (T)binaryFormatter.Deserialize(file);
                file.Close();
    			
    			if (action != null)
    				action();
            }        
        }
        catch(SerializationException se)
        {
            //Debug.Log("Error opening file due to serialization " + se.Source);
        }
        catch(Exception se)
        {
            //Debug.Log("Error opening file due to serialization " +  se.Source);
        }

        return data;
    }

	//Load Generic Type
	public static T Load(string filename, T data)// Action = callback
	{        
        try
        {
    		if(filename == "" || filename == null)
    		{
    			filename = Application.productName;
    		}

    		if(File.Exists(Application.persistentDataPath + "/" + filename + ".db"))
    		{
    			//Debug.Log("File exist");
    			BinaryFormatter binaryFormatter = new BinaryFormatter();
    			FileStream file = File.Open(Application.persistentDataPath + "/" + filename + ".db", FileMode.Open);

    			data = (T)binaryFormatter.Deserialize(file);
    			file.Close();
    		}            		
        }
        catch(SerializationException se)
        {
            //Debug.Log("Error opening file due to serialization " + se.Source);
        }
        catch(Exception se)
        {
            //Debug.Log("Error opening file due to serialization " +  se.Source);
        }

        return data;
	}

	//Check if file exist
	public static bool FileExist(string filename)
	{
		if(filename == "" || filename == null)
		{
			filename = Application.productName;
		}

		return File.Exists (Application.persistentDataPath + "/" + filename + ".db");
	}        
}

public class Data
{
    // delete file
    public static void DeleteFile(string filename)
    {
        if(filename == "" || filename == null)
        {
            filename = Application.productName;
        }

        File.Delete(Application.persistentDataPath + "/" + filename + ".db");
        //Debug.Log(Application.persistentDataPath + "/" + filename + ".db");
    }
}
