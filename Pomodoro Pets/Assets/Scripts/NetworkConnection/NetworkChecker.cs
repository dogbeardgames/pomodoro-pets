﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkChecker : MonoBehaviour {

	public static NetworkChecker instance;

	public bool isInternetAvailable = true;
	// Use this for initialization
	void Update()
	{
		instance = this;

		if (Application.internetReachability == NetworkReachability.NotReachable) 
		{
			isInternetAvailable = false;
		} 
		else 
		{
			isInternetAvailable = true;
		}
	}
		


}
