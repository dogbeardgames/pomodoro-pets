﻿using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Events;

public class VideoAds : MonoBehaviour
{
	[SerializeField] private string androidGameId = "1292524"; //ID for testing
	[SerializeField] private string iosGameId = "1292525"; //ID for testing
	[SerializeField] bool enableTestMode;

	string zoneId = "rewardedVideo";
	public static VideoAds Instance;
	//public delegate void OnVideoAdsFinished(int val1, int val2);
	//public static event OnVideoAdsFinished onVideoAdsFinished;

	public UnityEvent onVideoAdsFinished;

	void Awake()
	{
		if(Instance == null)
		{
			Instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else
		{
			Destroy(gameObject);
		}               
	}

	void Start()
	{
		string gameId = null;

		#if UNITY_IOS // If build platform is set to iOS...
		gameId = iosGameId;
		#elif UNITY_ANDROID // Else if build platform is set to Android...
		gameId = androidGameId;
		#endif

		if (string.IsNullOrEmpty(gameId)) { // Make sure the Game ID is set.
			//Debug.LogError("Failed to initialize Unity Ads. Game ID is null or empty.");
		} else if (!Advertisement.isSupported) {
			//Debug.LogWarning("Unable to initialize Unity Ads. Platform not supported.");
		} else if (Advertisement.isInitialized) {
			//Debug.Log("Unity Ads is already initialized.");
		} else {
			//Debug.Log(string.Format("Initialize Unity Ads using Game ID {0} with Test Mode {1}.",
			//	gameId, enableTestMode ? "enabled" : "disabled"));
			Advertisement.Initialize(gameId, enableTestMode);

			//VideoAds.Instance.ShowRewardedAd ();
		}
	}
	//check if video is finished
	bool isVideoFinished = false;
	public bool IsVideoFinished{
		get{ return isVideoFinished;}
	}
//	public void ShowRewardedAd()
//	{
//		if (Advertisement.IsReady ("rewardedVideo")) {
//			var options = new ShowOptions { resultCallback = HandleShowResult };
//			Advertisement.Show ("rewardedVideo");
//
//		} 
//		else {
//			Debug.Log ("Not Ready");
//		}
//	}
//
//	private void HandleShowResult(ShowResult result)
//
//	{
//		switch (result)
//		{
//		case ShowResult.Finished:
//			Debug.Log("The ad was successfully shown.");
//			//
//			// YOUR CODE TO REWARD THE GAMER
//			// Give coins etc.
//
//
//			break;
//		case ShowResult.Skipped:
//			Debug.Log("The ad was skipped before reaching the end.");
//			break;
//		case ShowResult.Failed:
//			Debug.LogError("The ad failed to be shown.");
//			break;
//		}
//	}
	public void CallVideoAds ()
	{
		if (string.IsNullOrEmpty (zoneId)) zoneId = null;
		Advertisement.IsReady (zoneId);
		ShowOptions options= new ShowOptions();
		options.resultCallback = HandleShowResult;
		Advertisement.Show (zoneId, options);


		//give Reward
		//RewardVideoAds (gold, gem , piggy);


	}

	public void RewardVideoAds(uint gold , uint gem , uint piggy)
	{
		DataController.Instance.playerData.gem += gem;
		DataController.Instance.playerData.gemacquired += gem;
		DataController.Instance.playerData.gold += gold;
		DataController.Instance.playerData.goldacquired += gold;
		DataController.Instance.playerData.pigyBank += piggy;
		if (DataController.Instance.playerData.pigyBank > 100) 
		{
			DataController.Instance.playerData.pigyBank = 100;
		}
	}


	private void HandleShowResult (ShowResult result)
	{
		switch (result)
		{
		case ShowResult.Finished:
			//Debug.Log ("Video completed. User rewarded " + "10" + " credits.");
			if(onVideoAdsFinished != null)
			{
				onVideoAdsFinished.Invoke ();
			}
			isVideoFinished = true;
			break;
		case ShowResult.Skipped:
			//Debug.LogWarning ("Video was skipped.");
			break;
		case ShowResult.Failed:
			//Debug.LogError ("Video failed to show.");

			GameObject finishTaskPopUps = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), GameObject.Find("Canvas").transform, false);
			ShowDialog dialog = finishTaskPopUps.GetComponent<ShowDialog>();

			dialog.DisableNO ();

			dialog.SetCaption("Oops. There seems to be a problem with your connection. Retry?");
			dialog.SetConfirmCaption("OK");
			// set events
			dialog.OnYesEvent += () =>
			{
				//Destroy (this.gameObject);	
			};

			break;
		}
	}


	public void ClaimFreeRewardsGold (int index)
	{
		if (string.IsNullOrEmpty (zoneId)) zoneId = null;
		Advertisement.IsReady (zoneId);
		ShowOptions options= new ShowOptions();
		options.resultCallback = HandleShowResult;
		Advertisement.Show (zoneId, options);

		AchievementConditions.instance.ClaimFreeReward(index);
	}

	public void ClaimFreeRewardsGem (int index)
	{
		if (string.IsNullOrEmpty (zoneId)) zoneId = null;
		Advertisement.IsReady (zoneId);
		ShowOptions options= new ShowOptions();
		options.resultCallback = HandleShowResult;
		Advertisement.Show (zoneId, options);

		AchievementConditions.instance.ClaimFreeReward(index);
	}

	public void ClaimFreeRewardsEtreats (int index)
	{
		if (string.IsNullOrEmpty (zoneId)) zoneId = null;
		Advertisement.IsReady (zoneId);
		ShowOptions options= new ShowOptions();
		options.resultCallback = HandleShowResult;
		Advertisement.Show (zoneId, options);

		AchievementConditions.instance.ClaimFreeReward(index);
	}


}


