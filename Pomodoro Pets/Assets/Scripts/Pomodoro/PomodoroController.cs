﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[RequireComponent(typeof(PomodoroAnimationController))]
public class PomodoroController : MonoBehaviour {

    PomodoroAnimationController pomodoroAnimController;

    public List<TaskItem> taskList = new List<TaskItem>();

    TaskItem currentTask;
    int taskGroupndex, taskIndex;
	// Use this for initialization
	void Start () {
        pomodoroAnimController = GetComponent<PomodoroAnimationController>();

        //active tasks
        //GetActiveTasks();

        //Completed Tasks
        //Debug.Log("Completed Tasks " + UnCompletedTask());
	}
	
//	// Update is called once per frame
//	void Update () {
//		
//	}

    #region METHODS
    public void GetActiveTasks()
    {        
        taskList = DataController.Instance.taskList.items.Where(item => item.isDone == false).Take(4).ToList();
    }

    public int CompletedTasks()
    {
        return DataController.Instance.taskList.items.Where(item => item.isDone == true).Count();
    }

    public int UnCompletedTask()
    {
        return DataController.Instance.taskList.items.Where(item => item.isDone == false).Count();
    }

    public void LoadPetHomeScreen()
    {
        FindObjectOfType<PetHomeController>().ShowPetHome();
    }

    public void ResetDoneTask()
    {
        DataController.Instance.taskList.items.Where(task => task.isDone == true).Select( item => item.isDone = false).ToList();
    }
    #endregion
}
