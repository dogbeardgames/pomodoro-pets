﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.IO;
using UnityEngine.SceneManagement;
#if UNITY_IOS
using UnityEngine.iOS;
#endif
    
public class PomodoroUI : MonoBehaviour {

	public Dictionary<string,List<string>> enumDictionarytimer = new Dictionary<string,List<string>> ();
	public List<AchievementBase> newList;

	[SerializeField]
	PetCategoryIcons petcategoryIcons;

	public static PomodoroUI Instance;   

	private GameObject addTaskConfirm;
	public GameObject taskPanel,
	//taskPanelNoSlide,
	categoryPanel, 
	taskMaintenancePanel,
	settingsPanel,
	playTaskPanel,
	taskPlayingPanel,
	taskBreakPanel,
	statsPanel,
	feedbackPanel,
	tasksIconPanel,
	menuPanel, 
	taskNotifPanel,
	achievementPanel,
	achievementNameObject,
	exitPanel,
	skipPopup, 
	skipshortPopup, 
	stopPopup,
	finishTaskPopUp;

	private GameObject tempTaskPanel,
	//taskPanelNoSlide,
	tempCategoryPanel, 
	tempTaskMaintenancePanel,
	tempSettingsPanel,
	tempPlayTaskPanel,
	tempTaskPlayingPanel,
	tempTaskBreakPanel,
	tempStatsPanel,
	tempFeedbackPanel,
	tempTasksIconPanel,
	tempMenuPanel, 
	tempTaskNotifPanel,
	tempAchievementPanel,
	tempSchievementNameObject,
	tempExitPanel; 


	private GameObject m_selectedItem;

	public static GameObject    activeObject,//for maintenance 
	pomodoroObject;// for pomodoro task

	public Button   btnTask,
	btnCategory,
	btnSettings,
	btnStatistics,
	btnAchievements,
	btnFeedback,
	btnExit,
	btnBack,
	btnYes, 
	btnHome,
	btnNo,
	btnSkipYes,
	btnShortYes,
	btnStopYes,
	btnFinishYes;

	Vector2 acquiredTextAppearPos;

	public Transform addTaskButton, showTaskPanelButton, taskContainer;

	public bool isMenuActive=false;

	public bool thereIsActiveDialog;

	void Awake()
	{
		Instance = this;
	}

	// Use this for initialization
	void Start () { 

        // ios notification       


		acquiredTextAppearPos = new Vector2((Screen.width/2.4f)*-1, (Screen.height/2.1f) * 0.9f);

		//better in application exit
		//DataController.Instance.playerdata.isnewInstalled = false

		//Debug.Log("On disable");
		DataController.Instance.SavePlayerData();

		activeObject = gameObject;

		//load json file localization
		LanguageCheckDropdown();

		InitializeButtonEvents();

		if (DataController.Instance.playerData.isTimerRunning && DataController.Instance.playerData.state == EnumCollection.PomodoroStates.TimerScreen ) {
			CreateTaskPlayingPanel ();
			DisableNotaskPanel ();
		} 
		else if (DataController.Instance.playerData.isTimerRunning && DataController.Instance.playerData.state == EnumCollection.PomodoroStates.BreakScreen ) {
			CreateBreak ();
			DisableNotaskPanel ();
		} 
		else if (DataController.Instance.playerData.isTimerRunning && DataController.Instance.playerData.state == EnumCollection.PomodoroStates.LongBreakScreen ) {
			CreateLongBreak ();
			DisableNotaskPanel ();
		} 

		else
		{
			CreateStartTaskPanel();
			//			state is Task Screen and no active task
			//			if (DataController.Instance.playerData.state == EnumCollection.PomodoroStates.TaskScreen && DataController.Instance.taskList.items.Count == 0) {
			//				DisablePlayButton ();
			//				// show no task dialog
			//				//Debug.Log("No Task, would you like to add?");
			//			} else {
			//				DisablePlayButton ();
			//			}
			//			//Debug.Log("Create task Panel");
			//
			//
			//			//Debug.Log (gameObject.GetComponent<Timer> ().isLongbreakEnd.ToString());
			DisablePlayButton ();
			DisableNotaskPanel ();
		}
		//InvokeRepeating ("Test", 2f,2f);
	}

	bool isStopsession = true;

	void Test()
	{
		GoldAnimation ("99999");
	}

	#region EVENT
	void InitializeButtonEvents()
	{
		btnTask.onClick.AddListener(InitializeTask); 
//		btnTask.onClick.AddListener (delegate {
//			Framework.SoundManager.Instance.PlaySFX("seIconTapPomodoro");	
//		});
		btnCategory.onClick.AddListener(InitializeCategory);
		btnSettings.onClick.AddListener(InitializeSettings);
		btnStatistics.onClick.AddListener(InitializeStatistics);
		btnAchievements.onClick.AddListener(InitializeAchievements);
		btnFeedback.onClick.AddListener(InitializeFeedback);
		btnHome.onClick.AddListener(HomeButton);
		btnYes.onClick.AddListener(Yes);
		btnNo.onClick.AddListener(No);

		addTaskButton.GetComponent<Button>().onClick.AddListener(() => AddTask());

		//gold
		GameObject.Find("btnGold").GetComponent<Button>().onClick.AddListener(() => ToggleMenuButton.ins.Hide ());
		//GameObject.Find("btnGold").GetComponent<Button>().onClick.AddListener(() => GoldButton ());
	}

	#endregion

	#region METHODS

	public void InitializeCategory()
	{
		HideGoldAnimation.instance.HideAnimation ();
		if (activeObject == null || activeObject.name != "CategoryPanel")
		{
			GameObject categoryObject = Instantiate(categoryPanel, transform, false);
			categoryObject.name = "CategoryPanel";
			activeObject = categoryObject;

			CloseMenu();
		}
		isMenuActive = true;
	}

	public void CreateStartTaskPanel()
	{
		DataController.Instance.playerData.state = EnumCollection.PomodoroStates.TaskScreen;

		//pomodoro
		pomodoroObject = Instantiate(playTaskPanel, taskContainer, false);
		pomodoroObject.name = "PlayTaskPanel";
		pomodoroObject.transform.Find("btnStartTimer").GetComponent<Button>().onClick.AddListener(CreateTaskPlayingPanel);

		// task UI
		//GetComponent<PomodoroController>().GetActiveTasks();
		if(GetComponent<PomodoroController>().taskList.Count > 0)
			pomodoroObject.transform.Find("txtTaskName").GetComponent<TextMeshProUGUI>().text = GetComponent<PomodoroController>().taskList[0].task;      
		btnHome.GetComponent<Button>().interactable = true;

		//disable button
		DisablePlayButton ();
	}

	public void DisablePlayButton()
	{
		//disable play button
		if(GetComponent<PomodoroController> ().CompletedTasks () == DataController.Instance.taskList.items.Count)
		{
			//Debug.Log ("<color=red>" + " interactable equals false " +"</color>");

			if (pomodoroObject.transform.Find ("btnStartTimer") != null) {
				pomodoroObject.transform.Find ("btnStartTimer").GetComponent<Button> ().interactable = false;
			}
		}

		else if(GetComponent<PomodoroController> ().UnCompletedTask () > 0)
		{
			//Debug.Log ("<color=red>" + " interactable equals true " + GetComponent<PomodoroController> ().UnCompletedTask () + "</color>");

			if (pomodoroObject.transform.Find ("btnStartTimer") != null) {
				pomodoroObject.transform.Find ("btnStartTimer").GetComponent<Button> ().interactable = true;
				TaskUI();
			}
		}  
	}

	public void DisableNotaskPanel()
	{
		//disable play button
		if(GetComponent<PomodoroController> ().CompletedTasks () == DataController.Instance.taskList.items.Count)
		{
			//Debug.Log ("<color=red>" + " interactable equals false " +"</color>");

			if (GameObject.Find ("NoTaskPanel") != null) {
				GameObject.Find ("NoTaskPanel").SetActive (true);
			}
		}

		else if(GetComponent<PomodoroController> ().UnCompletedTask () > 0)
		{
			//Debug.Log ("<color=red>" + " interactable equals true " + GetComponent<PomodoroController> ().UnCompletedTask () + "</color>");

			if (GameObject.Find ("NoTaskPanel") != null) {
				GameObject.Find ("NoTaskPanel").SetActive (false);
			}

		}  
	}

	public void CreateTaskPlayingPanel()
	{
		//Debug.Log ("<color=blue>" + "Timer is running........#$%^" + "</color>");

		DataController.Instance.playerData.isfromIndoor = false;
		DataController.Instance.playerData.isTimeExtended = false;

		//get Total duration of session duration then pass it to notification params (delay time)
		float duration =DataController.Instance.settings.sessionDuration;
		float preEndtime = DataController.Instance.settings.alarmTime * 60;

		float totalDurationNotiftask = duration * 60;

		float preEndalarmTime = totalDurationNotiftask - preEndtime;
		preEndalarmTime = preEndalarmTime / 60;

		float notiftime = preEndalarmTime * 60;

		//Debug.Log ("<color=blue>" + preEndalarmTime + "</color>");


		//Debug.Log ("Create Task Playing Panel" + totalDurationNotiftask.ToString());

		//Debug.Log ((long)totalDurationNotif + "Duration long");
		//Save Task duration here

		List<string> notifbreaksessionFinished = new List<string>();// language
		//LoadEnumJson("localizedTextTheme_sp.json");
		//var themeDic = enumDictionarytimer.Where (y => y.Key == "set_theme").ToDictionary (y => y.Key, y => y.Value);
		var notifbreakfinished = enumDictionarytimer.Where (y => y.Key == "notif_worksessionfinished").ToDictionary (y => y.Key, y => y.Value);
		foreach (List<string> listlongbreak in notifbreakfinished.Values) {
			foreach ( var notifBreakS in listlongbreak) {
				notifbreaksessionFinished.Add (notifBreakS);
			}
		}
		string notifBF="";
		for (int i = 0; i < notifbreaksessionFinished.Count; i++) {
			notifBF = notifbreaksessionFinished [i]; 
		}


		//Debug.Log ("<color = blue>" + duration  +"</color>");
		//Debug.Log ("<color>" + notifBF  +   "" +"</color>");

		//task notif
		PlainNotification.SendNotification(1, (long)totalDurationNotiftask, "Pomodoro Pets", notifBF, new Color32(0xff, 0x44, 0x44, 255), true, true, true, "app_icon");
		//pre end alarm notif

		if (DataController.Instance.settings.alarmTime > 0) 
		{
			PlainNotification.SendNotification (4, (long)notiftime, "Pomodoro Pets", "You have " + preEndtime / 60 + " " + "minute(s)" + " left until session end ", new Color32 (0xff, 0x44, 0x44, 255), true, true, true, "app_icon");
		}
		//Debug.Log ("<color=green> " + (long)notiftime + "</color>");
		
        //IOS
		#if UNITY_IOS
        LocalNotificationIos.SetNotification(notifBF,(int)totalDurationNotiftask, 1);
		if (DataController.Instance.settings.alarmTime > 0) 
		{
			LocalNotificationIos.SetNotification("You have " +  preEndtime/60  + " " + "minute(s)" + " left until session end",(int)notiftime, 1);
		}
		#endif


		DataController.Instance.playerData.state = EnumCollection.PomodoroStates.TimerScreen;

		//Debug.Log("Start Task");
		Destroy(pomodoroObject);
		pomodoroObject = Instantiate(taskPlayingPanel, taskContainer, false);

		//scale aniamtion
		pomodoroObject.transform.localScale = new Vector3(0, 0, 0);
		pomodoroObject.transform.DOScale(new Vector3(1, 1, 1), 1);

		//disable home button
		//btnHome.GetComponent<Button>().interactable = false;

		// timer start
		DataController.Instance.playerData.isTimerRunning = true;
//		DataUILogger.ins.SetSuperLog ("Create Task Playing Panel");@CHECK@

		Timer timer = GetComponent<Timer>();
		Button btnExtend, btnPause, btnStop, btnSkip, btnPetIcon;
		GameObject barObj = pomodoroObject.transform.Find("timerImage").gameObject;
		TextMeshProUGUI _timerText = pomodoroObject.transform.Find("txtTimer").GetComponent<TextMeshProUGUI>();
		btnExtend = pomodoroObject.transform.Find("ButtonContainer").transform.Find("btnExtend").GetComponent<Button>();
		btnPause = pomodoroObject.transform.Find("ButtonContainer").transform.Find("btnPause").GetComponent<Button>();
		btnStop = pomodoroObject.transform.Find("ButtonContainer").transform.Find("btnStop").GetComponent<Button>();
		btnSkip = pomodoroObject.transform.Find("ButtonContainer").transform.Find("btnSkip").GetComponent<Button>();
		btnPetIcon = pomodoroObject.transform.Find("petImage").GetComponent<Button>();

		// setup task UI
		TaskUI();

		if(DataController.Instance.playerData.isTimerRunning)
		{
			btnPause.GetComponent<PlayPause>().Pause();
		}
		else
		{
			btnPause.GetComponent<PlayPause>().Play();

			PlainNotification.CancelNotification (1);
			PlainNotification.CancelNotification (2);
			PlainNotification.CancelNotification (3);
			PlainNotification.CancelNotification (4);

			PlainNotification.CancelAllNotifications ();

			#if UNITY_IOS
			LocalNotificationIos.CancelAllNotification ();
			LocalNotificationIos.CancelNotification ();
			#endif
		}

		//btnStop.onClick.AddListener(() => timer.StopSession());
		btnStop.onClick.AddListener(() => StopPopUp());

		btnStopYes.onClick.AddListener(() => timer.StopSession());


		btnExtend.onClick.AddListener(() => timer.ExtendOneMinute());
		btnPause.onClick.AddListener(() => 
			{
                if(timer.IsPaused())
                {   
					//Debug.Log("Pauseing.....");

                    btnPause.GetComponent<PlayPause>().Pause();
                    //enable extend when pause
                    btnExtend.interactable = true;

					//restart notification
					float min = timer.m_min * 60;
					float sec = timer.m_sec;
					float totalNotifTime = min + sec;
					float preEndalarm = DataController.Instance.settings.alarmTime * 60;
					float finalPreEnd = totalNotifTime - preEndalarm;

					//Debug.Log(timer.m_min);
					//Debug.Log(timer.m_sec);

					//Debug.Log("total " + (long)totalNotifTime);
					//Debug.Log("total pre " + (long)finalPreEnd);
					//call notif
					PlainNotification.SendNotification(1, (long)totalNotifTime, "Pomodoro Pets", notifBF, new Color32(0xff, 0x44, 0x44, 255), true, true, true, "app_icon");

					//pre end
					if (DataController.Instance.settings.alarmTime > 0) 
					{
						if(totalNotifTime > preEndalarm )
						{
							PlainNotification.SendNotification (4, (long)finalPreEnd, "Pomodoro Pets", "You have " + preEndtime / 60 + " " + "minute(s)" + " left until session end ", new Color32 (0xff, 0x44, 0x44, 255), true, true, true, "app_icon");
						}
					}
					//Debug.Log ("<color=green> " + (long)notiftime + "</color>");

					//IOS
					#if UNITY_IOS
					LocalNotificationIos.SetNotification(notifBF,(int)totalDurationNotiftask, 1);					

					if (DataController.Instance.settings.alarmTime > 0) 
					{
						if(totalNotifTime > preEndalarm )
						{
							LocalNotificationIos.SetNotification("You have " +  finalPreEnd  + " " + "minute(s)" + " left until session end",(int)finalPreEnd, 1);
						}
					}
					#endif


                }
                else
                {                   
                    btnPause.GetComponent<PlayPause>().Play();
                    //disable extend when pause
                    btnExtend.interactable = false;

					PlainNotification.CancelNotification (1);
					PlainNotification.CancelNotification (2);
					PlainNotification.CancelNotification (3);
					PlainNotification.CancelNotification (4);
					PlainNotification.CancelAllNotifications ();

					#if UNITY_IOS
					LocalNotificationIos.CancelAllNotification ();
					LocalNotificationIos.CancelNotification ();
					#endif
                }
			});


		//skip confirmation


		btnSkip.onClick.AddListener(() => SkipPopUp());

		//btnSkipYes.onClick.AddListener(() => timer.Skip());
		//skipPopup.transform.Find("Image").FindChild("btnOk").GetComponent<Button> ().onClick.AddListener (() => timer.Skip());
		//btnPetIcon.onClick.AddListener(() => PetIconButton());

		//save confirmation
		btnPetIcon.onClick.AddListener(() => FinishPopUp());
		//btnFinishYes.onClick.AddListener(() => PetIconButton());

		//get next task 
		//List<TaskItem> nextTask =  DataController.Instance.taskList.items.Where (item => item.isDone == false).ToList ();
		//pomodoroObject.transform.Find ("txtTaskName").GetComponent<TextMeshProUGUI> ().text = nextTask [0].task;

		timer.SetTimerValue(DataController.Instance.settings.sessionDuration, barObj, _timerText);
		timer.StartTimer();
		// timer start

		addTaskButton.GetComponent<RectTransform>().DORotate(new Vector3(0, 90, 0), 0.5f, RotateMode.Fast).OnComplete(() => {
			showTaskPanelButton.GetComponent<RectTransform>().DORotate(new Vector3(0, 0, 0), 0.5f, RotateMode.Fast);
		});
		//Debug.Log("Should rotate");
	}

	public void CreateBreak()
	{
		if (DataController.Instance.playerData.isSkipped == false && DataController.Instance.playerData.state == EnumCollection.PomodoroStates.TimerScreen) {

			setNotifSfx ();
			GoldReward ();

			AchievementConditions.instance.PassionateWorker();
		}

		AchievementConditions.instance.FirstJob ();

		//get Total duration of short break session duration then pass it to notification params (delay time)
		float breakduration =DataController.Instance.settings.shortBreakDuration;
		float preEndtime = DataController.Instance.settings.alarmTime * 60;
		float totalDurationNotiftask = breakduration * 60;
		float preEndalarmTime = totalDurationNotiftask - preEndtime;
		preEndalarmTime = preEndalarmTime / 60;
		float notiftime = preEndalarmTime * 60;

		//Debug.Log ("<color=blue>" + preEndalarmTime + "</color>");


		List<string> shortbreakList = new List<string>();// short break
		//LoadEnumJson("localizedTextTheme_sp.json");

		var shortbreakString = enumDictionarytimer.Where (y => y.Key == "set_shortbreak").ToDictionary (y => y.Key, y => y.Value);
		foreach (List<string> listlongbreak in shortbreakString.Values) {
			foreach ( var shortbreakS in listlongbreak) {
				shortbreakList.Add (shortbreakS);
			}

		}

		string shortBreak="";
		for (int i = 0; i < shortbreakList.Count; i++) {
			shortBreak = shortbreakList [i]; 
		}
			
		List<string> notifbreaksessionFinished = new List<string>();// language
		//LoadEnumJson("localizedTextTheme_sp.json");
		//var themeDic = enumDictionarytimer.Where (y => y.Key == "set_theme").ToDictionary (y => y.Key, y => y.Value);
		var notifbreakfinished = enumDictionarytimer.Where (y => y.Key == "notif_breaksessionfinished").ToDictionary (y => y.Key, y => y.Value);
		foreach (List<string> listlongbreak in notifbreakfinished.Values) {
			foreach ( var notifBreakS in listlongbreak) {
				notifbreaksessionFinished.Add (notifBreakS);
			}
		}
		string notifBF="";
		for (int i = 0; i < notifbreaksessionFinished.Count; i++) {
			notifBF = notifbreaksessionFinished [i]; 
		}
//				PlainNotification.SendNotification(2, (long)totalDurationNotifsBreak, "Pomodoro Pets", notifBF, new Color32(0xff, 0x44, 0x44, 255), true, true, true, "app_icon");
//				PlainNotification.SendNotification(4, (long)notiftime, "Pomodoro Pets", "You have " +  preEndalarmTime     + timeFormat(preEndtime) +  "left until session end", new Color32(0xff, 0x44, 0x44, 255), true, true, true, "app_icon");
				PlainNotification.SendNotification(2, (long)totalDurationNotiftask, "Pomodoro Pets", notifBF, new Color32(0xff, 0x44, 0x44, 255), true, true, true, "app_icon");
				//pre end alarm notif
			if (DataController.Instance.settings.alarmTime > 0) 
			{
			PlainNotification.SendNotification (4, (long)notiftime, "Pomodoro Pets", "You have " + preEndtime / 60 + " minute(s)" + " left until session end ", new Color32 (0xff, 0x44, 0x44, 255), true, true, true, "app_icon");
			}
//		//IOS
		#if UNITY_IOS
		LocalNotificationIos.SetNotification(notifBF,(int)totalDurationNotiftask, 1);
		if (DataController.Instance.settings.alarmTime > 0) 
		{
		LocalNotificationIos.SetNotification("You have " +  preEndtime/60  + " minute(s)" + " left until session end",(int)notiftime, 1);
		}
		#endif

		DataController.Instance.playerData.state = EnumCollection.PomodoroStates.BreakScreen;

		btnHome.GetComponent<Button>().interactable = true;

		Destroy(pomodoroObject);
		pomodoroObject = Instantiate(taskBreakPanel, taskContainer, false);


		// scale animation
		pomodoroObject.transform.localScale = new Vector3(0, 0, 0);
		pomodoroObject.transform.DOScale(new Vector3(1, 1, 1), 1);

		// timer start
		DataController.Instance.playerData.isTimerRunning = true;
//		DataUILogger.ins.SetSuperLog ("Create Break");@CHECK@

		Timer timer = GetComponent<Timer>();
		Button btnPause, btnStop, btnSkip;
		GameObject barObj = pomodoroObject.transform.Find("timerImage").gameObject;
		TextMeshProUGUI _timerText = pomodoroObject.transform.Find("txtTimer").GetComponent<TextMeshProUGUI>();
		btnPause = pomodoroObject.transform.Find("ButtonContainer").transform.Find("btnPause").GetComponent<Button>();
		btnStop = pomodoroObject.transform.Find("ButtonContainer").transform.Find("btnStop").GetComponent<Button>();
		btnSkip = pomodoroObject.transform.Find("ButtonContainer").transform.Find("btnSkip").GetComponent<Button>();

		TaskUI();

		//      pomodoroObject.transform.Find("txtTaskName").GetComponent<TextMeshProUGUI>().text = "Short Break";  
		pomodoroObject.transform.Find("txtTaskName").GetComponent<TextMeshProUGUI>().text = shortBreak;     

		if(DataController.Instance.playerData.isTimerRunning)
		{
			btnPause.GetComponent<PlayPause>().Pause();

		}
		else
		{
			btnPause.GetComponent<PlayPause>().Play();
			PlainNotification.CancelAllNotifications ();

			LocalNotificationIos.CancelAllNotification ();
		}

		//btnStop.onClick.AddListener(() => timer.StopSession());
		btnStop.onClick.AddListener(() => StopPopUp());
		btnStopYes.onClick.AddListener(() => timer.StopSession());


		btnPause.onClick.AddListener(() => 
			{
                if(timer.IsPaused())
				{
					btnPause.GetComponent<PlayPause>().Pause();

					//restart notification
					float min = timer.m_min * 60;
					float sec = timer.m_sec;
					float totalNotifTime = min + sec;
					float preEndalarm = DataController.Instance.settings.alarmTime * 60;
					float finalPreEnd = totalNotifTime - preEndalarm;

					//Debug.Log(timer.m_min);
					//Debug.Log(timer.m_sec);

					//Debug.Log("total " + (long)totalNotifTime);
					//Debug.Log("total pre " + (long)finalPreEnd);
					//call notif
					PlainNotification.SendNotification(2, (long)totalNotifTime, "Pomodoro Pets", notifBF, new Color32(0xff, 0x44, 0x44, 255), true, true, true, "app_icon");

					//pre end
					if (DataController.Instance.settings.alarmTime > 0) 
					{
						if(totalNotifTime > preEndalarm )
						{
							PlainNotification.SendNotification (4, (long)finalPreEnd, "Pomodoro Pets", "You have " + preEndtime / 60 + " " + "minute(s)" + " left until session end ", new Color32 (0xff, 0x44, 0x44, 255), true, true, true, "app_icon");
						}
					}
					//Debug.Log ("<color=green> " + (long)notiftime + "</color>");

					//IOS
					#if UNITY_IOS
					LocalNotificationIos.SetNotification(notifBF,(int)totalDurationNotiftask, 1);					

					if (DataController.Instance.settings.alarmTime > 0) 
					{
					if(totalNotifTime > preEndalarm )
					{
					LocalNotificationIos.SetNotification("You have " +  finalPreEnd  + " " + "minute(s)" + " left until session end",(int)finalPreEnd, 1);
					}
					}
					#endif
				}
				else
				{
					btnPause.GetComponent<PlayPause>().Play();

					PlainNotification.CancelNotification (1);
					PlainNotification.CancelNotification (2);
					PlainNotification.CancelNotification (3);
					PlainNotification.CancelNotification (4);
					PlainNotification.CancelAllNotifications ();

					#if UNITY_IOS
					LocalNotificationIos.CancelAllNotification ();
					LocalNotificationIos.CancelNotification ();
					#endif
				}
			});

		//		btnSkip.onClick.AddListener(() => SkipPopUp());
		//btnSkip.onClick.AddListener(() => timer.Skip());

		//skip with popup
		btnSkip.onClick.AddListener(() => SkipPopUp());


		timer.SetTimerValue(DataController.Instance.settings.shortBreakDuration, barObj, _timerText);
		timer.StartTimer();

		//Gold reward

			
		//IOS notification
		//GetComponent<Timer> ().minimizedSeconds = 0; // reset leftTime
		//Debug.Log ("reset left Time");
		//pomodoroObject.transform.Find("txtTaskName").GetComponent<TextMeshProUGUI>().text = "Short Break";
		pomodoroObject.transform.Find("txtTaskName").GetComponent<TextMeshProUGUI>().text = shortBreak;     

		// rotate
		addTaskButton.GetComponent<RectTransform>().DORotate(new Vector3(0, 90, 0), 0.5f, RotateMode.Fast).OnComplete(() => {
			showTaskPanelButton.GetComponent<RectTransform>().DORotate(new Vector3(0, 0, 0), 0.5f, RotateMode.Fast);
		});

		//change status of current task when finished 

	}

	public void CreateLongBreak()
	{

		if (DataController.Instance.playerData.isSkipped == false && DataController.Instance.playerData.state == EnumCollection.PomodoroStates.TimerScreen) {
			setNotifSfx ();
			GoldReward ();
		}

		//get Total duration of short break session duration then pass it to notification params (delay time)
		float breakduration =DataController.Instance.settings.shortBreakDuration;
		float preEndtime = DataController.Instance.settings.alarmTime * 60;
		float totalDurationNotiftask = breakduration * 60;
		float preEndalarmTime = totalDurationNotiftask - preEndtime;
		preEndalarmTime = preEndalarmTime / 60;
		float notiftime = preEndalarmTime * 60;


		List<string> longbreakList = new List<string>();// language
		//LoadEnumJson("localizedTextTheme_sp.json");


		//var themeDic = enumDictionarytimer.Where (y => y.Key == "set_theme").ToDictionary (y => y.Key, y => y.Value);
		var longbreakString = enumDictionarytimer.Where (y => y.Key == "set_longbreak").ToDictionary (y => y.Key, y => y.Value);
		foreach (List<string> listlongbreak in longbreakString.Values) {
			foreach ( var longbreakS in listlongbreak) {
				longbreakList.Add (longbreakS);
			}

		}

		string longBreak="";
		for (int i = 0; i < longbreakList.Count; i++) {
			longBreak = longbreakList [i]; 
		}

		//call notif
		PlainNotification.SendNotification(3, (long)totalDurationNotiftask, "Pomodoro Pets", "Break session finished", new Color32(0xff, 0x44, 0x44, 255), true, true, true, "app_icon");
		//pre end alarm notif
		if (DataController.Instance.settings.alarmTime > 0) 
		{
			PlainNotification.SendNotification (4, (long)notiftime, "Pomodoro Pets", "You have " + preEndtime / 60 + " minute(s)" + " left until session end ", new Color32 (0xff, 0x44, 0x44, 255), true, true, true, "app_icon");
		}
		//IOS
		#if UNITY_IOS
		LocalNotificationIos.SetNotification("Break session finished",(int)totalDurationNotiftask, 1);
		if (DataController.Instance.settings.alarmTime > 0) 
		{
		LocalNotificationIos.SetNotification("You have " +  preEndtime/60  + " minute(s)" + " left until session end",(int)notiftime, 1);
		}
		#endif

		DataController.Instance.playerData.state = EnumCollection.PomodoroStates.LongBreakScreen;

		btnHome.GetComponent<Button>().interactable = true;

		Destroy(pomodoroObject);
		pomodoroObject = Instantiate(taskBreakPanel, taskContainer, false);


		//scale animation
		pomodoroObject.transform.localScale = new Vector3(0, 0, 0);
		pomodoroObject.transform.DOScale(new Vector3(1, 1, 1), 1);

		// timer start
		DataController.Instance.playerData.isTimerRunning = true;
//		DataUILogger.ins.SetSuperLog ("Create Break");@CHECK@

		Timer timer = GetComponent<Timer>();
		Button btnPause, btnStop, btnSkip;
		GameObject barObj = pomodoroObject.transform.Find("timerImage").gameObject;
		TextMeshProUGUI _timerText = pomodoroObject.transform.Find("txtTimer").GetComponent<TextMeshProUGUI>();
		btnPause = pomodoroObject.transform.Find("ButtonContainer").transform.Find("btnPause").GetComponent<Button>();
		btnStop = pomodoroObject.transform.Find("ButtonContainer").transform.Find("btnStop").GetComponent<Button>();
		btnSkip = pomodoroObject.transform.Find("ButtonContainer").transform.Find("btnSkip").GetComponent<Button>();

		TaskUI();

		//pomodoroObject.transform.Find("txtTaskName").GetComponent<TextMeshProUGUI>().text = "Long Break";
		pomodoroObject.transform.Find("txtTaskName").GetComponent<TextMeshProUGUI>().text = longBreak;     

		// rotate
		addTaskButton.GetComponent<RectTransform>().DORotate(new Vector3(0, 90, 0), 0.5f, RotateMode.Fast).OnComplete(() => {
			showTaskPanelButton.GetComponent<RectTransform>().DORotate(new Vector3(0, 0, 0), 0.5f, RotateMode.Fast);
		});


		if(DataController.Instance.playerData.isTimerRunning)
		{
			btnPause.GetComponent<PlayPause>().Pause();
		}
		else
		{
			btnPause.GetComponent<PlayPause>().Play();

			PlainNotification.CancelNotification (1);
			PlainNotification.CancelNotification (2);
			PlainNotification.CancelNotification (3);
			PlainNotification.CancelNotification (4);
			PlainNotification.CancelAllNotifications ();

			#if UNITY_IOS
			LocalNotificationIos.CancelAllNotification ();
			LocalNotificationIos.CancelNotification ();
			#endif
		}

		//btnStop.onClick.AddListener(() => timer.StopSession());
		btnStop.onClick.AddListener(() => StopPopUp());
		btnStopYes.onClick.AddListener(() => timer.StopSession());


		btnPause.onClick.AddListener(() => 
			{
				if(timer.IsPaused())
				{
					btnPause.GetComponent<PlayPause>().Pause();

					//restart notification
					float min = timer.m_min * 60;
					float sec = timer.m_sec;
					float totalNotifTime = min + sec;
					float preEndalarm = DataController.Instance.settings.alarmTime * 60;
					float finalPreEnd = totalNotifTime - preEndalarm;

					//Debug.Log(timer.m_min);
					//Debug.Log(timer.m_sec);

					//Debug.Log("total " + (long)totalNotifTime);
					//Debug.Log("total pre " + (long)finalPreEnd);
					//call notif
					PlainNotification.SendNotification(3, (long)totalNotifTime, "Pomodoro Pets", "Break session finished", new Color32(0xff, 0x44, 0x44, 255), true, true, true, "app_icon");

					//pre end
					if (DataController.Instance.settings.alarmTime > 0) 
					{
						if(totalNotifTime > preEndalarm )
						{
							PlainNotification.SendNotification (4, (long)finalPreEnd, "Pomodoro Pets", "You have " + preEndtime / 60 + " " + "minute(s)" + " left until session end ", new Color32 (0xff, 0x44, 0x44, 255), true, true, true, "app_icon");
						}
					}
					//Debug.Log ("<color=green> " + (long)notiftime + "</color>");

					//IOS
					#if UNITY_IOS
					LocalNotificationIos.SetNotification("Break session finished",(int)totalDurationNotiftask, 1);
		

					if (DataController.Instance.settings.alarmTime > 0) 
					{
					if(totalNotifTime > preEndalarm )
					{
							LocalNotificationIos.SetNotification("You have " +  finalPreEnd  + " " + "minute(s)" + " left until session end",(int)finalPreEnd, 1);
					}
					}
					#endif

				}
				else
				{
					btnPause.GetComponent<PlayPause>().Play();

					PlainNotification.CancelNotification (1);
					PlainNotification.CancelNotification (2);
					PlainNotification.CancelNotification (3);
					PlainNotification.CancelNotification (4);

					PlainNotification.CancelAllNotifications ();
					#if UNITY_IOS
					LocalNotificationIos.CancelAllNotification ();
					LocalNotificationIos.CancelNotification ();
					#endif
				}
			});


		//btnSkip.onClick.AddListener(() => timer.Skip());
		//skip with popup
		btnSkip.onClick.AddListener(() => SkipPopUp());


		timer.SetTimerValue(DataController.Instance.settings.longBreakDuration, barObj, _timerText);
		timer.StartTimer();



		//Debug.Log("duration success");
		//Debug.Log("Long break ends");

		//get Total duration of session duration then pass it to notification params (delay time)
		float duration =DataController.Instance.settings.longBreakDuration;
		totalDurationNotiftask = duration * 60;
		//Debug.Log ((long)totalDurationNotif + "Duration long");
		//Save Task duration here
		//Debug.Log("Long Break duration " + totalDurationNotiftask.ToString());

		List<string> notifworkFinishedlist = new List<string>();// language
		//LoadEnumJson("localizedTextTheme_sp.json");
		//var themeDic = enumDictionarytimer.Where (y => y.Key == "set_theme").ToDictionary (y => y.Key, y => y.Value);
		var notifworkfinished = enumDictionarytimer.Where (y => y.Key == "notif_breaksessionfinished").ToDictionary (y => y.Key, y => y.Value);
		foreach (List<string> listlongbreak in notifworkfinished.Values) {
			foreach ( var notifworkS in listlongbreak) {
				notifworkFinishedlist.Add (notifworkS);
			}
		}

		string notifwF="";
		for (int i = 0; i < notifworkFinishedlist.Count; i++) {
			notifwF = notifworkFinishedlist [i]; 
		}

		//PlainNotification.SendNotification(3, 30, "Pomodoro Pets", "Work Session finished.", new Color32(0xff, 0x44, 0x44, 255), true, true, true, "app_icon");
		PlainNotification.SendNotification(3, (long)totalDurationNotiftask, "Pomodoro Pets", notifwF, new Color32(0xff, 0x44, 0x44, 255), true, true, true, "app_icon");
		//pre end alarm notif
		//PlainNotification.SendNotification(4, (long)notiftime, "Pomodoro Pets", "You have" +  preEndalarmTime    + " left until session end", new Color32(0xff, 0x44, 0x44, 255), true, true, true, "app_icon");
		if (DataController.Instance.settings.alarmTime > 0) {
			PlainNotification.SendNotification (4, (long)notiftime, "Pomodoro Pets", "You have " + preEndalarmTime + " minute(s) left until session end", new Color32 (0xff, 0x44, 0x44, 255), true, true, true, "app_icon");
		}
//		//IOS
		#if UNITY_IOS
        LocalNotificationIos.SetNotification(notifwF,(int)totalDurationNotiftask, 1);
		if (DataController.Instance.settings.alarmTime > 0) 
		{
		LocalNotificationIos.SetNotification("You have " +  preEndtime/60 +  " minute(s) left until session end",(int)notiftime, 1);
		}
		#endif

	}
	public void PopUpHome()
	{
		if(DataController.Instance.playerData.isTimerRunning && DataController.Instance.playerData.state == EnumCollection.PomodoroStates.TimerScreen)
		{
			//Debug.Log("Task Notif Panel");
			//Debug.Log("Load Home Timer");
			taskNotifPanel.gameObject.SetActive(true);
		}
	}

	void PlayBGM()
	{
		//print ("Play BGM on this event");

		if (DataController.Instance.settings.isBGMOn) 
		{
			Framework.SoundManager.Instance.UnmuteSFX ();
			if (!Framework.SoundManager.Instance.isBGMPlaying("bgm" + DataController.Instance.playerData.backGround + "Home"))
			{
				Framework.SoundManager.Instance.PlayBGM("bgm" + DataController.Instance.playerData.backGround + "Home", true);		
			}
			Framework.SoundManager.Instance.FadeInInside ();
		}

	}

	public void HomeButton()// when the home button is pressed
	{
			Timer time = GetComponent<Timer> ();
			float totalTime = (time.m_min * 60) + time.m_sec;

			if(DataController.Instance.playerData.isTimerRunning && DataController.Instance.playerData.state == EnumCollection.PomodoroStates.TimerScreen)
			{
				//Debug.Log("Task Notif Panel");
				//Debug.Log("Load Home Timer");
				taskNotifPanel.gameObject.SetActive(true);
			}
			else if(!DataController.Instance.playerData.isTimerRunning && DataController.Instance.playerData.state == EnumCollection.PomodoroStates.TaskScreen)
			{
				//Debug.Log("Load Home TaskScreen");
				//PetHomeController.ins.PlayBGMClip ("bgmGenericHome");
			
				PlayBGM ();
				GetComponent<PomodoroController>().LoadPetHomeScreen();
				ToggleMenuButton.ins.Hide ();
			}
	
			else if(DataController.Instance.playerData.isTimerRunning && DataController.Instance.playerData.state == EnumCollection.PomodoroStates.LongBreakScreen)
			{
				//Debug.Log("Load Home LongBreakScreen");
				PlayBGM ();
				GetComponent<PomodoroController>().LoadPetHomeScreen();
				ToggleMenuButton.ins.Hide ();

				StartCoroutine (PetHomeBreakAlert.instance.BreakTimerUp(totalTime));
			}
	
			else if(DataController.Instance.playerData.isTimerRunning && DataController.Instance.playerData.state == EnumCollection.PomodoroStates.BreakScreen)
			{
				//Debug.Log("Load Home-BREAK Screen");
				PlayBGM ();
				GetComponent<PomodoroController>().LoadPetHomeScreen();
				ToggleMenuButton.ins.Hide ();
				//			int minutetoSeconds = GetComponent<Timer>().m_min * 60;
				//			DataController.Instance.playerData.timeLeftOnExit =  GetComponent<Timer>().m_sec + minutetoSeconds;
				StartCoroutine (PetHomeBreakAlert.instance.BreakTimerUp(totalTime));
			}
		}



	public void PetIconButton()
	{
		//Debug.Log ("<color=blue>" + "Finsi task" + "</color>");
		//DataController.Instance.playerData.isDeleting = false;
		GameObject.Find ("Pomodoro").GetComponent<TaskChecker> ().isDeleting = false;

		GetComponent<PomodoroController>().taskList[0].isDone = true;
		//Debug.Log (GetComponent<PomodoroController> ().taskList [0].task);

		//Save this task
		SaveFinishTask.instance.AddTask (GetComponent<PomodoroController>().taskList[0]);
		AchievementConditions.instance.FervidWorker();

		//AchievementConditions.instance.FirstJob ();
		//call passionate worker
		AchievementConditions.instance.Taskmaster();

		AchievementConditions.instance.MultiTasking();

		//6/28/2017
		//AchievementConditions.instance.MultiTasking();


		DataController.Instance.playerData.durationTime = 0;
		TaskUI();

		//GoldReward();

		//save task done
	}

	//bool isSlide = false;



	void InitializeTask()
	{
		DataController.Instance.playerData.taskpanelChecker = true;
		HideGoldAnimation.instance.HideAnimation ();
		//Debug.Log ("<color=red>" +"Sliding"+ "</color>");
		if(activeObject == null || activeObject.name != "TaskPanel")
		{
			GameObject taskObject = Instantiate(taskPanel,transform, false);

			//reposition in center
			taskObject.transform.localPosition = new Vector3(0,0,0);


			taskObject.name = "taskPanel";
			activeObject = taskObject;

			CloseMenu();
			isMenuActive = true;



			//removed slide
			//activeObject.transform.DOLocalMoveY(0, 1, false);
		}

	}        

	void InitializeSettings()
	{
		HideGoldAnimation.instance.HideAnimation ();

		if (tempSettingsPanel == null) {
			tempSettingsPanel = (GameObject)Instantiate (settingsPanel, transform, false);

		} else {
			
			tempSettingsPanel = (GameObject)Instantiate (settingsPanel, transform, false);
		}
		CloseMenu ();
	
		isMenuActive = true;
	
	}

	void InitializeStatistics()
	{
		HideGoldAnimation.instance.HideAnimation ();
//Debug.Log("Statistics");
//		if (tempStatsPanel == null) {
//			tempStatsPanel = Instantiate (statsPanel, transform, false);
//		} else {
//			tempStatsPanel.gameObject.SetActive (true);
//		}
//
//		CloseMenu ();
//
//		isMenuActive = true;
		DataController.Instance.playerData.taskpanelChecker = true;

		//Debug.Log ("<color=red>" +"Sliding"+ "</color>");
		if(activeObject == null || activeObject.name != "StatsPanel")
		{
			GameObject taskObject = Instantiate(statsPanel,transform, false);


			//reposition in center
			taskObject.transform.localPosition = new Vector3(0,0,0);


			taskObject.name = "statsPanel";
			activeObject = taskObject;

			CloseMenu();
			isMenuActive = true;

			//removed slide
			//activeObject.transform.DOLocalMoveY(0, 1, false);
		}

	}

	void InitializeAchievements()
	{
		HideGoldAnimation.instance.HideAnimation ();
		//Debug.Log("Achievements");

//		if (tempAchievementPanel == null) {
//			tempAchievementPanel = (GameObject)Instantiate (achievementPanel, transform, false);
//		} else {
//			tempAchievementPanel.gameObject.SetActive (true);
//		}
//	
//		CloseMenu ();
//
//		isMenuActive = true;


		if(activeObject == null || activeObject.name != "AchievementPanel")
		{
			GameObject taskObject = Instantiate(achievementPanel,transform, false);


			//reposition in center
			//taskObject.transform.localPosition = new Vector3(0,0,0);


			taskObject.name = "achievementPanel";
			activeObject = taskObject;

			CloseMenu();
			isMenuActive = true;

			//removed slide
			//activeObject.transform.DOLocalMoveY(0, 1, false);
		}



	}

	void InitializeFeedback()
	{
		//Debug.Log("Feedback");

//		if (tempFeedbackPanel == null) {
//			tempFeedbackPanel = Instantiate (feedbackPanel, transform, false);
//		} else {
//			tempFeedbackPanel.gameObject.SetActive(true);
//		}
//
//		CloseMenu ();
//
//		isMenuActive = true;
		HideGoldAnimation.instance.HideAnimation ();
		if(activeObject == null || activeObject.name != "FeedbackPanel")
		{
			GameObject taskObject = Instantiate(feedbackPanel,transform, false);


			//reposition in center
			//taskObject.transform.localPosition = new Vector3(0,0,0);


			taskObject.name = "feedbackPanel";
			activeObject = taskObject;

			CloseMenu();
			isMenuActive = true;

			//removed slide
			//activeObject.transform.DOLocalMoveY(0, 1, false);
		}
	}

	void ExitApp()
	{
		Framework.SoundManager.Instance.PlaySFX("seIconTapPomodoro");
		CloseMenu();
		exitPanel.gameObject.SetActive (true);
	}

	void Yes()
	{
		Framework.SoundManager.Instance.PlaySFX("seIconTapPomodoro");
		Application.Quit();
	}

	void No()
	{
		Framework.SoundManager.Instance.PlaySFX("seIconTapPomodoro");
		// menuPanel.transform.Find("PanelExit").gameObject.SetActive(false);
		exitPanel.gameObject.SetActive (false);
	}

	void CloseMenu()
	{
		ToggleMenuButton.ins.Hide ();
		//menuPanel.gameObject.SetActive(false);
	}

	//shows AddTaskPanel directly from PomodoroScreen
	void AddTask(System.Action callback = null)
	//public void AddTask()
	{


		//print ("EDITED BY RONALD WAS NOT WORKING.. ADD TASK");
		GameObject taskMaintenanceObject = Instantiate(taskMaintenancePanel, transform, false);
		taskMaintenanceObject.name = "TaskMaintenance";
		taskMaintenanceObject.GetComponent<TaskMaintenance>().SetUpAddTaskUI();
		taskMaintenanceObject.GetComponent<TaskMaintenance>().SetMaintenanceStateEvent(EnumCollection.MaintenanceStates.Add, callback);
		//hide menu
		ToggleMenuButton.ins.Hide ();
		Framework.SoundManager.Instance.PlaySFX("seIconTapPomodoro");
		isMenuActive = true;

	}  

	public void TaskUI()
	{        

		//DataController.Instance.playerData.ispetHome = false;

		Image petIconImage = null;

		try 
		{            
			petIconImage = pomodoroObject.transform.Find("petImage").GetComponent<Image>();
		}
		catch(NullReferenceException e)
		{

		}

		// clear pet icons
		if(DataController.Instance.taskList.items.Count == 0 )
		{
			for (int i = 0; i < tasksIconPanel.transform.childCount; i++)
			{
				tasksIconPanel.transform.GetChild(i).gameObject.SetActive(false);
			}
		}

		//task name
		if (GetComponent<PomodoroController> ().UnCompletedTask () > 0) {

			Time.timeScale = 1;
			Destroy (addTaskConfirm);
			Destroy (GameObject.Find ("AddTaskConfirmPanel"));

			//Debug.Log ("<color=red>"+ "GetComponent<PomodoroController> ().UnCompletedTask () > 0" +"</color>");

			int petIndex, categoryIndex;
			string breed;
			EnumCollection.PetIcon petIcon;
			Sprite petSprite = null;

			GetComponent<PomodoroController> ().GetActiveTasks ();


				pomodoroObject.transform.Find ("txtTaskName").GetComponent<TextMeshProUGUI> ().text = GetComponent<PomodoroController> ().taskList [0].task;

			if(DataController.Instance.playerData.state == EnumCollection.PomodoroStates.BreakScreen)
			{
				pomodoroObject.transform.Find ("txtTaskName").GetComponent<TextMeshProUGUI> ().text = "Short Break";
			}
			if(DataController.Instance.playerData.state == EnumCollection.PomodoroStates.LongBreakScreen)
			{
				pomodoroObject.transform.Find ("txtTaskName").GetComponent<TextMeshProUGUI> ().text = "Long Break";
			}
				
			for (int i = 0; i < tasksIconPanel.transform.childCount; i++) {
				//Time.timeScale = 0;

				//Debug.Log ("<color=blue>" + tasksIconPanel.transform.childCount + "</color>");

				tasksIconPanel.transform.GetChild (i).gameObject.SetActive (false);
			}                

			for (int i = 0; i < GetComponent<PomodoroController> ().taskList.Count; i++) {                
				//                if(GetComponent<PomodoroController>().taskList[i].isDone)
				//                {
				//                    tasksIconPanel.transform.GetChild(i).GetComponent<Image>().color = Color.green;
				//                }
				//                else
				//                {
				//                    tasksIconPanel.transform.GetChild(i).GetComponent<Image>().color = Color.blue;
				//                }

				petIndex = GetComponent<PomodoroController> ().taskList [i].activePetIndex;
				categoryIndex = GetComponent<PomodoroController> ().taskList [i].categoryIndex;
				breed = DataController.Instance.playerData.playerPetList [petIndex].petBreed;//petindex
				petIcon = (EnumCollection.PetIcon)DataController.Instance.categoryList.items [categoryIndex].petIconIndex;

				//Debug.Log (petIcon);

				switch (petIcon) {
				case EnumCollection.PetIcon.Generic:
					petSprite = petcategoryIcons.item.First (icon => icon.petBreed == breed).none;
					if (petIconImage != null && i == 0) {
						petIconImage.sprite = petSprite;
					}
					break;
				case EnumCollection.PetIcon.Play:
					petSprite = petcategoryIcons.item.First (icon => icon.petBreed == breed).play;
					if (petIconImage != null && i == 0) {
						petIconImage.sprite = petSprite;
					}
					break;
				case EnumCollection.PetIcon.School:
					petSprite = petcategoryIcons.item.First (icon => icon.petBreed == breed).school;
					if (petIconImage != null && i == 0) {
						petIconImage.sprite = petSprite;
					}
					break;
				case EnumCollection.PetIcon.Work:
					petSprite = petcategoryIcons.item.First (icon => icon.petBreed == breed).work;
					if (petIconImage != null && i == 0) {
						petIconImage.sprite = petSprite;
					}
					break;
				case EnumCollection.PetIcon.Workout:
					petSprite = petcategoryIcons.item.First (icon => icon.petBreed == breed).workOut;
					if (petIconImage != null && i == 0) {
						petIconImage.sprite = petSprite;
					}
					break;
				default:
					break;
				}                        

				//Debug.Log ("Pet Index " + petIndex + ", category index " + categoryIndex + ", breed " + breed + ", pet icon " + petIcon.ToString () + "hello world");

				//play sfx
				//Framework.SoundManager.Instance.PlaySFX ("seIconTapPomodoro");

				tasksIconPanel.transform.GetChild (i).gameObject.SetActive (true);

				tasksIconPanel.transform.GetChild (i).GetComponent<Image> ().sprite = petSprite;

				if (i != 0) {

					tasksIconPanel.transform.GetChild (i).GetComponent<Image> ().color = new Color32 (255, 255, 255, 128);
				}
			}
		} 
		//if (GetComponent<PomodoroController> ().CompletedTasks () == DataController.Instance.taskList.items.Count && DataController.Instance.playerData.isDeleting == false) {//GetComponent<PomodoroController>().UnCompletedTask() != 0 && DataController.Instance.taskList.items.Count != 0

		if (GetComponent<PomodoroController> ().CompletedTasks () == DataController.Instance.taskList.items.Count && GameObject.Find ("Pomodoro").GetComponent<TaskChecker> ().isDeleting == false) {//GetComponent<PomodoroController>().UnCompletedTask() != 0 && DataController.Instance.taskList.items.Count != 0
				
			//Debug.Log ("<color=red>" + "GetComponent<PomodoroController> ().CompletedTasks () == DataController.Instance.taskList.items.Count" + "</color>");

			// stop time
			Time.timeScale = 0;

			//Debug.Log ("No Task Available");


			//addTaskConfirm = Instantiate (Resources.Load<GameObject> ("Prefab/StoreDialog"), transform, false);
			addTaskConfirm = Instantiate (Resources.Load<GameObject> ("Prefab/AlertDialog"), transform, false);
			//if achievement is displayed
			//hide this popup
			if(GameObject.Find("panelPopupAchivement 1(Clone)") !=null)
			{
			addTaskConfirm.transform.SetParent (GameObject.Find ("Pomodoro").transform);
			addTaskConfirm.transform.SetSiblingIndex (0);
			}
			
			//addTaskConfirm.name = "AddTaskConfirmPanel";

//			ConfirmDialog confirmDialog = addTaskConfirm.GetComponent<ConfirmDialog> ();
//			confirmDialog.SetCaption ("You have finished all your tasks.\nWould you like to add more or stop?");
//			confirmDialog.SetConfirmCaption ("Add");
//			confirmDialog.SetCancelCaption ("Stop");

			ShowDialog dialog = addTaskConfirm.GetComponent<ShowDialog> ();
			dialog.SetCaption ("You have finished all your tasks.\nWould you like to add more or stop?");
			dialog.SetConfirmCaption ("Add");
			dialog.SetCancelCaption ("Stop");

			//add pop up event
			dialog.OnYesEvent += (() => {      
				//play sfx
				Framework.SoundManager.Instance.PlaySFX ("seIconTapPomodoro");
				//Debug.Log ("<color=green>" + "On Yes Event!!!$%^&*(" + "</color>");
				//Destroy (addTaskConfirm);
				addTaskConfirm.SetActive(true);
				TaskUI ();
				AddTask (() => {
					Time.timeScale = 1;
					addTaskConfirm.SetActive(false);
				});
			});

			dialog.OnNoEvent += (() => {                 

				//play sfx
				Framework.SoundManager.Instance.PlaySFX ("seIconTapPomodoro");
				//Debug.Log ("Stop Session");
				// increment total session count
				GetComponent<Timer> ().StopSession ();
				//Debug.Log ("<color=green>" + "On No Event!!!$%^&*(" + "</color>");
				//Destroy(addTaskConfirm);

			});
			//popUpDialog.OnNoEvent
			}
		 

	}    


	//Enum Collection json file load
	public void LoadEnumJson(string fileName)
	{
		string filePath = "";

		if (Application.platform == RuntimePlatform.Android)
		{
			// Android
			filePath = System.IO.Path.Combine(Application.streamingAssetsPath, fileName);

			// Android only use WWW to read file
			WWW reader = new WWW(filePath);
			while ( ! reader.isDone) {}

			string realPath = Application.persistentDataPath + fileName;
			System.IO.File.WriteAllBytes(realPath, reader.bytes);

			filePath = realPath;
		}
		else
		{
			// iOS
			filePath = System.IO.Path.Combine(Application.streamingAssetsPath, fileName);
		}

		if (File.Exists (filePath)) {
			//Debug.Log ("Test");
			string dataAsJson = File.ReadAllText (filePath);
			LocalizationDataList enumData = JsonUtility.FromJson<LocalizationDataList> (dataAsJson);

			for (int i = 0; i < enumData.items.Length; i++) 
			{
				List<string> enumlist = new List<string> ();
				for (int j = 0; j < enumData.items[i].value.Length; j++) {


					//Debug.Log ("<color=green>" + "Key: " +    enumData.items [i].key +    " ALL enum data: " + enumData.items [i].value[j] + "</color>");	


					enumlist.Add (enumData.items [i].value [j]); 
				}

				enumDictionarytimer.Add (enumData.items [i].key, enumlist);

				//enumlist.Add (enumData.items[i].value);
			}
			//			for (int i = 0; i < enumlist.Count; i++) {
			//				
			//				Debug.Log ("EnumList " + enumlist[i]);
			//			}
			//for (int i = 0; i < enumDictionary.Count; i++) {
			//	Debug.Log ("EnumDictionary Count: " + enumDictionary.Count);
			//}

			// Debug.Log ("Data loaded, dictionary contains: " + localizedText.Count + " entries");
		} else 
		{
			//Debug.LogError (filePath);
			//Debug.LogError ("Cannot find file!");
		}
	}

	void LanguageCheckDropdown()
	{
		//Localized dropdown depends on language selected in settings ***In progress 4/5/2016

		if (DataController.Instance.settings.languageIndex == 0 ) { // English
			LoadEnumJson("localizedtextDropdown_en.json");
			//Debug.Log("English");
		}
		else if (DataController.Instance.settings.languageIndex == 1 ) { // Spain
			LoadEnumJson("localizedtextDropdown_sp.json");
			//Debug.Log("Spanish");
		}
	}
	#endregion

	//giving gold reward

	public void GoldReward()
	{


		if (DataController.Instance.playerData.DateoftheDay == "none") {

			DataController.Instance.playerData.DateoftheDay = DateTime.Now.AddDays (-1).ToString ("MMMM dd, yyyy");
		}
			
		DateTime thisDate = Convert.ToDateTime (DataController.Instance.playerData.DateoftheDay);

		if ((DateTime.Today - thisDate).Days == 1) {

			DataController.Instance.playerData.targetGoldPerDay = 0;
			DataController.Instance.playerData.DateoftheDay = DateTime.Now.ToString ("MMMM dd, yyyy");
		}

		if (DataController.Instance.playerData.targetGoldPerDay < 250) {

			float time = DataController.Instance.settings.sessionDuration * 60;


			if (time > 1500) {
				DataController.Instance.playerData.gold += 15;
				DataController.Instance.playerData.targetGoldPerDay += 15;
				//Add to gold acquired
				DataController.Instance.playerData.goldacquired += 15;

				//Debug.Log ("<color=red>" + time + " > 1500" + "</color>");
				DataController.Instance.playerData.goldTimer = 0;
				//effect
				GoldAnimation("15");
				AchievementConditions.instance.CallRichKid();



			} 

			if (time == 1500) {
				DataController.Instance.playerData.gold += 10;
				DataController.Instance.playerData.targetGoldPerDay += 10;
				//Add to gold acquired
				DataController.Instance.playerData.goldacquired += 10;
				//Debug.Log ("<color=red>" + time + " == 1500" + "</color>");
				DataController.Instance.playerData.goldTimer = 0;
				GoldAnimation("10");
				AchievementConditions.instance.CallRichKid();


			} 

			if (time < 1500) {
				DataController.Instance.playerData.gold += 5;
				DataController.Instance.playerData.targetGoldPerDay += 5;
				//Add to gold acquired
				DataController.Instance.playerData.goldacquired += 5;
				//Debug.Log ("<color=red>" + time + " less than 1500" + "</color>");
				DataController.Instance.playerData.goldTimer = 0;
				GoldAnimation("5");
				AchievementConditions.instance.CallRichKid();

			}

		}
	}


	public void  SkipPopUp()
	{
		thereIsActiveDialog = true;
		
		GameObject skipPopUp = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), GameObject.Find("Canvas").transform, false);
		ShowDialog dialog = skipPopUp.GetComponent<ShowDialog>();

		dialog.SetCaption("Are you sure you want to skip current session?");
		dialog.SetConfirmCaption("Yes");
		dialog.SetCancelCaption("No");
		// set events
		dialog.OnYesEvent += () =>
		{
			thereIsActiveDialog = false;
			GetComponent<Timer>().Skip();
			Destroy (skipPopUp);	
		};
		dialog.OnNoEvent += () => 
		{
			thereIsActiveDialog = false;
			Destroy(skipPopUp);
		};
	}

	void  ShortSkipPopUp()
	{
		thereIsActiveDialog = true;
		skipshortPopup.gameObject.SetActive (true);
	}

	void  StopPopUp()
	{
		thereIsActiveDialog = true;
		stopPopup.gameObject.SetActive (true);
	}

	void  FinishPopUp()
	{
		thereIsActiveDialog = true;
		//finishTaskPopUp.gameObject.SetActive (true);

//		GameObject finishTaskPopUps = Instantiate (finishTaskPopUp, GameObject.Find ("Canvas").transform, false);
//		finishTaskPopUps.transform.Find ("Image").Find("btnOk").GetComponent<Button>().onClick.AddListener(()=> PetIconButton());
//		finishTaskPopUps.transform.Find ("Image").Find("btnOk").GetComponent<Button>().onClick.AddListener(()=> Destroy(finishTaskPopUps));
//		finishTaskPopUps.transform.Find ("Image").Find("btncancel").GetComponent<Button>().onClick.AddListener(()=> Destroy(finishTaskPopUps));

		GameObject finishTaskPopUps = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), GameObject.Find("Canvas").transform, false);
		ShowDialog dialog = finishTaskPopUps.GetComponent<ShowDialog>();
	
		dialog.SetCaption("Task will be marked as completed. Continue?");
		dialog.SetConfirmCaption("Yes");
		dialog.SetCancelCaption("No");
		// set events
		dialog.OnYesEvent += () =>
		{
			thereIsActiveDialog = false;
			PetIconButton();
			Destroy (finishTaskPopUps);	
		};
		dialog.OnNoEvent += () => 
		{
			thereIsActiveDialog = false;
			Destroy(finishTaskPopUps);
		};
	}


//	void GoldAnimation(string gold)
//	{
//		GameObject tweeningText = Instantiate(Resources.Load<GameObject>("Prefab/tweeningText"), FindObjectOfType<Canvas>().transform, false);
//		tweeningText.GetComponent<TextMeshProUGUI>().text = "+" + gold;
//		tweeningText.transform.DOLocalMoveY(30, 1.5f, false).OnComplete(() =>
//			{
//				tweeningText.GetComponent<TextMeshProUGUI>().DOFade(0, 1).OnComplete(() => Destroy(tweeningText));
//			}
//		);
//	}

	public void GoldAnimation(string gold)
	{
//		GameObject tweeningText = Instantiate(Resources.Load<GameObject>("Prefab/tweeningText"), FindObjectOfType<Canvas>().transform, false);
//		tweeningText.GetComponent<TextMeshProUGUI>().text = "+" + gold;
//
//		tweeningText.transform.localPosition = acquiredTextAppearPos;
//
//		tweeningText.transform.DOLocalMoveY((Screen.height/2), 1.5f, true).OnComplete(() =>
//			{
//				tweeningText.GetComponent<TextMeshProUGUI>().DOFade(0, 1).OnComplete(() => Destroy(tweeningText));
//			}
//		);
	
		//add gold acquired
		//check scene loaded
		string thisObject = "";


		Scene scene = SceneManager.GetActiveScene();


		if(!PomodoroUI.Instance.isMenuActive && !PetHomeController.ins.isOnPetHome)
		{
			if (scene.name == "Main") 
			{
				thisObject = "reward_text_location";

			} 
			else 
			{
				thisObject = "btnScreenShot";
			}

			Transform screenshot = GameObject.Find (thisObject).transform;
			GameObject tweeningText = Instantiate(Resources.Load<GameObject>("Prefab/tweeningText"), screenshot, false);

			tweeningText.GetComponent<TextMeshProUGUI>().text = "+" + gold;
			tweeningText.transform.localPosition = Vector2.zero;
			tweeningText.transform.localPosition = new Vector2 (0, tweeningText.transform.localPosition.y + (screenshot.GetComponent<RectTransform>().rect.height/2));

			tweeningText.transform.DOLocalMoveY(screenshot.GetComponent<RectTransform>().rect.height * 1.8f, 3, false).OnComplete(() =>
				{
					tweeningText.GetComponent<TextMeshProUGUI>().DOFade(0, 0.0001f).OnComplete(() => Destroy(tweeningText));
					//tweeningText.transform.DOLocalMoveY(target.localPosition.y, 3f, false)
				}
			);
		}

	
	}

	void GoldAcquiredText(string gold)
	{
		
	}

    public void ShowPetHome()
    {
        if(DataController.Instance.playerData.isTimerRunning && DataController.Instance.playerData.state == EnumCollection.PomodoroStates.TimerScreen)
        {
            //Debug.Log("Task Notif Panel");
            //Debug.Log("Load Home Timer");
            taskNotifPanel.gameObject.SetActive(true);
        }
        else if(!DataController.Instance.playerData.isTimerRunning && DataController.Instance.playerData.state == EnumCollection.PomodoroStates.TaskScreen)
        {
            //Debug.Log("Load Home TaskScreen");
            //PetHomeController.ins.PlayBGMClip ("bgmGenericHome");

            PlayBGM ();
            PetHomeController.ins.ShowPetHomeQuick ();
            ToggleMenuButton.ins.Hide ();
        }

        else if(DataController.Instance.playerData.isTimerRunning && DataController.Instance.playerData.state == EnumCollection.PomodoroStates.LongBreakScreen)
        {
            //Debug.Log("Load Home LongBreakScreen");
            PlayBGM ();
            PetHomeController.ins.ShowPetHomeQuick ();
            ToggleMenuButton.ins.Hide ();
        }

        else if(DataController.Instance.playerData.isTimerRunning && DataController.Instance.playerData.state == EnumCollection.PomodoroStates.BreakScreen)
        {


            //Debug.Log("Load Home-BREAK Screen");
            PlayBGM ();
            PetHomeController.ins.ShowPetHomeQuick ();
            ToggleMenuButton.ins.Hide ();
            //          int minutetoSeconds = GetComponent<Timer>().m_min * 60;
            //          DataController.Instance.playerData.timeLeftOnExit =  GetComponent<Timer>().m_sec + minutetoSeconds;


        }
    }


	// <summary>
	//	set notification alert
	// </summary>
	public void setNotifSfx()
	{
		//Debug.Log("test");
		int currentPetIndex = DataController.Instance.playerData.currentPetIndex;

		if (DataController.Instance.settings.isPreEndAlarmOn == true) 
		{
			if (DataController.Instance.settings.preEndAlertIndex == 0) 
			{
				Framework.SoundManager.Instance.PlaySFX ("seAlarm");
			} 
			else if (DataController.Instance.settings.preEndAlertIndex == 1) 
			{
				Framework.SoundManager.Instance.PlaySFX ("seDogBarkSmall");
			} 
			else if (DataController.Instance.settings.preEndAlertIndex == 2) 
			{
				Framework.SoundManager.Instance.PlaySFX ("seCatMeow");
			}
		}
	}


	/// <summary>
	/// minute format
	/// </summary>
	private string timeFormat(float time)
	{
		string format = "";
		if (time > 1 ) 
		{
			format = "minutes";
		} 
		else 
		{
			format = "minute";
		}
		return format;
	}
}
