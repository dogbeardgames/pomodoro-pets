﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotateIcon : MonoBehaviour {


	[SerializeField]
	Image timer;

	[SerializeField]
	RectTransform timerImage;

	private RectTransform edgeRect;
	private RectTransform imgRect;

	public Transform pawIcon;

	public bool HideWhenNotFilled = true;

	public static RotateIcon instance;

	void Start()
	{
		instance = this;
		//timer = GetComponent<Image>();
		imgRect = GetComponent<RectTransform> ();
		//edgeRect = transform.GetChild (0).GetComponent<RectTransform> ();
		edgeRect = transform.GetComponent<RectTransform> ();
		//

		try {
			pawIcon = transform.Find("iconTimerImage").GetComponent<Transform>();
		} catch (System.Exception ex) {
			
		}

	}


	 void Update()
	{
		//


		//edgeRect.localPosition = new Vector2 (pawIcon.fillAmount * imgRect.anchorMin.y,  edgeRect.anchoredPosition.x);
		float fillAmount = timer.fillAmount;
		//edgeRect.anchoredPosition = new Vector2(fillAmount,0);
		//edgeRect.localPosition = new Vector3 (Mathf.Sin (fillAmount * edgeRect.anchorMin.x *5 ), Mathf.Cos (fillAmount * edgeRect.anchorMax.x * 5 )) * 50;

		//edgeRect.anchorMin = new Vector2(edgeRect.anchorMin.x, fillAmount);
		//edgeRect.anchorMax = new Vector2 (edgeRect.anchorMax.x, fillAmount);      
		//edgeRect.anchoredPosition = Vector2.zero;

		//edgeRect.Rotate (new Vector3 (0, 0, 360+1));

		float buttonAngle = fillAmount * 360f;
		edgeRect.eulerAngles = new Vector3 (0f, 0f, buttonAngle);

		pawIcon.eulerAngles= new Vector3 (0f, 0f, 0f);

	}


}
