﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PetHomeBreakAlert : MonoBehaviour {

	float preEndAlertTime = 0;
	float shortBreakDurationTime = 0;
	float longBreakDurationTime = 0;
	float totalTime = 0;

	[SerializeField]
	CustomScrollView petHomeCustomScrollView;  

	public static PetHomeBreakAlert instance;

	// Use this for initialization
	void Start () 
	{
		instance = this;

	   	Timer timer = GameObject.Find("Pomodoro").GetComponent<Timer>();
		preEndAlertTime	 	 = DataController.Instance.settings.alarmTime * 60;
		shortBreakDurationTime  = DataController.Instance.settings.shortBreakDuration * 60;
		longBreakDurationTime   = DataController.Instance.settings.longBreakDuration * 60;

		float minute = timer.m_min;
		float seconds = timer.m_sec;
		totalTime = (minute * 60) + seconds;
		//Debug.Log ("Total Time: " + totalTime);



	}
	/// <summary>
	/// Format of time.
	/// </summary>
	/// <returns>The format.</returns>
	/// <param name="time">Time.</param>
	private string timeFormat(float time)
	{
		string format = "";
		if (time > 1) 
		{
			format = "minutes";
		} 
		else 
		{
			format = "minute";
		}
		return format;
	}
	bool isEnd = false;
	bool isPreEnd = false;
	/// <summary>
	/// Breaks the timer up.
	/// </summary>
	/// <returns>The timer up.</returns>
	/// <param name="time">Time.</param>
	public IEnumerator BreakTimerUp(float time)
	{
		isEnd = false;
		isPreEnd = false;
		while (time > 0) 
		{
			//Debug.Log (time--);
			yield return new WaitForSeconds (1);

			if (DataController.Instance.settings.isPreEndAlarmOn) 
			{
				if (time == preEndAlertTime && !isPreEnd) 
				{
					isPreEnd = true;
					CallPopUpPreEndAlarmTime ();
					//Debug.LogError ("test break");
				}
			}
			time--;
		}

		//Debug.LogError ("Break session Finished ");


		if (!isEnd) 
		{ 
			isEnd = true;

			Debug.Log ("test counter");
			CallPopUp ();
		}


			
	}


	/// <summary>
	/// Calls the pop up pre end alarm time.
	/// </summary>
	void CallPopUpPreEndAlarmTime()
	{
		//Debug.Log ("Pre end alarm");
		GameObject window = Instantiate (Resources.Load<GameObject>("Prefab/StoreDialog"),GameObject.Find("Canvas").transform, false);
		ShowDialog dialog = window.GetComponent <ShowDialog>();
		if (DataController.Instance.playerData.state == EnumCollection.PomodoroStates.BreakScreen)
		{
			dialog.SetCaption ("You have " + preEndAlertTime/60 + " " + timeFormat(preEndAlertTime/60) + " left in your short break session.");
		}
		else if (DataController.Instance.playerData.state == EnumCollection.PomodoroStates.LongBreakScreen)
		{
			dialog.SetCaption ("You have " + preEndAlertTime/60 + " " + timeFormat(preEndAlertTime/60) + " left in your long break session.");
		}

		dialog.SetConfirmCaption ("Ok");
		dialog.DisableNO ();
		dialog.OnYesEvent += delegate {
			
		};
	}
	/// <summary>
	/// Calls the pop up.
	/// </summary>
	void CallPopUp()
	{

		if (GameObject.Find ("StoreDialog(Clone)") != null) {
			Destroy (GameObject.Find ("StoreDialog(Clone)"));
		}

		//Debug.LogError ("test popup");
		GameObject window = Instantiate (Resources.Load<GameObject>("Prefab/StoreDialog"),GameObject.Find("Canvas").transform, false);
		ShowDialog dialog = window.GetComponent <ShowDialog>();
		dialog.SetConfirmCaption ("Ok");
		dialog.SetCaption ("Your break session is over.");

		dialog.DisableNO ();
		dialog.OnYesEvent += delegate {

			//Fixed for CM-21
			Framework.SoundManager.Instance.FadeOutInside();

			DataController.Instance.playerData.ispetHome = false;
		//	SceneController.Instance.LoadingScene ("Main");
			petHomeCustomScrollView.transform.DOLocalMoveX(petHomeCustomScrollView.Content.GetComponent<RectTransform>().rect.width/2, 0.75f, false).OnComplete(()=>petHomeCustomScrollView.SetContentXInitialPosition ());

			if (DataController.Instance.playerData.state == EnumCollection.PomodoroStates.BreakScreen)
			{
				DataController.Instance.playerData.state = EnumCollection.PomodoroStates.TimerScreen;
			}
			else if (DataController.Instance.playerData.state == EnumCollection.PomodoroStates.LongBreakScreen)
			{
				DataController.Instance.playerData.currentSessionCount = 1;
				DataController.Instance.playerData.state = EnumCollection.PomodoroStates.TaskScreen;
				DataController.Instance.playerData.ispetHome = false;
				//print("AW");
				UnityEngine.SceneManagement.SceneManager.LoadScene("Main", UnityEngine.SceneManagement.LoadSceneMode.Single);
			}




			Destroy(GameObject.Find("Destroy()"));
		};
	}

}
