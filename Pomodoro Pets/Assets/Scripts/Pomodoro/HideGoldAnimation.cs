﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideGoldAnimation : MonoBehaviour {

	public static HideGoldAnimation instance;
	// Use this for initialization
	void Start () 
	{
		instance = this;
	}

	public void HideAnimation()
	{
		if (GameObject.Find ("reward_text_location/tweeningText(Clone)") != null) 
		{
			GameObject.Find ("reward_text_location/tweeningText(Clone)").gameObject.SetActive (false);
		} 
	}
}
