﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DestroyGameObject : MonoBehaviour {

	[SerializeField]
	GameObject paneltoDestroy;


	// Use this for initialization
	public void DestroyGame () {
		Destroy (paneltoDestroy);
		//print("DO NOT DO THIS!");
	}

}
