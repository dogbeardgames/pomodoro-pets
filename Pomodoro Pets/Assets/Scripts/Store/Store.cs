﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Purchasing;
using UnityEngine.SceneManagement;
using TMPro;

public class Store : MonoBehaviour {

	public static Store ins;

	public int numOfPets;
    [SerializeField]
    Button btnback;

    [SerializeField]
    Transform tabGroup;

    GameObject shopShowDialog;

    // button that will be enabled after the item is purchased
    [HideInInspector]
    public Button[] buttonToActivate;
	[SerializeField]
	TextMeshProUGUI txtGold, txtGem;
	[SerializeField]
	Sprite btnOwnedSprite;

    public delegate void StoreCallBack();
    public StoreCallBack storeCallBack;
	// Use this for initialization

	public delegate void OnCloseHandler ();
	public OnCloseHandler OnClose;

	public Sprite GetSpriteOwned(){
		return btnOwnedSprite;
	}

	void Awake()
	{
		ins = this;
	}
	void Start () {
		numOfPets = DataController.Instance.playerData.playerPetList.Count;
        btnback.onClick.AddListener(Back);
		UpdateVirtualMoney ();
	}

	public void UpdateVirtualMoney()
	{
		txtGold.text = DataController.Instance.playerData.gold.ToString();
		txtGem.text = DataController.Instance.playerData.gem.ToString();
//        // for gold
//        if(DataController.Instance.playerData.gold >= 10000 && DataController.Instance.playerData.gold <= 999999)
//        {
//            float gold = DataController.Instance.playerData.gold / 1000;
//            Mathf.Floor(gold);
//            txtGold.text = gold + "K";
//            //            txtGold.text = gold.ToString("###.#0K");
//        }
//        else if(DataController.Instance.playerData.gold >= 1000000)
//        {
//            float gold = DataController.Instance.playerData.gold / 1000000;
//            Mathf.Floor(gold);
//            //            txtGold.text = gold.ToString("###.#0M");
//            txtGold.text = gold + "M";
//        }
//        else
//        {            
//            txtGold.text = DataController.Instance.playerData.gold.ToString("#,##0");
//        }
//        // for gem
//        if(DataController.Instance.playerData.gem >= 10000 && DataController.Instance.playerData.gem <= 999999)
//        {
//            float gem = DataController.Instance.playerData.gem / 1000;
//            Mathf.Floor(gem);
//            //            txtGem.text = gem.ToString("###.##K");
//            txtGem.text = gem + "K";
//        }
//        else if(DataController.Instance.playerData.gem >= 1000000)
//        {
//            float gem = DataController.Instance.playerData.gem / 1000000;
//            Mathf.Floor(gem);
//            //            txtGem.text = gem.ToString("###.##M");
//            txtGem.text = gem + "M";
//        }
//        else
//        {            
//            txtGem.text = DataController.Instance.playerData.gem.ToString("#,##0");
//        }  
	}
	
	// Update is called once per frame
	void Update () {
		//SpineMeshEnabler.ins.EnableMeshRenderer (false);
		if (SpineMeshEnabler.ins != null) {
			SpineMeshEnabler.ins.ActiveOnUpdate (true);
		}
	}       
		
    public void OpenStore(string tabName)
    {        
        if (tabName != "")
        {
            for (int i = 0; i < tabGroup.childCount; i++)
            {
                tabGroup.GetChild(i).GetComponent<Toggle>().isOn = tabGroup.GetChild(i).name == tabName ? true : false;

            }
        }      
    }

    #region Events
    void Back()
    {
		//SpineMeshEnabler.ins.EnableMeshRenderer (true);
		if (SpineMeshEnabler.ins != null) {
			SpineMeshEnabler.ins.ActiveOnUpdate (false);
		}
		SpineMeshEnabler.ins.EnableMesh ();
		if (OnClose != null)
			OnClose ();
        Destroy(gameObject);		
    }

	void OnEnable()
	{

	
	}
	void OnDestroy()
	{

	
	
	}
	void OnDisable()
	{

	

	}
    #endregion

    #region METHODS

    #endregion

    #region STORE
    // callback for purchases completed
    public void CompletePurchase(Product product)
    {        
        //Debug.Log("product " + product.definition.id);
        Framework.SoundManager.Instance.PlaySFX("seBuy");
        switch (product.definition.id)
        {
            case "Bark Vader":
                CallBack();
                ActivateButton();
                OpenDialog("You have purchased Bark Vader Full Set!", () =>
                    {
                        Destroy(shopShowDialog);
                    });
                break;
            case "Cat-tain Americat":
                CallBack();
                ActivateButton();
                OpenDialog("You have purchased Cat-tain Americat Full Set!", () =>
                    {
                        Destroy(shopShowDialog);
                    });               
                break;
            case "Barking Bad":
                CallBack();
                ActivateButton();
                OpenDialog("You have purchased Barking Bad Full Set!", () =>
                    {
                        Destroy(shopShowDialog);
                    });   
                break;
            case "Hipster Fedora":
                CallBack();
                ActivateButton();
                OpenDialog("You have purchased Hipster Fedora!", () =>
                    {
                        Destroy(shopShowDialog);
                    });
                break;
            case "Panda Hat":
                CallBack();
                ActivateButton();
                OpenDialog("You have purchased Panda Hat!", () =>
                    {
                        Destroy(shopShowDialog);
                    });
                break;
            case "Reverse Cap":
                CallBack();
                ActivateButton();
                OpenDialog("You have purchased Reverse Cap!", () =>
                    {
                        Destroy(shopShowDialog);
                    });
                break;
            case "Cat Collar":
                CallBack();
                ActivateButton();
                OpenDialog("You have purchased Cat Collar!", () =>
                    {
                        Destroy(shopShowDialog);
                    });
                break;
            case "Dog Collar":
                CallBack();
                ActivateButton();
                OpenDialog("You have purchased Dog Collar!", () =>
                    {
                        Destroy(shopShowDialog);
                    });
                break;
            case "Bowtie":
                CallBack();
                ActivateButton();
                OpenDialog("You have purchased Bowtie!", () =>
                    {
                        Destroy(shopShowDialog);
                    });
                break;
            case "Scarf":
                CallBack();
                ActivateButton();
                OpenDialog("You have purchased Scarf!", () =>
                    {
                        Destroy(shopShowDialog);
                    });
                break;
            case "Shades":
                CallBack();
                ActivateButton();
                OpenDialog("You have purchased Shades!", () =>
                    {
                        Destroy(shopShowDialog);
                    });
                break;
            case "Geek Glasses":
                CallBack();
                ActivateButton();
                OpenDialog("You have purchased Geek Glasses!", () =>
                    {
                        Destroy(shopShowDialog);
                    });
                break;
            case "Pink Side Flower":
                CallBack();
                ActivateButton();
                OpenDialog("You have purchased Pink Side Flower!", () =>
                    {
                        Destroy(shopShowDialog);
                    });
                break;
            case "Red Side Flower":
                CallBack();
                ActivateButton();
                OpenDialog("You have purchased Red Side Flower!", () =>
                    {
                        Destroy(shopShowDialog);
                    });
                break;
            case "Space":
                CallBack();
                ActivateButtonLocation();
                AchievementConditions.instance.AchievementBackground();
                OpenDialog("You have puschased Outer Space Background!", () =>
                    {                        
                        Destroy(shopShowDialog);
                    });
                break;
            case "Japan":
                CallBack();
                ActivateButtonLocation();
                AchievementConditions.instance.AchievementBackground();
                OpenDialog("You have puschased Japan Background!", () =>
                    {                        
                        Destroy(shopShowDialog);
                    });
                break;
            case "Silver Hammer":
                CallBack();
                OpenDialog("You have puschased Silver Hammer!", () =>
                    {                        
                        Destroy(shopShowDialog);
                    });
                break;
            case "Gold Hammer":
                CallBack();
                OpenDialog("You have puschased Gold Hammer!", () =>
                    {                        
                        Destroy(shopShowDialog);
                    });
                break;
            case "Husky":
                CallBack();
                AchievementConditions.instance.PetCountChecker();
                AchievementConditions.instance.BigDogChecker();
                AchievementConditions.instance.DogCounter();
                OpenDialog("You adopted Husky!", () =>
                    {                        
                        Destroy(shopShowDialog);
                    });
                break;
            case "Pug":
                CallBack();
                AchievementConditions.instance.PetCountChecker();
                AchievementConditions.instance.SmallDogChecker();
                AchievementConditions.instance.DogCounter();
                OpenDialog("You adopted Pug!", () =>
                    {                        
                        Destroy(shopShowDialog);
                    });
                break;
            case "Rottweiler":
                CallBack();
                AchievementConditions.instance.PetCountChecker();
                AchievementConditions.instance.BigDogChecker();
                AchievementConditions.instance.DogCounter();
                OpenDialog("You adopted Rottweiler!", () =>
                    {                        
                        Destroy(shopShowDialog);
                    });
                break;
            case "Yorkie":
                CallBack();
                AchievementConditions.instance.PetCountChecker();
                AchievementConditions.instance.SmallDogChecker();
                AchievementConditions.instance.DogCounter();
                OpenDialog("You adopted Yorkie!", () =>
                    {                        
                        Destroy(shopShowDialog);
                    });
                break;
            case "Chow Chow":
                CallBack();
                AchievementConditions.instance.PetCountChecker();
                AchievementConditions.instance.BigDogChecker();
                AchievementConditions.instance.DogCounter();
                OpenDialog("You adopted Chow Chow!", () =>
                    {                        
                        Destroy(shopShowDialog);
                    });
                break;
            case "Poodle":
                CallBack();
                AchievementConditions.instance.PetCountChecker();
                AchievementConditions.instance.SmallDogChecker();
                AchievementConditions.instance.DogCounter();
                OpenDialog("You adopted Poodle!", () =>
                    {                        
                        Destroy(shopShowDialog);
                    });
                break;
            case "Corgi":
                CallBack();
                AchievementConditions.instance.PetCountChecker();
                AchievementConditions.instance.SmallDogChecker();
                AchievementConditions.instance.DogCounter();
                OpenDialog("You adopted Corgi!", () =>
                    {                        
                        Destroy(shopShowDialog);
                    });
                break;
            case "Akita":
                CallBack();
                AchievementConditions.instance.PetCountChecker();
                AchievementConditions.instance.BigDogChecker();
                AchievementConditions.instance.DogCounter();
                OpenDialog("You adopted Akita!", () =>
                    {                        
                        Destroy(shopShowDialog);
                    });
                break;
            case "Siamese":
                CallBack();
                AchievementConditions.instance.PetCountChecker();
                AchievementConditions.instance.ThreeShortHairedCat();
                AchievementConditions.instance.CatCounter();
                OpenDialog("You adopted Siamese!", () =>
                    {                        
                        Destroy(shopShowDialog);
                    });
                break;
            case "Maine Coon":
                CallBack();
                AchievementConditions.instance.PetCountChecker();
                AchievementConditions.instance.CheckThreeHairyCat();
                AchievementConditions.instance.CatCounter();
                OpenDialog("You adopted Maine Coon!", () =>
                    {                        
                        Destroy(shopShowDialog);
                    });
                break;
            case "Bengal":
                CallBack();
                AchievementConditions.instance.PetCountChecker();
                AchievementConditions.instance.ThreeShortHairedCat();
                AchievementConditions.instance.CatCounter();
                OpenDialog("You adopted Bengal!", () =>
                    {                        
                        Destroy(shopShowDialog);
                    });
                break;
            case "Sphynx":
                CallBack();
                AchievementConditions.instance.PetCountChecker();
                AchievementConditions.instance.ThreeShortHairedCat();
                AchievementConditions.instance.CatCounter();
                OpenDialog("You adopted Sphynx!", () =>
                    {                        
                        Destroy(shopShowDialog);
                    });
                break;
            case "Himalayan":
                CallBack();
                AchievementConditions.instance.PetCountChecker();
                AchievementConditions.instance.CheckThreeHairyCat();
                AchievementConditions.instance.CatCounter();
                OpenDialog("You adopted Himalayan!", () =>
                    {                        
                        Destroy(shopShowDialog);
                    });
                break;
            case "Munchkin":
                CallBack();
                AchievementConditions.instance.PetCountChecker();
                AchievementConditions.instance.CheckThreeHairyCat();
                AchievementConditions.instance.CatCounter();
                OpenDialog("You adopted Munchkin!", () =>
                    {                        
                        Destroy(shopShowDialog);
                    });
                break;
            case "Ragdoll":
                CallBack();
                AchievementConditions.instance.PetCountChecker();
                AchievementConditions.instance.CheckThreeHairyCat();
                AchievementConditions.instance.CatCounter();
                OpenDialog("You adopted Ragdoll!", () =>
                    {                        
                        Destroy(shopShowDialog);
                    });
                break;
            case "Bombay":
                CallBack();
                AchievementConditions.instance.PetCountChecker();
                AchievementConditions.instance.ThreeShortHairedCat();
                AchievementConditions.instance.CatCounter();
                OpenDialog("You adopted Bombay!", () =>
                    {                        
                        Destroy(shopShowDialog);
                    });
                break;
            case "Gem Pack 1":
//                DataController.Instance.playerData.gem += 120; 
                FindObjectOfType<PetActionReward>().UserDefinedGemReward(120);
				//FindObjectOfType<PetActionReward>().UserDefinedGemReward(120);// gem animation THIS DOUBLES UP THE VALUE
                OpenDialog("Got 120 Gems!", () =>
                    {                        
                        Destroy(shopShowDialog);
                    });
                break;
            case "Gem Pack 2":
//                DataController.Instance.playerData.gem += 400;
                FindObjectOfType<PetActionReward>().UserDefinedGemReward(400);
                //FindObjectOfType<PetActionReward>().UserDefinedGemReward(400);// gem animation
                OpenDialog("Got 400 Gems!", () =>
                    {                        
                        Destroy(shopShowDialog);
                    });
                break;
            case "Gem Pack 3":                 
//                DataController.Instance.playerData.gem += 800;
                FindObjectOfType<PetActionReward>().UserDefinedGemReward(800);
                //FindObjectOfType<PetActionReward>().UserDefinedGemReward(800);// gem animation
                OpenDialog("Got 800 Gems!", () =>
                    {                        
                        Destroy(shopShowDialog);
                    });
                break;
            case "Gold Pack 1":
			
//                DataController.Instance.playerData.gold += 10000;
                FindObjectOfType<PetActionReward>().UserDefinedGoldReward(10000);
                //FindObjectOfType<PetActionReward>().UserDefinedGoldReward(10000);// gold animation
                OpenDialog("Got 10,000 Gold!", () =>
                    {                        
                        Destroy(shopShowDialog);
                    });
                break;
            case "Gold Pack 2":
//                DataController.Instance.playerData.gold += 25000;
                FindObjectOfType<PetActionReward>().UserDefinedGoldReward(25000);
                //FindObjectOfType<PetActionReward>().UserDefinedGoldReward(25000);// gold animation
                OpenDialog("Got 25,000 Gold!", () =>
                    {                        
                        Destroy(shopShowDialog);
                    });
                break;
            case "Gold Pack 3":
//                DataController.Instance.playerData.gold += 75000;
                FindObjectOfType<PetActionReward>().UserDefinedGoldReward(75000);
                //FindObjectOfType<PetActionReward>().UserDefinedGoldReward(75000);// gold animation
                OpenDialog("Got 75,000 Gold!", () =>
                    {
                        Destroy(shopShowDialog);
                    });
                break;
            case "Bark Vader Full Costume with BG":
                CallBack();

                break;
            case "Starter Pack":
                CallBack();
                ActivateButtonLocation();
                OpenDialog("You have purchased Starter Pack!", () =>
                    {
                        Destroy(shopShowDialog);
                    });
                break;
            case "Hoarder Pack":
                break;
            default:
                break;
        }

        UpdateVirtualMoney();
    }

    // process purchasing in store for common items
    public void CompletePurchase(FoodStoreItem foodStoreItem)
    {
		txtGold.text = DataController.Instance.playerData.gold.ToString();
		txtGem.text = DataController.Instance.playerData.gem.ToString();
    }

    public void PurchaseFailed(Product product, PurchaseFailureReason reason)
    {
        //Debug.Log("Failed to purchase " + ". Reason: " + reason);
        if(reason == PurchaseFailureReason.Unknown)
        {
            GameObject affirmDialog = Instantiate(Resources.Load<GameObject>("Prefab/AffirmDialog"), GameObject.Find("Canvas").transform, false);
            AffirmDialog _affirmDialog = affirmDialog.GetComponent<AffirmDialog>();
            _affirmDialog.SetCaption("Failed to purchase " + product.definition.id + ".\nCan't connect to store.");
            _affirmDialog.SetConfirmCaption("OK");
            _affirmDialog.OnYesEvent += () => { Destroy(affirmDialog); };
        }
    }

    public void OpenDialog(string caption, ShowDialog.PopUp popUp)
    {        
        ShowDialog showDialog;

		shopShowDialog = Instantiate(Resources.Load<GameObject>("Prefab/StoreDialog"), transform.parent, false);
        showDialog = shopShowDialog.GetComponent<ShowDialog>();

        showDialog.SetCaption(caption);
        showDialog.SetConfirmCaption("OK");
        showDialog.OnYesEvent += () => {Destroy(shopShowDialog);};
        showDialog.OnYesEvent += popUp;
		showDialog.DisableNO ();
    }

    void CallBack()
    {
        if(storeCallBack != null)
            storeCallBack();
    }

    public void UpdateFoodItem()
    {
        PetButtonItem[] petButtonItem = FindObjectsOfType<PetButtonItem>();
        for (int i = 0; i < petButtonItem.Length; i++)
        {
            petButtonItem[i].UpdateFoodItem();
        }
    }

//    public void UpdateEquipmentItem()
//    {
//        PetButton[] petButton = FindObjectsOfType<PetButton>();
//        for (int i = 0; i < petButton.Length; i++)
//        {
//            
//        }
//    }

    void ActivateButton()
    {
//        if(buttonToActivate != null)
//        {
//            for (int i = 0; i < buttonToActivate.Length; i++)
//            {
//                buttonToActivate[i].GetComponent<Image>().color = new Color32(255, 255, 255, 255);
//            }
//        } 
        PetButton[] petButtons = FindObjectsOfType<PetButton>();
        for (int i = 0; i < petButtons.Length; i++)
        {
//            petButtons[i].SetApply();
            petButtons[i].Unlock();
        }
    }

    void ActivateButtonLocation()
    {
        if (SceneManager.GetActiveScene().name == "PetsModule")
        {
            PetLocation[] petLocationButton = FindObjectsOfType<PetLocation>();
            for (int i = 0; i < petLocationButton.Length; i++)
            {
                if (DataController.Instance.playerData.backGround == petLocationButton[i].locationName)
                {
                    petLocationButton[i].ChangeLocation();
                    petLocationButton[i].Unlock();
                    break;
                }
            }
        }
        else if(SceneManager.GetActiveScene().name == "Main")
        {
            FindObjectOfType<PetHomeBackground>().SetBackground();
        }
    }  

    public void UpdatePlayThingsObject()
    {
        PlayThing[] playThing = FindObjectsOfType<PlayThing>();
        for (int i = 0; i < playThing.Length; i++)
        {
            playThing[i].UpdatePlayThingObject();
        }
    }
    #endregion
}
