﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class DoubleStorePanel : MonoBehaviour {

    [SerializeField]
    Button[] leftSide;
    [SerializeField]
    Button[] rightSide;

    [SerializeField]
    Button btnPrev, btnNext;

    int currentIndex = 0;

	// Use this for initialization
	void Start () {
        Initialize();
	}

    #region METHODS

    void Initialize()
    {
        if (leftSide.Length > 1)
        {
            btnPrev.onClick.AddListener(Prev);
            btnNext.onClick.AddListener(Next);
        }
        else
        {
            btnPrev.gameObject.SetActive(false);
            btnNext.gameObject.SetActive(false);
        }
    }

    void Prev()
    {
        if(currentIndex == 0)
        {
            leftSide[currentIndex].gameObject.SetActive(false);
            rightSide[currentIndex].gameObject.SetActive(false);
            currentIndex = leftSide.Length - 1;
            leftSide[currentIndex].gameObject.SetActive(true);
            rightSide[currentIndex].gameObject.SetActive(true);
        }
        else
        {
            leftSide[currentIndex].gameObject.SetActive(false);
            rightSide[currentIndex].gameObject.SetActive(false);
            currentIndex--;
            leftSide[currentIndex].gameObject.SetActive(true);
            rightSide[currentIndex].gameObject.SetActive(true);
        }
    }

    void Next()
    {
        if(currentIndex == leftSide.Length - 1)
        {
            leftSide[currentIndex].gameObject.SetActive(false);
            rightSide[currentIndex].gameObject.SetActive(false);
            currentIndex = 0;
            leftSide[currentIndex].gameObject.SetActive(true);
            rightSide[currentIndex].gameObject.SetActive(true);
        }
        else
        {
            leftSide[currentIndex].gameObject.SetActive(false);
            rightSide[currentIndex].gameObject.SetActive(false);
            currentIndex++;
            leftSide[currentIndex].gameObject.SetActive(true);
            rightSide[currentIndex].gameObject.SetActive(true);
        }
    }
    #endregion
}
