﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class Gold : MonoBehaviour {

    TextMeshProUGUI txtGold;

    Button button;

    Scene scene;
       
	GameObject store;
	// Use this for initialization
	void Start () {
        txtGold = transform.GetChild(0).GetComponent<TextMeshProUGUI>();

        button = GetComponent<Button>();
        button.onClick.AddListener(Click);

		if (SceneManager.GetActiveScene ().name == "Main") 
		{
			ToggleMenuButton.ins.Hide ();	
		}

        
	}  

	void OnEnable()
	{
		
	}
		
	void LateUpdate()
	{
		CurrentGold();
	}

    public void CurrentGold()
    {
        if(DataController.Instance.playerData.gold >= 10000 && DataController.Instance.playerData.gold <= 999999)
        {
            float gold = DataController.Instance.playerData.gold / 1000;
            Mathf.Floor(gold);
            txtGold.text = gold + "K";
//            txtGold.text = gold.ToString("###.#0K");
        }
        else if(DataController.Instance.playerData.gold >= 1000000)
        {
            float gold = DataController.Instance.playerData.gold / 1000000;
            Mathf.Floor(gold);
//            txtGold.text = gold.ToString("###.#0M");
            txtGold.text = gold + "M";
        }
        else
        {            
            txtGold.text = DataController.Instance.playerData.gold.ToString("#,##0");
        }       
			
        scene = SceneManager.GetActiveScene();
       // Debug.Log("Active scene " + scene.name);
    }

    void Click()
    {
        if(scene.name == "Main")
        {
				if(DataController.Instance.playerData.isTimerRunning && DataController.Instance.playerData.state == EnumCollection.PomodoroStates.TimerScreen)
				{
				FindObjectOfType<PomodoroUI>().taskNotifPanel.gameObject.SetActive(true);
				}
				else if(!DataController.Instance.playerData.isTimerRunning && DataController.Instance.playerData.state == EnumCollection.PomodoroStates.TaskScreen)
				{
					CallStore ();
					ToggleMenuButton.ins.Hide ();
				}

				else if(DataController.Instance.playerData.isTimerRunning && DataController.Instance.playerData.state == EnumCollection.PomodoroStates.LongBreakScreen)
				{
					CallStore ();
					ToggleMenuButton.ins.Hide ();
				}

				else if(DataController.Instance.playerData.isTimerRunning && DataController.Instance.playerData.state == EnumCollection.PomodoroStates.BreakScreen)
				{
					CallStore ();
					ToggleMenuButton.ins.Hide ();
				}
        }
        else
        {
            FindObjectOfType<PetUIController>().Store("toggleOthers");
        }
		Framework.SoundManager.Instance.PlaySFX("seIconTapPets");
    }

	void CallStore()
	{
		store = (GameObject)Instantiate(Resources.Load<GameObject>("Prefab/StorePanel"), GameObject.Find("Canvas").transform, false);
		//store.transform.Find("btnBack").GetComponent<Button>().onClick.AddListener(() => store.SetActive(false));
		store.GetComponent<Store>().OpenStore("toogleOthers");
	}
}
