﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Gem : MonoBehaviour {

    TextMeshProUGUI txtGem;

    Button button;

    Scene scene;

    // Use this for initialization
    void Start () {
        txtGem = transform.GetChild(0).GetComponent<TextMeshProUGUI>();

        button = GetComponent<Button>();
        button.onClick.AddListener(Click);

        CurrentGem();
    }	

    void LateUpdate()
    {
        CurrentGem();
    }

    #region METHODS
    public void CurrentGem()
    {
        if(DataController.Instance.playerData.gem >= 10000 && DataController.Instance.playerData.gem <= 999999)
        {
            float gem = DataController.Instance.playerData.gem / 1000;
            Mathf.Floor(gem);
//            txtGem.text = gem.ToString("###.##K");
            txtGem.text = gem + "K";
        }
        else if(DataController.Instance.playerData.gem >= 1000000)
        {
            float gem = DataController.Instance.playerData.gem / 1000000;
            Mathf.Floor(gem);
//            txtGem.text = gem.ToString("###.##M");
            txtGem.text = gem + "M";
        }
        else
        {            
            txtGem.text = DataController.Instance.playerData.gem.ToString("#,##0");
        }  
    }

    void Click()
    {
        if(scene.name == "Main")
        {
            GameObject storePanel = Instantiate(Resources.Load<GameObject>("Prefab/StorePanel"), FindObjectOfType<Canvas>().transform, false);
            //storePanel.transform.Find("btnBack").GetComponent<Button>().onClick.AddListener(() => Destroy(storePanel));
            storePanel.GetComponent<Store>().OpenStore("toogleOthers");
        }
        else
        {
            FindObjectOfType<PetUIController>().Store("toggleOthers");
        }
    }
    #endregion
}
