﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SingleStorePanel : MonoBehaviour {

    [SerializeField]
    GameObject[] item; 

    [SerializeField]
    Button btnPrev, btnNext;

    int currentIndex = 0;

	// Use this for initialization
	void Start () {
        Initialize();
	}
	
    #region METHODS

    void Initialize()
    {
        if (item.Length > 1)
        {
            btnPrev.onClick.AddListener(Prev);
            btnNext.onClick.AddListener(Next);
        }
        else
        {
            btnPrev.gameObject.SetActive(false);
            btnNext.gameObject.SetActive(false);
        }
    }

    void Prev()
    {
        if(currentIndex == 0)
        {
			item[currentIndex].SetActive(false);
            currentIndex = item.Length - 1;
            item[currentIndex].SetActive(true);           
        }
        else
        {
            item[currentIndex].SetActive(false);
            currentIndex--;
           	item[currentIndex].SetActive(true);
        }
    }

    void Next()
    {
        if(currentIndex == item.Length - 1)
        {
            item[currentIndex].SetActive(false);
            currentIndex = 0;
            item[currentIndex].SetActive(true);
        }
        else
        {
            item[currentIndex].SetActive(false);
            currentIndex++;
            item[currentIndex].SetActive(true);
        }
    }
    #endregion
}
