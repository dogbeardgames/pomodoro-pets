﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;


public class TutorialController : MonoBehaviour {

	public static TutorialController instance;

	void Start()
	{
		instance = this;
	}

	// Use this for initialization
	public void LoadAdoptPet () {

		//Debug.Log ("<color=red>" + "Scene to load: pet adopt"+ "</color>");

		DOTween.Init();

		//             initial screen
		if (DataController.Instance.playerData.playerPetList.Count == 0) {
			// call pet adopt scene
			SceneController.Instance.LoadingScene ("PetAdopt");

		} 
	}

    #region METHODS
    // sfx for swipe
    public void SwipeSFX()
    {
        Framework.SoundManager.Instance.PlaySFX("seItemCarouselShuffle");
    }
    // sfx for button press
    public void ButtonSFX()
    {
        Framework.SoundManager.Instance.PlaySFX("seIconTapPets");
    }

    #endregion
}
