﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

namespace Framework
{   
	public class SoundManager : MonoBehaviour
    {
		private static SoundManager s_instance = null;
		public static SoundManager Instance
		{
			get { return s_instance; }
		}            

        void Start()
        {
            Init();
			//DontDestroyOnLoad(gameObject);
        }

        [SerializeField] AudioClip[] sfxClip, bgmClip;
        [SerializeField] AudioSource bgmInside, bgmOutside, sfx;
        Dictionary<string, AudioClip> dictSFX = new Dictionary<string, AudioClip>();
        Dictionary<string, AudioClip> dictBGM = new Dictionary<string, AudioClip>();        	


		public AudioSource BGMInside{
			get{ return bgmInside; }
		}
		public AudioSource BGMOutside{
			get{ return bgmOutside; }
		}

		public AudioSource SFX
		{
			get{ return sfx;}
		}
		void Update()
		{
			//print ("BGM PLAYER is playing? " + bgmInside.isPlaying);
		}

        // Use this for initialization
        private void Init()
        {			
			if(s_instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                s_instance = this;
                DontDestroyOnLoad(gameObject);
            }            
            
			if (sfxClip != null) 
			{
				foreach (AudioClip sfx in sfxClip)// sfx dictionary
				{
					dictSFX.Add(sfx.name.ToLower(), sfx);
				}
			}

			if (bgmClip != null) 
			{
				foreach (AudioClip bgm in bgmClip)// bgm dictionary
				{
					dictBGM.Add(bgm.name.ToLower(), bgm);
				}
			}
        }

        #region METHODS
        public void MuteInsideBGM()
        {
            bgmInside.mute = true;
        }

        public void MuteOutsideBGM()
        {            
            bgmOutside.mute = true;
        }

        public void FadeOutOutside()
        {
            bgmOutside.DOFade(0, 2);
        }      

		public void FadeOutInside()
		{
			bgmInside.DOFade (0, 2);

		}

        public void FadeInOutside()
        {
			bgmOutside.DOFade(DataController.Instance.settings.volume, 2);
        }

		public void FadeInInside()
		{
			bgmInside.DOFade(DataController.Instance.settings.volume, 2);
		}

        public void UnmuteInsideBGM()
        {
            bgmInside.mute = false;
        }

        public void UnmuteOutsideBGM()
        {            
            bgmOutside.mute = false;
        }

        public void MuteSFX()
        {
            sfx.mute = true;
        }

        public void UnmuteSFX()
        {
            sfx.mute = false;
        }

        /// <summary>
        /// set bgm volume, min = 0, max = 1
        /// </summary>
        /// <param name="volume"></param>
        public void BGMVolume(float volume)
        {
            bgmInside.volume = volume;
            bgmOutside.volume = volume;
        }

        /// <summary>
        /// set sfx volume, min = 0, max = 1
        /// </summary>
        /// <param name="volume"></param>
        public void SFXVolume(float volume)
        {
            sfx.volume = volume;
        }

        public void PlaySFX(string name)
        {
            AudioClip clip;
            sfx.PlayOneShot(dictSFX[name.ToLower()]);
            dictSFX.TryGetValue(name.ToLower(), out clip);
            sfx.PlayOneShot(clip);            
        }

		public void PlaySFX(string name, bool loop)
		{
			AudioClip clip;			
			dictSFX.TryGetValue(name.ToLower(), out clip);
            sfx.clip = clip;
            sfx.Play();
			sfx.loop = loop;
		}

		public void SetSFXAndPlay(string name, bool loop)
		{
			AudioClip clip;			
			dictSFX.TryGetValue(name.ToLower(), out clip);
            sfx.clip = clip;
			//sfx.PlayOneShot(clip);          
			sfx.Play();
			sfx.loop = loop;
		}

        public void StopSFX()
        {         
            sfx.DOFade(0, 0.5f).OnComplete(() =>
                {
                    sfx.Stop();
                    sfx.volume = DataController.Instance.settings.volume;
                });            
        }

        public void PlayBGM(string name, bool isLooping)
        {
            //Debug.Log("<color=red>Inside Sound Manager!!!</color>");
            AudioClip clip;
            dictBGM.TryGetValue(name.ToLower(), out clip);
            bgmInside.clip = clip;
            bgmInside.loop = isLooping;
            bgmInside.Play();
        }

        public void PlayOutsideBGM(string name, bool isLooping)
        {
            AudioClip clip;
            dictBGM.TryGetValue(name.ToLower(), out clip);
            bgmOutside.clip = clip;
            bgmOutside.loop = isLooping;
            bgmOutside.Play();
        }

        public bool isBGMPlaying(string clipName)
        {            
            if(bgmInside.clip == null)
            {
                return false;
            }
            else if(bgmInside.clip.name.ToLower() == clipName.ToLower())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
    }
}
