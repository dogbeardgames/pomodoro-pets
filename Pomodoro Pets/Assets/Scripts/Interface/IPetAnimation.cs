﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPetAnimation {

    void AnimatePlayTime();   
    void AnimateEating();
    void AnimateDrinking();
    void AnimateBathing();
}
