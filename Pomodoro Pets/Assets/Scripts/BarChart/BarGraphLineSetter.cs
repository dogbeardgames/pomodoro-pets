﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarGraphLineSetter : MonoBehaviour {

	[SerializeField]
	GameObject[] lines;

	void Start () {
		Set ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void Set()
	{
		RectTransform rect = GetComponent<RectTransform> ();
		float heightPerLine = rect.rect.height/10f;
		//starting position
		float inc = -(rect.rect.height/2);
		for(int i=0; i<lines.Length; i++)
		{
			lines [i].transform.localPosition = new Vector2 (lines [i].transform.localPosition.x, inc);
			inc += heightPerLine;
		}
	}
}
