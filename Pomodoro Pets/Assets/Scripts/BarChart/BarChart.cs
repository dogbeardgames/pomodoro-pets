﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;


public class BarChart : MonoBehaviour {

	public Bar barPrefab;

	public List<TaskItem> TaskItem;
	List<Bar> bars = new List<Bar> ();

	[SerializeField]
	float chartHeights;

	[SerializeField]
	RectTransform barHieght;

	void Start()
	{
		Invoke ("TaskAllTime",0.01f);
		chartHeights = barHieght.rect.height;
		getTotalMaxValueByWeeklyTask ();

	}

	/// <summary>
	/// Displays the graph for category.
	/// </summary>
	public void DisplayGraphForCategory()
	{
		//Debug.Log ("<color=red>" + "Hello" +  StatController.instance.listForReports.Count + "</color>");
		int max = 0;
			
		try 
		{
			max = StatController.instance.listForReports.Max (t => t.task_count);
		} 
		catch (Exception ex) 
		{

		}
		//Debug.Log (max);

		float lineValue = 0;
		GameObject.Find ("Bar Chart").GetComponent<HorizontalLayoutGroup> ().spacing = 30;
		for (int i = 0; i < StatController.instance.listForReports.Count; i++) 
		{
			
			//Bar categoryBar = Instantiate (barPrefab) as Bar;
			//categoryBar.transform.SetParent (transform);
			Bar categoryBar = Instantiate ((barPrefab) as Bar, transform, false);

			RectTransform rt = categoryBar.bar.GetComponent<RectTransform> ();

			// get bar value
			lineValue = (float)StatController.instance.listForReports [i].task_count;

			float divider = generateFlexibleNumber (max);


			rt.sizeDelta = new Vector2 (rt.sizeDelta.x, lineValue * (chartHeights/divider));

			if (StatController.instance.listForReports[i].category.Length < i) 
			{
				categoryBar.label.text = "Undefined";
			} 

			else 
			{
				categoryBar.label.text = StatController.instance.listForReports[i].category;
			}
				
			categoryBar.barValue.text = StatController.instance.listForReports [i].task_count.ToString ();

			//if too small
			if (rt.sizeDelta.y < 30f) 
			{
				categoryBar.barValue.GetComponent<RectTransform> ().pivot = new Vector2 (0.5f, 0.2f);
				categoryBar.barValue.GetComponent<RectTransform> ().anchoredPosition = Vector2.zero;
			}
		}

		//generate line number
		try {
			LineNumberManager.instance.CategoryGraphLineGenerator (max);
		} catch (Exception ex) {
			LineNumberManager.instance.CategoryGraphLineGenerator (1);
		}
	}
	 
	/// <summary>
	/// Weekly Bar Graph.
	/// </summary>
	/// <param name="days">Days.</param>
	#region weekly
	List<TaskItem> weeklyStats  = new List<TaskItem>();
	public void Weekly()
	{
		float duration =0f;
		float taskcount=0f;
		float sessionCount=0f;
		float totalaverage=0f;
		float total=0f;

		for (int i = 0; i < 7; i++) 
		{
			for (int x = 0; x < TaskDataController.Instance.completedTaskData.taskItem.Count; x++)
			{
				//Debug.LogError (DateTime.Today.AddDays (-i).Date);

				if (TaskDataController.Instance.completedTaskData.taskItem [x].dateFinished.Date == DateTime.Today.AddDays (-i).Date) 
				{
					TaskItem taskitem = new TaskItem();
					//Debug.Log (TaskDataController.Instance.completedTaskData.taskItem [x].dateFinished);
					taskitem.task = TaskDataController.Instance.completedTaskData.taskItem [x].task;
					taskitem.dateFinished = TaskDataController.Instance.completedTaskData.taskItem [x].dateFinished;
					taskitem.session = TaskDataController.Instance.completedTaskData.taskItem [x].session;
					taskitem.finishDuration = TaskDataController.Instance.completedTaskData.taskItem [x].finishDuration;

					weeklyStats.Add (taskitem);
				}
			}
		}

		var results = weeklyStats
			.GroupBy(x=>x.dateFinished.DayOfWeek)
			.Select(x => new {
				//Month = new DateTime(1,x.Key,x.Key).ToString("MMMM"),
				Day = x.FirstOrDefault().dateFinished.DayOfWeek,

				SessionCount = x.GroupBy(y => y.session).Count(),
				TaskCount = x.Count(),
				session = x.Select(y=>y.session),
				duration = x.Sum(y=>y.finishDuration)
			}).ToList();

		for (int i = 0; i < results.Count; i++) 
		{
			Debug.Log ("Date:" + results[i].Day + "task:" + results[i].TaskCount);

		}

		int max = 0;
		try 
		{
			if(results.Count == 0)
			{
				max = 0;
			}
			else
			{
				max = results.Max (t => t.TaskCount);
			}
		} catch (Exception ex) 
		{

		}

		for (int i = 0; i < results.Count; i++) 
		{
			duration = results[i].duration;
			taskcount = results[i].TaskCount;
			sessionCount = results[i].SessionCount;
			totalaverage = duration/results[i].TaskCount;
			total = totalaverage / results [i].SessionCount;
		

			Bar categoryBar = Instantiate ((barPrefab) as Bar, transform, false);
			RectTransform rt = categoryBar.bar.GetComponent<RectTransform> ();


			float divider = generateFlexibleNumber (getTotalMaxValueByWeeklyTask());

			rt.sizeDelta = new Vector2 (rt.sizeDelta.x, total * (chartHeights/divider));


			categoryBar.label.text = results[i].Day.ToString();

			//newBar.barValue.text = vals [i].ToString ();
			//categoryBar.barValue.text = ((float)(results[i].TaskCount/(float)results[i].SessionCount)).ToString ();
			categoryBar.barValue.text = total.ToString ("0.00");

			//if too small
			if (rt.sizeDelta.y < 30f) {
				categoryBar.barValue.GetComponent<RectTransform> ().pivot = new Vector2 (0.5f, 0.2f);
				categoryBar.barValue.GetComponent<RectTransform> ().anchoredPosition = Vector2.zero;
			}

		}
			
		try {
			
			LineNumberManager.instance.GraphLineGenerator (getTotalMaxValueByWeeklyTask());
		} catch (Exception ex) {
			LineNumberManager.instance.GraphLineGenerator (1);
		}

	}

	private float getTotalMaxValueByTask()
	{
		List<float> maxTotal = new List<float> ();
		float duration =0f;
		float taskcount=0f;
		float sessionCount=0f;
		float totalaverage=0f;
		float total=0f;
		
		var results = TaskDataController.Instance.completedTaskData.taskItem
			//.Where(y=>y.date_sold.Year==DateTime.Now.Year)
			.GroupBy(x=>x.dateFinished.Month)
			.Select(x => new {
				//month = x.Key,
				Month = new DateTime(1,x.Key,x.Key).ToString("MMMM"),
				year = x.FirstOrDefault().dateFinished.Year,
				SessionCount = x.GroupBy(y => y.session).Count(),
				session = x.Select(y=>y.session),
				duration = x.Sum(y=>y.finishDuration),
				TaskCount = x.Count()
			}).ToList();
				
		for (int i = 0; i < results.Count; i++) 
		{
			duration = results[i].duration;
			taskcount = results[i].TaskCount;
			sessionCount = results[i].SessionCount;
			totalaverage = duration/results[i].TaskCount;
			total = totalaverage / results [i].SessionCount;

			maxTotal.Add (total);
		}

		//Debug.LogError (maxTotal [0]);
		//Debug.LogError (maxTotal [1]);
		//Debug.LogError (maxTotal.Max ());

		return maxTotal.Max ();
	}

	private float getTotalMaxValueByWeeklyTask()
	{
        try
        {
		List<TaskItem> weeklyStats  = new List<TaskItem>();
		List<float> maxTotalWeekly = new List<float> ();
		float duration =0f;
		float taskcount=0f;
		float sessionCount=0f;
		float totalaverage=0f;
		float total=0f;

		for (int i = 0; i < 7; i++) 
		{
			for (int x = 0; x < TaskDataController.Instance.completedTaskData.taskItem.Count; x++)
			{
				//Debug.LogError (DateTime.Today.AddDays (-i).Date);
				if (TaskDataController.Instance.completedTaskData.taskItem [x].dateFinished.Date == DateTime.Today.AddDays (-i).Date) 
				{
					TaskItem taskitem = new TaskItem();
					//Debug.Log (TaskDataController.Instance.completedTaskData.taskItem [x].dateFinished);
					taskitem.task = TaskDataController.Instance.completedTaskData.taskItem [x].task;
					taskitem.dateFinished = TaskDataController.Instance.completedTaskData.taskItem [x].dateFinished;
					taskitem.session = TaskDataController.Instance.completedTaskData.taskItem [x].session;
					taskitem.finishDuration = TaskDataController.Instance.completedTaskData.taskItem [x].finishDuration;

					weeklyStats.Add (taskitem);
				}
			}
		}

		var results = weeklyStats
			.GroupBy(x=>x.dateFinished.DayOfWeek)
			.Select(x => new {
				//Month = new DateTime(1,x.Key,x.Key).ToString("MMMM"),
				Day = x.FirstOrDefault().dateFinished.DayOfWeek,

				SessionCount = x.GroupBy(y => y.session).Count(),
				TaskCount = x.Count(),
				session = x.Select(y=>y.session),
				duration = x.Sum(y=>y.finishDuration)
			}).ToList();
	
		for (int i = 0; i < results.Count; i++) 
		{
			duration = results[i].duration;
			taskcount = results[i].TaskCount;
			sessionCount = results[i].SessionCount;
			totalaverage = duration/results[i].TaskCount;
			total = totalaverage / results [i].SessionCount;

			maxTotalWeekly.Add (total);

		}			
		return maxTotalWeekly.Max ();
        }
        catch(InvalidOperationException e)
        {
            return 0f;
        }
	}
	#endregion

	#region week

	List<WeekLyGraph> weeklychartList  = new List<WeekLyGraph>();
	public void Week()
	{
		weeklychartList.Clear ();
		float duration =0f;
		float taskcount=0f;
		float sessionCount=0f;
		float totalaverage=0f;
		float total=0f;

		bool isMondayAvailable = false;
		bool isTuesdayAvailable = false;
		bool isWednesdayAvailable = false;
		bool isThursdayAvailable = false;
		bool isFridayAvailable = false;
		bool isSaturdayAvailable = false;
		bool isSundayAvailable = false;

		var results = TaskDataController.Instance.completedTaskData.taskItem.Where(x=>x.dateFinished > DateTime.Now.AddDays(-7))
			.GroupBy(x=>x.dateFinished.DayOfWeek)
			.Select(x => new {
				//Month = new DateTime(1,x.Key,x.Key).ToString("MMMM"),
				Day = x.FirstOrDefault().dateFinished.DayOfWeek,
				Date = x.FirstOrDefault().dateFinished.Day,
				SessionCount = x.GroupBy(y => y.session).Count(),
				TaskCount = x.Count(),
				session = x.FirstOrDefault().session,
				duration = x.Sum(y=>y.finishDuration)
			}).ToList();

		int maxDates = 0;
		try {
			 maxDates = results.Max (x => x.Date);
		} catch (Exception ex) {
			 maxDates = 0;
		}
			
		List<int> existingNumbers = new List<int> ();
		List<int> notExistingDate = new List<int> ();

		for (int i = 0; i < results.Count; i++)
		{
			existingNumbers.Add (results [i].Date);	
		}
			
		int minimumDate = maxDates - 6;
		var notExist = Enumerable.Range (minimumDate, maxDates).Except (existingNumbers);

		// Add not existing to new List

		foreach (var dateItem in notExist)
		{
			if (dateItem < maxDates && dateItem >= minimumDate) 
			{
				Debug.Log ("<color=blue>" +"Not existing date is: " + dateItem +"</color>");
				WeekLyGraph weekGraph = new WeekLyGraph();
				weekGraph.DateofDay = dateItem;
				weekGraph.SessionCount  = 0;
				weekGraph.TaskCount 	= 0;
				weekGraph.session 		= 0;
				weekGraph.duration 		= 0;
				weeklychartList.Add (weekGraph);
			}
		}
		#region comment
		for (int i = 0; i < results.Count; i++) 
		{	
			WeekLyGraph wg = new WeekLyGraph();
			wg.day = results [i].Day;
			wg.DateofDay = results [i].Date;
			wg.SessionCount = results [i].SessionCount;
			wg.TaskCount = results [i].TaskCount;
			wg.session = results [i].session;
			wg.duration = results [i].duration;
			weeklychartList.Add (wg);
		}
		#endregion

		//var ordered = weeklychartList.OrderBy (x => ((int)x.day + 7) % 7).ToList ();
		var ordered = weeklychartList.OrderBy(x=>x.DateofDay).ToList();
		int maxDate = 0;
		try {
			 maxDate = weeklychartList.Max (x => x.DateofDay) -6;
		} catch (Exception ex) {
			 maxDate = 0;
		}

		//display graph for week

		GameObject.Find ("Bar Chart").GetComponent<HorizontalLayoutGroup> ().spacing = 3;

		for (int i = 0; i < ordered.Count; i++)
		{
			//Debug.Log ("<color=blue>" + ordered[i].DateofDay + "</color>");

			duration = ordered[i].duration;
			taskcount = ordered[i].TaskCount;
			sessionCount = ordered[i].SessionCount;
			totalaverage = duration/ordered[i].TaskCount;
			total = totalaverage / ordered [i].SessionCount;

			if (float.IsNaN(totalaverage))
			{
				totalaverage = 0;
			}

			if (float.IsNaN(total)) 
			{
				total = 0;
			}



			Bar categoryBar = Instantiate ((barPrefab) as Bar, transform, false);



			RectTransform rt = categoryBar.bar.GetComponent<RectTransform> ();
		
			float divider = generateFlexibleNumber (getTotalMaxValueByWeeklyTask());
			rt.sizeDelta = new Vector2 (rt.sizeDelta.x, total * (chartHeights/divider));

			if (maxDate > 0) {
				//categoryBar.label.text = (maxDate + i).ToString ();
				categoryBar.label.text = ordered[i].DateofDay.ToString();
			} 
			else 
			{
				categoryBar.label.text = "";
			}

			//maxDate = maxDate - 1;

			categoryBar.barValue.text = total.ToString ("0.00");

			//if too small
			if (rt.sizeDelta.y < 30f) 
			{
				categoryBar.barValue.GetComponent<RectTransform> ().pivot = new Vector2 (0.5f, 0.2f);
				categoryBar.barValue.GetComponent<RectTransform> ().anchoredPosition = Vector2.zero;
			}
			//maxDate =- 1;
		}

		//generate line number
		try {
			LineNumberManager.instance.GraphLineGenerator (getTotalMaxValueByWeeklyTask());
		} catch (Exception ex) {
			LineNumberManager.instance.GraphLineGenerator (1);
		}
	}
		
	#endregion
	/// <summary>
	/// Stats Last threemonth.
	/// </summary>

	public void lastThreemonth()
	{
		float duration =0f;
		float taskcount=0f;
		float sessionCount=0f;
		float totalaverage=0f;
		float total=0f;

		float[] barval = new float[]{ };

		try {
			var results = TaskDataController.Instance.completedTaskData.taskItem
				.GroupBy(x=>x.dateFinished.Month)
				.Select(x => new {
					Month = new DateTime(1,x.Key,x.Key).ToString("MMMM"),
					year = x.FirstOrDefault().dateFinished.Year,
					SessionCount = x.GroupBy(y => y.session).Count(),
					TaskCount = x.Count(),
					session = x.Select(y=>y.session),
					duration = x.Sum(y=>y.finishDuration)
				}).ToList();

			int max=0;
			try 
			{
				max = results.Max (t => t.TaskCount);
			} 
			catch (Exception ex)
			{
				
			}
	
		//Debug.Log (results.Count);
		GameObject.Find ("Bar Chart").GetComponent<HorizontalLayoutGroup> ().spacing = 30;
		for (int i = 0; i < 3; i++) 
			{

			duration = results[i].duration;
			taskcount = results[i].TaskCount;
			sessionCount = results[i].SessionCount;
			totalaverage = duration/results[i].TaskCount;
			total = totalaverage / results [i].SessionCount;
			barval = new float[]{total};
					
			//Bar categoryBar = Instantiate (barPrefab) as Bar;
			//categoryBar.transform.SetParent (transform);
			Bar categoryBar = Instantiate ((barPrefab) as Bar, transform, false);
			
			RectTransform rt = categoryBar.bar.GetComponent<RectTransform> ();
//			float normalizedValue = barval.Max()/max * 0.05f;
//			float line = barval.Max ();
//
//			//hieght of bar
//			float minimumLine =  LineNumberManager.instance.GenerateLineNumber(line)/10 ;
//
//			if(barval.Max() < minimumLine)
//			{
//				rt.sizeDelta = new Vector2 (rt.sizeDelta.x, total * (chartHeights/FindHighestLineNumber(barval.Max())) );
//			}
//			else
//			{
//				rt.sizeDelta = new Vector2 (rt.sizeDelta.x, (chartHeights * total)/FindHighestLineNumber(barval.Max()));
//			}
			
			//Bar Hieght
				float divider = generateFlexibleNumber (getTotalMaxValueByTask());
			rt.sizeDelta = new Vector2 (rt.sizeDelta.x, total * (chartHeights/divider));

			categoryBar.label.text = results[i].Month;
			//newBar.barValue.text = vals [i].ToString ();
			//categoryBar.barValue.text = ((float)(results[i].TaskCount/(float)results[i].SessionCount)).ToString ();
			categoryBar.barValue.text = total.ToString ("0.00");

			//if too small
			if (rt.sizeDelta.y < 30f) 
			{
			categoryBar.barValue.GetComponent<RectTransform> ().pivot = new Vector2 (0.5f, 0.2f);
			categoryBar.barValue.GetComponent<RectTransform> ().anchoredPosition = Vector2.zero;
			}

			}
			} 
			catch (ArgumentOutOfRangeException ex) 
			{

			}

		//generate line number
		try {
			LineNumberManager.instance.GraphLineGenerator (getTotalMaxValueByTask());
		} catch (Exception ex) {
			LineNumberManager.instance.GraphLineGenerator (1);
		}
	}

	/// <summary>
	/// Tasks all time.
	/// </summary>
	public void TaskAllTime()
	{
		float duration =0f;
		float taskcount=0f;
		float sessionCount=0f;
		float totalaverage=0f;
		float total=0f;

		float[] barval = new float[]{ };

		var results = TaskDataController.Instance.completedTaskData.taskItem
					//.Where(y=>y.date_sold.Year==DateTime.Now.Year)
						.GroupBy(x=>x.dateFinished.Month)
		                .Select(x => new {
		                     //month = x.Key,
						Month = new DateTime(1,x.Key,x.Key).ToString("MMMM"),
						year = x.FirstOrDefault().dateFinished.Year,
						SessionCount = x.GroupBy(y => y.session).Count(),
						session = x.Select(y=>y.session),
						duration = x.Sum(y=>y.finishDuration),
						TaskCount = x.Count()
		                }).ToList();
					
		int max = 0;
		try 
		{
			if(results.Count == 0)
			{
				max = 0;
			}
			else
			{
			max = results.Max (t => t.TaskCount);
			}
		} catch (Exception ex) 
		{

		}
		//Debug.Log (results.Count);
		GameObject.Find ("Bar Chart").GetComponent<HorizontalLayoutGroup> ().spacing = 30;
		for (int i = 0; i < results.Count; i++) 
		{

			//Debug.Log ("Month" + results [i].year);

			duration = results[i].duration;
			taskcount = results[i].TaskCount;
			sessionCount = results[i].SessionCount;
			totalaverage = duration/results[i].TaskCount;
			total = totalaverage / results [i].SessionCount;
			barval = new float[]{total};


			//Bar categoryBar = Instantiate (barPrefab) as Bar;
			//categoryBar.transform.SetParent (transform);
			Bar categoryBar = Instantiate ((barPrefab) as Bar, transform, false);
			RectTransform rt = categoryBar.bar.GetComponent<RectTransform> ();

//			float normalizedValue = barval.Max()/max * 0.05f;
//			float line = barval.Max ();
//
//			//hieght of bar
//
//			if(barval.Max() < minimumLine)
//			{
//				rt.sizeDelta = new Vector2 (rt.sizeDelta.x, total * (chartHeights/ FindHighestLineNumber(barval.Max())) );
//			}
//			else
//			{
//				rt.sizeDelta = new Vector2 (rt.sizeDelta.x, (chartHeights * total)/FindHighestLineNumber(barval.Max()));
//			}
//			Bar categoryBar = Instantiate (barPrefab) as Bar;
//			categoryBar.transform.SetParent (transform);

			// get bar value
			//Debug.LogError("t: " + getTotalMaxValueByTask());

			float divider = generateFlexibleNumber (getTotalMaxValueByTask());

			rt.sizeDelta = new Vector2 (rt.sizeDelta.x, total * (chartHeights/divider));

		
			categoryBar.label.text = results[i].Month;

			//newBar.barValue.text = vals [i].ToString ();
			//categoryBar.barValue.text = ((float)(results[i].TaskCount/(float)results[i].SessionCount)).ToString ();
			categoryBar.barValue.text = total.ToString ("0.00");

			//if too small
			if (rt.sizeDelta.y < 30f) {
				categoryBar.barValue.GetComponent<RectTransform> ().pivot = new Vector2 (0.5f, 0.2f);
				categoryBar.barValue.GetComponent<RectTransform> ().anchoredPosition = Vector2.zero;
			}

		}

		//generate line number
		try {
			//Debug.Log("trying GraphLineGenerator");
			LineNumberManager.instance.GraphLineGenerator (getTotalMaxValueByTask());
		} catch (Exception ex) {
			LineNumberManager.instance.GraphLineGenerator (1);
		}

	}

	/// <summary>
	/// Generates the flexible number.
	/// </summary>
	/// <returns>The flexible number.</returns>
	/// <param name="number">Number.</param>
	float generateFlexibleNumber(float number)
	{
		float highestNumber = 0;

		if (number.ToString ().Length == 1 || number < 11) {
			highestNumber = 10;
		} 
		else 
		{
			float lastNumber = number % 10;

			if (lastNumber > 0) {
				highestNumber = (number + 10) - lastNumber;
			} 
			else 
			{
				highestNumber = number;
			}
		}
		return highestNumber;

		Debug.Log ("Highest Number is: " + highestNumber);
	}
}

public class WeekLyGraph
{
	public DayOfWeek day;
	public int DateofDay;
	public int SessionCount;
	public int TaskCount;
	public uint session;
	public float duration;
}




