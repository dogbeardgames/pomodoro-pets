﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragCorrector : MonoBehaviour 
{
    public int baseThreshold = 6;
    public int basePPI = 210;
    public int dragThreshold = 0;


	// Use this for initialization
	void Start () 
    {
        dragThreshold = baseThreshold * (int)Screen.dpi / basePPI;

        EventSystem eventSystems = GetComponent<EventSystem>();

        if(eventSystems)
        {
            eventSystems.pixelDragThreshold = dragThreshold;
        }
	}		
}
