﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExitPopUp : MonoBehaviour {


	[SerializeField]
	GameObject PanelExit;


	bool enable = true;
	void Update () {
		if (Input.GetKeyUp(KeyCode.Escape)) 
        {
			//Debug.Log (enable);

			if (enable == true) {
				#if UNITY_ANDROID
				//EditorApplication.isPlaying = false;
				GameObject panelexit = Instantiate (PanelExit, GameObject.Find ("Canvas").transform, false);
				panelexit.transform.Find ("btnNo").GetComponent<Button> ().onClick.AddListener (() => DestroyPanelExit ());		
				enable = false;
				#else
				Instantiate (PanelExit, GameObject.Find ("Canvas").transform, false);
				#endif
			}
		}        
	}
	void DestroyPanelExit()
	{
		enable = true;
	}
}
