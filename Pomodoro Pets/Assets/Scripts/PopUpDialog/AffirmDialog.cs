﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AffirmDialog : MonoBehaviour {

    [SerializeField]
    Button btnYes;
    [SerializeField]
    TextMeshProUGUI txtCaption, txtYes;

    // Use this for initialization
    void Start () 
    {
        btnYes.onClick.AddListener(Yes);
    }

    #region PUBLIC METHODS
    /// <summary>
    /// Sets the caption.
    /// </summary>
    public void SetCaption(string caption)
    {
        txtCaption.text = caption;
        //        transform.Find("txtCaption").GetComponent<TextMeshProUGUI>().text = caption;
    }
    /// <summary>
    /// Sets the confirm caption.
    /// </summary>
    public void SetConfirmCaption(string caption)
    {
        txtYes.text = caption;
        //        transform.Find("btnYes").transform.Find("Text").GetComponent<TextMeshProUGUI>().text = caption;
    }
    #endregion

    #region PRIVATE METHODS
    void Yes()
    {
        if (OnYesEvent != null)
            OnYesEvent();        
    }
    #endregion

    #region Events
    public delegate void PopUp();
    public event PopUp OnYesEvent;
    #endregion
}
