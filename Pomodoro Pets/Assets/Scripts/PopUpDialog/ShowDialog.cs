﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ShowDialog : MonoBehaviour {

    private string m_caption;
    private string m_confirm, m_cancel;

	[SerializeField]
    Button btnYes, btnNo;
	[SerializeField]
    TextMeshProUGUI txtCaption;
    // Use this for initialization
    void Start () {

        //btnYes = transform.Find("btnYes").GetComponent<Button>();

        btnYes.onClick.AddListener(Yes);
		btnNo.onClick.AddListener(No);
    }

    #region PUBLIC METHODS
    /// <summary>
    /// Sets the caption.
    /// </summary>
    public void SetCaption(string caption)
    {
		txtCaption.text = caption;
    }
    /// <summary>
    /// Sets the confirm caption.
    /// </summary>
    public void SetConfirmCaption(string caption)
    {
		btnYes.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = caption;
    }

	public void SetCancelCaption(string caption)
	{
		btnNo.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = caption;
	}

	public void DisableNO()
	{
		btnNo.gameObject.SetActive (false);
	}
    #endregion

    #region PRIVATE METHODS
    void Yes()
    {
        if (OnYesEvent != null)
            OnYesEvent();        
    }       

	void No()
	{
		if (OnNoEvent != null)
			OnNoEvent();
	}       
    #endregion

    #region Events
    public delegate void PopUp();
    public event PopUp OnYesEvent;
	public event PopUp OnNoEvent;
    #endregion
}
