﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ConfirmDialog : MonoBehaviour {

    private string m_caption;
    private string m_confirm, m_cancel;

    [SerializeField]
    Button btnYes, btnNo;
    [SerializeField]
    TextMeshProUGUI txtCaption, txtYes, txtNo;

	// Use this for initialization
	void Start () {

//        btnYes = transform.Find("btnYes").GetComponent<Button>();
//        btnNo = transform.Find("btnNo").GetComponent<Button>();

        btnYes.onClick.AddListener(Yes);
        btnNo.onClick.AddListener(No);
	}

    #region PUBLIC METHODS
    /// <summary>
    /// Sets the caption.
    /// </summary>
    public void SetCaption(string caption)
    {
        txtCaption.text = caption;
//        transform.Find("txtCaption").GetComponent<TextMeshProUGUI>().text = caption;
    }
    /// <summary>
    /// Sets the confirm caption.
    /// </summary>
    public void SetConfirmCaption(string caption)
    {
        txtYes.text = caption;
//        transform.Find("btnYes").transform.Find("Text").GetComponent<TextMeshProUGUI>().text = caption;
    }
    /// <summary>
    /// Sets the cancel caption.
    /// </summary>
    /// <param name="caption">Caption.</param>
    public void SetCancelCaption(string caption)
    {
        txtNo.text = caption;
//        transform.Find("btnNo").transform.Find("Text").GetComponent<TextMeshProUGUI>().text = caption;
    }
    #endregion

    #region PRIVATE METHODS
    void Yes()
    {
        if (OnYesEvent != null)
            OnYesEvent();        
    }

    void No()
    {
        if (OnNoEvent != null)
            OnNoEvent();
    }       
    #endregion

    #region Events
    public delegate void PopUp();
    public event PopUp OnYesEvent;
    public event PopUp OnNoEvent;
    #endregion
}
