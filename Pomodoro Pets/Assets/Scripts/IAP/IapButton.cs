﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IapButton : MonoBehaviour {

	string buyText = "nothing";

	void Start()
	{
		ChangeLabel ();
	}

	public void TenTreats()
	{		
		buyText = "10 treats bought";
		ChangeLabel ();
	}

	public void TwentyTreats()
	{		
		buyText = "20 treats bought";
		ChangeLabel ();
	}

	public void Corgi()
	{		
		buyText = "corgi bought";
		ChangeLabel ();
	}

	public void BuyFailed()
	{		
		buyText = "failed to buy";
		ChangeLabel ();
	}

	void ChangeLabel()
	{
		GameObject.Find ("Label").GetComponent<Text> ().text = buyText;
	}
}
