﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonSFX : MonoBehaviour {

    Button button;

	// Use this for initialization
	void Start () {
        button = GetComponent<Button>();
        button.onClick.AddListener(ButtonClick);
	}
	
	#region METHODS

    void ButtonClick()
    {
        PetsAudio.Instance.ButtonSFX();
    }

    #endregion
}
