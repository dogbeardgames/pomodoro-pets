﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeVoulume : MonoBehaviour {


	//Audio source: what music will be tested
	public AudioSource music;

	// Update is called once per frame
	void Update () {

		//change music volume every update
		music.volume=DataController.Instance.settings.volume;

		//Debug.Log ("changed volume to " + DataController.Instance.settings.volume );
	}
}
