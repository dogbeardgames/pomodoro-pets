﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class changeNumber : MonoBehaviour {

	[SerializeField]
	TMP_InputField  inputVolume;

	[SerializeField]
	Slider sldrVolume;

	// Use this for initialization
	void Start () {
		
	}
	
	public void convertNumber()
	{
		double firstNumber = Math.Round ((double)sldrVolume.value, 1) * 100;

		////Debug.Log (firstNumber);
		inputVolume.text = firstNumber.ToString ();
	}
}
