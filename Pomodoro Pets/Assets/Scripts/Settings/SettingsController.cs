﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Linq;
using TMPro;
using Framework;

public class SettingsController : MonoBehaviour {

	public Dictionary<string,List<string>> enumDictionary = new Dictionary<string,List<string>> ();
	//List<string> enumlist = new List<string> ();


    [SerializeField]
    TMP_InputField  inputSessionDuration, 
                    inputShortBreakDuration, 
                    inputLongBreakDuration,
                    inputSessionCount,
                    inputAlarmTime,
                    inputVolume;
                

    [SerializeField]
    Slider  sldrSessionDuration, 
            sldrShortBreakDuration, 
            sldrLongBreakDuration,
            sldrSessionCount,
            sldrAlarmTime,
            sldrVolume;

    [SerializeField]
    IOSToggle  togglePreEndAlarmTime,
            toggleVibration,
			toggleBGM;

    [SerializeField]
    Dropdown    dropDownNotifAlert,
                dropDownPreEndAlert,
                dropDownThemes,
                dropDownLanguage;

    [SerializeField]
    Button btnSave, btnBack;

	[SerializeField]
	TextMeshProUGUI lblNote;

//    void Awake()
//    {
//        InitializeValue();
//    }



	// Use this for initialization
	void Start () {
        //InitializeValue();
        ButtonEvent();
        InitializeValue();
		PreEndAlarmSliderChange ();

		DisableSliders ();
		EnableSliders ();
		//bgmOn = DataController.Instance.settings.isBGMOn;
	}
	


	void OnDisable()
	{
		PomodoroUI.Instance.isMenuActive = false;
	}

    #region METHODS
    void ButtonEvent()
    {
        btnSave.onClick.AddListener(Save);
        btnBack.onClick.AddListener(Back);
    }

    void InitializeValue()
    {
		
        //input Field
        inputSessionDuration.text = DataController.Instance.settings.sessionDuration.ToString();
        inputShortBreakDuration.text = DataController.Instance.settings.shortBreakDuration.ToString();
        inputLongBreakDuration.text = DataController.Instance.settings.longBreakDuration.ToString();
        inputSessionCount.text = DataController.Instance.settings.sessionCount.ToString();
        inputAlarmTime.text = DataController.Instance.settings.alarmTime.ToString();
        inputVolume.text = DataController.Instance.settings.volume.ToString();

        //sliders
        sldrSessionDuration.minValue = DataController.Instance.settings.sessionMinDuration;
        sldrSessionDuration.maxValue = DataController.Instance.settings.sessionMaxDuration;
        sldrSessionDuration.value = DataController.Instance.settings.sessionDuration;

        sldrShortBreakDuration.minValue = DataController.Instance.settings.shortBreakMinDuration;
        sldrShortBreakDuration.maxValue = DataController.Instance.settings.shortBreakMaxDuration;
        sldrShortBreakDuration.value = DataController.Instance.settings.shortBreakDuration;

        sldrLongBreakDuration.minValue = DataController.Instance.settings.longBreakMinDuration;
        sldrLongBreakDuration.maxValue = DataController.Instance.settings.longBreakMaxDuration;
        sldrLongBreakDuration.value = DataController.Instance.settings.longBreakDuration;

        sldrSessionCount.minValue = DataController.Instance.settings.minSessionCount;
        sldrSessionCount.maxValue = DataController.Instance.settings.maxSessionCount;
        sldrSessionCount.value = DataController.Instance.settings.sessionCount;

        sldrAlarmTime.minValue = 0;
        sldrAlarmTime.maxValue = DataController.Instance.settings.maxPreEndAlarmTime;
        sldrAlarmTime.value = DataController.Instance.settings.alarmTime;

        sldrVolume.minValue = DataController.Instance.settings.minVolume;
        sldrVolume.maxValue = DataController.Instance.settings.maxVolume;
        sldrVolume.value = DataController.Instance.settings.volume * 100f;

        //toggle
        togglePreEndAlarmTime.On = DataController.Instance.settings.isPreEndAlarmOn;
        toggleVibration.On = DataController.Instance.settings.isVibrationOn;
		toggleBGM.On = DataController.Instance.settings.isBGMOn;

      //  slider event
        sldrShortBreakDuration.onValueChanged.AddListener(delegate {
            PreEndAlarmSliderChange();
        });

		sldrSessionDuration.onValueChanged.AddListener(delegate {
			PreEndAlarmSliderChange();
		});

		sldrLongBreakDuration.onValueChanged.AddListener(delegate {
			PreEndAlarmSliderChange();
		});

		togglePreEndAlarmTime.onOff.AddListener (delegate {


			sldrAlarmTime.interactable = false;


			inputAlarmTime.interactable = false;
		});
		togglePreEndAlarmTime.onOn.AddListener (delegate {
			//added code when timer running 
			if(DataController.Instance.playerData.isTimerRunning == true)
			{
				sldrAlarmTime.interactable = false;
				inputAlarmTime.interactable = false;
			}
			else
			{
				sldrAlarmTime.interactable = true;
				inputAlarmTime.interactable = true;
			}

		});

		sldrAlarmTime.onValueChanged.AddListener (delegate {
			OnPreEndAlarmTimeChanged();
		});

        //dropdown options
        List<string> themeoptions = new List<string>();// themes
		List<string> languageoptions = new List<string>();// language
		List<string> preEndAlertoption = new List<string>();// pre End
		List<string> notifAlertoption = new List<string>();// notification Alert

        //int count = Enum.GetNames(typeof(EnumCollection.Theme)).Length;



		//LoadEnumJson("localizedTextTheme_sp.json");
		LanguageCheckDropdown();


		// Theme dropdown  ***done
		var themeDic = enumDictionary.Where (y => y.Key == "set_theme").ToDictionary (y => y.Key, y => y.Value);

		foreach (List<string> listtheme in themeDic.Values) {
			foreach (var themeEnum in listtheme) {
				themeoptions.Add((themeEnum).ToString());
			}
		}
		dropDownThemes.AddOptions(themeoptions);
		themeoptions.Clear();

		//language dropdown ***done
		var languageDic = enumDictionary.Where (y => y.Key == "set_language").ToDictionary (y => y.Key, y => y.Value);

		foreach (List<string> listlang in languageDic.Values) {
			foreach (var dataEnum in listlang) {
				languageoptions.Add((dataEnum).ToString());
			}
		}
//        for (int i = 0; i < count; i++)
//        {
//            options.Add(((EnumCollection.Language)i).ToString());
//        }
		dropDownLanguage.AddOptions(languageoptions);
		languageoptions.Clear();



		//preEnd ALert dropdown ***done
		var preEndDic = enumDictionary.Where (y => y.Key == "set_preEndAlert").ToDictionary (y => y.Key, y => y.Value);
	
		foreach (List<string> listPreEndAlert in preEndDic.Values) {
			foreach (var dataEnum in listPreEndAlert) {
				preEndAlertoption.Add((dataEnum).ToString());
			}
		}
		dropDownPreEndAlert.AddOptions(preEndAlertoption);
		preEndAlertoption.Clear();


		//Notif ALert dropdown ***in progress
		var notificationAlert = enumDictionary.Where (y => y.Key == "set_notifAlert").ToDictionary (y => y.Key, y => y.Value);

		foreach (List<string> listNotifAlert in notificationAlert.Values) {
			foreach (var dataEnum in listNotifAlert) {
				notifAlertoption.Add((dataEnum).ToString());
			}
		}
		dropDownNotifAlert.AddOptions(notifAlertoption);
		notifAlertoption.Clear();
        //dropdown
        dropDownNotifAlert.value = DataController.Instance.settings.notifAlertIndex;
        dropDownPreEndAlert.value = DataController.Instance.settings.preEndAlertIndex;
        dropDownThemes.value = DataController.Instance.settings.themeIndex;
        dropDownLanguage.value = DataController.Instance.settings.languageIndex;
    
		dropDownNotifAlert.onValueChanged.AddListener (delegate {
			OnNotificationAlertChanged();
		});

		dropDownPreEndAlert.onValueChanged.AddListener (delegate {
			OnPreEndAlertChanged();
		});
	}


    void PreEndAlarmSliderChange()
    {
        //Debug.Log("event!");

		//Debug.Log ("<color=blue>" + "Slider sliding" +"</color>");
		float maxValue = GetLowestSliderValue();
        maxValue -= 1;
        //Debug.Log("Max value " + maxValue);
        
		sldrAlarmTime.maxValue = maxValue;


    }

	void OnNotificationAlertChanged()
	{
		SoundManager.Instance.SFX.Stop ();
		if (dropDownNotifAlert.options[dropDownNotifAlert.value].text == "Bark") {
			Framework.SoundManager.Instance.PlaySFX ("seDogBarkSmall");
		} else if (dropDownNotifAlert.options[dropDownNotifAlert.value].text == "Meow") {
			Framework.SoundManager.Instance.PlaySFX ("seCatMeow");
		} else {
			Framework.SoundManager.Instance.PlaySFX ("seAlarm");
		}
		//print (dropDownNotifAlert.options[dropDownNotifAlert.value].text + "WOW");

	}

	void OnPreEndAlarmTimeChanged()
	{
		if (sldrAlarmTime.normalizedValue == 0) {
			//togglePreEndAlarmTime.Interactible = false;

		} else {
			//togglePreEndAlarmTime.Interactible = true;
		}
	}

	void OnPreEndAlertChanged()
	{
		SoundManager.Instance.SFX.Stop ();
		if (dropDownPreEndAlert.options[dropDownPreEndAlert.value].text == "Bark") {
			Framework.SoundManager.Instance.PlaySFX ("seDogBarkSmall");
		} else if (dropDownPreEndAlert.options[dropDownPreEndAlert.value].text == "Meow") {
			Framework.SoundManager.Instance.PlaySFX ("seCatMeow");
		} else {
			Framework.SoundManager.Instance.PlaySFX ("seAlarm");
		}
		//print (dropDownNotifAlert.options[dropDownNotifAlert.value].text + "WOW");
	}

    // change theme based on dropdown value
    public void ThemeUpdate()
    {
		//play sfx
		Framework.SoundManager.Instance.PlaySFX ("seIconTapPomodoro");
		//nunu
        BackgroundThemeController.Instance.UpdateTheme((EnumCollection.Theme)dropDownThemes.value);
    }

    // change langage based on dropdown value
    public void LanguageUpdate()
    {
        LocalizationManager.instance.UpdateLanguage((EnumCollection.Language)dropDownLanguage.value);
    }


	float GetLowestSliderValue()
	{
		float[] sliderValue = {sldrSessionDuration.value, sldrShortBreakDuration.value, sldrLongBreakDuration.value};
		return sliderValue.Min();
	}


    void Save()
    {
		SoundManager.Instance.SFX.Stop ();
		SoundManager.Instance.PlaySFX ("seIconTapPomodoro");
        DataController.Instance.settings.sessionDuration = sldrSessionDuration.value;
        DataController.Instance.settings.shortBreakDuration = sldrShortBreakDuration.value;
        DataController.Instance.settings.longBreakDuration = sldrLongBreakDuration.value;
        DataController.Instance.settings.sessionCount = sldrSessionCount.value;
        DataController.Instance.settings.alarmTime = sldrAlarmTime.value;
        DataController.Instance.settings.volume = sldrVolume.value * 0.01f;
        DataController.Instance.settings.notifAlertIndex = dropDownNotifAlert.value;
        DataController.Instance.settings.preEndAlertIndex = dropDownPreEndAlert.value;

        DataController.Instance.settings.isPreEndAlarmOn = togglePreEndAlarmTime.On;
        DataController.Instance.settings.isVibrationOn = toggleVibration.On;
		DataController.Instance.settings.isBGMOn = toggleBGM.On;

        DataController.Instance.settings.notifAlertIndex = dropDownNotifAlert.value;
        DataController.Instance.settings.preEndAlertIndex = dropDownPreEndAlert.value;
        DataController.Instance.settings.themeIndex = dropDownThemes.value;
        DataController.Instance.settings.languageIndex = dropDownLanguage.value;
		switch(BackgroundThemeController.Instance.selectedTheme)
		{
			case EnumCollection.Theme.Dark:
			DataController.Instance.settings.themeIndex = 1;
				break;
		default:
			DataController.Instance.settings.themeIndex = 0;
			break;
		}
		// themes
		BackgroundThemeController.Instance.UpdateTheme((EnumCollection.Theme)DataController.Instance.settings.themeIndex);
		// language
		//LocalizationManager.instance.UpdateLanguage((EnumCollection.Language)DataController.Instance.settings.languageIndex);
		Destroy(gameObject);
        //Back();

		if (DataController.Instance.settings.isBGMOn) {
			SoundManager.Instance.UnmuteInsideBGM ();
			SoundManager.Instance.UnmuteOutsideBGM ();

			//to not able to hear after saving
			SoundManager.Instance.BGMVolume (0);
//			SoundManager.Instance.UnmuteSFX ();

		}

    }
	void UndoTheme()
	{
		if (DataController.Instance.settings.themeIndex == 1) {
			BackgroundThemeController.Instance.UpdateTheme ((EnumCollection.Theme.Dark));
		} else {
			BackgroundThemeController.Instance.UpdateTheme ((EnumCollection.Theme.Light));
		}
	}
    void Back()
    {
		SoundManager.Instance.SFX.Stop ();
		Framework.SoundManager.Instance.PlaySFX ("seIconTapPomodoro");	
		if (DataController.Instance.settings.isBGMOn) {
			SoundManager.Instance.UnmuteInsideBGM ();
			SoundManager.Instance.UnmuteOutsideBGM ();
			//to not able to hear after saving
			SoundManager.Instance.BGMVolume (0);
		}
		UndoTheme ();
		Destroy (gameObject);
    }


	//Enum Collection json file load
	public void LoadEnumJson(string fileName)
	{
		string filePath = "";

		if (Application.platform == RuntimePlatform.Android)
		{
			// Android
			filePath = System.IO.Path.Combine(Application.streamingAssetsPath, fileName);

			// Android only use WWW to read file
			WWW reader = new WWW(filePath);
			while ( ! reader.isDone) {}

			string realPath = Application.persistentDataPath + fileName;
			System.IO.File.WriteAllBytes(realPath, reader.bytes);

			filePath = realPath;
		}
		else
		{
			// iOS
			filePath = System.IO.Path.Combine(Application.streamingAssetsPath, fileName);
		}

		if (File.Exists (filePath)) {
			//Debug.Log ("Test");
			string dataAsJson = File.ReadAllText (filePath);
			LocalizationDataList enumData = JsonUtility.FromJson<LocalizationDataList> (dataAsJson);

			for (int i = 0; i < enumData.items.Length; i++) 
			{
				List<string> enumlist = new List<string> ();
				for (int j = 0; j < enumData.items[i].value.Length; j++) {
					

					//Debug.Log ("<color=green>" + "Key: " +    enumData.items [i].key +    " ALL enum data: " + enumData.items [i].value[j] + "</color>");	


					enumlist.Add (enumData.items [i].value [j]); 
				}

					enumDictionary.Add (enumData.items [i].key, enumlist);

				//enumlist.Add (enumData.items[i].value);
			}
//			for (int i = 0; i < enumlist.Count; i++) {
//				
//				Debug.Log ("EnumList " + enumlist[i]);
//			}
			//for (int i = 0; i < enumDictionary.Count; i++) {
			//	Debug.Log ("EnumDictionary Count: " + enumDictionary.Count);
			//}

			// Debug.Log ("Data loaded, dictionary contains: " + localizedText.Count + " entries");
		} else 
		{
			//Debug.LogError (filePath);
			//Debug.LogError ("Cannot find file!");
		}
	}
		
//	void selectedValue(Dropdown dropDownLanguage )
//	{
//
//
//
//		if (dropDownLanguage.value == 0 ) { // English
//			LocalizationManager.instance.LoadLocalizedText("localizedText_en.json");
//			Debug.Log("German");
//		}
//
//		else if (dropDownLanguage.value == 1 ) { // German
//			LocalizationManager.instance.LoadLocalizedText("localizedText_sp.json");
//			Debug.Log("German");
//		}
//
//		else if (dropDownLanguage.value == 2  ) {
//
//
//		}
//
//
//		else if(dropDownLanguage.value == 3 ){
//
//		}
//
//		else if (dropDownLanguage.value == 4  ){ 
//
//
//
//		}
//
//
//		else if(dropDownLanguage.value == 5 ){
//
//		}
//
//
//	}



	void LanguageCheckDropdown()
	{
		//Localized dropdown depends on language selected in settings ***In progress 4/5/2016

		if (DataController.Instance.settings.languageIndex == 0 ) { // English
			LoadEnumJson("localizedtextDropdown_en.json");
			//Debug.Log("English");
		}
		else if (DataController.Instance.settings.languageIndex == 1 ) { // Spain
			LoadEnumJson("localizedtextDropdown_sp.json");
			//Debug.Log("Spanish");
		}
	}

	public void SetAppVolume()
	{
		SoundManager.Instance.BGMVolume (sldrVolume.value * 0.01f);
		SoundManager.Instance.SFXVolume (sldrVolume.value * 0.01f);
		SoundManager.Instance.MuteInsideBGM ();
		SoundManager.Instance.MuteOutsideBGM ();
		//SoundManager.Instance.MuteSFX ();
	}

	//disable slider when timer is running
	void DisableSliders()
	{
		if(DataController.Instance.playerData.isTimerRunning == true)
		{
			sldrSessionDuration.interactable	 = false; 
			sldrShortBreakDuration.interactable  = false; 
			sldrLongBreakDuration.interactable   = false; 
			sldrSessionCount.interactable 		 = false; 
			sldrAlarmTime.interactable 			 = false;
			togglePreEndAlarmTime.Interactible   = false;

			inputSessionDuration.interactable 	 = false; 
			inputShortBreakDuration.interactable = false; 
			inputLongBreakDuration.interactable  = false; 
			inputSessionCount.interactable 		 = false; 
			inputAlarmTime.interactable 		 = false;
			togglePreEndAlarmTime.Interactible   = false;

			lblNote.gameObject.SetActive (true);

		}
	}

	void EnableSliders()
	{
		if (DataController.Instance.playerData.isTimerRunning == false) 
		{
			sldrSessionDuration.interactable     = true; 
			sldrShortBreakDuration.interactable  = true; 
			sldrLongBreakDuration.interactable   = true; 
			sldrSessionCount.interactable        = true; 
			sldrAlarmTime.interactable           = true;
			togglePreEndAlarmTime.Interactible   = true;

			inputSessionDuration.interactable 	 = true; 
			inputShortBreakDuration.interactable = true; 
			inputLongBreakDuration.interactable  = true; 
			inputSessionCount.interactable 		 = true; 
			inputAlarmTime.interactable = true;
			togglePreEndAlarmTime.Interactible = true;

			lblNote.gameObject.SetActive (false);
		}
	}


    #endregion
}
