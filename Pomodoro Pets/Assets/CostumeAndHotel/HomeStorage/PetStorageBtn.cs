﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PetStorageBtn : MonoBehaviour {
    public int PetIndex;

    [SerializeField]
    PetHomeController pc;

    [SerializeField]
    GameObject petStoragePanel;

    [SerializeField]
    public TextMeshProUGUI tm;

    [SerializeField]
    GameObject BtnSwap;

	public void SetName(string name)
    {
        gameObject.name = name;
        tm.text = name;
    }

    public void SelectPet()
    {
        pc.sendToStorage = true;
        BtnSwap.SetActive(true);
        pc.haveCurSelected = true;
        pc.curSelected = PetIndex;
        petStoragePanel.SetActive(false);
    }
}
