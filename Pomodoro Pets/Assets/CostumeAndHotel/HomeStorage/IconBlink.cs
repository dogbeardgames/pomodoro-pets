﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IconBlink : MonoBehaviour {
    bool _on;
    void start ()
    {
        InvokeRepeating("Blink", .5f, 0.5f);
    }
    void Blink()
    {
        _on = !_on;
        GetComponent<Image>().enabled = _on;
    }
}
