﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoragePetButton : MonoBehaviour {

    public PetStorage ps;

    [SerializeField]
    GameObject DogPanel, CatPanel;

    [SerializeField]
    Button DogButton, CatButton, EmptyButton;

    [SerializeField]
    GameObject PetStorage;

    private void OnEnable()
    {
        DogButton.interactable = false;
    }

    public void DogsTab()
    {
        DogButton.interactable = false;
        CatButton.interactable = true;
        DogPanel.SetActive(true);
        CatPanel.SetActive(false);
    }

    public void CatsTab()
    {
        CatButton.interactable = false;
        DogButton.interactable = true;
        CatPanel.SetActive(true);
        DogPanel.SetActive(false);
    }

    public void OpenStorage()
    {
        PetStorage.SetActive(true);
    }

    public void CloseStorage()
    {
        PetStorage.SetActive(false);
    }

    public void PutInStorage()
    {
        ps.SetInStorage = true;
        PetStorage.SetActive(false);
    }
}

