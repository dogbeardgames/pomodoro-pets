﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class PetStorage : MonoBehaviour
{

    public bool SetInStorage;

    [SerializeField]
    Button dogBtnTemplate, catBtnTemplate;

    [SerializeField]
    Transform dogParent, catParent;

    public List<GameObject> DogsInStorage = new List<GameObject>();
    public List<Sprite> DogsIcon = new List<Sprite>();

    public List<GameObject> CatsInStorage = new List<GameObject>();
    public List<Sprite> CatsIcon = new List<Sprite>();

    public List<Button> ToBeCleared = new List<Button>();
    
    private void OnEnable()
    {
        foreach(Button i in ToBeCleared)
        {
            Destroy(i.gameObject);
        }
        ToBeCleared.Clear();

        DogsInStorage = Enumerable.ToList(Enumerable.Distinct(DogsInStorage));
        CatsInStorage = Enumerable.ToList(Enumerable.Distinct(CatsInStorage));

        DogsInStorage = DogsInStorage.OrderBy(go => go.name).ToList();
        CatsInStorage = CatsInStorage.OrderBy(go => go.name).ToList();
        
        DogStorage();
        CatStorage();

    }

    void DogStorage()
    {
        for (int i = 0; i < DogsInStorage.Count; i++)
        {
            Button petClone = Instantiate(dogBtnTemplate) as Button;
            petClone.gameObject.SetActive(true);
            PetStorageBtn psb = petClone.GetComponent<PetStorageBtn>();
            psb.SetName(DogsInStorage[i].name);
            psb.PetIndex = DogsInStorage[i].GetComponent<PetHomePositionTracker>().objectToTrack.GetComponent<PetHomeButton>().petIndex;
            petClone.transform.GetChild(0).GetComponent<Image>().overrideSprite = DogsIcon.Find(x => x.name.Contains(petClone.name));
            petClone.transform.SetParent(dogParent);
            petClone.transform.localScale = new Vector3(1, 1, 1);
            ToBeCleared.Add(petClone);
        }
    }

    void CatStorage()
    {
        for (int i = 0; i < CatsInStorage.Count; i++)
        {
            Button petClone = Instantiate(catBtnTemplate) as Button;
            petClone.gameObject.SetActive(true);
            PetStorageBtn psb = petClone.GetComponent<PetStorageBtn>();
            psb.SetName(CatsInStorage[i].name);
            psb.PetIndex = CatsInStorage[i].GetComponent<PetHomePositionTracker>().objectToTrack.GetComponent<PetHomeButton>().petIndex;
            petClone.transform.GetChild(0).GetComponent<Image>().overrideSprite = CatsIcon.Find(x => x.name.Contains(petClone.name));
            petClone.transform.SetParent(catParent);
            petClone.transform.localScale = new Vector3(1, 1, 1);
            ToBeCleared.Add(petClone);
        }
    }

}