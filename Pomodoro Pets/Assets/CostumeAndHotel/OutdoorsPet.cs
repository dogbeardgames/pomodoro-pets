﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutdoorsPet : MonoBehaviour {

    public int PetPosition;
    public EnumCollection.Pet petType;
    public EnumCollection.PetCategory petCategory;
    public string petName;
    public string petBreed;
    public float happiness;
    public float hunger_thirst;
    public float cleanliness;
    public float energy;
    public string headEquipment;
    public string bodyEquipment;
    public string accessories;
    public bool isFoodFull;
    public bool isEnerygFull;
    public List<Vector3> bubblePosition;
    void Start()
    {
        Initialize();
    }
    public void Initialize()
    {
        EnumCollection.Pet petType = DataController.Instance.playerData.playerPetList[PetPosition].petType;
        string petBreed = DataController.Instance.playerData.playerPetList[PetPosition].petBreed;
        OutdoorsPetController.Instance.SetPet(petType, petBreed, PetPosition);
    }
}
