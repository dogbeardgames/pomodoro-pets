﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Spine.Unity.Modules;
using Spine.Unity;
using System.Linq;

public class OutdoorsPetController : MonoBehaviour
{
    public static OutdoorsPetController Instance;
    public OutdoorsPet[] pet;
    void Awake()
    {
        if (Instance == null)
            Instance = this;
    }
    private void Start()
    {
        pet = FindObjectsOfType<OutdoorsPet>();
    }
    public void SetPet(EnumCollection.Pet petType, string petBreed, int i)
    {
        PetBase petBase = DataController.Instance.playerData.playerPetList[i];
        pet[i].petName = petBase.petName;
        pet[i].petBreed = petBase.petBreed;
        pet[i].happiness = petBase.happiness;
        pet[i].hunger_thirst = petBase.hunger_thirst;
        pet[i].cleanliness = petBase.cleanliness;
        pet[i].energy = petBase.energy;
        pet[i].petType = petBase.petType;
        pet[i].petCategory = petBase.petCategory;
        pet[i].isFoodFull = petBase.isFoodFull;
        pet[i].accessories = petBase.accessories;
        pet[i].bodyEquipment = petBase.bodyEquipment;
        pet[i].headEquipment = petBase.headEquipment;
        if (petType == EnumCollection.Pet.Dog)
        {
            pet[i].GetComponent<SkeletonAnimation>().skeletonDataAsset = GetComponent<OutdoorsPetAssets>().PetSpine(EnumCollection.Pet.Dog, petBreed);
        }
        else if (petType == EnumCollection.Pet.Cat)
        {
            pet[i].GetComponent<SkeletonAnimation>().skeletonDataAsset = GetComponent<OutdoorsPetAssets>().PetSpine(EnumCollection.Pet.Cat, petBreed);
        }
        OutdoorsCostume petCostume = GetComponent<OutdoorsPetAssets>().petObject[i].GetComponent<OutdoorsCostume>();
        petCostume.ApplyAccessories(pet[i].accessories,i);
        petCostume.ApplyHead(pet[i].headEquipment,i);
        petCostume.ApplyBody(pet[i].bodyEquipment,i);
    }
}
