﻿using System.Collections;
using UnityEngine;
using Spine.Unity;
using Spine.Unity.Modules;
using Spine;
using System.Linq;
using System.Collections.Generic;
using System;
using Spine.Unity;

public class OutdoorsCostume : MonoBehaviour {
    SkeletonAnimation skeletonAnimation;
    List<int> equippedBodyList;
    List<int> equippedHeadList;
    List<int> equippedAccessoriesList;
    PetAssets petAssets;
    void Awake()
    {
        petAssets = FindObjectOfType<PetAssets>();
        skeletonAnimation = GetComponent<SkeletonAnimation>();
        equippedBodyList = new List<int>();
        equippedHeadList = new List<int>();
        equippedAccessoriesList = new List<int>();
    }

    private void Update()
    {
        skeletonAnimation.gameObject.GetComponent<MeshRenderer>().sortingOrder = Mathf.Abs( Mathf.RoundToInt(transform.position.y * 100f) * -1);
    }

    public void ApplyHead(string key, int index)
    {
        string slotName = "headCostume";
        string breed = DataController.Instance.playerData.playerPetList[index].petBreed;
        Sprite[] headSprites = Resources.LoadAll<Sprite>("Head/" + breed + "Head");
        Sprite headSprite;
        headSprite = headSprites.FirstOrDefault(s => s.name == key);
        equippedHeadList.Add(skeletonAnimation.skeleton.FindSlotIndex(slotName));
        if (headSprite != null)
        {
            skeletonAnimation.skeleton.AttachUnitySprite(slotName, headSprite);
            DataController.Instance.playerData.playerPetList[index].headEquipment = key;
            GetComponent<OutdoorsPet>().headEquipment = key;
        }
    }
    public void ApplyBody(string key, int index)
    {
        try
        {
            string breed = DataController.Instance.playerData.playerPetList[index].petBreed;
            if (DataController.Instance.bodyDatabase.items.FirstOrDefault(item => item.key == key).slot == "" || DataController.Instance.bodyDatabase.items.FirstOrDefault(item => item.key == key).slot == null)
            {
                string path = DataController.Instance.bodyDatabase.items.First(item => item.key == key).value;
                Sprite[] bodySprites = Resources.LoadAll<Sprite>(path + breed + key);
                Sprite BackLeftLeg = null,
                BackLeftLeg2 = null,
                BackRightLeg = null,
                BackRightLeg2 = null,
                FrontLeftLeg = null,
                FrontLeftLeg2 = null,
                FrontRightLeg = null,
                FrontRightLeg2 = null,
                LowerBody = null,
                UpperBody = null,
                UpperBody2 = null,
                Back = null,
                Shield = null;
                BackLeftLeg = bodySprites.First(s => s.name == key + "BackLeftLeg");
                BackLeftLeg2 = bodySprites.First(s => s.name == key + "BackLeftLeg2");
                BackRightLeg = bodySprites.First(s => s.name == key + "BackRightLeg");
                BackRightLeg2 = bodySprites.First(s => s.name == key + "BackRightLeg2");
                FrontLeftLeg = bodySprites.First(s => s.name == key + "FrontLeftLeg");
                FrontLeftLeg2 = bodySprites.First(s => s.name == key + "FrontLeftLeg2");
                FrontRightLeg = bodySprites.First(s => s.name == key + "FrontRightLeg");
                FrontRightLeg2 = bodySprites.First(s => s.name == key + "FrontRightLeg2");
                LowerBody = bodySprites.First(s => s.name == key + "LowerBody");
                UpperBody = bodySprites.First(s => s.name == key + "UpperBody");
                if (DataController.Instance.playerData.playerPetList[index].petType == EnumCollection.Pet.Dog)
                {
                    UpperBody2 = bodySprites.FirstOrDefault(s => s.name == key + "UpperBody2");
                }
                if (key == "BVader")
                {
                    Back = bodySprites.FirstOrDefault(s => s.name == key + "Cape");
                }
                if (key == "Catain")
                {
                    Shield = bodySprites.FirstOrDefault(s => s.name == key + "Shield");
                }
                equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("backLeftLeg"));
                equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("backLeftLeg2"));
                equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("backRightLeg"));
                equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("backRightLeg2"));
                equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("frontLeftLeg"));
                equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("frontLeftLeg2"));
                equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("frontRightLeg"));
                equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("frontRightLeg2"));
                equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("lowerBody"));
                equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("upperBody"));
                if (DataController.Instance.playerData.playerPetList[index].petType == EnumCollection.Pet.Dog)
                {
                    equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("upperBody2"));
                }
                if (key == "BVader")
                {
                    equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("back"));
                    skeletonAnimation.skeleton.AttachUnitySprite("back", Back);
                }
                if (key == "Catain")
                {
                    equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("shield"));
                    skeletonAnimation.skeleton.AttachUnitySprite("shield", Shield);
                }
                skeletonAnimation.skeleton.AttachUnitySprite("backLeftLeg", BackLeftLeg);
                skeletonAnimation.skeleton.AttachUnitySprite("backLeftLeg2", BackLeftLeg2);
                skeletonAnimation.skeleton.AttachUnitySprite("backRightLeg", BackRightLeg);
                skeletonAnimation.skeleton.AttachUnitySprite("backRightLeg2", BackRightLeg2);
                skeletonAnimation.skeleton.AttachUnitySprite("frontLeftLeg", FrontLeftLeg);
                skeletonAnimation.skeleton.AttachUnitySprite("frontLeftLeg2", FrontLeftLeg2);
                skeletonAnimation.skeleton.AttachUnitySprite("frontRightLeg", FrontRightLeg);
                skeletonAnimation.skeleton.AttachUnitySprite("frontRightLeg2", FrontRightLeg2);
                skeletonAnimation.skeleton.AttachUnitySprite("lowerBody", LowerBody);
                skeletonAnimation.skeleton.AttachUnitySprite("upperBody", UpperBody);
                if (DataController.Instance.playerData.playerPetList[index].petType == EnumCollection.Pet.Dog)
                {
                    skeletonAnimation.skeleton.AttachUnitySprite("upperBody2", UpperBody2);
                }
                DataController.Instance.playerData.playerPetList[index].bodyEquipment = key;
                GetComponent<Pet>().bodyEquipment = key;
            }
            else
            {
                string path = DataController.Instance.bodyDatabase.items.FirstOrDefault(item => item.key == key).value;
                Sprite[] sprites = Resources.LoadAll<Sprite>(path + breed + "NonSet");
                Sprite body;
                body = sprites.FirstOrDefault(s => s.name == key);
                equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex(DataController.Instance.bodyDatabase.items.FirstOrDefault(item => item.key == key).slot));
                skeletonAnimation.Skeleton.AttachUnitySprite(DataController.Instance.bodyDatabase.items.FirstOrDefault(item => item.key == key).slot, body);
                DataController.Instance.playerData.playerPetList[index].bodyEquipment = key;
                GetComponent<Pet>().bodyEquipment = key;
            }
        }
        catch (NullReferenceException e)
        {
        }
    }
    public void ApplyAccessories(string key, int index)
    {
        try
        {
            string path = DataController.Instance.AccessoriesDatabase.items.FirstOrDefault(item => item.key == key).value;
            string slotName = DataController.Instance.AccessoriesDatabase.items.FirstOrDefault(item => item.key == key).slot;
            string breed = DataController.Instance.playerData.playerPetList[index].petBreed;
            Sprite[] accessorySprites = Resources.LoadAll<Sprite>(path + breed + "Accessories");
            Sprite accessorySprite;
            accessorySprite = accessorySprites.FirstOrDefault(s => s.name == key);
            equippedAccessoriesList.Add(skeletonAnimation.skeleton.FindSlotIndex(slotName));
            skeletonAnimation.skeleton.AttachUnitySprite(slotName, accessorySprite);
            DataController.Instance.playerData.playerPetList[index].accessories = key;
            GetComponent<Pet>().accessories = key;
        }
        catch (NullReferenceException e)
        {
        }
    }
    public void ApplySleepHead(string key, string _catBreed, int index)
    {
        try
        {
            if (_catBreed == EnumCollection.CatBreed.Abyssinian.ToString() ||
                _catBreed == EnumCollection.CatBreed.Bengal.ToString() ||
                _catBreed == EnumCollection.CatBreed.Bombay.ToString() ||
                _catBreed == EnumCollection.CatBreed.Siamese.ToString() ||
                _catBreed == EnumCollection.CatBreed.Sphynx.ToString())
            {
                string path = DataController.Instance.headDatabase.items.FirstOrDefault(item => item.key == key).value;
                string slotName = DataController.Instance.headDatabase.items.FirstOrDefault(item => item.key == key).slot;
                string breed = DataController.Instance.playerData.playerPetList[index].petBreed;
                Sprite[] headSprites = Resources.LoadAll<Sprite>(path + breed + "Head");
                Sprite headSprite;
                headSprite = headSprites.FirstOrDefault(s => s.name == key);
                equippedHeadList.Add(skeletonAnimation.skeleton.FindSlotIndex(slotName));
                skeletonAnimation.skeleton.AttachUnitySprite(slotName, headSprite);
            }
            else
            {
                if (key == "pandaHat" || key == "CatainMask")
                {
                    string path = DataController.Instance.headDatabase.items.FirstOrDefault(item => item.key == key).value;
                    string slotName = "sleepHeadCostume";
                    string breed = DataController.Instance.playerData.playerPetList[index].petBreed;
                    Sprite[] headSprites = Resources.LoadAll<Sprite>(path + breed + "Head");
                    Sprite headSprite;
                    key = "sleep" + key.First().ToString().ToUpper() + key.Substring(1);
                    headSprite = headSprites.FirstOrDefault(s => s.name == key);
                    equippedHeadList.Add(skeletonAnimation.skeleton.FindSlotIndex("sleepHeadCostume"));
                    skeletonAnimation.skeleton.AttachUnitySprite(slotName, headSprite);
                }
            }
        }
        catch (NullReferenceException e)
        {
        }
    }
    public void ApplySleepBody(string key, string _catBreed, int index)
    {
        try
        {
            if (_catBreed == EnumCollection.CatBreed.Abyssinian.ToString() ||
                _catBreed == EnumCollection.CatBreed.Bengal.ToString() ||
                _catBreed == EnumCollection.CatBreed.Bombay.ToString() ||
                _catBreed == EnumCollection.CatBreed.Siamese.ToString() ||
                _catBreed == EnumCollection.CatBreed.Sphynx.ToString())
            {
                string breed = DataController.Instance.playerData.playerPetList[index].petBreed;

                if (DataController.Instance.bodyDatabase.items.FirstOrDefault(item => item.key == key).slot == "" || DataController.Instance.bodyDatabase.items.FirstOrDefault(item => item.key == key).slot == null)
                {
                    string path = DataController.Instance.bodyDatabase.items.First(item => item.key == key).value;
                    Sprite[] bodySprites = Resources.LoadAll<Sprite>(path + breed + key);
                    Sprite SleepBody,
                            SleepLegFront,
                            SleepLegBack,
                            Shield;
                    SleepBody = bodySprites.First(s => s.name == key + "SleepBody");
                    SleepLegFront = bodySprites.First(s => s.name == key + "SleepLegFront");
                    SleepLegBack = bodySprites.First(s => s.name == key + "SleepLegBack");
                    Shield = bodySprites.First(s => s.name == key + "Shield");
                    equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("sleepBody"));
                    equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("sleepLegFront"));
                    equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("sleepLegBack"));
                    equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("shield"));
                    skeletonAnimation.skeleton.AttachUnitySprite("sleepBody", SleepBody);
                    skeletonAnimation.skeleton.AttachUnitySprite("sleepLegFront", SleepLegFront);
                    skeletonAnimation.skeleton.AttachUnitySprite("sleepLegBack", SleepLegBack);
                    skeletonAnimation.skeleton.AttachUnitySprite("shield", Shield);
                }
                else
                {
                    if (key == "bowtie")
                    {
                        string path = DataController.Instance.bodyDatabase.items.FirstOrDefault(item => item.key == key).value;
                        Sprite[] sprites = Resources.LoadAll<Sprite>(path + breed + "NonSet");
                        Sprite body;
                        body = sprites.FirstOrDefault(s => s.name == key);
                        equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("bodyCostume2"));
                        skeletonAnimation.Skeleton.AttachUnitySprite("bodyCostume2", body);                                    
                    }
                    else
                    {
                        string path = DataController.Instance.bodyDatabase.items.FirstOrDefault(item => item.key == key).value;
                        Sprite[] sprites = Resources.LoadAll<Sprite>(path + breed + "NonSet");
                        Sprite body;
                        key = "sleep" + key.First().ToString().ToUpper() + key.Substring(1);
                        body = sprites.FirstOrDefault(s => s.name == key);
                        equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("bodyCostume"));
                        skeletonAnimation.Skeleton.AttachUnitySprite("bodyCostume", body);
                    }
                }
            }
            else
            {
                string breed = DataController.Instance.playerData.playerPetList[index].petBreed;
                if (DataController.Instance.bodyDatabase.items.FirstOrDefault(item => item.key == key).slot == "" || DataController.Instance.bodyDatabase.items.FirstOrDefault(item => item.key == key).slot == null)
                {
                    string path = DataController.Instance.bodyDatabase.items.First(item => item.key == key).value;
                    Sprite[] bodySprites = Resources.LoadAll<Sprite>(path + breed + key);
                    Sprite SleepRightLegFront,
                    SleepRightLegBack,
                    SleepBody,
                    SleepLeftLegBack,
                    SleepLeftLegFront,
                    Shield;
                    SleepBody = bodySprites.First(s => s.name == "sleep" + key + "Body");
                    SleepRightLegFront = bodySprites.First(s => s.name == "sleep" + key + "RightLegFront");
                    SleepRightLegBack = bodySprites.First(s => s.name == "sleep" + key + "RightLegBack");
                    SleepLeftLegFront = bodySprites.First(s => s.name == "sleep" + key + "LeftLegFront");
                    SleepLeftLegBack = bodySprites.First(s => s.name == "sleep" + key + "LeftLegBack");
                    Shield = bodySprites.First(s => s.name == key + "Shield");
                    equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("sleepCatainBody"));
                    equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("sleepCatainRightLegFront"));
                    equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("sleepCatainRightLegBack"));
                    equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("sleepCatainLeftLegFront"));
                    equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("sleepCatainLeftLegBack"));
                    equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex("shield"));
                    skeletonAnimation.skeleton.AttachUnitySprite("sleepCatainBody", SleepBody);
                    skeletonAnimation.skeleton.AttachUnitySprite("sleepCatainRightLegFront", SleepRightLegFront);
                    skeletonAnimation.skeleton.AttachUnitySprite("sleepCatainRightLegBack", SleepRightLegBack);
                    skeletonAnimation.skeleton.AttachUnitySprite("sleepCatainLeftLegFront", SleepLeftLegFront);
                    skeletonAnimation.skeleton.AttachUnitySprite("sleepCatainLeftLegBack", SleepLeftLegBack);
                    skeletonAnimation.skeleton.AttachUnitySprite("shield", Shield);
                }
                else
                {
                    string path = DataController.Instance.bodyDatabase.items.FirstOrDefault(item => item.key == key).value;
                    string slotName = "sleepBodyCostume";
                    Sprite[] sprites = Resources.LoadAll<Sprite>(path + breed + "NonSet");
                    Sprite body;
                    key = "sleep" + key.First().ToString().ToUpper() + key.Substring(1);
                    body = sprites.FirstOrDefault(s => s.name == key);
                    equippedBodyList.Add(skeletonAnimation.skeleton.FindSlotIndex(slotName));
                    skeletonAnimation.Skeleton.AttachUnitySprite(slotName, body);
                }
            }
        }
        catch (NullReferenceException e)
        {
        }
    }
    public void ApplySleepAccessory(string key, string _catBreed, int index)
    {
        try
        {
            if (_catBreed == EnumCollection.CatBreed.Himalayan.ToString() ||
               _catBreed == EnumCollection.CatBreed.Maine_Coon.ToString() ||
               _catBreed == EnumCollection.CatBreed.Munchkin.ToString() ||
               _catBreed == EnumCollection.CatBreed.Persian.ToString() ||
               _catBreed == EnumCollection.CatBreed.Ragdoll.ToString())
            {
                string path = DataController.Instance.AccessoriesDatabase.items.FirstOrDefault(item => item.key == key).value;
                string slotName = "sleepAccessoryCostume";
                string breed = DataController.Instance.playerData.playerPetList[index].petBreed;
                Sprite[] accessorySprites = Resources.LoadAll<Sprite>(path + breed + "Accessories");
                Sprite accessorySprite;
                key = "sleep" + key.First().ToString().ToUpper() + key.Substring(1);
                accessorySprite = accessorySprites.FirstOrDefault(s => s.name == key);
                equippedAccessoriesList.Add(skeletonAnimation.skeleton.FindSlotIndex(slotName));
                skeletonAnimation.skeleton.AttachUnitySprite(slotName, accessorySprite);
            }
            else
            {
                string path = DataController.Instance.AccessoriesDatabase.items.FirstOrDefault(item => item.key == key).value;
                string slotName = "accessoryCostume";
                string breed = DataController.Instance.playerData.playerPetList[index].petBreed;
                Sprite[] accessorySprites = Resources.LoadAll<Sprite>(path + breed + "Accessories");
                Sprite accessorySprite;
                accessorySprite = accessorySprites.FirstOrDefault(s => s.name == key);
                equippedAccessoriesList.Add(skeletonAnimation.skeleton.FindSlotIndex(slotName));
                skeletonAnimation.skeleton.AttachUnitySprite(slotName, accessorySprite);
            }
        }
        catch (NullReferenceException e)
        {
        }
    }
    public void ApplySleepCostume(int index)
    {
        Pet pet = FindObjectOfType<Pet>();
        if (pet != null)
        {
            pet.GetComponent<SkeletonAnimation>().Initialize(true);
            ApplySleepHead(pet.headEquipment, pet.petBreed, index);
            ApplySleepBody(pet.bodyEquipment, pet.petBreed, index);
            ApplySleepAccessory(pet.accessories, pet.petBreed, index);
        }
    }
}
