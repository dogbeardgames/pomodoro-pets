﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScaleForeground : MonoBehaviour {

	[SerializeField]
	GameObject foreground;

	// Use this for initialization
	void Start () 
	{
		//if (Screen.orientation == ScreenOrientation.LandscapeLeft) {
			RectTransform rt = foreground.GetComponent<RectTransform> ();
			rt.localScale = new Vector3 (1.2f, 1, 1);
		//}

		//else if(Screen.orientation == ScreenOrientation.Portrait)
		//{
		//	RectTransform rt = foreground.GetComponent<RectTransform> ();
		//	//rt.sizeDelta = new Vector2 (rt.sizeDelta.x, (chartHeights * normalizedValue)/line);
		//	rt.localScale = new Vector3 (1, 1, 1);
		//}
	}

}
