﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PetPlaythings : ScriptableObject 
{
    public PlayThings[] items;
}

[System.Serializable]
public class PlayThings
{
    public string itemName;
    public Sprite image;
    public bool isForDog, isForCat, isOwned;

    public enum InteractionType {Animation, Interaction}
    public InteractionType interActionType;
}
