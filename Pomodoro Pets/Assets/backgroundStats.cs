﻿//using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;




public class backgroundStats : MonoBehaviour {

	[SerializeField]
	//Transform[] trans = GameObject.Find ("numberChart").transform.GetComponentsInChildren <Transform>(true);
	Transform[] trans;

	void Start()
	{
		LineColor ();
	}

	void OnEnable()
	{
		LineColor ();
	}


	void LineColor()
	{

		trans = transform.GetComponentsInChildren<Transform> (true);

		if(DataController.Instance.settings.themeIndex == 0)
		{
			foreach (var item in trans) 
			{
				//Debug.Log ("<color=blue>" + item.name + "</color>");

				//dash
				if (item.name == "line") 
				{
					item.GetComponent<Image> ().color = new Color(0,0,0,1);
				}
			}
		}

		else if(DataController.Instance.settings.themeIndex == 1)
		{
			foreach (var item in trans) 
			{
				//Debug.Log ("<color=blue>" + item.name + "</color>");

				//dash
				if (item.name == "line") 
				{	
					item.GetComponent<Image> ().color = new Color(1,1,1,1);
				}
			}
		}
	}

}
