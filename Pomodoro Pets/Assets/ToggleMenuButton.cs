﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleMenuButton : MonoBehaviour {
	public static ToggleMenuButton ins;

	[SerializeField]
	List<GameObject> lstButton;

	public GameObject showPanel;
	[SerializeField]
	GameObject activeGlow;

	//delegate void ShowMenu();
	event CoroutineUtilities.OnCompleteEventHandler OnMenuShow;
	event CoroutineUtilities.OnLoopEventHandler OnShowing;

	bool _show, _reverse;
	void Awake()
	{
		ins = this;
	}


	void Start()
	{
		//ins = this;
		//OnMenuShow += Test;
		//OnShowing += Test;
		OnShowing += ShowMenuItems;
	}

	[SerializeField]
    bool isON = true;
	[SerializeField]
	bool animating = false;
	// Use this for initialization

	void Test(int cntr)
	{
		//print ("nagbibilang! " + cntr);
	}
	void Show(int cntr)
	{
		
	}
	public void ShowPanel () {
		
		if (!animating) {
			int cntr = 0;
			animating = true;
			if (isON == false) {
				//StartCoroutine (IEShow(true, false));
				_show = true; _reverse = false;
				StaticCoroutine.StartForLoop(0.01f,cntr, lstButton.Count, OnShowing);
				isON = true;
				activeGlow.SetActive (true);
			} else if(isON==true) {
				//StartCoroutine (IEShow(false, true));
				_show = false; _reverse = true;
				cntr = lstButton.Count-1;
				StaticCoroutine.StartForLoopReverse(0.01f,cntr, 0, OnShowing);
				isON = false;
				activeGlow.SetActive (false);
			}
			//Framework.SoundManager.Instance.PlaySFX("seIconTapPomodoro");
		}



	}

    // toggle the button when a menu is pressed
    public void Toggle()
    {
        isON = !isON;
    }

	public void Hide()
	{
		isON = false;
		for (int i = 0; i < lstButton.Count; i++) {
			lstButton [i].gameObject.SetActive (false);

		}
		activeGlow.SetActive (false);
	}

	void ShowMenuItems(int index)
	{
		if (!_reverse) {
			lstButton [index].gameObject.SetActive (_show);
			if(index >= lstButton.Count-1)
			{
				animating = false;
			}
		} else {
			lstButton [index].gameObject.SetActive (_show);
			if(index <= 0)
			{
				animating = false;
			}
		}
	}

	IEnumerator IEShow(bool val, bool reverse)
	{
		if (!reverse) {
			for (int i = 0; i < lstButton.Count; i++) {
				lstButton [i].gameObject.SetActive (val);
				yield return new WaitForSeconds (0.01f);
			}
		} else {
			for(int i=lstButton.Count-1; i>=0; i--)
			{
				lstButton [i].gameObject.SetActive (val);
				yield return new WaitForSeconds (0.01f);
			}
		}
		animating = false;
	}




}
