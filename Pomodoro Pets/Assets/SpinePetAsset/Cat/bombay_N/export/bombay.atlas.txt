
bombay.png
size: 2048,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
parts/chest
  rotate: false
  xy: 1251, 784
  size: 263, 238
  orig: 263, 238
  offset: 0, 0
  index: -1
parts/earLeft
  rotate: true
  xy: 1217, 630
  size: 152, 178
  orig: 152, 178
  offset: 0, 0
  index: -1
parts/earRight
  rotate: false
  xy: 1397, 604
  size: 152, 178
  orig: 152, 178
  offset: 0, 0
  index: -1
parts/eyes
  rotate: true
  xy: 1993, 876
  size: 146, 33
  orig: 146, 33
  offset: 0, 0
  index: -1
parts/footBackLeft
  rotate: false
  xy: 903, 450
  size: 57, 72
  orig: 57, 72
  offset: 0, 0
  index: -1
parts/footBackRight
  rotate: false
  xy: 962, 450
  size: 57, 72
  orig: 57, 72
  offset: 0, 0
  index: -1
parts/footFrontLeft
  rotate: true
  xy: 1118, 542
  size: 65, 84
  orig: 65, 84
  offset: 0, 0
  index: -1
parts/footFrontRight
  rotate: false
  xy: 836, 438
  size: 65, 84
  orig: 65, 84
  offset: 0, 0
  index: -1
parts/head
  rotate: false
  xy: 823, 524
  size: 293, 306
  orig: 293, 306
  offset: 0, 0
  index: -1
parts/lowerBody
  rotate: false
  xy: 1516, 803
  size: 240, 219
  orig: 240, 219
  offset: 0, 0
  index: -1
parts/mouthDown
  rotate: false
  xy: 1713, 750
  size: 42, 51
  orig: 42, 51
  offset: 0, 0
  index: -1
parts/mouthUp
  rotate: false
  xy: 2, 2
  size: 67, 50
  orig: 67, 50
  offset: 0, 0
  index: -1
parts/nose
  rotate: false
  xy: 1217, 809
  size: 29, 24
  orig: 29, 24
  offset: 0, 0
  index: -1
parts/sleepBody
  rotate: false
  xy: 453, 512
  size: 368, 318
  orig: 368, 318
  offset: 0, 0
  index: -1
parts/sleepLegBack
  rotate: false
  xy: 679, 411
  size: 155, 99
  orig: 155, 99
  offset: 0, 0
  index: -1
parts/sleepLegFront
  rotate: false
  xy: 1758, 769
  size: 231, 88
  orig: 231, 88
  offset: 0, 0
  index: -1
parts/sleepTail
  rotate: false
  xy: 1758, 859
  size: 233, 163
  orig: 233, 163
  offset: 0, 0
  index: -1
parts/tail1
  rotate: false
  xy: 71, 6
  size: 39, 46
  orig: 39, 46
  offset: 0, 0
  index: -1
parts/tail10
  rotate: false
  xy: 1713, 665
  size: 39, 41
  orig: 39, 41
  offset: 0, 0
  index: -1
parts/tail11
  rotate: false
  xy: 277, 13
  size: 40, 39
  orig: 40, 39
  offset: 0, 0
  index: -1
parts/tail2
  rotate: false
  xy: 1021, 477
  size: 38, 45
  orig: 38, 45
  offset: 0, 0
  index: -1
parts/tail3
  rotate: false
  xy: 154, 12
  size: 39, 40
  orig: 39, 40
  offset: 0, 0
  index: -1
parts/tail4
  rotate: false
  xy: 1713, 623
  size: 39, 40
  orig: 39, 40
  offset: 0, 0
  index: -1
parts/tail5
  rotate: false
  xy: 195, 12
  size: 39, 40
  orig: 39, 40
  offset: 0, 0
  index: -1
parts/tail6
  rotate: false
  xy: 236, 12
  size: 39, 40
  orig: 39, 40
  offset: 0, 0
  index: -1
parts/tail7
  rotate: false
  xy: 1713, 708
  size: 40, 40
  orig: 40, 40
  offset: 0, 0
  index: -1
parts/tail8
  rotate: false
  xy: 112, 12
  size: 40, 40
  orig: 40, 40
  offset: 0, 0
  index: -1
parts/tail9
  rotate: false
  xy: 1061, 481
  size: 39, 41
  orig: 39, 41
  offset: 0, 0
  index: -1
parts/upperBackLegLeft
  rotate: false
  xy: 1551, 610
  size: 79, 191
  orig: 79, 191
  offset: 0, 0
  index: -1
parts/upperBackLegRight
  rotate: false
  xy: 1632, 610
  size: 79, 191
  orig: 79, 191
  offset: 0, 0
  index: -1
parts/upperFrontLegLeft
  rotate: true
  xy: 453, 413
  size: 97, 224
  orig: 97, 224
  offset: 0, 0
  index: -1
parts/upperFrontLegRight
  rotate: false
  xy: 1118, 609
  size: 97, 224
  orig: 97, 224
  offset: 0, 0
  index: -1
spa/spaEyes
  rotate: false
  xy: 1016, 835
  size: 233, 187
  orig: 233, 187
  offset: 0, 0
  index: -1
spa/spaHead
  rotate: true
  xy: 2, 54
  size: 349, 435
  orig: 349, 435
  offset: 0, 0
  index: -1
spa/spaMatt
  rotate: false
  xy: 453, 832
  size: 561, 190
  orig: 561, 190
  offset: 0, 0
  index: -1
spa/spaTowel
  rotate: false
  xy: 2, 405
  size: 449, 617
  orig: 449, 617
  offset: 0, 0
  index: -1
