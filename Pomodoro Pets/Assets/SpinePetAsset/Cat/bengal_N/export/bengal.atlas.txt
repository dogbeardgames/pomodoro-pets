
bengal.png
size: 2048,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
Swing/swingLeftChains
  rotate: true
  xy: 1359, 631
  size: 40, 598
  orig: 40, 598
  offset: 0, 0
  index: -1
Swing/swingLeftConnector
  rotate: true
  xy: 1549, 5
  size: 52, 281
  orig: 52, 281
  offset: 0, 0
  index: -1
Swing/swingRightConnector
  rotate: false
  xy: 1970, 741
  size: 52, 281
  orig: 52, 281
  offset: 0, 0
  index: -1
costumes/Catain/CatainBackRightLeg2
  rotate: true
  xy: 1238, 419
  size: 126, 83
  orig: 126, 83
  offset: 0, 0
  index: -1
costumes/Catain/CatainSleepLegFront
  rotate: true
  xy: 1958, 398
  size: 231, 88
  orig: 231, 88
  offset: 0, 0
  index: -1
costumes/Nonset/bowtie
  rotate: true
  xy: 1549, 59
  size: 87, 413
  orig: 87, 413
  offset: 0, 0
  index: -1
costumes/Nonset/flowerPink
  rotate: false
  xy: 1066, 2
  size: 481, 407
  orig: 481, 407
  offset: 0, 0
  index: -1
costumes/Nonset/flowerRed
  rotate: true
  xy: 1549, 148
  size: 481, 407
  orig: 481, 407
  offset: 0, 0
  index: -1
costumes/Nonset/reverseCap
  rotate: false
  xy: 639, 2
  size: 425, 383
  orig: 425, 383
  offset: 0, 0
  index: -1
costumes/Nonset/scarf
  rotate: true
  xy: 1359, 673
  size: 349, 609
  orig: 349, 609
  offset: 0, 0
  index: -1
costumes/Nonset/sleepPetCollar
  rotate: true
  xy: 675, 547
  size: 475, 682
  orig: 475, 682
  offset: 0, 0
  index: -1
costumes/Nonset/sleepScarf
  rotate: false
  xy: 2, 387
  size: 671, 635
  orig: 671, 635
  offset: 0, 0
  index: -1
parts/earRight
  rotate: true
  xy: 1359, 534
  size: 95, 184
  orig: 95, 184
  offset: 0, 0
  index: -1
parts/eyes
  rotate: false
  xy: 1832, 22
  size: 145, 35
  orig: 145, 35
  offset: 0, 0
  index: -1
parts/footBackLeft
  rotate: false
  xy: 1970, 697
  size: 76, 42
  orig: 76, 42
  offset: 0, 0
  index: -1
parts/footBackRight
  rotate: false
  xy: 1970, 653
  size: 76, 42
  orig: 76, 42
  offset: 0, 0
  index: -1
parts/footFrontLeft
  rotate: false
  xy: 1964, 83
  size: 61, 80
  orig: 61, 80
  offset: 0, 0
  index: -1
parts/mouthUp
  rotate: true
  xy: 1979, 13
  size: 68, 46
  orig: 68, 46
  offset: 0, 0
  index: -1
parts/nose
  rotate: true
  xy: 2027, 136
  size: 27, 19
  orig: 27, 19
  offset: 0, 0
  index: -1
parts/sleepLegFront
  rotate: true
  xy: 1958, 165
  size: 231, 88
  orig: 231, 88
  offset: 0, 0
  index: -1
parts/upperBackLegLeft
  rotate: true
  xy: 1323, 420
  size: 112, 194
  orig: 112, 194
  offset: 0, 0
  index: -1
spa/spaMatt
  rotate: false
  xy: 675, 411
  size: 561, 134
  orig: 561, 134
  offset: 0, 0
  index: -1
spa/spaTowel
  rotate: false
  xy: 2, 2
  size: 635, 383
  orig: 635, 383
  offset: 0, 0
  index: -1

bengal2.png
size: 2048,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
Swing/swingBack
  rotate: true
  xy: 2, 68
  size: 424, 321
  orig: 424, 321
  offset: 0, 0
  index: -1
Swing/swingFront
  rotate: false
  xy: 1239, 690
  size: 424, 321
  orig: 424, 321
  offset: 0, 0
  index: -1
Swing/swingRightChains
  rotate: true
  xy: 2, 26
  size: 40, 598
  orig: 40, 598
  offset: 0, 0
  index: -1
costumes/Catain/CatainBackLeftLeg
  rotate: false
  xy: 1864, 74
  size: 178, 243
  orig: 178, 243
  offset: 0, 0
  index: -1
costumes/Catain/CatainBackLeftLeg2
  rotate: false
  xy: 602, 13
  size: 122, 63
  orig: 122, 63
  offset: 0, 0
  index: -1
costumes/Catain/CatainBackRightLeg
  rotate: false
  xy: 1481, 368
  size: 166, 320
  orig: 166, 320
  offset: 0, 0
  index: -1
costumes/Catain/CatainFrontLeftLeg
  rotate: false
  xy: 1463, 20
  size: 94, 346
  orig: 94, 346
  offset: 0, 0
  index: -1
costumes/Catain/CatainFrontLeftLeg2
  rotate: true
  xy: 1864, 2
  size: 70, 124
  orig: 70, 124
  offset: 0, 0
  index: -1
costumes/Catain/CatainFrontRightLeg
  rotate: true
  xy: 341, 437
  size: 119, 351
  orig: 119, 351
  offset: 0, 0
  index: -1
costumes/Catain/CatainFrontRightLeg2
  rotate: true
  xy: 726, 12
  size: 64, 113
  orig: 64, 113
  offset: 0, 0
  index: -1
costumes/Catain/CatainLowerbody
  rotate: false
  xy: 1265, 18
  size: 196, 350
  orig: 196, 350
  offset: 0, 0
  index: -1
costumes/Catain/CatainMask
  rotate: false
  xy: 325, 115
  size: 308, 320
  orig: 308, 320
  offset: 0, 0
  index: -1
costumes/Catain/CatainSleepBody
  rotate: false
  xy: 1111, 370
  size: 368, 318
  orig: 368, 318
  offset: 0, 0
  index: -1
costumes/Catain/CatainUpperBody
  rotate: false
  xy: 1665, 639
  size: 372, 372
  orig: 372, 372
  offset: 0, 0
  index: -1
costumes/Nonset/fedoraHat
  rotate: false
  xy: 341, 558
  size: 367, 453
  orig: 367, 453
  offset: 0, 0
  index: -1
costumes/Nonset/geekGlasses
  rotate: false
  xy: 1559, 42
  size: 303, 275
  orig: 303, 275
  offset: 0, 0
  index: -1
costumes/Nonset/pandaHat
  rotate: true
  xy: 710, 377
  size: 339, 399
  orig: 339, 399
  offset: 0, 0
  index: -1
costumes/Nonset/petCollar
  rotate: true
  xy: 710, 718
  size: 293, 527
  orig: 293, 527
  offset: 0, 0
  index: -1
costumes/Nonset/shades
  rotate: true
  xy: 960, 79
  size: 289, 303
  orig: 289, 303
  offset: 0, 0
  index: -1
parts/footFrontRight
  rotate: true
  xy: 841, 15
  size: 61, 80
  orig: 61, 80
  offset: 0, 0
  index: -1
parts/head
  rotate: true
  xy: 635, 78
  size: 297, 323
  orig: 297, 323
  offset: 0, 0
  index: -1
parts/mouthDown
  rotate: false
  xy: 635, 383
  size: 38, 52
  orig: 38, 52
  offset: 0, 0
  index: -1
parts/sleepBody
  rotate: false
  xy: 1649, 319
  size: 368, 318
  orig: 368, 318
  offset: 0, 0
  index: -1
parts/tail1
  rotate: true
  xy: 1559, 324
  size: 42, 48
  orig: 42, 48
  offset: 0, 0
  index: -1
parts/tail10
  rotate: false
  xy: 367, 72
  size: 40, 41
  orig: 40, 41
  offset: 0, 0
  index: -1
parts/tail11
  rotate: false
  xy: 409, 73
  size: 41, 40
  orig: 41, 40
  offset: 0, 0
  index: -1
parts/tail2
  rotate: false
  xy: 452, 73
  size: 40, 40
  orig: 40, 40
  offset: 0, 0
  index: -1
parts/tail3
  rotate: false
  xy: 964, 37
  size: 39, 40
  orig: 39, 40
  offset: 0, 0
  index: -1
parts/tail4
  rotate: true
  xy: 494, 74
  size: 39, 41
  orig: 39, 41
  offset: 0, 0
  index: -1
parts/tail5
  rotate: true
  xy: 537, 74
  size: 39, 41
  orig: 39, 41
  offset: 0, 0
  index: -1
parts/tail6
  rotate: false
  xy: 1005, 37
  size: 39, 40
  orig: 39, 40
  offset: 0, 0
  index: -1
parts/tail7
  rotate: false
  xy: 923, 35
  size: 39, 41
  orig: 39, 41
  offset: 0, 0
  index: -1
parts/tail8
  rotate: false
  xy: 1990, 31
  size: 40, 41
  orig: 40, 41
  offset: 0, 0
  index: -1
parts/tail9
  rotate: false
  xy: 325, 72
  size: 40, 41
  orig: 40, 41
  offset: 0, 0
  index: -1
spa/spaHead
  rotate: false
  xy: 2, 494
  size: 337, 517
  orig: 337, 517
  offset: 0, 0
  index: -1

bengal3.png
size: 2048,256
format: RGBA8888
filter: Linear,Linear
repeat: none
costumes/Catain/CatainShield
  rotate: true
  xy: 544, 52
  size: 171, 243
  orig: 171, 243
  offset: 0, 0
  index: -1
costumes/Catain/CatainSleepLegBack
  rotate: false
  xy: 1246, 10
  size: 155, 99
  orig: 155, 99
  offset: 0, 0
  index: -1
parts/chest
  rotate: true
  xy: 2, 8
  size: 215, 301
  orig: 215, 301
  offset: 0, 0
  index: -1
parts/earLeft
  rotate: true
  xy: 1846, 130
  size: 93, 182
  orig: 93, 182
  offset: 0, 0
  index: -1
parts/lowerBody
  rotate: true
  xy: 1025, 37
  size: 186, 219
  orig: 186, 219
  offset: 0, 0
  index: -1
parts/sleepLegBack
  rotate: false
  xy: 1442, 33
  size: 155, 99
  orig: 155, 99
  offset: 0, 0
  index: -1
parts/sleepTail
  rotate: false
  xy: 789, 65
  size: 234, 158
  orig: 234, 158
  offset: 0, 0
  index: -1
parts/upperBackLegRight
  rotate: true
  xy: 1246, 111
  size: 112, 194
  orig: 112, 194
  offset: 0, 0
  index: -1
parts/upperFrontLegLeft
  rotate: true
  xy: 1442, 134
  size: 89, 200
  orig: 89, 200
  offset: 0, 0
  index: -1
parts/upperFrontLegRight
  rotate: true
  xy: 1644, 134
  size: 89, 200
  orig: 89, 200
  offset: 0, 0
  index: -1
spa/spaEyes
  rotate: false
  xy: 305, 2
  size: 237, 221
  orig: 237, 221
  offset: 0, 0
  index: -1
