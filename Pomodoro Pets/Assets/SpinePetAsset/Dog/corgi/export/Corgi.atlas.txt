
Corgi.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
parts/ear lft
  rotate: false
  xy: 2, 34
  size: 165, 181
  orig: 165, 181
  offset: 0, 0
  index: -1
parts/ear rght
  rotate: false
  xy: 787, 329
  size: 165, 181
  orig: 165, 181
  offset: 0, 0
  index: -1
parts/eyes
  rotate: true
  xy: 954, 368
  size: 142, 34
  orig: 142, 34
  offset: 0, 0
  index: -1
parts/head
  rotate: false
  xy: 2, 217
  size: 289, 293
  orig: 289, 293
  offset: 0, 0
  index: -1
parts/lower mouth
  rotate: false
  xy: 482, 219
  size: 39, 44
  orig: 39, 44
  offset: 0, 0
  index: -1
parts/m chest
  rotate: false
  xy: 293, 265
  size: 249, 245
  orig: 249, 245
  offset: 0, 0
  index: -1
parts/m foot back lft
  rotate: false
  xy: 446, 110
  size: 62, 70
  orig: 62, 70
  offset: 0, 0
  index: -1
parts/m foot back rght
  rotate: true
  xy: 787, 265
  size: 62, 70
  orig: 62, 70
  offset: 0, 0
  index: -1
parts/m foot front  rght
  rotate: true
  xy: 278, 2
  size: 69, 87
  orig: 69, 87
  offset: 0, 0
  index: -1
parts/m foot front lft
  rotate: true
  xy: 367, 2
  size: 69, 87
  orig: 69, 87
  offset: 0, 0
  index: -1
parts/m lower body
  rotate: false
  xy: 544, 294
  size: 241, 216
  orig: 241, 216
  offset: 0, 0
  index: -1
parts/m upper back leg lft
  rotate: true
  xy: 293, 182
  size: 81, 187
  orig: 81, 187
  offset: 0, 0
  index: -1
parts/m upper back leg rght
  rotate: true
  xy: 544, 211
  size: 81, 187
  orig: 81, 187
  offset: 0, 0
  index: -1
parts/m upper front leg lft
  rotate: false
  xy: 169, 49
  size: 107, 166
  orig: 107, 166
  offset: 0, 0
  index: -1
parts/m upper front leg rght
  rotate: true
  xy: 278, 73
  size: 107, 166
  orig: 107, 166
  offset: 0, 0
  index: -1
parts/nose
  rotate: false
  xy: 446, 75
  size: 48, 33
  orig: 48, 33
  offset: 0, 0
  index: -1
parts/upper mouthi
  rotate: true
  xy: 733, 202
  size: 90, 47
  orig: 90, 47
  offset: 0, 0
  index: -1
