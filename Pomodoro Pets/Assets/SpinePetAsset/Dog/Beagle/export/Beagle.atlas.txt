
Beagle.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
parts/ear lft
  rotate: false
  xy: 290, 67
  size: 222, 188
  orig: 222, 188
  offset: 0, 0
  index: -1
parts/ear rght
  rotate: true
  xy: 837, 276
  size: 234, 174
  orig: 234, 174
  offset: 0, 0
  index: -1
parts/eyes
  rotate: false
  xy: 609, 95
  size: 170, 47
  orig: 170, 47
  offset: 0, 0
  index: -1
parts/head
  rotate: false
  xy: 2, 141
  size: 286, 369
  orig: 286, 369
  offset: 0, 0
  index: -1
parts/m chest
  rotate: false
  xy: 290, 257
  size: 296, 253
  orig: 296, 253
  offset: 0, 0
  index: -1
parts/m foot back lft
  rotate: false
  xy: 702, 7
  size: 72, 86
  orig: 72, 86
  offset: 0, 0
  index: -1
parts/m foot back rght
  rotate: true
  xy: 781, 84
  size: 72, 85
  orig: 72, 85
  offset: 0, 0
  index: -1
parts/m foot front lft
  rotate: false
  xy: 173, 47
  size: 75, 92
  orig: 75, 92
  offset: 0, 0
  index: -1
parts/m foot front rght
  rotate: false
  xy: 776, 2
  size: 65, 80
  orig: 65, 80
  offset: 0, 0
  index: -1
parts/m lower body
  rotate: false
  xy: 588, 314
  size: 247, 196
  orig: 247, 196
  offset: 0, 0
  index: -1
parts/m upper back leg lft
  rotate: true
  xy: 2, 19
  size: 120, 169
  orig: 120, 169
  offset: 0, 0
  index: -1
parts/m upper back leg rght
  rotate: false
  xy: 609, 144
  size: 120, 168
  orig: 120, 168
  offset: 0, 0
  index: -1
parts/m upper front foot lft
  rotate: true
  xy: 825, 158
  size: 116, 163
  orig: 116, 163
  offset: 0, 0
  index: -1
parts/m upper leg front rght
  rotate: false
  xy: 514, 83
  size: 93, 172
  orig: 93, 172
  offset: 0, 0
  index: -1
parts/mouth
  rotate: true
  xy: 731, 171
  size: 141, 92
  orig: 141, 92
  offset: 0, 0
  index: -1
parts/mouth down
  rotate: false
  xy: 843, 32
  size: 47, 50
  orig: 47, 50
  offset: 0, 0
  index: -1
parts/nose
  rotate: false
  xy: 514, 13
  size: 96, 68
  orig: 96, 68
  offset: 0, 0
  index: -1
parts/tail 1
  rotate: true
  xy: 612, 15
  size: 78, 88
  orig: 78, 88
  offset: 0, 0
  index: -1
parts/tail 2
  rotate: false
  xy: 868, 88
  size: 68, 68
  orig: 68, 68
  offset: 0, 0
  index: -1
parts/tail 3
  rotate: false
  xy: 938, 88
  size: 67, 68
  orig: 67, 68
  offset: 0, 0
  index: -1
