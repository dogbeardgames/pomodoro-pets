
Corgi.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
parts/ear lft
  rotate: true
  xy: 656, 363
  size: 165, 181
  orig: 165, 181
  offset: 0, 0
  index: -1
parts/ear rght
  rotate: false
  xy: 345, 192
  size: 165, 181
  orig: 165, 181
  offset: 0, 0
  index: -1
parts/eyes
  rotate: true
  xy: 454, 48
  size: 142, 34
  orig: 142, 34
  offset: 0, 0
  index: -1
parts/head
  rotate: false
  xy: 413, 593
  size: 289, 293
  orig: 289, 293
  offset: 0, 0
  index: -1
parts/lower mouth
  rotate: false
  xy: 454, 2
  size: 39, 44
  orig: 39, 44
  offset: 0, 0
  index: -1
parts/m chest
  rotate: false
  xy: 704, 641
  size: 249, 245
  orig: 249, 245
  offset: 0, 0
  index: -1
parts/m foot back lft
  rotate: false
  xy: 539, 135
  size: 62, 70
  orig: 62, 70
  offset: 0, 0
  index: -1
parts/m foot back rght
  rotate: true
  xy: 810, 277
  size: 62, 70
  orig: 62, 70
  offset: 0, 0
  index: -1
parts/m foot front  rght
  rotate: true
  xy: 621, 209
  size: 69, 87
  orig: 69, 87
  offset: 0, 0
  index: -1
parts/m foot front lft
  rotate: true
  xy: 710, 209
  size: 69, 87
  orig: 69, 87
  offset: 0, 0
  index: -1
parts/m lower body
  rotate: false
  xy: 413, 375
  size: 241, 216
  orig: 241, 216
  offset: 0, 0
  index: -1
parts/m upper back leg lft
  rotate: false
  xy: 839, 341
  size: 81, 187
  orig: 81, 187
  offset: 0, 0
  index: -1
parts/m upper back leg rght
  rotate: true
  xy: 621, 280
  size: 81, 187
  orig: 81, 187
  offset: 0, 0
  index: -1
parts/m upper front leg lft
  rotate: false
  xy: 345, 24
  size: 107, 166
  orig: 107, 166
  offset: 0, 0
  index: -1
parts/m upper front leg rght
  rotate: false
  xy: 512, 207
  size: 107, 166
  orig: 107, 166
  offset: 0, 0
  index: -1
parts/nose
  rotate: true
  xy: 976, 974
  size: 48, 33
  orig: 48, 33
  offset: 0, 0
  index: -1
parts/upper mouthi
  rotate: true
  xy: 490, 100
  size: 90, 47
  orig: 90, 47
  offset: 0, 0
  index: -1
spa/spaEyes
  rotate: false
  xy: 704, 530
  size: 237, 109
  orig: 237, 109
  offset: 0, 0
  index: -1
spa/spaHead
  rotate: false
  xy: 2, 403
  size: 409, 619
  orig: 409, 619
  offset: 0, 0
  index: -1
spa/spaMatt
  rotate: false
  xy: 413, 888
  size: 561, 134
  orig: 561, 134
  offset: 0, 0
  index: -1
spa/spaTowel
  rotate: false
  xy: 2, 18
  size: 341, 383
  orig: 341, 383
  offset: 0, 0
  index: -1
