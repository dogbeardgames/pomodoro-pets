
Beagle.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
parts/ear lft
  rotate: false
  xy: 345, 129
  size: 222, 188
  orig: 222, 188
  offset: 0, 0
  index: -1
parts/ear rght
  rotate: false
  xy: 701, 414
  size: 234, 174
  orig: 234, 174
  offset: 0, 0
  index: -1
parts/eyes
  rotate: true
  xy: 956, 716
  size: 170, 47
  orig: 170, 47
  offset: 0, 0
  index: -1
parts/head
  rotate: false
  xy: 413, 517
  size: 286, 369
  orig: 286, 369
  offset: 0, 0
  index: -1
parts/m chest
  rotate: true
  xy: 701, 590
  size: 296, 253
  orig: 296, 253
  offset: 0, 0
  index: -1
parts/m foot back lft
  rotate: false
  xy: 814, 156
  size: 72, 86
  orig: 72, 86
  offset: 0, 0
  index: -1
parts/m foot back rght
  rotate: true
  xy: 937, 516
  size: 72, 85
  orig: 72, 85
  offset: 0, 0
  index: -1
parts/m foot front lft
  rotate: false
  xy: 937, 422
  size: 75, 92
  orig: 75, 92
  offset: 0, 0
  index: -1
parts/m foot front rght
  rotate: false
  xy: 345, 321
  size: 65, 80
  orig: 65, 80
  offset: 0, 0
  index: -1
parts/m lower body
  rotate: false
  xy: 413, 319
  size: 247, 196
  orig: 247, 196
  offset: 0, 0
  index: -1
parts/m upper back leg lft
  rotate: false
  xy: 664, 243
  size: 120, 169
  orig: 120, 169
  offset: 0, 0
  index: -1
parts/m upper back leg rght
  rotate: false
  xy: 786, 244
  size: 120, 168
  orig: 120, 168
  offset: 0, 0
  index: -1
parts/m upper front foot lft
  rotate: true
  xy: 678, 27
  size: 116, 163
  orig: 116, 163
  offset: 0, 0
  index: -1
parts/m upper leg front rght
  rotate: false
  xy: 569, 145
  size: 93, 172
  orig: 93, 172
  offset: 0, 0
  index: -1
parts/mouth
  rotate: true
  xy: 584, 2
  size: 141, 92
  orig: 141, 92
  offset: 0, 0
  index: -1
parts/mouth down
  rotate: false
  xy: 956, 664
  size: 47, 50
  orig: 47, 50
  offset: 0, 0
  index: -1
parts/nose
  rotate: true
  xy: 664, 145
  size: 96, 68
  orig: 96, 68
  offset: 0, 0
  index: -1
parts/tail 1
  rotate: false
  xy: 734, 153
  size: 78, 88
  orig: 78, 88
  offset: 0, 0
  index: -1
parts/tail 2
  rotate: false
  xy: 843, 86
  size: 68, 68
  orig: 68, 68
  offset: 0, 0
  index: -1
parts/tail 3
  rotate: true
  xy: 843, 17
  size: 67, 68
  orig: 67, 68
  offset: 0, 0
  index: -1
spa/spaEyes
  rotate: false
  xy: 345, 18
  size: 237, 109
  orig: 237, 109
  offset: 0, 0
  index: -1
spa/spaHead
  rotate: false
  xy: 2, 403
  size: 409, 619
  orig: 409, 619
  offset: 0, 0
  index: -1
spa/spaMatt
  rotate: false
  xy: 413, 888
  size: 561, 134
  orig: 561, 134
  offset: 0, 0
  index: -1
spa/spaTowel
  rotate: false
  xy: 2, 18
  size: 341, 383
  orig: 341, 383
  offset: 0, 0
  index: -1
