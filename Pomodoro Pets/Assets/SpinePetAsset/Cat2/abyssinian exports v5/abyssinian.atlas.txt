
abyssinian.png
size: 2048,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
parts/chest
  rotate: false
  xy: 2, 78
  size: 242, 228
  orig: 242, 228
  offset: 0, 0
  index: -1
parts/earLeft
  rotate: true
  xy: 517, 320
  size: 130, 224
  orig: 130, 224
  offset: 0, 0
  index: -1
parts/earRight
  rotate: true
  xy: 1788, 889
  size: 130, 224
  orig: 130, 224
  offset: 0, 0
  index: -1
parts/eyes
  rotate: false
  xy: 2, 43
  size: 134, 33
  orig: 134, 33
  offset: 0, 0
  index: -1
parts/footBackLeft
  rotate: false
  xy: 1392, 484
  size: 54, 79
  orig: 54, 79
  offset: 0, 0
  index: -1
parts/footBackRight
  rotate: false
  xy: 802, 371
  size: 54, 79
  orig: 54, 79
  offset: 0, 0
  index: -1
parts/footFrontLeft
  rotate: true
  xy: 1595, 587
  size: 57, 98
  orig: 57, 98
  offset: 0, 0
  index: -1
parts/footFrontRight
  rotate: false
  xy: 743, 344
  size: 57, 106
  orig: 57, 106
  offset: 0, 0
  index: -1
parts/head
  rotate: false
  xy: 2, 308
  size: 513, 711
  orig: 513, 711
  offset: 0, 0
  index: -1
parts/lowerBody
  rotate: false
  xy: 246, 126
  size: 163, 180
  orig: 163, 180
  offset: 0, 0
  index: -1
parts/mouthDown
  rotate: false
  xy: 2, 2
  size: 37, 39
  orig: 37, 39
  offset: 0, 0
  index: -1
parts/mouthUp
  rotate: false
  xy: 1796, 661
  size: 59, 36
  orig: 59, 36
  offset: 0, 0
  index: -1
parts/nose
  rotate: false
  xy: 882, 468
  size: 25, 16
  orig: 25, 16
  offset: 0, 0
  index: -1
parts/sleepBody
  rotate: false
  xy: 1225, 565
  size: 368, 318
  orig: 368, 318
  offset: 0, 0
  index: -1
parts/sleepLegBack
  rotate: true
  xy: 1117, 479
  size: 155, 99
  orig: 155, 99
  offset: 0, 0
  index: -1
parts/sleepLegFront
  rotate: true
  xy: 1706, 652
  size: 231, 88
  orig: 231, 88
  offset: 0, 0
  index: -1
parts/sleepTail
  rotate: false
  xy: 882, 486
  size: 233, 148
  orig: 233, 148
  offset: 0, 0
  index: -1
parts/tail1
  rotate: false
  xy: 1972, 793
  size: 45, 45
  orig: 45, 45
  offset: 0, 0
  index: -1
parts/tail2
  rotate: true
  xy: 246, 73
  size: 51, 53
  orig: 51, 53
  offset: 0, 0
  index: -1
parts/tail3
  rotate: false
  xy: 1448, 508
  size: 55, 55
  orig: 55, 55
  offset: 0, 0
  index: -1
parts/tail4
  rotate: false
  xy: 1505, 510
  size: 52, 53
  orig: 52, 53
  offset: 0, 0
  index: -1
parts/tail5
  rotate: false
  xy: 1972, 840
  size: 47, 47
  orig: 47, 47
  offset: 0, 0
  index: -1
parts/tail6
  rotate: false
  xy: 301, 73
  size: 53, 51
  orig: 53, 51
  offset: 0, 0
  index: -1
parts/tail7
  rotate: false
  xy: 1972, 747
  size: 44, 44
  orig: 44, 44
  offset: 0, 0
  index: -1
parts/tail8
  rotate: false
  xy: 356, 76
  size: 48, 48
  orig: 48, 48
  offset: 0, 0
  index: -1
parts/upperBackLegLeft
  rotate: false
  xy: 1796, 699
  size: 86, 188
  orig: 86, 188
  offset: 0, 0
  index: -1
parts/upperBackLegRight
  rotate: false
  xy: 1884, 699
  size: 86, 188
  orig: 86, 188
  offset: 0, 0
  index: -1
parts/upperFrontLegLeft
  rotate: true
  xy: 1218, 471
  size: 92, 172
  orig: 92, 172
  offset: 0, 0
  index: -1
parts/upperFrontLegRight
  rotate: false
  xy: 411, 128
  size: 92, 178
  orig: 92, 178
  offset: 0, 0
  index: -1
spa/spaEyes
  rotate: true
  xy: 1595, 646
  size: 237, 109
  orig: 237, 109
  offset: 0, 0
  index: -1
spa/spaHead
  rotate: false
  xy: 517, 452
  size: 363, 567
  orig: 363, 567
  offset: 0, 0
  index: -1
spa/spaMatt
  rotate: false
  xy: 1225, 885
  size: 561, 134
  orig: 561, 134
  offset: 0, 0
  index: -1
spa/spaTowel
  rotate: false
  xy: 882, 636
  size: 341, 383
  orig: 341, 383
  offset: 0, 0
  index: -1
