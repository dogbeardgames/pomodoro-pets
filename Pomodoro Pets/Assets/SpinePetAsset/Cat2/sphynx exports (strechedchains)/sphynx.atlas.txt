
sphynx.png
size: 2048,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
parts/chest
  rotate: false
  xy: 1089, 568
  size: 197, 290
  orig: 197, 290
  offset: 0, 0
  index: -1
parts/earLeft
  rotate: true
  xy: 453, 399
  size: 118, 234
  orig: 118, 234
  offset: 0, 0
  index: -1
parts/earRight
  rotate: true
  xy: 372, 114
  size: 118, 234
  orig: 118, 234
  offset: 0, 0
  index: -1
parts/eyes
  rotate: false
  xy: 2, 53
  size: 131, 32
  orig: 131, 32
  offset: 0, 0
  index: -1
parts/face
  rotate: true
  xy: 1363, 897
  size: 125, 299
  orig: 125, 299
  offset: 0, 0
  index: -1
parts/footBackLeft
  rotate: true
  xy: 2, 2
  size: 49, 40
  orig: 49, 40
  offset: 0, 0
  index: -1
parts/footBackRight
  rotate: true
  xy: 44, 2
  size: 49, 40
  orig: 49, 40
  offset: 0, 0
  index: -1
parts/footFrontLeft
  rotate: true
  xy: 1288, 568
  size: 57, 78
  orig: 57, 78
  offset: 0, 0
  index: -1
parts/footFrontRight
  rotate: false
  xy: 689, 439
  size: 57, 78
  orig: 57, 78
  offset: 0, 0
  index: -1
parts/head
  rotate: false
  xy: 802, 559
  size: 285, 299
  orig: 285, 299
  offset: 0, 0
  index: -1
parts/lowerBody
  rotate: false
  xy: 1378, 715
  size: 163, 180
  orig: 163, 180
  offset: 0, 0
  index: -1
parts/mouthDown
  rotate: false
  xy: 1021, 518
  size: 40, 39
  orig: 40, 39
  offset: 0, 0
  index: -1
parts/mouthUp
  rotate: false
  xy: 689, 399
  size: 58, 38
  orig: 58, 38
  offset: 0, 0
  index: -1
parts/nose
  rotate: false
  xy: 701, 259
  size: 31, 16
  orig: 31, 16
  offset: 0, 0
  index: -1
parts/sleepBody
  rotate: false
  xy: 2, 87
  size: 368, 318
  orig: 368, 318
  offset: 0, 0
  index: -1
parts/sleepLegBack
  rotate: true
  xy: 1909, 867
  size: 155, 99
  orig: 155, 99
  offset: 0, 0
  index: -1
parts/sleepLegFront
  rotate: true
  xy: 1288, 627
  size: 231, 88
  orig: 231, 88
  offset: 0, 0
  index: -1
parts/sleepTail
  rotate: false
  xy: 372, 234
  size: 233, 163
  orig: 233, 163
  offset: 0, 0
  index: -1
parts/tail1
  rotate: false
  xy: 86, 4
  size: 41, 47
  orig: 41, 47
  offset: 0, 0
  index: -1
parts/tail2
  rotate: true
  xy: 802, 514
  size: 43, 57
  orig: 43, 57
  offset: 0, 0
  index: -1
parts/tail3
  rotate: false
  xy: 748, 455
  size: 46, 62
  orig: 46, 62
  offset: 0, 0
  index: -1
parts/tail4
  rotate: false
  xy: 701, 337
  size: 44, 60
  orig: 44, 60
  offset: 0, 0
  index: -1
parts/tail5
  rotate: true
  xy: 918, 517
  size: 40, 51
  orig: 40, 51
  offset: 0, 0
  index: -1
parts/tail6
  rotate: false
  xy: 701, 277
  size: 44, 58
  orig: 44, 58
  offset: 0, 0
  index: -1
parts/tail7
  rotate: true
  xy: 971, 520
  size: 37, 48
  orig: 37, 48
  offset: 0, 0
  index: -1
parts/tail8
  rotate: true
  xy: 861, 519
  size: 38, 55
  orig: 38, 55
  offset: 0, 0
  index: -1
parts/upperBackLegLeft
  rotate: false
  xy: 607, 259
  size: 92, 138
  orig: 92, 138
  offset: 0, 0
  index: -1
parts/upperBackLegRight
  rotate: false
  xy: 608, 119
  size: 92, 138
  orig: 92, 138
  offset: 0, 0
  index: -1
parts/upperFrontLegLeft
  rotate: false
  xy: 1543, 724
  size: 92, 171
  orig: 92, 171
  offset: 0, 0
  index: -1
parts/upperFrontLegRight
  rotate: true
  xy: 1378, 621
  size: 92, 171
  orig: 92, 171
  offset: 0, 0
  index: -1
spa/spaEyes
  rotate: false
  xy: 1664, 881
  size: 243, 141
  orig: 243, 141
  offset: 0, 0
  index: -1
spa/spaHead
  rotate: false
  xy: 453, 519
  size: 347, 503
  orig: 347, 503
  offset: 0, 0
  index: -1
spa/spaMatt
  rotate: false
  xy: 802, 860
  size: 559, 162
  orig: 559, 162
  offset: 0, 0
  index: -1
spa/spaTowel
  rotate: false
  xy: 2, 407
  size: 449, 615
  orig: 449, 615
  offset: 0, 0
  index: -1
