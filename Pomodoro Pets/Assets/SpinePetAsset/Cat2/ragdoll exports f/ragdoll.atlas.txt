
ragdoll.png
size: 2048,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
parts/eyes
  rotate: false
  xy: 312, 35
  size: 146, 33
  orig: 146, 33
  offset: 0, 0
  index: -1
parts/face
  rotate: false
  xy: 1098, 36
  size: 235, 199
  orig: 235, 199
  offset: 0, 0
  index: -1
parts/footBackLeft
  rotate: true
  xy: 1335, 132
  size: 103, 71
  orig: 103, 71
  offset: 0, 0
  index: -1
parts/footBackRight
  rotate: true
  xy: 1335, 27
  size: 103, 71
  orig: 103, 71
  offset: 0, 0
  index: -1
parts/head
  rotate: true
  xy: 1410, 5
  size: 377, 321
  orig: 377, 321
  offset: 0, 0
  index: -1
parts/lowerBody
  rotate: false
  xy: 1733, 33
  size: 296, 349
  orig: 296, 349
  offset: 0, 0
  index: -1
parts/mouthDown
  rotate: false
  xy: 529, 20
  size: 42, 48
  orig: 42, 48
  offset: 0, 0
  index: -1
parts/mouthUp
  rotate: false
  xy: 460, 18
  size: 67, 50
  orig: 67, 50
  offset: 0, 0
  index: -1
parts/nose
  rotate: true
  xy: 1733, 2
  size: 29, 24
  orig: 29, 24
  offset: 0, 0
  index: -1
parts/sleepLeftLegFront
  rotate: false
  xy: 2, 31
  size: 308, 204
  orig: 308, 204
  offset: 0, 0
  index: -1
parts/tail1
  rotate: true
  xy: 637, 12
  size: 223, 234
  orig: 223, 234
  offset: 0, 0
  index: -1
parts/tail4
  rotate: true
  xy: 873, 9
  size: 226, 223
  orig: 226, 223
  offset: 0, 0
  index: -1
parts/upperLegFrontRight
  rotate: true
  xy: 312, 70
  size: 165, 323
  orig: 165, 323
  offset: 0, 0
  index: -1
spa/spaMatt
  rotate: false
  xy: 2, 237
  size: 1406, 785
  orig: 1406, 785
  offset: 0, 0
  index: -1
spa/spaTowel
  rotate: true
  xy: 1410, 384
  size: 638, 634
  orig: 638, 634
  offset: 0, 0
  index: -1

ragdoll2.png
size: 2048,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
parts/chest
  rotate: false
  xy: 978, 316
  size: 378, 271
  orig: 378, 271
  offset: 0, 0
  index: -1
parts/earLeft
  rotate: true
  xy: 767, 99
  size: 154, 210
  orig: 154, 210
  offset: 0, 0
  index: -1
parts/earRight
  rotate: false
  xy: 1225, 104
  size: 154, 210
  orig: 154, 210
  offset: 0, 0
  index: -1
parts/footFrontLeft
  rotate: false
  xy: 1948, 887
  size: 83, 111
  orig: 83, 111
  offset: 0, 0
  index: -1
parts/footFrontRight
  rotate: true
  xy: 2, 2
  size: 83, 111
  orig: 83, 111
  offset: 0, 0
  index: -1
parts/sleepBubble
  rotate: false
  xy: 555, 530
  size: 421, 468
  orig: 421, 468
  offset: 0, 0
  index: -1
parts/sleepHead
  rotate: false
  xy: 2, 87
  size: 496, 427
  orig: 496, 427
  offset: 0, 0
  index: -1
parts/sleepLeftLegBack
  rotate: true
  xy: 767, 255
  size: 273, 171
  orig: 273, 171
  offset: 0, 0
  index: -1
parts/sleepMainBody
  rotate: false
  xy: 978, 589
  size: 476, 409
  orig: 476, 409
  offset: 0, 0
  index: -1
parts/sleepRightLegBack
  rotate: true
  xy: 1624, 139
  size: 252, 254
  orig: 252, 254
  offset: 0, 0
  index: -1
parts/sleepRightLegFront
  rotate: false
  xy: 500, 6
  size: 212, 141
  orig: 212, 141
  offset: 0, 0
  index: -1
parts/sleepTail
  rotate: false
  xy: 1456, 661
  size: 339, 337
  orig: 339, 337
  offset: 0, 0
  index: -1
parts/tail2
  rotate: true
  xy: 1358, 318
  size: 269, 264
  orig: 269, 264
  offset: 0, 0
  index: -1
parts/tail3
  rotate: true
  xy: 1624, 393
  size: 266, 264
  orig: 266, 264
  offset: 0, 0
  index: -1
parts/upperLegBackLeft
  rotate: false
  xy: 1381, 72
  size: 172, 244
  orig: 172, 244
  offset: 0, 0
  index: -1
parts/upperLegBackRight
  rotate: true
  xy: 979, 139
  size: 175, 244
  orig: 175, 244
  offset: 0, 0
  index: -1
parts/upperLegFrontLeft
  rotate: false
  xy: 1797, 679
  size: 149, 319
  orig: 149, 319
  offset: 0, 0
  index: -1
spa/spaEyes
  rotate: false
  xy: 500, 149
  size: 265, 365
  orig: 265, 365
  offset: 0, 0
  index: -1
spa/spaHead
  rotate: false
  xy: 2, 516
  size: 551, 482
  orig: 551, 482
  offset: 0, 0
  index: -1
