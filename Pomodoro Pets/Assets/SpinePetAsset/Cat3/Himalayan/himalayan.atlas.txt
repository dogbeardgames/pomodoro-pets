
himalayan.png
size: 2048,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
parts/eyes
  rotate: true
  xy: 2002, 876
  size: 146, 33
  orig: 146, 33
  offset: 0, 0
  index: -1
parts/footBackLeft
  rotate: true
  xy: 1844, 25
  size: 103, 71
  orig: 103, 71
  offset: 0, 0
  index: -1
parts/footBackRight
  rotate: true
  xy: 1917, 25
  size: 103, 71
  orig: 103, 71
  offset: 0, 0
  index: -1
parts/footFrontLeft
  rotate: true
  xy: 1044, 13
  size: 70, 85
  orig: 70, 85
  offset: 0, 0
  index: -1
parts/footFrontRight
  rotate: true
  xy: 953, 12
  size: 71, 89
  orig: 71, 89
  offset: 0, 0
  index: -1
parts/mouthDown
  rotate: false
  xy: 2002, 826
  size: 42, 48
  orig: 42, 48
  offset: 0, 0
  index: -1
parts/mouthUp
  rotate: true
  xy: 1990, 61
  size: 67, 50
  orig: 67, 50
  offset: 0, 0
  index: -1
parts/nose
  rotate: false
  xy: 2002, 800
  size: 29, 24
  orig: 29, 24
  offset: 0, 0
  index: -1
parts/sleepLeftLegBack
  rotate: false
  xy: 953, 85
  size: 257, 152
  orig: 257, 152
  offset: 0, 0
  index: -1
parts/sleepLeftLegFront
  rotate: true
  xy: 1844, 130
  size: 282, 177
  orig: 282, 177
  offset: 0, 0
  index: -1
parts/sleepMainBody
  rotate: false
  xy: 1366, 3
  size: 476, 409
  orig: 476, 409
  offset: 0, 0
  index: -1
parts/sleepRightLegBack
  rotate: false
  xy: 2, 2
  size: 234, 235
  orig: 234, 235
  offset: 0, 0
  index: -1
parts/tail1
  rotate: false
  xy: 238, 6
  size: 221, 231
  orig: 221, 231
  offset: 0, 0
  index: -1
parts/tail3
  rotate: true
  xy: 1212, 13
  size: 224, 143
  orig: 224, 143
  offset: 0, 0
  index: -1
parts/upperLegBackLeft
  rotate: true
  xy: 707, 65
  size: 172, 244
  orig: 172, 244
  offset: 0, 0
  index: -1
parts/upperLegBackRight
  rotate: true
  xy: 461, 62
  size: 175, 244
  orig: 175, 244
  offset: 0, 0
  index: -1
spa/spaMatt
  rotate: false
  xy: 2, 239
  size: 1362, 783
  orig: 1362, 783
  offset: 0, 0
  index: -1
spa/spaTowel
  rotate: false
  xy: 1366, 414
  size: 634, 608
  orig: 634, 608
  offset: 0, 0
  index: -1

himalayan2.png
size: 2048,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
parts/earLeft
  rotate: false
  xy: 481, 2
  size: 127, 143
  orig: 127, 143
  offset: 0, 0
  index: -1
parts/earRight
  rotate: false
  xy: 610, 2
  size: 127, 143
  orig: 127, 143
  offset: 0, 0
  index: -1
parts/face
  rotate: true
  xy: 1293, 450
  size: 241, 346
  orig: 241, 346
  offset: 0, 0
  index: -1
parts/head
  rotate: false
  xy: 920, 601
  size: 371, 421
  orig: 371, 421
  offset: 0, 0
  index: -1
parts/lowerBody
  rotate: true
  xy: 1627, 726
  size: 296, 349
  orig: 296, 349
  offset: 0, 0
  index: -1
parts/sleepBubble
  rotate: false
  xy: 505, 548
  size: 413, 474
  orig: 413, 474
  offset: 0, 0
  index: -1
parts/sleepHead
  rotate: false
  xy: 2, 95
  size: 477, 443
  orig: 477, 443
  offset: 0, 0
  index: -1
parts/sleepRightLegFront
  rotate: false
  xy: 1529, 276
  size: 205, 141
  orig: 205, 141
  offset: 0, 0
  index: -1
parts/sleepTail
  rotate: false
  xy: 1293, 693
  size: 332, 329
  orig: 332, 329
  offset: 0, 0
  index: -1
parts/tail2
  rotate: false
  xy: 1245, 206
  size: 282, 242
  orig: 282, 242
  offset: 0, 0
  index: -1
parts/upperBody
  rotate: false
  xy: 920, 339
  size: 323, 260
  orig: 323, 260
  offset: 0, 0
  index: -1
parts/upperFrontLegLeft
  rotate: true
  xy: 1736, 284
  size: 133, 189
  orig: 133, 189
  offset: 0, 0
  index: -1
parts/upperFrontLegRight
  rotate: true
  xy: 1529, 147
  size: 127, 181
  orig: 127, 181
  offset: 0, 0
  index: -1
spa/spaEyes
  rotate: true
  xy: 1641, 419
  size: 305, 335
  orig: 305, 335
  offset: 0, 0
  index: -1
spa/spaHead
  rotate: false
  xy: 2, 540
  size: 501, 482
  orig: 501, 482
  offset: 0, 0
  index: -1
