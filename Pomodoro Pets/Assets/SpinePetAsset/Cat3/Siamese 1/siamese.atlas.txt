
siamese.png
size: 2048,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
parts/chest
  rotate: false
  xy: 453, 156
  size: 263, 238
  orig: 263, 238
  offset: 0, 0
  index: -1
parts/earLeft
  rotate: true
  xy: 453, 2
  size: 152, 178
  orig: 152, 178
  offset: 0, 0
  index: -1
parts/earRight
  rotate: true
  xy: 633, 2
  size: 152, 178
  orig: 152, 178
  offset: 0, 0
  index: -1
parts/eyes
  rotate: false
  xy: 1065, 511
  size: 146, 33
  orig: 146, 33
  offset: 0, 0
  index: -1
parts/face
  rotate: false
  xy: 1443, 716
  size: 293, 306
  orig: 293, 306
  offset: 0, 0
  index: -1
parts/footBackLeft
  rotate: true
  xy: 813, 16
  size: 57, 72
  orig: 57, 72
  offset: 0, 0
  index: -1
parts/footBackRight
  rotate: false
  xy: 887, 3
  size: 57, 72
  orig: 57, 72
  offset: 0, 0
  index: -1
parts/footFrontLeft
  rotate: false
  xy: 813, 75
  size: 65, 84
  orig: 65, 84
  offset: 0, 0
  index: -1
parts/footFrontRight
  rotate: false
  xy: 880, 77
  size: 65, 84
  orig: 65, 84
  offset: 0, 0
  index: -1
parts/head
  rotate: false
  xy: 1738, 716
  size: 293, 306
  orig: 293, 306
  offset: 0, 0
  index: -1
parts/lowerBody
  rotate: false
  xy: 823, 495
  size: 240, 219
  orig: 240, 219
  offset: 0, 0
  index: -1
parts/mouthDown
  rotate: false
  xy: 1401, 589
  size: 42, 51
  orig: 42, 51
  offset: 0, 0
  index: -1
parts/mouthUp
  rotate: true
  xy: 1401, 642
  size: 67, 50
  orig: 67, 50
  offset: 0, 0
  index: -1
parts/nose
  rotate: false
  xy: 421, 105
  size: 29, 24
  orig: 29, 24
  offset: 0, 0
  index: -1
parts/sleepBody
  rotate: false
  xy: 453, 396
  size: 368, 318
  orig: 368, 318
  offset: 0, 0
  index: -1
parts/sleepLegBack
  rotate: true
  xy: 1300, 554
  size: 155, 99
  orig: 155, 99
  offset: 0, 0
  index: -1
parts/sleepLegFront
  rotate: true
  xy: 861, 163
  size: 231, 88
  orig: 231, 88
  offset: 0, 0
  index: -1
parts/sleepTail
  rotate: false
  xy: 1065, 546
  size: 233, 163
  orig: 233, 163
  offset: 0, 0
  index: -1
parts/tail1
  rotate: false
  xy: 228, 2
  size: 39, 46
  orig: 39, 46
  offset: 0, 0
  index: -1
parts/tail10
  rotate: true
  xy: 311, 9
  size: 39, 41
  orig: 39, 41
  offset: 0, 0
  index: -1
parts/tail11
  rotate: false
  xy: 1576, 675
  size: 40, 39
  orig: 40, 39
  offset: 0, 0
  index: -1
parts/tail2
  rotate: true
  xy: 951, 163
  size: 38, 45
  orig: 38, 45
  offset: 0, 0
  index: -1
parts/tail3
  rotate: true
  xy: 354, 9
  size: 39, 40
  orig: 39, 40
  offset: 0, 0
  index: -1
parts/tail4
  rotate: false
  xy: 1453, 674
  size: 39, 40
  orig: 39, 40
  offset: 0, 0
  index: -1
parts/tail5
  rotate: false
  xy: 1494, 674
  size: 39, 40
  orig: 39, 40
  offset: 0, 0
  index: -1
parts/tail6
  rotate: false
  xy: 1535, 674
  size: 39, 40
  orig: 39, 40
  offset: 0, 0
  index: -1
parts/tail7
  rotate: false
  xy: 947, 121
  size: 40, 40
  orig: 40, 40
  offset: 0, 0
  index: -1
parts/tail8
  rotate: false
  xy: 269, 8
  size: 40, 40
  orig: 40, 40
  offset: 0, 0
  index: -1
parts/tail9
  rotate: false
  xy: 947, 78
  size: 39, 41
  orig: 39, 41
  offset: 0, 0
  index: -1
parts/upperBackLegLeft
  rotate: true
  xy: 228, 50
  size: 79, 191
  orig: 79, 191
  offset: 0, 0
  index: -1
parts/upperBackLegRight
  rotate: false
  xy: 951, 203
  size: 79, 191
  orig: 79, 191
  offset: 0, 0
  index: -1
parts/upperFrontLegLeft
  rotate: true
  xy: 823, 396
  size: 97, 224
  orig: 97, 224
  offset: 0, 0
  index: -1
parts/upperFrontLegRight
  rotate: true
  xy: 2, 32
  size: 97, 224
  orig: 97, 224
  offset: 0, 0
  index: -1
spa/spaEyes
  rotate: true
  xy: 718, 161
  size: 233, 141
  orig: 233, 141
  offset: 0, 0
  index: -1
spa/spaHead
  rotate: false
  xy: 1086, 711
  size: 355, 311
  orig: 355, 311
  offset: 0, 0
  index: -1
spa/spaMatt
  rotate: false
  xy: 453, 716
  size: 631, 306
  orig: 631, 306
  offset: 0, 0
  index: -1
spa/spaTowel
  rotate: false
  xy: 2, 131
  size: 449, 891
  orig: 449, 891
  offset: 0, 0
  index: -1
