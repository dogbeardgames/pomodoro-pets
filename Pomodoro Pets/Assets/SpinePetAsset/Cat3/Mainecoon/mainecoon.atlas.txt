
mainecoon.png
size: 2048,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
parts/eyes
  rotate: false
  xy: 497, 20
  size: 144, 36
  orig: 144, 36
  offset: 0, 0
  index: -1
parts/footBackLeft
  rotate: false
  xy: 1938, 973
  size: 90, 49
  orig: 90, 49
  offset: 0, 0
  index: -1
parts/footBackRight
  rotate: false
  xy: 1780, 74
  size: 90, 49
  orig: 90, 49
  offset: 0, 0
  index: -1
parts/footFrontLeft
  rotate: false
  xy: 1938, 897
  size: 63, 74
  orig: 63, 74
  offset: 0, 0
  index: -1
parts/footFrontRight
  rotate: true
  xy: 1780, 9
  size: 63, 74
  orig: 63, 74
  offset: 0, 0
  index: -1
parts/lowerBody
  rotate: false
  xy: 238, 12
  size: 257, 225
  orig: 257, 225
  offset: 0, 0
  index: -1
parts/mouthDown
  rotate: false
  xy: 1938, 854
  size: 42, 41
  orig: 42, 41
  offset: 0, 0
  index: -1
parts/mouthUp
  rotate: false
  xy: 1872, 82
  size: 64, 41
  orig: 64, 41
  offset: 0, 0
  index: -1
parts/nose
  rotate: false
  xy: 1260, 217
  size: 27, 20
  orig: 27, 20
  offset: 0, 0
  index: -1
parts/sleepLeftLegBack
  rotate: false
  xy: 781, 72
  size: 274, 165
  orig: 274, 165
  offset: 0, 0
  index: -1
parts/sleepLeftLegFront
  rotate: false
  xy: 497, 58
  size: 282, 179
  orig: 282, 179
  offset: 0, 0
  index: -1
parts/sleepMainBody
  rotate: false
  xy: 1302, 3
  size: 476, 409
  orig: 476, 409
  offset: 0, 0
  index: -1
parts/sleepRightLegBack
  rotate: false
  xy: 2, 2
  size: 234, 235
  orig: 234, 235
  offset: 0, 0
  index: -1
parts/tail2
  rotate: false
  xy: 1780, 125
  size: 252, 287
  orig: 252, 287
  offset: 0, 0
  index: -1
parts/tail3
  rotate: true
  xy: 1057, 61
  size: 176, 201
  orig: 176, 201
  offset: 0, 0
  index: -1
spa/spaMatt
  rotate: false
  xy: 2, 239
  size: 1298, 783
  orig: 1298, 783
  offset: 0, 0
  index: -1
spa/spaTowel
  rotate: false
  xy: 1302, 414
  size: 634, 608
  orig: 634, 608
  offset: 0, 0
  index: -1

mainecoon2.png
size: 2048,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
parts/chest
  rotate: false
  xy: 1590, 658
  size: 345, 275
  orig: 345, 275
  offset: 0, 0
  index: -1
parts/earLeft
  rotate: false
  xy: 1118, 403
  size: 164, 179
  orig: 164, 179
  offset: 0, 0
  index: -1
parts/earRight
  rotate: true
  xy: 948, 226
  size: 164, 179
  orig: 164, 179
  offset: 0, 0
  index: -1
parts/head
  rotate: false
  xy: 1285, 600
  size: 303, 333
  orig: 303, 333
  offset: 0, 0
  index: -1
parts/sleepBubble
  rotate: false
  xy: 2, 3
  size: 413, 474
  orig: 413, 474
  offset: 0, 0
  index: -1
parts/sleepHead
  rotate: false
  xy: 2, 479
  size: 510, 454
  orig: 510, 454
  offset: 0, 0
  index: -1
parts/sleepRightLegFront
  rotate: true
  xy: 1129, 196
  size: 205, 141
  orig: 205, 141
  offset: 0, 0
  index: -1
parts/sleepTail
  rotate: false
  xy: 945, 584
  size: 338, 349
  orig: 338, 349
  offset: 0, 0
  index: -1
parts/tail1
  rotate: true
  xy: 698, 254
  size: 259, 248
  orig: 259, 248
  offset: 0, 0
  index: -1
parts/upperBackLegLeft
  rotate: true
  xy: 417, 2
  size: 168, 190
  orig: 168, 190
  offset: 0, 0
  index: -1
parts/upperBackLegRight
  rotate: false
  xy: 948, 392
  size: 168, 190
  orig: 168, 190
  offset: 0, 0
  index: -1
parts/upperFrontLegLeft
  rotate: true
  xy: 698, 123
  size: 129, 186
  orig: 129, 186
  offset: 0, 0
  index: -1
parts/upperFrontLegRight
  rotate: true
  xy: 1590, 527
  size: 129, 186
  orig: 129, 186
  offset: 0, 0
  index: -1
spa/spaEyes
  rotate: true
  xy: 417, 172
  size: 305, 279
  orig: 305, 279
  offset: 0, 0
  index: -1
spa/spaHead
  rotate: false
  xy: 514, 515
  size: 429, 418
  orig: 429, 418
  offset: 0, 0
  index: -1
