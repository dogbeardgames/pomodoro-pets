
Beagle.png
size: 2048,512
format: RGBA8888
filter: Linear,Linear
repeat: none
parts/ear lft
  rotate: false
  xy: 727, 141
  size: 186, 157
  orig: 186, 157
  offset: 0, 0
  index: -1
parts/ear rght
  rotate: false
  xy: 529, 152
  size: 196, 146
  orig: 196, 146
  offset: 0, 0
  index: -1
parts/eyes
  rotate: false
  xy: 70, 17
  size: 143, 40
  orig: 143, 40
  offset: 0, 0
  index: -1
parts/head
  rotate: true
  xy: 1098, 271
  size: 239, 309
  orig: 239, 309
  offset: 0, 0
  index: -1
parts/m chest
  rotate: false
  xy: 1682, 272
  size: 248, 238
  orig: 248, 238
  offset: 0, 0
  index: -1
parts/m foot back lft
  rotate: false
  xy: 377, 13
  size: 60, 71
  orig: 60, 71
  offset: 0, 0
  index: -1
parts/m foot back rght
  rotate: false
  xy: 1932, 286
  size: 60, 81
  orig: 60, 81
  offset: 0, 0
  index: -1
parts/m foot front lft
  rotate: true
  xy: 215, 21
  size: 63, 78
  orig: 63, 78
  offset: 0, 0
  index: -1
parts/m foot front rght
  rotate: true
  xy: 2, 2
  size: 55, 66
  orig: 55, 66
  offset: 0, 0
  index: -1
parts/m lower body
  rotate: false
  xy: 2, 59
  size: 207, 164
  orig: 207, 164
  offset: 0, 0
  index: -1
parts/m upper back leg lft
  rotate: false
  xy: 1932, 369
  size: 101, 141
  orig: 101, 141
  offset: 0, 0
  index: -1
parts/m upper back leg rght
  rotate: false
  xy: 915, 157
  size: 101, 141
  orig: 101, 141
  offset: 0, 0
  index: -1
parts/m upper front foot lft
  rotate: true
  xy: 1098, 172
  size: 97, 136
  orig: 97, 136
  offset: 0, 0
  index: -1
parts/m upper leg front rght
  rotate: false
  xy: 1018, 155
  size: 78, 143
  orig: 78, 143
  offset: 0, 0
  index: -1
parts/mouth
  rotate: true
  xy: 432, 105
  size: 118, 77
  orig: 118, 77
  offset: 0, 0
  index: -1
parts/mouth down
  rotate: false
  xy: 1994, 317
  size: 47, 50
  orig: 47, 50
  offset: 0, 0
  index: -1
parts/nose
  rotate: false
  xy: 295, 27
  size: 80, 57
  orig: 80, 57
  offset: 0, 0
  index: -1
parts/tail
  rotate: true
  xy: 1236, 175
  size: 94, 138
  orig: 94, 138
  offset: 0, 0
  index: -1
spa/spaEyes
  rotate: false
  xy: 211, 86
  size: 219, 137
  orig: 219, 137
  offset: 0, 0
  index: -1
spa/spaHead
  rotate: true
  xy: 2, 225
  size: 285, 525
  orig: 285, 525
  offset: 0, 0
  index: -1
spa/spaMatt
  rotate: false
  xy: 529, 300
  size: 567, 210
  orig: 567, 210
  offset: 0, 0
  index: -1
spa/spaTowel
  rotate: false
  xy: 1409, 245
  size: 271, 265
  orig: 271, 265
  offset: 0, 0
  index: -1
