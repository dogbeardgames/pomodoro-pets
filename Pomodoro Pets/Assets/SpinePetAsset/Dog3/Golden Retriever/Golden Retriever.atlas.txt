
Golden Retriever.png
size: 2048,512
format: RGBA8888
filter: Linear,Linear
repeat: none
parts/ear lft
  rotate: true
  xy: 565, 14
  size: 153, 229
  orig: 153, 229
  offset: 0, 0
  index: -1
parts/ear lrght
  rotate: true
  xy: 1698, 158
  size: 153, 229
  orig: 153, 229
  offset: 0, 0
  index: -1
parts/eyes copy
  rotate: true
  xy: 1944, 359
  size: 151, 40
  orig: 151, 40
  offset: 0, 0
  index: -1
parts/head
  rotate: true
  xy: 1008, 247
  size: 263, 374
  orig: 263, 374
  offset: 0, 0
  index: -1
parts/m chest
  rotate: false
  xy: 1384, 268
  size: 312, 242
  orig: 312, 242
  offset: 0, 0
  index: -1
parts/m foot back lft
  rotate: true
  xy: 1164, 52
  size: 82, 48
  orig: 82, 48
  offset: 0, 0
  index: -1
parts/m foot back rght
  rotate: false
  xy: 1164, 2
  size: 82, 48
  orig: 82, 48
  offset: 0, 0
  index: -1
parts/m foot front lft
  rotate: false
  xy: 1608, 163
  size: 78, 103
  orig: 78, 103
  offset: 0, 0
  index: -1
parts/m foot front rght
  rotate: false
  xy: 1084, 31
  size: 78, 103
  orig: 78, 103
  offset: 0, 0
  index: -1
parts/m lower body
  rotate: false
  xy: 1247, 55
  size: 201, 190
  orig: 201, 190
  offset: 0, 0
  index: -1
parts/m upper back leg lft
  rotate: false
  xy: 796, 14
  size: 156, 153
  orig: 156, 153
  offset: 0, 0
  index: -1
parts/m upper back leg rght
  rotate: false
  xy: 1450, 113
  size: 156, 153
  orig: 156, 153
  offset: 0, 0
  index: -1
parts/m upper leg lft
  rotate: true
  xy: 1450, 6
  size: 105, 128
  orig: 105, 128
  offset: 0, 0
  index: -1
parts/m upper leg rght
  rotate: true
  xy: 954, 29
  size: 105, 128
  orig: 105, 128
  offset: 0, 0
  index: -1
parts/mouth
  rotate: false
  xy: 1608, 79
  size: 87, 82
  orig: 87, 82
  offset: 0, 0
  index: -1
parts/mouth down
  rotate: false
  xy: 1986, 375
  size: 47, 50
  orig: 47, 50
  offset: 0, 0
  index: -1
parts/nose
  rotate: true
  xy: 1986, 427
  size: 83, 57
  orig: 83, 57
  offset: 0, 0
  index: -1
parts/tail
  rotate: false
  xy: 1698, 313
  size: 244, 197
  orig: 244, 197
  offset: 0, 0
  index: -1
spa/spaEyes
  rotate: false
  xy: 1008, 136
  size: 237, 109
  orig: 237, 109
  offset: 0, 0
  index: -1
spa/spaHead
  rotate: true
  xy: 2, 183
  size: 327, 619
  orig: 327, 619
  offset: 0, 0
  index: -1
spa/spaMatt
  rotate: false
  xy: 2, 47
  size: 561, 134
  orig: 561, 134
  offset: 0, 0
  index: -1
spa/spaTowel
  rotate: true
  xy: 623, 169
  size: 341, 383
  orig: 341, 383
  offset: 0, 0
  index: -1
