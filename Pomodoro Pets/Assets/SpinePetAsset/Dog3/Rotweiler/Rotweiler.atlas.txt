
Rotweiler.png
size: 2048,512
format: RGBA8888
filter: Linear,Linear
repeat: none
parts/chest
  rotate: true
  xy: 1317, 88
  size: 276, 236
  orig: 276, 236
  offset: 0, 0
  index: -1
parts/ear lft
  rotate: false
  xy: 1649, 233
  size: 117, 132
  orig: 117, 132
  offset: 0, 0
  index: -1
parts/ear rght
  rotate: false
  xy: 1555, 67
  size: 117, 132
  orig: 117, 132
  offset: 0, 0
  index: -1
parts/eyes
  rotate: true
  xy: 456, 35
  size: 152, 46
  orig: 152, 46
  offset: 0, 0
  index: -1
parts/foot back left
  rotate: false
  xy: 1674, 47
  size: 74, 87
  orig: 74, 87
  offset: 0, 0
  index: -1
parts/foot back right
  rotate: false
  xy: 504, 70
  size: 74, 87
  orig: 74, 87
  offset: 0, 0
  index: -1
parts/foot front left
  rotate: false
  xy: 1674, 136
  size: 68, 95
  orig: 68, 95
  offset: 0, 0
  index: -1
parts/foot front rght
  rotate: false
  xy: 580, 76
  size: 68, 81
  orig: 68, 81
  offset: 0, 0
  index: -1
parts/head
  rotate: true
  xy: 984, 108
  size: 256, 331
  orig: 256, 331
  offset: 0, 0
  index: -1
parts/lower body
  rotate: false
  xy: 2, 2
  size: 231, 185
  orig: 231, 185
  offset: 0, 0
  index: -1
parts/mouth
  rotate: true
  xy: 1953, 372
  size: 128, 91
  orig: 128, 91
  offset: 0, 0
  index: -1
parts/mouth down
  rotate: true
  xy: 738, 110
  size: 47, 88
  orig: 47, 88
  offset: 0, 0
  index: -1
parts/nose
  rotate: false
  xy: 650, 94
  size: 86, 63
  orig: 86, 63
  offset: 0, 0
  index: -1
parts/upper back leg left
  rotate: true
  xy: 1790, 384
  size: 116, 161
  orig: 116, 161
  offset: 0, 0
  index: -1
parts/upper back leg right
  rotate: false
  xy: 235, 26
  size: 117, 161
  orig: 117, 161
  offset: 0, 0
  index: -1
parts/upper front leg right
  rotate: false
  xy: 354, 31
  size: 100, 156
  orig: 100, 156
  offset: 0, 0
  index: -1
parts/upper leg front left
  rotate: false
  xy: 1555, 201
  size: 92, 164
  orig: 92, 164
  offset: 0, 0
  index: -1
spa/spaEyes
  rotate: false
  xy: 1547, 367
  size: 241, 133
  orig: 241, 133
  offset: 0, 0
  index: -1
spa/spaHead
  rotate: true
  xy: 2, 189
  size: 311, 533
  orig: 311, 533
  offset: 0, 0
  index: -1
spa/spaMatt
  rotate: false
  xy: 984, 366
  size: 561, 134
  orig: 561, 134
  offset: 0, 0
  index: -1
spa/spaTowel
  rotate: true
  xy: 537, 159
  size: 341, 445
  orig: 341, 445
  offset: 0, 0
  index: -1
