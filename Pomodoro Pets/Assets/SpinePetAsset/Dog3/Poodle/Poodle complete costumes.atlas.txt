
Poodle complete costumes.png
size: 2048,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
parts/ear lft
  rotate: true
  xy: 971, 777
  size: 102, 552
  orig: 102, 552
  offset: 0, 0
  index: -1
parts/ear rght
  rotate: true
  xy: 971, 881
  size: 126, 544
  orig: 126, 544
  offset: 0, 0
  index: -1
parts/eyes
  rotate: false
  xy: 254, 74
  size: 164, 213
  orig: 164, 213
  offset: 0, 0
  index: -1
parts/head
  rotate: false
  xy: 2, 544
  size: 357, 463
  orig: 357, 463
  offset: 0, 0
  index: -1
parts/m chest
  rotate: false
  xy: 1289, 545
  size: 262, 230
  orig: 262, 230
  offset: 0, 0
  index: -1
parts/m foot back lft
  rotate: false
  xy: 2, 2
  size: 83, 35
  orig: 83, 35
  offset: 0, 0
  index: -1
parts/m foot back rght
  rotate: true
  xy: 1525, 777
  size: 105, 35
  orig: 105, 35
  offset: 0, 0
  index: -1
parts/m front foot lft
  rotate: true
  xy: 459, 289
  size: 64, 71
  orig: 64, 71
  offset: 0, 0
  index: -1
parts/m front foot rght
  rotate: false
  xy: 1562, 815
  size: 70, 67
  orig: 70, 67
  offset: 0, 0
  index: -1
parts/m lower body
  rotate: true
  xy: 1906, 884
  size: 123, 137
  orig: 123, 137
  offset: 0, 0
  index: -1
parts/m upper leg back lft
  rotate: false
  xy: 809, 521
  size: 159, 119
  orig: 159, 119
  offset: 0, 0
  index: -1
parts/m upper leg back rght
  rotate: false
  xy: 634, 517
  size: 173, 123
  orig: 173, 123
  offset: 0, 0
  index: -1
parts/m upper leg lft front
  rotate: false
  xy: 459, 355
  size: 106, 195
  orig: 106, 195
  offset: 0, 0
  index: -1
parts/m upper leg rght front
  rotate: true
  xy: 1517, 884
  size: 123, 188
  orig: 123, 188
  offset: 0, 0
  index: -1
parts/mouth
  rotate: true
  xy: 1707, 898
  size: 109, 197
  orig: 109, 197
  offset: 0, 0
  index: -1
parts/mouth down
  rotate: false
  xy: 567, 442
  size: 59, 108
  orig: 59, 108
  offset: 0, 0
  index: -1
parts/nose
  rotate: true
  xy: 970, 537
  size: 66, 180
  orig: 66, 180
  offset: 0, 0
  index: -1
parts/tail
  rotate: false
  xy: 971, 605
  size: 316, 170
  orig: 316, 170
  offset: 0, 0
  index: -1
spa/spaEyes
  rotate: true
  xy: 254, 289
  size: 253, 203
  orig: 253, 203
  offset: 0, 0
  index: -1
spa/spaHead
  rotate: false
  xy: 634, 642
  size: 335, 365
  orig: 335, 365
  offset: 0, 0
  index: -1
spa/spaMatt
  rotate: true
  xy: 2, 39
  size: 503, 250
  orig: 503, 250
  offset: 0, 0
  index: -1
spa/spaTowel
  rotate: false
  xy: 361, 552
  size: 271, 455
  orig: 271, 455
  offset: 0, 0
  index: -1
