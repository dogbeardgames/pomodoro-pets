
Chow chow.png
size: 2048,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
parts/back foot lft
  rotate: false
  xy: 534, 59
  size: 103, 57
  orig: 103, 57
  offset: 0, 0
  index: -1
parts/back foot rght
  rotate: true
  xy: 932, 375
  size: 103, 71
  orig: 103, 71
  offset: 0, 0
  index: -1
parts/brows
  rotate: false
  xy: 570, 483
  size: 261, 93
  orig: 261, 93
  offset: 0, 0
  index: -1
parts/ear left
  rotate: false
  xy: 819, 357
  size: 111, 121
  orig: 111, 121
  offset: 0, 0
  index: -1
parts/ear right
  rotate: true
  xy: 1769, 406
  size: 111, 121
  orig: 111, 121
  offset: 0, 0
  index: -1
parts/eyes
  rotate: false
  xy: 403, 24
  size: 129, 37
  orig: 129, 37
  offset: 0, 0
  index: -1
parts/foot front left
  rotate: false
  xy: 534, 118
  size: 111, 108
  orig: 111, 108
  offset: 0, 0
  index: -1
parts/foot front right
  rotate: false
  xy: 1014, 471
  size: 114, 105
  orig: 114, 105
  offset: 0, 0
  index: -1
parts/head
  rotate: true
  xy: 565, 578
  size: 442, 569
  orig: 442, 569
  offset: 0, 0
  index: -1
parts/lower body
  rotate: false
  xy: 1136, 492
  size: 366, 272
  orig: 366, 272
  offset: 0, 0
  index: -1
parts/mouth
  rotate: false
  xy: 534, 228
  size: 146, 81
  orig: 146, 81
  offset: 0, 0
  index: -1
parts/nose
  rotate: true
  xy: 755, 323
  size: 158, 62
  orig: 158, 62
  offset: 0, 0
  index: -1
parts/tail1
  rotate: false
  xy: 570, 311
  size: 183, 170
  orig: 183, 170
  offset: 0, 0
  index: -1
parts/tail2
  rotate: true
  xy: 403, 317
  size: 224, 165
  orig: 224, 165
  offset: 0, 0
  index: -1
parts/tail3
  rotate: false
  xy: 833, 480
  size: 179, 96
  orig: 179, 96
  offset: 0, 0
  index: -1
parts/tongue
  rotate: false
  xy: 682, 226
  size: 62, 83
  orig: 62, 83
  offset: 0, 0
  index: -1
parts/upper body
  rotate: true
  xy: 1504, 428
  size: 336, 263
  orig: 336, 263
  offset: 0, 0
  index: -1
parts/upper leg front lft
  rotate: false
  xy: 1900, 548
  size: 146, 223
  orig: 146, 223
  offset: 0, 0
  index: -1
parts/upper leg front rght
  rotate: false
  xy: 1900, 323
  size: 146, 223
  orig: 146, 223
  offset: 0, 0
  index: -1
parts/upperleftlegback
  rotate: false
  xy: 403, 63
  size: 129, 252
  orig: 129, 252
  offset: 0, 0
  index: -1
parts/upperrightlegback
  rotate: false
  xy: 1769, 519
  size: 129, 252
  orig: 129, 252
  offset: 0, 0
  index: -1
spa/spaEyes
  rotate: true
  xy: 1739, 773
  size: 247, 279
  orig: 247, 279
  offset: 0, 0
  index: -1
spa/spaHead
  rotate: false
  xy: 2, 543
  size: 561, 477
  orig: 561, 477
  offset: 0, 0
  index: -1
spa/spaMatt
  rotate: false
  xy: 1136, 766
  size: 601, 254
  orig: 601, 254
  offset: 0, 0
  index: -1
spa/spaTowel
  rotate: false
  xy: 2, 2
  size: 399, 539
  orig: 399, 539
  offset: 0, 0
  index: -1
