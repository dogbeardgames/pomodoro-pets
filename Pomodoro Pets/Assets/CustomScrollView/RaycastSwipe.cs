﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class RaycastSwipe : MonoBehaviour, IBeginDragHandler , IEndDragHandler, IDragHandler, IPointerDownHandler, IPointerUpHandler {

	[SerializeField]
	bool dragging;

	[SerializeField]
	Vector2 beginDragPos, endDragPos;
	float dragDist;


	public bool IsDragging{
		get{ return dragging;}
	}

//	public delegate void Drag ();
//	public Drag WhileDragging;
	public delegate void DragEventHandler(PointerEventData data, Vector2 startPos);
	public DragEventHandler WhileDragging;

	public delegate void DragStartEventHandler(Vector2 startPos);
	public DragStartEventHandler OnDragStart;

	public delegate void DragEndEventHandler(Vector2 endPos);
	public DragEndEventHandler OnDragEnd;

	public delegate void PointerDownEventHandler();
	public PointerDownEventHandler OnPointerPressedDown;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnBeginDrag(PointerEventData data)
	{
		//print ("drag start");
		beginDragPos = data.position;
		if(OnDragStart != null)
		{
			OnDragStart (beginDragPos);
		}
	}

	public void OnEndDrag(PointerEventData data)
	{
		dragging = false;
		//endDragPos = data.position;
		//dragDist = Vector2.Distance (beginDragPos, endDragPos);
		//print ("distance " + dragDist);
		if (OnDragEnd != null) {
			OnDragEnd (data.position);
		}
	}

	public void OnDrag(PointerEventData data)
	{
		dragging = true;

		if (WhileDragging != null) {
			WhileDragging (data, beginDragPos);
		} else {
			//print ("null");
		}
	}

	public void OnPointerDown(PointerEventData data)
	{
		//print ("pointer down");	
		if(OnPointerPressedDown != null)
		{
			OnPointerPressedDown ();
		}
	}

	public void OnPointerUp(PointerEventData data)
	{

	}
}
