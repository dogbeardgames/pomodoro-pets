﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using DG.Tweening;


public class CustomScrollView : MonoBehaviour {

	bool interactible;

	[SerializeField]
	RectTransform content;
	[SerializeField]
	float scrollValue = 0;

	float _y;
	[SerializeField]
	float _maxLeft, _maxRight;

	//velocity var
	[SerializeField]
	float _initialTime, _finalTime;
	[SerializeField]
	Vector2 _initialPos, _finalPos;
	//end velocity

	[SerializeField]
	bool _dragBegun, _dragging, _dragEnd;

	int _direction = 0; //0=initial, 1=left, 2=right
	int _recentDirection = 0;

	float _draggingStayTimer = 0;
	float _draggingStayDelayTimer = 0;
	bool _draggingStayDelayTriggered = false;
	bool _draggingStayTriggered = false;
	Vector2 _draggingDelayPosition;

	public bool Interactible{
		set{ 
			interactible = value;

			swipe.enabled = interactible;
		}
	}

	public RectTransform Content{
		get{ return content;}
	}
	public float MaxLeft{
		get{ return _maxLeft;}
	}
	public float MaxRight{
		get{ return _maxRight;}
	}

//	Sequence _inertiaSequence;
//	short _sequenceState = 0; //0=idle, 1=running, 2=end


	public delegate void ContentMoveEventHandler();
	public ContentMoveEventHandler OnContentMove;

	RaycastSwipe swipe;

	void Start () {
		SetContentXInitialPosition();
		swipe = content.GetComponent<RaycastSwipe> ();
		swipe.WhileDragging += DragContent;
		swipe.WhileDragging += Drag;
		swipe.OnDragStart += BeginDrag;
		swipe.OnDragEnd += DragEnd;
		swipe.OnPointerPressedDown += PointerDown;


		//_inertiaSequence = DOTween.Sequence ();

	}

	public void SetContentXInitialPosition()
	{
		float _contentWidth = content.rect.width;
		float _xPos = 0;
		_xPos = _contentWidth - GetComponent<RectTransform> ().rect.width;
		_xPos = _xPos / 2;

		_maxLeft = _xPos;
		_maxRight = -_xPos;
		content.localPosition = new Vector2 (_xPos, 0f);
		_y = content.localPosition.y;
		scrollValue = 0;
	}

	void FixedUpdate()
	{
		if(_dragBegun)
		{
			_finalTime += Time.deltaTime * 10f;
		}
		if(_dragging)
		{
			_draggingStayTimer += Time.deltaTime * 10f;
			_draggingStayDelayTimer += Time.deltaTime * 10f;
		}
	
	}

	void Update()
	{
		if(_draggingStayDelayTimer >= 0.1f)
		{
			_draggingStayDelayTriggered = true;
		}
		if(_draggingStayTimer >= 0.3f)
		{
			_draggingStayTriggered = true;
		}

		if(OnContentMove != null)
		{
			OnContentMove ();
		}
	}

#region events


	void PointerDown()
	{
		KillSequence();
	}

	void BeginDrag(Vector2 startPos)
	{
		_initialPos = startPos;
		_initialTime = 0f;
		_dragBegun = true;
		_dragging = false;
		_dragEnd = false;
		//print ("drag start");
	}

	void DragEnd(Vector2 endPos)
	{	
		_dragBegun = false;
		_dragEnd = true;
		_dragging = false;

		_finalPos = endPos;

		//print ("drag end");
		//print (MathScript.ins.GetVelocity(_finalPos,_initialPos,_finalTime, _initialTime));
		ScrollInertia (MathScript.ins.GetVelocity(_finalPos,_initialPos,_finalTime, _initialTime));
		_finalTime = 0;

		_direction = 0;
	}

	void Drag(PointerEventData draggingPos, Vector2 dragPos)
	{
		//print ("dragging velo " + MathScript.ins.GetVelocity(_finalPos,_initialPos,_finalTime, _initialTime  ) );
		UpdateMouseDirection(draggingPos);
	}

	void DragContent(PointerEventData data, Vector2 startPos)
	{
		//area of improvement MAX movement calculation

		//if(data.position.x > startPos.x)
		if(_direction == 2)
		{
			if(content.localPosition.x + (Vector2.Distance(data.position, _initialPos) * 0.1f) < _maxLeft)
			{
				content.localPosition = new Vector2 (content.localPosition.x + Vector2.Distance(data.position, _initialPos) * 0.1f, _y);
			}
		}
		//else if(data.position.x < startPos.x)
		else if(_direction == 1)
		{
			if (content.localPosition.x - (Vector2.Distance (data.position, _initialPos) * 0.1f) > _maxRight) 
			{
				content.localPosition = new Vector2 (content.localPosition.x - Vector2.Distance(data.position, _initialPos) * 0.1f, _y);
			}
		}
		DraggingStay (data, 0.3f);
		_dragging = true;
	}

	void UpdateMouseDirection(PointerEventData draggingPos)
	{
		if(Input.GetAxis("Mouse X")<0)
		{
			//reset initial variables on direction changed
			if (_recentDirection == 2 || _recentDirection == 0)
			{
				_recentDirection = 1;
				_initialPos = draggingPos.position;
				_finalTime = 0;
			}
			_direction = 1;

		}
		else if(Input.GetAxis("Mouse X")>0)
		{
			//reset initial variables on direction changed
			if (_recentDirection == 1 || _recentDirection == 0)
			{
				_initialPos = draggingPos.position;
				_recentDirection = 2;
				_finalTime = 0;
			}
			_direction = 2;

		}
	}

	void DraggingStay(PointerEventData data, float duration)
	{
		//dragging stay is the time when it is being drag but not moving
		if(_draggingStayDelayTriggered)
		{
			_draggingDelayPosition = data.position;
			_draggingStayDelayTimer = 0;
			_draggingStayDelayTriggered = false;
		}

		if(_draggingStayTriggered)
		{
			if(data.position.x == _draggingDelayPosition.x)
			{
				_finalTime = 0;
				_initialPos = data.position;
			}
			_draggingStayTimer = 0;
			_draggingStayTriggered = false;
		}

	}


#endregion

	#region tween events



	void KillSequence()
	{
		//print ("tween killed!");
		DOTween.Kill("inertia");
	}


	void ScrollInertia(float velocity)
	{
		//mass is given = 1kg
		//force = m*a
		float _xPos = 0;

		try
		{
			if(_direction == 1)
			{
				_xPos = content.localPosition.x - velocity * 1.5f;
				//print ("1 --> " + _xPos );
				if (_xPos < _maxRight) {
					_xPos = _maxRight;

					content.DOLocalMoveX (_xPos, 1f, false).SetId("inertia");//.SetEase(Ease.Linear);
				} else {

					content.DOLocalMoveX (_xPos, 1f, false).SetId("inertia");//.SetEase(Ease.Linear);
				}
			}
			else if(_direction == 2)
			{
				_xPos = content.localPosition.x + velocity * 1.5f;
				//print ("2 --> " + _xPos );
				if (_xPos > _maxLeft) {
					_xPos = _maxLeft;

					content.DOLocalMoveX (_xPos, 1f, false).SetId("inertia");//.SetEase(Ease.Linear);
				} else {
					content.DOLocalMoveX (_xPos, 1f, false).SetId("inertia");//.SetEase(Ease.Linear);
				}
			}
		}catch(Exception ex)
		{
			
		}

	}
	#endregion

}
