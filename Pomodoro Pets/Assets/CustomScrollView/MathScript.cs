﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MathScript : MonoBehaviour {

	public static MathScript ins;
	// Use this for initialization
	void Awake () {
		ins = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public float GetVelocity(Vector2 finalPos, Vector2 initialPos, float finalTime, float initialTime )
	{
		return (Vector2.Distance(finalPos, initialPos)) / (finalTime - initialTime); 
	}
}
